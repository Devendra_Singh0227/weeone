//
//  ExtendReserveTimeVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 17/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class ExtendReserveTimeVC: UIViewController {

    //MARK: variables
    var activeRideObject : RideListModel?
    var totalHrs = 0
    var selectedHrs = 0
    var isApiCalled : ((_ model : RideListModel) -> ())?
    
    //MARK: Outlets
    @IBOutlet weak var viewExtend : UIView!
    @IBOutlet weak var btnMinus : UIButton!
    @IBOutlet weak var btnPlus : UIButton!
    @IBOutlet weak var lblHour : UILabel!
    @IBOutlet weak var viewMain : UIView!
    @IBOutlet weak var btnExtendReservation : UIButtonCommon!

    //MARK: View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
    }
    
    //MARK: Initial config
    func initialConfig(){
        
        btnExtendReservation.click = {
            self.btnExtendReservation_Click()
        }
        
        let timeLimit : Double = Utilities.getDoubleValue(value: activeRideObject?.partnerSetting?.extendTimeLimit)
        totalHrs = Int(floor(timeLimit/60))
        
        if totalHrs < 1{
            btnPlus.isUserInteractionEnabled = false
            btnPlus.setTitleColor(ColorConstants.TextColorSecondary, for: .normal)
        }
        
        btnMinus.isUserInteractionEnabled = false
        btnMinus.setTitleColor(ColorConstants.TextColorSecondary, for: .normal)
        
        selectedHrs = 1
        
        lblHour.text = "\(selectedHrs) Hours"
    }
    
    //MARK: Button action methods
    @IBAction func btnExtendReservation_Click(){
        callApiForExtendReservationTime()
    }
    
    @IBAction func btnMinus_Click(_ sender : UIButton){
        
        if selectedHrs > 1{
            
            selectedHrs = selectedHrs - 1
            lblHour.text = "\(selectedHrs) Hours"
        }
        
        if selectedHrs > 1{
            btnPlus.setTitleColor(ColorConstants.TextColorTheme, for: .normal)
            btnPlus.isUserInteractionEnabled = true
        }else{
            btnMinus.setTitleColor(ColorConstants.TextColorTheme, for: .normal)
            btnMinus.isUserInteractionEnabled = false
            
            btnPlus.setTitleColor(ColorConstants.TextColorTheme, for: .normal)
            btnPlus.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func btnPlus_Click(_ sender : UIButton){
        
        if selectedHrs < totalHrs{
            selectedHrs = selectedHrs + 1
            lblHour.text = "\(selectedHrs) Hours"
        }
        
        if selectedHrs < totalHrs{
            btnMinus.setTitleColor(ColorConstants.TextColorTheme, for: .normal)
            btnMinus.isUserInteractionEnabled = true
        }else{
            btnPlus.setTitleColor(ColorConstants.TextColorSecondary, for: .normal)
            btnPlus.isUserInteractionEnabled = false
            
            btnMinus.setTitleColor(ColorConstants.TextColorTheme, for: .normal)
            btnMinus.isUserInteractionEnabled = true
        }
        
    }
    
    @IBAction func btnClose_Click(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: Api calls
extension ExtendReserveTimeVC{
    
    func callApiForExtendReservationTime(){
        
        var param : [String:Any] = [:]
        param["rideId"] = activeRideObject?.id
        param["timeLimit"] = selectedHrs * 60
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.ExtendReservation, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any]{
                
                SyncManager.sharedInstance.syncMaster({
                    self.dismiss(animated: true) {
                        self.isApiCalled?(Mapper<RideListModel>().map(JSON: dictResponse)!)
                    }
                }) {
                    
                }
                
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
