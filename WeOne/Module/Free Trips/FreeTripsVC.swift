//
//  FreeTripsVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 03/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class FreeTripsVC: UIViewController {

    //MARK: - Variables
    var arrData = [CellModel]()
    
    //MARK: - Outlets
    @IBOutlet weak var btnInviteFriends: UIButtonCommon!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }


    //MARK: Private Methods
    func initialConfig() {
        
        tableView.register(UINib(nibName: "FreeTripsStaticTextCell", bundle: nil), forCellReuseIdentifier: "FreeTripsStaticTextCell")
        tableView.register(UINib(nibName: "FreeTripsStaticImageCell", bundle: nil), forCellReuseIdentifier: "FreeTripsStaticImageCell")
        tableView.register(UINib(nibName: "FreeTripsInviteCodeCell", bundle: nil), forCellReuseIdentifier: "FreeTripsInviteCodeCell")        
        
        prepareDataSource()
    }
    
    func prepareDataSource() {
        arrData.removeAll()
        
        arrData.append(CellModel.getModel(placeholder: "KLblFreeTrips1".localized, type: .FreeTripsStaticText, imageName: "", cellObj: nil))
        
        arrData.append(CellModel.getModel(placeholder: "", type: .FreeTripsImage, imageName: "freetrips", cellObj: nil))
        
        arrData.append(CellModel.getModel(placeholder: "KLblFreeTrips2".localized, type: .FreeTripsStaticText, imageName: "", cellObj: nil))
        
        arrData.append(CellModel.getModel(placeholder: "KLblFreeTrips3".localized, type: .FreeTripsStaticText, imageName: "", cellObj: nil))
        
        arrData.append(CellModel.getModel(placeholder: "NS07XEX8MG07", placeholder2: "", text: "", type: .FreeTripsShareCode, imageName: "", cellObj: nil))
        
        
        tableView.reloadData()
    }
    
    //MARK:- IBActions
    @IBAction func btnBack_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK: - UITableView's DataSource & Delegate Methods
extension FreeTripsVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = arrData[indexPath.row]
        
        switch model.cellType! {
            
        case .FreeTripsStaticText:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FreeTripsStaticTextCell") as? FreeTripsStaticTextCell
            cell?.setData(model: model)
            return cell!
            
        case .FreeTripsImage:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FreeTripsStaticImageCell") as? FreeTripsStaticImageCell
            cell?.setData(model: model)
            return cell!
            
        case .FreeTripsShareCode:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FreeTripsInviteCodeCell") as? FreeTripsInviteCodeCell
            cell?.setData(model: model)
            return cell!
            
            
        default:
            return UITableViewCell()
        }
        
        
    }
}

//MARK:- Extra Methods
extension FreeTripsVC {
    
    convenience init() {
        self.init(nibName: "FreeTripsVC", bundle: nil)
    }
}
