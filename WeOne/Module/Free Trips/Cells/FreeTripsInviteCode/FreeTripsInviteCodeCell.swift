//
//  FreeTripsInviteCodeCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 03/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class FreeTripsInviteCodeCell: UITableViewCell {

    //MARK: - Variables
    var cellModel:CellModel?
    
    //MARK: - Outlets
    @IBOutlet weak var lblInviteCode: UILabel!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        lblInviteCode.text = model.placeholder
    }
    
}
