//
//  FreeTripsStaticTextCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 03/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class FreeTripsStaticTextCell: UITableViewCell {

    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        lblTitle.text = model.placeholder
        lblTitle.setLineSpacing(lineSpacing: 1.0, lineHeightMultiple: 1.4)
    }
    
}
