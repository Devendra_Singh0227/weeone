//
//  AddSavePlaceVC.swift
//  WeOne
//
//  Created by iMac on 30/03/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class AddSavePlaceVC: UIViewController {

    //MARK: outlets
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var viewName: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewSaved: UITableView!

    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtName: UITextField!

    @IBOutlet weak var btnSave: UIButtonCommon!
    @IBOutlet weak var btnSavedPlaces: UIButton!

    @IBOutlet weak var imgFav: UIImageView!
    
    @IBOutlet weak var constrainTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constrainTableViewSavedHeight: NSLayoutConstraint!
    @IBOutlet weak var constrainTxtNameHeight: NSLayoutConstraint!
    @IBOutlet weak var constrainViewHeight: NSLayoutConstraint!

    //MARK: variables
    var activeTextField = UITextField()
    
    var getLocation : ((_ model : PlacesModel) -> ())?
    var arrSearchPlace : [PlacesModel] = []
    var arrPlace : [FavouritePlaceModel] = []

    var placesClient : GMSPlacesClient = GMSPlacesClient()

    var startLocation: PlacesModel?
    var stopLocation: PlacesModel?
    var endLocation: PlacesModel?

    var rideModel:RideListModel = RideListModel()
    
    var isFromSetting = false

    override func viewDidLoad() {
        super.viewDidLoad()

        initialConfig()
        
    }
    
    func initialConfig() {
        
        imgFav.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Fav"))
        tableView.register(UINib(nibName: "PlacePickerCell", bundle: nil), forCellReuseIdentifier: "PlacePickerCell")
        tableViewSaved.register(UINib(nibName: "SavedPlaceCell", bundle: nil), forCellReuseIdentifier: "SavedPlaceCell")

        LocationManager.SharedManager.getLocation()
        LocationManager.SharedManager.getCurrentLocation = { location in
            
            self.getAddressFromLatLon(pdblLatitude: location.coordinate.latitude, withLongitude: location.coordinate.longitude) { (address) in
                //            self.lblFrom.text = address
            }
        }
        
        txtAddress.delegate = self
        txtName.delegate = self

        txtAddress.tintColor = ColorConstants.ThemeColor
        txtName.tintColor = ColorConstants.ThemeColor

        viewAddress.layer.cornerRadius = viewAddress.frame.height / 2
        viewName.layer.cornerRadius = viewName.frame.height / 2

        setData()
        
        setNevigationLayout()
        
        btnSave.click = {
            self.btnSave_click()
        }
        
        txtAddress.becomeFirstResponder()

    }
    func setNevigationLayout() {
        
              
        prepareDataSource()
    }
    
    func prepareDataSource(){
        arrPlace = SyncManager.sharedInstance.fetchAllFavouritePlaces()
        
        if arrPlace.count > 0 {
            btnSavedPlaces.isHidden = false
            tableViewSaved.isHidden = false
        } else {
            btnSavedPlaces.isHidden = true
            tableViewSaved.isHidden = true
        }
        
        tableViewSaved.reloadData()
    }
    
    func setData() {
        let model = PlacesModel()

        if rideModel.startLocation != nil{
            txtAddress.text = rideModel.startLocation?.name
            
            model.addressFullText = rideModel.startLocation?.name
            model.latitude = rideModel.startLocation?.latitude
            model.longitude = rideModel.startLocation?.longitude


        } else if rideModel.geoLocation != nil {
            txtAddress.text = rideModel.geoLocation?.name
            
            model.addressFullText = rideModel.geoLocation?.name
            model.latitude = rideModel.geoLocation?.latitude
            model.longitude = rideModel.geoLocation?.longitude

        } else {
           // txtAddress.textField.placeholder = "KLblSelectSource".localized
        }
        
        startLocation = model
        
    }

    func btnSave_click() {
        
        guard !Utilities.checkStringEmptyOrNil(str: txtAddress.text) else {
            self.showToast(text: "KlblEnterAddress".localized)

//            Utilities.showAlertView(message: "KlblEnterAddress".localized)
            return
        }
        
        
        startLocation?.name = txtName.text
        callApiForAddRemoveFavPlace(action: AppConstants.Action.FAVOURITE, startLocation!)
        
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnSavedPlace(_ sender: UIButton) {
        
        constrainTableViewHeight.constant = 0
        constrainTxtNameHeight.constant = 57
        constrainViewHeight.constant = 167
        self.viewName.isHidden = false

    }

    @objc func btnRemoveFav_Click(_ sender: UIButton) {
        
        let place = self.arrPlace[sender.tag]
        let model = PlacesModel()

        self.view.showConfirmationPopupWithMultiButton(title: "KLblRemoveSavedPlace".localized, message: "\("KLblRemoveMsg".localized) \(place.geoLocation?.name ?? "")?", cancelButtonTitle: "KLblCancelCap".localized, isAttributedChangeColor: true, strAttribute: place.geoLocation?.name ?? "",  confirmButtonTitle: "KLblConfirmCap".localized, onConfirmClick: {
            // Confirm Click
            
            model.name = place.name
            model.addressFullText = place.geoLocation?.name
            
            model.latitude = place.geoLocation?.latitude
            model.longitude = place.geoLocation?.longitude
            model.id = place.id
            
            self.callApiForAddRemoveFavPlace(action: AppConstants.Action.REMOVE_FROM_FAVOURITE, model, index: sender.tag)
            
        }) {
            // cancel click
        }
    }
}

//MARK: API Calling
extension AddSavePlaceVC {
    func callApiForAddRemoveFavPlace(action: Int, _ model : PlacesModel, index: Int = 0){
        
        var param : [String:Any] = [:]
        param["action"] = action
        param["name"] = model.name
        if action == AppConstants.Action.REMOVE_FROM_FAVOURITE {
            param["id"] = model.id
        }
        
        var location : [String:Any] = [:]
        location["type"] = "point"
        location["name"] = model.addressFullText
        location["coordinates"] = [model.longitude ?? 0, model.latitude ?? 0]
     
        param["geoLocation"] = location
    
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.AddRemoveFavPlace, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any]{

                if let arrFavouritePlace = dictResponse["favouritePlaces"] as? [[String:Any]], arrFavouritePlace.count > 0 {
                    SyncManager.sharedInstance.insertFavouritePlaces(list: arrFavouritePlace)
                }
            
                if action == AppConstants.Action.REMOVE_FROM_FAVOURITE {
                    SyncManager.sharedInstance.removeFavourite(model.id ?? "")
                    self.prepareDataSource()
                } else {
                    self.getLocation?(model)
                    self.navigationController?.popViewController(animated: true)
                }

            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }

    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double, getFullAddress : ((_ address : String) -> ())?) {

        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude
        let lon: Double = pdblLongitude
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)

        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)

        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in

                NetworkClient.sharedInstance.stopIndicator()

                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                if placemarks != nil {
                    let pm = placemarks! as [CLPlacemark]

                    if pm.count > 0 {

                        let pm = placemarks![0]


                        var arrLoc = [String]()
                        arrLoc.append(pm.subLocality ?? "")
                        arrLoc.append(pm.locality ?? "")
                        arrLoc.append(pm.administrativeArea ?? "")
                        arrLoc.append(pm.country ?? "")

                        getFullAddress?(arrLoc.joined(separator: ","))
                     
                    }
                }
        })
    }
    
    func setButtonEnable(_ value : Bool){
        
        if value{
            self.btnSave.button.isEnabled = true
            self.btnSave.alpha = 1
        }else{
            self.btnSave.button.isEnabled = false
            self.btnSave.alpha = 0.5
        }
    }
}
//MARK: Tableview methods
extension AddSavePlaceVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView != tableViewSaved {
            return arrSearchPlace.count
        } else {
            return arrPlace.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView != tableViewSaved {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlacePickerCell") as! PlacePickerCell
            cell.setCellData(model: arrSearchPlace[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SavedPlaceCell") as! SavedPlaceCell
            cell.setCellData(model: arrPlace[indexPath.row])
            cell.btnRemove.tag = indexPath.row
            cell.btnRemove.addTarget(self, action: #selector(self.btnRemoveFav_Click(_:)), for: .touchUpInside)

            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView != tableViewSaved {
            constrainTableViewHeight.constant = 0
            constrainTxtNameHeight.constant = 57
            constrainViewHeight.constant = 167
            
            self.viewName.isHidden = false
            
            getLocationCoordinate(arrSearchPlace[indexPath.row])
        } else {
            if !isFromSetting {
                
                let place = arrPlace[indexPath.row]
                let model = PlacesModel()
                
                model.name = place.name
                model.addressFullText = place.geoLocation?.name
                
                model.longitude = place.geoLocation?.longitude
                model.latitude = place.geoLocation?.latitude
                model.id = place.id
                
                getLocation?(model)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }

}
//MARK: textfield delegate methods
extension AddSavePlaceVC : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
            self.tableView.isHidden = true
            self.arrSearchPlace.removeAll()
            self.tableView.reloadData()
        
        if textField.isEditing {
            textField.PlaceHolderColorKey = "kTextColorSecondary"
            if textField.tag == 0 {
                viewAddress.layer.borderColor = ColorConstants.ThemeColor.cgColor
            } else {
                viewName.layer.borderColor = ColorConstants.ThemeColor.cgColor
            }
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.PlaceHolderColorKey = "kTextColorSecondaryAlpha"
        if textField.tag == 0 {
            viewAddress.layer.borderColor = ColorConstants.UnderlineColor.cgColor
        } else {
            viewName.layer.borderColor = ColorConstants.UnderlineColor.cgColor
        }

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        
        if range.location == 0{
            
            if string == " " {
                return false
            }
        }
        
        let text = textField.text!
        let textRange = Range(range, in: text)
        let updatedText = text.replacingCharacters(in: textRange!, with: string)
        
        if textField == self.txtAddress {
            
            self.setButtonEnable(false)
            
            if updatedText.count != 0 {
                
                DispatchQueue.main.async {
                    self.placeAutocomplete(searchText: updatedText)
                }
                
            } else {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                    self.arrSearchPlace.removeAll()
                    self.tableView.reloadData()
                    self.tableView.isHidden = true
                    self.constrainTableViewHeight.constant = 0
                    self.constrainTxtNameHeight.constant = 57
                    self.constrainViewHeight.constant = 167

                    self.viewName.isHidden = false

                })
            }
        }
        
        return true
    }
}

//MARK: Google auto places complete api
extension AddSavePlaceVC {
    
    //Place Auto Complete
    func placeAutocomplete(searchText : String) {
        
        arrSearchPlace.removeAll()
        tableView.reloadData()
        
        placesClient.autocompleteQuery(searchText, bounds: nil, filter: nil, callback: {(results, error) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            if let results = results {
                for result in results {
                    let model = PlacesModel()
                    model.addressFullText = result.attributedFullText.string
                    model.addressPrimaryText = result.attributedPrimaryText.string
                    model.addressSecondaryText = result.attributedSecondaryText?.string
                    model.placeId = result.placeID
                    self.arrSearchPlace.append(model)
                }
                self.tableView.isHidden = false
                self.constrainTableViewHeight.constant = 318
                self.constrainTxtNameHeight.constant = 0
                self.constrainViewHeight.constant = 110

                self.viewName.isHidden = true

                self.tableView.reloadData()
            }
        })
    }
    
    //Get Location Coordinate
    func getLocationCoordinate(_ model : PlacesModel){
        
        placesClient.lookUpPlaceID(model.placeId ?? "", callback: { (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            
            guard let _ = place else {
                print("No place details for \(model.placeId ?? "")")
                return
            }
            
            model.latitude  = place!.coordinate.latitude
            model.longitude = place!.coordinate.longitude
            
            if self.txtAddress.isEditing {
                
                self.txtAddress.text = model.addressFullText
//                self.txtAddress.textField.endEditing(true)
                self.startLocation = model
                self.txtAddress.text = self.startLocation?.addressFullText
                
                
            }
            
            self.arrSearchPlace.removeAll()
            self.tableView.reloadData()
            self.setButtonEnable(true)
        })
    }
}
