//
//  PlacePickerVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 17/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class PlacePickerVC: UIViewController {
    
    //MARK: variables
    var placesClient : GMSPlacesClient = GMSPlacesClient()
    var arrSearchPlace : [PlacesModel] = []
    var arrNavigationPlace : [[String:Any]] = []
    var getDestination : ((_ model : [[String:Any]]) -> ())?
    
    //MARK: outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewStopLocation: UIView!
    @IBOutlet weak var viewReturnAddress: UIView!
    
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnSwap: UIButton!
    @IBOutlet weak var btnSavedPlace: UIButton!
    
    @IBOutlet weak var txtCurrentLocation: UITextField!
    @IBOutlet weak var txtDestination: UITextField!
    @IBOutlet weak var txtStopLocation: UITextField!
    
    @IBOutlet weak var constraintHeightViewNavigation: NSLayoutConstraint!
    @IBOutlet weak var constraintTrailingViewAdd: NSLayoutConstraint!
    
    @IBOutlet weak var viewAdd: ShadowCard!
    @IBOutlet weak var imgAdd: UIImageView!
    @IBOutlet weak var imgStop: UIImageView!
    @IBOutlet weak var imgEnd: UIImageView!
    
    @IBOutlet weak var imgNevigate: UIImageView!

    @IBOutlet weak var lblReturnAddress: UILabel!

    var rideModel:RideListModel = RideListModel()
    
    var startLocation: PlacesModel?
    var stopLocation: PlacesModel?
    var endLocation: PlacesModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
    }
    
    //MARK: Private Methods
    func initialConfig() {
        
        imgAdd.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Add"))
        imgNevigate.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "gps"))
        btnSavedPlace.setImage(UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Fav")), for: .normal)
        btnSwap.setImage(UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Swap")), for: .normal)

        tableView.register(UINib(nibName: "PlacePickerCell", bundle: nil), forCellReuseIdentifier: "PlacePickerCell")

        LocationManager.SharedManager.getLocation()
        LocationManager.SharedManager.getCurrentLocation = { location in
            
            self.getAddressFromLatLon(pdblLatitude: location.coordinate.latitude, withLongitude: location.coordinate.longitude) { (address) in
                //            self.lblFrom.text = address
            }
        }

        txtCurrentLocation.delegate = self
        txtStopLocation.delegate = self
        txtDestination.delegate = self
        
        setData()
        
        setNevigationLayout()
        arrNavigationPlace.removeAll()
    }
    
    func setNevigationLayout() {
        tableView.isHidden = true
        
    }
    
    
    func isValid() {
        if !Utilities.checkStringEmptyOrNil(str: txtCurrentLocation?.text) {
            return
        }
        
        if !Utilities.checkStringEmptyOrNil(str: txtStopLocation?.text) {
            return
        }
        
        if !Utilities.checkStringEmptyOrNil(str: txtDestination?.text) {
            return
        }
        
        self.btnDone.isEnabled = true
        self.btnDone.alpha = 1
    }
    
    func setData() {
        
        
        var model = PlacesModel()
        
        lblReturnAddress.text = rideModel.estimateReturnLocation?.name
        
        if rideModel.navigateLocations.count == 3 {
             txtCurrentLocation.text = rideModel.navigateLocations[0].name
             
            model = PlacesModel()

             model.addressFullText = rideModel.navigateLocations[0].name
             model.longitude = rideModel.navigateLocations[0].coordinates?.first
             model.latitude = rideModel.navigateLocations[0].coordinates?.last

             startLocation = model
            
            txtStopLocation.text = rideModel.navigateLocations[1].name
            
            model = PlacesModel()

            model.addressFullText = rideModel.navigateLocations[1].name
            model.longitude = rideModel.navigateLocations[1].coordinates?.first
            model.latitude = rideModel.navigateLocations[1].coordinates?.last

            stopLocation = model

             txtDestination.text = rideModel.navigateLocations[2].name
             
            model = PlacesModel()

             model.addressFullText = rideModel.navigateLocations[2].name
             model.longitude = rideModel.navigateLocations[2].coordinates?.first
             model.latitude = rideModel.navigateLocations[2].coordinates?.last

             endLocation = model

        } else if rideModel.navigateLocations.count == 2 {
            txtCurrentLocation.text = rideModel.navigateLocations[0].name
            
            model = PlacesModel()

            model.addressFullText = rideModel.navigateLocations[0].name
            model.longitude = rideModel.navigateLocations[0].coordinates?.first
            model.latitude = rideModel.navigateLocations[0].coordinates?.last

            startLocation = model

            txtDestination.text = rideModel.navigateLocations[1].name
            
            model = PlacesModel()

            model.addressFullText = rideModel.navigateLocations[1].name
            model.longitude = rideModel.navigateLocations[1].coordinates?.first
            model.latitude = rideModel.navigateLocations[1].coordinates?.last

            endLocation = model

        } else {
            if rideModel.startLocation != nil{
                txtCurrentLocation.text = rideModel.startLocation?.name
                
                model.addressFullText = rideModel.startLocation?.name
                model.longitude = rideModel.startLocation?.longitude
                model.latitude = rideModel.startLocation?.latitude


            } else if rideModel.geoLocation != nil {
                txtCurrentLocation.text = rideModel.geoLocation?.name
                
                model.addressFullText = rideModel.geoLocation?.name
                model.longitude = rideModel.geoLocation?.longitude
                model.latitude = rideModel.geoLocation?.latitude

            }
            
            
            startLocation = model
            
            model = PlacesModel()
            
            if rideModel.estimateEndLocation != nil {
                txtDestination.text = rideModel.estimateEndLocation?.name
             
                model.addressFullText = rideModel.estimateEndLocation?.name
                model.longitude = rideModel.estimateEndLocation?.longitude
                model.latitude = rideModel.estimateEndLocation?.latitude

            } else if rideModel.endLocation != nil {
                txtDestination.text = rideModel.endLocation?.name
                
                model.addressFullText = rideModel.endLocation?.name
                model.longitude = rideModel.endLocation?.longitude
                model.latitude = rideModel.endLocation?.latitude

            }
            
            endLocation = model

        }
    }
    
    //MARK:- IBActions
    @IBAction func btnAddStop_Click(_ sender: UIButton) {
        viewStopLocation.isHidden = !viewStopLocation.isHidden
        btnSwap.isHidden = viewStopLocation.isHidden
        
        if viewStopLocation.isHidden {
            imgAdd.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Add"))

            viewAdd.layer.shadowOffset = CGSize.zero
            viewAdd.layer.shadowColor = ColorConstants.ShadowColor.cgColor
            constraintHeightViewNavigation.constant = 140
            constraintTrailingViewAdd.constant = 36
            
            imgEnd.image = #imageLiteral(resourceName: "aim")

        } else {
            imgAdd.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Minus"))

            viewAdd.layer.shadowOffset = CGSize.zero
            viewAdd.layer.shadowColor = UIColor.clear.cgColor
            constraintHeightViewNavigation.constant = 210
            constraintTrailingViewAdd.constant = 12
            
            imgStop.image = #imageLiteral(resourceName: "Stop")
            imgEnd.image = #imageLiteral(resourceName: "Destination")

        }
    }
    
    @IBAction func btnSwap_Click(_ sender: UIButton) {
        
        let tempLocation = stopLocation
        stopLocation = endLocation
        endLocation = tempLocation
        
        self.txtStopLocation.text = self.stopLocation?.addressFullText
        self.txtDestination.text = self.endLocation?.addressFullText
        
    }
    
    @IBAction func btnClose_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDone_Click(_ sender: UIButton) {
        
        guard !Utilities.checkStringEmptyOrNil(str: txtCurrentLocation.text) && startLocation != nil else {
//            Utilities.showAlertView(message: "KLblEnterSourceLocation".localized)
            self.showToast(text: "KLblEnterSourceLocation".localized)

            return
        }
        
        guard !Utilities.checkStringEmptyOrNil(str: txtDestination.text) && endLocation != nil else {
//            Utilities.showAlertView(message: "KLblEnterDestinationLocation".localized)
            self.showToast(text: "KLblEnterDestinationLocation".localized)

            return
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnClearSource_Click(_ sender: UIButton) {
        self.arrSearchPlace.removeAll()
        self.tableView.reloadData()
        
        startLocation = nil
    }
    
    @IBAction func btnClearDestination_Click(_ sender: UIButton) {
        
        self.arrSearchPlace.removeAll()
        self.tableView.reloadData()
            
        endLocation = nil
        
    }
    
    @IBAction func btnSavedPlace(_ sender: UIButton) {
        
        if SyncManager.sharedInstance.fetchAllFavouritePlaces().count > 0{
            
            let vc = SavedPlaceListVC()
//            if txtCurrentLocation.isEditing{
//                vc.activeTextField = txtCurrentLocation
//             }else if txtStopLocation.isEditing {
//                 vc.activeTextField = txtStopLocation
//             }else if txtDestination.isEditing{
//                 vc.activeTextField = txtDestination
//             }
             
            vc.getLocation = { placeModel in
//                if vc.activeTextField == self.txtCurrentLocation {
//                    self.txtCurrentLocation.text = placeModel.addressFullText
//                    self.startLocation = placeModel
//                } else if vc.activeTextField == self.txtStopLocation {
//                    self.txtStopLocation.text = placeModel.addressFullText
//                    self.stopLocation = placeModel
//                } else if vc.activeTextField == self.txtDestination {
//                    self.txtDestination.text = placeModel.addressFullText
//                    self.endLocation = placeModel
//                }
                print(placeModel)
            }
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
        }else{
            moveToAddPlaceVC()
        }
    }
    
    @IBAction func btnAddPlace_Click(_ sender: UIButton) {
        moveToAddPlaceVC()
    }
    
    @IBAction func btnShowOnMap(_ sender: UIButton) {
        
        guard !Utilities.checkStringEmptyOrNil(str: txtCurrentLocation.text) && startLocation != nil else {
//            Utilities.showAlertView(message: "KLblEnterSourceLocation".localized)
            self.showToast(text: "KLblEnterSourceLocation".localized)

            return
        }
        
        guard !Utilities.checkStringEmptyOrNil(str: txtDestination.text) && endLocation != nil else {
            self.showToast(text: "KLblEnterDestinationLocation".localized)

//            Utilities.showAlertView(message: "KLblEnterDestinationLocation".localized)
            return
        }
        
        
        arrNavigationPlace.removeAll()
        
        if startLocation != nil {
            
            var location : [String:Any] = [:]
            location["type"] = "point"
            location["name"] = startLocation?.addressFullText
            location["coordinates"] = [startLocation?.longitude ?? 0, startLocation?.latitude ?? 0]
            
            
            arrNavigationPlace.append(location)
        }
        
        if stopLocation != nil {
            
            var location : [String:Any] = [:]
            location["type"] = "point"
            location["name"] = stopLocation?.addressFullText
            location["coordinates"] = [stopLocation?.longitude ?? 0, stopLocation?.latitude ?? 0]
            arrNavigationPlace.append(location)
            
        }
        
        if endLocation != nil {
            var location : [String:Any] = [:]
            location["type"] = "point"
            location["name"] = endLocation?.addressFullText
            location["coordinates"] = [endLocation?.longitude ?? 0, endLocation?.latitude ?? 0]
            arrNavigationPlace.append(location)
        }
        
        self.getDestination?(arrNavigationPlace)
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnNevigate(_ sender: UIButton) {
        
        guard !Utilities.checkStringEmptyOrNil(str: txtCurrentLocation.text) && startLocation != nil else {
            self.showToast(text: "KLblEnterSourceLocation".localized)

//            Utilities.showAlertView(message: "KLblEnterSourceLocation".localized)
            return
        }
        
        guard !Utilities.checkStringEmptyOrNil(str: txtDestination.text) && endLocation != nil else {
            self.showToast(text: "KLblEnterDestinationLocation".localized)

//            Utilities.showAlertView(message: "KLblEnterDestinationLocation".localized)
            return
        }
        
        if let model = endLocation {
            Utilities.openMap(latitude: model.longitude ?? 0.0, longitude: model.latitude ?? 0.0)
        }
        
        
    }
    
    @IBAction func btnSetLocationOnMap(_ sender: UIButton) {
        
        let vc = SetLocationVC()
        
        if self.txtDestination.isEditing {
            vc.placesModel.textField = "txtDestination"
        } else if self.txtStopLocation.isEditing {
            vc.placesModel.textField = "txtStopLocation"
        } else {
            vc.placesModel.textField = "txtCurrentLocation"
        }
        
        vc.getLocation = { placeModel in
            if placeModel.textField == "txtCurrentLocation" {
                self.txtCurrentLocation.text = placeModel.addressFullText
                self.startLocation = placeModel
            } else if placeModel.textField == "txtStopLocation" {
                self.txtStopLocation.text = placeModel.addressFullText
                self.stopLocation = placeModel
            } else {
                self.txtDestination.text = placeModel.addressFullText
                self.endLocation = placeModel
            }
            print(placeModel)
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: private methods
    func moveToAddPlaceVC(){
        let vc = AddSavePlaceVC(nibName: "AddSavePlaceVC", bundle: nil)
        
        if txtCurrentLocation.isEditing{
            vc.activeTextField = txtCurrentLocation
         }else if txtStopLocation.isEditing {
             vc.activeTextField = txtStopLocation
         }else if txtDestination.isEditing{
             vc.activeTextField = txtDestination
         }
         
        vc.getLocation = { placeModel in
            if vc.activeTextField == self.txtCurrentLocation {
                self.txtCurrentLocation.text = placeModel.addressFullText
                self.startLocation = placeModel
            } else if vc.activeTextField == self.txtStopLocation {
                self.txtStopLocation.text = placeModel.addressFullText
                self.stopLocation = placeModel
            } else if vc.activeTextField == self.txtDestination{
                self.txtDestination.text = placeModel.addressFullText
                self.endLocation = placeModel
            }
            print(placeModel)
        }
        UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: Location manager delegate
extension PlacePickerVC{
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double, getFullAddress : ((_ address : String) -> ())?) {
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude
        let lon: Double = pdblLongitude
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                
                NetworkClient.sharedInstance.stopIndicator()
                
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                if placemarks != nil {
                    
                    let pm = placemarks! as [CLPlacemark]
                    
                    if pm.count > 0 {
                        
                        let pm = placemarks![0]
                        
                        
                        var arrLoc = [String]()
                        arrLoc.append(pm.subLocality ?? "")
                        arrLoc.append(pm.locality ?? "")
                        arrLoc.append(pm.administrativeArea ?? "")
                        arrLoc.append(pm.country ?? "")
                        
                        getFullAddress?(arrLoc.joined(separator: ","))
                        
                    }
                }
        })
    }
}

//MARK: Tableview methods
extension PlacePickerVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSearchPlace.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlacePickerCell") as! PlacePickerCell
        cell.setCellData(model: arrSearchPlace[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        getLocationCoordinate(arrSearchPlace[indexPath.row])
    }
}

//MARK: textfield delegate methods
extension PlacePickerVC : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.tableView.isHidden = true
        
        self.arrSearchPlace.removeAll()
        self.tableView.reloadData()
        
        if textField == txtCurrentLocation && textField.text == "KLblCurrentLocation".localized{
            textField.text = ""
        }else if textField == txtStopLocation && textField.text == "KLblAddStop".localized{
            textField.text = ""
        }else if textField == txtDestination && textField.text == "KLblWhereTo".localized{
            textField.text = ""
        }
        
 
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        
        self.btnDone.isEnabled = false
        self.btnDone.alpha = 0.5
        
        if range.location == 0{
            
            if string == " " {
                return false
            }
        }
        
        let text = textField.text!
        let textRange = Range(range, in: text)
        let updatedText = text.replacingCharacters(in: textRange!, with: string)
        
        if updatedText.count != 0 {
            
            DispatchQueue.main.async {
                self.placeAutocomplete(searchText: updatedText)
            }
            
        }else{
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                self.arrSearchPlace.removeAll()
                self.tableView.reloadData()
                self.tableView.isHidden = true
            })
        }
        
        if textField == txtCurrentLocation{
            txtCurrentLocation.textColor = ColorConstants.TextColorPrimary
        }else if textField == txtStopLocation{
            txtStopLocation.textColor = ColorConstants.TextColorPrimary
        }else if textField == txtDestination{
            txtDestination.textColor = ColorConstants.TextColorPrimary
        }
        
        return true
    }
    
}

//MARK: Google auto places complete api
extension PlacePickerVC{
    
    //Place Auto Complete
    func placeAutocomplete(searchText : String) {
        
        arrSearchPlace.removeAll()
        tableView.reloadData()
        
        placesClient.autocompleteQuery(searchText, bounds: nil, filter: nil, callback: {(results, error) -> Void in
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            if let results = results {
                for result in results {
                    let model = PlacesModel()
                    model.addressFullText = result.attributedFullText.string
                    model.addressPrimaryText = result.attributedPrimaryText.string
                    model.addressSecondaryText = result.attributedSecondaryText?.string
                    model.placeId = result.placeID
                    self.arrSearchPlace.append(model)
                }
                self.tableView.isHidden = false
                self.tableView.reloadData()
            }
        })
    }
    
    //Get Location Coordinate
    func getLocationCoordinate(_ model : PlacesModel){
        
        placesClient.lookUpPlaceID(model.placeId ?? "", callback: { (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            
            guard let _ = place else {
                print("No place details for \(model.placeId ?? "")")
                return
            }
            
            model.latitude  = place!.coordinate.latitude
            model.longitude = place!.coordinate.longitude
            
            if self.txtCurrentLocation.isEditing {
                
                self.txtCurrentLocation.endEditing(true)
                self.startLocation = model
                self.txtCurrentLocation.text = self.startLocation?.addressFullText
                
            } else if self.txtStopLocation.isEditing {
                
                self.txtStopLocation.endEditing(true)
                self.stopLocation = model
                self.txtStopLocation.text = self.stopLocation?.addressFullText
                
            } else {
                
                self.txtDestination.endEditing(true)
                self.endLocation = model
                self.txtDestination.text = self.endLocation?.addressFullText
                            
            }
            
            self.arrSearchPlace.removeAll()
            self.tableView.reloadData()
            self.tableView.isHidden = true
            self.isValid()
        })
    }
}

class PlacesModel: NSObject {
    var addressFullText : String?
    var name : String?
    var addressPrimaryText : String?
    var addressSecondaryText : String?
    var placeId : String?
    var latitude : Double?
    var longitude : Double?
    var textField : String?
    var id : String?
}
