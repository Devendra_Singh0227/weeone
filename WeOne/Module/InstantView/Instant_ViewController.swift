//
//  Instant_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 03/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Instant_ViewController: UIViewController {

    @IBOutlet weak var support_View: ViewDesign!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    var rideType = AppConstants.RideType.Instant
    var checkScreen = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func card_Btn(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            sender.setImage(UIImage(named: "switchBuss"), for: .normal)
        } else {
            sender.tag = 0
            sender.setImage(UIImage(named: "switchcard"), for: .normal)
        }
    }
    
    @IBAction func extraOrderInsurance_Btn(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            sender.setImage(UIImage(named: "switch"), for: .normal)
        } else {
            sender.tag = 0
            sender.setImage(UIImage(named: "switchActive"), for: .normal)
        }
    }
    
    @IBAction func seeinsurancePolicy_Btn(_ sender: UIButton) {
        let vc = Terms_ConditionViewController()
        vc.checkScreen = "BookVC"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func support_Btn(_ sender: UIButton) {
        support_View.isHidden = false
    }
    
    @IBAction func support_View_Support_Btn(_ sender: UIButton) {
        support_View.isHidden = true
    }
    
    @IBAction func save_Btn(_ sender: UIButton) {
        if checkScreen == "payment" {
            navigationController?.popViewController(animated: true)
        } else {
            let vc = BookingconfirmationPopupVC(nibName: "BookingconfirmationPopupVC", bundle: nil)
            let first_String: [NSAttributedString.Key: Any] = [.foregroundColor: ColorScheme.KcommontextColor(), .font: UIFont(name: "Montserrat-Regular", size: 18)!]
            let second_String: [NSAttributedString.Key: Any] = [.foregroundColor: ColorScheme.KcommontextColor(), .font: UIFont(name: "Montserrat-Regular", size: 18)!]
            let firstOne = NSMutableAttributedString(string: "2000 Nok", attributes: first_String)
            let secondOne = NSMutableAttributedString(string: "On Your Card", attributes: second_String)
            firstOne.append(secondOne)
            vc.headerTitle = "Instant Booking Confirmation"
            vc.desc1 = "You now enter into a binding agreement with Weone about vehicle rental."
            vc.desc2 = "We Reserve"
            vc.reserveNok = firstOne//"2000 Nok On Your Card"
            vc.payableamout = "Payable Amount"
            vc.amountNok = "200 Nok"
            vc.instantSetting = "Edit Instant Booking Settings"
            
            vc.confirmClicked = {
                self.navigationController?.popViewController(animated: true)
            }
            
            vc.cancelBtnClicked = {
                self.dismiss(animated: true, completion: nil)
            }
            
            vc.modalPresentationStyle = .custom
            vc.modalTransitionStyle = .crossDissolve
            
            if UIViewController.current()?.presentedViewController != nil {
                UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
                })
            }
            else{
                UIViewController.current()?.present(vc, animated: true, completion: {
                })
            }
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
