//
//  FreeRidesVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 13/06/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit
import Toast

class FreeRidesVC: UIViewController {
    
    var shareLink = String()
    
    @IBOutlet weak var btnShareCode : UIButtonCommon!
    @IBOutlet weak var lblCode : UILabel!
    @IBOutlet weak var viewNavigator: UIViewCommon!
    @IBOutlet weak var image_Height: NSLayoutConstraint!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    
    //MARK: View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
        //checkSreenSize()
        callApiToGetReferralCode()
        
//        btnShareCode.click = {
//            let activityViewController = UIActivityViewController(activityItems: [self.shareLink], applicationActivities: nil)
//
//            if let popoverController = activityViewController.popoverPresentationController {
//                popoverController.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
//                popoverController.sourceView = self.view
//                popoverController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
//            }
//
//            self.present(activityViewController, animated: true, completion: nil)
//
//        }
        
//        viewNavigator.click = {
//            self.navigationController?.popViewController(animated: true)
//        }
    }
    

    func initialConfig(){
        
//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnTapToCopy_Click(_ sender : UIButton){
        UIPasteboard.general.string = lblCode.text
        self.view.makeToast("Code copied")
    }
    
    @IBAction func share_Btn(_ sender: UIButton) {
        let activityViewController = UIActivityViewController(activityItems: [self.shareLink], applicationActivities: nil)
        
        if let popoverController = activityViewController.popoverPresentationController {
            popoverController.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
            popoverController.sourceView = self.view
            popoverController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
        }
        
        self.present(activityViewController, animated: true, completion: nil)
        
    }
}

//MARK: Api call
extension FreeRidesVC{
    
    func callApiToGetReferralCode(){
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.GetReferralCode, method: .get, parameters: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any]{
                
                let strCode: String = dictResponse["senderReferralCode"] as? String ?? ""

                self.lblCode.text = strCode
                self.shareLink = dictResponse["referralLink"] as? String ?? ""

            }
        }) { (failureMessage, failureCode) in
            DispatchQueue.main.async {
                Utilities.showAlertView(message: failureMessage)
            }            
        }
    }
}




