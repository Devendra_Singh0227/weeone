//
//  DamageReportVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 30/09/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class DamageReportVC: UIViewController {

    //MARK: - Variables
    var selectedPartsCode = [String]()
    var arrData = [CellModel]()
    var rideId : String?
    var rideModel:RideListModel = RideListModel()
    var arrDamageReport = [DamageReportModel]()
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var btnUploadPicture: UIButtonCommon!
    @IBOutlet weak var btnReportDamage: UIButtonCommon!
    @IBOutlet weak var svgView: SVGView!
    
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

       initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }

    //MARK: Private Methods
    func initialConfig() {
       
        
        segmentControl.setTitle("KLblPictures".localized, forSegmentAt: 0)
        segmentControl.setTitle("KLblList".localized, forSegmentAt: 1)
        segmentControl.layer.cornerRadius = 5.0
        segmentControl.layer.borderColor = ColorConstants.TextColorTheme.cgColor
        segmentControl.layer.borderWidth = 1.0
        segmentControl.layer.masksToBounds = true
//        segmentControl.setTitleTextAttributes([NSAttributedString.Key.font: FontScheme.kRegularFont(size: 16.0), NSAttributedString.Key.foregroundColor : ColorConstants.TextColorTheme], for: .normal)
//        segmentControl.setTitleTextAttributes([NSAttributedString.Key.font: FontScheme.kRegularFont(size: 16.0), NSAttributedString.Key.foregroundColor : UIColor.white], for: .selected)
        
        tableView.register(UINib(nibName: "DamageReportListCell", bundle: nil), forCellReuseIdentifier: "DamageReportListCell")
        
        preparaDataSource()
        
        btnUploadPic_Click()
        btnReportIssue_Click()
        
        callAPIForDamageReport()
    }
    
    func preparaDataSource() {
        
        changeNodeColor()
        
        if arrDamageReport.count == 0 {
            tableView.loadNoDataFoundView(message: "KMsgNoDamageReportFound".localized) {
                // refresh
            }
        }
        else {
            tableView.removeLoadAPIFailedView()
        }
        
        tableView.reloadData()
    }
    
    //MARK:- IBActions
    private func btnUploadPic_Click(){
        btnUploadPicture.click = {
        }
    }
    
    private func btnReportIssue_Click(){
        btnReportDamage.click = {
            
            let vc = ReportIssueSelectPartVC()
            vc.onClick = { (damageType) in
                let vc = SubmitDamageReportVC()
                vc.rideModel = self.rideModel
                vc.isReloadRequired = {
                    self.callAPIForDamageReport()
                }
                vc.damageType = damageType
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            self.navigationController?.present(vc, animated: false, completion: nil)
        }
    }
    
    @IBAction func btnClose_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func segmentValueChanged(_ sender: UISegmentedControl) {
        if segmentControl.selectedSegmentIndex == 0{
            let x = 0 * self.scrollView.frame.size.width
            scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
        }else{
            let x = 1 * self.scrollView.frame.size.width
            scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
        }
    }
    
    //Change SVG Node Color
    func changeNodeColor() {
        
        if let contents = (svgView.node as? Group)?.contents {
            
            for item in contents {
                
                if let nodeShape = self.svgView.node.nodeBy(tag: item.tag.first ?? "") as? Shape {
                    if (self.arrDamageReport.contains { $0.damageParts == item.tag.first ?? "" }) {
                        nodeShape.fill = Color.rgb(r: 255, g: 161, b: 163)
                    }
                    else {
                        nodeShape.fill = Color.rgb(r: 142, g: 142, b: 142)
                    }
                }
                
                item.nodeBy(tag: item.tag.first ?? "")?.onTouchPressed({ (touch) in
                    
                    let arr = self.arrDamageReport.filter { $0.damageParts == touch.node?.tag.first }
                    if arr.count > 0 {
                        self.openDamageReportPopup(arr: arr)
                    }
                    
//                    if let damageReport = self.arrDamageReport.first(where: { $0.damageParts == touch.node?.tag.first ?? "" }) {
//                        self.openDamageReportPopup(model: damageReport)
//                    }
                })
            }
        }
    }
    
    func openDamageReportPopup(arr:[DamageReportModel]) {
        
        let vc = DamageReportDetailPopupVC()
        vc.arrDamageReport = arr
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        self.navigationController?.present(vc, animated: false, completion: nil)
        
    }
    
//    //Change SVG Node Color
//    func changeNodeColor() {
//
//        if let contents = (svgView.node as? Group)?.contents {
//
//            for item in contents {
//
//                item.nodeBy(tag: item.tag.first ?? "")?.onTouchPressed({ (touch) in
//
//                    let nodeShape = self.svgView.node.nodeBy(tag: touch.node?.tag.first ?? "") as! Shape
//                    if self.selectedPartsCode.contains(touch.node?.tag.first ?? "") {
//                        nodeShape.fill = Color.rgb(r: 142, g: 142, b: 142)
//                        self.selectedPartsCode.removeAll { (str) -> Bool in
//                            return str == touch.node?.tag.first
//                        }
//                    }
//                    else{
//                        nodeShape.fill = Color.rgb(r: 255, g: 161, b: 163)
//                        self.selectedPartsCode.append(touch.node?.tag.first ?? "")
//                    }
//                })
//            }
//        }
//    }
}

//MARK: - UITableView's DataSource & Delegate Methods
extension DamageReportVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDamageReport.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrDamageReport[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "DamageReportListCell") as? DamageReportListCell
        cell?.setData(model: model)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.openDamageReportPopup(arr: [arrDamageReport[indexPath.row]])
    }
    
}

//MARK: Scrollview delegate methods
extension DamageReportVC : UIScrollViewDelegate{
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == self.scrollView{
            if scrollView.contentOffset.x == 0{
                segmentControl.selectedSegmentIndex = 0
            }else{
                segmentControl.selectedSegmentIndex = 1
            }
        }
    }
}

//MARK: - API Calling
extension DamageReportVC {
    
    func callAPIForDamageReport() {
        
        var req = [String:Any]()
        req["vehicleId"] = rideModel.vehicleId?.id ?? ""
        
        if rideId != nil{
            req["rideId"] = rideId ?? ""
        }
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.DamageReportList, method: .post, parameters: req, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            //save Data
            if let list = response as? [[String:Any]] {
                
                self.arrDamageReport = Mapper<DamageReportModel>().mapArray(JSONArray: list)
            }
            self.preparaDataSource()
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(title: AppConstants.AppName, message: failureMessage)
        }
    }
}
