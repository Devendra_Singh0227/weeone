//
//  DamageReportListCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 30/09/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class DamageReportListCell: UITableViewCell {

    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnCheckBox: UIButton!
    @IBOutlet weak var lblPart: UILabel!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:DamageReportModel) {
        
        lblTitle.text = Utilities.returnDamageCaterory(model.damageCategory)
        
        if Utilities.checkStringEmptyOrNil(str: model.desc) == false{
            lblDesc.text = model.desc
            lblDesc.isHidden = false
        }else{
            lblDesc.isHidden = true
        }
        
//        lblPart.text = Utilities.returnPartName(model.damageParts ?? "")
//        lblDesc.setAttributeStringWithLineSpacing(lineSpacing: 1.0, lineHeightMultiple: 1.5, spacing: 0.48, textColor: ColorConstants.TextColor.withAlphaComponent(0.80), font: lblDesc.font)        
    }
}
