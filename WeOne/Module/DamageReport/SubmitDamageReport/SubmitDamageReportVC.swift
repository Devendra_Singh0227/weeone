//
//  SubmitDamageReportVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 18/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class SubmitDamageReportVC: UIViewController {
    
    //MARK: - Variables
    var selectedPartsCode = [String]()
    var rideModel:RideListModel = RideListModel()
    var damageType:Int = 0
    var isReloadRequired:(()->())?
    
    //MARK: - Outlets
    @IBOutlet weak var btnReportDamage: UIButtonCommon!
    @IBOutlet weak var svgView: SVGView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

         initialConfig()
    }


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }
    
    //MARK: Private Methods
    func initialConfig() {
        
        btnReportDamage.click = {
            self.btnDamageReport_Click()
        }
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 4.0
        
        changeNodeColor()
    }
    
    //MARK:- IBActions
    
    func btnDamageReport_Click() {
        
        if selectedPartsCode.count > 0 {
            let vc = ReportAnIssuePopupVC()
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.onClickSkip = {
                self.createReqAndCallAPI()
            }
            vc.onSubmitClicked = { (img, text) in
                
                self.createReqAndCallAPI(img: img, text: text)
                
            }
            self.navigationController?.present(vc, animated: false, completion: nil)
        }
        else {
            self.showToast(text: "KLblSelectDamagePartMsg".localized)

//            Utilities.showAlertView(message: "KLblSelectDamagePartMsg".localized)
        }
        
    }
    
    @IBAction func btnClose_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    //Change SVG Node Color
    func changeNodeColor() {
        
        if let contents = (svgView.node as? Group)?.contents {
            
            for item in contents {
                
                item.nodeBy(tag: item.tag.first ?? "")?.onTouchPressed({ (touch) in
                    
                    
                    let nodeShape = self.svgView.node.nodeBy(tag: touch.node?.tag.first ?? "") as! Shape
                    
                    if self.selectedPartsCode.contains(touch.node?.tag.first ?? "") {
                        
                        nodeShape.fill = Color.rgb(r: 142, g: 142, b: 142)
                        self.selectedPartsCode.removeAll { (str) -> Bool in
                            return str == touch.node?.tag.first
                        }
                    }
                    else{
                        nodeShape.fill = Color.rgb(r: 255, g: 161, b: 163)
                        
                        for obj in self.selectedPartsCode {
                            if let nodeShape = self.svgView.node.nodeBy(tag: obj) as? Shape {
                                nodeShape.fill = Color.rgb(r: 142, g: 142, b: 142)
                            }
                        }
                        self.selectedPartsCode.removeAll()
                        self.selectedPartsCode.append(touch.node?.tag.first ?? "")
                    }
                })
            }
        }
    }
}

extension SubmitDamageReportVC : UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {        
        return svgView
    }
}

//MARK: - API Calling
extension SubmitDamageReportVC {
    
    func createReqAndCallAPI(img:UIImage? = nil, text:String = "") {
        
        var req = [String:Any]()
        req["vehicleId"] = rideModel.vehicleId?.id ?? ""
        req["partnerId"] = rideModel.vehicleId?.partnerId ?? ""
        req["damageCategory"] = damageType
        req["description"] = text
        if self.selectedPartsCode.count > 0 {
            req["damageParts"] = selectedPartsCode.first
        }
        
        
        if let image = img {
            UploadManager.sharedInstance.uploadFile(AppConstants.serverURL, command: AppConstants.URL.UploadFile, image: image, folderKeyvalue: nil, uploadParamKey: "file", params: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, isPregress:true, success: { (response, message) in
                
                if let dictResponse = response as? [String:Any] {
                    if let dict = dictResponse["data"] as? [String:Any]{
                        
                        if let arrFiles = dict["files"] as? [[String:Any]] {
                            if let path = arrFiles.first?["absolutePath"] as? String {
                                req["images"] = [path]
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                                     self.callAPIForSubmitDamage(req: req)
                                })
                            }
                        }
                    }
                }
            }) { (failureMessage) in
                Utilities.showAlertView(message: failureMessage)
            }
        }
        else {
            callAPIForSubmitDamage(req: req)
        }
    }
    
    func callAPIForSubmitDamage(req:[String:Any]) {
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.AddDamageReoport, method: .post, parameters: req, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            Utilities.showAlertWithButtonAction(title: "", message: message, buttonTitle: StringConstants.ButtonTitles.KOk, onOKClick: {
                // code here
                self.dismiss(animated: true, completion: {
                    self.isReloadRequired?()
                    self.navigationController?.popViewController(animated: true)
                })
            })
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
