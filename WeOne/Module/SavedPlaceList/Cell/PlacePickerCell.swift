//
//  PlacePickerCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 17/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class PlacePickerCell: UITableViewCell {

    @IBOutlet weak var imgPin: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnRemove: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(model : PlacesModel) {
        
        imgPin.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Pin"))
        lblTitle.text = model.addressPrimaryText
        lblAddress.text = model.addressFullText
    }
}
