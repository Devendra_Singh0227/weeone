//
//  SavedPlaceListVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 09/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class SavedPlaceListVC: UIViewController {

    //MARK: Variables
    var arrPlaces = [FavouritePlaceModel]()
    var getLocation : ((_ model : PlacesModel) -> ())?
    var startLocation: PlacesModel?
    
    //MARK: outlets
    @IBOutlet weak var name_TxtField: UITextField!
    @IBOutlet weak var address_TxtField: UITextField!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
        tableView.register(UINib(nibName: "PlacePickerCell", bundle: nil), forCellReuseIdentifier: "PlacePickerCell")
        prepareDataSource()
    }
    
    func initialConfig(){
        
//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
    }
    
    //MARK: prepareDataSource
    func prepareDataSource(){
        arrPlaces = SyncManager.sharedInstance.fetchAllFavouritePlaces()
        
        if arrPlaces.count == 0 {
//            self.navigationController?.popViewController(animated: true)
        }
        tableView.reloadData()
    }
    
    //MARK: Button action methods
    @IBAction func btnBack_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func save_Btn(_ sender: ButtonDesign) {
        startLocation?.name = name_TxtField.text
        callApiForAddFavPlace(action: AppConstants.Action.FAVOURITE)
    }
}

//MARK: Tableview methods
extension SavedPlaceListVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPlaces.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlacePickerCell") as! PlacePickerCell
        let model = arrPlaces[indexPath.row]
//        cell.setCellData(model: arrPlaces[indexPath.row])
        cell.btnRemove.tag = indexPath.row
        cell.lblTitle.text = model.name ?? ""
        cell.lblAddress.text = model.geoLocation?.name ?? ""
        cell.btnRemove.addTarget(self, action: #selector(self.btnRemoveFav_Click(_:)), for: .touchUpInside)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let place = arrPlaces[indexPath.row]
//        let model = PlacesModel()
//
//        model.name = place.name
//        model.addressFullText = place.geoLocation?.name
//
//        model.latitude = place.geoLocation?.latitude
//        model.longitude = place.geoLocation?.longitude
//        model.id = place.id
//
//        getLocation?(model)
//        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    @objc func btnRemoveFav_Click(_ sender: UIButton) {
        
        let model = self.arrPlaces[sender.tag]
        let vc = ConfirmPopupVC()
         vc.headerTitle = "Remove Saved Place"
        vc.message = "Do you want to remove"
        vc.name = "\(model.name ?? "")?"
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        vc.confirmClicked = {
            self.callApiForRemoveFavPlace(model)
        }
            UIViewController.current()?.present(vc, animated: true, completion: {
        })
    }
}

//MARK: API Calling
extension SavedPlaceListVC {
    func callApiForRemoveFavPlace( _ model : FavouritePlaceModel){
        
        var param : [String:Any] = [:]
        param["action"] = AppConstants.Action.REMOVE_FROM_FAVOURITE
        param["name"] = model.name
        param["id"] = model.id
        
        var location : [String:Any] = [:]
        location["type"] = "point"
        location["name"] = model.geoLocation?.name ?? ""
        location["coordinates"] = [model.geoLocation?.longitude ?? 0, model.geoLocation?.latitude ?? 0]
        
        param["geoLocation"] = location
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.AddRemoveFavPlace, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let _ = response as? [String:Any]{
                
                SyncManager.sharedInstance.removeFavourite(model.id ?? "")
                self.prepareDataSource()
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
//    func callApiForAddFavPlace(action: Int, _ model : PlacesModel, index: Int = 0){
    func callApiForAddFavPlace(action: Int, index: Int = 0){
        var param : [String:Any] = [:]
        param["action"] = action
        param["name"] = address_TxtField.text!
//        if action == AppConstants.Action.REMOVE_FROM_FAVOURITE {
//            param["id"] = model.id
//        }
        
        var location : [String:Any] = [:]
        location["type"] = "point"
        location["name"] = name_TxtField.text!
        location["coordinates"] = [ 0, 0]
        
        param["geoLocation"] = location
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.AddRemoveFavPlace, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any]{
                
                if let arrFavouritePlace = dictResponse["favouritePlaces"] as? [[String:Any]], arrFavouritePlace.count > 0 {
                    SyncManager.sharedInstance.insertFavouritePlaces(list: arrFavouritePlace)
                }
                
                if action == AppConstants.Action.REMOVE_FROM_FAVOURITE {
//                    SyncManager.sharedInstance.removeFavourite(model.id ?? "")
//                    self.prepareDataSource()
                } else {
                    self.prepareDataSource()
                    self.name_TxtField.text = ""
                    self.address_TxtField.text = ""
//                    self.getLocation?(model)
//                    self.navigationController?.popViewController(animated: true)
                }
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
