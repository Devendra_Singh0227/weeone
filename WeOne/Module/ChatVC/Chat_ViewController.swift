//
//  Chat_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 25/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Chat_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
        tableView.register(UINib(nibName: "Sender_TableViewCell", bundle: nil), forCellReuseIdentifier: "Sender_TableViewCell")
        tableView.register(UINib(nibName: "Reciever_TableViewCell", bundle: nil), forCellReuseIdentifier: "Reciever_TableViewCell")
        
    }
    
    func initialConfig() {
//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section % 2 == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Reciever_TableViewCell", for: indexPath) as? Reciever_TableViewCell
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Sender_TableViewCell", for: indexPath) as? Sender_TableViewCell
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return self.showHeaderViewController(section: section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func showHeaderViewController(section: Int) -> UIView {
        
        // Header View
        var sizeOfString = CGSize()
        var dateString = String()
        var finalDate = "20 Jul 2020"
        let headerView : UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 40))
        headerView.backgroundColor = UIColor.clear
        headerView.tag = section
//        if jsondata.count == 0 {
//            Apicall()
//        } else {
//
//            dateString = (self.jsondata.object(at: section) as? NSDictionary)?.value(forKey: "Date") as! String
//            //            print("datedse",dateString)
//            finalDate = dateString.dateformatter()
//            if finalDate != "TODAY" && finalDate != "YESTERDAY" {
//                finalDate =  dateFromStringForSection(datestring: dateString) as String
//            }
//            if let font = UIFont(name: "Helvetica", size: 14.0)
//            {
//                let fontAttributes = [NSAttributedString.Key.font: font] // it says name, but a UIFont works
//                sizeOfString = (finalDate as NSString).size(withAttributes: fontAttributes)
//            }
//        }
        // Set Date on label
        
        // Get the width of String
        
        // Header View Label.
        let headerName = UILabel()
        headerName.frame = CGRect(x: 0, y: 0, width: 90 , height: 40)
        //        headerName.center = CGPoint(x: UIScreen.main.bounds.width / 2, y: CGRectGetHeight(headerView.frame)/2)
        headerName.center = CGPoint(x: UIScreen.main.bounds.width / 2, y: (headerView.frame.height)/2)
        headerName.backgroundColor = UIColor(red: 207/255, green: 218/255, blue: 228/255, alpha: 1.0)
        headerName.textAlignment = .center
        headerName.lineBreakMode = NSLineBreakMode.byWordWrapping
        headerName.layer.cornerRadius = 7
        headerName.clipsToBounds = true
        headerName.font =  UIFont(name: "Helvetica", size: 12.0)
        headerName.textColor = UIColor.darkGray
        headerName.adjustsFontSizeToFitWidth = true
        headerName.numberOfLines = 0
        headerName.text = finalDate
        headerView.addSubview(headerName)
        
        return headerView
    }
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return UITableView.automaticDimension
    //    }
    //
    //    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    //        let tableHeaderView =  UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 10, height:30))
    //        //tableHeaderView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
    //        tableHeaderView.backgroundColor = UIColor(red: 207/255, green: 218/255, blue: 228/255, alpha: 1.0)
    ////        let arrayDates = chatArray[section]
    ////        var finalDate = String()
    ////        var finalDate1 = String()
    ////        let lastSection = chatArray.count - 1
    ////        print(lastSection)
    ////        if section == lastSection {
    ////            // Set Date on label
    //////            let firstDate = arrayDates.created ?? ""
    //////            finalDate1 = getCurrentDateInFullFormatDate(date: firstDate)
    ////            //            finalDate1 = getDateInhhmmee(date: secondDateConverted)
    ////            // Header View Label.
    //            let headerName = UILabel(frame: CGRect(x: tableHeaderView.center.x - 80, y: tableHeaderView.center.y , width: 200, height: 20))
    //            headerName.backgroundColor = UIColor.clear
    //            headerName.textAlignment = .center
    //            headerName.lineBreakMode = NSLineBreakMode.byWordWrapping
    //            headerName.layer.cornerRadius = 15
    //            headerName.clipsToBounds = true
    //            headerName.font =  UIFont(name: "HelveticaNeue", size: 12.0)
    //            headerName.textColor = UIColor.darkGray
    //            headerName.adjustsFontSizeToFitWidth = true
    //            headerName.numberOfLines = 0
    //            headerName.text = "20 Jul 2020"
    //            tableHeaderView.addSubview(headerName)
    //            return tableHeaderView
    //            //   print(firstDate)
    ////        }else {
    ////            // Set Date on label
    ////            let firstDate = arrayDates.created ?? ""
    ////            finalDate1 = getCurrentDateInFullFormatDate(date: firstDate)
    ////            //            finalDate1 = getDateInhhmmee(date: secondDateConverted)
    ////
    ////            // print(finalDate1)
    ////            let dict1 = chatArray[section + 1]
    ////            let secondDate = dict1.created ?? ""
    ////            finalDate = getCurrentDateInFullFormatDate(date: secondDate)
    ////            //            finalDate = getDateInhhmmee(date: firstDateConverted)
    ////            // print(finalDate)
    ////
    ////            if finalDate1 == finalDate{
    ////                //   print("abc")
    ////            }else{
    ////                // Header View Label.
    ////                let headerName = UILabel(frame: CGRect(x: tableHeaderView.center.x - 80, y: tableHeaderView.center.y , width: 200, height: 20))
    ////                headerName.backgroundColor = UIColor.clear
    ////                headerName.textAlignment = .center
    ////                headerName.lineBreakMode = NSLineBreakMode.byWordWrapping
    ////                headerName.layer.cornerRadius = 15
    ////                headerName.clipsToBounds = true
    ////                headerName.font =  UIFont(name: "HelveticaNeue", size: 12.0)
    ////                headerName.textColor = UIColor.darkGray
    ////                headerName.adjustsFontSizeToFitWidth = true
    ////                headerName.numberOfLines = 0
    ////                headerName.text = "\(finalDate1)"
    ////                tableHeaderView.addSubview(headerName)
    ////            }
    ////
    ////            return tableHeaderView
    ////        }
    //    }
    //
    //    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    ////        let arrayDates = chatArray[section]
    ////        var finalDate = String()
    ////        var finalDate1 = String()
    ////        let lastSection = chatArray.count - 1
    ////        print(lastSection)
    ////        if section == lastSection {
    ////            return 40
    ////        }else{
    ////            // Set Date on label
    ////            if let firstDate = arrayDates.created{
    ////                finalDate = getCurrentDateInFullFormatDate(date: firstDate)
    ////                //                finalDate = getDateInhhmmee(date: firstDateConverted)
    ////                print(finalDate)
    ////                let dateArray2 = chatArray[section + 1]
    ////                if let secondDate = dateArray2.created{
    ////                    finalDate1 = getCurrentDateInFullFormatDate(date: secondDate)
    ////                    //                    finalDate1 = getDateInhhmmee(date: secondDateConverted)
    ////                }
    ////            }
    ////            // print(finalDate)
    ////            if finalDate1 == finalDate{
    ////                return 0
    ////            }else{
    //                return 40
    ////            }
    ////        }
    //    }
    //
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        return .leastNormalMagnitude
    //    }
}
