//
//  TextFieldCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 23/01/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class TextFieldCell: UITableViewCell {
    
    var model : CellModel = CellModel()
    var maxLimit : Double = 0.0
    
    @IBOutlet weak var viewKM: UIView!
    
    @IBOutlet weak var txtField: UITextField!
    
    @IBOutlet weak var lblKM: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        txtField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(_ model : CellModel){
        self.model = model
        txtField.placeholder = model.placeholder
        txtField.PlaceHolderColorKey = NSString(string : "kTextColorPrimary")
        txtField.keyboardType = model.keyboardType ?? .default
        
        if model.cellType == .UnlockCarBatteryCell{
            maxLimit = 100.0
        }else if model.cellType == .UnlockCarKmCell{
            viewKM.isHidden = false
            lblKM.text = "\("KlblStartKm".localized) \(model.startKm)"
            maxLimit = 1000000
        }
    }
}

extension TextFieldCell : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.model.userText = textField.text
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let result = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        
        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        
        if range.location == 0{
            
            if string == " " {
                return false
            }
        }
        
        if textField.keyboardType == .decimalPad{
            guard let oldText = textField.text, let r = Range(range, in: oldText) else {
                    return true
                }
                
            if (result.toDouble() ?? 0) <= (Double(maxLimit) ){
                    let newText = oldText.replacingCharacters(in: r, with: string)
                    let isNumeric = newText.isEmpty || (Double(newText) != nil)
                    let numberOfDots = newText.components(separatedBy: ".").count - 1
                    
                    let numberOfDecimalDigits: Int
                    if let dotIndex = newText.firstIndex(of: ".") {
                        numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
                        
                    } else {
                        numberOfDecimalDigits = 0
                    }
                    return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2
                }else{
                    return false
                }
        }
        
        return true
    }
}
