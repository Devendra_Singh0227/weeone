//
//  TextFieldCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 23/01/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class UnlockCarCell: UITableViewCell {
    
    var model : CellModel = CellModel()
    var maxLimit : Double = 0.0
    var charLimit = 250
    
    @IBOutlet weak var txtKMField: UITextFiledCommon!
    @IBOutlet weak var txtFuelField: UITextFiledCommon!
    @IBOutlet weak var lblCharCount: UILabel!
    @IBOutlet weak var txtAddComment: UITextFiledCommon!
    @IBOutlet weak var viewKmFuel: UIStackView!
    @IBOutlet weak var viewKmFuelHeight: NSLayoutConstraint!
    @IBOutlet weak var stackViewBottom: NSLayoutConstraint!
    @IBOutlet weak var viewCommentHeight: NSLayoutConstraint!

    @IBOutlet weak var viewTop: NSLayoutConstraint!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        txtKMField.textField.delegate = self
        txtFuelField.textField.delegate = self
        txtAddComment.textField.delegate = self
        
        txtAddComment.heightViewLine.constant = 0
        
        lblCharCount.text = "0/250"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(_ model : CellModel){
        self.model = model
      
        if model.cellType == .ReportDamageTextViewCell {
            viewKmFuel.isHidden = true
            viewKmFuelHeight.constant = 0
            viewCommentHeight.constant = 40
            viewTop.constant = 13
            stackViewBottom.constant = 30
            txtAddComment.viewTop.constant = 0

        } else {
            txtKMField.textField.keyboardType = model.keyboardType ?? .default
            txtFuelField.textField.keyboardType = model.keyboardType ?? .default

        }
    }
}

extension UnlockCarCell : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtKMField.textField {
            self.model.userText = textField.text
        } else if textField == txtFuelField.textField{
            self.model.userText1 = textField.text
        }else{
            self.model.userText2 = textField.text
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let result = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        
        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        
        if range.location == 0{
            
            if string == " " {
                return false
            }
        }
        
        if textField == txtAddComment.textField{
            
            guard let stringRange = Range(range, in: textField.text ?? "") else { return false }

            let updatedText = (textField.text ?? "").replacingCharacters(in: stringRange, with: string)
            
            lblCharCount.text = "\(updatedText.count)/250"
            return updatedText.count < charLimit
        }
        
        if textField.keyboardType == .decimalPad{
            guard let oldText = textField.text, let r = Range(range, in: oldText) else {
                    return true
                }
                
            if txtKMField.textField.isEditing {
                 maxLimit = 1000000
            } else if txtFuelField.textField.isEditing{
                 maxLimit = 100.0
            }
            
            if (result.toDouble() ?? 0) <= (Double(maxLimit) ){
                    let newText = oldText.replacingCharacters(in: r, with: string)
                    let isNumeric = newText.isEmpty || (Double(newText) != nil)
                    let numberOfDots = newText.components(separatedBy: ".").count - 1
                    
                    let numberOfDecimalDigits: Int
                    if let dotIndex = newText.firstIndex(of: ".") {
                        numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
                        
                    } else {
                        numberOfDecimalDigits = 0
                    }
                    return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2
                }else{
                    return false
                }
        }
        
        return true
    }
}
