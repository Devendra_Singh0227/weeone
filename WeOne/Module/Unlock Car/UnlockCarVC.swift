//
//  UnlockCarVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 23/01/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class UnlockCarVC: UIViewController {
    
    //MARK: Variables
    var isApiCalled : Bool = false
    var arrCell : [CellModel] = [CellModel]()
    var attachmentReason : AttachmentReson = .StartRide
    var rideModel : RideListModel = RideListModel()
    var isRideStarted : (() -> ())?
    var dictEndRideRequest : [String:Any] = [String:Any]()
    
    var arrUploadOdometer = [Any]()
    var arrUploadImage = [Any]()
    var uploadingPopup : UIViewController?
    
    
    //MARK: outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnSubmit: UIButtonCommon!
    @IBOutlet weak var viewNavigation: UIViewCommon!
    
    //MARK: View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
    }
    
    //MARK: Initial config
    func initialConfig(){
        tableView.register(UINib(nibName: "AddPictureCell", bundle: nil), forCellReuseIdentifier: "AddPictureCell")
        tableView.register(UINib(nibName: "UnlockCarCell", bundle: nil), forCellReuseIdentifier: "UnlockCarCell")
        
        (attachmentReason == .StartRide) ? (viewNavigation.text = "KLblEndRentalReport".localized as NSString) : (viewNavigation.text = "KLblEndRentalReport".localized as NSString)
        
        btnSubmit_Click()
        prepareDataSource()
        
        setData()
        
        viewNavigation.button.setImage(#imageLiteral(resourceName: "close"), for: .normal)
        
        viewNavigation.click = {
            self.btnClose_Click(UIButton())
        }
                
    }
    
    func setData() {
        
        let imgModel = CellModel()
        imgModel.cellObj = UIImage.init(named: "ODOMeter")
        arrUploadOdometer.append(imgModel)
        
        let arrImage = ["Car-Back", "Car-Front", "Car-Left", "Car-Right"]
        
        for imageName in arrImage {
            let imgModel = CellModel()
            imgModel.cellObj = UIImage.init(named: imageName)
            arrUploadImage.append(imgModel)
        }
        
    }
    
    //MARK: prepare datasource
    func prepareDataSource(){
        
        arrCell.removeAll()
        
        arrCell.append(CellModel.getModel(placeholder : "KlblAddCarPicture".localized, type: .UnlockCarPictureCell, isSelected: (rideModel.vehicleId?.iotVehicleId == nil) ? true : false))
        
        arrCell.append(CellModel.getModel(placeholder : "KLblOdoMeter".localized, type: .UnlockCarOdometerCell))
        
//        if rideModel.vehicleId?.iotProvider == AppConstants.IOTProvider.WithoutTelemetric{
            
            if (attachmentReason == .EndRide){
                arrCell.append(CellModel.getModel(placeholder : "KlblEnterKm".localized, type: .UnlockCarKmCell, keyBoardType : .decimalPad, startKm: rideModel.startKm))
            }
//        }
        tableView.reloadData()
    }
    
    //MARK: Button click methods
    @IBAction func btnClose_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnRentalReport(_ sender: UIButton) {
        let vc = DriveNowVC()
        vc.isRentalExpand = true
        if let vehicleModel = rideModel.vehicleId {
            vc.vehicleModel = vehicleModel
        }
        self.navigationController?.pushViewController(vc, animated: true)

    }
   
    @IBAction func btnDamageReport(_ sender: UIButton) {
        let vc = DriveNowVC()
        vc.isDamageReportExpand = true
        if let vehicleModel = rideModel.vehicleId {
            vc.vehicleModel = vehicleModel
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSubmit_Click(){
        btnSubmit.click = {
            self.checkValidation()
        }
    }
    
    //MARK: Check validation
    func checkValidation(){
        
        self.view.endEditing(true)
        
        var req : [String:Any] = [:]
        
        if (attachmentReason == .EndRide){
            
            if true{
                let filter = arrCell.filter {$0.cellType == .UnlockCarPictureCell}
                
                var arrImages = [String]()
                
                for item in filter.first!.dataArr{
                    
                    let userText = (item as! CellModel).userText
                    if !Utilities.checkStringEmptyOrNil(str: userText){
                        arrImages.append(userText ?? "")
                    }
                }
                
                
                let arrimageType = filter.first!.dataArr.map { (obj) -> Int in
                    return (obj as! CellModel).imageType
                }

                for item in filter.first!.dataArr{
                    let cellModel = (item as! CellModel)
                    if cellModel.isUploading {
                        uploadingPopup = self.view.returnConfirmationPopup(title: "", message: "KLblAttachmentsUploading".localized, confirmButtonTitle: StringConstants.ButtonTitles.KOk, onConfirmClick: {
                            
                        })
                        
                        uploadingPopup?.modalPresentationStyle = .custom
                        uploadingPopup?.modalTransitionStyle = .crossDissolve
                        self.navigationController?.present(uploadingPopup!, animated: true, completion: nil)
                        return
                    }
                }
                
                if arrImages.count != AppConstants.ImageUploadMaxLimit.EndRide{
                   //KD Utilities.showAlertView(message: "KLblPleaseUploadAllImageMsg".localized)
                    
                    self.showToast(text: "KlblSubmitCompleteData".localized)

                    return
                }
                
                
                var dictAttachment : [[String:Any]] = [[:]]
                dictAttachment.removeAll()

                for cellModel  in filter.first!.dataArr {
                    if let model = cellModel as? CellModel {
                        let dict = ["path": model.userText ?? "", "type": model.imageType] as [String : Any]
                        dictAttachment.append(dict)
                    }
                }
                

                
                req["attachments"] = dictAttachment
            }
        }else{
            
            let filter = arrCell.filter {$0.cellType == .UnlockCarPictureCell}
            
            if filter.first!.dataArr.count > 0{
                
                var dictAttachment : [[String:Any]] = [[:]]
                dictAttachment.removeAll()

                for cellModel  in filter.first!.dataArr {
                    if let model = cellModel as? CellModel {
                        let dict = ["path": model.userText ?? "", "type": model.imageType] as [String : Any]
                        dictAttachment.append(dict)
                    }
                }

                req["attachments"] = dictAttachment

            }
        }
        
        
        if true{
            let filter = arrCell.filter {$0.cellType == .UnlockCarOdometerCell}
            
            if filter.first!.dataArr.count > 0{
                
                var arrImages = [String]()
                
                for item in filter.first!.dataArr{
                    
                    let userText = (item as! CellModel).userText
                    if !Utilities.checkStringEmptyOrNil(str: userText){
                        arrImages.append(userText ?? "")
                    }
                }
                
                for item in filter.first!.dataArr{
                    let cellModel = (item as! CellModel)
                    if cellModel.isUploading {
                        uploadingPopup = self.view.returnConfirmationPopup(title: "", message: "KLblAttachmentsUploading".localized, confirmButtonTitle: StringConstants.ButtonTitles.KOk, onConfirmClick: {
                            
                        })
                        
                        uploadingPopup?.modalPresentationStyle = .custom
                        uploadingPopup?.modalTransitionStyle = .crossDissolve
                        self.navigationController?.present(uploadingPopup!, animated: true, completion: nil)
                        return
                    }
                }
                
                if arrImages.count != AppConstants.ImageUploadMaxLimit.OdometerImage{
                    //KD Utilities.showAlertView(message: "KLblPleaseOdometerImageMsg".localized)
                    self.showToast(text: "KLblPleaseOdometerImageMsg".localized)


                    return
                }
                
                req["odometerImg"] = arrImages
            }
        }
        
        if rideModel.vehicleId?.iotProvider == AppConstants.IOTProvider.ShareBoxKeySharing{
            
            if (attachmentReason == .EndRide){
                let filter = arrCell.filter {$0.cellType == .UnlockCarKmCell}
                
                if filter.count > 0{
                    if Utilities.checkStringEmptyOrNil(str: filter.first!.userText){
                        //Utilities.showAlertView(message: "KLblPleaseEnterKm".localized)
                        
                        self.showToast(text: "KLblPleaseEnterKm".localized)

                        return
                    }
                    
                    
                    req["endKm"] = Utilities.getDoubleValue(value: filter.first!.userText ?? "0")
                    
                    if Utilities.checkStringEmptyOrNil(str: filter.first!.userText1){
                       self.showToast(text: "KlblSubmitCompleteData".localized)

                        // Utilities.showAlertView(message: "KLblPleaseEnterBatteryLevel".localized)
                        return
                    }
                    
                    
                    req["endFuelLevel"] = Utilities.getDoubleValue(value:filter.first!.userText1  ?? "0")
                    
                    if !Utilities.checkStringEmptyOrNil(str: filter.first!.userText2){
                        req["comment"] = filter.first!.userText2
                    }
                }
            }
        }else{
            
            //Comments if added for telemetric car
            if (attachmentReason == .EndRide){
                let filter = arrCell.filter {$0.cellType == .UnlockCarKmCell}
                
                if filter.count > 0{
                    
                    if !Utilities.checkStringEmptyOrNil(str: filter.first!.userText2){
                        req["comment"] = filter.first!.userText2
                    }
                }
            }
        }
        
        req["rideId"] = rideModel.id ?? ""
        
        if (attachmentReason == .EndRide){
            
            
            LocationManager.SharedManager.getLocation()
            LocationManager.SharedManager.getCurrentLocation = { location in
                
                if self.isApiCalled == false{
                    self.dictEndRideRequest = req
                    
                    var stopLocation : [String:Any] = [:]
                    stopLocation["type"] = "point"
                    stopLocation["name"] = "1"
                    stopLocation["coordinates"] = [location.coordinate.longitude, location.coordinate.latitude]

                    self.dictEndRideRequest["stopLocation"] = stopLocation
                    
                    self.callAPIForEndRide(req: self.dictEndRideRequest)
                }
            }
            
        }else{
            callAPIForStartRide(req: req)
        }
    }
}

//MARK: Tableview methods
extension UnlockCarVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCell.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrCell[indexPath.row]
        
        switch model.cellType!{
            
        case .UnlockCarPictureCell, .UnlockCarOdometerCell:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddPictureCell", for: indexPath) as! AddPictureCell
            
            model.classType = .UnlockCarVC
            
            if model.cellType == .UnlockCarOdometerCell{
                cell.setCellData(model, maxCount: AppConstants.ImageUploadMaxLimit.OdometerImage)
            }else{
                cell.setCellData(model, maxCount: AppConstants.ImageUploadMaxLimit.EndRide)
            }
            
            cell.reloadTable = { arr in
                model.dataArr = arr
                self.checkAllImagesAreUploadedOrNot()
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            }
            return cell
            
        case .UnlockCarKmCell, .UnlockCarBatteryCell:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "UnlockCarCell", for: indexPath) as! UnlockCarCell
            
            if rideModel.vehicleId?.iotProvider == AppConstants.IOTProvider.WithTelemetric{
                cell.viewKmFuel.isHidden = true
            }
            cell.setCellData(model)
            return cell
            
        default : return UITableViewCell()
        }
        
    }
}

//MARK: Check all images are uploaded or not
extension UnlockCarVC{
    
    func checkAllImagesAreUploadedOrNot(){
        
        if uploadingPopup != nil{
            
            var arrImages = [Bool]()
            var arrFailed = [Bool]()
            
            if true{
                
                let filter = arrCell.filter {$0.cellType == .UnlockCarPictureCell}
                
                for item in filter.first!.dataArr{
                    let cellModel = (item as! CellModel)
                    if cellModel.isUploaded{
                        arrImages.append(cellModel.isUploaded)
                    }
                    
                    if cellModel.isFailed{
                        arrFailed.append(cellModel.isFailed)
                    }
                }
            }
            
            if true{
                
                let filter = arrCell.filter {$0.cellType == .UnlockCarOdometerCell}
                
                for item in filter.first!.dataArr{
                    let cellModel = (item as! CellModel)
                    if cellModel.isUploaded{
                        arrImages.append(cellModel.isUploaded)
                    }
                    
                    if cellModel.isFailed{
                        arrFailed.append(cellModel.isFailed)
                    }
                }
            }
            
            if arrImages.count == (AppConstants.ImageUploadMaxLimit.OdometerImage + AppConstants.ImageUploadMaxLimit.EndRide){
                self.dismiss(animated: true, completion: nil)
            }
            
            if arrFailed.count > 0{
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}

//MARK: Api calls
extension UnlockCarVC {
    
    func callAPIForStartRide(req:[String:Any]) {
        
        SyncManager.sharedInstance.startRide(reqDict: req, success: { (response) in
            // success
            self.isRideStarted?()
            
            let vc = HomeVC()
            self.navigationController?.popAllAndSwitch(to: vc)
            
        }) {
            
        }
        
    }
    
    func callAPIForEndRide(req:[String:Any]) {
        
        isApiCalled = true
        SyncManager.sharedInstance.stopRide(reqDict: req, { (response) in
            // success code
            let model = Mapper<RideListModel>().map(JSON: response)!
            SyncManager.sharedInstance.activeRideObject = model
            
            let vc = FareSummaryVC(nibName: "FareSummaryVC", bundle: nil)
            vc.rideModel = model
            self.navigationController?.popAllAndSwitch(to: vc)
        }) {
            // failure copde
            self.isApiCalled = false
        }
        
    }
}
