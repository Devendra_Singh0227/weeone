//
//  StaticPageVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 24/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit
import WebKit

class StaticPageVC: UIViewController {
    
    //MARK: - Variables
    var strTitle:String = ""
    var strUrl:String = ""
    
    //MARK: - Outlets
    @IBOutlet weak var viewNavigation: UIViewCommon!

    @IBOutlet weak var webView: WKWebView!
    
    //MARK:- Controller's Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewNavigation.click = {
            self.btnClose_Clicked()
        }
        
        viewNavigation.text = strTitle.replacingOccurrences(of: "_", with: " ").lowercased().capitalizingFirstLetter() as NSString
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupWebViewUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }
    
    //MARK:- Private Method
    func setupWebViewUI() {
        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
        
        
        let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let wkUController = WKUserContentController()
        wkUController.addUserScript(userScript)
        let wkWebConfig = WKWebViewConfiguration()
        wkWebConfig.userContentController = wkUController
        
        webView.scrollView.showsVerticalScrollIndicator = false
        self.view.layoutIfNeeded()
        
        getStaticData()

    }
    
    //MARK:- IBActions
    func btnClose_Clicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension StaticPageVC {
    func getStaticData() {
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.StaticPage + strTitle, method: .get, parameters: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any], !dictResponse.isEmpty {
                if let description = dictResponse["description"] as? String {
                    self.webView.contentMode = .scaleToFill
                    
                    let headerString = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header>"
                    self.webView.loadHTMLString(headerString + description, baseURL: nil)
                }
            } else {
                Utilities.showAlertView(message: message)
            }
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }

}
extension StaticPageVC: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        NetworkClient.sharedInstance.stopIndicator()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        NetworkClient.sharedInstance.stopIndicator()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!){
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
    }
}

