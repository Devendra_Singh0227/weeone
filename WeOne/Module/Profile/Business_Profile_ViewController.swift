//
//  Business_Profile_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 19/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Business_Profile_ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var table_View: UITableView!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    var arrCard = [CardModel]()
    var weeklyOn = false
    var montlyOn = false
    @IBOutlet var tableHeaderView: UIView!
    @IBOutlet weak var first_Name_TxtField: UITextField!
    @IBOutlet weak var last_Name_TxtField: UITextField!
    @IBOutlet weak var email_txtField: UITextField!
    @IBOutlet weak var expense_Report_EmailTxtFld: UITextField!
    @IBOutlet weak var firstView: ViewDesign!
    @IBOutlet weak var secondView: ViewDesign!
    @IBOutlet weak var thirdView: ViewDesign!
    @IBOutlet weak var expenseView: ViewDesign!
    @IBOutlet weak var weekly_Switch: UIButton!
    @IBOutlet weak var monthly_Switch: UIButton!
    var cardArray = [CardsListModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialConfig()
        loadNib()
    }
    
    func initialConfig(){
        
        first_Name_TxtField.delegate = self
        last_Name_TxtField.delegate = self
        email_txtField.delegate = self
        expense_Report_EmailTxtFld.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callgetCardApi()
    }
    
    func loadNib() {
        table_View.register(UINib(nibName: "PaymentCardCell", bundle: nil), forCellReuseIdentifier: "PaymentCardCell")
        table_View.register(UINib(nibName: "Add_Card_TableViewCell", bundle: nil), forCellReuseIdentifier: "Add_Card_TableViewCell")
        table_View.tableHeaderView = tableHeaderView
        table_View.tableHeaderView?.frame.size = CGSize(width: table_View.frame.width, height: CGFloat(570))
    }
   
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == first_Name_TxtField {
            firstView.borderwidth = 2
            firstView.backgroundColor = UIColor.white
            firstView.borderColor = ColorConstants.TextColorPrimary
        } else if textField == last_Name_TxtField {
            secondView.borderwidth = 2
            secondView.backgroundColor = UIColor.white
            secondView.borderColor = ColorConstants.TextColorPrimary
        } else if textField == email_txtField {
            thirdView.borderwidth = 2
            thirdView.backgroundColor = UIColor.white
            thirdView.borderColor = ColorConstants.TextColorPrimary
        } else {
            expenseView.borderwidth = 2
            expenseView.backgroundColor = UIColor.white
            expenseView.borderColor = ColorConstants.TextColorPrimary
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == first_Name_TxtField {
            firstView.borderwidth = 0
            firstView.backgroundColor = ColorConstants.textbackgroundcolor
        } else if textField == last_Name_TxtField {
            secondView.borderwidth = 0
            secondView.backgroundColor = ColorConstants.textbackgroundcolor
        } else if textField == email_txtField {
            if !(textField.text!.isValidEmail()) {
                 thirdView.borderwidth = 2
                 thirdView.backgroundColor = ColorConstants.TextColorError
            }
            thirdView.borderwidth = 0
            thirdView.backgroundColor = ColorConstants.textbackgroundcolor
        } else {
            if !(textField.text!.isValidEmail()) {
                expenseView.borderColor = ColorConstants.TextColorError
                expenseView.borderwidth = 2
            }
            expenseView.borderwidth = 0
            expenseView.backgroundColor = ColorConstants.textbackgroundcolor
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        if textField == first_Name_TxtField {
            if txtAfterUpdate.count < 41 {
                return true
            } else {
                return false
            }
        } else if textField == last_Name_TxtField {
            if txtAfterUpdate.count < 81 {
                return true
            } else {
                return false
            }
        } else if textField == email_txtField {
            if txtAfterUpdate.count < 51 {
                return true
            } else {
                return false
            }
        } else {
            if txtAfterUpdate.count < 51 {
                return true
            } else {
                return false
            }
        }
    }
    
    @IBAction func weekly_Btn(_ sender: UIButton) {
        if sender.isSelected == false {
            sender.isSelected = true
            monthly_Switch.isSelected = false
        } else {
            sender.isSelected = false
            //monthly_Switch.isSelected = true
        }
    }
    
    @IBAction func monthly_Btn(_ sender: UIButton) {
        if sender.isSelected == false {
            sender.isSelected = true
            weekly_Switch.isSelected = false
        } else {
            sender.isSelected = false
        }
    }
    
    @objc func add_Card_Btn() {
        let VC = PaymentViewController()
        VC.checkScreen = "leftmenu"
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cardArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == cardArray.count  {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Add_Card_TableViewCell") as? Add_Card_TableViewCell
            cell?.add_card_Btn.addTarget(self, action: #selector(add_Card_Btn), for: .touchUpInside)
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCardCell") as? PaymentCardCell
            let model = cardArray[indexPath.row]
            cell?.btnRemove.tag = indexPath.row
            cell?.btnRemove.addTarget(self, action: #selector(remove_Card(sender:)), for: .touchUpInside)
            cell?.remove_Card_Img.isHidden = true
            cell?.check_Img.isHidden = false
            cell?.check_Image_TrailingConstrant.constant = 36
            
            cell?.lblCard.text = model.cardNo ?? ""
            let type = model.cardType ?? ""
            if type == "Visa" {
                cell?.imgCard.image = UIImage(named:"visa1")
            } else {
                cell?.imgCard.image = UIImage(named:"bg_card_back")
            }

            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    @objc func remove_Card(sender: UIButton) {
       
        let vc = ConfirmPopupVC()
        let id = cardArray[sender.tag].id ?? 0
        vc.confirmClicked = {
            self.callApiForDeleteCard(card_Id: id)
        }

        vc.headerTitle = "Remove Card"
        vc.message = "Are you sure you want to remove card?"
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        
        if UIViewController.current()?.presentedViewController != nil {
            UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
            })
        }
        else{
            UIViewController.current()?.present(vc, animated: true, completion: {
            })
        }
    }
    
    func callgetCardApi() {
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.getCards, method: .get, parameters: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [[String:Any]] {
                self.cardArray = Mapper<CardsListModel>().mapArray(JSONArray: dictResponse)
                self.table_View.reloadData()
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func callApiForDeleteCard(card_Id: Int) {
        
        //NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: "\(AppConstants.URL.DeleteCard)\(card_Id)", method: .delete, parameters: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            //                Utilities.showAlertWithButtonAction(title: "", message: response as? String ?? "", buttonTitle: StringConstants.ButtonTitles.KOk, onOKClick: {
            self.callgetCardApi()
            //                })
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
