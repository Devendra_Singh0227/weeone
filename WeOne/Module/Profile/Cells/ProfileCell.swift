//
//  ProfileCell.swift
//  E-Scooter
//
//  Created by CORUSCATEMAC on 06/06/19.
//  Copyright © 2019 CORUSCATEMAC. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell, UITextFieldDelegate {

    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vwRightImage: UIView!
    @IBOutlet weak var first_Name_txtField: UITextField!
    @IBOutlet weak var last_Name_TxtField: UITextField!
    @IBOutlet weak var email_TxtField: UITextField!
    @IBOutlet weak var default_PaymentLbl: UILabel!
    @IBOutlet weak var first_name_View: ViewDesign!
    @IBOutlet weak var last_name_View: ViewDesign!
    @IBOutlet weak var email_View: ViewDesign!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.first_Name_txtField.delegate = self
        self.last_Name_TxtField.delegate = self
        self.email_TxtField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        lblTitle.text = model.placeholder
        vwRightImage.isHidden = !model.isSelected
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == first_Name_txtField {
            self.first_name_View.borderwidth = 2
            self.first_name_View.backgroundColor = UIColor.white
        } else if textField == last_Name_TxtField {
            self.last_name_View.borderwidth = 2
            self.last_name_View.backgroundColor = UIColor.white
        } else {
            self.email_View.borderwidth = 2
            self.email_View.backgroundColor = UIColor.white
        }
    }
    
}
