//
//  Footer_View.swift
//  Rental_SpaceApp
//
//  Created by Dev's Mac on 19/06/20.
//  Copyright © 2020 osvinuser. All rights reserved.
//

import Foundation
import UIKit

class Footer_View: UIView {
    
    @IBOutlet weak var invite_Employee_Btn: UIButton!
    @IBOutlet weak var save_Btn: ButtonDesign!
    @IBOutlet weak var info_Btn: UIButton!
    @IBOutlet weak var info_View: ViewDesign!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    
    func loadViewFromNib() {
        let viewheader = UINib(nibName: "Footer_View", bundle: Bundle(for: type(of:self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        viewheader.frame = bounds
        viewheader.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(viewheader)
    }
    
    
    @IBAction func info_Btn(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            info_View.isHidden = false
        } else {
            sender.tag = 0
            info_View.isHidden = true
        }
    }
}


