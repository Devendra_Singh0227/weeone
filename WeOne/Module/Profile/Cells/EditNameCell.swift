//
//  EditNameCell.swift
//  WeOne
//
//  Created by iMac on 11/06/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class EditNameCell: UITableViewCell {
    
    //MARK: - Variables
    var cellModel:CellModel?
    
    //MARK: - Outlets
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var btnVerify: UIButton!
    
    @IBOutlet weak var viewFirst: UIView!
    @IBOutlet weak var viewLast: UIView!
    
    @IBOutlet weak var txtFirst: UITextField!
    @IBOutlet weak var txtLast: UITextField!
    @IBOutlet weak var default_Lbl: UILabel!
    @IBOutlet weak var email_View: ViewDesign!
    @IBOutlet weak var email_TxtField: UITextField!
    @IBOutlet weak var weekly_Btn: UIButton!
    @IBOutlet weak var monthly_Btn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        email_TxtField.delegate = self
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        
        cellModel = model
        txtFirst.delegate = self
        txtLast.delegate = self
        
        if model.cellType == .ProfileEditName {
            txtLast.text = model.userText1 ?? ""
            txtLast.keyboardType = model.keyboardType ?? .default
            viewLast.isHidden = false
            
            txtLast.tintColor = ColorConstants.ThemeColor
        } else {
            viewLast.isHidden = true
        }
        
        if ApplicationData.isUserLoggedIn{
            
            btnVerify.isHidden = true

            if model.cellType == .ProfileEditEmail{
//                if ApplicationData.user?.primaryEmail?.isVerified == false{
//                    btnVerify.setTitle("KUnverified".localized, for: .normal)
//                    btnVerify.setTitleColor(ColorConstants.TextColorSecondary, for: .normal)
//                    btnVerify.isHidden = false
//                } else {
//                    btnVerify.setTitle("KVerified".localized, for: .normal)
//                    btnVerify.setTitleColor(ColorConstants.TextColorGreen, for: .normal)
//                    btnVerify.isHidden = false
//                }
            }
        } else {
            btnVerify.isHidden = true
            //            txtField.textField.textFieldRightPadding(padding: 0)
        }
        
        if model.cellType == .ProfileChangePassword || model.cellType == .ProfileEditEmail || model.classType == .SettingsVC {
            txtFirst.isUserInteractionEnabled = false
        }
        txtFirst.tintColor = ColorConstants.ThemeColor
        txtFirst.text = model.userText ?? ""
        imgView.image = UIImage(named: model.imageName ?? "")
        txtFirst.keyboardType = model.keyboardType ?? .default
        
        
    }
}
//MARK:- TextFieldDelegate's Methods
extension  EditNameCell: UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if cellModel?.cellType == .ProfileEditName {
            cellModel?.userText = txtFirst.text
            cellModel?.userText1 = txtLast.text
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.email_View.borderwidth = 2
        self.email_View.backgroundColor = UIColor.white
        
    }
}
