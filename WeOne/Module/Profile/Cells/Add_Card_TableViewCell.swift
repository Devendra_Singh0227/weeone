//
//  Add_Card_TableViewCell.swift
//  WeOne
//
//  Created by Dev's Mac on 19/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Add_Card_TableViewCell: UITableViewCell {

    @IBOutlet weak var add_card_Btn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
