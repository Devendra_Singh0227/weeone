//
//  ExpenseReprt_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 09/09/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class ExpenseReprt_ViewController: UIViewController {

    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var header_Lbl: UILabel!
    var checkScreen = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialConfig()
    }
    
    func initialConfig(){
        
//           if AppConstants.hasSafeArea {
//               headerView_HeightConstraint.constant = 90
//           }
//           else {
//               headerView_HeightConstraint.constant = 74
//           }
        if checkScreen == "BookVC" {
            header_Lbl.text = "Insurance Policy"
        } else {
            header_Lbl.text = "Expense Report"
        }
    }

    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}
