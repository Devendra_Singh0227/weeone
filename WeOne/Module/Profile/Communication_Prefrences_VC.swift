//
//  Communication_Prefrences_VC.swift
//  WeOne
//
//  Created by Dev's Mac on 31/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Communication_Prefrences_VC: UIViewController {

    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialConfig()
    }

    func initialConfig(){
        
//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func weeOne_Btn(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            sender.setImage(UIImage(named: "switch"), for: .normal)
        } else {
            sender.tag = 0
            sender.setImage(UIImage(named: "switchActive"), for: .normal)
        }
    }
}
