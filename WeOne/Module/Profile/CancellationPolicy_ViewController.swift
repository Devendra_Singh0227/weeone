//
//  CancellationPolicy_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 23/12/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class CancellationPolicy_ViewController: UIViewController {

    @IBOutlet weak var nofee_Lbl: UILabel!
    @IBOutlet weak var tripRate_Lbl: UILabel!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
        setLblTxt()
        set_LblTxt()
        // Do any additional setup after loading the view.
    }
    
    func setLblTxt() {
        let first_String: [NSAttributedString.Key: Any] = [.foregroundColor: ColorScheme.KcommontextColor(), .font: UIFont(name: "Montserrat-Regular", size: 17)!]
        let second_String: [NSAttributedString.Key: Any] = [.foregroundColor: ColorScheme.KcommontextColor(), .font: UIFont(name: "Montserrat-SemiBold", size: 18)!]
        let firstOne = NSMutableAttributedString(string: "Cancellation more than 24 hours before the Trip starts", attributes: first_String)
        let secondOne = NSMutableAttributedString(string: " : no fee.", attributes: second_String)
        firstOne.append(secondOne)
        self.nofee_Lbl.attributedText = firstOne
    }
    
    func set_LblTxt() {
        let first_String: [NSAttributedString.Key: Any] = [.foregroundColor: ColorScheme.KcommontextColor(), .font: UIFont(name: "Montserrat-Regular", size: 17)!]
        let second_String: [NSAttributedString.Key: Any] = [.foregroundColor: ColorScheme.KcommontextColor(), .font: UIFont(name: "Montserrat-SemiBold", size: 18)!]
        let firstOne = NSMutableAttributedString(string: "Cancellation less than 24 hours before the Trip starts", attributes: first_String)
        let secondOne = NSMutableAttributedString(string: " : 50% of trip rate.", attributes: second_String)
        firstOne.append(secondOne)
        self.tripRate_Lbl.attributedText = firstOne
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
