//
//  EditProfileVC.swift
//  E-Scooter
//
//  Created by CORUSCATEMAC on 06/06/19.
//  Copyright © 2019 CORUSCATEMAC. All rights reserved.
//

import UIKit
import MobileCoreServices

class EditProfileVC: UIViewController {
    
    //MARK: - Variables
    var isUploadImage : Bool = false
    let genders = ["Male", "Female"]
    var selectedGender = 1
    var arrCellData : [CellModel] = []
    
    //MARK: outlets
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viewProfile: UIView!
    //  @IBOutlet weak var viewEdit: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightOfTableView: NSLayoutConstraint!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var btnSave: UIButtonCommon!
    @IBOutlet weak var viewNavigation: UIViewCommon!
    
    
    //MARK: view life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //        setData()
        prepareDataSource()
    }
    
    //MARK:  Initial config
    func initialConfig() {
        tableView.register(UINib(nibName: "SignupTextFieldCell", bundle: nil), forCellReuseIdentifier: "SignupTextFieldCell")
        tableView.register(UINib(nibName: "SignupPhoneTextField", bundle: nil), forCellReuseIdentifier: "SignupPhoneTextField")
        tableView.register(UINib(nibName: "EditNameCell", bundle: nil), forCellReuseIdentifier: "EditNameCell")
        
        setData()
        
        btnSave.click = {
            self.btnDone_Click()
        }
        viewNavigation.click = {
            self.btnClose_Click()
        }
        
    }
    
    //MARK: prepare Data source
    func prepareDataSource() {
        
        let user = ApplicationData.user
        
        arrCellData.removeAll()
        
//        arrCellData.append(CellModel.getModel(placeholder: "KLblFirstName".localized, placeholder2: "KLblLastName".localized, text: user?.firstName, userText1: user?.lastName, type:  .ProfileEditName, imageName: "User1@x", keyBoardType: .default, textFieldReturnType: .next, isRequired: true, apiKey: "firstName", apiKey2: "lastName", errorMessage: "KLblEnterFirstName".localized, errorMessage2: "KLblEnterLastName".localized))
//
//        arrCellData.append(CellModel.getModel(placeholder: "KLblEmailAddress".localized, text: user?.primaryEmail?.email, type: .ProfileEditEmail, imageName: "mail", keyBoardType: .emailAddress, textFieldReturnType: .next, apiKey: "emails", errorMessage: "KLblEnterEmailAddress".localized))
//
//        arrCellData.append(CellModel.getModel(placeholder: "KLblPhoneNumber".localized, text: user?.primaryMobile?.mobile, userText1: user?.primaryMobile?.countryCode, type: .ProfileEditPhone, imageName: "mobile", keyBoardType: .numberPad, textFieldReturnType: .next, apiKey: "mobiles", errorMessage: "KLblEnterPhoneNumber".localized))
//
//        arrCellData.append(CellModel.getModel(placeholder: "KLblChangePassword".localized, text: "***********", type: .ProfileChangePassword, imageName: "password-1"))
        
        tableView.reloadData()
    }
    
    func setData() {
        let user = ApplicationData.user
        if let url = URL(string: AppConstants.serverURL + "/\(user?.image ?? "")")  {
            imgProfile.setImageForURL(url: url, placeHolder: UIImage(named: "user_placeholder"))
        } else {
            imgProfile.image = UIImage(named: "user_placeholder")
        }
    }
    
    func checkValidation() -> (Bool, [String:Any]) {
        
        var request : [String:Any] = [:]
        
        for model in arrCellData {
            if model.cellType == .ProfileEditName {
                
                if Utilities.checkStringEmptyOrNil(str: model.userText) {
                    self.showToast(text: model.errorMessage ?? "")

                    //Utilities.showAlertView(message: model.errorMessage ?? "")
                    return (false , [:])
                }
                
                if Utilities.checkStringEmptyOrNil(str: model.userText1) {
                    self.showToast(text: model.errorMessage2 ?? "")

                    //Utilities.showAlertView(message: model.errorMessage2 ?? "")
                    return (false , [:])
                }
                
                request[model.apiKey ?? ""] = model.userText
                request[model.apiKey2 ?? ""] = model.userText1
                
            }
        }
        return (true, request)
    }
    
    func showPickedImage(_ info: [UIImagePickerController.InfoKey : Any]) {
        addImages(info)
    }
    
    func addImages(_ info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage{
            self.imgProfile.image = image
            self.isUploadImage = true
        }
    }
    
    func openCustomCamera() {
        if ( UIImagePickerController.isSourceTypeAvailable(.camera)) {
            let vc = CustomeCameraVC.init(nibName: "CustomeCameraVC", bundle: nil)
            vc.isFromProfile = true
            vc.isImagePicked = { dictInfo in
                self.showPickedImage(dictInfo)
            }
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let actionController: UIAlertController = UIAlertController(title: "KMsgInvalidCamera".localized, message: "", preferredStyle: .alert)
            let cancelAction: UIAlertAction = UIAlertAction(title: "KLblOk".localized, style: .cancel) { action -> Void     in
                //Just dismiss the action sheet
            }
            
            actionController.addAction(cancelAction)
            actionController.popoverPresentationController?.sourceView = UIViewController.current().view
            UIViewController.current().present(actionController, animated: true, completion: nil)
            
        }
        
    }
    
    //MARK: button action methods
    @IBAction func btnProfile_Click(_ sender: UIButton) {
        openCustomCamera()
        // Utilities.open_galley_or_camera(delegate: self, mediaType: [kUTTypeImage as String])
    }
    
    
    /*  @IBAction func btnChangePassword_Click(_ sender: UIButton) {
     let vc = ChangePasswordVC(nibName: "ChangePasswordVC", bundle: nil)
     self.navigationController?.pushViewController(vc, animated: true)
     } */
    
    func btnClose_Click() {
        self.navigationController?.popViewController(animated: true)
    }
        
    func btnDone_Click() {
        
        self.view.endEditing(true)
        let (isValidate, request) = checkValidation()
        if isValidate {
            if isUploadImage {
                callUploadImageAPI(req: request)
            } else {
                callAPIForUpdateProfile(request: request)
            }
        }
    }
}

//MARK: TableView methods
extension EditProfileVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCellData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = arrCellData[indexPath.row]
        
        switch model.cellType! {
            
        case .ProfileEditName, .ProfileEditEmail, .ProfileChangePassword:
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditNameCell") as? EditNameCell
            cell?.setData(model: model)
            return cell!
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SignupPhoneTextField") as? SignupPhoneTextField
            
            cell?.setData(model: model, isSignUp: false)
            cell?.btnVerify.addTarget(self, action: #selector(btnVerify_Click), for: .touchUpInside)
            
            return cell!
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let model = arrCellData[indexPath.row]
//        if model.cellType == .ProfileChangePassword {
//            let vc = ChangePasswordVC(nibName: "ChangePasswordVC", bundle: nil)
//            self.navigationController?.pushViewController(vc, animated: true)
//        } else if model.cellType == .ProfileEditEmail {
//            let vc = VerifyMobileVC(nibName: "VerifyMobileVC", bundle: nil)
//            self.navigationController?.pushViewController(vc, animated: true)
//
//        }  else if model.cellType == .ProfileEditPhone {
//            let vc = VerifyMobileVC(nibName: "VerifyMobileVC", bundle: nil)
//            vc.isFromMobile = true
//            self.navigationController?.pushViewController(vc, animated: true)
//
//        }
    }
    
    @objc func btnVerify_Click(_ sender : UIButton){
        moveToOtpVc()
    }
    
    func moveToOtpVc(){
        AppNavigation.shared.currentStateList = AppNavigation.shared.config.accessConfig.requiredToUseApp
        let vc = ForgotPwdOTPVC(nibName: "ForgotPwdOTPVC", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: Imagepicker delegate methods
extension EditProfileVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true) {
            if let image = info[.originalImage] as? UIImage{
                self.imgProfile.image = image
                self.isUploadImage = true
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

//MARK:- API Calling
extension EditProfileVC {
    
    func callUploadImageAPI(req:[String:Any]) {
        
        UploadManager.sharedInstance.uploadFile(AppConstants.serverURL, command: AppConstants.URL.UploadFile, image: imgProfile.image, folderKeyvalue: nil, uploadParamKey: "file", params: nil , headers: ApplicationData.sharedInstance.authorizationHeaders, isPregress: true, success: { (response, message) in
            
            if let data = response as? [String:Any]{
                
                if let dict = data["data"] as? [String:Any]{
                    
                    if let arrFiles = dict["files"] as? [[String:Any]] {
                        
                        if let obj = arrFiles.first{
                            
                            let imagePath = obj["absolutePath"] as? String
                            self.callAPIForUpdateProfile(request: req, imagePath: imagePath ?? "")
                        }
                    }
                }
            }
            
        }) { (failureMessage) in
            
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func callAPIForUpdateProfile(request:[String:Any],imagePath:String = "") {
        
        var param : [String:Any] = request
        
        if imagePath.count > 0 {
            param["image"] = imagePath
        }
        
//        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
//
//        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.UpdateProfile, method: .put, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
//
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//
//                if let dictResponse = response as? [String:Any] {
//
//                    ApplicationData.sharedInstance.saveUserData(dictResponse)
//
//                    Utilities.showAlertWithButtonAction(title: "", message: message, buttonTitle: StringConstants.ButtonTitles.KOk, onOKClick: {
//
//                        let dictMobile = ApplicationData.user?.primaryMobile
//                        if dictMobile?.isVerified == false{
//                            self.moveToOtpVc()
//                        }else{
//                            self.navigationController?.popViewController(animated: true)
//                        }
//                    })
//                }
//            }
//
//        }) { (failureMessage, failureCode) in
//            Utilities.showAlertView(message: failureMessage)
//        }
    }
}
