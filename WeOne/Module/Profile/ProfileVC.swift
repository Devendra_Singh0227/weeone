//
//  ProfileVC.swift
//  E-Scooter
//
//  Created by CORUSCATEMAC on 05/06/19.
//  Copyright © 2019 CORUSCATEMAC. All rights reserved.
//

import UIKit
import HCSStarRatingView

class ProfileVC: UIViewController, UITextFieldDelegate {

    //MARK: - Variables
    
    var arrData = [CellModel]()
    var arrCard = [CardModel]()
    var cardArray = [CardsListModel]()
    let HEADER_HEIGHT = 730
    var selected_Index = -1
    var profileModel: UserInfoModel?
    //MARK: - Outlets
    
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var first_name_TxtField: UITextField!
    @IBOutlet weak var second_name_TxtField: UITextField!
    @IBOutlet weak var email_TxtField: UITextField!
    @IBOutlet weak var firstView: ViewDesign!
    @IBOutlet weak var secondView: ViewDesign!
    @IBOutlet weak var thirdView: ViewDesign!
    @IBOutlet weak var header_View_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var rating_View: HCSStarRatingView!
    @IBOutlet weak var verify_License_Btn: UIButton!
    var req = [String:Any]()
    
    //MARK:- Controller's Life Cycle verified-24px
    override func viewDidLoad() {
        super.viewDidLoad()

        first_name_TxtField.delegate = self
        second_name_TxtField.delegate = self
        email_TxtField.delegate = self
        
        initialConfig()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == first_name_TxtField {
            firstView.borderwidth = 2
            firstView.backgroundColor = UIColor.white
        } else if textField == second_name_TxtField {
            secondView.backgroundColor = UIColor.white
            secondView.borderwidth = 2
        } else {
            thirdView.borderColor = ColorConstants.TextColorPrimary
            thirdView.backgroundColor = UIColor.white
            thirdView.borderwidth = 2
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == first_name_TxtField {
            firstView.borderwidth = 0
            firstView.borderColor = ColorConstants.TextColorPrimary
            firstView.backgroundColor = ColorConstants.textbackgroundcolor
        } else if textField == second_name_TxtField {
            secondView.borderwidth = 0
            secondView.borderColor = ColorConstants.TextColorPrimary
            secondView.backgroundColor = ColorConstants.textbackgroundcolor
        }  else {
            if !(email_TxtField.text!.isValidEmail()) {
                thirdView.borderColor = ColorConstants.TextColorError
                thirdView.BorderWidth = 2
            } else {
                thirdView.borderColor = ColorConstants.TextColorError
                thirdView.BorderWidth = 0
            }
        }
        req = ["firstName": first_name_TxtField.text!,
               "lastName": second_name_TxtField.text!]
        callAPIForUpdateProfile(request: req)
    }
    
    @IBAction func verify_License_Btn(_ sender: UIButton) {
        openCustomCamera()
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: Private Methods
    func initialConfig() {
        
         tableView.tableHeaderView = headerView
         tableView.tableHeaderView?.frame.size = CGSize(width: tableView.frame.width, height: CGFloat(HEADER_HEIGHT))
        
        tableView.register(UINib(nibName: "PaymentCardCell", bundle: nil), forCellReuseIdentifier: "PaymentCardCell")
        callApiForuser_Data()
//        prepareDataSource()
    }
    
    func setHeaderData() {
        
        first_name_TxtField.text = profileModel?.firstName ?? ""
        second_name_TxtField.text = profileModel?.lastName ?? ""
        email_TxtField.text = profileModel?.email ?? ""
        if let url = URL(string: AppConstants.serverURL + "/\(profileModel?.profilePic ?? "")")  {
           imgProfile.setImageForURL(url: url, placeHolder: UIImage(named: "user_placeholder"))
           imgProfile.contentMode = .scaleAspectFill
        }
        rating_View.value = 1
        
//        if profileModel?.isVerified == true {
//            verify_License_Btn.setTitle("", for: .normal)
//            verify_License_Btn.setImage(UIImage(named:"verified-24px"), for: .normal)
//            verify_License_Btn.isUserInteractionEnabled = false
//        } else {
//            verify_License_Btn.setTitle("Verify Driving License", for: .normal)
//            verify_License_Btn.isUserInteractionEnabled = true
//        }
    }
      
//    func prepareDataSource() {
//        arrData.removeAll()
//
//        //arrData.append(CellModel.getModel(placeholder: "KLblDocuments".localized, text: "", type: .ProfileDocument))
//        arrData.append(CellModel.getModel(placeholder: "KLblEditProfile".localized, text: "", type: .ProfileEditProfile))
//
//        arrData.append(CellModel.getModel(placeholder: "KLblChangePassword".localized, text: "", type: .SettingChangePassword))
//
//        tableView.reloadData()
//    }

    //MARK:- IBActions
    
    @IBAction func business_Btn_Click(_ sender: UIButton) {
        let VC = Business_Profile_ViewController()
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func profile_Btn_Click(_ sender: UIButton) {
        openCustomCamera()
    }
    
    @IBAction func communication_Prefrence_Btn_Click(_ sender: UIButton) {
        let VC = Communication_Prefrences_VC()
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func logout_Btn(_ sender: UIButton) {
        ApplicationData.sharedInstance.logoutUser()
    }
    
    func showPickedImage(_ info: [UIImagePickerController.InfoKey : Any]) {
        addImages(info)
    }
    
    func addImages(_ info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage{
            self.imgProfile.image = image
                        
            callUploadImageAPI()
        }
    }

    func openCustomCamera() {
//        if ( UIImagePickerController.isSourceTypeAvailable(.camera)) {
            let vc = CustomeCameraVC.init(nibName: "CustomeCameraVC", bundle: nil)
            vc.isFromProfile = true
            vc.isImagePicked = { dictInfo in
                self.showPickedImage(dictInfo)
            }
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
//        }
//        else {
//            let actionController: UIAlertController = UIAlertController(title: "KMsgInvalidCamera".localized, message: "", preferredStyle: .alert)
//            let cancelAction: UIAlertAction = UIAlertAction(title: "KLblOk".localized, style: .cancel) { action -> Void     in
//                //Just dismiss the action sheet
//            }
//
//            actionController.addAction(cancelAction)
//            actionController.popoverPresentationController?.sourceView = UIViewController.current().view
//            UIViewController.current().present(actionController, animated: true, completion: nil)
//        }
    }
    
    func callUploadImageAPI() {
        
        req = ["firstName": first_name_TxtField.text!,
               "lastName": second_name_TxtField.text!]
        
        UploadManager.sharedInstance.uploadFile(AppConstants.serverURL, command: AppConstants.URL.UploadFile, image: imgProfile.image, folderKeyvalue: nil, uploadParamKey: "file", params: nil , headers: ApplicationData.sharedInstance.authorizationHeaders, isPregress: true, success: { (response, message) in
            
            if let data = response as? [String:Any]{
                
                if let dict = data["data"] as? [String:Any]{
                    
                    if let arrFiles = dict["files"] as? [[String:Any]] {
                        
                        if let obj = arrFiles.first{
                            
                            let imagePath = obj["absolutePath"] as? String
                            self.callAPIForUpdateProfile(request: self.req, imagePath: imagePath ?? "")
                        }
                    }
                }
            }
            
        }) { (failureMessage) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func callAPIForUpdateProfile(request:[String:Any],imagePath:String = "") {
        
        var param : [String:Any] = request
        
        if imagePath.count > 0 {
            param["image"] = imagePath
        }
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.UpdateProfile, method: .put, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                
                if let dictResponse = response as? [String:Any] {
                    self.callApiForuser_Data()
                    Utilities.showAlertWithButtonAction(title: "", message: message, buttonTitle: StringConstants.ButtonTitles.KOk, onOKClick: {
                            self.navigationController?.popViewController(animated: true)
                    })
                }
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func callgetCardApi() {
        
       // NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.getCards, method: .get, parameters: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [[String:Any]] {
                    self.cardArray = Mapper<CardsListModel>().mapArray(JSONArray: dictResponse)
                    self.tableView.reloadData()
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func callApiForuser_Data() {
        
       // NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.getProfile, method: .get, parameters: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            if let dict = response as? [String:Any]{
                
                self.callgetCardApi()
                if let list = dict["data"] as? [String:Any]{
                   
                   let profileData = Mapper<UserInfoModel>().map(JSON: list)
                    ApplicationData.sharedInstance.saveUserData(list)
                
                    self.profileModel = profileData
                    self.setHeaderData()
                }
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}

//MARK: - UITableView's DataSource & Delegate Methods
extension ProfileVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cardArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = cardArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCardCell") as? PaymentCardCell
        cell?.remove_Card_Img.isHidden = true
        cell?.check_Img.isHidden = false
        cell?.btnRemove.isHidden = true
        cell?.check_Image_TrailingConstrant.constant = 30
        cell?.lblCard.text = model.cardNo ?? ""
        let type = model.cardType ?? ""
        if type == "Visa" {
             cell?.imgCard.image = UIImage(named:"visa1")
        } else {
             cell?.imgCard.image = UIImage(named:"bg_card_back")
        }
        if selected_Index == indexPath.row {
            cell?.check_Img.isHidden = false
            cell?.lblCard.textColor = ColorScheme.kTextColorPrimary()
        } else {
            cell?.check_Img.isHidden = true
             cell?.lblCard.textColor = ColorScheme.KcommontextColor()
        }

        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {        
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let model = cardArray[indexPath.row]
        selected_Index = indexPath.row
        tableView.reloadData()
        
    }
}
