//
//  PromoCodeVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 11/06/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class PromoCodeVC: UIViewController {

    //MARK: Variables
    var arrData : [CellModel] = [CellModel]()
    
    //MARK: Outlets
    @IBOutlet weak var viewNavigator: UIViewCommon!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var viewHeader: UIView!
    @IBOutlet weak var Promo_Popup: UIView!
    @IBOutlet weak var txtPromoCode: UITextField!
    @IBOutlet weak var viewSeparator: UIView!
    @IBOutlet weak var imgPromo: UIImageView!
    @IBOutlet weak var btnApply: UIButtonCommon!
    @IBOutlet weak var promoCode_TableView: UITableView!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    
    //MARK: View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
        promoCode_TableView.register(UINib(nibName: "PromocodeCell", bundle: nil), forCellReuseIdentifier: "PromocodeCell")
       
    }

    //MARK: Initial config
    func initialConfig(){
        
//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
//        viewTxtPromo.layer.borderWidth = 1.0
//        viewTxtPromo.layer.borderColor = ColorConstants.BorderColor.cgColor
        
//        btnApply.setBackGroundColor(color: ColorConstants.ButtonBorderColor)
//        btnApply.button.isUserInteractionEnabled = false
//
//        promoCode_TableView.register(UINib(nibName: "PromocodeCell", bundle: nil), forCellReuseIdentifier: "PromocodeCell")
//
//        viewSeparator.isHidden = true
//        txtPromoCode.delegate = self
        
//        tableView.tableHeaderView = viewHeader
//        btnApply.click = {
//            self.btnApply_Click()
//        }
        
//        viewNavigator.click = {
//            self.navigationController?.popViewController(animated: true)
//        }
    }
    
    //MARK: Button action method
    
    @IBAction func apply_Btn(_ sender: UIButton) {
        let vc = PromocodeAppliedPopup()
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        vc.isDismissed = {
//            self.viewTxtPromo.layer.borderWidth = 1.0
//            self.viewTxtPromo.layer.borderColor = ColorConstants.BorderColor.cgColor
//            self.txtPromoCode.text = ""
//            self.viewSeparator.isHidden = false
//            self.imgPromo.isHidden = true
//            self.arrData.append(CellModel())
//            self.tableView.reloadData()
        }
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

//MARK: Tableview methods
extension PromoCodeVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PromocodeCell", for: indexPath) as! PromocodeCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 124
    }
}

//MARK: textField delegate methods
extension PromoCodeVC : UITextFieldDelegate{
    
    @IBAction func txtPromoTextChanged(_ textField: UITextField) {
        
        if textField.text?.count ?? 0 > 0 {
//            viewTxtPromo.layer.borderWidth = 1.0
//            viewTxtPromo.layer.borderColor = ColorConstants.ScheduleThemeColor.cgColor
        }else{
//            viewTxtPromo.layer.borderWidth = 1.0
//            viewTxtPromo.layer.borderColor = ColorConstants.BorderColor.cgColor
        }
        
        if textField.text?.count ?? 0 > 6 {
            btnApply.button.isUserInteractionEnabled = true
            btnApply.setBackGroundColor(color: ColorConstants.TextColorPrimary)
            
        } else {
            btnApply.button.isUserInteractionEnabled = false
            btnApply.setBackGroundColor(color: ColorConstants.ButtonBorderColor)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if !string.canBeConverted(to: String.Encoding.ascii) {
            return false
        }
        
        if range.location == 0{
            
            if string == " " {
                return false
            }
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
}


