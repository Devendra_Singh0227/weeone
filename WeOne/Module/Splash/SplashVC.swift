//
//  SplashVC.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved

import UIKit
import Photos
import MobileCoreServices
import CoreLocation
import RevealingSplashView

class SplashVC: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var splashImage: UIImageView!
    @IBOutlet weak var btnRefresh: UIButtonCommon!
    @IBOutlet weak var location_View: UIView!
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    @IBOutlet weak var title_Lbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        showLocation_PopUp()
        checkUserlogged()
//        syncApi()
//        let revealingSplashView = RevealingSplashView(iconImage: UIImage(named: "icon")!,iconInitialSize: CGSize(width: 80, height: 80), backgroundColor: ColorConstants.commonWhite)
//
//        //Adds the revealing splash view as a sub view
//        self.view.addSubview(revealingSplashView)
//
//        //Starts animation
//        revealingSplashView.startAnimation(){
//            print("Completed")
//            self.location_View.backgroundColor = ColorConstants.TextColorPrimary
//            self.showLocation_PopUp()
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    func checkUserlogged() {
        if Defaults[.isUserLoggedIn] == true {
            navigateTo_Home()
        } else {
            if Defaults[.isUserLoggedOut] == true {
                navigate_To_Login()
            } else {
              showLocation_PopUp()
            }
        }
    }
    
    //IntialConfig
    func navigate_To_Login() {
//        syncApi()
        let vc = SignUpVC(nibName: "SignUpVC", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
     func navigateTo_Home() {
        let vc = HomeVC()
        vc.checkScreen = "Splash"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func syncApi(){
        AppNavigation.shared.currentStateList = AppNavigation.shared.config.accessConfig.requiredToUseApp
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        SyncManager.sharedInstance.syncMaster({
            self.checkUserlogged()
            // success
           // self.nevigateNext()
        }) {
            
            if Defaults[.MasterSyncDate] != nil && Defaults[.MasterSyncDate] != AppConstants.startingDate{
                //self.nevigateNext()
            } else {
                // failure
                self.btnRefresh.isHidden = false
            }
        }
    }
    
    func showLocation_PopUp() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.location_View.isHidden = false
        })
    }
    
    func fetchCurrentLocation() {
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        
        // Here you can check whether you have allowed the permission or not.
        if CLLocationManager.locationServicesEnabled(){
            switch(CLLocationManager.authorizationStatus()){
            case .authorizedAlways, .authorizedWhenInUse:
                print("Authorize.")
                Defaults[.location_access] = true
                self.locationManager.startUpdatingLocation()
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                    CLLocationManager.authorizationStatus() ==  .authorizedAlways){
                    currentLocation = locationManager.location
                }
                break
            case .notDetermined:
                print("Not determined.")
                 Defaults[.location_access] = false
                DispatchQueue.main.async {
                    LocationManager.SharedManager.openSettings()
                }
                break
            case .restricted:
                print("Restricted.")
                  Defaults[.location_access] = false
                DispatchQueue.main.async {
                    LocationManager.SharedManager.openSettings()
                }
                break
            case .denied:
                print("Denied.")
                  Defaults[.location_access] = false
                DispatchQueue.main.async {
                    LocationManager.SharedManager.openSettings()
                }
            @unknown default:
                print("")
            }
        }
    }
    
    @IBAction func allow_Btn(_ sender: UIButton) {
        Defaults[.location_access] = true
        location_View.isHidden = true
        fetchCurrentLocation()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.navigate_To_Login()
        })
    }
    
    @IBAction func deny_Btn(_ sender: UIButton) {
        location_View.isHidden = true
        Defaults[.location_access] = false
        self.navigate_To_Login()
    }
    
    func checkVersionUpdate() {
        
        /*NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.VersionCheck, method: .get, parameters: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
         
         if let dictResponse = response as? [String:Any]{
         Utilities.checkVersioningUpdate(dict: dictResponse)
         }
         }) { (failureMessage, failureCode) in
         
         }*/
    }
    
    func btnRefresh_Click() {
        self.btnRefresh.isHidden = true
//        intialConfig()
    }
}

