//
//  FilterVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 03/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit
import RangeSeekSlider

class FilterVC: UIViewController, RangeSeekSliderDelegate {
    
    //MARK: - Variables
    var arrData = [CellModel]()
    var isFilterApplied : (() -> ())?
    let req = Defaults[.Filter]
    var index = -1
    var rideType : RideType = .Upcoming
    
    enum filterType {
        case Category
        case Brand
        case Price
        case Equipment
        case Transmission
        case ChildSeat
        case Other
    }
    
    var filter : ((_ category : Date, _ brand: Date,  _ price: String,  _ equipment: String,  _ transmission: String,  _ childseat: String,  _ other: String) -> ())?
    
    //MARK: - Outlets
    
    @IBOutlet weak var viewNavigation: UIViewCommon!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnShowCars: UIButtonCommon!
    var filterCell : filterType!
    @IBOutlet weak var category_View: UIView!
    @IBOutlet weak var brand_View: UIView!
    @IBOutlet weak var price_View: UIView!
    @IBOutlet weak var equipment_View: UIView!
    @IBOutlet weak var transmission_View: UIView!
    @IBOutlet weak var ChildSeat_View: UIView!
    @IBOutlet weak var others_View: UIView!
    @IBOutlet weak var search_height: NSLayoutConstraint!
    @IBOutlet weak var PriceFilter_View: UIView!
    @IBOutlet weak var search_View: UIView!
    @IBOutlet weak var slider_Min_Value_Lbl: UILabel!
    @IBOutlet weak var slider_Max_Lbl: UILabel!
    @IBOutlet weak var range_Slider: RangeSeekSlider!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var apply_Btn: UIButton!
    @IBOutlet weak var clear_Btn: UIButton!
    var imagesArray = ["car icons-2", "car icons-7" , "car icons-4", "car icons-5", "car icons-6", "car icons-8", "Group 3406", "car icons-9", "car icons-10"]
    var category_filter_array = ["City", "Compact", "Family", "Small Van", "Large Van", "Multi Van", "Trailer", "SUV", "Exclusive"]
    var brand_filter = ["Audi", "BMW", "Chevrolet", "FIAT", "Ford", "Hyundai", "Jeep", "Kia", "Mazda"]
    var classArray = ["Light", "Medium", "Exclusive"]
    var transmissionArray = ["Automatic", "Manual", "Hybrid", "Electric", "Gasoline", "Diesel"]
    var otherArray = ["All-Wheel Drive", "Tow Hitch", "Loading Facility", "Winter Tyres", "Suitcases", "Cargo"]
    var childArray = ["Infants\n(0 to 12 Months)", "Toddlers\n(1 to 4 Years)", "Young Children\n(4 to 11 Years)"]
    //var ageArray = ["Infants\n(0 to 12 Months)", "Toddlers\n(1 to 4 Years)", "Young Children\n(4 to 11 Years)"]
    var filter_Array = [MasterModel]()
    
    //MARK: - View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        range_Slider.delegate = self
        filterCell = filterType.Category
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }
    
    //MARK: Private Methods
    func initialConfig() {
        
        viewNavigation.text = "KLblCarFilter".localized as! NSString

        tableView.register(UINib(nibName: "Category_FilterTCell", bundle: nil), forCellReuseIdentifier: "Category_FilterTCell")
        tableView.register(UINib(nibName: "TypeFilterCell", bundle: nil), forCellReuseIdentifier: "TypeFilterCell")
        tableView.register(UINib(nibName: "TrasmissionFilterCell", bundle: nil), forCellReuseIdentifier: "TrasmissionFilterCell")
        tableView.register(UINib(nibName: "SpecialsFilterCell", bundle: nil), forCellReuseIdentifier: "SpecialsFilterCell")
        tableView.register(UINib(nibName: "FuelChargeFilterCell", bundle: nil), forCellReuseIdentifier: "FuelChargeFilterCell")
        tableView.register(UINib(nibName: "BrandTagCell", bundle: nil), forCellReuseIdentifier: "BrandTagCell")
                
//        let master = SyncManager.sharedInstance.fetchMasterWithCode(AppConstants.MasterCode.VehicleCategory)
//        if master.count > 0{
//
//            let arrCarTypes = SyncManager.sharedInstance.fetchMasterWithParentId(master.first!.id ?? "")
//            filter_Array = arrCarTypes
//        }
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func category_Btn(_ sender: UIButton) {
        index = -1
        filter_Array.removeAll()
//        let master = SyncManager.sharedInstance.fetchMasterWithCode(AppConstants.MasterCode.VehicleCategory)
//        if master.count > 0{
//
//            let arrCarTypes = SyncManager.sharedInstance.fetchMasterWithParentId(master.first!.id ?? "")
//            filter_Array = category_filter_array//arrCarTypes
//        }
        PriceFilter_View.isHidden = true
        search_height.constant = 0
        search_View.isHidden = true
        filterCell = filterType.Category
        category_View.backgroundColor = UIColor.white
        brand_View.backgroundColor = ColorScheme.KcommonWhite()
        price_View.backgroundColor = ColorScheme.KcommonWhite()
        equipment_View.backgroundColor = ColorScheme.KcommonWhite()
        transmission_View.backgroundColor = ColorScheme.KcommonWhite()
        others_View.backgroundColor = ColorScheme.KcommonWhite()
        ChildSeat_View.backgroundColor = ColorScheme.KcommonWhite()
        tableView.reloadData()
    }
    
    @IBAction func brand_Btn(_ sender: UIButton) {
        index = -1
        filter_Array.removeAll()
//        let master = SyncManager.sharedInstance.fetchMasterWithCode(AppConstants.MasterCode.Brand)
//        if master.count > 0{
//
//            let arrChildSeats = SyncManager.sharedInstance.fetchMasterWithParentId(master.first!.id ?? "")
//            filter_Array = arrChildSeats
//        }
        
        PriceFilter_View.isHidden = true
        search_height.constant = 66
        search_View.isHidden = false
        filterCell = filterType.Brand
        category_View.backgroundColor = ColorScheme.KcommonWhite()
        brand_View.backgroundColor = UIColor.white
        price_View.backgroundColor = ColorScheme.KcommonWhite()
        equipment_View.backgroundColor = ColorScheme.KcommonWhite()
        transmission_View.backgroundColor = ColorScheme.KcommonWhite()
        others_View.backgroundColor = ColorScheme.KcommonWhite()
        ChildSeat_View.backgroundColor = ColorScheme.KcommonWhite()
        tableView.reloadData()
    }
    
    @IBAction func price_Btn(_ sender: UIButton) {
        index = -1
        PriceFilter_View.isHidden = false
        search_View.isHidden = true
        filterCell = filterType.Price
        search_height.constant = 0
        category_View.backgroundColor = ColorScheme.KcommonWhite()
        brand_View.backgroundColor = ColorScheme.KcommonWhite()
        price_View.backgroundColor = UIColor.white
        equipment_View.backgroundColor = ColorScheme.KcommonWhite()
        transmission_View.backgroundColor = ColorScheme.KcommonWhite()
        others_View.backgroundColor = ColorScheme.KcommonWhite()
        ChildSeat_View.backgroundColor = ColorScheme.KcommonWhite()
    }
    
    @IBAction func equipment_Btn(_ sender: UIButton) {
        index = -1
        filter_Array.removeAll()
//        let master = SyncManager.sharedInstance.fetchMasterWithCode(AppConstants.MasterCode.EquipmentType)
//        if master.count > 0{
//
//            let arrChildSeats = SyncManager.sharedInstance.fetchMasterWithParentId(master.first!.id ?? "")
//            filter_Array = arrChildSeats
//        }
        
        PriceFilter_View.isHidden = true
        search_View.isHidden = true
        filterCell = filterType.Equipment
        search_height.constant = 0
        category_View.backgroundColor = ColorScheme.KcommonWhite()
        brand_View.backgroundColor = ColorScheme.KcommonWhite()
        price_View.backgroundColor = ColorScheme.KcommonWhite()
        equipment_View.backgroundColor = UIColor.white
        transmission_View.backgroundColor = ColorScheme.KcommonWhite()
        others_View.backgroundColor = ColorScheme.KcommonWhite()
        ChildSeat_View.backgroundColor = ColorScheme.KcommonWhite()
        tableView.reloadData()
    }
    
    @IBAction func transmission_Btn(_ sender: UIButton) {
        index = -1
        prepareDataSource()
        PriceFilter_View.isHidden = true
        search_View.isHidden = true
        filterCell = filterType.Transmission
        search_height.constant = 0
        category_View.backgroundColor = ColorScheme.KcommonWhite()
        brand_View.backgroundColor = ColorScheme.KcommonWhite()
        price_View.backgroundColor = ColorScheme.KcommonWhite()
        equipment_View.backgroundColor = ColorScheme.KcommonWhite()
        transmission_View.backgroundColor = UIColor.white
        others_View.backgroundColor = ColorScheme.KcommonWhite()
        ChildSeat_View.backgroundColor = ColorScheme.KcommonWhite()
        tableView.reloadData()
    }
    
    @IBAction func child_Btn(_ sender: UIButton) {
        index = -1
        filter_Array.removeAll()
//        let master = SyncManager.sharedInstance.fetchMasterWithCode(AppConstants.MasterCode.ChildSeats)
//        if master.count > 0{
//
//            let arrChildSeats = SyncManager.sharedInstance.fetchMasterWithParentId(master.first!.id ?? "")
//            filter_Array = arrChildSeats
//        }
        
        PriceFilter_View.isHidden = true
        search_View.isHidden = true
        search_height.constant = 0
        filterCell = filterType.ChildSeat
        category_View.backgroundColor = ColorScheme.KcommonWhite()
        brand_View.backgroundColor = ColorScheme.KcommonWhite()
        price_View.backgroundColor = ColorScheme.KcommonWhite()
        equipment_View.backgroundColor = ColorScheme.KcommonWhite()
        transmission_View.backgroundColor = ColorScheme.KcommonWhite()
        others_View.backgroundColor = ColorScheme.KcommonWhite()
        ChildSeat_View.backgroundColor = UIColor.white
        tableView.reloadData()
    }
    
    @IBAction func other_Btn(_ sender: UIButton) {
        index = -1
        PriceFilter_View.isHidden = true
        search_View.isHidden = true
        search_height.constant = 0
        filterCell = filterType.Other
        category_View.backgroundColor = ColorScheme.KcommonWhite()
        brand_View.backgroundColor = ColorScheme.KcommonWhite()
        price_View.backgroundColor = ColorScheme.KcommonWhite()
        equipment_View.backgroundColor = ColorScheme.KcommonWhite()
        transmission_View.backgroundColor = ColorScheme.KcommonWhite()
        others_View.backgroundColor = UIColor.white
        ChildSeat_View.backgroundColor = ColorScheme.KcommonWhite()
        tableView.reloadData()
    }
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, stringForMaxValue maxValue: CGFloat) -> String? {
        print(maxValue)
        slider_Max_Lbl.text = "\(maxValue) kr"
        return "\(maxValue)"
    }
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, stringForMinValue minValue: CGFloat) -> String? {
        print(minValue)
        slider_Min_Value_Lbl.text = "\(minValue) kr - "
        return "\(minValue)"
    }
    
    func prepareDataSource() {
        
        arrData.removeAll()
        
        let filterReq = Defaults[.Filter]
        
        if true{
            let master = SyncManager.sharedInstance.fetchMasterWithCode(AppConstants.MasterCode.VehicleCategory)
            if master.count > 0{
                
                let arrCarTypes = SyncManager.sharedInstance.fetchMasterWithParentId(master.first!.id ?? "")
                
                if filterReq != nil {
                    arrCarTypes.forEach { (model) in
                        if (filterReq!["category"] as? [String] ?? []).contains(model.id ?? ""){
                            model.isSelected = true
                        }
                    }
                }
                
                arrData.append(CellModel.getModel(placeholder: "Brands".localized, placeholder2: "", text: "", type: .FilterType, cellObj: nil, dataArr: arrCarTypes))
            }
        }
        
        //Car Key
        if true{
            
            var arrCarKey = [CellModel]()
            arrCarKey.append(CellModel.getModel(placeholder: "KLblApplication".localized, placeholder2: "", text: "", type: .FilterKeys, cellObj: nil))
            arrCarKey.append(CellModel.getModel(placeholder: "KLblKey".localized, placeholder2: "", text: "", type: .FilterKeys, cellObj: nil))
            
            if filterReq != nil {
                arrCarKey.forEach { (model) in
                    if (filterReq!["iotProvider"] as? [Int] ?? []).contains(Int(model.placeholder == "KLblApplication".localized ? AppConstants.CarKeyTypes.AUTOMATIC : AppConstants.CarKeyTypes.MANUAL)){
                        model.isSelected = true
                    }
                }
            }
            
            arrData.append(CellModel.getModel(placeholder: "KLblUNLOCKINGCAR".localized, placeholder2: "", text: "", type: .FilterTransmission, imageName: "", cellObj: nil, dataArr: arrCarKey))
        }
 
        //Transmission
        if true{
            
            var arrTransmission = [CellModel]()

            arrTransmission.append(CellModel.getModel(placeholder: "KAutomatic".localized, placeholder2: "gearBox", text: "\(AppConstants.GearBoxType.AUTOMATIC)", type: .FilterTransmission, imageName: "manu-gear", cellObj: nil))
            arrTransmission.append(CellModel.getModel(placeholder: "KManual".localized, placeholder2: "gearBox", text: "\(AppConstants.GearBoxType.MANUAL)", type: .FilterTransmission, imageName: "manu-gear", cellObj: nil))
            
            arrTransmission.append(CellModel.getModel(placeholder: "KLblElectric".localized, placeholder2: "fuelType", text: "\(AppConstants.FuelType.Electric)", type: .FilterTransmission, cellObj: nil))
            arrTransmission.append(CellModel.getModel(placeholder: "KLblGasoline".localized, placeholder2: "carAttributes.gasoline", text: "", type: .FilterTransmission, cellObj: nil))

            arrTransmission.append(CellModel.getModel(placeholder: "KLblHybrid".localized, placeholder2: "carAttributes.hybrid", text: "", type: .FilterTransmission, cellObj: nil))
            arrTransmission.append(CellModel.getModel(placeholder: "KLblDiesel".localized, placeholder2: "carAttributes.diesel", text: "", type: .FilterTransmission, cellObj: nil))

            if filterReq != nil {
                arrTransmission.forEach { (model) in
                    
                    if let values = filterReq![model.placeholder2 ?? ""] as? NSArray {
                        if model.placeholder2 == "fuelType" && values.contains(Int(model.userText ?? "") ?? 0){
                            model.isSelected = true
                        } else if model.placeholder2 == "gearBox" && values.contains(Int(model.userText ?? "") ?? 0){
                            model.isSelected = true
                        }
                    } else if filterReq!.keys.contains(model.placeholder2 ?? "") && filterReq![model.placeholder2 ?? ""] as? Int == 1 {
                        model.isSelected = true
                    } else if filterReq!.keys.contains(model.placeholder2 ?? "") &&  filterReq![model.placeholder2 ?? ""] as? Int == 2{
                        model.isSelected = true
                    }
                }
            }

            
            arrData.append(CellModel.getModel(placeholder: "KLblOtherFilters".localized, placeholder2: "", text: "", type: .FilterTransmission, imageName: "", cellObj: nil, dataArr: arrTransmission))
            
        }
        
        // Fuel Type
        if true{
            var arrFuelType = [CellModel]()
            
            arrFuelType.append(CellModel.getModel(placeholder: "KLblAll-wheel".localized, placeholder2: "carAttributes.wheelDrive", text: "", type: .FilterFualType, cellObj: nil))

            arrFuelType.append(CellModel.getModel(placeholder: "KLblWinterTyres".localized, placeholder2: "carAttributes.winterTyre", text: "", type: .FilterFualType, cellObj: nil))


            arrFuelType.append(CellModel.getModel(placeholder: "KLblTowhitch".localized, placeholder2: "carAttributes.towHitch", text: "", type: .FilterFualType, cellObj: nil))
            

            arrFuelType.append(CellModel.getModel(placeholder: "KLblSuitcases".localized, placeholder2: "suitcases", text: "", type: .FilterFualType, cellObj: nil))

            arrFuelType.append(CellModel.getModel(placeholder: "KLblLoadingFacility".localized, placeholder2: "carAttributes.throughLoadingFacility", text: "", type: .FilterFualType, cellObj: nil))

            arrFuelType.append(CellModel.getModel(placeholder: "KLblCargo".localized, placeholder2: "carAttributes.cargo", text: "", type: .FilterFualType, cellObj: nil))

            
            if filterReq != nil {
                arrFuelType.forEach { (model) in
                    if filterReq!.keys.contains(model.placeholder2 ?? "") && filterReq![model.placeholder2 ?? ""] as? Int == 1 {
                        model.isSelected = true
                    }
                }
            }

            arrData.append(CellModel.getModel(placeholder: "KLblFualType".localized, placeholder2: "", text: "", type: .FilterFualType, imageName: "", cellObj: nil, dataArr: arrFuelType, apiKey: "", errorMessage: ""))
            
        }
        
        // Child Seats
        if true {
            
            let master = SyncManager.sharedInstance.fetchMasterWithCode(AppConstants.MasterCode.ChildSeats)
            if master.count > 0{
                
                let arrChildSeats = SyncManager.sharedInstance.fetchMasterWithParentId(master.first!.id ?? "")
                
                var arrChild = [CellModel]()

                for item in arrChildSeats {
                    arrChild.append(CellModel.getModel(placeholder: item.name, placeholder2: "", text: "", type: .FilterChildSeats, cellObj: nil, data: arrChildSeats))
                }

                if filterReq != nil {
                    arrChildSeats.forEach { (model) in
                        if (filterReq!["childSeats"] as? [String] ?? []).contains(model.id ?? ""){
                            model.isSelected = true
                        }
                    }
                }
                
                arrData.append(CellModel.getModel(placeholder: "KLblChildSeats".localized, placeholder2: "", text: "", type: .FilterChildSeats, imageName: "", cellObj: nil, dataArr: arrChildSeats))
                
            }
        }
        
        tableView.reloadData()
    }
    
    //MARK:- IBActions
    func btnShowCars_Click() {
        
        var req = [String:Any]()
        
        for model in arrData{
            
            if model.cellType == CellType.FilterType{
                
                if let arrMaster = model.dataArr as? [MasterModel], (arrMaster.filter { $0.isSelected }).count > 0{
                    req["category"] = arrMaster.filter { $0.isSelected }.map { $0.id ?? ""}
                }
                
            } else if model.cellType == CellType.FilterTransmission{
                
                if let arrMaster = model.dataArr as? [CellModel], (arrMaster.filter { $0.isSelected }).count > 0{
                    
                    if model.placeholder == "KLblUNLOCKINGCAR".localized {
                        req["iotProvider"] = arrMaster.filter { $0.isSelected }.map { ($0.placeholder == "KLblApplication".localized ? AppConstants.CarKeyTypes.AUTOMATIC : AppConstants.CarKeyTypes.MANUAL) }.first
                    } else if model.placeholder == "KLblOtherFilters".localized {
                        
                        req["fuelType"] = arrMaster.filter { $0.isSelected && $0.placeholder2 == "fuelType" }.map { ($0.placeholder == "KLblElectric".localized ? AppConstants.FuelType.Electric : AppConstants.FuelType.Combustion) }

                        req["gearBox"]  = arrMaster.filter { $0.isSelected  && $0.placeholder2 == "gearBox" }.map { ($0.placeholder == "KAutomatic".localized ? AppConstants.GearBoxType.AUTOMATIC : AppConstants.GearBoxType.MANUAL) }

                        req["carAttributes.gasoline"]  = arrMaster.filter { $0.isSelected  && $0.placeholder == "KLblGasoline".localized }.map {$0.isSelected}.first

                        req["carAttributes.hybrid"]  = arrMaster.filter { $0.isSelected  && $0.placeholder == "KLblHybrid".localized }.map {$0.isSelected}.first

                        req["carAttributes.diesel"]  = arrMaster.filter { $0.isSelected  && $0.placeholder == "KLblDiesel".localized }.map {$0.isSelected}.first

                    }
                }
            } else if model.cellType == CellType.FilterFualType {
                
                if let arrMaster = model.dataArr as? [CellModel], (arrMaster.filter { $0.isSelected }).count > 0{
                    req["carAttributes.wheelDrive"] = arrMaster.filter { $0.isSelected && $0.placeholder == "KLblAll-wheel".localized}.map{$0.isSelected}.first

                    req["carAttributes.winterTyre"] = arrMaster.filter { $0.isSelected && $0.placeholder == "KLblWinterTyres".localized}.map{$0.isSelected}.first

                    req["carAttributes.towHitch"] = arrMaster.filter { $0.isSelected && $0.placeholder == "KLblTowhitch".localized}.map{$0.isSelected}.first

                    req["suitcases"] = arrMaster.filter { $0.isSelected && $0.placeholder == "KLblSuitcases".localized}.map{$0.isSelected}.first

                    req["carAttributes.throughLoadingFacility"] = arrMaster.filter { $0.isSelected && $0.placeholder == "KLblLoadingFacility".localized}.map{$0.isSelected}.first
                    
                    req["carAttributes.cargo"] = arrMaster.filter { $0.isSelected && $0.placeholder == "KLblCargo".localized}.map{$0.isSelected}.first


                }
            } else if model.cellType == CellType.FilterChildSeats{
                
                if let arrMaster = model.dataArr as? [MasterModel], (arrMaster.filter { $0.isSelected }).count > 0 {
                                        
                    req["childSeats"] = arrMaster.filter { $0.isSelected }.map { $0.id ?? ""}
                }

            
            } else if model.cellType == CellType.FilterFuleCharge{
                
                var request : [String:Any] = [:]
                
                if Int(model.userText ?? "0") ?? 0 > 0{
                    request[">="] = Int(model.userText ?? "0")
                }
                
                if Int(model.userText1 ?? "100") ?? 100 < 100{
                    request["<="] = Int(model.userText1 ?? "0")
                }
                
                if request.count > 0{
                    req["fuelLevel"] = request
                }
            }
        }
        
        print(req)
        
        if req.count > 0{
            Defaults[.Filter] = req
        }else{
            Defaults[.Filter] = nil
        }
        
        isFilterApplied?()
        self.navigationController?.popViewController(animated: true)
    }
    
    func btnClose_Click() {
        Defaults[.Filter] = req
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnReset_Click(_ sender: UIButton) {
        
        Defaults[.Filter] = nil
        self.prepareDataSource()
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        }
    }
}

//MARK: - UITableView's DataSource & Delegate Methods
extension FilterVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         switch filterCell {
         case .Category:
            return category_filter_array.count//filter_Array.count
         case .Brand:
            return brand_filter.count//filter_Array.count
         case .Price:
            return 1
         case .Equipment:
            return classArray.count
         case .Transmission:
            return transmissionArray.count//filter_Array.count
         case .ChildSeat:
            return childArray.count//filter_Array.count
         case .Other:
            return otherArray.count
         case .none:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //let filterArray = filter_Array[indexPath.row]
        switch filterCell {
        case .Category:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Category_FilterTCell") as? Category_FilterTCell
           
            cell?.car_View.isHidden = false
//            let image = filterArray.icon!
//            let url = URL(string: "\(AppConstants.imageURL)\(image )")
            cell?.car_Image.image = UIImage(named: imagesArray[indexPath.row]) //.setImageForURL(url: url, placeHolder: UIImage(named: ""))
            cell?.item_Label.text = category_filter_array[indexPath.row]//filterArray.name ?? ""
//            cell?.age_Lbl.text = ""
            if indexPath.row == index {
                cell?.car_View.borderwidth = 1
                cell?.tick_Image.image = UIImage(named: "checkpurple")
            } else {
                cell?.car_View.borderwidth = 0
                cell?.tick_Image.image = UIImage(named: "checkwhite")
            }
            //cell?.setData(model: model)
            return cell!
            
        case .ChildSeat :
            let cell = tableView.dequeueReusableCell(withIdentifier: "Category_FilterTCell") as? Category_FilterTCell
            cell?.car_View.isHidden = true
            cell?.item_Label.text = childArray[indexPath.row]
//            cell?.age_Lbl.text = filterArray.name ?? ""
            if indexPath.row == index {
                cell?.tick_Image.image = UIImage(named: "checkpurple")
            } else {
                cell?.tick_Image.image = UIImage(named: "checkwhite")
            }
//            cell?.setData(model: model)
            return cell!
            
        case .Brand:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Category_FilterTCell") as? Category_FilterTCell
            cell?.car_View.isHidden = true
            cell?.item_Label.text = brand_filter[indexPath.row]//filterArray.name ?? ""
            cell?.age_Lbl.text = ""
            if indexPath.row == index {
                cell?.tick_Image.image = UIImage(named: "checkpurple")
            } else {
                cell?.tick_Image.image = UIImage(named: "checkwhite")
            }
//            cell?.setData(model: model)
            return cell!
            
        case .Equipment :
             let cell = tableView.dequeueReusableCell(withIdentifier: "Category_FilterTCell") as? Category_FilterTCell
             cell?.car_View.isHidden = true
             cell?.item_Label.text = classArray[indexPath.row]
             cell?.age_Lbl.text = ""
             if indexPath.row == index {
                 cell?.tick_Image.image = UIImage(named: "checkpurple")
             } else {
                 cell?.tick_Image.image = UIImage(named: "checkwhite")
             }
//            cell?.setData(model: model)
            return cell!
            
        case .Price :
             let cell = tableView.dequeueReusableCell(withIdentifier: "BrandTagCell") as? BrandTagCell
//            cell?.setData(model: model)
            return cell!
            
        case .Transmission :
             let cell = tableView.dequeueReusableCell(withIdentifier: "Category_FilterTCell") as? Category_FilterTCell
             cell?.car_View.isHidden = true
             cell?.item_Label.text = transmissionArray[indexPath.row]//filterArray.name ?? ""
             cell?.age_Lbl.text = ""
             if indexPath.row == index {
                 cell?.tick_Image.image = UIImage(named: "checkpurple")
             } else {
                 cell?.tick_Image.image = UIImage(named: "checkwhite")
             }
//            cell?.setData(model: model)
            return cell!
            
        case .Other :
//            cell?.setData(model: model)
            let cell = tableView.dequeueReusableCell(withIdentifier: "Category_FilterTCell") as? Category_FilterTCell
            cell?.car_View.isHidden = true
            cell?.item_Label.text = otherArray[indexPath.row]
            cell?.age_Lbl.text = ""
            if indexPath.row == index {
                cell?.tick_Image.image = UIImage(named: "checkpurple")
            } else {
                cell?.tick_Image.image = UIImage(named: "checkwhite")
            }
            return cell!
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        index = indexPath.row
        clear_Btn.setTitleColor(ColorConstants.TextColorPrimary, for: .normal)
        apply_Btn.setTitleColor(ColorConstants.TextColorPrimary, for: .normal)
        tableView.reloadData()
    }
}

//MARK:- Extra Methods
extension FilterVC {
    
    convenience init() {
        self.init(nibName: "FilterVC", bundle: nil)
    }
}
