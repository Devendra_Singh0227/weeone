//
//  TrasmissionFilterCollectionCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 03/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class TrasmissionFilterCollectionCell: UICollectionViewCell {

    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var vwBg: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
//    @IBOutlet weak var imgCheck: UIImageView!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    //MARK: Set Data
    func setData(cellModel:CellModel?, masterModel: MasterModel?) {
        
        if let model = cellModel {
            if model.cellType == .FilterFualType {
                lblTitle.textColor = ColorConstants.TextColorPrimary.withAlphaComponent(0.80)
            } else {
                lblTitle.textColor = ColorConstants.TextColorPrimary
            }

            lblTitle.text = model.placeholder

            imgView.image = UIImage(named: "unchecked_checkbox_black")

            if model.isSelected {
                imgView.image = UIImage(named: ThemeManager.sharedInstance.getImage(string: "checked"))
            }else {
                imgView.image = UIImage(named: "unchecked_checkbox_black")
            }

        } else if let model = masterModel {
            lblTitle.textColor = ColorConstants.TextColorPrimary

            lblTitle.text = model.name

            imgView.image = UIImage(named: "unchecked_checkbox_black")

            if model.isSelected {
                imgView.image = UIImage(named: ThemeManager.sharedInstance.getImage(string: "checked"))
            }else {
                imgView.image = UIImage(named: "unchecked_checkbox_black")
            }

        }
    }
}
