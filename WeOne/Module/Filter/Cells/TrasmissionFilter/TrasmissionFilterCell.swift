//
//  TrasmissionFilterCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 03/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class TrasmissionFilterCell: UITableViewCell {

    //MARK: - Variables
    var arrData = [CellModel]()
    var arrMaster : [MasterModel] = [MasterModel]()

    //MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!

    @IBOutlet weak var viewLineLeading: NSLayoutConstraint!
    @IBOutlet weak var viewLineTrailing: NSLayoutConstraint!

    @IBOutlet weak var viewLine: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblHeight: NSLayoutConstraint!
    @IBOutlet weak var lblBottom: NSLayoutConstraint!

    var cellModel = CellModel()
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        
        collectionView.register(UINib(nibName: "TrasmissionFilterCollectionCell", bundle: nil), forCellWithReuseIdentifier: "TrasmissionFilterCollectionCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set DataC
    func setData(model:CellModel) {
        
        self.cellModel = model
        
        if model.cellType == .FilterFualType {
            
            viewLineLeading.constant = 20
            viewLineTrailing.constant = 20

            lblTitle.isHidden = true
            lblHeight.constant = 0
            lblBottom.constant = 0
            
            viewLine.backgroundColor = ColorConstants.UnderlineColor.withAlphaComponent(0.80)
        } else {
            viewLineLeading.constant = 0
            viewLineTrailing.constant = 0

            lblTitle.isHidden = false
            lblHeight.constant = 19
            lblBottom.constant = 10

            viewLine.backgroundColor = ColorConstants.UnderlineColor

        }
        
        print(model.placeholder)
        
        lblTitle.text = model.placeholder
        
        if cellModel.cellType == .FilterChildSeats {
            if let arr = model.dataArr as? [MasterModel] {
                arrMaster = arr
            }

        } else {
            if let arr = model.dataArr as? [CellModel] {
                arrData = arr
            }

        }
               
        if arrData.count > 0  || arrMaster.count > 0{
            
            if model.cellType == .FilterChildSeats {
                if (arrMaster.count % 3) == 0 {
                    collectionViewHeight.constant = CGFloat(((arrMaster.count / 3)*35))
                }
                else {
                    collectionViewHeight.constant = CGFloat((((arrMaster.count / 3) + 1)*35))
                }
            } else {
                if (arrData.count % 2) == 0 {
                    collectionViewHeight.constant = CGFloat(((arrData.count / 2)*35))
                }
                else {
                    collectionViewHeight.constant = CGFloat((((arrData.count / 2) + 1)*35))
                }
            }
        }
        else {
            collectionViewHeight.constant = 0
        }

        
        collectionView.reloadData()
    }
}

//MARK: - UICollectionView's DataSource & Delegate Methods
extension TrasmissionFilterCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if cellModel.cellType == .FilterChildSeats {
            return arrMaster.count
        } else {
            return arrData.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrasmissionFilterCollectionCell", for: indexPath) as? TrasmissionFilterCollectionCell
        if cellModel.cellType == .FilterChildSeats {
            let model = arrMaster[indexPath.row]
            cell?.setData(cellModel: nil, masterModel: model)

        } else {
            let model = arrData[indexPath.row]

            cell?.setData(cellModel: model, masterModel: nil)
        }
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if cellModel.cellType == .FilterChildSeats {
            return CGSize(width: (collectionView.bounds.width/3), height: 35)
        } else {
            return CGSize(width: (collectionView.bounds.width/2), height: 35)
        }
//        return CGSize(width: (collectionView.bounds.width/2)-7.5, height: collectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if cellModel.cellType == .FilterChildSeats {
            let model = arrMaster[indexPath.row]
            model.isSelected = !model.isSelected
            collectionView.reloadData()

        } else {
            let model = arrData[indexPath.row]
            model.isSelected = !model.isSelected
            collectionView.reloadData()

        }
    }
}
