//
//  Category_FilterTCell.swift
//  WeOne
//
//  Created by Dev's Mac on 05/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Category_FilterTCell: UITableViewCell {

    @IBOutlet weak var tick_Image: UIImageView!
    @IBOutlet weak var item_Label: UILabel!
    @IBOutlet weak var car_View: ViewDesign!
    @IBOutlet weak var number_Lbl: UILabel!
    @IBOutlet weak var car_Image: UIImageView!
    @IBOutlet weak var age_Lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
