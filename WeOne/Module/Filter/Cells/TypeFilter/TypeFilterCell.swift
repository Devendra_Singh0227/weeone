//
//  TypeFilterCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 03/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class TypeFilterCell: UITableViewCell {

    //MARK: - Variables
    var arrMaster : [MasterModel] = [MasterModel]()
    
    //MARK: - Outlets    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        
        collectionView.register(UINib(nibName: "TypeFilterCollectionCell", bundle: nil), forCellWithReuseIdentifier: "TypeFilterCollectionCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        
        if let arr = model.dataArr as? [MasterModel] {
            arrMaster = arr
        }
        
        if arrMaster.count > 0 {
            if (arrMaster.count % 3) == 0 {
                collectionViewHeight.constant = CGFloat(((arrMaster.count / 3)*101))
            }
            else {
                collectionViewHeight.constant = CGFloat((((arrMaster.count / 3) + 1)*101))
            }
        }
        else {
            collectionViewHeight.constant = 0
        }
        
        
        collectionView.reloadData()
    }
}

//MARK: - UICollectionView's DataSource & Delegate Methods
extension TypeFilterCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrMaster.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let model = arrMaster[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TypeFilterCollectionCell", for: indexPath) as? TypeFilterCollectionCell
        cell?.setData(model: model)
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        return CGSize(width: (collectionView.bounds.width/3 - 7.2), height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let model = arrMaster[indexPath.row]
        model.isSelected = !model.isSelected
        collectionView.reloadData()
    }
}
