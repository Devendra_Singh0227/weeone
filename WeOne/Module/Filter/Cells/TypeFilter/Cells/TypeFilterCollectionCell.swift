//
//  TypeFilterCollectionCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 03/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class TypeFilterCollectionCell: UICollectionViewCell {

    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var vwBg: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgCheck.isHidden = true
    }

    
    //MARK: Set Data
    func setData(model: MasterModel) {
        
        if model.isSelected {
            vwBg.backgroundColor = ColorConstants.ThemeColor
            lblTitle.textColor = UIColor.white
        }
        else {
            vwBg.backgroundColor = ColorConstants.UnderlineColor.withAlphaComponent(0.30)
            lblTitle.textColor = ColorConstants.TextColorPrimary
        }
        
        lblTitle.text = model.name
        
        if model.isSelected {
            
            if let url = URL(string : (AppConstants.imageURL + (model.image ?? ""))){
                imgView.setImageForURL(url: url, placeHolder: UIImage(named : "Placeholder"))
            }else{
                imgView.image = UIImage(named : "Placeholder")
            }
        }
        else{
            
            if let url = URL(string : (AppConstants.imageURL + (model.icon ?? ""))){
                imgView.setImageForURL(url: url, placeHolder: UIImage(named : "Placeholder"))
            }else{
                imgView.image = UIImage(named : "Placeholder")
            }
        }
    }
}
