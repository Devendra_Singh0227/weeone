//
//  FuelChargeFilterCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 03/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class FuelChargeFilterCell: UITableViewCell, RangeSeekSliderDelegate{
    
    //MARK: - Variables
    var cellModel : CellModel = CellModel()
    
    //MARK: - Outlets    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var slider: RangeSeekSlider!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        configureSlider()
        slider.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func configureSlider() {
        
        slider.minLabelColor = UIColor.black.withAlphaComponent(0.65)
        slider.maxLabelColor = UIColor.black.withAlphaComponent(0.65)
        slider.handleColor = ColorConstants.TextColorTheme
        slider.tintColor = ColorConstants.TextColorTheme
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        
        cellModel = model
        lblTitle.text = model.placeholder
        
        if (model.userText ?? "").count > 0{
            slider.selectedMinValue = CGFloat(Int(model.userText ?? "0")!)
        }else{
            slider.selectedMinValue = 0
        }
        
        if (model.userText1 ?? "").count > 0{
            slider.selectedMaxValue = CGFloat(Int(model.userText1 ?? "0")!)
        }else{
            slider.selectedMaxValue = 100
        }
        
        slider.layoutSubviews()
    }
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        cellModel.userText = (Int(minValue)).description
        cellModel.userText1 = (Int(maxValue)).description
    }
}
