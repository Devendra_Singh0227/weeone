//
//  BrandTagCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 09/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class BrandTagCell: UITableViewCell {

    //MARK: - Variables
    var arrMaster : [MasterModel] = [MasterModel] ()
    
    //MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tagList: TagListView!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        //setupUIForTagList()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupUIForTagList() {
        
       // tagList.textFont = FontScheme.kRegularFont(size: 12)
        tagList.tagBackgroundColor = UIColor.white
        tagList.tagSelectedBackgroundColor = ColorConstants.Filter.FilterSelectedTypeBGColor
        tagList.alignment = .left
        
        tagList.borderColor = ColorConstants.Filter.FilterBorderBGColor
        tagList.borderWidth = 1
        tagList.textColor = ColorConstants.TextTitleBlack
        tagList.selectedTextColor = ColorConstants.Payment.PaymentTextPurple
        tagList.delegate = self
        tagList.cornerRadius = 10
        
        
        tagList.tagViews.forEach {
            $0.cornerRadius = $0.bounds.size.height / 2
        }
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        
        lblTitle.text = model.placeholder
        
        if let arrData = model.dataArr as? [MasterModel] {
            
            arrMaster = arrData
            tagList.removeAllTags()
            for item in arrData{
                let tagView = tagList.addTag(item.name ?? "")
                tagView.isSelected = item.isSelected
            }
        }
        
        tagList.tagViews.forEach {
            $0.cornerRadius = $0.bounds.size.height / 2
        }
    }
}

//MARK:- TagList View Delegate's Methods
extension BrandTagCell : TagListViewDelegate {
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        tagView.isSelected = !tagView.isSelected
        
        arrMaster.filter( { $0.name == title}).first?.isSelected = tagView.isSelected
    }
}
