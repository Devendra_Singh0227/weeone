//
//  AddCardViewController.swift
//  E-Scooter
//
//  Created by iMac on 17/09/19.
//  Copyright © 2019 CORUSCATEMAC. All rights reserved.
//

import UIKit

class AddCardViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var viewCardNumber: UIView!
    @IBOutlet weak var viewCardHolder: UIView!
    @IBOutlet weak var viewExpiryDate: UIView!
    @IBOutlet weak var viewCVV: UIView!

    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtCardHolder: UITextField!
    @IBOutlet weak var txtExpiryDate: UITextField!
    @IBOutlet weak var txtCVV: UITextField!
    
    @IBOutlet weak var btnAdd: UIButtonCommon!
    @IBOutlet weak var btnExpQue: UIButton!
    @IBOutlet weak var btnCVVQue: UIButton!

    @IBOutlet weak var viewNavigation: UIViewCommon!

    @IBOutlet var viewCards: [UIView]!
        
    
    //MARK: - Variables
    var addClick: (() ->())?
    var isFromAuth : Bool = false
    var selectedRow : Int = 0
    var arrCardTypes : [String] = ["KLblPersonalCard".localized, "KLblJoCard".localized]
    var cardType : Int = AppConstants.CardTypes.PersonalCard
    
    var arrCountries = [String]()
    
    //MARK: - View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        btnAddClick()
        viewNavigation.click = {
            self.btnBackTapped()
        }
        
    }
    
    func setupUI() {
        
        btnCVVQue.setImage(UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Question")), for: .normal)
        btnExpQue.setImage(UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Question")), for: .normal)

        txtCardNumber.tintColor = ColorConstants.ThemeColor
        txtCardHolder.tintColor = ColorConstants.ThemeColor
        txtExpiryDate.tintColor = ColorConstants.ThemeColor
        txtCVV.tintColor = ColorConstants.ThemeColor
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        Utilities.dismissToolTip(views: appDelegate.window!.subviews)

    }
    
    @objc func textFieldValueChanged(_ textField: UITextField) {
        
//        [lblCardNumer, lblCardHolder, lblExpiryDate, lblCVV].first(where: { $0.tag == textField.tag })?.text = textField.text
    }

    func checkValidation() -> Bool {
        
        let currentYear = DateUtilities.convertStringFromDate(date: Date(), format: "yy")
        let currentMonth = DateUtilities.convertStringFromDate(date: Date(), format: "MM")
        
        if txtCardNumber.text!.trimmed().isEmpty || txtCardNumber.text!.count < 16 {
            self.showToast(text: StringConstants.MakePayment.KLblEnterValidCardNumber)

            return false
            
        }
        else if txtCardHolder.text!.trimmed().isEmpty  {
            self.showToast(text: StringConstants.MakePayment.KLblEnterHolderName)

            return false
            
        }
        else if txtExpiryDate.text?.count == 0 {
            self.showToast(text: StringConstants.MakePayment.KLblEnterExpiryDate)
            return false
        }
        else if txtExpiryDate.text?.count ?? 0 < 5 {
            self.showToast(text: StringConstants.MakePayment.KLblEnterValidExpiryDate)
            return false
        } else if Int((txtExpiryDate.text ?? "" ).components(separatedBy: "/").first!) ?? 0 > 12 ||  Int((txtExpiryDate.text ?? "" ).components(separatedBy: "/").first!) ?? 0 == 0{
            self.showToast(text: StringConstants.MakePayment.KLblEnterValidExpiryDate)
            return false
            
        }
        else if Int((txtExpiryDate.text ?? "" ).components(separatedBy: "/").last!) ?? 0 < Int(currentYear) ?? 0{
            self.showToast(text: StringConstants.MakePayment.KLblEnterValidExpiryDate)
            return false
            
        }
        else if (Int((txtExpiryDate.text ?? "" ).components(separatedBy: "/").last!) ?? 0) == (Int(currentYear) ?? 0) && !((Int((txtExpiryDate.text ?? "" ).components(separatedBy: "/").first!) ?? 0) > (Int(currentMonth) ?? 0)) {
            self.showToast(text: StringConstants.MakePayment.KLblEnterValidExpiryDate)
            return false
        }
        else if txtCVV.text?.count == 0 {
            self.showToast(text: StringConstants.MakePayment.KLblEnterCVVCode)
            return false
            
        }
        else if txtCVV.text?.count ?? 0 < 3 {
            self.showToast(text: StringConstants.MakePayment.KLblEnterValidCVVCode)
            return false
        }
        
        return true
    }
    
    func btnAddClick() {
        btnAdd.click = {
            self.view.endEditing(true)
            if self.checkValidation() {
                
                self.validateCard()
            }
        }
    }

    func btnBackTapped() {
            self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnExpiryHelp_Click(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        Utilities.dismissToolTip(views: appDelegate.window!.subviews)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            Utilities.showPopOverView(str: "KMsgExp".localized, arrowSize: CGSize(width: 9, height: 9), maxWidth: 250, x: 27, sender: sender)
        })

    }
    
    @IBAction func btnCvvHelp_Click(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        Utilities.dismissToolTip(views: appDelegate.window!.subviews)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            Utilities.showPopOverView(str: "KMsgCVV".localized, arrowSize: CGSize(width: 9, height: 9), maxWidth: 250, x: 10, sender: sender)
        })
    }
}

//MARK: - Textfield Delegate
extension AddCardViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let result = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string

        if textField == txtCardNumber {
            
            
            let spaceOffset = 5
            
            if range.location == 19 {
                return false
            }
            
            if range.length == 1 {
                if (range.location == 5 || range.location == 10 || range.location == 15) {
                    let text = textField.text ?? ""
                    textField.text = String(text[..<text.index(before: text.endIndex)])
                    String(text[..<text.index(text.startIndex, offsetBy: spaceOffset)])
                }
                return true
            }
            
            if (range.location == 4 || range.location == 9 || range.location == 14) {
                textField.text = String(format: "%@ ", textField.text ?? "")
            }
            
                        
            return true
        } else if textField == txtCardHolder {
            return UserInputValidator.nameOnlyWithLength(text: textField.text!, range: range, replacementString: string, maxLength: nil)
        } else if textField == txtExpiryDate {
            
            if range.length > 0 {
                if range.location == 3 {
                    var originalText = textField.text
                    originalText = originalText?.replacingOccurrences(of: "/", with: "")
                    textField.text = originalText
                }
                return true
            }
            
            if string == " " {
                return false
            }
            
            if range.location >= 5 {
                return false
            }
            
            var originalText = textField.text
            
            if range.location == 2 {
                originalText?.append("/")
                textField.text = originalText
            }
            return true
        }else if textField == txtCVV {
            
            if range.location == 4 {
                return false
            }
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtCardNumber {
            viewCardNumber.backgroundColor = ColorConstants.ThemeColor
            viewExpiryDate.backgroundColor = ColorConstants.UnderlineColor
            viewCVV.backgroundColor = ColorConstants.UnderlineColor
            viewCardHolder.backgroundColor = ColorConstants.UnderlineColor

        } else if textField == txtExpiryDate {
            viewExpiryDate.backgroundColor = ColorConstants.ThemeColor
            viewCardNumber.backgroundColor = ColorConstants.UnderlineColor
            viewCVV.backgroundColor = ColorConstants.UnderlineColor
            viewCardHolder.backgroundColor = ColorConstants.UnderlineColor

        } else if textField == txtCVV {
            viewCVV.backgroundColor = ColorConstants.ThemeColor
            viewCardHolder.backgroundColor = ColorConstants.UnderlineColor
            viewCardNumber.backgroundColor = ColorConstants.UnderlineColor
            viewExpiryDate.backgroundColor = ColorConstants.UnderlineColor

        } else if textField == txtCardHolder {
            viewCardHolder.backgroundColor = ColorConstants.ThemeColor
            viewCVV.backgroundColor = ColorConstants.UnderlineColor
            viewCardNumber.backgroundColor = ColorConstants.UnderlineColor
            viewExpiryDate.backgroundColor = ColorConstants.UnderlineColor
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtCardNumber {
            textField.isUserInteractionEnabled = true
        }
    }
    
}

//MARK: API Calling
extension AddCardViewController {

    func validateCard() {
        
        let param = STPCardParams()
        param.cvc = txtCVV.text
        param.expMonth = UInt((txtExpiryDate.text ?? "" ).components(separatedBy: "/").first!) ?? 0
        param.expYear = UInt((txtExpiryDate.text ?? "" ).components(separatedBy: "/").last!) ?? 0
        param.number = txtCardNumber.text
        param.name = txtCardHolder.text
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        STPAPIClient.shared().createToken(withCard: param) { (token, error) in
            NetworkClient.sharedInstance.stopIndicator()
            
            if error != nil{
                
                self.view.showConfirmationPopupWithSingleButton(title: "", message: error?.localizedDescription ?? "", confirmButtonTitle: StringConstants.ButtonTitles.KOk, onConfirmClick: {
                })
                
                return
            }
            
            self.callApiToAddCard(token: token!.tokenId)
        }
    }
    
    func callApiToAddCard(token : String) {
        
        let param : [String:Any] = ["cardToken" : token, "cardType" : cardType]
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.AddCard, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message ) in
            
            print(response)
            if let dictResponse = response as? [String:Any] {
                SyncManager.sharedInstance.insertCard(dict: dictResponse)
                self.addClick?()
                if self.isFromAuth {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "updateData"), object: nil, userInfo: nil)
                    if AppNavigation.shared.checkUserHasAccess(vc: self) {
                        self.navigationController?.popViewController(animated: true)
                    }
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}

//MARK:- Extra Methods
extension AddCardViewController {
    
    convenience init() {
        self.init(nibName: "AddCardViewController", bundle: nil)
    }
}
