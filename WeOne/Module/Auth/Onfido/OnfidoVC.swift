//
//  OnfidoVC.swift
//  WeOne
//
//  Created by iMac on 21/06/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit
import Onfido

class OnfidoVC: UIViewController {
    
    var check = ""
    private var applicantId : String = ""
    private var token : String = ""
    var checktype = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //OnfidoHelper.shared.verifyDocument(vc: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if check == "" {
            
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func start_Verification_Btn(_ sender: ButtonDesign) {
        check = "move"
        if checktype == "" {
            //moveToOnfidofor_DL()
        } else {
            //moveToOnfidofor_Visa()
        }
        //generateApplicantId()
        let responseHandler: (OnfidoResponse) -> Void = { response in
            
            if case let OnfidoResponse.error(innerError) = response {
                // self.showErrorMessage(forError: innerError)
            } else if case OnfidoResponse.success = response {
                // SDK flow has been completed successfully. You may want to create a check in your backend at this point.
                // Follow https://github.com/onfido/onfido-ios-sdk#2-creating-a-check to understand how to create a check
                let alert = UIAlertController(title: "Success", message: "SDK flow has been completed successfully", preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "OK", style: .default, handler: { _ in })
                alert.addAction(alertAction)
                self.present(alert, animated: true)
            } else if case OnfidoResponse.cancel = response {
                let alert = UIAlertController(title: "Canceled", message: "Canceled by user", preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "OK", style: .default, handler: { _ in })
                alert.addAction(alertAction)
                self.present(alert, animated: true)
            }
        }
        
        // TODO Call your backend to get `sdkToken` https://github.com/onfido/onfido-ios-sdk#31-sdk-tokens
        let sdkToken: String = "SDK_TOKEN"
        
        let config = try! OnfidoConfig.builder()
            .withSDKToken(sdkToken)
            //.withWelcomeStep()
            .withDocumentStep(ofType: .visa(config: nil))
            //.withFaceStep(ofVariant: .photo(withConfiguration: nil))
            //.withDocumentStep(ofType: .drivingLicence(config: DrivingLicenceConfiguration(country: "GBR")))
            .build()
        
        let onfidoFlow = OnfidoFlow(withConfiguration: config)
            .with(responseHandler: responseHandler)
        
        do {
            let onfidoRun = try onfidoFlow.run()
            
            var modalPresentationStyle: UIModalPresentationStyle = .fullScreen  // due to iOS 13 you must specify .fullScreen as the default is now .pageSheet
            
            if UIDevice.current.userInterfaceIdiom == .pad {
                modalPresentationStyle = .formSheet // to present modally on iPads
            }
            
            onfidoRun.modalPresentationStyle = modalPresentationStyle
            self.present(onfidoRun, animated: true, completion: nil)
        } catch let error {
            // cannot execute the flow
            //self.showErrorMessage(forError: error)
        }
    }
    
    private func generateApplicantId() {
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.CreateApplicant, method: .post, parameters: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any]{
                self.applicantId = dictResponse["applicantId"] as? String ?? ""
            }
            self.generateToken()
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    //Generate Token
    private func generateToken(){
        
        getToken {
            //self.moveToOnfido()
        }
    }
    
    //Get Token
    private func getToken(success:(()->Void)?){
        
        var req = [String:Any]()
        req["applicantId"] = applicantId
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.GenerateToken, method: .post, parameters: req, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any]{
                self.token = dictResponse["token"] as? String ?? ""
            }
            success?()
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    //Call VerifyDocument
    func callVerifyDocumentApi() {
        
        var req = [String:Any]()
        req["applicantId"] = applicantId
        req["userId"] = ApplicationData.user?.id
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.CreateOnfidoCheck, method: .post, parameters: req, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any]{
                ApplicationData.sharedInstance.saveUserData(dictResponse)
            }
            
            //let a = AppNavigation.shared.checkUserHasAccess(vc: self.vc)
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func moveToOnfidofor_DL() {
        let responseHandler: (OnfidoResponse) -> Void = { response in
            
            if case let OnfidoResponse.error(innerError) = response {
                print(innerError)
                // self.showErrorMessage(forError: innerError)
            } else if case OnfidoResponse.success = response {
                //self.callVerifyDocumentApi()
                print(response)
            } else if case OnfidoResponse.cancel = response {
                
            }
        }
        
        // TODO Call your backend to get `sdkToken`
        
        let config = try! OnfidoConfig.builder()
            .withSDKToken(token)
            .withFaceStep(ofVariant: .photo(withConfiguration: nil))
            .withDocumentStep(ofType: .drivingLicence(config: DrivingLicenceConfiguration(country: "GBR")))
            .build()
        
        let onfidoFlow = OnfidoFlow(withConfiguration: config)
            .with(responseHandler: responseHandler)
        
        do {
            let onfidoRun = try onfidoFlow.run()
            
            var modalPresentationStyle: UIModalPresentationStyle = .fullScreen  // due to iOS 13 you must specify .fullScreen as the default is now .pageSheet
            
            if UIDevice.current.userInterfaceIdiom == .pad {
                modalPresentationStyle = .formSheet // to present modally on iPads
            }
            
            onfidoRun.modalPresentationStyle = modalPresentationStyle
            self.present(onfidoRun, animated: true, completion: nil)
        } catch let error {
            print(error)
            // cannot execute the flow
            //self.showErrorMessage(forError: error)
        }
    }
    
    func moveToOnfidofor_Visa() {
        let responseHandler: (OnfidoResponse) -> Void = { response in
            
            if case let OnfidoResponse.error(innerError) = response {
                print(innerError)
                // self.showErrorMessage(forError: innerError)
            } else if case OnfidoResponse.success = response {
                //self.callVerifyDocumentApi()
                print(response)
            } else if case OnfidoResponse.cancel = response {
                
            }
        }
        
        // TODO Call your backend to get `sdkToken`
        
        let config = try! OnfidoConfig.builder()
            .withSDKToken(token)
            .withDocumentStep(ofType: .visa(config: nil))
            .build()
        
        let onfidoFlow = OnfidoFlow(withConfiguration: config)
            .with(responseHandler: responseHandler)
        
        do {
            let onfidoRun = try onfidoFlow.run()
            
            var modalPresentationStyle: UIModalPresentationStyle = .fullScreen  // due to iOS 13 you must specify .fullScreen as the default is now .pageSheet
            
            if UIDevice.current.userInterfaceIdiom == .pad {
                modalPresentationStyle = .formSheet // to present modally on iPads
            }
            
            onfidoRun.modalPresentationStyle = modalPresentationStyle
            self.present(onfidoRun, animated: true, completion: nil)
        } catch let error {
            print(error)
            // cannot execute the flow
            //self.showErrorMessage(forError: error)
        }
    }
}
