//
//  CustomeCameraVC.swift
//  E-Scooter
//
//  Created by MacOs14 on 17/05/19.
//  Copyright © 2019 CORUSCATEMAC. All rights reserved.
//

import UIKit
import AVKit
import MobileCoreServices
import Photos
import TensorFlowLite
import TensorFlowLiteC

class CustomeCameraVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var viewCamera: UIView!
    @IBOutlet weak var viewCapturedImage: UIView!

    @IBOutlet weak var viewCapturedBottom: UIView!
    @IBOutlet weak var viewCameraBottom: UIView!
    
    @IBOutlet weak var viewFlash: UIStackView!

    @IBOutlet weak var viewPermissionDenied: ShadowCard!
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgCapture: UIImageView!
    @IBOutlet weak var btnFlashOff: UIButton!
    @IBOutlet weak var btnFlashAuto: UIButton!
    @IBOutlet weak var btnFlashOn: UIButton!

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnFlip: UIButton!
    @IBOutlet weak var btnGallery: UIButton!

    @IBOutlet weak var btnTick: UIButton!

    @IBOutlet weak var lblName: UILabel!
    
    //MARK: Variables
    var imagePickers : UIImagePickerController?
    private var modelDataHandler: ModelDataHandler? =
      ModelDataHandler(modelFileInfo: MobileNet.modelInfo, labelsFileInfo: MobileNet.labelsInfo)
    private var result: Result?

    var arrCarData = ["freight car", "passenger car", "sports car", "streetcar"]

    var btnClose = UIButton()
    var isFromUploadDrivingLic = false
    var isImagePicked : ((_ info : [UIImagePickerController.InfoKey : Any]) -> ())?
    var infoDict : [UIImagePickerController.InfoKey : Any] = [:]
    
    var captureSession: AVCaptureSession!
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var vdo : AVCaptureVideoDataOutput!

    var usingFrontCamera = false
    
    var isFromProfile = false

    var flashMode = AVCaptureDevice.FlashMode.off
    var checkScreen = ""
    
    //MARK: View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
        if isFromUploadDrivingLic {
            checkCameraPermission()
        } else {
//            if self.captureSession != nil {
//                self.captureSession.startRunning()
//            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.captureSession.stopRunning()
    }
        
    override func viewDidLayoutSubviews() {
        DispatchQueue.main.async {
            if self.videoPreviewLayer != nil {
                self.videoPreviewLayer.frame = self.viewCamera.bounds
            }
        }
    }
    //MARK: initial Config
    func initialConfig(){
        
        imgCapture.image = UIImage(named : ThemeManager.sharedInstance.getImage(string: "Capture"))
        btnTick.setImage(UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Tick")), for: .normal)

        viewPermissionDenied.isHidden = true
       // viewCapturedImage.isHidden = true
        btnFlashOff.isSelected = true

        if isFromProfile {
            btnGallery.setImage(UIImage(named: "gallery"), for: .normal)
            btnFlip.setImage(UIImage(named: "flipcam"), for: .normal)
//            btnFlip.isHidden = true
//            btnGallery.isHidden = true
        } else {
            lblName.isHidden = true
        }
        checkCameraPermission()
        
    }
    
    @objc func zoomLevelChanges(_ sender: UISlider) {
        print(sender.value)
        let device = Utilities.getCaptureDevice()
        do {
            try device?.lockForConfiguration()
            defer { device?.unlockForConfiguration() }
            device?.videoZoomFactor = CGFloat(Int(sender.value))

        } catch {
            debugPrint(error)
        }
    }
    
    func displayStringsForResults(atRow row: Int) -> (String) {

      var fieldName: String = ""
      var info: String = ""

      guard let tempResult = result, tempResult.inferences.count > 0 else {

        if row == 1 {
          fieldName = "No Results"
          info = ""
        }
        else {
          fieldName = ""
          info = ""
        }
        return (fieldName)
      }

      let maxAge = tempResult.inferences.map { $0.confidence * 100.0}.max() // 8

      print(tempResult.inferences)
      print(maxAge)
      
      for result in tempResult.inferences {
          if result.confidence == maxAge {
              info =  String(format: "%.2f", result.confidence * 100.0) + "%"
              if arrCarData.contains(result.label) {
                  return (result.label)
              } else {
                  return ("Not car")
              }
          }
      }
      
      
      if row < tempResult.inferences.count {
                    
        let inference = tempResult.inferences[row]
        fieldName = inference.label
        info =  String(format: "%.2f", inference.confidence * 100.0) + "%"
          
          print("info\(inference.confidence * 100.0)")
      }
      else {
        fieldName = ""
        info = ""
      }

      return (fieldName)
    }
    
    //MARK: Button action methods
    @IBAction func btnCamera_Click(_ sender: UIButton) {
                
        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        settings.flashMode = flashMode
        stillImageOutput.capturePhoto(with: settings, delegate: self)
    }
    
    @IBAction func btnGallery_Click(_ sender: UIButton) {
        if sender.currentImage == UIImage(named: "gallery") {
            checkGallaryPermission()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnFlip_Click(_ sender: UIButton) {
        if sender.currentImage == UIImage(named: "flipcam") {
            swapCamera()
        } else {
            self.navigationController?.popViewController(animated: true)
            self.isImagePicked?(infoDict)
        }
    }
    
    @IBAction func btnFlashOn_Click(_ sender: UIButton) {
        
        btnFlashOn.isSelected = true
        btnFlashOff.isSelected = false
        btnFlashAuto.isSelected = false
        
        flashMode = AVCaptureDevice.FlashMode.on
    }
    
    @IBAction func btnFlashOff_Click(_ sender: UIButton) {
        
        btnFlashOn.isSelected = false
        btnFlashOff.isSelected = true
        btnFlashAuto.isSelected = false
        flashMode = AVCaptureDevice.FlashMode.off
    }

    @IBAction func btnFlashAuto_Click(_ sender: UIButton) {
        
        btnFlashOn.isSelected = false
        btnFlashOff.isSelected = false
        btnFlashAuto.isSelected = true
        flashMode = AVCaptureDevice.FlashMode.auto
    }

    @IBAction func btnOpenSetting_Click(_ sender: UIButton) {
        OpenApplicationSettings()
    }
    
    @IBAction func btnClose_Click(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCloseCapturedImage_Clice(_ sender: UIButton) {
        
        //hideShowView(viewCapturedImage: true, viewCapturedBottom: true, viewCamera: false, viewCameraBottom: false, btnBack: false, viewFlash: false)
    }
    
    @IBAction func btnDone_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.isImagePicked?(infoDict)
    }
    
    func btnDoneClick() {
        if infoDict.count == 0{
            Utilities.showAlertView(message: StringConstants.UploadLicense.KLblPickOrClickImage)
        } else {
            self.navigationController?.popViewController(animated: true)
            self.isImagePicked?(infoDict)
        }
    }
    
    func OpenApplicationSettings(){
        if let url = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(url, options: [:], completionHandler: { _ in

            })
        }
    }
    
    func hideShowView(viewCapturedImage: Bool, viewCapturedBottom: Bool, viewCamera: Bool, viewCameraBottom: Bool, btnBack: Bool, viewFlash: Bool) {
        self.viewCapturedImage.isHidden = viewCapturedImage
        self.viewCapturedBottom.isHidden = viewCapturedBottom
        self.viewCamera.isHidden = viewCamera
        self.viewCameraBottom.isHidden = viewCameraBottom
        
        self.btnBack.isHidden = btnBack
        self.viewFlash.isHidden = viewFlash
    }
}

//MARK: Private methods
extension CustomeCameraVC{
    func checkCameraPermission() {
               
           if AVCaptureDevice.authorizationStatus(for: .video) == .authorized{
               self.setupCameraView()
           }else if AVCaptureDevice.authorizationStatus(for: .video) == .notDetermined {
               AVCaptureDevice.requestAccess(for: .video) { success in
                   print(success)
                   if success {
                       DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
                            self.setupCameraView()
                       })
                   } else {
                       DispatchQueue.main.async {
                           self.viewPermissionDenied.isHidden = false
                       }
                   }
               }
           }
       }
        
    func checkGallaryPermission(){
        let status = PHPhotoLibrary.authorizationStatus()
        
        if (status == PHAuthorizationStatus.authorized) {
            Utilities.openGallery(self, mediaType: [kUTTypeImage as String])
        }
            
        else if (status == PHAuthorizationStatus.denied) {
            
            UIViewController.current()?.view.showConfirmationPopupWithMultiButton(title: "", message: StringConstants.UploadLicense.KLblGalleryPermission, cancelButtonTitle: StringConstants.ButtonTitles.KCancel, confirmButtonTitle: StringConstants.ButtonTitles.KOk, onConfirmClick: {
                // confirm
                self.OpenApplicationSettings()
                
            }, onCancelClick: {
                // cancel
            })
        }
            
        else if (status == PHAuthorizationStatus.notDetermined) {
            
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                
                if (newStatus != PHAuthorizationStatus.authorized) {
                    
                    UIViewController.current()?.view.showConfirmationPopupWithMultiButton(title: "", message: StringConstants.UploadLicense.KLblGalleryPermission, cancelButtonTitle: StringConstants.ButtonTitles.KCancel, confirmButtonTitle: StringConstants.ButtonTitles.KOk, onConfirmClick: {
                        // confirm
                        self.OpenApplicationSettings()
                        
                    }, onCancelClick: {
                        // cancel
                    })
                }
            })
        }
    }
    
    func addImagePickerToContainerView() {
        
        self.viewPermissionDenied.isHidden = true
        
        if UIImagePickerController.isCameraDeviceAvailable(.rear) {
            imagePickers = UIImagePickerController()

            imagePickers?.delegate = self
            imagePickers?.sourceType = .camera
            imagePickers?.cameraCaptureMode = .photo

            addChild(imagePickers!)
            
            // Add the child's View as a subview
            imagePickers?.view.frame = viewCamera.bounds
            imagePickers?.cameraFlashMode = .off
            imagePickers?.allowsEditing = false
            imagePickers?.showsCameraControls = false
            imagePickers?.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            viewCamera.addSubview((imagePickers?.view)!)

            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.imagePickers?.view.layoutIfNeeded()
                self.viewCamera.layoutIfNeeded()

                self.view.layoutIfNeeded()
            }
        }else{
            Utilities.showAlertView(message: StringConstants.UploadLicense.KLblCameraNotAvailable)
        }
    }
    
    fileprivate func swapCamera() {

        // Get current input
        guard let input = captureSession.inputs[0] as? AVCaptureDeviceInput else { return }

        // Begin new session configuration and defer commit
        captureSession.beginConfiguration()
        defer { captureSession.commitConfiguration() }

        // Create new capture device
        var newDevice: AVCaptureDevice?
        if input.device.position == .back {
            newDevice = captureDevice(with: .front)
            viewFlash.isHidden = true
            btnFlashOff.isSelected = true
            flashMode = AVCaptureDevice.FlashMode.off
        } else {
            newDevice = captureDevice(with: .back)
             viewFlash.isHidden = false
        }

        // Create new capture input
        var deviceInput: AVCaptureDeviceInput!
        do {
            deviceInput = try AVCaptureDeviceInput(device: newDevice!)
        } catch let error {
            print(error.localizedDescription)
            return
        }

        // Swap capture device inputs
        captureSession.removeInput(input)
        captureSession.addInput(deviceInput)
    }

    /// Create new capture device with requested position
    fileprivate func captureDevice(with position: AVCaptureDevice.Position) -> AVCaptureDevice? {

        let devices = AVCaptureDevice.DiscoverySession(deviceTypes: [ .builtInWideAngleCamera, .builtInMicrophone, .builtInDualCamera, .builtInTelephotoCamera ], mediaType: AVMediaType.video, position: .unspecified).devices

        //if let devices = devices {
            for device in devices {
                if device.position == position {
                    return device
                }
            }
        //}

        return nil
    }

    func setupCameraView() {
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .medium

        guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
            else {
                print("Unable to access back camera!")
                return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            stillImageOutput = AVCapturePhotoOutput()
            
            if captureSession.canAddInput(input) && captureSession.canAddOutput(stillImageOutput) {
                captureSession.addInput(input)
                captureSession.addOutput(stillImageOutput)
                
                vdo = AVCaptureVideoDataOutput()
                vdo.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString) as String: kCVPixelFormatType_32BGRA]

                setupLivePreview()
            }
        }
        catch let error  {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
    }
        
    func setupLivePreview() {
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.connection?.videoOrientation = .portrait
        viewCamera.layer.addSublayer(videoPreviewLayer)
        
        DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
            
            let queue = DispatchQueue(label: "myqueue")
            self.vdo!.setSampleBufferDelegate(self, queue: queue)
            
            self.vdo.alwaysDiscardsLateVideoFrames = true
            
            if self.captureSession.canAddOutput(self.vdo) {
                self.captureSession.addOutput(self.vdo)
            } else {
                return
            }
            
            self.captureSession.automaticallyConfiguresApplicationAudioSession = false
            self.captureSession.usesApplicationAudioSession = true
            
            self.captureSession.startRunning()
            //Step 13
        }
    }
}
 
//MARK: UIImagepicker view delegate methods
extension CustomeCameraVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
                
       // hideShowView(viewCapturedImage: false, viewCapturedBottom: false, viewCamera: true, viewCameraBottom: true, btnBack: true, viewFlash: true)
        btnGallery.setImage(UIImage(named: "cameraClose"), for: .normal)
        btnFlip.setImage(UIImage(named: "click"), for: .normal)
        infoDict = info
        if let image = info[.originalImage] as? UIImage{
            self.imgView.image = image
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

extension CustomeCameraVC: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard let imageData = photo.fileDataRepresentation()
            else { return }
        
       // hideShowView(viewCapturedImage: false, viewCapturedBottom: false, viewCamera: true, viewCameraBottom: true, btnBack: true, viewFlash: true)
        
        let image = UIImage(data: imageData)
        imgView.image = image

        infoDict = [UIImagePickerController.InfoKey.originalImage : image!]

//        viewCapturedImage.isHidden = false
        //btnCapture.setImage(UIImage(named : "retake"), for: .normal)
//        self.imgTick.isHidden = false

    }
}

extension CustomeCameraVC: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        let pixelBuffer: CVPixelBuffer? = CMSampleBufferGetImageBuffer(sampleBuffer)
        
        guard let imagePixelBuffer = pixelBuffer else {
            return
        }
        
        if !isFromProfile {
            result = modelDataHandler?.runModel(onFrame: imagePixelBuffer)
            
            if let tempResult = result, tempResult.inferences.count > 0 {
                
                let maxAge = tempResult.inferences.map { $0.confidence * 100.0}.max() // 8
                
//                print(tempResult.inferences)
//                print(maxAge ?? 0.0)
//
                for result in tempResult.inferences {
                    //                if (result.confidence * 100.0) == maxAge {
//                    print(result.label)
                    
                    DispatchQueue.main.async {
                        if self.arrCarData.contains(result.label) {
                            self.lblName.text = result.label
                        } else {
                            self.lblName.text = "Not car"
                        }
                        //                    }
                    }
                }
            }
        } else {
            self.lblName.isHidden = true 
        }
    }
}
