//
//  ForgotPasswordVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 28/09/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit
import PhoneNumberKit

class ForgotPasswordVC: UIViewController {
    
    //MARK: - Variables
    var strEmail  :String?
    
    var arrCountries = [String]()
    var arrCountryCodes = [String]()

    var selectedCountry: Int? {
        didSet {
            if let index = selectedCountry {
                //KD lblCountryCode.text = self.arrCountryCodes[index]

                txtPhoneNumber.partialFormatter.defaultRegion = arrCountries[index].components(separatedBy: ":").last ?? ""

                let url = URL.init(string: "http://flagpedia.net/data/flags/normal/\(self.arrCountries[index].components(separatedBy: ":")[1].lowercased()).png")
                imgCountry.sd_setImage(with: url, completed: { (image, error, cache, url) in
                    })

            }
        }
    }

    //MARK: - Outlets
    @IBOutlet weak var imgLogo: UIImageView!

    @IBOutlet weak var txtEmail: UITextFiledCommon!
    @IBOutlet weak var txtPhoneNumber: PhoneNumberTextField!

    @IBOutlet weak var btnResetPassword: UIButtonCommon!

    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var viewLine: UIView!

    @IBOutlet weak var btnCountry: UIButton!
    @IBOutlet weak var btnVerify: UIButton!
    
    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var email_View: ViewDesign!
    @IBOutlet weak var email_TxtField: UITextField!
    @IBOutlet weak var error_Lbl: UILabel!

    var countryName = ""
    var isWithMobile = false
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        email_TxtField.delegate = self
        //initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }
    

    //MARK: Private Methods
    func initialConfig() {
        
        imgLogo.image = UIImage(named : ThemeManager.sharedInstance.getImage(string: "appLogo"))

        txtEmail.setText(text: strEmail)
        btnResetPassword.click = {
            self.btnResetPassword_Click()
        }
        
        getCountryData()
        
//        let attributedTitle = NSAttributedString(string: "KLblPhoneNumber".localized, attributes: [NSAttributedString.Key.kern: 0.64, NSAttributedString.Key.foregroundColor : ColorConstants.TextColorPrimary, NSAttributedString.Key.font : FontScheme.kLightFont(size: 16.0)])
//        txtPhoneNumber.attributedPlaceholder = attributedTitle

        txtPhoneNumber.tintColor = ColorConstants.ThemeColor
        txtPhoneNumber.delegate = self
//        txtPhoneNumber.heightViewLine.constant = 0
//        txtPhoneNumber.showEyeButton = false
//        txtPhoneNumber.textField.keyboardType = .numberPad
        
//        let attributedTitle = NSAttributedString(string: lblDesc.text ?? "", attributes: [NSAttributedString.Key.kern: 0.64, NSAttributedString.Key.foregroundColor : ColorConstants.TextColorPrimary, NSAttributedString.Key.font : FontScheme.kLightFont(size: 16.0)])
//        lblDesc.attributedText = attributedTitle

//        lblDesc.setAttributeStringWithLineSpacing(lineSpacing: 1.0, lineHeightMultiple: 1.5, spacing: 0.64, textColor: ColorConstants.TextColorPrimary, font: lblDesc.font)

    }
    
    func getCountryData(){
        arrCountries.removeAll()
        arrCountryCodes.removeAll()
        let arrCountry = Utilities.readJsonFile(fileName: "countryCodes")

        for i in 0..<arrCountry.count{
            let model = arrCountry[i]
            arrCountries.append("\(model["name"] as? String ?? ""):\(model["code"] as? String ?? "")")
            arrCountryCodes.append(model["dial_code"] as? String ?? "")
        }
        
        for i in 0..<arrCountry.count{
            
            let model = arrCountry[i]
            if ApplicationData.isUserLoggedIn{
                
//                if model["dial_code"] as? String == ApplicationData.user?.primaryMobile?.countryCode {
//                    self.selectedCountry = i
//                    self.countryName = arrCountryCodes[i]
                    break
//                }
                
            } else {
                if selectedCountry == nil{
                    if model["code"] as? String == Locale.current.regionCode ?? "" {
                        self.selectedCountry = i
                        self.countryName = arrCountryCodes[i]
                        break
                    }
                } else {
                    if model["dial_code"] as? String == self.countryName {
                        self.selectedCountry = i
                        self.countryName = arrCountryCodes[i]
                        break
                    }
                }
            }
        }
    }
    //MARK:- IBActions
    
    @IBAction func btnCountryListTapped(_ sender: UIButton) {
        
        //self.btnVerify.isHidden = true
                
        self.view.showSelectionPopup(topHeading: "KLblSelectCountry".localized, defaultSelectedIndex: self.selectedCountry , isFromCity: false, isFromSignup: true, isShowSearchBar: true, isShowSelectedImage: true, arrData: arrCountries, onSubmitClick: { (index) in
            
//            if ApplicationData.user?.primaryMobile?.mobile == self.txtPhoneNumber.text && ApplicationData.user?.primaryMobile?.countryCode == self.arrCountryCodes[index] {
//                self.btnVerify.isHidden = false
//            }else{
//                self.btnVerify.isHidden = true
//            }
            
//            self.cellModel?.userText1 = self.arrCountryCodes[index]
            self.selectedCountry = index
        })
    }
    func btnResetPassword_Click() {
        self.view.endEditing(true)
        
        if !Utilities.checkStringEmptyOrNil(str: txtPhoneNumber.text)  {
            let (validate, req) = getrequestAndValidate()
            if validate {
                isWithMobile = true
                callApiForForgotPassword(req: req)
            }
        } else {
            let (validate, req) = getrequestAndValidate()
            if validate {
                isWithMobile = false
                callApiForForgotPassword(req: req)
            }
        }

    }
    
    @IBAction func btnClose_Click(_ sender: UIButton) {
        self.view.endEditing(true)
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func moveToOtpVc(){
    }

    @IBAction func verify_Btn(_ sender: UIButton) {
        if email_TxtField.text == "" {
            email_View.BorderWidth = 2
            email_View.layer.borderColor = ColorConstants.TextColorError.cgColor
            error_Lbl.isHidden = false
        } else if !(email_TxtField.text!.isValidEmail()) {
            email_View.BorderWidth = 2
            email_View.layer.borderColor = ColorConstants.TextColorError.cgColor
            error_Lbl.isHidden = false
             //self.show_Toast(message: StringConstants.validationerror.validEmail)
        } else {
            let vc = Verify_Email_ViewController()
            vc.email = email_TxtField.text!
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK:- API Calling
extension ForgotPasswordVC {
    
    func callApiForForgotPassword(req:[String:Any]) {
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.ForgorPassword, method: .post, parameters: req, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if  self.isWithMobile {
                
                if let dictResponse = response as? [String:Any] {
                    let userInfo = Mapper<UserInfoModel>().mapArray(JSONArray: [dictResponse])
                    
                    let vc = ForgotPwdOTPVC(nibName: "ForgotPwdOTPVC", bundle: nil)
                    vc.isFromForgotPWD = true
                    vc.userModel = userInfo.first ?? UserInfoModel()
                    self.navigationController?.pushViewController(vc, animated: true)
                    if self.navigationController != nil{
                        for viewController in (self.navigationController?.viewControllers)!{
                            if viewController.isKind(of: ForgotPasswordVC.self) {
                                self.navigationController?.viewControllers.remove(at: (self.navigationController?.viewControllers.firstIndex(of: viewController))!)
                            }
                        }
                    }
                }
            } else {
                self.showAlert(msg: message) {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }) { (failureMessage, failureCode) in
            self.showAlert(msg: failureMessage, success: nil)
        }
    }
    
    func showAlert(msg : String,success:(()->Void)?) {
        
        let vc = self.view.returnConfirmationPopup(title: "", message:  msg, confirmButtonTitle: StringConstants.ButtonTitles.KOk, onConfirmClick: {
            success?()
        })
        
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: {
        })
    }
}


//MARK:- Extra Methods
extension ForgotPasswordVC {
    
    convenience init() {
        self.init(nibName: "ForgotPasswordVC", bundle: nil)
    }
    
    func getrequestAndValidate() -> (Bool, [String:Any]) {
        var req = [String:Any]()
        
        if txtPhoneNumber.text == "" {
            guard !Utilities.checkStringEmptyOrNil(str: txtEmail.getText()) else {
                self.showToast(text: "KLblEnterEmail".localized)

//                Utilities.showAlertView(message: "KLblEnterEmail".localized)
                return (false,[:])
            }
            
            guard txtEmail.getText()?.isValidEmail() == true else {
                self.showToast(text: "KLblEnterValidEmail".localized)

//                Utilities.showAlertView(message: "KLblEnterValidEmail".localized)
                return (false,[:])
            }

            req["email"] = txtEmail.getText()

        } else if txtEmail.text == "" {
            guard !Utilities.checkStringEmptyOrNil(str: txtPhoneNumber.text) else {
                self.showToast(text: "KLblEnterEmail".localized)

//                Utilities.showAlertView(message: "KLblEnterEmail".localized)
                return (false,[:])
            }
            
            req["mobile"] = txtPhoneNumber.text

        }

        
        
        return (true, req)
    }
}
//MARK:- TextFieldDelegate's Methods
extension  ForgotPasswordVC: UITextFieldDelegate{
    
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           let textFieldText: NSString = (textField.text ?? "") as NSString
           let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
           if txtAfterUpdate.count < 51 {
               return true
           } else {
               return false
           }
       }
       
       func textFieldDidBeginEditing(_ textField: UITextField) {
           error_Lbl.isHidden = true
           email_View.BorderWidth = 2
           email_View.backgroundColor = ColorConstants.commonWhite
           email_View.layer.borderColor = ColorConstants.TextColorPrimary.cgColor
       }
       
       func textFieldDidEndEditing(_ textField: UITextField) {
           email_View.backgroundColor = ColorConstants.textbackgroundcolor
           email_View.BorderWidth = 0
       }
}
