//
//  ForgotPwdOTPVC.swift
//  E-Scooter
//
//  Created by Coruscate Mac on 14/05/19.
//  Copyright © 2019 CORUSCATEMAC. All rights reserved.
//

import UIKit
import PhoneNumberKit
import ObjectMapper
import SimpleAnimation

class ForgotPwdOTPVC: UIViewController {
    
    //MARK: IBOutletss
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var viewPlaceholder: UIView!
    @IBOutlet weak var viewResendOTP: UIView!
    
    @IBOutlet weak var otpVw: SVPinView!
    @IBOutlet weak var viewGradiant: GradientView!
    
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var lblCodeSendTo: UILabel!
    @IBOutlet weak var lblReSendTo: UILabel!
    @IBOutlet weak var lblError: UILabel!
    
    @IBOutlet weak var btnResendToEmail: UIButton!
    @IBOutlet weak var btnResendToPhone: UIButton!
    
    @IBOutlet weak var codeSentView: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var btnResendCode: UIButtonCommon!
    @IBOutlet weak var OtpView_BottomConstraint: NSLayoutConstraint!
    
    let phoneNumberKit = PhoneNumberKit()
    
    //MARK: Variables
    var timer: Timer?
    var isFromAuth : Bool = false
    var isFromForgotPWD : Bool = false
    var isPhoneNoSelected : Bool = false
    var strPhoneNumber = ""
    var userModel = UserInfoModel()
    var editClick :((String) -> ())?
    
    var seconds: TimeInterval = 60 {
        didSet {
            if seconds == 0 {
                btnResend.isHidden = false
               // lblError.isHidden = false
                timer?.invalidate()
                timer = nil
                lblTimer.alpha = 0.0
            }else if seconds == 59.0 {
                self.codeSentView.isHidden = false
                self.animateView()
            } else if seconds == 50.0 {
                codeSentView.isHidden = true
            } else {
                btnResend.isHidden = true
                //lblError.isHidden = true
                lblTimer.alpha = 1.0
            }
            let text = Utilities.attributedString(firstString: "KLblResendCodeIn".localized, secondString: " \(timeString (time: seconds))", firstFont: FontScheme.FontConstant.kRegularFont, secondFont: FontScheme.FontConstant.kSemiBoldFont, firstColor: ColorConstants.textcolor, secondColor: ColorConstants.secondTextColor, firstSize: 14, secondSize: 14)
            self.lblTimer.attributedText = text
            
        }
    }
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.endEditing(false)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.codeSentView.isHidden = true
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }
    
    
    //MARK: Private Methods
    private func initialConfig(){
        
        startTimer()
        
        if !isFromForgotPWD {
            userModel = ApplicationData.user ?? UserInfoModel()
        }
        
        otpVw.style = .box
        otpVw.activeBorderLineColor = ColorConstants.TextColorPrimary
        otpVw.font = UIFont(name: "Montserrat-SemiBold", size: 18)!
        lblCodeSendTo.text = strPhoneNumber
        
        otpVw.isContentTypeOneTimeCode = true
        otpVw.didFinishCallback = { otp in
            self.VerifyOtp_Api()
        }
    }
    
    func animateView() {
        codeSentView.slideIn(from: .left)
    }
    
    @IBAction func edit_Phone_Btn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func startTimer() {
        
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (timer) in
            self.seconds -= 1
        })
    }
    
    func timeString(time: TimeInterval) -> String {
        let minute = Int(time) / 60 % 60
        let second = Int(time) % 60
        
        return String(format: "%02i : %02i", minute, second)
    }
    
    //MARK: IBAction
    @IBAction func btnBack_Click(_ sender: UIButton) {
         editClick?("edit")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmit_Click(_ sender: UIButton) {
        
        self.view.endEditing(true)
       
        if self.otpVw.getPin().count == 0 {
//            self.show_Toast(message: "Please enter otp")
        } else {
//            if isFromForgotPWD {
//                VerifyOtp_Api()
//                move_ToChange_numberScreen()
//            } else {
////                VerifyOtp_Api()
                move_ToEmailSCreen()
//            }
        }
    }
    
    func move_ToChange_numberScreen() {
//        let vc = VerifyMobileVC()
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func move_ToEmailSCreen() {
        let vc = FB_email_ViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func move_To_HomeSCreen() {
        let vc = HomeVC()
        vc.checkScreen = "Otp"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.view.frame.origin.y == 0 {
//                self.view.frame.origin.y -= keyboardSize.height
                self.OtpView_BottomConstraint.constant = 264//keyboardSize.height + 22
//            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        self.OtpView_BottomConstraint.constant = 40
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func VerifyOtp_Api() {
        
        let param : [String: Any] = ["otp" : self.otpVw.getPin(),
                                     "mobile" : Defaults[.Mobile] as Any,
                                     "deviceType" : "IOS",
                                     "deviceToken" : "hfefefdfefewe",
                                     "requestId" : "80e7ecf1ad974d12bc948ad035568609",
                                     "countryCode" : Defaults[.Code] as Any]

//        if isFromForgotPWD {
//            param["isForgotPassword"] = true
//        }
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.VerifyResetOTP, method: .post, parameters: param, headers: nil, success: { (response, message) in
            
            DispatchQueue.main.async {
                
                if let data = response as? [String : Any] {
                    
                    let userJson = data["user"] as? [String: Any]
                    ApplicationData.sharedInstance.saveLoginData(data: data)
                    ApplicationData.sharedInstance.saveUserData(userJson!)
                    if Defaults[.isNewUser] == true {
                        self.move_ToEmailSCreen()
                    } else {
                        self.move_To_HomeSCreen()
                    }
                }
            }
            
        }) { (failureMessage, failureCode) in
             Utilities.showAlertView(message: failureMessage)
        }
    }
    
    @IBAction func btnResend(_ sender: UIButton) {
        self.timer?.invalidate()
        self.timer = nil
        self.seconds = 60
        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (timer) in
            self.seconds -= 1
        })
        resendOtp()
    }
    
    func resendOtp() {
        
        var param = Parameters()
//        param["mobile"] = userModel.primaryMobile?.mobile
//        if isFromForgotPWD {
//            param["isForgotPassword"] = true
//        }
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.ResendOtp, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            self.startTimer()
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
