////
////  VerifyMobileVC.swift
////  WeOne
////
////  Created by Coruscate Mac on 11/06/20.
////  Copyright © 2020 Coruscate Mac. All rights reserved.
////
//
//import UIKit
//import PhoneNumberKit
//
//class VerifyMobileVC: UIViewController {
//
//    //MARK: Outlets
//    @IBOutlet weak var viewPhoneNumber: UIView!
//    @IBOutlet weak var viewEmail: UIView!
//    @IBOutlet weak var viewPhoneNumberLine: UIView!
//    @IBOutlet weak var viewEmailLine: UIView!
//    @IBOutlet weak var btnSave: UIButtonCommon!
//    @IBOutlet weak var txtPhoneNumber: PhoneNumberTextField!
//    @IBOutlet weak var txtEmail: UITextField!
//    @IBOutlet weak var imgCountry: UIImageView!
//    @IBOutlet weak var lblVerified: UILabel!
//    @IBOutlet weak var lblUnVerified: UILabel!
//    @IBOutlet weak var lblUnVerifiedMsg: UILabel!
//
//    //MARK: Variables
//    let user = ApplicationData.user
//
//    var arrCountries = [String]()
//    var arrCountryCodes = [String]()
//    var selectedCountryCode = ""
//    var strDate = ""
//    var selectedCountry: Int? {
//        didSet {
//            if let index = selectedCountry {
//                //KD lblCountryCode.text = self.arrCountryCodes[index]
//
//                txtPhoneNumber.partialFormatter.defaultRegion = arrCountries[index].components(separatedBy: ":").last ?? ""
//
//                let url = URL.init(string: "http://flagpedia.net/data/flags/normal/\(self.arrCountries[index].components(separatedBy: ":")[1].lowercased()).png")
//                imgCountry.sd_setImage(with: url, completed: { (image, error, cache, url) in
//                })
//
//                let phone = txtPhoneNumber.text ?? ""
//                let phoneNumber = phone.replacingOccurrences( of:"[^0-9]", with: "", options: .regularExpression)
//
//                if user?.primaryMobile?.isVerified ?? false && user?.primaryMobile?.mobile == phoneNumber && user?.primaryMobile?.countryCode == selectedCountryCode {
//                    lblVerified.text = "KVerifiedOn".localized + self.strDate
//                    lblVerified.isHidden = false
//                    lblUnVerified.isHidden = true
//                } else {
//                    lblVerified.isHidden = true
//                    lblUnVerified.isHidden = false
//                }
//
//
//            }
//        }
//    }
//
//
//    var isFromMobile: Bool = false 
//
//    //MARK: View life cycle methods
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        intialConfig()
//    }
//
//    //MARK: Initial config
//    func intialConfig(){
//
//        getCountryData()
//
//        btnSave.click = {
//            self.btnDone_Click()
//        }
//
//        if isFromMobile {
//            viewPhoneNumber.isHidden = false
//            viewEmail.isHidden = true
//            txtPhoneNumber.text = user?.primaryMobile?.mobile
//            txtPhoneNumber.placeholder = "KLblPhoneNumber".localized
//            txtPhoneNumber.tintColor = ColorConstants.ThemeColor
//            txtPhoneNumber.keyboardType = .numberPad
//
//            if user?.primaryMobile?.isVerified ?? false {
//                let date = DateUtilities.convertDateFromStringWithFromat(dateStr: (user?.primaryMobile?.lastVerifiedAt ?? ""), format: DateUtilities.DateFormates.kMainSourceFormat)
//
//                strDate = DateUtilities.convertStringFromDate(date: date, format: DateUtilities.DateFormates.kVerifiedDateFormat)
//
//                lblVerified.text = "KVerifiedOn".localized + strDate
//                lblVerified.isHidden = false
//                lblUnVerified.isHidden = true
//                lblUnVerifiedMsg.isHidden = true
//            } else {
//                lblVerified.isHidden = true
//                lblUnVerified.isHidden = false
//                lblUnVerifiedMsg.isHidden = false
//            }
//        } else {
//            viewPhoneNumber.isHidden = true
//            viewEmail.isHidden = false
//            txtEmail.text = user?.primaryEmail?.email
//            txtEmail.placeholder = "KLblEmailAddress".localized
//            txtEmail.tintColor = ColorConstants.ThemeColor
//            txtEmail.keyboardType = .emailAddress
//
//            lblUnVerifiedMsg.isHidden = true
//
//            if user?.primaryEmail?.isVerified ?? false {
//
//                let date = DateUtilities.convertDateFromStringWithFromat(dateStr: (user?.primaryEmail?.lastVerifiedAt ?? ""), format: DateUtilities.DateFormates.kMainSourceFormat)
//
//                strDate = DateUtilities.convertStringFromDate(date: date, format: DateUtilities.DateFormates.kVerifiedDateFormat)
//
//                lblVerified.text = "KVerifiedOn".localized + strDate
//                lblVerified.isHidden = false
//                lblUnVerified.isHidden = true
//            } else {
//                lblVerified.isHidden = true
//                lblUnVerified.isHidden = false
//             //   lblUnVerifiedMsg.isHidden = false
//            }
//
//        }
//
//    }
//
//    //MARK: Button click actions
//    @IBAction func btnCountryCode_Click(_ sender: UIButton) {
//        self.view.showSelectionPopup(topHeading: "KLblSelectCountry".localized, defaultSelectedIndex: self.selectedCountry, isFromCity: false, isFromSignup: true, isShowSearchBar: true, isShowSelectedImage: true, arrData: arrCountries, onSubmitClick: { (index) in
//
//            if ApplicationData.user?.primaryMobile?.mobile == self.txtPhoneNumber.text && ApplicationData.user?.primaryMobile?.countryCode == self.arrCountryCodes[index] {
//                let date = DateUtilities.convertDateFromStringWithFromat(dateStr: (self.user?.primaryMobile?.lastVerifiedAt ?? ""), format: DateUtilities.DateFormates.kMainSourceFormat)
//
//                let strDt = DateUtilities.convertStringFromDate(date: date, format: DateUtilities.DateFormates.kVerifiedDateFormat)
//
//                self.lblVerified.text = "KVerifiedOn".localized + strDt
//                self.lblVerified.isHidden = false
//                self.lblUnVerified.isHidden = true
//                self.lblUnVerifiedMsg.isHidden = true
//
//            }else{
//                self.lblVerified.isHidden = true
//                self.lblUnVerified.isHidden = false
//                self.lblUnVerifiedMsg.isHidden = false
//
//            }
//            self.selectedCountryCode = self.arrCountryCodes[index]
//            self.selectedCountry = index
//        })
//    }
//
//     @IBAction func btnClose_Click(_ sender: UIButton) {
//        self.navigationController?.popViewController(animated: true)
//    }
//
//    func btnDone_Click() {
//        if lblVerified.isHidden {
//            self.view.endEditing(true)
//            let (isValidate) = checkValidation()
//            if isValidate {
//                let finalParam = prepareRequest()
//                callAPIForUpdateProfile(request: finalParam)
//            }
//        } else {
//            if isFromMobile {
//                self.showToast(text: "KVerifiedMobile".localized)
//            } else {
//                self.showToast(text: "KVerifiedEmail".localized)
//            }
//        }
//    }
//
//
//    func checkValidation() -> (Bool) {
//
//
//        if isFromMobile {
//            if Utilities.checkStringEmptyOrNil(str: txtPhoneNumber.text) {
////                Utilities.showAlertView(message: "KLblEnterPhoneNumber".localized)
//                self.showToast(text: "KLblEnterPhoneNumber".localized)
//
//                return (false)
//            }
//
////            if (txtPhoneNumber.text ?? "").isValidMobileNumber() == false{
////                Utilities.showAlertView(message: "KLblEnterValidPhoneNumber".localized)
////                return (false)
////            }
//
//        } else {
//
//            if Utilities.checkStringEmptyOrNil(str: txtEmail.text) {
//                self.showToast(text: "KLblEnterEmailAddress".localized)
//
////                Utilities.showAlertView(message: "KLblEnterEmailAddress".localized)
//                return (false)
//            }
//
//            if (txtEmail.text ?? "").isValidEmail() == false {
//                self.showToast(text: "KLblEnterValidEmailAddress".localized)
//
////                Utilities.showAlertView(message: "KLblEnterValidEmailAddress".localized)
//                return (false)
//            }
//        }
//
//        return (true)
//    }
//
//    func getCountryData() {
//        arrCountries.removeAll()
//        arrCountryCodes.removeAll()
//        let arrCountry = Utilities.readJsonFile(fileName: "countryCodes")
//
//
//        for i in 0..<arrCountry.count{
//            let model = arrCountry[i]
//            arrCountries.append("\(model["name"] as? String ?? ""):\(model["code"] as? String ?? "")")
//            arrCountryCodes.append(model["dial_code"] as? String ?? "")
//        }
//
//        for i in 0..<arrCountry.count{
//
//            let model = arrCountry[i]
//            if ApplicationData.isUserLoggedIn{
//
//                if model["dial_code"] as? String == ApplicationData.user?.primaryMobile?.countryCode {
//                    self.selectedCountry = i
//                    self.selectedCountryCode = arrCountryCodes[i]
//                    //self.countryName = arrCountryCodes[i]
//                    break
//                }
//
//            }else{
//
//                if selectedCountry == nil{
//                    if model["code"] as? String == Locale.current.regionCode ?? "" {
//                        self.selectedCountry = i
//                        //self.countryName = arrCountryCodes[i]
//                        break
//                    }
//
//
//                }else{
//                    /*if model["dial_code"] as? String == self.countryName {
//                     self.selectedCountry = i
//                     self.countryName = arrCountryCodes[i]
//                     break
//                     }*/
//                }
//            }
//        }
//    }
//
//    func moveToOtpVc(){
//        AppNavigation.shared.currentStateList = AppNavigation.shared.config.accessConfig.requiredToUseApp
//        let vc = ForgotPwdOTPVC(nibName: "ForgotPwdOTPVC", bundle: nil)
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
//
//}
//extension VerifyMobileVC: UITextFieldDelegate {
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if textField == txtPhoneNumber {
//            viewPhoneNumberLine.backgroundColor = ColorConstants.ThemeColor
//            viewEmailLine.backgroundColor = ColorConstants.UnderlineColor
//        } else {
//            viewEmailLine.backgroundColor = ColorConstants.ThemeColor
//            viewPhoneNumberLine.backgroundColor = ColorConstants.UnderlineColor
//        }
//        return true
//    }
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
//
//        if textField == txtPhoneNumber {
//            let strCurrent = newString.replacingOccurrences( of:"[^0-9]", with: "", options: .regularExpression)
//
//            if user?.primaryMobile?.isVerified ?? false && user?.primaryMobile?.mobile == strCurrent && user?.primaryMobile?.countryCode == selectedCountryCode {
//                lblVerified.text = "KVerifiedOn".localized + self.strDate
//                lblVerified.isHidden = false
//                lblUnVerified.isHidden = true
//            } else {
//                lblVerified.isHidden = true
//                lblUnVerified.isHidden = false
//            }
//        } else {
//
//            if user?.primaryEmail?.isVerified ?? false && user?.primaryEmail?.email == newString {
//                lblVerified.text = "KVerifiedOn".localized + self.strDate
//                lblVerified.isHidden = false
//                lblUnVerified.isHidden = true
//
//            } else {
//                lblVerified.isHidden = true
//                lblUnVerified.isHidden = false
//
//            }
//        }
//        return true
//    }
//}
////MARK:- API Calling
//extension VerifyMobileVC {
//
//    func prepareRequest() -> [String : Any] {
//        var finalParam = [String : Any]()
//        if isFromMobile {
//                        var req = [String:Any]()
//                        req["mobile"] = txtPhoneNumber.text
//                        req["isPrimary"] = user?.primaryMobile?.isPrimary ?? false
//
//                            req["countryCode"] = selectedCountryCode
//
//                        req["isVerified"] = user?.primaryMobile?.isVerified ?? false
//
//            //            if mobileModel.mobile == data["mobiles"] as? String && mobileModel.countryCode == countryCode{
//            //                req["id"] = mobileModel.id
//            //            }
//
//                        finalParam["mobiles"] = [req]
//
//        } else {
//            finalParam["emails"] = [["email": txtEmail.text ?? "",
//                                     "isPrimary": user?.primaryEmail?.isPrimary ?? false,
//                                     "isVerified": user?.primaryEmail?.isVerified ?? false]]
//
//
//        }
//        return finalParam
//    }
//
//    func callAPIForUpdateProfile(request:[String:Any]) {
//
//        let param : [String:Any] = request
//
//        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
//
//        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.UpdateProfile, method: .put, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
//
//            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//
//                if let dictResponse = response as? [String:Any] {
//
//                    ApplicationData.sharedInstance.saveUserData(dictResponse)
//                    let dictMobile = ApplicationData.user?.primaryMobile
//                    if dictMobile?.isVerified == false {
//                        self.moveToOtpVc()
//                    } else {
//                        Utilities.showAlertWithButtonAction(title: "", message: message, buttonTitle: StringConstants.ButtonTitles.KOk, onOKClick: {
//                        self.navigationController?.popViewController(animated: true)
//
//                        })
//                    }
//                }
//            }
//        }) { (failureMessage, failureCode) in
//            Utilities.showAlertView(message: failureMessage)
//        }
//    }
//}
