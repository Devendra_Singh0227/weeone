//
//  SignUpVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 28/09/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//
/* if AppConstants.hasSafeArea {
           vwMainBottom.constant = 34
       }
       else {
           vwMainBottom.constant = 0
       } */
import UIKit
import PhoneNumberKit
import CoreTelephony

class SignUpVC: UIViewController, UITextFieldDelegate {

    //MARK: - Variables
    var arrData = [CellModel]()
    var isGdprShown = false
    var dictGDPR : [String:Any] = [:]
    var strReferralCode = String()
    var arrCountries = [String]()
    var arrCountryCodes = [String]()
    var isSuccess : (() -> ())?
    var cellModel:CellModel?
    var code = ""
    var isFromMain = false 
    @IBOutlet weak var text_View: ViewDesign!
    @IBOutlet weak var error_Lbl: UILabel!
    
    //MARK: - Outlets
    
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constrainTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var popUp_View: UIView!
    @IBOutlet weak var btnVerify: ButtonDesign!
    @IBOutlet weak var txtField: PhoneNumberTextField!
    @IBOutlet weak var imgCountry: UIImageView!
    
    var selectedCountry: Int? {
        didSet {
            if let index = selectedCountry {
                 txtField.text = self.arrCountryCodes[index]

                txtField.partialFormatter.defaultRegion = arrCountries[index].components(separatedBy: ":").last ?? ""

                let url = URL.init(string: "http://flagpedia.net/data/flags/normal/\(self.arrCountries[index].components(separatedBy: ":")[1].lowercased()).png")
                imgCountry.sd_setImage(with: url, completed: { (image, error, cache, url) in
                })
            }
        }
    }
    
    //MARK:- Controller's Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        getCountryData()
        text_View.layer.borderWidth = 0
        txtField.delegate = self
        txtField.maxDigits = 12
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        text_View.layer.borderWidth = 2
        text_View.backgroundColor = UIColor.white
        text_View.layer.borderColor = ColorConstants.TextColorPrimary.cgColor
        error_Lbl.isHidden = true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if txtField.text == "" || txtField.text!.count < 8 {
            text_View.layer.borderColor = ColorConstants.TextColorPrimary.cgColor
            error_Lbl.isHidden = true
        } else {
            
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }

    @IBAction func none_Of_Above_Btn(_ sender: UIButton) {
        popUp_View.isHidden = true
    }
    
    @IBAction func change_number(_ sender: UIButton) {
         let vc = ForgotPasswordVC()
        //        vc.isFromForgotPassword = true
        //        vc.mobile = self.userModel.primaryMobile?.mobile ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }

    //MARK: get country Data
    func getCountryData(){
        arrCountries.removeAll()
        arrCountryCodes.removeAll()
        let arrCountry = Utilities.readJsonFile(fileName: "countryCodes")
        
        for i in 0..<arrCountry.count{
            let model = arrCountry[i]
            arrCountries.append("\(model["name"] as? String ?? ""):\(model["code"] as? String ?? "")")
            arrCountryCodes.append(model["dial_code"] as? String ?? "")
            
            if model["code"] as? String == Locale.current.regionCode ?? "" {
                self.selectedCountry = i
            }
        }
    }
    
    //MARK:- IBActions
     
    @IBAction func continue_Btn(_ sender: ButtonDesign) {
//         moveToOtpVc()
        let mobile = txtField.text?.components(separatedBy: " ")
        let code = mobile?[0]
        Defaults[.Mobile] = txtField.nationalNumber
        Defaults[.Code] = code

        if txtField.text == "" || txtField.nationalNumber.count < 9 {
            text_View.layer.borderColor = ColorConstants.TextColorError.cgColor
            text_View.layer.borderWidth = 2
            error_Lbl.isHidden = false
            //self.show_Toast(message: StringConstants.validationerror.validphone)
        } else {
//            moveToOtpVc()
            loginIn_Api(code: code!)
        }
    }
    
    func moveToOtpVc() {
        let vc = ForgotPwdOTPVC(nibName: "ForgotPwdOTPVC", bundle: nil)
        vc.strPhoneNumber = txtField.text!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func loginIn_Api(code: String) {
       
        let param : [String:Any] = ["mobile"        : txtField.nationalNumber,
                                    "countryCode"   : code]
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.SendOTP, method: .post, parameters: param, headers: nil, success: { (response, message) in
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.moveToOtpVc()
            }
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }

    @IBAction func country_Btn_Click(_ sender: UIButton) {
        getCountryData()

        show_SelectionPopup(topHeading: "KLblSelectCountry".localized, defaultSelectedIndex: self.selectedCountry, isFromCity: false, isFromSignup: true, isShowSearchBar: true, isShowSelectedImage: true, arrData: arrCountries, onSubmitClick: { (index) in

            self.selectedCountry = index
        })
    }
    
    func show_SelectionPopup(topHeading: String?,defaultSelectedIndex:Int?, isFromCity : Bool = false, isFromSignup : Bool = false, isShowSearchBar:Bool,isShowSelectedImage:Bool, arrData: [String], onSubmitClick: @escaping (Int) -> ()) {
        let popup = CountryListPopup.init(nibName: "CountryListPopup", bundle: nil)
        popup.arr_tblData = arrData
        popup.isFromCity = isFromCity
        popup.isShowSearchBar = isShowSearchBar
        popup.selectedIndex = defaultSelectedIndex
        popup.isShowSelectedImage = isShowSelectedImage
        popup.isFromSignup = isFromSignup
        popup.selectedCountry = { countryIndex in
            onSubmitClick(countryIndex)
        }
        popup.modalPresentationStyle = .fullScreen
        UIViewController.current()?.present(popup, animated: true, completion: {
        })
    }
}

