//
//  Email_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 28/07/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Email_ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var email_TxtFld: UITextField!
    @IBOutlet weak var error_Lbl: UILabel!
    var email = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        email_TxtFld.delegate = self
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continue_Btn(_ sender: UIButton) {
        
        if email_TxtFld.text == "" {
            emailView.layer.borderColor = ColorConstants.TextColorError.cgColor
            error_Lbl.isHidden = false
            emailView.BorderWidth = 2
            //self.show_Toast(message: StringConstants.validationerror.validEmail)
        } else if !(email_TxtFld.text!.isValidEmail()) {
            emailView.layer.borderColor = ColorConstants.TextColorError.cgColor
            error_Lbl.isHidden = false
            emailView.BorderWidth = 2
             //self.show_Toast(message: StringConstants.validationerror.validEmail)
        } else {
//            moveTo_NameVC()
            callApiForEmailCheck()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        if txtAfterUpdate.count < 51 {
            return true
        } else {
            return false
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        error_Lbl.isHidden = true
        emailView.BorderWidth = 2
        emailView.backgroundColor = ColorConstants.commonWhite
        emailView.layer.borderColor = ColorConstants.TextColorPrimary.cgColor
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        emailView.backgroundColor = ColorConstants.textbackgroundcolor
        emailView.BorderWidth = 0
        error_Lbl.isHidden = true
    }
    
    func moveTo_NameVC() {
        let vc = Name_ViewController(nibName: "Name_ViewController", bundle: nil)
        vc.email = email_TxtFld.text!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func already_Btn(_ sender: UIButton) {
        navigationController?.pop_To_ViewController(ofClass: SignUpVC.self, animated: true)
    }
        
    func callApiForEmailCheck() {
        
        let param : [String: Any] = [ "email"    : email_TxtFld.text!]
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.checkEmail, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            DispatchQueue.main.async {
                if let dictResponse = response as? [String:Any] {
                    if dictResponse["isExist"] as? Int == 1 {
                        Utilities.showAlertView(message: dictResponse["message"] as? String)
                    } else {
                         self.moveTo_NameVC()
                    }
                }               
            }
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
