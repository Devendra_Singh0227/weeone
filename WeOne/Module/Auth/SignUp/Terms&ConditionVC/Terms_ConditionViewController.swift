//
//  Terms&ConditionVC.swift
//  WeOne
//
//  Created by Dev's Mac on 28/07/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit
import WebKit

class Terms_ConditionViewController: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var navigationHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var navigationView: ViewDesign!
    @IBOutlet weak var webView: WKWebView!
    var checkScreen = ""
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerViewheight: NSLayoutConstraint!
    @IBOutlet weak var footerView: ViewDesign!
    @IBOutlet weak var footerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var header_Lbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        intialConfig()
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 8.0)
        webView.navigationDelegate = self
    }

    func intialConfig() {
        if checkScreen == "terms" {
            setupScreen()
            setupweb(url: "\(AppConstants.serverURL)\(AppConstants.StaticPage.TermsCondition)")
        } else if checkScreen == "BookVC" {
            setupScreen()
            header_Lbl.text = "Insurance Policy"
            setupweb(url: "\(AppConstants.serverURL)\(AppConstants.StaticPage.PrivacyPolicy)")
        } else {
            setupweb(url: "\(AppConstants.serverURL)\(AppConstants.StaticPage.TermsCondition)")
        }
    }
    
    func setupScreen() {
        navigationView.isHidden = false
        navigationHeightConstraint.constant = 50
        headerView.isHidden = true
        headerViewheight.constant = 0
        footerView.isHidden = true
        footerViewHeight.constant = 0
    }
    
    func setupweb(url: String) {
        let myURL = URL(string: url)
        var myRequest = URLRequest(url: myURL!)
        print(myRequest)
        myRequest.httpMethod = "GET"
        webView.load(myRequest)
    }
    
    @IBAction func agree_Btn(_ sender: UIButton) {
         let vc = PaymentViewController(nibName: "PaymentViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
