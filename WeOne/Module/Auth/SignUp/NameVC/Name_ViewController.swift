//
//  Name_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 28/07/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Name_ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var first_Name_TxtField: UITextField!
    @IBOutlet weak var first_View: ViewDesign!
    @IBOutlet weak var second_View: ViewDesign!
    @IBOutlet weak var last_Name_TxtField: UITextField!
    var email = ""
    @IBOutlet weak var error1_LblHeight: NSLayoutConstraint!
    @IBOutlet weak var error2_LblHeight: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        first_Name_TxtField.delegate = self
        last_Name_TxtField.delegate = self
    
        // Do any additional setup after loading the view.
    }
    
//    func getuser_Values() {
//         first_Name_TxtField.text = Defaults[.FirstName] as? String ?? ""
//         last_Name_TxtField.text = Defaults[.LastName] as? String ?? ""
//    }

    @IBAction func baack_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func done_Btn(_ sender: ButtonDesign) {
        
        if first_Name_TxtField.text!.count < 2 {
            first_View.borderColor = ColorConstants.TextColorError
            error1_LblHeight.constant = 17
            first_View.borderwidth = 2
            //self.show_Toast(message: StringConstants.validationerror.validFirstname)
        } else if last_Name_TxtField.text!.count < 2 {
            second_View.borderColor = ColorConstants.TextColorError
            error2_LblHeight.constant = 17
            second_View.borderwidth = 2
            //self.show_Toast(message: StringConstants.validationerror.validLastname)
        } else {
//            moveTo_nextScreen()
            callApiForRegister()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == first_Name_TxtField {
            error1_LblHeight.constant = 0
            first_View.borderColor = ColorConstants.TextColorPrimary
            first_View.borderwidth = 2
            first_View.backgroundColor = ColorConstants.commonWhite
            first_View.layer.borderColor = ColorConstants.TextColorPrimary.cgColor
        } else {
            error2_LblHeight.constant = 0
            second_View.borderColor = ColorConstants.TextColorPrimary
            second_View.borderwidth = 2
            second_View.backgroundColor = ColorConstants.commonWhite
            second_View.layer.borderColor = ColorConstants.TextColorPrimary.cgColor
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == first_Name_TxtField {
            first_View.borderwidth = 0
            first_View.backgroundColor = ColorConstants.textbackgroundcolor
            error1_LblHeight.constant = 0
        } else {
            second_View.borderwidth = 0
            second_View.backgroundColor = ColorConstants.textbackgroundcolor
            error2_LblHeight.constant = 0
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
        if textField == first_Name_TxtField {
            if txtAfterUpdate.count < 41 {
                return true
            } else {
                return false
            }
        } else {
            if txtAfterUpdate.count < 81 {
                return true
            } else {
                return false
            }
        }
    }
    
    func moveTo_nextScreen() {
        let vc = Terms_ConditionViewController(nibName: "Terms_ConditionViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func callApiForRegister() {

        let param : [String: Any] = ["referralCode" : "",
                                    "mobile"        : Defaults[.Mobile] as? String ?? "",
                                    "email"         : email,
                                    "countryCode"   : Defaults[.Code] as? String ?? "",
                                    "lastName"      : last_Name_TxtField.text!,
                                    "firstName"     : first_Name_TxtField.text!]

        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.Signup, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            DispatchQueue.main.async {
                if let dictResponse = response as? [String:Any] {
                    let userJson = dictResponse["data"] as? [String:Any]
                    ApplicationData.sharedInstance.saveLoginData(data: dictResponse)
                    ApplicationData.sharedInstance.saveUserData(userJson!)
                    self.moveTo_nextScreen()
                }
            }

        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}

