//
//  SignupPhoneTextField.swift
//  WeOne
//
//  Created by iMac on 20/02/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit
import PhoneNumberKit

class SignupPhoneTextField: UITableViewCell {
    
    //MARK: - Outlets3
    @IBOutlet weak var lblCountryCode: UILabel!

    @IBOutlet weak var viewLine: UIView!
    
    @IBOutlet weak var txtField: PhoneNumberTextField!
//    @IBOutlet weak var txtField: UITextFiledCommon!

   
    @IBOutlet weak var txtLeading: NSLayoutConstraint!
    @IBOutlet weak var txtTrailing: NSLayoutConstraint!

    @IBOutlet weak var btnCountry: UIButton!
    @IBOutlet weak var btnVerify: UIButton!
    
    @IBOutlet weak var imgCountry: UIImageView!

    @IBOutlet weak var txtBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var txtTopConstraint: NSLayoutConstraint!

    @IBOutlet weak var viewBottomConstraint: NSLayoutConstraint!
    
    //MARK: - Variables
    var cellModel:CellModel?
    
    var arrCountries = [String]()
    var arrCountryCodes = [String]()
    
    var selectedCountry: Int? {
        didSet {
            if let index = selectedCountry {
                //KD lblCountryCode.text = self.arrCountryCodes[index]

                txtField.partialFormatter.defaultRegion = arrCountries[index].components(separatedBy: ":").last ?? ""

                let url = URL.init(string: "http://flagpedia.net/data/flags/normal/\(self.arrCountries[index].components(separatedBy: ":")[1].lowercased()).png")
                imgCountry.sd_setImage(with: url, completed: { (image, error, cache, url) in
                    })

            }
        }
    }

    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        txtField.delegate = self
//        self.txtField.textField.leftPadding = 64

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel, isSignUp: Bool) {
        
        getCountryData()

        cellModel = model
//        txtField.type = model.textFieldType
        //txtField.placeholder = model.placeholder

        txtField.text =  model.userText
        
        
        txtField.tintColor = ColorConstants.ThemeColor
        
//        let attributedTitle = NSAttributedString(string: model.placeholder ?? "", attributes: [NSAttributedString.Key.kern: 0.64, NSAttributedString.Key.foregroundColor : ColorConstants.TextColorPrimary, NSAttributedString.Key.font : FontScheme.kLightFont(size: 16.0)])
//        txtField.attributedPlaceholder = attributedTitle


        if model.cellType == .ProfileEditPhone {
//            txtField.font = FontScheme.kMediumFont(size: 16)
            txtBottomConstraint.constant = 20
            txtTopConstraint.constant = 20
            txtField.isUserInteractionEnabled = false
            btnCountry.isUserInteractionEnabled = false
        }
        
        if isSignUp {
            txtLeading.constant = 30
            txtTrailing.constant = 30
        } else {
            txtLeading.constant = 20
            txtTrailing.constant = 20
        }
        
   
        if ApplicationData.isUserLoggedIn{
            
            if model.cellType == .SignUpPhone{
//                btnVerify.isHidden = false
////                txtField.textField.textFieldRightPadding(padding: 50)
//
//                if ApplicationData.user?.primaryMobile?.isVerified == false{
//                    btnVerify.setImage(UIImage(named : "errorUpload"), for: .normal)
//                    btnVerify.isUserInteractionEnabled = true
//                }else{
//                    btnVerify.setImage(UIImage(named : "Verified"), for: .normal)
//                    btnVerify.isUserInteractionEnabled = false
//                }
//            }else{
//                if ApplicationData.user?.primaryMobile?.isVerified == false{
//                    btnVerify.setTitle("KUnverified".localized, for: .normal)
//                    btnVerify.setTitleColor(ColorConstants.TextColorSecondary, for: .normal)
//                    btnVerify.isHidden = false
//                } else {
//                    btnVerify.setTitle("KVerified".localized, for: .normal)
//                    btnVerify.isUserInteractionEnabled = false
//                    btnVerify.isHidden = false
//                }
            }
            
        }else{
            btnVerify.isHidden = true
//            txtField.textField.textFieldRightPadding(padding: 0)
        }
        
    }
    
    //MARK: get country Data
    func getCountryData(){
        arrCountries.removeAll()
        arrCountryCodes.removeAll()
        let arrCountry = Utilities.readJsonFile(fileName: "countryCodes")
    
        
        for i in 0..<arrCountry.count{
            let model = arrCountry[i]
            arrCountries.append("\(model["name"] as? String ?? ""):\(model["code"] as? String ?? "")")
            arrCountryCodes.append(model["dial_code"] as? String ?? "")
        }
        
        for i in 0..<arrCountry.count{
            
            let model = arrCountry[i]
            if ApplicationData.isUserLoggedIn{
                
//                if model["dial_code"] as? String == ApplicationData.user?.primaryMobile?.countryCode {
//                    self.selectedCountry = i
//                    cellModel?.userText1 = arrCountryCodes[i]
                    break
//                }
                
            }else{
                
                if cellModel?.userText1 == nil{
                    if model["code"] as? String == Locale.current.regionCode ?? "" {
                        self.selectedCountry = i
                        cellModel?.userText1 = arrCountryCodes[i]
                        break
                    }
                    
                    
                }else{
                    if model["dial_code"] as? String == cellModel?.userText1 {
                        self.selectedCountry = i
                        cellModel?.userText1 = arrCountryCodes[i]
                        break
                    }
                }
            }
        }
    }

    @IBAction func btnCountryListTapped(_ sender: UIButton) {
        
        //self.btnVerify.isHidden = true
        
//        self.showSelectionPopup(topHeading: "KLblSelectCountry".localized, defaultSelectedIndex: self.selectedCountry, isFromCity: false, isFromSignup: true, isShowSearchBar: true, isShowSelectedImage: true, arrData: arrCountries, onSubmitClick: { (index) in
//
//            if ApplicationData.user?.primaryMobile?.mobile == self.txtField.text && ApplicationData.user?.primaryMobile?.countryCode == self.arrCountryCodes[index] {
//                self.btnVerify.isHidden = false
//            }else{
//                self.btnVerify.isHidden = true
//            }
//
//            self.cellModel?.userText1 = self.arrCountryCodes[index]
//            self.selectedCountry = index
//        })
    }

}


//MARK:- TextFieldDelegate's Methods
extension  SignupPhoneTextField: UITextFieldDelegate{
    
    func didBeginEditing(textField: UITextField) {
      //  viewLine.backgroundColor = ColorConstants.TextColorTheme
        print("begin editing")
    }
    
    func shouldReturn(textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        viewLine.backgroundColor = ColorConstants.UnderlineColor
        cellModel?.userText = txtField.text

    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        viewLine.backgroundColor = ColorConstants.ThemeColor
    }

    func didEndEditing(textField:UITextField) {
     //   viewLine.backgroundColor = ColorConstants.BorderColor
        cellModel?.userText = txtField.text
    }
    
    func textFieldValueChanged(textField: UITextField) {
//        if ApplicationData.isUserLoggedIn{
//            if cellModel?.cellType == .SignUpPhone{
//                if ApplicationData.user?.primaryMobile?.mobile == txtField.text && ApplicationData.user?.primaryMobile?.countryCode == self.arrCountryCodes[self.selectedCountry ?? 0]{
//                    btnVerify.isHidden = false
//                }else{
//                    btnVerify.isHidden = true
//                }
//            }
//        }
    }
}

