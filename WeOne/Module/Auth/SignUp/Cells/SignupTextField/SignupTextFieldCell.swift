//
//  SignupTextFieldCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 28/09/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class SignupTextFieldCell: UITableViewCell {
    
    //MARK: - Variables
    var cellModel:CellModel?
    
    //MARK: - Outlets
    @IBOutlet weak var txtField: UITextFiledCommon!
    
    @IBOutlet weak var txtLeading: NSLayoutConstraint!
    @IBOutlet weak var txtTrailing: NSLayoutConstraint!

    @IBOutlet weak var btnVerify: UIButton!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        txtField.delegate = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel, isSignUp: Bool) {
//        cellModel = model
//        txtField.type = model.textFieldType
//        txtField.textField.placeholder = model.placeholder
//        txtField.setText(text: model.userText ?? "")
//        txtField.imgView.image = UIImage(named: model.imageName ?? "")
//        txtField.textField.keyboardType = model.keyboardType ?? .default
//        txtField.placeholder = NSString(string : model.placeholder ?? "")
//
//        if isSignUp {
//            txtLeading.constant = 30
//            txtTrailing.constant = 30
//        } else {
//            txtLeading.constant = 20
//            txtTrailing.constant = 20
//            txtField.isIcon = false
//        }
//
//        if ApplicationData.isUserLoggedIn{
//
//            if model.cellType == .SignUpPhone{
//                btnVerify.isHidden = false
//                txtField.textField.textFieldRightPadding(padding: 50)
//
//                if ApplicationData.user?.primaryMobile?.isVerified == false{
//                    btnVerify.setImage(UIImage(named : "errorUpload"), for: .normal)
//                    btnVerify.isUserInteractionEnabled = true
//                } else {
//                    btnVerify.setImage(UIImage(named : "Verified"), for: .normal)
//                    btnVerify.isUserInteractionEnabled = false
//                }
//            }else{
//                btnVerify.isHidden = true
//            }
//
//        }else{
//            btnVerify.isHidden = true
//            txtField.textField.textFieldRightPadding(padding: 0)
//        }
    }
}

//MARK:- TextFieldDelegate's Methods
extension  SignupTextFieldCell: TextFieldDelegate{
    
    func didBeginEditing(textField: UITextField) {
        print("begin editing")
    }
    
    func shouldReturn(textField: UITextField) {
        
    }
    
    func didEndEditing(textField:UITextField) {
        cellModel?.userText = txtField.getText()
    }
    
    func textFieldValueChanged(textField: UITextField) {
//        if ApplicationData.isUserLoggedIn{
//            if cellModel?.cellType == .SignUpPhone{
//                if ApplicationData.user?.primaryMobile?.mobile == txtField.getText(){
//                    btnVerify.isHidden = false
//                }else{
//                    btnVerify.isHidden = true
//                }
//            }
//        }
    }
}
