//
//  SignupFooterCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 28/09/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class SignupFooterCell: UITableViewCell {

    //MARK: - Variables
    var cellModel:CellModel?
    var btnSignUp_Click:(()->())?
    var isTermClick : (() -> ())?
    var isPrivacyClick : (() -> ())?
    
    //MARK: - Outlets    
    @IBOutlet weak var lblToc: UILabel!
    @IBOutlet weak var btnSignUp: UIButtonCommon!
    @IBOutlet weak var btnLogin: UIButton!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        //setClickableLink()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        cellModel = model
        btnSignUp.click = {
            self.btnSignUp_Click?()
        }
    }
    
//    func setClickableLink() {
//
//        let customType = ActiveType.custom(pattern: "\\s\("kLblTOC".localized)\\b")
//        let customType2 = ActiveType.custom(pattern: "\\s\("KLblPrivacyPolicy".localized)\\b")
//
//        lblToc.enabledTypes.append(customType)
//        lblToc.enabledTypes.append(customType2)
//
//        lblToc.customize { label in
//            lblToc.text = "KLblAcceptTOCPrivacy".localized
//
//            lblToc.numberOfLines = 0
//            lblToc.lineSpacing = 10.0
//
//            //Custom types
//
//            lblToc.customColor[customType] = ColorConstants.TextColorTheme
//            lblToc.customColor[customType2] = ColorConstants.TextColorTheme
//
//            lblToc.configureLinkAttribute = { (type, attributes, isSelected) in
//                var atts = attributes
//
//                switch type {
//                case customType, customType2:
//                    atts[NSAttributedString.Key.underlineStyle] = NSUnderlineStyle.single.rawValue
////                    atts[NSAttributedString.Key.font] = FontScheme.kMediumFont(size: 14.0)
//
//                default: ()
//                atts[NSAttributedString.Key.underlineStyle] = nil
//                    break
//                }
//
//                return atts
//            }
//
//            lblToc.handleCustomTap(for: customType) {_ in
//                self.isTermClick?()
//            }
//            lblToc.handleCustomTap(for: customType2) {_ in
//                self.isPrivacyClick?()
//            }
//        }
//    }
    
    //MARK:- IBActions
    @IBAction func btnChkBox_Clicked(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        cellModel?.isSelected = sender.isSelected
    }
}
