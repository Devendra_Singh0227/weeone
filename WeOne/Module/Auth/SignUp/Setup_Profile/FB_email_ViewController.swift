//
//  FB_email_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 28/07/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import SimpleAnimation

class FB_email_ViewController: UIViewController {

    @IBOutlet weak var numberVerifyView: ViewDesign!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4 ) {
            self.numberVerifyView.isHidden = false
            self.numberVerifyView.slideIn(from: .top)
        }
    }

    @IBAction func email_Btn(_ sender: ButtonDesign) {
        movetoEmailVC(email: "", firstName: "", lastName: "")
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
   
    @IBAction func skip_Btn(_ sender: ButtonDesign) {
        let VC = Loading_ViewController()
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    func movetoEmailVC(email: String, firstName: String, lastName: String) {
        let vc = Email_ViewController(nibName: "Email_ViewController", bundle: nil)
        vc.email = email
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func MoveToHomeVc() {
        let vc = HomeVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func facebook_Btn(_ sender: ButtonDesign) {
        
        // NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logOut()
        fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) in
            
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email")) {
                        if((AccessToken.current) != nil){
                            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                                
                                if (error == nil) {
                                    
                                    if let dict = result as? [String : Any] {
                                        print("dict is :\(dict)")
                                        let picturedict = dict["picture"] as? Dictionary<String, Any>
                                        let imageUrl = picturedict?["data"] as? Dictionary<String, Any>
                                        let profilePic = imageUrl?["url"] as? String
                                        let email = dict["email"] as? String ?? ""
                                        let firstName = dict["first_name"] as? String ?? ""
                                        let lastName = dict["last_name"] as? String ?? ""
                                       
                                        Defaults[.FirstName] = firstName
                                        Defaults[.LastName] = lastName
                                        Defaults[.Email] = email
                                        Defaults[.profilePic] = profilePic
                                        DispatchQueue.main.async {
                                            self.callApiForRegister(email: email, lastName: lastName, firstName: firstName)
                                        }
                                        
                                        if let tokenString = AccessToken.current?.tokenString {
                                           // self.fbLoginApi(id: tokenString)
                                        }
                                    }
                                } else {
                                    print("somthing went wrong")
                                }
                            })
                        }
                    }
                }
            }
        }
    }
    
    func callApiForRegister(email: String, lastName: String, firstName: String) {
        
        let param : [String: Any] = ["referralCode" : "",
                                     "mobile"        : Defaults[.Mobile] as? String ?? "",
                                     "email"         : email,
                                     "countryCode"   : Defaults[.Code] as? String ?? "",
                                     "lastName"      : lastName,
                                     "firstName"     : firstName]
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.Signup, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            print(response)
            if let dictResponse = response as? [String:Any] {
                ApplicationData.sharedInstance.saveLoginData(data: dictResponse)
                Defaults[.isUserLoggedIn] = true
//                SyncManager.sharedInstance.upsertPlayerId()
            }
            self.MoveToHomeVc()
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
