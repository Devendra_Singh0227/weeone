//
//  CancellationInformationVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 11/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class CancellationInformationVC: UIViewController {
    
    //MARK: - Variables
    var arrData = [CellModel]()
    
    //MARK: - Outlets
    @IBOutlet weak var viewNavigation: UIViewCommon!

    @IBOutlet weak var btnCancelRentalConditons: UIButtonCommon!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintHeightTableView: NSLayoutConstraint!
    @IBOutlet weak var viewMain: UIView!
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true)
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
       // self.constraintHeightTableView.constant = self.tableView.contentSize.height
    }
    
    //MARK: Private Methods
    func initialConfig() {
        
        tableView.register(UINib(nibName: "CISingleTextCell", bundle: nil), forCellReuseIdentifier: "CISingleTextCell")
        
        tableView.register(UINib(nibName: "CIDoubleTextCell", bundle: nil), forCellReuseIdentifier: "CIDoubleTextCell")
        
        viewNavigation.text = "KLblCancellationInformation".localized as NSString

        viewNavigation.click = {
            self.btnCancel_Click()
        }

        
        prepareDataSource()
        
    }
    
    func prepareDataSource() {
        arrData.removeAll()
        
        arrData.append(CellModel.getModel(placeholder: "In case of cancellation of lease relations, \nthe following applies rules and fees.", text: "", type: .CISingleText))
        
        arrData.append(CellModel.getModel(placeholder: "After the tenancy has started.", text: "KLblFullPrice".localized, type: .CIDoubleText))
        
        arrData.append(CellModel.getModel(placeholder: "Less than 1 hour after order, and before the tenancy starts.", text: "KLblNoFees".localized, type: .CIDoubleText))
        
        arrData.append(CellModel.getModel(placeholder: "More than 1 hour after order, and more than 24 hours before tenancy starter.", text: "KLblNoFees".localized, type: .CIDoubleText))
        
        arrData.append(CellModel.getModel(placeholder: "More than 1 hour after order, and less than 24 hours before the tenancy starts.", text: "KLblHalfprice".localized, type: .CIDoubleText))
        
        arrData.append(CellModel.getModel(placeholder: "Cancellation fees will be credited in Weone credit\nWeone credit can be used in future bookings", text: "", type: .CICreditText))

        tableView.reloadData()
    }
    
    func btnCancel_Click() {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: - UITableView's DataSource & Delegate Methods
extension CancellationInformationVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrData[indexPath.row]
        
        switch model.cellType! {
        case .CISingleText, .CICreditText:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CISingleTextCell") as? CISingleTextCell
            cell?.setData(model: model)
            return cell!
            
        case .CIDoubleText:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CIDoubleTextCell") as? CIDoubleTextCell
            cell?.setData(model: model)
            return cell!
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
}
