//
//  CISingleTextCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 11/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class CISingleTextCell: UITableViewCell {
    
    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var lblText: UILabel!

    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        lblText.text = model.placeholder
        
        if model.cellType == .CICreditText {

            lblText.textColor = ColorConstants.ThemeColor
            
           // lblText.font = FontScheme.KLBoldFont(size: 12)
            
//            lblText.changeStringColor(string: lblText.text ?? "", array: ["Weone credit can be used in future bookings"], colorArray: [ColorConstants.TextColorSecondary], changeFontOfString: ["Weone credit can be used in future bookings"], font: [FontScheme.kMediumFont(size: 12)])
        }
        
    }
    
}
