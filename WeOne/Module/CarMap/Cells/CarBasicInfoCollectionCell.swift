//
//  CarBasicInfoCollectionCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 27/11/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class CarBasicInfoCollectionCell: UICollectionViewCell {
        
    //MARK: - Outlets
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewCarInfo: UIView!

    @IBOutlet weak var imgVehicle: UIImageView!
    @IBOutlet weak var imgFuelType: UIImageView!
    @IBOutlet weak var lblVehicleName: UILabel!

    @IBOutlet weak var btnReserve: UIButtonCommon!

    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var collection_view: UICollectionView!
    
   
    @IBOutlet weak var lblFuelPercentage: UILabel!

    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblTotalKm: UILabel!

    @IBOutlet weak var lblNextBookingTime: UILabel!
    @IBOutlet weak var btnPreviousRentalInfo: UIButton!
    
    var carFeatures:(([CellModel])->())?

    //MARK: - Variables

    var vehicleModel = VehicleModel()
    var arrPackages = [PackagesModel]()
    var arrcarFeatures = [CellModel]()

    var rideType: Int?
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        viewMain.addShadow(offset: CGSize.zero, color: ColorConstants.ShadowColor, opacity: 0.6, radius: 4.5)
        
        collection_view.register(UINib(nibName: "CarPropertyCell", bundle: nil), forCellWithReuseIdentifier: "CarPropertyCell")
        collection_view.dataSource = self
        collection_view.delegate = self

        
        viewCarInfo.isOpaque = false
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
     //   self.collection_view.reloadData()

    }
    //MARK: Set Data
    func setData(_ model : VehicleModel, rideType: Int, startDate : Date? = nil, endDate : Date? = nil) {
        
        self.rideType = rideType
        let package = model.packages
        
        viewCarInfo.backgroundColor = ColorConstants.ThemeColor.withAlphaComponent(0.80)
        lblTotalAmount.textColor = ColorConstants.ThemeColor
        lblNextBookingTime.textColor = ColorConstants.ThemeColor
        
        if rideType == AppConstants.RideType.Schedule {
            
            let totalAmount = package.reduce(0.0) { $0 + ($1.calculatedRate) }
            
            lblTotalAmount.text = String(format : "\(Utilities.convertToCurrency(number: totalAmount, currencyCode: package.first?.currency))")
            
            btnReserve.setText(text: "KLblBook".localized)
            
            if startDate != nil && endDate != nil {
                
               let dateRangeStart = DateUtilities.convertStringFromDate(date: startDate!, format: DateUtilities.DateFormates.kLongDate)
                let dateRangeEnd = DateUtilities.convertStringFromDate(date: endDate!, format: DateUtilities.DateFormates.kLongDate)
                
                let now = DateUtilities.convertDateFromStringWithFromat(dateStr: dateRangeStart, format: DateUtilities.DateFormates.kLongDate)
                let date = DateUtilities.convertDateFromStringWithFromat(dateStr: dateRangeEnd, format: DateUtilities.DateFormates.kLongDate)
                
                let components = Calendar.current.dateComponents([.day, .hour], from: now, to: date)
                
                if components.hour != 0 && components.day != 0{
                    lblNextBookingTime.text = "\(components.hour ?? 0) hours | \(components.day ?? 0) days"
                } else if components.day != 0 {
                    lblNextBookingTime.text = "\(components.day ?? 0) days"
                } else if components.hour != 0 {
                     lblNextBookingTime.text = "\(components.hour ?? 0) hours"
                }

                lblNextBookingTime.textColor = ColorConstants.TextColorSecondary
                lblNextBookingTime.changeStringColor(string: lblNextBookingTime.text ?? "", array: ["|"], colorArray: [ColorConstants.ThemeColor])
            }
        } else {
            
            btnReserve.setText(text: "kLblRESERVE".localized)

            lblNextBookingTime.text = getReturnTimeString(endTime: "", reservedDateTime: model.nextBookingDetail?.reservedDateTime)
//            lblNextBookingTime.changeStringColor(string: lblNextBookingTime.text ?? "", array: ["Available for"], colorArray: [ColorConstants.TextColorSecondary], changeFontOfString: ["Available for"],  font :  [FontScheme.kMediumFont(size: 12)])

            lblTotalAmount.text = String(format : "\(Utilities.convertToCurrency(number: package.first!.rate, currencyCode: package.first?.currency))\(package.first?.getFormattedUnit ?? "")")

            if package.count == 0{
                arrPackages.first?.isSelected = true
            }

        }
        
        vehicleModel = model
                
        arrPackages = model.packages
        
        lblVehicleName.text = "\(vehicleModel.getCarName) | \(model.numberPlate ?? "")"

        imgVehicle.roundCorners([.topLeft, .topRight], radius: 10, width: AppConstants.ScreenSize.SCREEN_WIDTH - 40)
        imgVehicle.setImageForURL(url: model.getVehicleImage, placeHolder: UIImage(named : "carPlaceholder"))

        if model.fuelType == AppConstants.FuelType.Combustion{
            imgFuelType.image = UIImage(named : "fuel")
        }else{
            imgFuelType.image = UIImage(named : "electric-station")
        }

        lblFuelPercentage.text = "\(model.fuelLevelStr ?? "")"
        if model.estimatedKmPerFuel > 0.0 {
            lblTotalKm.text = "| \(Int(model.estimatedKmPerFuel)) \("KLblKm".localized)"
            lblTotalKm.isHidden = false
        } else {
            lblTotalKm.isHidden = true
        }
        arrcarFeatures = vehicleModel.getCarProperties(isLoadAll: false)
        
        self.collection_view.reloadData()
        
    }

    func getReturnTimeString(endTime: String?, reservedDateTime: String?) -> String? {
        let returnHours = 24

        if (endTime == "") {
            if reservedDateTime != "" {
                
                let reservedate = DateUtilities.convertDateFromServerString(dateStr: reservedDateTime ?? "")
                
                let cal = Calendar.current
                let components = cal.dateComponents([.hour], from: Date(), to: reservedate)
                let hours = components.hour!
                if hours < returnHours && hours != 0 {
                    return String(format : "KLblCarAvailableTime".localized,"\(hours)")
                } else {
                    return String(format : "KLblCarAvailableTime".localized,"\(returnHours)")
                }
            }
        }
        return String(format : "KLblCarAvailableTime".localized,"\(returnHours)")
    }


    @IBAction func btnInsurance_Click(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        vehicleModel.isInsurance = !vehicleModel.isInsurance
    }
    
    @IBAction func btnInfo_Click(_ sender: UIButton) {
        
        let vc = InsurancePopupVC(nibName: "InsurancePopupVC", bundle: nil)
        vc.vehicle = vehicleModel
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        
        UIViewController.current().present(vc, animated: true, completion: {
        })
    }
    
    @IBAction func btnSeeRates_Click(_ sender: UIButton) {
        let vc = PackageDetailPopupVC(nibName: "PackageDetailPopupVC", bundle: nil)
        vc.arrPackages = arrPackages
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        
        UIViewController.current().present(vc, animated: true, completion: {
        })

    }
    
    @IBAction func btnTransmission_Click(_ sender: UIButton) {
//        Utilities.showPopOverView(str: vehicleModel.gearBox == AppConstants.GearBoxType.AUTOMATIC ? "\("KAutomaticTrasmission".localized)" : "\("KManualTrasmission".localized)", arrowSize: CGSize(width: 9, height: 9), sender: sender)
    }
    
    @IBAction func btnFuelType_Click(_ sender: UIButton) {
//        Utilities.showPopOverView(str: vehicleModel.fuelType == AppConstants.FuelType.Combustion ? "KLblCombustion".localized : "KLblElectric".localized, arrowSize: CGSize(width: 9, height: 9), sender: sender)
    }
    
    @IBAction func btnSuitCase_Click(_ sender: UIButton) {
//        Utilities.showPopOverView(str: "\(vehicleModel.travelBags) \("KLblSuitcases".localized)", arrowSize: CGSize(width: 9, height: 9), sender: sender)
    }
    
    @IBAction func btnChildSeats_Click(_ sender: UIButton) {
//        Utilities.showPopOverView(str: vehicleModel.getChildSeatGroups, arrowSize: CGSize(width: 9, height: 9), sender: sender)
    }
}

//MARK: - UICollectionView's DataSource & Delegate Methods
extension CarBasicInfoCollectionCell: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrcarFeatures.count < 4 {
            return arrcarFeatures.count
        } else {
            return 4
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if arrcarFeatures.count < 4 {
            return CGSize(width: ((AppConstants.ScreenSize.SCREEN_WIDTH - 72) / CGFloat(arrcarFeatures.count)) , height: collectionView.bounds.height)
        } else {
            return CGSize(width: ((AppConstants.ScreenSize.SCREEN_WIDTH - 72) / 4) , height: collectionView.bounds.height)
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarPropertyCell", for: indexPath) as! CarPropertyCell
    
        cell.setCellData(arrcarFeatures[indexPath.row], index: indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
     /*   if rideType != AppConstants.RideType.Schedule {
            for item in arrPackages {
                item.isSelected = false
            }

            arrPackages[indexPath.row].isSelected = true
            collection_view.reloadData()
        }*/
    }
}
