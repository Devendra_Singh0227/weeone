//
//  Cars_CollectionViewCell.swift
//  WeOne
//
//  Created by Dev's Mac on 03/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Cars_CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var car_Image: ImageViewDesign!
    @IBOutlet weak var book_now_Btn: UIButton!
    @IBOutlet weak var instant_Btn: UIButton!
    @IBOutlet weak var car_Name: UILabel!
    @IBOutlet weak var perHour_Lbl: UILabel!
    @IBOutlet weak var available_Lbl: UILabel!
    @IBOutlet weak var km_Lbl: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
