//
//  PackageCell.swift
//  WeOne
//
//  Created by Mayur on 23/01/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class PackageCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblFare: UILabel!
    @IBOutlet weak var viewMain: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: leftAnchor),
            contentView.rightAnchor.constraint(equalTo: rightAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    func setCellData(model : PackagesModel) {
        
        lblName.text = model.name
        lblFare.text = "\(Utilities.convertToCurrency(number: model.rate, currencyCode: model.currency))"
        if model.isSelected {
            viewMain.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        } else{
            viewMain.backgroundColor = .clear
        }
    }
}
