//
//  CarMapVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 26/11/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class CarMapVC: UIViewController {
    
    //MARK: - Variables
    var isShowReservedRides : Bool = false

    var currentLocation: CLLocation = CLLocation()
    var currentLocationMarker: GMSMarker = GMSMarker()
    var camera : GMSCameraPosition = GMSCameraPosition()
    var arrMarkers: [GMSMarker] = []
    var arrCities : [VehicleModel] = []
    var arrCollData : [VehicleModel] = []
    var isFromDriveNow : Bool = false
    var isFromStartRide : Bool = false
    var isApiCalled : Bool = false
    var isShowFuelStation : Bool = false
    var isShowChargeStation : Bool = false
    var reservationStartDate:Date?
    var reservationEndDate:Date?
    var rideType = AppConstants.RideType.Instant
    var cityID : String = ""
    var isCarosuolShownOnce : Bool = false
    var currentCarousolIndex : Int = 0
    var mapScreensViewActions: [MapScreensViewActions] = [MapScreensViewActions]()
    
    var placesClient : GMSPlacesClient = GMSPlacesClient()
    var arrSearchPlace : [PlacesModel] = []
    var arrcarFeatures = [CellModel]()
    
    var getDestination : ((_ model : PlacesModel, _ isSourceLocation: Bool) -> ())?
    
    var markerIndex = 0
    var previousTheme = ThemeManager.DefaultTheme

    //MARK: - Outlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var collectView: UICollectionView!
    @IBOutlet weak var viewBasicCarInfo: UIView!
    @IBOutlet weak var viewFilter: ShadowCard!
    @IBOutlet weak var viewFooterDrivenow: UIView!
    @IBOutlet weak var viewFooterNevigation: UIView!
    @IBOutlet weak var viewNevigation: UIView!

    @IBOutlet weak var viewShowOnMap: UIButtonCommon!
    @IBOutlet weak var viewNavigate: UIButtonCommon!

    @IBOutlet weak var viewSchedule: ShadowCard!
    @IBOutlet weak var viewDriveNow: ShadowCard!

    @IBOutlet weak var btnSchedule: UIButton!
    @IBOutlet weak var btnDriveNow: UIButton!
    @IBOutlet weak var btnSwap: UIButton!

    @IBOutlet weak var swipeTosee_Lbl: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgAdd: UIImageView!
    @IBOutlet weak var imgFuel: UIImageView!
    @IBOutlet weak var imgInfo: UIImageView!

    @IBOutlet weak var imgStop: UIImageView!
    @IBOutlet weak var imgEnd: UIImageView!
    
    @IBOutlet weak var constraintBottomViewCurrentLocation: NSLayoutConstraint!
    @IBOutlet weak var constraintViewBasicCarInfoHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imgFilter: UIImageView!
    
    @IBOutlet weak var viewFilterBadge: UIView!
    @IBOutlet weak var viewFuelStation: UIView!
    @IBOutlet weak var viewToolBar: UIView!
    
    @IBOutlet weak var viewShowAllRides: ShadowCard!
    @IBOutlet weak var imageCar: UIImageView!
    
    @IBOutlet weak var viewReportDamage: ShadowCard!
    @IBOutlet weak var viewChat: ShadowCard!
    @IBOutlet weak var viewCurrentLocation: ShadowCard!
    @IBOutlet weak var viewAdd: ShadowCard!
    
    @IBOutlet weak var viewDrawer: ShadowCard!
    
    @IBOutlet weak var viewNevigationToolBar: UIView!
    
    @IBOutlet weak var viewStopLocation: UIView!
    
    @IBOutlet weak var txtCurrentLocation: UITextField!
    @IBOutlet weak var txtDestination: UITextField!
    @IBOutlet weak var txtStopLocation: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    //Reserve ride outlets
    @IBOutlet weak var constraintBottomReserveLocation: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightViewNavigation: NSLayoutConstraint!
    @IBOutlet weak var constraintTrailingViewAdd: NSLayoutConstraint!
    @IBOutlet weak var support_View: ViewDesign!
    @IBOutlet weak var search_View: ShadowCard!
    @IBOutlet weak var carsView_heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var calendar_View: ViewDesign!
    
    var rideModel:RideListModel = RideListModel()
    let gmsMarkerInfoWindow = GMSMarkerInforWindow()
    var startLocation: PlacesModel?
    var stopLocation: PlacesModel?
    var endLocation: PlacesModel?
  
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        collectView.register(UINib(nibName: "Cars_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Cars_CollectionViewCell")
        checkSreenSize()
        
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }
    
    func checkSreenSize() {
        if AppConstants.DeviceType.IS_IPHONE_6 {
            carsView_heightConstraint.constant = 320
        } else if AppConstants.DeviceType.IS_IPHONE_6P {
            carsView_heightConstraint.constant = 350
        } else if AppConstants.DeviceType.IS_IPHONE_X { //11pro
            carsView_heightConstraint.constant = 380
        } else if AppConstants.DeviceType.IS_IPHONE_XR {//11
            carsView_heightConstraint.constant = 380
        } else if AppConstants.DeviceType.IS_IPHONE_XS_MAX {
            carsView_heightConstraint.constant = 380
        }
        //setLayoutFromActions()
    }
    
    //MARK:- Private Methods
    func initialConfig() {
        
        //imgInfo.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Information"))
        //FindNearbyRide_Api()
        loadMapJsonFile()
        mapView.mapType = .normal
        mapView.setMinZoom(4.0, maxZoom: 20.0)
        mapView.isBuildingsEnabled = false
        markerIndex = 0
        
        collectView.register(UINib(nibName: "CarBasicInfoCollectionCell", bundle: nil), forCellWithReuseIdentifier: "CarBasicInfoCollectionCell")
       LocationManager.SharedManager.getLocation()
       LocationManager.SharedManager.getCurrentLocation = { location in
//           self.currentLocation = location
//           self.showCurrentLocation()
            self.callApiForGetNearbyCityVehicle({
               self.setLayoutFromActions()
           })
       }
        //Cars_CollectionViewCell
        
//        tableView.register(UINib(nibName: "PlacePickerCell", bundle: nil), forCellReuseIdentifier: "PlacePickerCell")
        
//        if CLLocationManager.authorizationStatus() == .denied{
//            callApiForGetNearbyCityVehicle()
//        }
//        callApiForGetNearbyCityVehicle({
//            self.setLayoutFromActions()
//            //self.setLayoutFromActions(mapScreensViewActions: [.showFindRideLayout, .showSupportButtons])
//        })
        
//        if self.rideType == AppConstants.RideType.Schedule {
            //constraintViewBasicCarInfoHeight.constant = 446
//        } else {
            //constraintViewBasicCarInfoHeight.constant = 446
//        }
        
       
        
//        txtCurrentLocation.delegate = self
//        txtStopLocation.delegate = self
//        txtDestination.delegate = self

//        viewShowOnMap.setBackGroundColor(color: ColorConstants.TextColorSecondary)
//        viewShowOnMap.isCancel = true
//        viewShowOnMap.click = {
//            self.btnShowOnMap_Click()
//        }
        
//        viewNavigate.click = {
//            self.btnNavigate()
//        }

//        btnDriveNow.addTarget(self, action: #selector(holdDownDriveNow(_:)), for: .touchDown)
//        btnSchedule.addTarget(self, action: #selector(holdDownSchedule(_:)), for: .touchDown)
        
    }
    
   func FindNearbyRide_Api() {
        let param : [String: Any] = ["isChargeStation" : "false",
                                     "isFuelStation" :"false",
                                     "currentLocation" : "false",
                                     "rideType": "1",
                                     "ZoomLevel": "10"]
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.VerifyResetOTP, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let data = response as? [String : Any] {
               print(response)
                
                if self.navigationController != nil{
                    for viewController in (self.navigationController?.viewControllers)!{
                        if viewController.isKind(of: ForgotPwdOTPVC.self) {
                            self.navigationController?.viewControllers.remove(at: (self.navigationController?.viewControllers.firstIndex(of: viewController))!)
                        }
                    }
                }
            }
            
        }) { (failureMessage, failureCode) in
             Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func setNevigationLayout() {
                
//        tableView.isHidden = true
        
//        setData()
        
    }
    
//    func setData() {
//
//        if rideModel.startLocation != nil || rideModel.geoLocation != nil{
//            let model = PlacesModel()
//
//            if rideModel.startLocation != nil{
//                txtCurrentLocation.text = rideModel.startLocation?.name
//
//                model.addressFullText = rideModel.startLocation?.name
//                model.longitude = rideModel.startLocation?.longitude
//                model.latitude = rideModel.startLocation?.latitude
//
//
//            } else if rideModel.geoLocation != nil {
//                txtCurrentLocation.text = rideModel.geoLocation?.name
//
//                model.addressFullText = rideModel.geoLocation?.name
//                model.longitude = rideModel.geoLocation?.longitude
//                model.latitude = rideModel.geoLocation?.latitude
//
//            } else {
//               // txtCurrentLocation.placeholder = "KLblCurrentLocation".localized
//            }
//
//            startLocation = model
//        }else{
//
//           // txtCurrentLocation.placeholder = "KLblCurrentLocation".localized
//            txtCurrentLocation.text = "KLblCurrentLocation".localized
//
//            //Set current location
//            let model = PlacesModel()
//            model.latitude = currentLocation.coordinate.latitude
//            model.longitude = currentLocation.coordinate.longitude
//
//            Utilities.getAddressFromLatLon(pdblLatitude: model.latitude ?? 0.0, withLongitude:  model.longitude ?? 0.0) { (address, pm) in
//                model.addressFullText = address
//
//                self.startLocation = model
//                self.txtCurrentLocation.text = address
//            }
//
//        }
//
//        if rideModel.estimateEndLocation != nil || rideModel.endLocation != nil{
//            let model = PlacesModel()
//
//            if rideModel.estimateEndLocation != nil {
//                txtDestination.text = rideModel.estimateEndLocation?.name
//
//                model.addressFullText = rideModel.estimateEndLocation?.name
//                model.longitude = rideModel.estimateEndLocation?.longitude
//                model.latitude = rideModel.estimateEndLocation?.latitude
//
//            } else if rideModel.endLocation != nil {
//                txtDestination.text = rideModel.endLocation?.name
//
//                model.addressFullText = rideModel.endLocation?.name
//                model.longitude = rideModel.endLocation?.longitude
//                model.latitude = rideModel.endLocation?.latitude
//
//            } else {
//               // txtDestination.textField.placeholder = "KLblWhereTo".localized
//            }
//
//            endLocation = model
//        }else{
//           // txtDestination.textField.text = "KLblWhereTo".localized
//            endLocation = PlacesModel()
//            //txtDestination.textField.placeholder = "KLblWhereTo".localized
//            self.showPathForCar()
//        }
//
//        viewStopLocation.isHidden = true
//       // txtStopLocation.text = "KLblAddStop".localized
//    }
    //Map json file
    func loadMapJsonFile(){
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                 print("style",styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
       
        let obj = SyncManager.sharedInstance.activeRideObject
        
//        if obj != nil {
//            self.isFromStartRide = true
//            self.setLayoutFromActions(mapScreensViewActions: [.showDrawer, .showOngoingRideLayout])
//        } else {
////            setLayoutFromActions(mapScreensViewActions: [.showHomeLayout, .showCurrentLocationButton])
//            setLayoutFromActions(mapScreensViewActions: [.showFindRideLayout])
//        }
    }
    
//    func setMapLayout() {
//        if let arrRides = SyncManager.sharedInstance.arrReserveRides, arrRides.count > 0 && isFromDriveNow == false {
//
//            ThemeManager.sharedInstance.RideType = arrRides.first?.rideType ?? 1
//            setLayoutFromActions(mapScreensViewActions: [.showDrawer, .showReservationLayout])
//        }
//
//        showReservedRides()
//
//        if self.isFromDriveNow{
//            if !self.isCarosuolShownOnce {
//                self.isCarosuolShownOnce = true
//
//                self.arrCollData = self.arrCities.filter { $0.markerType == AppConstants.MarkerType.Vehicle && $0.geoLocation != nil }
//
//                if self.arrCollData.count > 0 {
//
//                    if self.viewBasicCarInfo.isHidden == true{
//                        self.showCarBasicInfo(true)
//                        self.animateToSpecificMarker(self.arrCollData.first!, false)
//                    }
//
//                    self.collectView.reloadData()
//                }
//            }
//        } else {
//            self.showPathForCar()
//        }
//
//    }
    
//    func setLayoutFromActions(mapScreensViewActions: [MapScreensViewActions]) {
    func setLayoutFromActions() {
        
        //viewToolBar.isHidden = false
        //isCarosuolShownOnce = false
        //self.showAllMarkers()
        
       // self.setMapLayout()
        
//        if !self.isCarosuolShownOnce {
            //self.isCarosuolShownOnce = true
            
//            self.arrCollData = self.arrCities.filter { $0.markerType == AppConstants.MarkerType.Vehicle && $0.geoLocation != nil }
            
            //                 if self.arrCollData.count > 0 {
            
            //                 if self.viewBasicCarInfo.isHidden == true{
            self.showCarBasicInfo(true)
            //self.animateToSpecificMarker(self.arrCollData.first!, false)
            //                 }
            
            self.collectView.reloadData()
       // viewDrawer.isHidden = true
       // viewToolBar.isHidden = true
       // viewReportDamage.isHidden = true
        
//        viewChat.isHidden = true
//        viewCurrentLocation.isHidden = true
       // viewFooterDrivenow.isHidden = true
        
        //viewBasicCarInfo.isHidden = true
       // viewShowAllRides.isHidden = true
        
        //viewFooterNevigation.isHidden = true
//        viewNevigation.isHidden = true
       // viewNevigationToolBar.isHidden = true
//        tableView.isHidden = true
        
//        for action in mapScreensViewActions {
//
//            switch action {
//
//            case .showDrawer:
////                viewDrawer.isHidden = false
//
//                break
//
//            case .showHomeLayout:  // Drive Now & Schedule Button, Dot Markers, ShowDrawer, ShowCurrentLocationButton
//               // viewFooterDrivenow.isHidden = false
//                //viewDrawer.isHidden = false
//
//                break
//
//            case .showSupportButtons: //   Chat, Report Damage
////                viewReportDamage.isHidden = true
////                viewChat.isHidden = true
//
//                break
//
//            case .showCurrentLocationButton: // User’s Current Location is not visible on screen
//
//                //Getting location
//                LocationManager.SharedManager.getLocation()
//                LocationManager.SharedManager.getCurrentLocation = { location in
//                    self.currentLocation = location
//                    self.showCurrentLocation()
//                }
//
//                break
//
//            case .showFindRideLayout: // Tool Bar, Car Pager, Vehicle Marker
//
//
//                viewToolBar.isHidden = false
//                isCarosuolShownOnce = false
//                self.showAllMarkers()
//
//                self.setMapLayout()
//
//                 if !self.isCarosuolShownOnce {
//                 self.isCarosuolShownOnce = true
//
//                 self.arrCollData = self.arrCities.filter { $0.markerType == AppConstants.MarkerType.Vehicle && $0.geoLocation != nil }
//
////                 if self.arrCollData.count > 0 {
//
////                 if self.viewBasicCarInfo.isHidden == true{
//                 self.showCarBasicInfo(true)
//                 //self.animateToSpecificMarker(self.arrCollData.first!, false)
////                 }
//
//                 self.collectView.reloadData()
////                 }
//                 }
//
//
//                break
//
//            case .showReservationLayout: // Reservation Pager, Show Path to Vehicle , ShowSupportButtons, ShowDrawer
//
//                viewShowAllRides.isHidden = false
//                isShowReservedRides = true
//
//                viewShowAllRides.backgroundColor = ColorConstants.ThemeColor
//                imageCar.image = UIImage(named : "carWhiteSelected")
//
//                /*KD //Getting live location
//                 LocationManager.SharedManager.getLocation(true)
//                 LocationManager.SharedManager.getLiveLocation = { location in
//                 self.currentLocation = location
//
//                 if self.currentLocation.coordinate.latitude != 0.0{
//                 self.showPathForCar()
//                 }
//                 } */
//
//                break
//
//            case .showOngoingRideLayout: // ShowSupportButtons, OngoingRideBottomSheet, ShowNetworkConnectionLayout, ShowDestinationPath
//
////                LocationManager.SharedManager.getLocation()
////                LocationManager.SharedManager.getCurrentLocation = { location in
////                    self.currentLocation = location
////                    self.showCurrentLocation()
//////                    Kd   self.showPathForOngoignRide()
////
////                }
////
////                self.setDataAccordingToRideStatus()
////                mapView.clear()
////                self.mapView.isBuildingsEnabled = false
//
//                break
//
//            case .showNavigationLayout: // ShowOnMap button, Navigate button, ShowLocationSelectionLayout, Back Button
//
//                btnSwap.setImage(UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Swap")), for: .normal)
//                imgAdd.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Add"))
//
//                txtCurrentLocation.tintColor = ColorConstants.ThemeColor
//                txtStopLocation.tintColor = ColorConstants.ThemeColor
//                txtDestination.tintColor = ColorConstants.ThemeColor
//
//                mapView.clear()
//                setNevigationLayout()
////                viewFooterNevigation.isHidden = false
//                viewNevigation.isHidden = false
//                viewNevigationToolBar.isHidden = false
//                viewNevigationToolBar.backgroundColor = UIColor.clear
//                break
//
//            }
//        }
        
    }
    
    //MARK:- Button action methods
        
    @IBAction func support_Btn(_ sender: UIButton) {
        support_View.isHidden = false
    }
    
    @IBAction func support_View_Btn(_ sender: UIButton) {
        support_View.isHidden = true
    }
    
    @IBAction func calendar_Btn(_ sender: UIButton) {
        calendar_View.isHidden = false
    }
    
    @IBAction func btnMenu_Click(_ sender: UIButton) {
    
        self.navigationController?.popViewController(animated: true)
//        ThemeManager.sharedInstance.RideType = ThemeManager.DefaultTheme
//
//        if isFromDriveNow {
//            //            self.navigationController?.popViewController(animated: true)
//            isFromDriveNow = false
//            isShowFuelChargeStation = false
//            showCarBasicInfo(false)
//            setLayoutFromActions(mapScreensViewActions: [.showHomeLayout, .showCurrentLocationButton])
//
//        } else {
//            let leftMenu = LeftMenuVC(nibName: "LeftMenuVC", bundle: nil)
//            let rootLeft = SideMenuNavigationController(rootViewController: leftMenu)
//            rootLeft.presentationStyle = .menuSlideIn
//            rootLeft.presentationStyle.backgroundColor = UIColor.clear
//            rootLeft.menuWidth = UIScreen.main.bounds.width
//            rootLeft.leftSide = true
//            rootLeft.enableSwipeToDismissGesture = false
//            present(rootLeft, animated: true, completion: nil)
//        }
    }
    
//    @IBAction func btnNevigationBack(_ sender: UIButton) {
//        markerIndex = 0
//        imgAdd.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Add"))
//        viewAdd.layer.shadowOffset = CGSize.zero
//        viewAdd.layer.shadowColor = ColorConstants.ShadowColor.cgColor
//        constraintHeightViewNavigation.constant = 140
//        stopLocation = nil
//        txtDestination.text = ""
//        txtStopLocation.text = ""
//        txtCurrentLocation.text = ""
//        //setMapLayout()
//    }
    
//    @IBAction func btnAddStop_Clicl(_ sender: UIButton) {
//        viewStopLocation.isHidden = !viewStopLocation.isHidden
//        btnSwap.isHidden = viewStopLocation.isHidden
//
//        if viewStopLocation.isHidden {
//            imgAdd.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Add"))
//
//            viewAdd.layer.shadowOffset = CGSize.zero
//            viewAdd.layer.shadowColor = ColorConstants.ShadowColor.cgColor
//            constraintHeightViewNavigation.constant = 140
//            constraintTrailingViewAdd.constant = 36
//            stopLocation = nil
//
//            imgEnd.image = #imageLiteral(resourceName: "aim")
//          //  showPathForNevigation()
//        } else {
//            imgAdd.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Minus"))
//
//            viewAdd.layer.shadowOffset = CGSize.zero
//            viewAdd.layer.shadowColor = UIColor.clear.cgColor
//            constraintHeightViewNavigation.constant = 210
//            constraintTrailingViewAdd.constant = 12
//
//            imgStop.image = #imageLiteral(resourceName: "Stop")
//            imgEnd.image = #imageLiteral(resourceName: "Destination")
//
//        }
//    }
    
//    @IBAction func btnSwap_Click(_ sender: UIButton) {
//
//        let tempLocation = stopLocation
//        stopLocation = endLocation
//        endLocation = tempLocation
//
//        self.txtStopLocation.text = self.stopLocation?.addressFullText
//        self.txtDestination.text = self.endLocation?.addressFullText
//
//    }

    
//    @objc func holdDownDriveNow(_ sender: UIButton) {
//        btnDriveNow.backgroundColor = ColorConstants.TextColorWhitePrimary.withAlphaComponent(0.50)
//    }

//    @IBAction func btnDriveNow_Click(_ sender: UIButton) {
//        btnDriveNow.backgroundColor = UIColor.clear
//
//        guard (NetworkClient.sharedInstance.networkReachability?.isReachable)! else {
//
//            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
//            //            failure("No Internet Connection.",100)
//            showToast(text: "KMsgNoInternetConnection".localized)
//            return
//        }
//
//
//        isFromDriveNow = true
//
//        ThemeManager.sharedInstance.RideType = AppConstants.RideType.Instant
//        self.rideType = AppConstants.RideType.Instant //kd
//
//        if Defaults[.Filter] != nil {
//            arrCities.removeAll()
//        }
//        if arrCities.count > 0 {
//            setLayoutFromActions(mapScreensViewActions: [.showFindRideLayout, .showSupportButtons])
//        } else {
//            callApiForGetNearbyCityVehicle({
////                self.setLayoutFromActions(mapScreensViewActions: [.showFindRideLayout, .showSupportButtons])
//            })
//        }
//
//        //        let vc = CarMapVC(nibName: "CarMapVC", bundle: nil)
//        //        vc.isFromDriveNow = true
//        //        vc.mapScreensViewActions =
//        //        vc.reservationStartDate = reservationStartDate
//        //        vc.reservationEndDate = reservationEndDate
//        //        self.navigationController?.pushViewController(vc, animated: true)
//    }
    
//    @objc func holdDownSchedule(_ sender: UIButton) {
//        viewSchedule.mainBGColor = ColorConstants.ScheduleThemeColor.withAlphaComponent(0.50)
//    }

//    @IBAction func btnSchedule_Click(_ sender: UIButton) {
//        viewSchedule.mainBGColor = ColorConstants.ScheduleThemeColor
//
//        ThemeManager.sharedInstance.RideType = AppConstants.RideType.Schedule
//
//        let vc = CarScheduleVC()
//        self.rideType = AppConstants.RideType.Schedule //kd
//
//        vc.getScheduleDetail = { (startDate, endDate, cityId) in
//
//            self.reservationStartDate = startDate
//            self.reservationEndDate = endDate
//            self.cityID = cityId
//            self.isFromDriveNow = true
////            if self.arrCities.count > 0 {
////                self.setLayoutFromActions(mapScreensViewActions: [.showFindRideLayout, .showSupportButtons])
////            } else {
//                self.callApiForGetNearbyCityVehicle({
//                    self.setLayoutFromActions(mapScreensViewActions: [.showFindRideLayout, .showSupportButtons])
//                })
////            }
//
//        }
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
    
    @IBAction func btnFilter_Click(_ sender: UIButton) {
        
        let vc = FilterVC(nibName: "FilterVC", bundle: nil)
        vc.isFilterApplied = {
            self.showCarBasicInfo(false)
            self.callApiForGetNearbyCityVehicle()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnGasStation_Click(_ sender: UIButton) {
        search_View.isHidden = false
//        if isShowFuelChargeStation{
//            isShowFuelChargeStation = false
//        }else{
//            isShowFuelChargeStation = true
//        }
//
//        self.showCurrentLocation()
    }
    
//    @IBAction func btnChat_Click(_ sender: UIButton) {
//
//    }
    
    @IBAction func btnCurrentLocation_Click(_ sender: UIButton) {
        
        if Defaults[.location_access] == true {
             showCurrentLocation()
        } else {
            if let url = URL(string: UIApplication.openSettingsURLString) {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:], completionHandler:  { (Bool) in
                        //print(Bool)
                        self.showCurrentLocation()
                    })
                }
            }
        }
    }
    
    //Reserve ride button action methods
//    @IBAction func btnShowReservedRides_Click(_ sender: UIButton) {
//
//        if isShowReservedRides == false {
//
//            viewShowAllRides.mainBGColor = ColorConstants.ThemeColor
//            imageCar.image = UIImage(named : "carWhiteSelected")
//           // self.showReservedRides()
//            isShowReservedRides = true
//            self.showPathForCar(currentCarousolIndex)
//
//            constraintBottomReserveLocation.constant = 4
//
//        } else {
//
//            viewShowAllRides.mainBGColor = UIColor.white
//            imageCar.image = UIImage(named : "LM_car-compact")
//
//            let vc = children.last
//            vc?.willMove(toParent: nil)
//            vc?.view.removeFromSuperview()
//            vc?.removeFromParent()
//
//            isShowReservedRides = false
//            self.showAllMarkers()
//
//            constraintBottomReserveLocation.constant = -120
//        }
//
//        self.viewShowAllRides.layoutSubviews()
//    }
    
//    @IBAction func btnReportReserve_Click(_ sender: UIButton) {
//
//        if let arrRides = SyncManager.sharedInstance.arrReserveRides, arrRides.count > 0{
//
//            let rideModel = RideListModel()
//            rideModel.vehicleId = Mapper<VehicleModel>().map(JSON: arrRides[currentCarousolIndex].vehicleId?.getDict() ?? [:])!
//
//            let vc = ReportDamageVC(nibName: "ReportDamageVC", bundle: nil)
//            vc.rideModel = rideModel
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//    }
    
//    @IBAction func btnCurrentLocationReserve_Click(_ sender: UIButton) {
//
//        let camera : GMSCameraPosition = GMSCameraPosition.camera(withLatitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude, zoom: (mapView.camera.zoom == 4) ? AppConstants.DefaultZoomLevel : mapView.camera.zoom)
//        mapView.camera = camera
//
//        self.showCurrentlocationMarker()
//    }
    

//    func btnShowOnMap_Click() {
//        showPathForNevigation()
//    }
    
    
//    func btnNavigate() {
//        guard !Utilities.checkStringEmptyOrNil(str: txtCurrentLocation.text) && startLocation != nil else {
//            self.showToast(text: "KLblEnterSourceLocation".localized)
//
////            Utilities.showAlertView(message: "KLblEnterSourceLocation".localized)
//            return
//        }
//
//        guard !Utilities.checkStringEmptyOrNil(str: txtDestination.text) && endLocation != nil else {
//            self.showToast(text: "KLblEnterDestinationLocation".localized)
//
////            Utilities.showAlertView(message: "KLblEnterDestinationLocation".localized)
//            return
//        }
//
//        Utilities.openMap(latitude: endLocation?.longitude ?? 0.0, longitude: endLocation?.latitude ?? 0.0)
//    }
    
//    @objc func btnClose_Click(_ sender: UIButton) {
//        //        mapView.selectedMarker?.icon = UIImage(named : "carMarker")
//        mapView.selectedMarker?.icon = self.imageWithImage(image: UIImage(named: "carMarker")!, scaledToSize: CGSize(width: 32, height: 39))
//
//        showCarBasicInfo(false)
//    }

//    @objc func btnBookNow_Click(_ sender: UIButton) {
//
//        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0)
//        SyncManager.sharedInstance.syncMaster ({
//
//            if AppNavigation.shared.isValidToReservation(){
//
//                let model = self.arrCollData[sender.tag]
//
//                let packageModel  = model.packages.filter { $0.isSelected}.first ?? PackagesModel()
//                let vc = DetailPopupVC(nibName: "DetailPopupVC", bundle: nil)
//                let nav = UINavigationController(rootViewController: vc)
//
//                let obj = RideListModel()
//                obj.vehicleId = Mapper<VehicleModel>().map(JSON: self.arrCities[sender.tag].getDict())!
//                obj.pricingConfigData = packageModel
//                obj.selectedCard = SyncManager.sharedInstance.getPrimaryCard()
//
//                var req = [String:Any]()
//                if self.rideType == AppConstants.RideType.Schedule {
//                    vc.detailType = .BookingDetail
//                    req["tripType"] = AppConstants.RideType.Schedule
//                    req["rideType"] = AppConstants.RideType.Schedule
//                }
//                else {
//                    vc.detailType = .ReservationDetail
//                    req["tripType"] = AppConstants.RideType.Instant
//                    req["rideType"] = AppConstants.RideType.Instant
//                }
//
//                if model.nextBookingDetail != nil{
//                    req["estimatedReturnTime"] = model.nextBookingDetail?.reservedDateTime
//                }
//
//                req["isAutoDeduct"] = AppConfigurationModel.shared.configs.isAutoDeduct
//
//                vc.onConfirm_Click = { value in
//
//                    self.isFromDriveNow = false
//
//                    if value == "Cancelled" {
//                        self.setLayoutFromActions(mapScreensViewActions: [.showHomeLayout,.showCurrentLocationButton])
//                    } else {
//                        req["cardId"] = value
//
//                        SyncManager.sharedInstance.callApiForReserverRide(req) {
//                            //                        AppNavigation.shared.moveToHome()
//                            //                            self.dismiss(animated: true) {
//                            self.showCarBasicInfo(false)
//                            self.setMapLayout()
//                            //                            }
//                        }
//                    }
//                }
//
//                if let startDT = self.reservationStartDate {
//                    obj.reservedDateTime = DateUtilities.convertToISOFormat(dateStr: startDT)
//
//                    if Date() > startDT{
//                        req["startDateTime"] = DateUtilities.convertToISOFormat(dateStr: Date())
//                    }else{
//                        req["startDateTime"] = DateUtilities.convertToISOFormat(dateStr: startDT)
//                    }
//                }
//                else {
//                    req["startDateTime"] = DateUtilities.convertToISOFormat(dateStr: Date())
//                }
//
//                if let endDT = self.reservationEndDate {
//                    obj.reservedEndDateTime = DateUtilities.convertToISOFormat(dateStr: endDT)
//                    req["endDateTime"] = DateUtilities.convertToISOFormat(dateStr: endDT)
//                }
//
//                req["vehicleId"] = self.arrCollData[sender.tag].id ?? ""
//                req["pricingConfigData"] = packageModel.getDict()
//                if model.isInsurance {
//                    req["isInsurance"] = true
//                }
//                else {
//                    req["isInsurance"] = false
//                }
//
//                SyncManager.sharedInstance.getFareSummary(reqDict: req, success: { (response) in
//                    //success
//
//                    if let fareSummary = Mapper<FareSummaryModel>().map(JSON: response) {
//
//                        obj.fareSummary = fareSummary
//                        obj.totalFare = fareSummary.totalFareEstimate
//                    }
//
//                    vc.rideModel = obj
//                    nav.modalPresentationStyle = .custom
//                    nav.modalTransitionStyle = .crossDissolve
//                    //
//                    self.present(nav, animated: true, completion: nil)
//
//                    //                    self.present(vc, animated: true, completion: {
//                    //                    })
//                }) {
//                    //failed
//                }
//            }
//        })
//    }
    
    @objc func btnDetails_Click(_ sender: UIButton) {
        
//        let vc = CheckMoreInformationVC(nibName: "CheckMoreInformationVC", bundle: nil)
//        let nav = UINavigationController(rootViewController: vc)
//        vc.isFromCarMap = true
//        vc.strVehicleId = arrCollData[sender.tag].id ?? ""
//        vc.rideType = self.rideType
//        vc.startDate = reservationStartDate
//        vc.endDate = reservationEndDate
//        vc.vehicleModel = arrCollData[sender.tag]
//
//        self.present(nav, animated: true, completion: nil)
    }
}

//MARK: Show current Location
extension CarMapVC {
    
    func showCurrentLocation() {
 
        camera = GMSCameraPosition.camera(withLatitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude, zoom: 14.0)
        mapView.camera = camera
        let position = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude)
        
        currentLocationMarker.icon = UIImage(named : "currentLocationMarker")
        currentLocationMarker.position = position
        currentLocationMarker.map = mapView
    }
    
    func showCurrentlocationMarker() {
        
       // mapView.isMyLocationEnabled = true
        let position = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude)
        
        //currentLocationMarker.zIndex = 1
        currentLocationMarker.icon = UIImage(named : "currentLocationMarker")
        
//        CATransaction.begin()
//        CATransaction.setAnimationDuration(1.0)
        currentLocationMarker.position = position
//        CATransaction.commit()
        
        currentLocationMarker.map = mapView
//        showPathForNevigation()
    }
    
    func showPathForNevigation() {
        
        guard !Utilities.checkStringEmptyOrNil(str: txtCurrentLocation?.text) && startLocation != nil else {
            self.showToast(text: "KLblEnterSourceLocation".localized)

//            Utilities.showAlertView(message: "KLblEnterSourceLocation".localized)
            return
        }
        
        guard !Utilities.checkStringEmptyOrNil(str: txtDestination?.text) && endLocation != nil else {
            self.showToast(text: "KLblEnterDestinationLocation".localized)

//            Utilities.showAlertView(message: "KLblEnterDestinationLocation".localized)
            return
        }
        
        if stopLocation?.latitude == nil && startLocation?.latitude != nil && endLocation?.latitude != nil  {
            self.drawPathWithouStopLocation()
        } else if stopLocation?.latitude != nil && startLocation?.latitude != nil && endLocation?.latitude != nil {
            self.drawPathWithStopLocation()
        }
    }
    
    func drawPathWithStopLocation() {
        Utilities.drawPathBetweenCoordinates(CLLocation(latitude: startLocation?.latitude ?? 0.0, longitude: startLocation?.longitude ?? 0.0), CLLocation(latitude: stopLocation?.latitude ?? 0.0, longitude: stopLocation?.longitude ?? 0.0), CLLocation(latitude: endLocation?.latitude ?? 0.0, longitude: endLocation?.longitude ?? 0.0)) { (dict) in
            
            self.mapView.clear()
            
            if self.startLocation != self.currentLocation{
           //     self.dropPathMarker(CLLocationCoordinate2D(latitude: self.startLocation?.latitude ?? 0.0, longitude: self.startLocation?.longitude ?? 0.0), imageName: "selectedCarMarker", scaledToSize: CGSize(width: 32, height: 39))
            }
            self.dropPathMarker(CLLocationCoordinate2D(latitude: self.stopLocation?.latitude ?? 0.0, longitude: self.stopLocation?.longitude ?? 0.0), imageName: "Stop",scaledToSize: CGSize(width: 25, height: 25))
            self.dropPathMarker(CLLocationCoordinate2D(latitude: self.endLocation?.latitude ?? 0.0, longitude: self.endLocation?.longitude ?? 0.0), imageName: "Destination",scaledToSize: CGSize(width: 25, height: 25))
            
            if let arr = dict?["routes"] as? [[String:Any]], arr.count > 0 {
                self.drawRoute(arr: arr)
            }
        }
    }
    
    func drawPathWithouStopLocation() {
        Utilities.getRoutesBetweenCoordinates(CLLocation(latitude: startLocation?.latitude ?? 0.0, longitude: startLocation?.longitude ?? 0.0), CLLocation(latitude: endLocation?.latitude ?? 0.0, longitude: endLocation?.longitude ?? 0.0)) { (dict) in
            
            self.mapView.clear()
            
            if self.startLocation != self.currentLocation{
                self.dropPathMarker(CLLocationCoordinate2D(latitude: self.startLocation?.latitude ?? 0.0, longitude: self.startLocation?.longitude ?? 0.0), imageName: "Marker", scaledToSize: CGSize(width: 30, height: 30))
            }
            self.dropPathMarker(CLLocationCoordinate2D(latitude: self.endLocation?.latitude ?? 0.0, longitude: self.endLocation?.longitude ?? 0.0), imageName: "Marker", scaledToSize: CGSize(width: 30, height: 30))
            
            if let arr = dict?["routes"] as? [[String:Any]], arr.count > 0 {
                self.drawRoute(arr: arr)
            }
        }
    }
}

//MARK: MapView methods

extension CarMapVC : GMSMapViewDelegate {
    
    func dropPathMarker(_ location : CLLocationCoordinate2D, imageName: String, scaledToSize: CGSize? = CGSize()) {
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(location.latitude, location.longitude)
        if scaledToSize == nil {
            marker.icon = UIImage(named: imageName)
        } else {
            marker.icon = self.imageWithImage(image: UIImage(named: imageName)!, scaledToSize: scaledToSize!)
        }
        marker.map = self.mapView
    }
    
    func dropMarker(_ location : CLLocation, _ isSelected: Bool = false, vehicleModel : VehicleModel) {
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        marker.icon = UIImage(named : "selected_car")
        marker.map = mapView
        marker.userData = vehicleModel
        arrMarkers.append(marker)
        
        //marker.zIndex = Int32(markerIndex)
        //        if isFromStartRide {
        //            return
        //  marker.icon = UIImage(named : "car_Station")
        //        } else {
        //            if vehicleModel.markerType == AppConstants.MarkerType.City{
        //                marker.icon = UIImage(named : "cityMarker")
        //            } else if vehicleModel.markerType == AppConstants.MarkerType.Vehicle{
        //
        //                if isSelected {
        //                    if let arrRides = SyncManager.sharedInstance.arrReserveRides, arrRides.count > 0{
        //                        marker.icon = self.imageWithImage(image: UIImage(named: ThemeManager.sharedInstance.getImage(string: "selected_car"))!, scaledToSize: CGSize(width: 32, height: 39))
        //
        //                    } else {
        //                        marker.icon = self.imageWithImage(image: UIImage(named: "selected_car")!, scaledToSize: CGSize(width: 32, height: 39))
        //                    }
        //
        //                } else if isFromDriveNow {
        //marker.icon = self.imageWithImage(image: UIImage(named: "selected_car")!, scaledToSize: CGSize(width: 32, height: 39))//selected_car
        //
        //                    //                    marker.icon = UIImage(named : "carMarker")
        //                } else {
        //                    marker.icon = UIImage(named : "cityDot")
        //                }
        //            } else if vehicleModel.markerType == AppConstants.MarkerType.VehicleStation {
        //                marker.icon = UIImage(named : "vehicleStation")
        //            } else if vehicleModel.markerType == AppConstants.MarkerType.FuelStation {
        //                marker.icon = self.imageWithImage(image: UIImage(named : ThemeManager.sharedInstance.getImage(string: "markerFuelStation"))!, scaledToSize: CGSize(width: 32, height: 39))
        //            } else if vehicleModel.markerType == AppConstants.MarkerType.EvChargeStations {
        //                marker.icon = self.imageWithImage(image: UIImage(named : ThemeManager.sharedInstance.getImage(string: "markerFuelStation"))!, scaledToSize: CGSize(width: 32, height: 39))
        //            }
        //        }
      
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if self.viewBasicCarInfo.isHidden == false{
            self.showCarBasicInfo(false)
            search_View.isHidden = true
            support_View.isHidden = true
            calendar_View.isHidden = true
            //gmsMarkerInfoWindow.removeFromSuperview()
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
       // if let eventInfoObject = marker.userData as? VehicleModel {
            
            gmsMarkerInfoWindow.frame = CGRect(x: 0, y: 0, width: 108, height: 32)
            gmsMarkerInfoWindow.nameLabel.text = "15 min"//eventInfoObject.name ?? ""
            gmsMarkerInfoWindow.center = mapView.projection.point(for: marker.position)
            gmsMarkerInfoWindow.center.y = gmsMarkerInfoWindow.center.y - sizeForOffset(view: gmsMarkerInfoWindow)
            self.mapView.addSubview(gmsMarkerInfoWindow)
            
       // } else {
            NSLog("Did tap a normal marker")            
       // }
        return false
    }
    
    func sizeForOffset(view: UIView) -> CGFloat {
        return 80.0
    }
    
    
//        if marker.userData == nil {
//            return true
//        }
//
//        let model = marker.userData as? VehicleModel
//
//        if isFromStartRide {
//            Utilities.openMap(latitude: model?.geoLocation?.longitude ?? 0.0, longitude: model?.geoLocation?.latitude ?? 0.0)
//
//            return true
//
//        } else {
//
//            if isFromDriveNow {
//
//                if model?.markerType == AppConstants.MarkerType.FuelStation || model?.markerType == AppConstants.MarkerType.EvChargeStations {
//
//                    //Charging Station And Fuel Station
//                    Utilities.openMap(latitude: model?.geoLocation?.longitude ?? 0, longitude: model?.geoLocation?.latitude ?? 0)
//                    return true
//                }
//
//                arrCollData = arrCities.filter { $0.markerType == AppConstants.MarkerType.Vehicle && $0.geoLocation != nil }
//                collectView.reloadData()
//
//                let filter = self.arrCollData.filter { $0.geoLocation != nil && ($0.id
//                    == model?.id) }
//
//                if filter.count > 0{
//                    let index = self.arrCollData.firstIndex(of: filter.first!)
//                    self.collectView.scrollToItem(at: IndexPath(row: index!, section: 0), at: .centeredHorizontally, animated: true)
//                }
//
//                if self.viewBasicCarInfo.isHidden == true{
//                    showCarBasicInfo(true)
//                    self.animateToSpecificMarker(model, true)
//                }
//            }
//
//            if model?.markerType == AppConstants.MarkerType.City{
//                self.mapView?.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 12.0)
//            } else if model?.markerType == AppConstants.MarkerType.Vehicle && isFromDriveNow == true{
//
//                if let selectedMarker = mapView.selectedMarker {
//                    selectedMarker.icon = self.imageWithImage(image: UIImage(named: "selected_car")!, scaledToSize: CGSize(width: 32, height: 39))
//
//                }
//                markerIndex = markerIndex + 1
//                marker.zIndex = Int32(markerIndex)
//
//                mapView.selectedMarker = marker
//                marker.icon = self.imageWithImage(image: UIImage(named: "selected_car")!, scaledToSize: CGSize(width: 32, height: 39))
//                marker.icon = self.imageWithImage(image: UIImage(named: ThemeManager.sharedInstance.getImage(string: "carmarker"))!, scaledToSize: CGSize(width: 32, height: 39))
//            }
//        }
//        return true
//    }
    
//    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
//        let zoom = mapView.camera.zoom
//        print("map idle position zoom is ",String(zoom))
//
//        if SyncManager.sharedInstance.activeRideObject == nil && !isFromDriveNow && SyncManager.sharedInstance.arrReserveRides?.count ?? 0 == 0 {
//            //viewCurrentLocation.isHidden = GMSCoordinateBounds(region: self.mapView.projection.visibleRegion()).contains(CLLocationCoordinate2D(latitude: self.currentLocation.coordinate.latitude, longitude: self.currentLocation.coordinate.longitude))
//        }
//
////        if isShowFuelChargeStation {
////            self.callApiForGetNearbyCityVehicle()
////        }
//    }
    
    func animateToSpecificMarker(_ model : VehicleModel?, _ isFromTap : Bool = false){
        
//        if !isFromTap{
//            guard let marker = arrMarkers.first(where: { ($0.userData as? VehicleModel)?.id == model?.id }) else { return }
//            _ = mapView(mapView, didTap: marker)
//        }
        let lat = Double(model?.latitude ?? "0.0")
        let long = Double(model?.longitude ?? "0.0")
        var bounds = GMSCoordinateBounds()
        bounds = bounds.includingCoordinate(CLLocationCoordinate2D(latitude: lat ?? 0.0, longitude: long ?? 0.0))
        
        self.mapView.animate(with: GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 70, left: 50, bottom: 450, right: 50)))
    }
}

//MARK: Tableview methods

//extension CarMapVC : UITableViewDelegate, UITableViewDataSource{
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return arrSearchPlace.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "PlacePickerCell") as! PlacePickerCell
//        cell.setCellData(model: arrSearchPlace[indexPath.row])
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//       // getLocationCoordinate(arrSearchPlace[indexPath.row])
//    }
//}

//MARK: - UICollectionView's DataSource & Delegate Methods

extension CarMapVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCities.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cars_data = arrCities[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cars_CollectionViewCell", for: indexPath) as! Cars_CollectionViewCell
        cell.instant_Btn.tag = indexPath.row
        cell.book_now_Btn.tag = indexPath.row
        cell.instant_Btn.addTarget(self, action: #selector(instant_Btn_Click(_:)), for: .touchUpInside)
        cell.book_now_Btn.addTarget(self, action: #selector(book_now_Btn_Click(_:)), for: .touchUpInside)
        cell.car_Name.text = cars_data.model ?? ""
        cell.perHour_Lbl.text = "\(cars_data.perHourCost ?? 0.0) Nok / Hour"
        let image = cars_data.images.first

        let url = URL(string: "\(AppConstants.imageURL)\(image ?? "")")
        cell.car_Image.setImageForURL(url: url, placeHolder: UIImage(named: "mind blowing car image"))
        //showPathForCar(indexPath.row)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: self.view.bounds.width, height: collectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        openCarDetail(indexPath.row)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        print("Scrolled")
        
        if scrollView == collectView{
            var visibleRect = CGRect()

            visibleRect.origin = collectView.contentOffset
            visibleRect.size = collectView.bounds.size

            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)

            guard let indexPath = collectView.indexPathForItem(at: visiblePoint) else { return }

            let model = arrCities[indexPath.row]
            self.animateToSpecificMarker(model)
        }
    }
    
    @objc func instant_Btn_Click(_ sender: UIButton) {
        
//        let vc = BookingconfirmationPopupVC(nibName: "BookingconfirmationPopupVC", bundle: nil)
//        vc.headerTitle = "Instant Booking Confirmation"
//        vc.desc1 = "You now enter into a binding agreement with Weone about vehicle rental."
//        vc.instantSetting = "Edit Instant Booking Settings"
//        vc.payableamout = "Payable Amount"
//        vc.carOpenlock = "Car opens/locks by App"
//        vc.amountNok = "190 NOK"
//        vc.desc2 = "We Reserve2000 Nok\n On Your Card."
//        vc.reserveNok = NSAttributedString(string: "")
//
//        vc.confirmClicked = {
////            if self.detailModel.vehicle?.iotProvider == AppConstants.IOTProvider.WithTelemetric {
////                let VC = TelematicsKeyPickUpVC()
////                VC.detailModel = self.detailModel
////                self.navigationController?.pushViewController(VC, animated: true)
//
//        }
//        //else {
////                let VC = TelematicsKeyPickUpVC()
////                VC.detailModel = self.detailModel
////                self.navigationController?.pushViewController(VC, animated: true)
//
//            //}
//        //}
//
//        vc.instantBtnClicked = {
//            self.dismiss(animated: true, completion: nil)
//            let VC = Instant_ViewController()
//            self.navigationController?.pushViewController(VC, animated: true)
//        }
//
//        vc.modalPresentationStyle = .custom
//        vc.modalTransitionStyle = .crossDissolve
//
//        if UIViewController.current()?.presentedViewController != nil {
//            UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
//            })
//        }
//        else{
//            UIViewController.current()?.present(vc, animated: true, completion: {
//            })
//        }
        //        let model = arrCollData[sender.tag]
                let vc = Instant_ViewController()
        //        vc.vehicleModel = model
        //        vc.isFromCourosal = true
        //        if self.rideType == AppConstants.RideType.Schedule {
        //            vc.reservationEndDate = self.reservationEndDate
        //            vc.reservationStartDate = self.reservationStartDate
        //        }
        //        vc.rideType = AppConstants.RideType.Instant
                self.navigationController?.pushViewController(vc, animated: true)
    }
    
     @objc func book_now_Btn_Click(_ sender: UIButton) {
            
//            let model = arrCollData[sender.tag]
            let vc = Book_Now_ViewController()
//            vc.strVehicleId = arrCollData[sender.tag].id ?? ""
//            Defaults[.vehicleId] = arrCollData[sender.tag].id ?? ""
    //        vc.vehicleModel = model
    //        vc.isFromCourosal = true
    //        if self.rideType == AppConstants.RideType.Schedule {
    //            vc.reservationEndDate = self.reservationEndDate
    //            vc.reservationStartDate = self.reservationStartDate
    //        }
    //
//            vc.rideType = AppConstants.RideType.Schedule
            self.navigationController?.pushViewController(vc, animated: true)
        }
    
    func openCarDetail(_ index : Int) {
        
        let vc = CarBasicDetailVC()
        vc.Vehicle = arrCities[index]
       // vc.isFromCarMap = true
        //vc.strVehicleId = arrCities[index].vehicleid ?? 0
//        vc.rideType = self.rideType
//        vc.startDate = reservationStartDate
//        vc.endDate = reservationEndDate
       // vc.vehicleDetail = arrCities[index]

        navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- Extra Methods
extension CarMapVC {
    
//    func showReservedRides() {
//
//        if let arrRides = SyncManager.sharedInstance.arrReserveRides, arrRides.count > 0{
//
//            let reserveRideVC = ReserveRideVC()
//            reserveRideVC.modalPresentationStyle = .overCurrentContext
//            self.addChild(reserveRideVC)
//
//            self.view.addSubview(reserveRideVC.view)
//            reserveRideVC.didMove(toParent: self)
//            reserveRideVC.setAllReserveRides(arrRides)
//
//            let height = reserveRideVC.view.frame.height
//            let width  = self.view.frame.width
//            reserveRideVC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
//
//            reserveRideVC.onConfirm_Click = {
//                self.isFromDriveNow = false
////                self.setMapLayout()
//            }
//
//            reserveRideVC.onStartRide_Click = {
//                self.removeReserveRideVC()
//                self.isFromStartRide = true
////                self.setLayoutFromActions(mapScreensViewActions: [.showDrawer, .showOngoingRideLayout])
//            }
//
//            reserveRideVC.currentNavigationButton = {
//                let camera : GMSCameraPosition = GMSCameraPosition.camera(withLatitude: self.currentLocation.coordinate.latitude, longitude: self.currentLocation.coordinate.longitude, zoom: (self.mapView.camera.zoom == 4) ? AppConstants.DefaultZoomLevel : self.mapView.camera.zoom)
//                self.mapView.camera = camera
//
//                self.showCurrentlocationMarker()
//
//            }
//
//            reserveRideVC.selectNavigation = {
//                //                self.mapView.clear()
//                self.removeReserveRideVC()
////                self.setLayoutFromActions(mapScreensViewActions: [.showNavigationLayout])
//            }
//
//            //Swipe detect
//            reserveRideVC.swipeDetected = { index in
//                self.currentCarousolIndex = index
//                self.showPathForCar(index)
//            }
//
//            //Refresh all rides (Get called when ride cancelled by user)
//            reserveRideVC.refreshAllRides = {
//                self.initialConfig()
//
//                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
//                    self.mapView.animate(toZoom: 10)
//                }
//            }
//
//            //called when timer of particular ride is completed
//            reserveRideVC.reserveRideCompleted = {
//
//                Utilities.showAlertWithButtonAction(title: "", message: "KMsgReservationTimeout".localized, buttonTitle: StringConstants.ButtonTitles.KOk) {
//
//                    NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
//                        SyncManager.sharedInstance.syncMaster ({
//
//                            AppNavigation.shared.moveToHome(isPopAndSwitch: true)
//
//                           /* self.isFromDriveNow = false
//                            self.setMapLayout()
//                            self.setLayoutFromActions(mapScreensViewActions: [.showHomeLayout, .showCurrentLocationButton])
//                            //                            self.initialConfig()
//
//                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
//                                self.mapView.animate(toZoom: 10)
//                            } */
//                        })
//                    })
//                }
//            }
//        } else {
//            removeReserveRideVC()
//        }
//    }
    
    
//    func removeReserveRideVC() {
//        let vc = children.last
//        vc?.willMove(toParent: nil)
//        vc?.view.removeFromSuperview()
//        vc?.removeFromParent()
//
//        // self.mapView.clear()
//
//    }
    //MARK: set Start Ride data
//    func setDataAccordingToRideStatus(){
//        showActiveRideView()
//    }
    
//    func setPathForOnGoingRideNavigation() {
//
//        mapView.clear()
//        let updatedModel = SyncManager.sharedInstance.activeRideObject
//        var placeModel = PlacesModel()
//
//        self.startLocation = PlacesModel()
//        self.stopLocation = PlacesModel()
//        self.endLocation = PlacesModel()
//
//        if updatedModel?.navigateLocations.count == 3 {
//
//            placeModel.addressFullText = updatedModel?.navigateLocations[0].name
//            placeModel.longitude = updatedModel?.navigateLocations[0].coordinates?.first
//            placeModel.latitude = updatedModel?.navigateLocations[0].coordinates?.last
//
//            self.startLocation = placeModel
//
//            placeModel = PlacesModel()
//
//            placeModel.addressFullText = updatedModel?.navigateLocations[1].name
//            placeModel.longitude = updatedModel?.navigateLocations[1].coordinates?.first
//            placeModel.latitude = updatedModel?.navigateLocations[1].coordinates?.last
//
//            self.stopLocation = placeModel
//
//            placeModel = PlacesModel()
//
//            placeModel.addressFullText = updatedModel?.navigateLocations[2].name
//            placeModel.longitude = updatedModel?.navigateLocations[2].coordinates?.first
//            placeModel.latitude = updatedModel?.navigateLocations[2].coordinates?.last
//
//            self.endLocation = placeModel
//
//
//        } else if updatedModel?.navigateLocations.count == 2 {
//
//            placeModel = PlacesModel()
//
//            placeModel.addressFullText = updatedModel?.navigateLocations[0].name
//            placeModel.longitude = updatedModel?.navigateLocations[0].coordinates?.first
//            placeModel.latitude = updatedModel?.navigateLocations[0].coordinates?.last
//
//            self.startLocation = placeModel
//
//            placeModel = PlacesModel()
//
//            placeModel.addressFullText = updatedModel?.navigateLocations[1].name
//            placeModel.longitude = updatedModel?.navigateLocations[1].coordinates?.first
//            placeModel.latitude = updatedModel?.navigateLocations[1].coordinates?.last
//
//            self.endLocation = placeModel
//
//        }
//
//        if self.stopLocation?.latitude == nil && self.startLocation?.latitude != nil && self.endLocation?.latitude != nil  {
//            self.drawPathWithouStopLocation()
//        } else if self.stopLocation?.latitude != nil && self.startLocation?.latitude != nil && self.endLocation?.latitude != nil {
//            self.drawPathWithStopLocation()
//        }
//
//    }
    
//    func showActiveRideView(){
//
//        if let model = SyncManager.sharedInstance.activeRideObject{
//
//            print(model.rideType)
//
//            let pauseEndRideVC = PauseEndRideVC()
//
//            pauseEndRideVC.modalPresentationStyle = .overCurrentContext
//            self.addChild(pauseEndRideVC)
//            self.view.addSubview(pauseEndRideVC.view)
//            pauseEndRideVC.didMove(toParent: self)
//            pauseEndRideVC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: self.view.frame.width, height: pauseEndRideVC.view.frame.height)
//
//            if model.navigateLocations.count > 0 {
//                self.setPathForOnGoingRideNavigation()
//            }
//            pauseEndRideVC.getNavigation = { arrNavigateLocations in
//                self.setPathForOnGoingRideNavigation()
//            }
//
//            pauseEndRideVC.setData(model)
//
//            getStation()
//        }
//    }
    
//    func getStation() {
//
//        var request = Parameters()
//        request["partnerId"] = SyncManager.sharedInstance.activeRideObject?.partnerId
//
//        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.StationOfPartner, method: .post, parameters: request, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
//
//            if let dictResponse = response as? [[String:Any]]{
//
//                let arr = Mapper<VehicleModel>().mapArray(JSONArray: dictResponse)
//                var bounds = GMSCoordinateBounds()
//
//                for item in arr {
//
//                    if item.geoLocation != nil{
//
//                        let location = CLLocation(latitude: item.geoLocation?.latitude ?? 0.0, longitude: item.geoLocation?.longitude ?? 0.0)
//                        self.dropMarker(location, vehicleModel : item)
//                        bounds = bounds.includingCoordinate(CLLocationCoordinate2D(latitude: item.geoLocation?.latitude ?? 0.0, longitude: item.geoLocation?.longitude ?? 0.0))
//                    }
//                }
//            }
//
//        }) { (failureMessage, failureCode) in
//            Utilities.showAlertView(message: failureMessage)
//        }
//    }
    
    func showCarBasicInfo(_ value : Bool) {
        
        self.viewBasicCarInfo.transform = CGAffineTransform.init(scaleX: 0, y: 0)
        viewBasicCarInfo.isHidden = false
        UIView.animate(withDuration: 0.5, animations: {
            self.viewBasicCarInfo.transform = .identity
            self.view.layoutIfNeeded()
        }) { (completed) in
            
        }
    }
}

//MARK: When ride is active, show path

extension CarMapVC {
    
    //MARK: Show path on car
    
    func showPathForOngoignRide() {
        
        if let obj = SyncManager.sharedInstance.activeRideObject, obj != nil{
            
            let location = CLLocation(latitude: obj.geoLocation?.latitude ?? 0.0, longitude: obj.geoLocation?.longitude ?? 0.0)
            
            //Checking distance betwwen current location and car location
            let distanceInMeters = currentLocation.distance(from: location) // result is in meters
            
            //If distance is less then 10 meters then user has reached to destination
            if distanceInMeters < 10{
                LocationManager.SharedManager.stopLiveLocationUpdate()
            }
            
            Utilities.getRoutesBetweenCoordinates(currentLocation, location) { (dict) in
                
                self.mapView.clear()
                self.markerNeedToDropWhenPathShown(0)
                
                if let arr = dict?["routes"] as? [[String:Any]], arr.count > 0 {
                    self.drawRoute(arr: arr)
                    self.dropPathMarker(CLLocationCoordinate2D(latitude: obj.geoLocation?.latitude ?? 0.0, longitude: obj.geoLocation?.longitude ?? 0.0), imageName: ThemeManager.sharedInstance.getImage(string: "SelectedMarker"), scaledToSize: CGSize(width: 32, height: 39))
                    
                } else {
                    self.showPathForCenterLocation(carLocation: location)
                }
            }
        }
    }
    
    func showPathForCar(_ index : Int = 0) {
        
//        if let arr = SyncManager.sharedInstance.arrReserveRides, arr.count > 0{
            
            let location = CLLocation(latitude: arrCities[index].geoLocation?.latitude ?? 0.0, longitude: arrCities[index].geoLocation?.longitude ?? 0.0)
            
            //Checking distance betwwen current location and car location
            let distanceInMeters = currentLocation.distance(from: location) // result is in meters
            
            //If distance is less then 10 meters then user has reached to destination
            if distanceInMeters < 10{
                LocationManager.SharedManager.stopLiveLocationUpdate()
            }
            
            Utilities.getRoutesBetweenCoordinates(currentLocation, location) { (dict) in
                
                self.mapView.clear()
                self.markerNeedToDropWhenPathShown(index)
                
                if let arr = dict?["routes"] as? [[String:Any]], arr.count > 0 {
                    self.drawRoute(arr: arr)
                } else {
                    self.showPathForCenterLocation(carLocation: location)
                }
            }
//        }
    }
    
    func showPathForCenterLocation(carLocation: CLLocation) {
        
        LocationRoute.SharedManager.geocode(location: carLocation)
        
        LocationRoute.SharedManager.getLocation = { (location) in
            
            Utilities.getRoutesBetweenCoordinates(carLocation, CLLocation(latitude: location["lat"] ?? 0.0, longitude: location["lng"] ?? 0.0)) { (dict) in
                
                if let arr = dict?["routes"] as? [[String:Any]], arr.count > 0 {
                    self.drawRoute(arr: arr)
                }
            }
        }
    }
    
    
    func drawRoute(arr: [[String:Any]]) {
        
        if let path = GMSPath.init(fromEncodedPath: (arr.first?["overview_polyline"] as? [String:Any] ?? [:])?["points"] as? String ?? "") {
            
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 3.0
            polyline.strokeColor = ColorConstants.ThemeColor
            polyline.map = self.mapView
            
            let bounds = GMSCoordinateBounds(path: path)
            
            if viewNevigation.isHidden == false {
                
                if viewStopLocation.isHidden == false {
                    self.mapView.animate(with: GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 360, left: 50, bottom: 100 , right: 50)))
                } else {
                    self.mapView.animate(with: GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 300 , left: 50, bottom: 100 , right: 50)))
                }
            } else {
                
                if SyncManager.sharedInstance.activeRideObject != nil {
                    //Ongoing ride view height
                    self.mapView.animate(with: GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top:  100, left: 50, bottom: 405, right: 50)))
                } else {
                    self.mapView.animate(with: GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top:  100, left: 50, bottom: 260, right: 50)))
                }
            }
                        
        } else {
            print("error some error in fetching data")
        }
    }
    
    func showAllMarkers(){
        
        arrMarkers.removeAll()
        //mapView.clear()
        
        for item in self.arrCities {
            
            if item.geoLocation != nil {
                
                let location = CLLocation(latitude: item.geoLocation?.latitude ?? 0.0, longitude: item.geoLocation?.longitude ?? 0.0)
                self.dropMarker(location,  vehicleModel: item)
            }
        }
        
        self.showCurrentlocationMarker()
        self.mapView.animate(toZoom: 16.0)
    }
    
    func markerNeedToDropWhenPathShown(_ index : Int = 0){
        
        self.showCurrentlocationMarker()
        if let arr = SyncManager.sharedInstance.arrReserveRides, arr.count > 0 {
            
            let location = CLLocation(latitude: arr[index].geoLocation?.latitude ?? 0.0, longitude: arr[index].geoLocation?.longitude ?? 0.0)
            
            self.dropMarker(location, true, vehicleModel : arr[index].vehicleId!)
        }
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}

//MARK: Api calls
extension CarMapVC {
    
    func callApiForGetNearbyCityVehicle(_ completion : (() -> ())? = nil){
        
        var param : [String:Any] = [:]
        param["zoomLevel"] = 12//mapView.camera.zoom
        param["long"] =  "77.55"//currentLocation.coordinate.longitude
        param["lat"] = "30.85"//currentLocation.coordinate.latitude
        param["rideType"] = 1
        
//        if isShowChargeStation {
//            let coordinate = mapView.projection.coordinate(for: mapView.center)
//            param["currentLocation"] = [coordinate.longitude , coordinate.latitude]
//        } else {
//            param["currentLocation"] = [currentLocation.coordinate.longitude , currentLocation.coordinate.latitude]
//        }
//        //        if isFromDriveNow{
//        param["rideType"] = self.rideType
//        //        }
//
//        if let startDT = reservationStartDate {
//            param["reservedDateTime"] = DateUtilities.convertToISOFormat(dateStr: startDT)
//        }
//
//        if let endDT = reservationEndDate {
//            param["reservedEndDateTime"] = DateUtilities.convertToISOFormat(dateStr: endDT)
//        }
//
//        if cityID != "" && self.rideType == AppConstants.RideType.Schedule {
//            param["cityId"] = self.cityID
//        }
//
//
//        if isFromDriveNow {
//
//            if Defaults[.Filter] != nil {
//                imgFilter.image = #imageLiteral(resourceName: "Filter-1")
//                viewFilter.backgroundColor = ColorConstants.TextColorPrimary
//
//                param["filter"] = Defaults[.Filter]
//            } else {
////                imgFilter.image = #imageLiteral(resourceName: "filter")
//                viewFilter.backgroundColor = UIColor.clear
//
//            }
//        }
//
//        if isShowChargeStation{
//            viewFuelStation.backgroundColor = ColorConstants.TextColorPrimary
////            imgFuel.image = #imageLiteral(resourceName: "gas-station-1")
//        } else {
//            viewFuelStation.backgroundColor = UIColor.clear
////            imgFuel.image = #imageLiteral(resourceName: "fuel")
//        }
        
       // viewFuelStation.layoutSubviews()
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.GetNearbyCityVehicle, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            self.arrMarkers.removeAll()
            //self.mapView.clear()
            print(response)
            if let dict = response as? [String:Any]{
                if dict["message"] as? String ?? "" == "No Vehicle available" {
                    self.swipeTosee_Lbl.isHidden = true
                } else {
                    if let list = dict["vehicles"] as? [[String:Any]]{
                    self.arrCities = Mapper<VehicleModel>().mapArray(JSONArray: list)
                       for data in self.arrCities {
                           let lat = Double(data.latitude ?? "0.0")
                           let long = Double(data.longitude ?? "0.0")
                           let location = CLLocation(latitude: lat ?? 0.0, longitude: long ?? 0.0)
                           self.currentLocation = location
                           self.dropMarker(location,  vehicleModel: data)
                        
                       }
                    }
                }
            }
            
            self.showCurrentLocation()
            
            completion?()
            
            //self.setMapLayout()
            self.collectView.reloadData()
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}

//MARK: textfield delegate methods
//extension CarMapVC : UITextFieldDelegate{
//
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        self.tableView.isHidden = true
//        self.arrSearchPlace.removeAll()
//        self.tableView.reloadData()
//
//        if textField == txtCurrentLocation && textField.text == "KLblCurrentLocation".localized {
//            textField.text = ""
//        } else if textField == txtStopLocation && textField.text == "KLblAddStop".localized {
//            textField.text = ""
//        } else if textField == txtDestination && textField.text == "KLblWhereTo".localized {
//            textField.text = ""
//        }
//    }
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//
//        if !string.canBeConverted(to: String.Encoding.ascii) {
//            return false
//        }
//        if range.location == 0{
//
//            if string == " " {
//                return false
//            }
//        }
//
//        let text = textField.text!
//        let textRange = Range(range, in: text)
//        let updatedText = text.replacingCharacters(in: textRange!, with: string)
//
//        if updatedText.count != 0 {
//
////            DispatchQueue.main.async {
////                self.placeAutocomplete(searchText: updatedText)
////            }
//
//        } else {
//
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
//
//                self.arrSearchPlace.removeAll()
//                self.tableView.reloadData()
//                self.tableView.isHidden = true
//            })
//        }
//
//       /* if textField == txtCurrentLocation{
//            txtCurrentLocation.textField.textColor = ColorConstants.TextColorPrimary
//        }else if textField == txtStopLocation{
//            txtStopLocation.textField.textColor = ColorConstants.TextColorPrimary
//        }else if textField == txtDestination{
//            txtDestination.textField.textColor = ColorConstants.TextColorPrimary
//        }*/
//
//        return true
//    }
//
//    func textFieldDidEndEditing(_ textField: UITextField) {
//       /* if textField.text?.count == 0 {
//            if textField == txtCurrentLocation{
//                textField.text = "KLblCurrentLocation".localized
//                txtCurrentLocation.textField.textColor = ColorConstants.TextColorSecondary
//            }else if textField == txtStopLocation.textField{
//                textField.text = "KLblAddStop".localized
//                txtStopLocation.textField.textColor = ColorConstants.TextColorSecondary
//            }else if textField == txtDestination.textField{
//                textField.text = "KLblWhereTo".localized
//                txtDestination.textField.textColor = ColorConstants.TextColorSecondary
//            }
//        }else{
//            txtCurrentLocation.textField.textColor = ColorConstants.TextColorPrimary
//            txtStopLocation.textField.textColor = ColorConstants.TextColorPrimary
//            txtDestination.textField.textColor = ColorConstants.TextColorPrimary
//        } */
//    }
//}

//MARK: Google auto places complete api
//extension CarMapVC {
//
//    //Place Auto Complete
//    func placeAutocomplete(searchText : String) {
//
//        arrSearchPlace.removeAll()
//        tableView.reloadData()
//
//        placesClient.autocompleteQuery(searchText, bounds: nil, filter: nil, callback: {(results, error) -> Void in
//            if let error = error {
//                print("Autocomplete error \(error)")
//                return
//            }
//            if let results = results {
//                for result in results {
//                    let model = PlacesModel()
//                    model.addressFullText = result.attributedFullText.string
//                    model.addressPrimaryText = result.attributedPrimaryText.string
//                    model.addressSecondaryText = result.attributedSecondaryText?.string
//                    model.placeId = result.placeID
//                    self.arrSearchPlace.append(model)
//                }
//                self.tableView.isHidden = false
//                self.tableView.reloadData()
//            }
//        })
//    }
//
//    func isValid() {
//        if !Utilities.checkStringEmptyOrNil(str: txtCurrentLocation?.text) {
//            return
//        }
//
//        if !Utilities.checkStringEmptyOrNil(str: txtStopLocation?.text) {
//            return
//        }
//
//        if !Utilities.checkStringEmptyOrNil(str: txtDestination?.text) {
//            return
//        }
//
//    }
//
//
//    //Get Location Coordinate
//    func getLocationCoordinate(_ model : PlacesModel){
//
//        placesClient.lookUpPlaceID(model.placeId ?? "", callback: { (place, error) -> Void in
//            if let error = error {
//                print("lookup place id query error: \(error.localizedDescription)")
//                return
//            }
//
//            guard let _ = place else {
//                print("No place details for \(model.placeId ?? "")")
//                return
//            }
//
//            model.latitude  = place!.coordinate.latitude
//            model.longitude = place!.coordinate.longitude
//
//        if self.txtCurrentLocation.isEditing {
//
//                self.txtCurrentLocation.endEditing(true)
//
//                self.startLocation = model
//                self.txtCurrentLocation.text = self.startLocation?.addressFullText
//
//                self.getDestination?(model, true)
//
//
//            } else if self.txtStopLocation.isEditing {
//
//                self.txtStopLocation.endEditing(true)
//
//                self.stopLocation = model
//                self.txtStopLocation.text = self.stopLocation?.addressFullText
//
//                self.getDestination?(model, true)
//
//
//            }else {
//
//                self.txtDestination.endEditing(true)
//
//                self.endLocation = model
//                self.txtDestination.text = self.endLocation?.addressFullText
//
//                self.getDestination?(model, false)
//
//            }
//
//            self.arrSearchPlace.removeAll()
//            self.tableView.reloadData()
//            self.tableView.isHidden = true
//            self.isValid()
//
//        })
//    }
//}
