//
//  ReportIssueSelectPartVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 15/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class ReportIssueSelectPartVC: UIViewController {

    //MARK: Variables
    var arrCellData : [CellModel] = [CellModel]()
    var onClick:((_ damageType : Int)->())?
    
    //MARK: outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintHeightViewMain: NSLayoutConstraint!
    @IBOutlet weak var viewMain: UIView!
    
    //MARK: view life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
    }
    
    //MARK: initial config
    func initialConfig(){
        tableView.register(UINib(nibName: "ReportIssueCell", bundle: nil), forCellReuseIdentifier: "ReportIssueCell")
        
        viewMain.roundCorners([.topLeft, .topRight], radius: 5.0, width: AppConstants.ScreenSize.SCREEN_WIDTH)
        
        prepareDataSource()
        
        tableView.tableFooterView = UIView(frame : CGRect(x: 0, y: 0, width: 0, height: 12))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showView(true)
    }
    
    //MARK: prepare DataSource
    func prepareDataSource(){
        arrCellData.removeAll()
        
        arrCellData.append(CellModel.getModel(placeholder: "KLblScratch".localized, type: .ReportIssueScratch, cellObj:AppConstants.DamageCategory.SCRATCH))
        arrCellData.append(CellModel.getModel(placeholder: "KLblDent".localized, type: .ReportIssueDent, cellObj:AppConstants.DamageCategory.DENT))
        arrCellData.append(CellModel.getModel(placeholder: "KLblChip".localized, type: .ReportIssueChip, cellObj:AppConstants.DamageCategory.CHIP))
        arrCellData.append(CellModel.getModel(placeholder: "KLblMissingPart".localized, type: .ReportIssueMissingPart, cellObj:AppConstants.DamageCategory.MISSING_PART))
        arrCellData.append(CellModel.getModel(placeholder: "KLblBrokenPart".localized, type: .ReportIssueBrokenPart, cellObj:AppConstants.DamageCategory.BROKEN_PART))
        arrCellData.append(CellModel.getModel(placeholder: "KLblInterior".localized, type: .ReportIssueInterior, cellObj:AppConstants.DamageCategory.INTERIOR))
        arrCellData.append(CellModel.getModel(placeholder: "KLblCancelCap".localized, type: .ReportIssueCancel))

        tableView.reloadData()
        
        constraintHeightViewMain.constant = CGFloat((arrCellData.count * 60) + 12)
        viewMain.transform = CGAffineTransform.init(translationX: 1, y: 1200)
    }
    
    func showView(_ value : Bool, completion : (() -> ())? = nil){
        
        if value {
            UIView.animate(withDuration: 0.8, delay: 0, options: .curveEaseInOut, animations: {
                self.viewMain.transform = .identity
                self.view.layoutIfNeeded()
            }, completion: nil)
        }else{
            UIView.animate(withDuration: 0.8, delay: 0, options: .curveEaseInOut, animations: {
                self.viewMain.transform = CGAffineTransform.init(translationX: 1, y: 1200)
                self.view.layoutIfNeeded()
            }, completion: { value in
                completion?()
            })
        }
    }
}

//MARK: Tableview methods
extension ReportIssueSelectPartVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCellData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrCellData[indexPath.row]

        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportIssueCell", for: indexPath) as! ReportIssueCell
        cell.setCellData(model)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let model = arrCellData[indexPath.row]
        if model.cellType == .ReportIssueCancel {
            showView(false) {
                self.dismiss(animated: true, completion: {
                })
            }
        }
        else {
            showView(false) {
                self.dismiss(animated: true, completion: {
                    self.onClick?(model.cellObj as? Int ?? 0)
                })
            }
        }
    }
}
