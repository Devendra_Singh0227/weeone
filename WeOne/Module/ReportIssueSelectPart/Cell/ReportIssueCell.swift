//
//  ReportIssueCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 15/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class ReportIssueCell: UITableViewCell {

    @IBOutlet weak var btnCommon: UIButtonCommon!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        btnCommon.button.isUserInteractionEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(_ model : CellModel){
        
        btnCommon.layer.cornerRadius = 5.0
    
        if model.cellType == .ReportIssueCancel{
            btnCommon.button.setTitle(model.placeholder ?? "", for: .normal)
            btnCommon.setBackGroundColor(color: UIColor.white)
            btnCommon.layer.borderWidth = 1.0
            btnCommon.layer.borderColor = ColorConstants.ThemeColor.cgColor
            btnCommon.button.setTitleColor(ColorConstants.ThemeColor, for: .normal)
            
        }else{
        
            btnCommon.setText(text: model.placeholder ?? "")
            btnCommon.button.setTitleColor(UIColor.white, for: .normal)
            btnCommon.setBackGroundColor(color: ColorConstants.ThemeColor)
            btnCommon.layer.borderWidth = 0.0
            
        }
        
        self.layoutSubviews()
    }
}
