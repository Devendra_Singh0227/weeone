//
//  CarBasicDetailVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 27/11/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit
import TagListView
import FAPaginationLayout

class CarBasicDetailVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, TagListViewDelegate {
    
    //MARK: - Variables
    var arrcarFeatures = [CellModel]()
    var strVehicleId = 0
    var detailModel : VehicleDetailModel = VehicleDetailModel()
    var startDate:Date?
    var endDate:Date?
    var rideType = AppConstants.RideType.Schedule
    var Vehicle : VehicleModel = VehicleModel()
    
    //MARK: - Outlets
    
    @IBOutlet var viewHeader: UIView!
    @IBOutlet var ViewFooter: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgVehicle: UIImageView!
    
    @IBOutlet weak var lblVehicleName: UILabel!
    @IBOutlet weak var lblPricePerDay: UILabel!
    @IBOutlet weak var lblFuelPercentage: UILabel!
    @IBOutlet weak var cars_CollectionView: UICollectionView!
    @IBOutlet weak var rental_Report_CollectionView: UICollectionView!
    
    @IBOutlet weak var vwMoreInfoBottom: NSLayoutConstraint!
    @IBOutlet weak var viewBookNow: UIView!
    @IBOutlet weak var circularProgressBar: CircularProgressBar!
    @IBOutlet weak var labelPerDay: UILabel!
    @IBOutlet weak var lblCarModel: UILabel!
    
    @IBOutlet weak var previous_Rental_Report_TopConstraint: NSLayoutConstraint!
    @IBOutlet weak var more_Information_View: UIView!
    @IBOutlet weak var more_Info_DropImge: UIImageView!
    var Feddback_Array = ["Dirty Interior", "Refill Windscreen"]
    @IBOutlet weak var tags_View: TagListView!
    @IBOutlet weak var previous_Rental_View: UIView!
    @IBOutlet weak var support_View: UIView!
    @IBOutlet weak var previous_Rental_DropImage: UIImageView!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var carType_Lbl: UILabel!
    @IBOutlet weak var manualGear_Lbl: UILabel!
    @IBOutlet weak var seats_Lbl: UILabel!
    @IBOutlet weak var winter_tyre_Lbl: UILabel!
    @IBOutlet weak var suitcase_Lbl: UILabel!
    @IBOutlet weak var manualKey_Lbl: UILabel!
    @IBOutlet weak var infantSeat_Lbl: UILabel!
    @IBOutlet weak var gasoline_Lbl: UILabel!
    @IBOutlet weak var previousRentalReport_View: UIView!
    @IBOutlet weak var divider_Line: UILabel!
    var isselected = false
    @IBOutlet weak var rentalReport_FirstImage: UIImageView!
    @IBOutlet weak var rentalReport_SecondImage: UIImageView!
    @IBOutlet weak var rentalReport_ThirdImage: UIImageView!
    @IBOutlet weak var rentalReport_FourthImage: UIImageView!
    @IBOutlet weak var name_Lbl: UILabel!
    @IBOutlet weak var time_Lbl: UILabel!
    @IBOutlet weak var distance_Lbl: UILabel!
    @IBOutlet weak var description_Lbl: UILabel!
    @IBOutlet weak var userImage: ImageViewDesign!
    @IBOutlet weak var car_Name_Lbl: UILabel!
    @IBOutlet weak var car_Number_Lbl: UILabel!
    @IBOutlet weak var perHour_Lbl: UILabel!
    @IBOutlet weak var availabel_Lbl: UILabel!
    @IBOutlet weak var distanceAway_Lbl: UILabel!
    @IBOutlet weak var estimatedFuel_Lbl: UILabel!
    
    //MARK:- Controller's Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }
    
    //MARK: Private Methods
    func initialConfig() {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
        viewConfigrations()
        tags_View.addTags(Feddback_Array)
        tags_View.delegate = self
        
        tableView.tableHeaderView = viewHeader
        
        previousRentalReport_View.isHidden = true
        let footer = ViewFooter!
        footer.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 200)

        tableView.tableFooterView = footer
        
        tableView.register(UINib(nibName: "CarFeatureTableCell", bundle: nil), forCellReuseIdentifier: "CarFeatureTableCell")
        
        tableView.register(UINib(nibName: "ExtraCell", bundle: nil), forCellReuseIdentifier: "ExtraCell")
        
        cars_CollectionView.register(UINib(nibName: "CarFeatureCollectionCell", bundle: nil), forCellWithReuseIdentifier: "CarFeatureCollectionCell")

        rental_Report_CollectionView.register(UINib(nibName: "Previous_Rental_Reports_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Previous_Rental_Reports_CollectionViewCell")
        callApiForCarDetail()
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.support_View.isHidden = true
    }
    
    func preparecarFeatureDataSource() {
        arrcarFeatures.removeAll()
        
        let master = SyncManager.sharedInstance.fetchMasterWithId(detailModel.vehicle?.category ?? "")
        
        if master.count > 0{
            arrcarFeatures.append(CellModel.getModel(placeholder: master.first!.name ?? "", type: .CFFamilyCar, imageName: "car_white", cellObj: nil))
        }
        
        arrcarFeatures.append(CellModel.getModel(placeholder: "\(detailModel.vehicle?.tyres ?? "") tires", type: .CFWinterTires, imageName: "tyre", cellObj: nil))
        
        arrcarFeatures.append(CellModel.getModel(placeholder: "\(detailModel.vehicle?.seats ?? 0) seats", type: .CFFiveSeat, imageName: "seat", cellObj: nil))
        
        arrcarFeatures.append(CellModel.getModel(placeholder: detailModel.vehicle?.gearBox == AppConstants.GearBoxType.AUTOMATIC ? "\("KAutomaticTrasmission".localized)" : "\("KManualTrasmission".localized)", type: .CFAutomaticTransmission, imageName: "gearshift", cellObj: nil))
        
        arrcarFeatures.append(CellModel.getModel(placeholder: "\(detailModel.vehicle?.travelBags ?? 0) suitcases", type: .CFThreeSuitcase, imageName: "portmanteau", cellObj: nil))
        
        if detailModel.vehicle?.fuelType != nil{
            
            arrcarFeatures.append(CellModel.getModel(placeholder: detailModel.vehicle?.fuelType == AppConstants.FuelType.Combustion ? "KLblCombustion".localized : "KLblElectric".localized, type: .CFThreeSuitcase, imageName: "fuelType", cellObj: nil))
        }
        
        tableView.reloadData()
        
        setData()
    }
    
    func setData(){
//        let gear = detailModel.features?.gearType
//        let key = detailModel.vehicle?.vehicleKey
//        if gear == 2 {
//             manualGear_Lbl.text = "Manual Gear"
//        } else {
//             manualGear_Lbl.text = "Automatic"
//        }
//        if key == 2 {
//            manualKey_Lbl.text = "Manual Key"
//        } else {
//            manualKey_Lbl.text = "Automatic"
//        }
        let text = Utilities.attributedString(firstString: "Available For ", secondString: "\(detailModel.availability ?? 0) Hours", firstFont: FontScheme.FontConstant.kRegularFont, secondFont: FontScheme.FontConstant.kSemiBoldFont, firstColor: ColorConstants.textcolor, secondColor: ColorConstants.secondTextColor, firstSize: 12, secondSize: 12)
        //print("atttext",text)
        availabel_Lbl.attributedText = text
        distanceAway_Lbl.text = "\(detailModel.availableRange ?? 0) Meters Away"
        estimatedFuel_Lbl.text = "\(detailModel.fuelLevel ?? 0)% | \(detailModel.estimatedKmPerFuel ?? 0) Km"
        manualGear_Lbl.text = detailModel.features?.gearType ?? ""
        carType_Lbl.text = detailModel.features?.CarCategory ?? ""
        seats_Lbl.text =  "\(detailModel.features?.noOfSeat ?? 0) Seats"
        infantSeat_Lbl.text = detailModel.features?.childSeat ?? ""
        suitcase_Lbl.text = "\(detailModel.features?.suitcases ?? 0) Suitcases"
        car_Name_Lbl.text = detailModel.features?.CarCategory ?? ""
        car_Number_Lbl.text = Vehicle.registrationNumber ?? ""
        perHour_Lbl.text = "\(Vehicle.perHourCost ?? 0.0) Nok/Hour"
        gasoline_Lbl.text = detailModel.features?.fuelType ?? ""
        let winterTyre = detailModel.features?.winterTyre
        if winterTyre == true {
             winter_tyre_Lbl.text = "Winter Tyre"
        } else {
            winter_tyre_Lbl.text = ""
        }
       
        let url = AppConstants.imageURL
//        for i in 0..<detailModel.previousDamageReports!.first!.images!.count {
//            if i == 0 {
//                let imageUrl = url + (detailModel.previousDamageReports?.first?.images?[i].path)!
//                let image = URL(string :imageUrl)
//                rentalReport_FirstImage.setImageForURL(url: image, placeHolder: UIImage(named : "mind blowing car image"))
//            } else if i == 1 {
//                let image = URL(string :url + (detailModel.previousDamageReports?.first?.images?[i].path)!)
//                rentalReport_SecondImage.setImageForURL(url: image, placeHolder: UIImage(named : "mind blowing car image"))
//            } else if i == 2 {
//                let image = URL(string :url + (detailModel.previousDamageReports?.first?.images?[i].path)!)
//                rentalReport_ThirdImage.setImageForURL(url: image, placeHolder: UIImage(named : "mind blowing car image"))
//            } else {
//                let image = URL(string :url + (detailModel.previousDamageReports?.first?.images?[i].path)!)
//                rentalReport_FourthImage.setImageForURL(url: image, placeHolder: UIImage(named : "mind blowing car image"))
//            }
//        }
//        if let url = URL(string : AppConstants.imageURL + (detailModel)){
//                   imgVehicle.setImageForURL(url: url, placeHolder: UIImage(named : "carPlaceholder"))
//               }else{
//                   imgVehicle.image = UIImage(named : "carPlaceholder")
//               }
        //        let brandName = SyncManager.sharedInstance.fetchNameFromBrand(detailModel.vehicle?.brand ?? "")
        //
        //         if brandName != nil{
        //             lblVehicleName.text = "\(brandName ?? "") - \(detailModel.vehicle?.name ?? "")"
        //         }else{
//        lblVehicleName.text = detailModel.vehicle?.name
//        //         }
//
//        lblCarModel.text = detailModel.vehicle?.model
//
//        if let url = URL(string : AppConstants.imageURL + (detailModel.vehicle?.image ?? "")){
//            imgVehicle.setImageForURL(url: url, placeHolder: UIImage(named : "carPlaceholder"))
//        }else{
//            imgVehicle.image = UIImage(named : "carPlaceholder")
//        }
//        lblFuelPercentage.text = "\(detailModel.vehicle?.fuelLevel ?? 0.0) % \nFUEl"
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//            self.circularProgressBar.setProgress(to: (self.detailModel.vehicle?.fuelLevel ?? 0.0) / 100, withAnimation: true)
//        }
//
//        if detailModel.packages.count > 0{
//            let filter = detailModel.packages.filter { (model) -> Bool in
//                if model.hours == AppConstants.GetPricePackageConfigHour{
//                    return true
//                }
//
//                return false
//            }
//
//            if filter.count > 0 {
//                lblPricePerDay.text = Utilities.convertToCurrency(number: filter.first!.rate, currencyCode: filter.first!.currency)
//                labelPerDay.text =  "/ \(filter.first!.getFormattedUnit )"
//            }else{
//
//                lblPricePerDay.text = Utilities.convertToCurrency(number: detailModel.packages.first!.rate, currencyCode: detailModel.packages.first!.currency)
//                labelPerDay.text = "/ \(detailModel.packages.first!.getFormattedUnit )"
//
//            }
//        }else{
//            lblPricePerDay.isHidden = true
//            labelPerDay.isHidden = true
//        }
    }
    
    //MARK: set circular Progress
    func setCircularProgress(){
        circularProgressBar.lineWidth = 1.0
        circularProgressBar.safePercent = 100
        circularProgressBar.labelSize = 0
    }
    
    //MARK:- IBActions
    @IBAction func btnCheckMoreInfo_Click(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            more_Info_DropImge.image = UIImage(named: "back_down")
            isselected = true
        } else {
            sender.tag = 0
            more_Info_DropImge.image = UIImage(named: "back_right")
            isselected = false
        }
        tableView.reloadData()
    }
    
    @IBAction func previousCar_Btn(_ sender: UIButton) {
        let collectionBounds = self.cars_CollectionView.bounds
        let contentOffset = CGFloat(floor(self.cars_CollectionView.contentOffset.x - collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    
    @IBAction func nextCar_Btn(_ sender: UIButton) {
        let collectionBounds = self.cars_CollectionView.bounds
        let contentOffset = CGFloat(floor(self.cars_CollectionView.contentOffset.x + collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    
    func moveCollectionToFrame(contentOffset : CGFloat) {
        
        let frame: CGRect = CGRect(x : contentOffset ,y : self.cars_CollectionView.contentOffset.y ,width : self.cars_CollectionView.frame.width,height : self.cars_CollectionView.frame.height)
        self.cars_CollectionView.scrollRectToVisible(frame, animated: true)
    }
    
    @IBAction func supportBtn_Click(_ sender: UIButton) {
        support_View.isHidden = false
    }
    
    @IBAction func supportView_Chat_Btn(_ sender: UIButton) {
        support_View.isHidden = true
        let vc = Chat_ViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func supportView_Support_Btn(_ sender: UIButton) {
        support_View.isHidden = true
        let vc = SupportVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func supportView_Damage_Btn(_ sender: UIButton) {
        support_View.isHidden = true
        let vc = ReportDamageVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnBack_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func previous_Rental_Report_Btn(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            previous_Rental_DropImage.image = UIImage(named: "back_down")
            previousRentalReport_View.isHidden = false
             let footer = ViewFooter!
             footer.frame = CGRect(x: 0, y: 0, width: 0, height: 640)
             tableView.tableFooterView = footer
            divider_Line.isHidden = true
        } else {
            sender.tag = 0
            previous_Rental_DropImage.image = UIImage(named: "back_right")
             previousRentalReport_View.isHidden = true
            divider_Line.isHidden = false
             let footer = ViewFooter!
             footer.frame = CGRect(x: 0, y: 0, width: 0, height: 200)
             tableView.tableFooterView = footer
        }
        tableView.reloadData()
    }
    
    @IBAction func book_Car_Btn(_ sender: ButtonDesign) {
        let vc = Book_Now_ViewController()
        vc.strVehicleId = detailModel.vehicle?.id ?? ""
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func openMoreInformation() {
        
        let vc = CheckMoreInformationVC()
        let nav = UINavigationController(rootViewController: vc)
        vc.rideType = self.rideType
        vc.vehicleDetail = self.detailModel
        vc.startDate = self.startDate
        vc.endDate = self.endDate
        self.present(nav, animated: true, completion: nil)
    }
    
    private func viewConfigrations() {
        
        rental_Report_CollectionView.register(UINib(nibName: "Previous_Rental_Reports_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Previous_Rental_Reports_CollectionViewCell")
        rental_Report_CollectionView.contentInset = UIEdgeInsets.init(top: 0, left: 40, bottom: 0, right: 40)
        rental_Report_CollectionView.decelerationRate = UIScrollView.DecelerationRate.fast
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == cars_CollectionView {
            return detailModel.car_images?.count ?? 0
        } else {
            return 2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if collectionView == cars_CollectionView {
            let images = detailModel.car_images?[indexPath.row]
            let cell = cars_CollectionView.dequeueReusableCell(withReuseIdentifier: "CarFeatureCollectionCell", for: indexPath) as! CarFeatureCollectionCell

            let url = URL(string: "\(AppConstants.imageURL)\(images ?? "")")
            cell.car_imgView.setImageForURL(url: url, placeHolder: UIImage(named: "carPlaceholder"))
            return cell
        } else {
            let cell = rental_Report_CollectionView.dequeueReusableCell(withReuseIdentifier: "Previous_Rental_Reports_CollectionViewCell", for: indexPath) as! Previous_Rental_Reports_CollectionViewCell
            return cell
        }
        //let model = arrCollData[indexPath.row]
        
        //        cell.setData(model, rideType: rideType, startDate: reservationStartDate, endDate: reservationEndDate)
        //        cell.btnReserve.click = {
        //            self.openCarDetail(indexPath.row)
        //        }
        
        //        cell.btnPreviousRentalInfo.tag = indexPath.row
        //        cell.btnPreviousRentalInfo.addTarget(self, action: #selector(btnPreviousRental_Click(_:)), for: .touchUpInside)
        //        cell.btnClose.addTarget(self, action: #selector(btnClose_Click(_:)), for: .touchUpInside)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == cars_CollectionView {
            let layout = cars_CollectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 0
            layout.invalidateLayout()
            
            return CGSize(width: self.view.frame.width, height:(collectionView.bounds.height))

        } else {
            var cellSize: CGSize = collectionView.bounds.size
            cellSize.width -= collectionView.contentInset.left
            cellSize.width -= collectionView.contentInset.right
            cellSize.height = 150//cellSize.width
            
            return cellSize
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
//
//        if collectionView == cars_CollectionView {
//            let layout = cars_CollectionView.collectionViewLayout as! UICollectionViewFlowLayout
//            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//            layout.minimumInteritemSpacing = 0
//            layout.minimumLineSpacing = 0
//            layout.invalidateLayout()
//            return CGSize(width: self.view.frame.width, height:(collectionView.bounds.height))
//
//        } else {
//            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
//            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//            layout.minimumInteritemSpacing = 10
//            layout.minimumLineSpacing = 0
//            layout.invalidateLayout()
//            return CGSize(width: self.rental_Report_CollectionView.frame.width, height: 150)
//        }
//    }
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title)")
    }
}

//MARK: Tableview methods
extension CarBasicDetailVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isselected == false {
            return 0
        } else {
            return detailModel.more_info?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarFeatureTableCell", for: indexPath) as! CarFeatureTableCell
        let descriptionData = detailModel.more_info?[indexPath.row]
        cell.title_Lbl.text = descriptionData?.name ?? ""
        cell.description_Lbl.text = descriptionData?.text?.htmlToString
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if isselected == false {
            return 0
        } else {
            return UITableView.automaticDimension
        }
    }
}

//MARK: API Calls
extension CarBasicDetailVC {
    
    func callApiForCarDetail(){
        
        let param : [String:Any] = ["vehicleId"     : Vehicle.vehicleid ?? 0,
                                    "state"         : "Uttar Pradesh",
                                    "city"          : "Badvel",
                                    "country"       : "india"]
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.GetVehicleDetail, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
//            print(response)
            if let dictResponse = response as? [String:Any] {
                self.detailModel = Mapper<VehicleDetailModel>().map(JSON: dictResponse)!
        }
            self.setData()
            self.cars_CollectionView.reloadData()
            self.tableView.reloadData()
            
        }) { (failureMessage, failureCode) in
            
        }
    }
}
