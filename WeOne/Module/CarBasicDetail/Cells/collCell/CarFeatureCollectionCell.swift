//
//  CarFeatureCollectionCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 27/11/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class CarFeatureCollectionCell: UICollectionViewCell {
    
    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var car_imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var km_Lbl: UILabel!
    @IBOutlet weak var car_Name_Lbl: UILabel!
    @IBOutlet weak var car_Number_Lbl: UILabel!
    @IBOutlet weak var perHour_Lbl: UILabel!
    
    @IBOutlet weak var availabel_Lbl: UILabel!
    @IBOutlet weak var distanceAway_Lbl: UILabel!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK: Set Data
    func setData(model:CellModel) {
//        imgView.image = UIImage(named: model.imageName ?? "")
        lblName.text = model.placeholder
    }
}
