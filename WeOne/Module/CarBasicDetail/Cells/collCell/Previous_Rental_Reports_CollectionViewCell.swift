//
//  Previous_Rental_Reports_CollectionViewCell.swift
//  WeOne
//
//  Created by Dev's Mac on 18/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Previous_Rental_Reports_CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var useserImage: ImageViewDesign!
    @IBOutlet weak var name_Lbl: UILabel!
    @IBOutlet weak var time_Lbl: UILabel!
    @IBOutlet weak var distance_Lbl: UILabel!
    @IBOutlet weak var description_Lbl: UILabel!
    @IBOutlet weak var speedodometerImage: UIImageView!
    @IBOutlet weak var fuelImage: UIImageView!
    @IBOutlet weak var fuel_Lbl: UILabel!    
    @IBOutlet weak var speed_Lbl: UILabel!
    @IBOutlet weak var firstImage: UIImageView!
    @IBOutlet weak var secondImage: UIImageView!
    @IBOutlet weak var fourthIMage: UIImageView!
    @IBOutlet weak var thirdImage: UIImageView!
    @IBOutlet weak var showCar_Btn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
