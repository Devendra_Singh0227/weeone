//
//  ExtraCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 09/01/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class ExtraCell: UITableViewCell {

    @IBOutlet weak var lblExtra: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
