//
//  CarFeatureTableCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 29/11/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class CarFeatureTableCell: UITableViewCell {

    var arrCarFeatures : [CellModel] = [CellModel]()
    
    @IBOutlet weak var title_Lbl: UILabel!
    @IBOutlet weak var titleImage: UIImageView!
    @IBOutlet weak var description_Lbl: UILabel!
    
    
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var constraintHeightCollView: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
//        collView.delegate = self
//        collView.dataSource = self
//        collView.register(UINib(nibName: "CarFeatureCollectionCell", bundle: nil), forCellWithReuseIdentifier: "CarFeatureCollectionCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(_ model : [CellModel]){
        arrCarFeatures = model
        
        if arrCarFeatures.count > 0{
            if arrCarFeatures.count % 2 == 0{
                constraintHeightCollView.constant = CGFloat((arrCarFeatures.count / 3) * 106)
            }else{
                constraintHeightCollView.constant = CGFloat(((arrCarFeatures.count / 3) + 1) * 106)
            }
        }
        collView.reloadData()
    }
}

//MARK: - UICollectionView's DataSource & Delegate Methods
extension CarFeatureTableCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrCarFeatures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let model = arrCarFeatures[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarFeatureCollectionCell", for: indexPath) as! CarFeatureCollectionCell
        cell.setData(model: model)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let width = collectionView.bounds.width / 3
        
        return CGSize(width: width, height: 106)
    }
}
