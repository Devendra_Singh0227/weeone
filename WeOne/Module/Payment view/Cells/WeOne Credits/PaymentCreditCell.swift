//
//  PaymentCreditCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 11/06/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class PaymentCreditCell: UITableViewCell {

    //MARK: - Variables
    var switchChanged : ((_ switch : Bool) -> ())?

    //MARK: - Outlets
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnSwitch: UIButton!
    @IBOutlet weak var btnInfo: UIButton!
    @IBOutlet weak var imgDollor: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        imgDollor.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Dollor"))
        btnInfo.setImage(UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Information")), for: .normal)

    }

    func setData(_ model : CellModel){
        btnSwitch.isSelected = model.isSelected
        lblPrice.text =  String(format : "\(Utilities.convertToCurrency(number: Double(model.userText ?? "")!, currencyCode: AppConstants.Currency.NOK))")
    }

    @IBAction func btnInfo_Click(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        Utilities.dismissToolTip(views: appDelegate.window!.subviews)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            Utilities.showPopOverView(str: "KCreditInfoDesc".localized, arrowSize: CGSize(width: 9, height: 9), maxWidth: 150, x: 27, sender: sender)
        })

    }
    
    @IBAction func btnSwitch(_ sender: UIButton) {
        btnSwitch.isSelected = !btnSwitch.isSelected
        self.switchChanged?(btnSwitch.isSelected)
    }
}
