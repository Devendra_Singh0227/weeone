//
//  PaymentProfileCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 11/06/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class PaymentProfileCell: UITableViewCell {

    @IBOutlet weak var imgProfile : UIImageView!
    @IBOutlet weak var lblTitle : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ model : CellModel){
        imgProfile.image = UIImage(named : model.imageName ?? "")
        lblTitle.text = model.placeholder
    }
}
