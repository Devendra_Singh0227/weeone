//
//  PaymentSectionCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 02/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class PaymentSectionCell: UITableViewCell {

    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var constraintLeadingLblTitle: NSLayoutConstraint!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel, isLongPress : Bool = false) {
        lblTitle.text = model.placeholder
        
        if isLongPress{
                   constraintLeadingLblTitle.constant = 60
               }else{
                   constraintLeadingLblTitle.constant = 20
               }
    }

}
