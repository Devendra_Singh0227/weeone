//
//  PaymentCardCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 11/06/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class PaymentCardCell: UITableViewCell {
    
    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var lblCard: UILabel!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var check_Img: UIImageView!
    @IBOutlet weak var remove_Card_Img: UIImageView!
    @IBOutlet weak var check_Image_TrailingConstrant: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: Set Data
//    func setData(model:CellModel) {
//
//        imgCard.image = UIImage(named: model.imageName ?? "")
//        lblCard.text = model.placeholder
//
//        if let obj = model.cellObj as? CardModel {
//
//            if obj.cardType == AppConstants.CardTypes.JobCard && obj.isPrimary{
//                btnCheck.isHidden = false
//                btnCheck.setImage(UIImage(named : "checkGreen"), for: .normal)
//            }else
//
//            if obj.cardType == AppConstants.CardTypes.PersonalCard && obj.isPrimary{
//                btnCheck.isHidden = false
//                btnCheck.setImage(UIImage(named : ThemeManager.sharedInstance.getImage(string: "CircleChecked")), for: .normal)
//            }else{
//                btnCheck.isHidden = true
//            }
//        }else{
//            btnCheck.isHidden = true
//        }
//    }
    
    //Default payment screen set
//    func setDataForDefaultPayment(_ model:CellModel, isLongPress : Bool = false) {
//        imgCard.image = UIImage(named: model.imageName ?? "")
//        lblCard.text = model.placeholder
//        
//        if let obj = model.cellObj as? CardModel {
//            
//            if obj.cardType == AppConstants.CardTypes.JobCard && obj.isPrimary{
//                btnCheck.isHidden = false
//                btnCheck.setImage(UIImage(named : "checkGreen"), for: .normal)
//            }else
//            
//            if obj.cardType == AppConstants.CardTypes.PersonalCard && obj.isPrimary{
//                btnCheck.isHidden = false
//                btnCheck.setImage(UIImage(named : ThemeManager.sharedInstance.getImage(string: "CircleChecked")), for: .normal)
//            }else{
//                btnCheck.isHidden = true
//            }
//        }else{
//            btnCheck.isHidden = true
//        }
//        
//        if isLongPress{
//            btnCheck.isHidden = true
//            viewSelect.isHidden = false
//        }else{
//            viewSelect.isHidden = true
//        }
//        
//       // btnSelect.isSelected = model.isSelected
//    }
}
