//
//  PaymentAddCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 11/06/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class PaymentAddCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var constraintLeadingimgAdd: NSLayoutConstraint!
    @IBOutlet weak var add_CardBtn: UIButton!
    @IBOutlet weak var personal_Profile_Btn: UIButton!
    @IBOutlet weak var business_Profile_Btn: UIButton!
    @IBOutlet weak var info_Btn: UIButton!
    @IBOutlet weak var info_View: ViewDesign!
    @IBOutlet weak var switch_Btn: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ model : CellModel, isLongPress : Bool = false){
        lblTitle.text = model.placeholder
        
        if isLongPress{
            constraintLeadingimgAdd.constant = 60
        }else{
            constraintLeadingimgAdd.constant = 20
        }
    }
}
