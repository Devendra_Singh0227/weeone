//
//  PaymentViewController.swift
//  WeOne
//
//  Created by Coruscate Mac on 11/06/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit
import PhoneNumberKit
import FormTextField

class PaymentViewController: UIViewController, UITextFieldDelegate {
  
    //MARK: Variables
    var isPresented : Bool = false
    var arrData = [CellModel]()
    
    //MARK: Outlets
    @IBOutlet weak var viewNavigation : UIViewCommon!
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var credit_View: ViewDesign!
    @IBOutlet weak var zip_View: ViewDesign!
    @IBOutlet weak var month_View: ViewDesign!
    @IBOutlet weak var Cvv_View: ViewDesign!
    @IBOutlet weak var credit_Card_TxtField: FormTextField!
   
    @IBOutlet weak var month_TxtField: FormTextField!
    @IBOutlet weak var Cvv_TxtField: FormTextField!
    @IBOutlet weak var Zip_TxtField: UITextField!
    @IBOutlet weak var header_View: UIView!
    @IBOutlet weak var PayPal_View: ViewDesign!
    @IBOutlet weak var skip_View: ViewDesign!
    var checkScreen = ""
    @IBOutlet weak var button_TopConstraint: NSLayoutConstraint!
    @IBOutlet weak var stack_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var paypal_TopConstraint: NSLayoutConstraint!
    @IBOutlet weak var weeOne_Lbl: UILabel!
    @IBOutlet weak var corporate_Lbl: UILabel!
    @IBOutlet weak var navigation_View: ViewDesign!
    @IBOutlet weak var pick_to_Pay_View: UIView!
    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var scroll_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardView: ViewDesign!
    @IBOutlet weak var personal_Lbl: UILabel!
    
    var cardType : Int = AppConstants.CardTypes.PersonalCard
    var arrCountryCodes = [String]()
    var arrCountries = [String]()
    var selectedCountry: Int? {
           didSet {
               if let index = selectedCountry {
                    Zip_TxtField.text = self.arrCountryCodes[index]

//                   txtField.partialFormatter.defaultRegion = arrCountries[index].components(separatedBy: ":").last ?? ""

                   let url = URL.init(string: "http://flagpedia.net/data/flags/normal/\(self.arrCountries[index].components(separatedBy: ":")[1].lowercased()).png")
                   imgCountry.sd_setImage(with: url, completed: { (image, error, cache, url) in
                   })
               }
           }
       }
    
    //MARK: View life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        personal_Lbl.text = "Personal Card"
        credit_Card_TxtField.inputType = .integer
        credit_Card_TxtField.formatter = CardNumberFormatter()
        
        month_TxtField.inputType = .integer
        month_TxtField.formatter = CardExpirationDateFormatter()
        
        Cvv_TxtField.inputType = .integer
        Cvv_TxtField.formatter = CardNumberFormatter()
        
        checkSreenSize()
        credit_Card_TxtField.delegate = self
        month_TxtField.delegate = self
        Cvv_TxtField.delegate = self
        Zip_TxtField.delegate = self
        if checkScreen == "leftmenu" {
            checkScreenWhenFromLeftmenu()
            headerView_HeightConstraint.constant = 50
            navigation_View.isHidden = false
            pick_to_Pay_View.isHidden = true
           
            PayPal_View.isHidden = true
            skip_View.isHidden = true
            cardView.isHidden = false
            button_TopConstraint.constant = 20
            stack_HeightConstraint.constant = 50
            paypal_TopConstraint.constant = 12
            weeOne_Lbl.isHidden = true
            corporate_Lbl.text = "Weeone may charge a small amount to confirm your card details. This is immediately refunded."
        } else {
            
        }
    }
    
    func checkSreenSize() {
        if AppConstants.DeviceType.IS_IPHONE_6 {
            scroll_HeightConstraint.constant = 640
        } else if AppConstants.DeviceType.IS_IPHONE_6P {
            scroll_HeightConstraint.constant = 710
        } else if AppConstants.DeviceType.IS_IPHONE_X { //11pro
            scroll_HeightConstraint.constant = 750
        } else if AppConstants.DeviceType.IS_IPHONE_XR {//11
            scroll_HeightConstraint.constant = 840
        } else if AppConstants.DeviceType.IS_IPHONE_XS_MAX {
            scroll_HeightConstraint.constant = 840
        }
    }
    
    func checkScreenWhenFromLeftmenu() {
        if AppConstants.DeviceType.IS_IPHONE_6 {
            scroll_HeightConstraint.constant = 580
        } else if AppConstants.DeviceType.IS_IPHONE_6P {
            scroll_HeightConstraint.constant = 636
        } else if AppConstants.DeviceType.IS_IPHONE_X { //11pro
            scroll_HeightConstraint.constant = 680
        } else if AppConstants.DeviceType.IS_IPHONE_XR {//11
            scroll_HeightConstraint.constant = 840
        } else if AppConstants.DeviceType.IS_IPHONE_XS_MAX {
            scroll_HeightConstraint.constant = 840
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == credit_Card_TxtField {
            credit_View.borderwidth = 2
            credit_View.backgroundColor = ColorConstants.commonWhite
        } else if textField == month_TxtField {
            month_View.borderwidth = 2
            month_View.backgroundColor = ColorConstants.commonWhite
        } else if textField == Cvv_TxtField {
            Cvv_View.borderwidth = 2
            Cvv_View.backgroundColor = ColorConstants.commonWhite
        } else {
            zip_View.borderwidth = 2
            zip_View.backgroundColor = ColorConstants.commonWhite
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textFieldText: NSString = (textField.text ?? "") as NSString
         let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
         if textField == credit_Card_TxtField {
             if txtAfterUpdate.count < 20 {
                 return true
             } else {
                 return false
             }
         } else if textField == month_TxtField {
             if txtAfterUpdate.count < 6 {
                 return true
             } else {
                 return false
             }
         } else {
            if txtAfterUpdate.count < 4 {
                return true
            } else {
                return false
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == credit_Card_TxtField {
            credit_View.borderwidth = 0
            credit_View.backgroundColor = ColorConstants.textbackgroundcolor
        } else if textField == month_TxtField {
            month_View.borderwidth = 0
            month_View.backgroundColor = ColorConstants.textbackgroundcolor
        } else if textField == Cvv_TxtField {
            Cvv_View.borderwidth = 0
            Cvv_View.backgroundColor = ColorConstants.textbackgroundcolor
        } else {
            zip_View.borderwidth = 0
            zip_View.backgroundColor = ColorConstants.textbackgroundcolor
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        //        SyncManager.sharedInstance.syncMaster ({
        //            self.prepareDataSource()
        //        })
    }
    
    func validateCard() {
        
        let param = STPCardParams()
        param.cvc = Cvv_TxtField.text
        param.expMonth = UInt((month_TxtField.text ?? "" ).components(separatedBy: "/").first!) ?? 0
        param.expYear = UInt((month_TxtField.text ?? "" ).components(separatedBy: "/").last!) ?? 0
        param.number = credit_Card_TxtField.text
        param.name = credit_Card_TxtField.text
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        STPAPIClient.shared().createToken(withCard: param) { (token, error) in
            NetworkClient.sharedInstance.stopIndicator()
            
            if error != nil{
                
                self.view.showConfirmationPopupWithSingleButton(title: "", message: error?.localizedDescription ?? "", confirmButtonTitle: StringConstants.ButtonTitles.KOk, onConfirmClick: {
                })
                
                return
            }
            //self.callApiToAddCard(token: token!.tokenId)
        }
    }
    
    func callApiToAddCard() {
        
        let param : [String:Any] = ["cardNo" : credit_Card_TxtField.text?.digits ?? "",
                                    "nameonCard" : "\(ApplicationData.user?.firstName ?? "")\(ApplicationData.user?.lastName ?? "")",
                                    "exp": month_TxtField.text!,
                                    "cvv" : Cvv_TxtField.text!,
                                    "isDefault" : 0,
                                    "isPersonal": cardType]
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.AddCard, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message ) in
            
            if let dictResponse = response as? [String:Any] {
                 self.navigationController?.popViewController(animated: true)
            }
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    //MARK: Initial config
    func initialConfig(){
        
        viewNavigation.click = {
            self.btnClose_Click()
        }
        
        tableView.register(UINib(nibName: "PaymentSectionCell", bundle: nil), forCellReuseIdentifier: "PaymentSectionCell")
        tableView.register(UINib(nibName: "PaymentCardCell", bundle: nil), forCellReuseIdentifier: "PaymentCardCell")
        tableView.register(UINib(nibName: "PaymentAddCell", bundle: nil), forCellReuseIdentifier: "PaymentAddCell")
        tableView.register(UINib(nibName: "PaymentProfileCell", bundle: nil), forCellReuseIdentifier: "PaymentProfileCell")
        tableView.register(UINib(nibName: "PaymentCreditCell", bundle: nil), forCellReuseIdentifier: "PaymentCreditCell")
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.cancelsTouchesInView = false
        
        self.view.addGestureRecognizer(tap)
        
    }
    
    func getCountryData(){
        arrCountries.removeAll()
        arrCountryCodes.removeAll()
        let arrCountry = Utilities.readJsonFile(fileName: "countryCodes")
        
        for i in 0..<arrCountry.count{
            let model = arrCountry[i]
            arrCountries.append("\(model["name"] as? String ?? ""):\(model["code"] as? String ?? "")")
            arrCountryCodes.append(model["dial_code"] as? String ?? "")
        }
    }
    
    func show_SelectionPopup(topHeading: String?,defaultSelectedIndex:Int?, isFromCity : Bool = false, isFromSignup : Bool = false, isShowSearchBar:Bool,isShowSelectedImage:Bool, arrData: [String], onSubmitClick: @escaping (Int) -> ()) {
        let popup = CountryListPopup.init(nibName: "CountryListPopup", bundle: nil)
        popup.arr_tblData = arrData
        popup.isFromCity = isFromCity
        popup.isShowSearchBar = isShowSearchBar
        popup.selectedIndex = defaultSelectedIndex
        popup.isShowSelectedImage = isShowSelectedImage
        popup.isFromSignup = isFromSignup
        popup.selectedCountry = { countryIndex in
            onSubmitClick(countryIndex)
        }
        popup.modalPresentationStyle = .fullScreen
        UIViewController.current()?.present(popup, animated: true, completion: {
        })
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func country_Drop_Btn(_ sender: UIButton) {
        getCountryData()
         show_SelectionPopup(topHeading: "KLblSelectCountry".localized, defaultSelectedIndex: self.selectedCountry, isFromCity: false, isFromSignup: true, isShowSearchBar: true, isShowSelectedImage: true, arrData: arrCountries, onSubmitClick: { (index) in
                self.selectedCountry = index
                self.zip_View.borderwidth = 2
         })
    }
    
    @IBAction func personal_BusinessBtn(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            personal_Lbl.text = "Business Card"
            sender.setImage(UIImage(named: "switchBuss"), for: .normal)
            cardType = 0
        } else {
             sender.tag = 0
            personal_Lbl.text = "Personal Card"
            sender.setImage(UIImage(named: "switchcard"), for: .normal)
            cardType = 1
        }
    }
    
    @IBAction func card_Scanner_Btn(_ sender: UIButton) {
        let VC = OnfidoVC()
        VC.checktype = "Visa"
        navigationController?.pushViewController(VC, animated: true)
    }
    
    func MoveToHomeVC() {
        let VC = HomeVC()
        navigationController?.pushViewController(VC, animated: true)
    }
    
    //MARK: Prepare datasource
    func prepareDataSource(){
        
        let user = ApplicationData.user
        
        arrData.removeAll()
        
        //cards
        arrData.append(CellModel.getModel(placeholder: "KLblCards".localized, text: "", type: .PaymentSection))
        
        for model in SyncManager.sharedInstance.fetchAllCards(){
            arrData.append(CellModel.getModel(placeholder: model.getFormattedCardNumber(), text: "", type: .PaymentCard, imageName: Utilities.getCreditCardImage(cardType: model.brand ?? ""), cellObj: model, isSelected: model.isPrimary))
        }
        
        arrData.append(CellModel.getModel(placeholder: "KLblAddNewCard".localized, type: .PaymentAddNewCard))
        
        //Profile
        arrData.append(CellModel.getModel(placeholder: "KRideProfile".localized, text: "", type: .PaymentSection))
        
        arrData.append(CellModel.getModel(placeholder: "KPersonalProfile".localized, text: "", type: .PaymentPersonalProfile, imageName: "userPurple" ))
        
        arrData.append(CellModel.getModel(placeholder: "KBusinessProfile".localized, text: "", type: .PaymentBusinessProfile, imageName: "brifcaseGreen" ))
        
        //        arrData.append(CellModel.getModel(placeholder: "KAddProfile".localized, type: .PaymentAddNewProfile))
        
        arrData.append(CellModel.getModel(text: "\(user?.walletAmount ?? 0)", type: .PaymentCreditCell, isSelected: user?.isWalletPaymentEnable ?? false))
        
        
        tableView.reloadData()
    }
    
    //MARK: Button action methods
    func btnClose_Click() {
        if isPresented{
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func adCard_Btn(_ sender: UIButton) {
        callApiToAddCard()
//         validateCard()
//        let VC = OnfidoVC()
//        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func skip_now_Btn(_ sender: UIButton) {
        let vc = Loading_ViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        Utilities.dismissToolTip(views: appDelegate.window!.subviews)
        
    }
    
}

//MARK: Tableview methods
extension PaymentViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrData[indexPath.row]
        
        switch model.cellType! {
        case .PaymentSection:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentSectionCell") as? PaymentSectionCell
            cell?.setData(model: model)
            return cell!
            
        case .PaymentCard:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCardCell") as? PaymentCardCell
//            cell?.setData(model: model)
            return cell!
            
        case .PaymentAddNewProfile, .PaymentAddNewCard:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentAddCell") as? PaymentAddCell
            cell?.setData(model)
            return cell!
            
        case .PaymentPersonalProfile, .PaymentBusinessProfile:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentProfileCell") as? PaymentProfileCell
            cell?.setData(model)
            return cell!
            
        case .PaymentCreditCell:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCreditCell") as? PaymentCreditCell
            cell?.setData(model)
            cell?.switchChanged = { value in
                self.callAPIForUpdateProfile(value)
            }
            return cell!
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let model = arrData[indexPath.row]
        
        switch model.cellType! {
        case .PaymentPersonalProfile:
            let vc = CompanyProfileVC()
            vc.addClick = {
                self.prepareDataSource()
            }
            vc.isFromPersonal = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        case .PaymentBusinessProfile:
            let vc = CompanyProfileVC()
            vc.addClick = {
                self.prepareDataSource()
            }
            vc.isFromPersonal = false
            self.navigationController?.pushViewController(vc, animated: true)
            
        case .PaymentAddNewCard:
            let vc = AddCardViewController()
            AppNavigation.shared.currentStateList = AppNavigation.shared.config.accessConfig.requiredToUseApp
            vc.addClick = {
                self.prepareDataSource()
            }
            
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
            break
            
        default: break
        }
    }
}
//MARK:- API Calling
extension PaymentViewController {
    
    func callAPIForUpdateProfile(_ isWalletPaymentEnable: Bool) {
        
        var param = [String:Any]()
        param["isWalletPaymentEnable"] = isWalletPaymentEnable
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.UpdateProfile, method: .put, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                
                if let dictResponse = response as? [String:Any] {
                    
                    ApplicationData.sharedInstance.saveUserData(dictResponse)
                }
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
