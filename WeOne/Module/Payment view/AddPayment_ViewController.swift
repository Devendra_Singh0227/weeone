//
//  AddPayment_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 19/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class AddPayment_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var table_View: UITableView!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet var footer_View: UIView!
    @IBOutlet weak var info_View: ViewDesign!
    
    var arrCard = [CardModel]()
    var cardArray = [CardsListModel]()
    
    var showInfo = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initial_Config()
        table_View.register(UINib(nibName: "PaymentCardCell", bundle: nil), forCellReuseIdentifier: "PaymentCardCell")
    }
    
    func initial_Config() {

        //arrCard = SyncManager.sharedInstance.fetchAllCards()
        callgetCardApi()
        let footer = footer_View!
        footer.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 450)
        table_View.tableFooterView = footer
        table_View.reloadData()
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func remove_Card(sender: UIButton) {
        let vc = ConfirmPopupVC()
        let id = cardArray[sender.tag].id ?? -1
        vc.confirmClicked = {
            self.callApiForDeleteCard(card_Id: id)
        }
        vc.headerTitle = "Remove Card"
        vc.message = "Are you sure you want to remove card?"
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        
        if UIViewController.current()?.presentedViewController != nil {
            UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
            })
        }
        else{
            UIViewController.current()?.present(vc, animated: true, completion: {
            })
        }
    }
    
    @IBAction func add_NewCard_Btn(_ sender: UIButton) {
        let VC = PaymentViewController()
        VC.checkScreen = "leftmenu"
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func promoCode_Btn(_ sender: UIButton) {
        let vc = PromoCodeVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func instantSetting_Btn(_ sender: UIButton) {
        let VC = Instant_ViewController()
        VC.checkScreen = "payment"
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func personal_Profile(_ sender: UIButton) {
        let VC = Personal_Profile_ViewController()
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func business_Profile_Btn(_ sender: UIButton) {
        let VC = Business_Profile_ViewController()
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func info_Btn(_ sender: UIButton) {
        
//        let str = Utilities.attributedString(firstString: "Weeone", secondString: "Use Weone credits for your next booking", firstFont: FontScheme.FontConstant.kSemiBoldFont, secondFont: FontScheme.FontConstant.kRegularFont, firstColor: ColorConstants.secondTextColor, secondColor: ColorConstants.secondTextColor, firstSize: 14, secondSize: 14)
//        let stri = str.string
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
//            Utilities.showPopOverView(str: stri, arrowSize: CGSize(width: 9, height: 9), maxWidth: 250, x: 27, sender: sender)
//        })
        if sender.tag == 0 {
            sender.tag = 1
            info_View.isHidden = false
        } else {
            sender.tag = 0
            info_View.isHidden = true
        }
    }
    
    func callgetCardApi() {
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.getCards, method: .get, parameters: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [[String:Any]] {
                    self.cardArray = Mapper<CardsListModel>().mapArray(JSONArray: dictResponse)
                    self.table_View.reloadData()
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func callApiForDeleteCard(card_Id: Int) {
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: "\(AppConstants.URL.DeleteCard)\(card_Id)", method: .delete, parameters: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
                self.callgetCardApi()
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    @IBAction func weeOneCredit_Btn(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            sender.setImage(UIImage(named: "switch"), for: .normal)
        } else {
            sender.tag = 0
            sender.setImage(UIImage(named: "switchActive"), for: .normal)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cardArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = cardArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCardCell") as? PaymentCardCell
        cell?.lblCard.text = model.cardNo ?? ""
        let type = model.cardType ?? ""
        if type == "Visa" {
            cell?.imgCard.image = UIImage(named:"visa1")
        } else {
            cell?.imgCard.image = UIImage(named:"bg_card_back")
        }
        
        cell?.btnRemove.tag = indexPath.row
        cell?.btnRemove.addTarget(self, action: #selector(remove_Card(sender:)), for: .touchUpInside)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
