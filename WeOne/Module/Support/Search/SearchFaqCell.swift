//
//  SearchFaqCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 29/01/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class SearchFaqCell: UITableViewCell {
        
    var cellModel = CellModel()
    var textChanged : ((_ text : String) -> ())?
    var timer: Timer = Timer()
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        txtSearch.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(_ model : CellModel){
        cellModel = model
        txtSearch.becomeFirstResponder()
    }
    
    //MARK: button click methods
    @IBAction func btnSearch_Click(_ sender: UIButton) {
        UIViewController.current().view.endEditing(true)
        textChanged?(txtSearch.text ?? "")
    }
    
    @IBAction func txtValueChanged(_ sender: UITextField) {
//        if sender.text?.count == 0{
//            timer.invalidate()
//            textChanged?("")
//        }else{
//            timer.invalidate()
//            timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(timerEnded), userInfo: nil, repeats: false)
//        }
    }
    
    @objc func timerEnded(_ timer : Timer){
        textChanged?(txtSearch.text ?? "")
    }
}

//MARK: TextField delegate methods
extension SearchFaqCell : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       // textChanged?(txtSearch.text ?? "")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       // textChanged?(txtSearch.text ?? "")
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        
        textChanged?(txtSearch.text ?? "")
        
        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        
        if range.location == 0{
            
            if string == " " {
                return false
            }
        }
        
        return true
    }
}



