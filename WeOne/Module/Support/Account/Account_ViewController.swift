//
//  Account_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 04/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//


import UIKit

class Account_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var header_Label: UILabel!
    
    var checkScreen = ""
    var account_array = ["Unblocking Your Account".localized, "Changing Your Email Address".localized, "Changing Your Phone Number".localized, "Choosing A Language".localized, "Updating Or Saving An Address".localized, "Getting Weone Newsletter".localized, "Requesting Your Data".localized, "Collecting And Processing Personal Data".localized, "Deleting Your Account".localized ]
    
    var table_array = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialConfig()
        tableView.register(UINib(nibName: "Account_TableViewCell", bundle: nil), forCellReuseIdentifier: "Account_TableViewCell")
//        if checkScreen == "Account" {
//             info_view.isHidden = true
//             table_array =  account_array
//             header_Label.text = "Account"
//        } else if checkScreen == "Payment" {
//             info_view.isHidden = false
//             table_array = payment_array
//             header_Label.text = "Payment and Pricing"
//        } else if checkScreen == "UsingWeone" {
//             info_view.isHidden = true
//             header_Label.text = "Using Weone"
//             table_array = usingWeone_array
//        } else {
//            info_view.isHidden = true
//            header_Label.text = "About Weone"
//            table_array = aboutWeone_array
//        }
    }

    func initialConfig() {
        
//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return account_array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Account_TableViewCell", for: indexPath) as? Account_TableViewCell
        cell?.item_Label.text = account_array[indexPath.row]
    
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
