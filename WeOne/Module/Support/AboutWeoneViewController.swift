//
//  AboutWeoneViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 25/01/21.
//  Copyright © 2021 Coruscate Mac. All rights reserved.
//

import UIKit

class AboutWeoneViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var table_View: UITableView!
    var aboutWeone_array = ["Rate App".localized, "Weone for Business".localized, "Weone for Car Owners".localized ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

         table_View.register(UINib(nibName: "Account_TableViewCell", bundle: nil), forCellReuseIdentifier: "Account_TableViewCell")
        // Do any additional setup after loading the view.
    }


    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aboutWeone_array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table_View.dequeueReusableCell(withIdentifier: "Account_TableViewCell", for: indexPath) as? Account_TableViewCell
        cell?.item_Label.text = aboutWeone_array[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    @IBAction func terms_Condition_Btn(_ sender: UIButton) {
        let vc = Terms_ConditionViewController()
        vc.checkScreen = "terms"
        navigationController?.pushViewController(vc, animated: true)
    }
}
