//
//  Your_Conversation_VC.swift
//  WeOne
//
//  Created by Dev's Mac on 04/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Your_Conversation_VC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var table_View: UITableView!
    
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialConfig()
         table_View.register(UINib(nibName: "Your_ConversationTableViewCell", bundle: nil), forCellReuseIdentifier: "Your_ConversationTableViewCell")
        // Do any additional setup after loading the view.
    }
    
    func initialConfig() {
           
//           if AppConstants.hasSafeArea {
//               headerView_HeightConstraint.constant = 90
//           }
//           else {
//               headerView_HeightConstraint.constant = 74
//           }
       }
    
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table_View.dequeueReusableCell(withIdentifier: "Your_ConversationTableViewCell", for: indexPath) as? Your_ConversationTableViewCell
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension// 120
    }
}
