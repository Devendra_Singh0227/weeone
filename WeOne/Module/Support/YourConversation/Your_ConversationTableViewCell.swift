//
//  Your_ConversationTableViewCell.swift
//  WeOne
//
//  Created by Dev's Mac on 04/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Your_ConversationTableViewCell: UITableViewCell {

    @IBOutlet weak var first_msg_Label: UILabel!    
    @IBOutlet weak var description_Label: UILabel!
    @IBOutlet weak var time_Label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
