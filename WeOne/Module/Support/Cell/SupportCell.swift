//
//  SupportCell.swift
//  WeOne
//
//  Created by Mayur on 25/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class SupportCell: UITableViewCell {
    
    @IBOutlet weak var lblCall: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
//        setClickableLink()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    func setClickableLink() {
//
//        let customType = ActiveType.custom(pattern: "\\\(AppConstants.ContactNo)\\b")
//        let customType2 = ActiveType.custom(pattern: "\\\(AppConstants.ContactEmail)\\b")
//
//        lblCall.enabledTypes.append(customType)
//        lblCall.enabledTypes.append(customType2)
//
//        lblCall.customize { label in
//
//            lblCall.text = String(format : "KLblSupportCall".localized,AppConstants.ContactNo, AppConstants.ContactEmail)
//
//            lblCall.numberOfLines = 0
//            lblCall.lineSpacing = 10.0
//
//            //Custom types
//
//            lblCall.customColor[customType] = ColorConstants.TextColorTheme
//            lblCall.customColor[customType2] = ColorConstants.TextColorTheme
//
//            lblCall.configureLinkAttribute = { (type, attributes, isSelected) in
//                var atts = attributes
//                switch type {
//                case customType, customType2:
//                    atts[NSAttributedString.Key.underlineStyle] = NSUnderlineStyle.single.rawValue
//                default: ()
//                atts[NSAttributedString.Key.underlineStyle] = nil
//                    break
//                }
//
//                return atts
//            }
//
//            lblCall.handleCustomTap(for: customType) {_ in
//                Utilities.call(phoneNumber: AppConstants.ContactNo)
//            }
//
//            lblCall.handleCustomTap(for: customType2) {_ in
//                Utilities.openEmail(email: AppConstants.ContactEmail)
//            }
//        }
//    }
    
}
