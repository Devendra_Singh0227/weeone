//
//  PaymentPricingViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 22/01/21.
//  Copyright © 2021 Coruscate Mac. All rights reserved.
//

import UIKit

class PaymentPricingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var table_View: UITableView!
    @IBOutlet weak var support_View: ViewDesign!
    var payment_array = ["Card Authorisation".localized, "Issues With A Promotion".localized, "Using A Promo Code".localized, "Airport, Parking And Additional Fees".localized, "Price Calculation With Promo Code".localized, "Cancellation Fees".localized, " Failed Card Payment".localized, "Unable To Add A Card".localized, "Adding Or Choosing A Payment Method".localized, "Resending Your Receipt".localized, "Removing A Card".localized ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_View.register(UINib(nibName: "Account_TableViewCell", bundle: nil), forCellReuseIdentifier: "Account_TableViewCell")
        // Do any additional setup after loading the view.
    }
    
    @IBAction func support_Btn(_ sender: UIButton) {
        support_View.isHidden = false
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return payment_array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table_View.dequeueReusableCell(withIdentifier: "Account_TableViewCell", for: indexPath) as? Account_TableViewCell
        cell?.item_Label.text = payment_array[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
