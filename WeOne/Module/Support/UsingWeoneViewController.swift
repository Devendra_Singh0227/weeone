//
//  UsingWeoneViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 25/01/21.
//  Copyright © 2021 Coruscate Mac. All rights reserved.
//

import UIKit

class UsingWeoneViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var table_View: UITableView!
    var usingWeone_array = ["Lost An Item".localized, "Having A Technical Issue".localized, "Rating The Trip".localized, "Booking Car".localized, "Reporting An Accident".localized, "Damage Report".localized, "Cancelling Booking".localized ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table_View.register(UINib(nibName: "Account_TableViewCell", bundle: nil), forCellReuseIdentifier: "Account_TableViewCell")
        // Do any additional setup after loading the view.
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usingWeone_array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table_View.dequeueReusableCell(withIdentifier: "Account_TableViewCell", for: indexPath) as? Account_TableViewCell
        cell?.item_Label.text = usingWeone_array[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1 {
            let vc = Support_Detail_ViewController()
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
