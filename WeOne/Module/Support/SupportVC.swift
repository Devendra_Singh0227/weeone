//
//  SupportVC.swift
//  WeOne
//
//  Created by Mayur on 25/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class SupportVC: UIViewController {
    
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var viewNavigation: UIViewCommon!
    @IBOutlet weak var viewGradiant: GradientView!

    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnSearch: UIButton!

    //MARK: Variables
    var arrCellData : [CellModel] = []
    var arrFilter : [CellModel] = []
    var arrFaqs : [[String:Any]] = [[String:Any]]()
    var supportArr = ["Issue With The Last Trip", "Issue With The Past Trip", "Accounts", "Payments and Pricing", "Using Weone", "Your Conversations", "Tutorial"]
    var isFilter = false
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
    }
    
    //MARK: intial config
    func initialConfig(){
        
//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
        
//        table_view.register(UINib(nibName: "Account_TableViewCell", bundle: nil), forCellReuseIdentifier: "Account_TableViewCell")
//        getFAQData()
//
//        txtSearch.tintColor = ColorConstants.ThemeColor
//        getFAQData()
//      //  prepareDataSource()
//        applyShadow()
//
//        viewNavigation.click = {
//            self.btnClose_Click()
//        }
    }
    
    private func applyShadow() {
        
        let shadowColor = #colorLiteral(red: 0.2666666667, green: 0.4470588235, blue: 0.768627451, alpha: 0.41)
        viewGradiant.layer.shadowColor = shadowColor.cgColor
        viewGradiant.layer.shadowOpacity = 1
        viewGradiant.layer.shadowOffset = CGSize.zero
        viewGradiant.layer.shadowRadius = 10.0
        viewGradiant.layer.cornerRadius = 25
    }
    
    @IBAction func issue_with_lastTrip_Btn(_ sender: UIButton) {
        let VC = IssueWithLastTripVC()
        //VC.checkScreen = "Account"
        navigationController?.pushViewController(VC, animated: true)
    }
        
    @IBAction func issue_with_pastTrip_Btn(_ sender: UIButton) {
        let VC = TripHistoryContainerVC()
        //VC.checkScreen = "Account"
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func account_Btn(_ sender: UIButton) {
        let VC = Account_ViewController()
        VC.checkScreen = "Account"
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func payment_pricing_Btn(_ sender: UIButton) {
        let VC = PaymentPricingViewController()
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func using_weOne_Btn(_ sender: UIButton) {
        let VC = UsingWeoneViewController()
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func your_conversation_Btn(_ sender: UIButton) {
        let VC = Your_Conversation_VC()
       // VC.checkScreen = "UsingWeone"
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func tutorial_Btn(_ sender: UIButton) {
        let VC = Support_Detail_ViewController()
        // VC.checkScreen = "UsingWeone"
         navigationController?.pushViewController(VC, animated: true)
    }
    //PrepareDatasource
    func prepareDataSource() {
        arrCellData.removeAll()
        //arrCellData.append(CellModel.getModel(type: .SupportSearchCell))
//        arrCellData.append(CellModel.getModel(type: .SupportCallCell))

     //   table_view.reloadData()
    }
    
    func getFAQData() {
        
        //            if ApplicationData.isUserLoggedIn {
        //    //            self.arrCellData.append(CellModel.getModel(placeholder: "Issue with the last trip", placeholder2: "", type: .SupportIssueWithLastTrip))
        //    //            self.arrCellData.append(CellModel.getModel(placeholder: "Issue with the past trips", placeholder2: "", type: .SupportIssueWithPastTrip, isLast: true))
        //            }
        //            self.arrCellData.append(CellModel.getModel(placeholder: "Account", placeholder2: "", type: .SupportAccount))
        //            self.arrCellData.append(CellModel.getModel(placeholder: "Payments and Pricing", placeholder2: "", type: .SupportPayment))
        //            self.arrCellData.append(CellModel.getModel(placeholder: "Using Weone", placeholder2: "", type: .SupportUsingWeOne, isLast: true))
        //                    if ApplicationData.isUserLoggedIn {
        //            //        self.arrCellData.append(CellModel.getModel(placeholder: "Your Conversations", placeholder2: "", type: .SupportConversation))
        //                    }
        //
        //            self.table_view.reloadData()
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.FAQ, method: .post, parameters: [:], headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            print(response)
            if let dictResponse = response as? [String:Any]{
                
                if let arr = dictResponse["list"] as? [[String:Any]]{
                    
                    self.arrFaqs = arr
                    let _ = arr.map {
                        self.arrCellData.append(CellModel.getModel(placeholder: $0["question"] as? String ?? "", placeholder2: $0["answer"] as? String ?? "", type: .SupportFAQCell))
                    }
                }
                
                self.table_view.reloadData()
            }
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
     func btnClose_Click() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCall_Click(_ sender: UIButton) {
        Utilities.call(phoneNumber: AppConstants.ContactNo)
    }
    
    @IBAction func btnEmail_Click(_ sender: UIButton) {
        Utilities.openEmail(email: AppConstants.ContactEmail)
    }
}

//MARK: Tableview methods
extension SupportVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return supportArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "Account_TableViewCell", for: indexPath) as? Account_TableViewCell
        
        cell?.item_Label.text = supportArr[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            let VC = IssueWithLastTripVC()
            navigationController?.pushViewController(VC, animated: true)
        } else if indexPath.row == 1 {
            let VC = IssueWithLastTripVC()
            navigationController?.pushViewController(VC, animated: true)
        } else if indexPath.row == 2 {
            let VC = Account_ViewController()
            VC.checkScreen = "Account"
            navigationController?.pushViewController(VC, animated: true)
        } else if indexPath.row == 3 {
            let VC = Account_ViewController()
            VC.checkScreen = "Payment"
            navigationController?.pushViewController(VC, animated: true)
        } else if indexPath.row == 4 {
            let VC = Account_ViewController()
            VC.checkScreen = "UsingWeone"
            navigationController?.pushViewController(VC, animated: true)
        } else if indexPath.row == 5  {
            let VC = Your_Conversation_VC()
            navigationController?.pushViewController(VC, animated: true)
        } else {
            let VC = Support_Detail_ViewController()
            navigationController?.pushViewController(VC, animated: true)
        }
//        var model = CellModel()
//
//        if isFilter {
//            model = arrFilter[indexPath.row]
//        } else {
//            model = arrCellData[indexPath.row]
//        }

//        switch model.cellType! {
//
//        case .SupportIssueWithLastTrip:
//
//            if let lastRideObject = SyncManager.sharedInstance.lastRideObject, lastRideObject == nil {
//
//                self.showToast(text: "KLblNoRideMsg".localized)
//
//            } else {
//                let vc = IssueWithLastTripVC()
//                vc.rideModel = SyncManager.sharedInstance.lastRideObject!
//                UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
//            }
//
//            break
//
//        case .SupportIssueWithPastTrip:
//            let vc = TripHistoryContainerVC()
//            vc.isFromSupport = true
//            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
//
//            break
//
//        case .SupportAccount:
//            let vc = SettingsVC()
//            vc.saticPageCode = AppConstants.StaticPageCode.Account
//            self.navigationController?.pushViewController(vc, animated: true)
//
//            break
//
//        case .SupportPayment:
//            let vc = SettingsVC()
//            vc.saticPageCode = AppConstants.StaticPageCode.Payment_Pricing
//            self.navigationController?.pushViewController(vc, animated: true)
//
//            break
//
//        case .SupportUsingWeOne:
//            let vc = SettingsVC()
//            vc.saticPageCode = AppConstants.StaticPageCode.Using_Weone
//            self.navigationController?.pushViewController(vc, animated: true)
//            break
//
//        case .SupportConversation:
//            break
//
//
//        default : break
//        }
    }
    
    func searchData(_ text : String){
        if text.count == 0{
            self.isFilter = false
            
        } else {
            self.isFilter = true
            arrFilter.removeAll()
            
            arrFilter = arrCellData.filter { (dict) -> Bool in
                if (dict.placeholder ?? "").lowercased().contains(text){
                    return true
                }
                return false
            }
        }
        
        table_view.reloadData()
        
    }
}

//MARK: TextField delegate methods
extension SupportVC : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchData(txtSearch.text ?? "")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       // textChanged?(txtSearch.text ?? "")
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        searchData(newString)
        
        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        
        if range.location == 0{
            
            if string == " " {
                return false
            }
        }
        
        return true
    }
}



