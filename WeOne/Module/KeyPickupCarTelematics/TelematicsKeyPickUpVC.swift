//
//  TelematicsKeyPickUpVC.swift
//  WeOne
//
//  Created by Dev's Mac on 24/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit
import UICircularProgressRing

class TelematicsKeyPickUpVC: UIViewController {

    @IBOutlet weak var cars_CollectionView: UICollectionView!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var support_View: ViewDesign!
    @IBOutlet weak var car_InfoView: UIView!
    @IBOutlet weak var detailViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var drop_Image: UIImageView!
    @IBOutlet weak var lblVehicleName: UILabel!
    @IBOutlet weak var lblVehicleNumber: UILabel!
    @IBOutlet weak var lblPricePerDay: UILabel!
    var detailModel : VehicleDetailModel = VehicleDetailModel()
    @IBOutlet weak var ring: UICircularTimerRing!
    var timer = Timer()
    var Count = 0
    var checkScreen = ""
    @IBOutlet weak var inspect_Lbl: UILabel!
    @IBOutlet weak var move_Arrow_View: UIView!
    @IBOutlet weak var profile_StackView: UIStackView!
    @IBOutlet weak var profile_StackHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var start_Btn: ButtonDesign!
    @IBOutlet weak var timerView: ViewDesign!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var name_Lbl: UILabel!
    @IBOutlet weak var pickUp_Lbl: UILabel!
    @IBOutlet weak var title_Lbl: UILabel!
    @IBOutlet weak var scheduleView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.value(forKey: "CheckScreen") != nil {
            title_Lbl.text = "Return Key In Share Box"
            UserDefaults.standard.removeObject(forKey: "CheckScreen")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                let vc = RideSummary_ViewController()
                vc.detailModel = self.detailModel
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func initialConfig() {
//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
        
        if detailModel.vehicle?.iotProvider == AppConstants.IOTProvider.WithTelemetric {
            timerView.isHidden = false
            chatView.isHidden = true
        }
        profile_StackView.isHidden = true
        profile_StackHeightConstraint.constant = 0
        name_Lbl.text = ""
        pickUp_Lbl.text = ""
        detailViewHeightConstraint.constant = 0
        self.set_Progress(Time: 600)
        self.startTimer(Time: 600)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timeRemaining), userInfo: nil, repeats: true)
        cars_CollectionView.register(UINib(nibName: "Location_Photos_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Location_Photos_CollectionViewCell")
    }
    
    @objc func timeRemaining() {
        Count += 1
    }
    
    func set_Progress(Time: Double) {
        ring.style = .ontop
        ring.innerRingWidth = 6
        ring.innerRingColor = #colorLiteral(red: 0.5764763355, green: 0.2338527441, blue: 0.8235091567, alpha: 1)
        ring.outerRingWidth = 4
        ring.outerRingColor = #colorLiteral(red: 0.7546172738, green: 0.7448188663, blue: 0.7663549185, alpha: 1)
        ring.startAngle = 270
        ring.isClockwise = false
        ring.valueFormatter = TimerRingFormatter(timerTo: Int(Time))
        ring.font = UIFont.systemFont(ofSize: 14)
        ring.translatesAutoresizingMaskIntoConstraints = false
        
    }

    private func startTimer(Time: Double) {
        
        self.ring.startTimer(to: Time) { (state) in
            switch state {
            case .finished:
                print("finished")
            case .continued:
                print("")
            default: break
            }
        } 
    }
    
    @IBAction func inspectRide_Btn(_ sender: UIButton) {
        
        let vc = Inspect_Car_ViewController()
        vc.detailModel = self.detailModel
        vc.callBack_Method = {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                self.timerView.isHidden = false
                self.start_Btn.backgroundColor = ColorConstants.TextColorPrimary
                self.start_Btn.isUserInteractionEnabled = true
                self.start_Btn.setTitle("START", for: .normal)//TextColorKey = ColorConstants.commonWhite
                self.start_Btn.setTitleColor(ColorConstants.commonWhite, for: .normal)
                let time: Double = Double(600 - self.Count)
                self.move_Arrow_View.isHidden = true
                self.inspect_Lbl.text = "Thank you for inspecting"
                self.set_Progress(Time: time)
                self.startTimer(Time: time)
            })
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func start_Ride_Btn(_ sender: UIButton) {
        
    }
    
    @IBAction func support_Btn(_ sender: UIButton) {
        support_View.isHidden =  false
    }
    
    @IBAction func supportView_CancelRide_Btn(_ sender: UIButton) {
        support_View.isHidden = true
        let vc = CancellationPopUp()
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        vc.confirmClicked = {
            
            self.navigationController?.pop_To_ViewController(ofClass: HomeVC.self, animated: true)
        }
        vc.viewCancellation_Btn = {
            let vc = CancellationPolicy_ViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        UIViewController.current()?.present(vc, animated: true, completion: {
        })
    }
    
    @IBAction func supportView_Damage_Btn(_ sender: UIButton) {
        support_View.isHidden = true
        let vc = ReportDamageVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func car_InfoDrop_Btn(_ sender: UIButton) {
        if sender.tag == 0 {
            drop_Image.image = UIImage(named: "back_down")
            sender.tag = 1
            car_InfoView.isHidden = false
//            if detailModel.vehicle?.iotProvider == AppConstants.IOTProvider.WithTelemetric {
                detailViewHeightConstraint.constant = 300
//            } else {
//                detailViewHeightConstraint.constant = 434
//            }
            
        } else {
            sender.tag = 0
             drop_Image.image = UIImage(named: "back_right")
             car_InfoView.isHidden = true
            detailViewHeightConstraint.constant = 0
        }
    }
    
    @IBAction func support_View_Support_Btn(_ sender: UIButton) {
        support_View.isHidden =  true
        let vc = SupportVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func location_Btn(_ sender: UIButton) {
        
    }
    
    @IBAction func startRide_Btn(_ sender: ButtonDesign) {
        let vc = ConfirmPopupVC()
        vc.hide = true
        vc.headerTitle = "Start Ride"
        vc.message = "Are you sure you want to start the ride?"
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        vc.confirmClicked = {
            let Vc = OnGoingRide_ViewController()
            Vc.checkScreen = "KeyPickup"
            Vc.detailModel = self.detailModel
            self.navigationController?.pushViewController(Vc, animated: true)
        }
        UIViewController.current()?.present(vc, animated: true, completion: {
        })
    }
    
    @IBAction func navigate_Btn(_ sender: UIButton) {
        
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

extension TelematicsKeyPickUpVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4//arrCollData.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        //let model = arrCollData[indexPath.row]
        let cell = cars_CollectionView.dequeueReusableCell(withReuseIdentifier: "Location_Photos_CollectionViewCell", for: indexPath) as! Location_Photos_CollectionViewCell

//        cell.setData(model, rideType: rideType, startDate: reservationStartDate, endDate: reservationEndDate)

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: 100, height: 80)
    }
}
