//
//  DriveNowVC.swift
//  WeOne
//
//  Created by Brijesh on 09/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class DriveNowVC: UIViewController {
    
    //MARK:- Variables
    var vehicleModel = VehicleModel()
    var driveNowModel : DriveNowModel = DriveNowModel()
    var arrData = [CellModel]()
    var arrcarFeatures = [CellModel]()
    var isExpand = false
    var reservationStartDate:Date?
    var reservationEndDate:Date?
    var rideType = AppConstants.RideType.Instant
    
    var isRentalExpand = false
    var isDamageReportExpand = false
    var isFromCourosal = false
    
    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnReserve: UIButtonCommon!
    
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
        
    }
    
    //MARK:- Private Methods
    private func initialConfig() {
        
//        let brandName = SyncManager.sharedInstance.fetchNameFromBrand((vehicleModel.brand ?? ""))
//
//        if brandName != nil{
//            lblTitle.text = "\(brandName ?? "") - \(vehicleModel.model ?? "") | \(vehicleModel.numberPlate ?? "")"
//        } else {

        lblTitle.text = "\(vehicleModel.getCarName) | \(vehicleModel.numberPlate ?? "")"
//        }
        
//        lblTitle.changeStringColor(string: lblTitle.text ?? "", array: [(vehicleModel.numberPlate ?? "")], colorArray: [ColorConstants.TextColorWhitePrimary],changeFontOfString: [(vehicleModel.numberPlate ?? "")], font : [FontScheme.kMediumFont(size: 20)])

        
        tableView.register(UINib(nibName: "MoreInformationTitleDescCell", bundle: nil), forCellReuseIdentifier: "MoreInformationTitleDescCell")
        tableView.register(UINib(nibName: "SliderCell", bundle: nil), forCellReuseIdentifier: "SliderCell")
        tableView.register(UINib(nibName: "FuelDetailCell", bundle: nil), forCellReuseIdentifier: "FuelDetailCell")
        tableView.register(UINib(nibName: "MoreInformationFeatureCollCell", bundle: nil), forCellReuseIdentifier: "MoreInformationFeatureCollCell")
        tableView.register(UINib(nibName: "MoreInfoCell", bundle: nil), forCellReuseIdentifier: "MoreInfoCell")
        tableView.register(UINib(nibName: "PreviousRentalReportCell", bundle: nil), forCellReuseIdentifier: "PreviousRentalReportCell")
        tableView.register(UINib(nibName: "PreviousDamageReportCell", bundle: nil), forCellReuseIdentifier: "PreviousDamageReportCell")
        
        callAPIForGetPreviousRental()
        
        btnReserve.isHidden = !isFromCourosal
        
        if rideType == AppConstants.RideType.Schedule {
            btnReserve.setText(text: "KLblBook".localized)
        } else {
            btnReserve.setText(text: "kLblRESERVE".localized)
        }
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.contentInsetAdjustmentBehavior = .never

        btnReserve_Click()
        

    }
    
    func prepareDataSource() {
        
        arrData.removeAll()
        arrcarFeatures.removeAll()
        
        arrData.append(CellModel.getModel(placeholder: driveNowModel.vehicleDetails?.fuelLevelStr ?? "", placeholder2: "\(Int(driveNowModel.vehicleDetails?.estimatedKmPerFuel ?? 0.0)) \("KLblKm".localized)", type: .DNSlider, dataArr : driveNowModel.vehicleDetails?.images ?? [String]()))
     //  arrData.append(CellModel.getModel(type: .DNFuelDetailCell))
        
        arrcarFeatures = vehicleModel.getCarProperties(isLoadAll: true)
        
        if arrcarFeatures.count > 0 {
            arrData.append(CellModel.getModel(type: .DNCarFeature))
        }
        
        arrData.append(CellModel.getModel(placeholder: "More Information", type: .DNMoreInformation, isExpand:self.isExpand))
        
        if self.isExpand {
            // Other Description
            if let arrOtherDesc = driveNowModel.vehicleDetails?.otherDescriptions, arrOtherDesc.count > 0 {
                for obj in arrOtherDesc {
                    
                    let master = SyncManager.sharedInstance.fetchMasterWithId(obj.id ?? "")
                    
                    if master.count > 0 {
                        arrData.append(CellModel.getModel(placeholder: master.first!.name, text: obj.desc,  type: .CMITitleDesc, imageName: master.first!.image ?? "", cellObj: obj))
                        
                        if master.first!.code?.lowercased() == AppConstants.MasterCode.Fuel {
                            
                            let strDiscount = String(format : "KLblFuelDiscount".localized, Utilities.convertToCurrency(number: driveNowModel.vehicleDetails?.fuelDiscount ?? 0,  currencyCode: driveNowModel.vehicleDetails?.currency))
                            let discount = "<p><span style=\"font-size: 16px;\">\(strDiscount)</span></p>"
                            let strExtraCharge = String(format : "KLblFuelExtraCharge".localized, Utilities.convertToCurrency(number: driveNowModel.vehicleDetails?.fuelDiscount ?? 0, currencyCode: driveNowModel.vehicleDetails?.currency))
                            let extraCharge = "<p><span style=\"font-size: 16px;\">\(strExtraCharge)</span></p>"
                            arrData.append(CellModel.getModel(placeholder: "KLblRefuellingRules".localized, text: "\(discount) \n \(extraCharge)",  type: .CMITitleDesc, cellObj: obj, isSelected: true))
                        }
                    }
                }
            }
        }
        arrData.append(CellModel.getModel(placeholder: "Previous Rental Report", type: .DNPreviousRentalReport, isExpand: isRentalExpand ? true : false))
        arrData.append(CellModel.getModel(placeholder: "Previous Damage Report", type: .DNDamageReport, isExpand: isDamageReportExpand ? true : false))
               

        tableView.reloadData()
        
    }
    
    @IBAction func btnBack_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func btnReserve_Click(){
        btnReserve.click = {
            self.openCarDetail()
        }
    }
}

//MARK:- UITableView's Data Source & Delegate Methods
extension DriveNowVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = arrData[indexPath.row]
        
        switch model.cellType! {
        case .DNSlider:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SliderCell") as? SliderCell
            cell?.setData(model: model)
            return cell!
            
        case .DNFuelDetailCell:
            let cell = tableView.dequeueReusableCell(withIdentifier: "FuelDetailCell") as? FuelDetailCell
            cell?.setData(model: driveNowModel.vehicleDetails ?? VehicleModel())
            return cell!
            
        case .DNCarFeature:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInformationFeatureCollCell") as? MoreInformationFeatureCollCell
            cell?.setCellData(arrcarFeatures)
            return cell!
            
        case .DNPreviousRentalReport:
            if model.isExpand {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PreviousRentalReportCell") as? PreviousRentalReportCell
                cell?.setData(arrPreviousRentals: driveNowModel.previousRentals)
                
                cell?.btnExpand.tag = indexPath.row
                cell?.btnExpand.addTarget(self, action: #selector(btnPreviousRentalReport_Click(_:)), for: .touchUpInside)
                cell?.beginUpdate = {
                    
                    self.tableView?.beginUpdates()
                }
                
                cell?.didExpandLabel = {
                    
                    model.isExpand = true
                    self.tableView?.endUpdates()
                }
                
                cell?.didCollapseLabel = {
                    
                    model.isExpand = false
                    self.tableView?.endUpdates()
                }
                return cell!
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInfoCell") as? MoreInfoCell
                cell?.setData(model: model, isExpand : model.isExpand)
                return cell!
            }
            
        case .DNDamageReport:
            if model.isExpand {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PreviousDamageReportCell") as? PreviousDamageReportCell
                cell?.setData(arrDamageReport: driveNowModel.lastDamageReport, vehicleModel: driveNowModel.vehicleDetails ?? VehicleModel())
                
                cell?.btnExpand.tag = indexPath.row
                cell?.btnExpand.addTarget(self, action: #selector(btnPreviousRentalReport_Click(_:)), for: .touchUpInside)
                return cell!
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInfoCell") as? MoreInfoCell
                cell?.setData(model: model,  isExpand : model.isExpand)
                return cell!
            }
            
            
        case .DNMoreInformation:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInfoCell") as? MoreInfoCell
            cell?.setData(model: model, isExpand: self.isExpand)
            return cell!
            
        case .CMITitleDesc:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInformationTitleDescCell") as? MoreInformationTitleDescCell
            cell?.setData(model: model, isExpand: isExpand)
            return cell!
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = arrData[indexPath.row]
                    
        if model.cellType == .DNMoreInformation {
            self.isExpand = !self.isExpand
            self.prepareDataSource()
        }
        else if model.cellType == .DNDamageReport ||  model.cellType == .DNPreviousRentalReport{
            model.isExpand = !model.isExpand
            tableView.reloadData()
        }
    }
    
    @objc func btnPreviousRentalReport_Click(_ sender: UIButton) {
        let model = arrData[sender.tag]
        model.isExpand = !model.isExpand
        tableView.reloadData()
    }
    
    func openCarDetail() {
        
        let vc = CheckMoreInformationVC(nibName: "CheckMoreInformationVC", bundle: nil)
        let nav = UINavigationController(rootViewController: vc)
        vc.isFromCarMap = true
        //vc.strVehicleId = vehicleModel.id ?? ""
        vc.rideType = self.rideType
        vc.startDate = reservationStartDate
        vc.endDate = reservationEndDate
        vc.vehicleModel = vehicleModel
        
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
    }
}

//MARK: - API Calling
extension DriveNowVC {
    
    func callAPIForGetPreviousRental() {
        
        let reqDict = ["vehicleId" : vehicleModel.id ?? ""]
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.GetPreviousRental, method: .post, parameters: reqDict, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            //save Data
            if let responseDict = response as? [String:Any] {
                
                self.driveNowModel = Mapper<DriveNowModel>().map(JSON: responseDict)!
            }
            
            self.prepareDataSource()
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(title: AppConstants.AppName, message: failureMessage)
        }
    }
}


