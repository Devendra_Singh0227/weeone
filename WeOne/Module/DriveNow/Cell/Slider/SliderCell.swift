//
//  SliderCell.swift
//  WeOne
//
//  Created by Brijesh on 09/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class SliderCell: UITableViewCell {

    //MARK: - Variables
    var arrImages = [String]()
    var timer = Timer()
    var currentIndex = 0
    
    //MARK: - Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl : UIPageControl!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewVerticleLine: UIView!
    
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var viewFuel: UIView!

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnLeftArrow: UIButton!
    @IBOutlet weak var btnRightArrow: UIButton!

    @IBOutlet weak var lblFuelPercentage: UILabel!
    @IBOutlet weak var lblTotalKm: UILabel!

    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        
//        collectionViewHeight.constant = (AppConstants.ScreenSize.SCREEN_WIDTH - 32) * 0.52
        collectionViewHeight.constant = 174
        collectionView.register(UINib(nibName: "SliderCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SliderCollectionCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        
        viewFuel.backgroundColor = ColorConstants.UnderlineColor.withAlphaComponent(0.80)
        
        if model.cellType == .DNSlider || model.cellType == .ReportDamageListVC {
            if model.cellType == .ReportDamageListVC {
                viewTitle.isHidden = false
                collectionViewHeight.constant = 234
                stackViewHeight.constant = 266
            } else {
                stackViewHeight.constant = 234
            }
            
            lblFuelPercentage.text = model.placeholder
            
            if let km : Double = Double(model.placeholder2 ?? ""), km > 0.0 {
                lblTotalKm.text = model.placeholder2
            } else {
                lblTotalKm.isHidden = true
                viewVerticleLine.isHidden = true
            }
            
            viewFuel.isHidden = false 
        } else {
            stackViewHeight.constant = 174
        }
        
        arrImages.removeAll()
        if let arr = model.dataArr as? [String] {
            arrImages = arr
        }
        
        if arrImages.count > 1 {
            btnLeftArrow.isHidden = false
            btnRightArrow.isHidden = false
        } else {
            btnLeftArrow.isHidden = true
            btnRightArrow.isHidden = true
        }
        
        timer.invalidate()
//        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(callTimer), userInfo: nil, repeats: true)
        
//        pageControl.numberOfPages = arrImages.count
//        pageControl.hidesForSinglePage = true
//        pageControl.currentPage = 0
        
        collectionView.reloadData()
    }
    
    @objc func callTimer() {
        
        if currentIndex < (arrImages.count - 1) {
            currentIndex += 1
        }
        else {
            currentIndex = 0
        }
        updatePager()
        self.collectionView.scrollToItem(at: IndexPath(item: currentIndex, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    func updatePager() {
        pageControl.currentPage = currentIndex
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var visibleRect = CGRect()
        
        visibleRect.origin = collectionView.contentOffset
        visibleRect.size = collectionView.bounds.size
        
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        
        guard let indexPath = collectionView.indexPathForItem(at: visiblePoint) else { return }
        
        currentIndex = indexPath.row
        updatePager()
    }
    
    //MARK :- UIButton Action
    @IBAction func btnLeftArrow_Click(_ sender: UIButton) {
        
        let collectionBounds = self.collectionView.bounds
        let contentOffset = CGFloat(floor(self.collectionView.contentOffset.x - collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)

    }

    @IBAction func btnRight_Click(_ sender: UIButton) {
        let collectionBounds = self.collectionView.bounds
        let contentOffset = CGFloat(floor(self.collectionView.contentOffset.x + collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    
    func moveCollectionToFrame(contentOffset : CGFloat) {
        let frame: CGRect = CGRect(x : contentOffset ,y : self.collectionView.contentOffset.y ,width : self.collectionView.frame.width,height : self.collectionView.frame.height)
        self.collectionView.scrollRectToVisible(frame, animated: true)
    }


}


//MARK: - UICollectionView's DataSource & Delegate Methods
extension SliderCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let model = arrImages[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCollectionCell", for: indexPath) as? SliderCollectionCell
        cell?.setImage(strURL: model)
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        let model = arrImages[indexPath.row]
//        Utilities.openYoutubeVideo(videoIdentifier: model.youtube_video_id ?? "")
//
//    }
}
