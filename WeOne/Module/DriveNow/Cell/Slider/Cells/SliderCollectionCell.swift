//
//  SliderCollectionCell.swift
//  WeOne
//
//  Created by Brijesh on 09/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class SliderCollectionCell: UICollectionViewCell {

    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var imageView: UIImageView!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: Set Data
    func setImage(strURL: String) {
        if strURL.count > 0 , let url = URL(string: "\(AppConstants.serverURL)\(strURL)") {
            imageView.setImageForURL(url: url, placeHolder: UIImage(named: "carPlaceholder"))
        }

    }
    func setData(images: ImagesModel) {
        
        if images.path?.count ?? 0 > 0 , let url = URL(string: "\(AppConstants.serverURL)\(images.path ?? "")") {
            imageView.setImageForURL(url: url, placeHolder: UIImage(named: "carPlaceholder"))
        }
    }
    

}
