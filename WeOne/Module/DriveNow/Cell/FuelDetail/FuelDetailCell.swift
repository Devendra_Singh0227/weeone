//
//  FuelDetailCell.swift
//  WeOne
//
//  Created by Brijesh on 09/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class FuelDetailCell: UITableViewCell {

    //MARK:- Variables
    
    //MARK:- Outlets
    @IBOutlet weak var lblFuelPercentage: UILabel!
    @IBOutlet weak var lblTotalKm: UILabel!
    
    //MARK:- View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- setData
    func setData(model: VehicleModel) {
        
        lblFuelPercentage.text = model.fuelLevelStr ?? "-"
        lblTotalKm.text = "\(Int(model.totalKm)) \("KLblKm".localized)"
    }
}
