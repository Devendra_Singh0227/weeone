//
//  DamageReportReasonCell.swift
//  WeOne
//
//  Created by iMac on 29/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class DamageReportReasonCell: UICollectionViewCell {

    //MARK:- Variables
    
    //MARK:- Outlets
    @IBOutlet weak var btnImg: UIButton!
    @IBOutlet weak var lblText: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK:- setData
    func setData(categoryId:Int) {
        btnImg.setImage(UIImage(named: ThemeManager.sharedInstance.getImage(string: "checked")), for: .normal)

        lblText.text = Utilities.returnDamageCaterory([categoryId])
    }


}
