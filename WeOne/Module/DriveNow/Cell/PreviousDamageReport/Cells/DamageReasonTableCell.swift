//
//  DamageReasonTableCell.swift
//  WeOne
//
//  Created by Brijesh on 10/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class DamageReasonTableCell: UITableViewCell {
    
    //MARK:- Variables
    
    //MARK:- Outlets
    @IBOutlet weak var btnImg: UIButton!
    @IBOutlet weak var lblText: UILabel!
    
    //MARK:- View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- setData
    func setData(categoryId:Int) {
        btnImg.isSelected = true
        lblText.text = Utilities.returnDamageCaterory([categoryId])
    }
    
}
