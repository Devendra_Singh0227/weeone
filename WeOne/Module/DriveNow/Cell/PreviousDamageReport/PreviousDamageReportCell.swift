//
//  PreviousDamageReportCell.swift
//  WeOne
//
//  Created by Brijesh on 10/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class PreviousDamageReportCell: UITableViewCell {
    
    //MARK:- Variables
    var arrDamageReport = [DamageReportModel]()
    var selectedIndex = 0
    var vehicleModel:VehicleModel = VehicleModel()
    
    //MARK:- Outlets
    @IBOutlet weak var collectViewNumber: UICollectionView!
    @IBOutlet weak var collectViewImages: UICollectionView!
    @IBOutlet weak var collectViewReason: UICollectionView!

    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnExpand: UIButton!
//    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collViewReasonHeight: NSLayoutConstraint!
    @IBOutlet weak var viewNoDataFound: UIView!
    @IBOutlet weak var collectionViewWidth: NSLayoutConstraint!

    //MARK:- View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        
        collectViewNumber.register(UINib(nibName: "CollectionNumberCell", bundle: nil), forCellWithReuseIdentifier: "CollectionNumberCell")
        collectViewImages.register(UINib(nibName: "CollectionImagesCell", bundle: nil), forCellWithReuseIdentifier: "CollectionImagesCell")
        
        collectViewReason.register(UINib(nibName: "DamageReportReasonCell", bundle: nil), forCellWithReuseIdentifier: "DamageReportReasonCell")

      //  tableView.register(UINib(nibName: "DamageReasonTableCell", bundle: nil), forCellReuseIdentifier: "DamageReasonTableCell")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- setData
    func setData(arrDamageReport:[DamageReportModel], vehicleModel : VehicleModel) {
        self.vehicleModel = vehicleModel
        self.arrDamageReport.removeAll()
        self.arrDamageReport = arrDamageReport
        if self.arrDamageReport.count > 0 {
            self.arrDamageReport = self.arrDamageReport.map({ (obj) -> DamageReportModel in
                obj.isSelected = false
                return obj
            })
            self.arrDamageReport.first?.isSelected = true
            selectedIndex = 0
            self.arrDamageReport.append(DamageReportModel())
            collectViewNumber.isHidden = false
            collectViewImages.isHidden = false
            collectViewReason.isHidden = false
            viewNoDataFound.isHidden = true
        }
        else {
            collectViewNumber.isHidden = true
            collectViewImages.isHidden = true
            collectViewReason.isHidden = true
            viewNoDataFound.isHidden = false
        }
        

        reloadData()
    }
    
    
    func reloadData() {
        
        if arrDamageReport.count > selectedIndex {
            let model = arrDamageReport[selectedIndex]
            lblDesc.text = model.desc
            if arrDamageReport[selectedIndex].damageCategory.count > 2 {
                collViewReasonHeight.constant = 34 * 2
            } else {
                collViewReasonHeight.constant = 34
            }
        }
        
        collectionViewWidth.constant = CGFloat((arrDamageReport.count * 26 + ((arrDamageReport.count - 1) * 10)))

        collectViewNumber.reloadData()
        collectViewImages.reloadData()
        collectViewReason.reloadData()

//        tableView.reloadData()
    }
    
    
}
//MARK:- UICollectionView's Data Source & Delegate Methods
extension PreviousDamageReportCell : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectViewNumber {
            return arrDamageReport.count
        } else if collectionView == collectViewImages {
            if arrDamageReport.count > selectedIndex {
                return arrDamageReport[selectedIndex].images.count
            }
            return 0
        } else if collectionView == collectViewReason {
            if arrDamageReport.count > selectedIndex {
                return arrDamageReport[selectedIndex].damageCategory.count
            }
            return 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectViewNumber {
            return CGSize(width: 26, height: collectionView.bounds.height);
        }
        else if collectionView == collectViewImages {
            return CGSize(width: 82, height: 80);
        } else if collectionView == collectViewReason {
//            let model = arrDamageReport[selectedIndex].damageCategory[indexPath.row]
            return CGSize(width: collectionView.bounds.width / 2, height: 34)
            //            }
        }
        return CGSize(width: 0, height: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectViewNumber {
            let model =  arrDamageReport[indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionNumberCell", for: indexPath) as? CollectionNumberCell
            if indexPath.row == arrDamageReport.count - 1{
                cell?.lblNumber.text = "All"
            }
            else {
                cell?.lblNumber.text = "\(indexPath.row + 1)"
            }
            
            cell?.setData(model: model)
            return cell!
        } else if collectionView == collectViewImages {
            
            let model =  arrDamageReport[selectedIndex].images[indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionImagesCell", for: indexPath) as? CollectionImagesCell
            cell?.setData(images: model)
            return cell!
            
        } else if collectionView == collectViewReason {
            let model = arrDamageReport[selectedIndex].damageCategory[indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DamageReportReasonCell", for: indexPath) as? DamageReportReasonCell
            cell?.setData(categoryId:model)
            return cell!

        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectViewNumber {
            if  indexPath.row == arrDamageReport.count - 1 {
                let vc = ReportDamageListVC()
                vc.vehicleModel = vehicleModel
                UIViewController.current().navigationController?.pushViewController(vc, animated: true)
            }
            else {
                let model =  arrDamageReport[indexPath.row]
               arrDamageReport = arrDamageReport.map({ (obj) -> DamageReportModel in
                   obj.isSelected = false
                   return obj
               })
               model.isSelected = true
               selectedIndex = indexPath.row
               reloadData()
            }
        }
    }
}

