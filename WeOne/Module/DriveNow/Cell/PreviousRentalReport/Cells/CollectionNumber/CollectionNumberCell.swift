//
//  CollectionNumberCell.swift
//  WeOne
//
//  Created by Brijesh on 10/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class CollectionNumberCell: UICollectionViewCell {
    
    //MARK:- Variables
    
    //MARK:- Outlets
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblNumber: UILabel!
    
    //MARK:- View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK:- setData
    func setData(model:PreviousRentalsModel) {
        if model.isSelected {
            viewBg.backgroundColor = ColorConstants.ThemeColor
            viewBg.layer.borderColor = ColorConstants.ThemeColor.cgColor
            lblNumber.textColor = .white
        }
        else {
            viewBg.backgroundColor = .white
            viewBg.layer.borderColor = ColorConstants.TextColorSecondary.cgColor
            lblNumber.textColor = .black
        }
    }
    
    func setData(model:DamageReportModel) {
        if model.isSelected {
            viewBg.backgroundColor = ColorConstants.ThemeColor
            viewBg.layer.borderColor = ColorConstants.ThemeColor.cgColor
            lblNumber.textColor = .white
        }
        else {
            viewBg.backgroundColor = .white
            viewBg.layer.borderColor = ColorConstants.TextColorSecondary.cgColor
            lblNumber.textColor = .black
        }
    }

}
