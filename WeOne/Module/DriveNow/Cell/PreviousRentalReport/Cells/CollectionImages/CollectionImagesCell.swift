//
//  CollectionImagesCell.swift
//  WeOne
//
//  Created by Brijesh on 10/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class CollectionImagesCell: UICollectionViewCell {
    
    //MARK:- Variables
    
    //MARK:- Outlets
    @IBOutlet weak var imgView: UIImageView!
    
    //MARK:- View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK:- setData
    func setImage(strURL: String) {
        if strURL.count > 0 , let url = URL(string: "\(AppConstants.serverURL)\(strURL)") {
           imgView.setImageForURL(url: url, placeHolder: UIImage(named: "carPlaceholder"))
       }
    }

    func setData(images: ImagesModel) {
        if images.path?.count ?? 0 > 0 , let url = URL(string: "\(AppConstants.serverURL)\(images.path ?? "")") {
           imgView.setImageForURL(url: url, placeHolder: UIImage(named: "carPlaceholder"))
       }
    }

}
