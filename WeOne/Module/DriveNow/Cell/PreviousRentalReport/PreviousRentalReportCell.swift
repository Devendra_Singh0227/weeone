//
//  PreviousRentalReportCell.swift
//  WeOne
//
//  Created by Brijesh on 09/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class PreviousRentalReportCell: UITableViewCell {
    
    //MARK:- Variables
    var beginUpdate : (()->())?
    var didExpandLabel : (()->())?
    var didCollapseLabel : (()->())?
    var arrPreviousRentals = [PreviousRentalsModel]()
    var selectedIndex = 0
    
    //MARK:- Outlets
    @IBOutlet weak var collectViewNumber: UICollectionView!
    @IBOutlet weak var collectViewImages: UICollectionView!
    @IBOutlet weak var lblKm: UILabel!
    @IBOutlet weak var lblFuelPercentage: UILabel!
    @IBOutlet weak var lblTotalKm: UILabel!
    @IBOutlet weak var lblDesc: ExpandableLabel!
    @IBOutlet weak var btnExpand: UIButton!
    @IBOutlet weak var viewNoDataFound: UIView!
    @IBOutlet weak var viewMain: CustomDashedView!

    @IBOutlet weak var imgAfterOdometer: UIImageView!
    @IBOutlet weak var collectionViewWidth: NSLayoutConstraint!

    
    //MARK:- View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        
        Utilities.configureExpandableLabel(label: lblDesc)
        lblDesc.delegate = self
        
        
        collectViewNumber.register(UINib(nibName: "CollectionNumberCell", bundle: nil), forCellWithReuseIdentifier: "CollectionNumberCell")
        collectViewImages.register(UINib(nibName: "CollectionImagesCell", bundle: nil), forCellWithReuseIdentifier: "CollectionImagesCell")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- setData
    func setData(arrPreviousRentals:[PreviousRentalsModel]) {
        self.arrPreviousRentals.removeAll()
        self.arrPreviousRentals = arrPreviousRentals
        if self.arrPreviousRentals.count > 0 {
            self.arrPreviousRentals = self.arrPreviousRentals.map({ (obj) -> PreviousRentalsModel in
                obj.isSelected = false
                return obj
            })
            self.arrPreviousRentals.first?.isSelected = true
            selectedIndex = 0
            
            collectViewNumber.isHidden = false
            collectViewImages.isHidden = false
            viewNoDataFound.isHidden = true
            
            collectionViewWidth.constant = CGFloat((arrPreviousRentals.count * 26 + ((arrPreviousRentals.count - 1) * 10)))
            
            reloadData()
            
        } else{
            collectViewNumber.isHidden = true
            collectViewImages.isHidden = true
            viewNoDataFound.isHidden = false
        }
        
    }
    func reloadData() {
        
        if arrPreviousRentals.count > selectedIndex {
            let model = arrPreviousRentals[selectedIndex]
            lblFuelPercentage.text = model.fuelLevelStr ?? "-"
            lblTotalKm.text = "\("KlblDistanceTravelled".localized) \(model.totalKm) \("KLblKm".localized)"
            lblDesc.text = model.comment ?? "-"
            lblDesc.collapsed = !model.isExpand
            
            imgAfterOdometer.isHidden = true
            if let strUrl =  model.afterOdometer , strUrl.count > 0 , let url = URL(string: "\(AppConstants.serverURL)\(strUrl)") {
                imgAfterOdometer.isHidden = false

                imgAfterOdometer.setImageForURL(url: url, placeHolder: nil)
                imgAfterOdometer.contentMode = .scaleAspectFill
                viewMain.betweenDashesSpace = 0

            }
            else {
//                imgAfterOdometer.image = UIImage(named: "Odometer-2")
            }
        }
        collectViewNumber.reloadData()
        collectViewImages.reloadData()
    }
}

//MARK:- UICollectionView's Data Source & Delegate Methods
extension PreviousRentalReportCell : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectViewNumber {
             return arrPreviousRentals.count
        }
        else if collectionView == collectViewImages {
            if arrPreviousRentals.count > 0 {
                return arrPreviousRentals[selectedIndex].afterAttachment.count
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectViewNumber {
            let model =  arrPreviousRentals[indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionNumberCell", for: indexPath) as? CollectionNumberCell
            cell?.lblNumber.text = "\(indexPath.row + 1)"
            cell?.setData(model: model)
            return cell!
        }
        else if collectionView == collectViewImages {
             let model =  arrPreviousRentals[selectedIndex].afterAttachment[indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionImagesCell", for: indexPath) as? CollectionImagesCell
            cell?.setData(images: model)
            return cell!
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectViewNumber {
            
            let model =  arrPreviousRentals[indexPath.row]
            arrPreviousRentals = arrPreviousRentals.map({ (obj) -> PreviousRentalsModel in
                obj.isSelected = false
                return obj
            })
            model.isSelected = true
            selectedIndex = indexPath.row
            reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectViewNumber {
            return CGSize(width: 26, height: collectionView.bounds.height);
        }
        else if collectionView == collectViewImages {
             return CGSize(width: 82, height: collectionView.bounds.height);
        }
        return CGSize(width: 0, height: 0);
    }
}

// MARK: ExpandableLabel Delegate
extension PreviousRentalReportCell : ExpandableLabelDelegate {

    func willExpandLabel(_ label: ExpandableLabel) {

        self.beginUpdate?()
    }

    func didExpandLabel(_ label: ExpandableLabel) {

        self.didExpandLabel?()
    }

    func willCollapseLabel(_ label: ExpandableLabel) {

        self.beginUpdate?()
    }

    func didCollapseLabel(_ label: ExpandableLabel) {

        self.didCollapseLabel?()
    }
}
