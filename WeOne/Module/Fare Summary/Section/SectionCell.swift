//
//  SectionCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 14/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class SectionCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnDropDown: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setCellData(_ model : CellModel){
        lblTitle.text = model.placeholder
        
        if model.placeholder == "KLblTrip".localized {
            btnDropDown.isHidden = false 
            btnDropDown.isSelected = model.isSelected
        }
    }
}
