//
//  FareSummaryVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 14/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class FareSummaryVC: UIViewController {
    
    //MARK: Variables
    var arrCellData : [CellModel] = [CellModel]()
    var rideModel:RideListModel = RideListModel()
    
    var isFromYourTrip = false
    
    //MARK: Outlets
    @IBOutlet weak var viewNavigation: UIViewCommon!
    @IBOutlet weak var viewSendReceipt: UIView!
    @IBOutlet weak var viewPay: UIView!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnPay: UIButtonCommon!
    @IBOutlet weak var constrainbtnPayHeight: NSLayoutConstraint!

    @IBOutlet weak var btnSendReceipt: UIButton!
    
    //MARK: view life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
    }
    
    //MARK: Initial config
    func initialConfig(){
        
        viewNavigation.backgroundColor = ColorConstants.ThemeColor
        viewNavigation.text = "KLblRideSummary".localized as NSString
        viewNavigation.button.setImage(#imageLiteral(resourceName: "close"), for: .normal)
        
        prepareDataSource()
        
        if rideModel.isPaid {
            btnPay.setText(text: "KLblDone".localized)
        } else {
            btnPay.setText(text: "KLblProceedToPay".localized)
        }
        
        btnPay.click = {
            self.btnPay_Click()
        }
        
        
        viewNavigation.click = {
            self.btnClose_Click()
        }
        
        tableView.register(UINib(nibName: "FareSummaryInfoCell", bundle: nil), forCellReuseIdentifier: "FareSummaryInfoCell")
        tableView.register(UINib(nibName: "TripInfoCell", bundle: nil), forCellReuseIdentifier: "TripInfoCell")
        tableView.register(UINib(nibName: "SectionCell", bundle: nil), forCellReuseIdentifier: "SectionCell")
        tableView.register(UINib(nibName: "MapCell", bundle: nil), forCellReuseIdentifier: "MapCell")
        
        tableView.register(UINib(nibName: "MoreInformationCardCell", bundle: nil), forCellReuseIdentifier: "MoreInformationCardCell")
        tableView.register(UINib(nibName: "BreakdownCell", bundle: nil), forCellReuseIdentifier: "BreakdownCell")
        tableView.register(UINib(nibName: "ReleaseAmountCell", bundle: nil), forCellReuseIdentifier: "ReleaseAmountCell")
        
        btnSendReceipt.addTarget(self, action: #selector(btnSendReceipt_Click), for: .touchUpInside)
        btnSendReceipt.addTarget(self, action: #selector(holdDown(_:)), for: .touchDown)
        
        if !isFromYourTrip {
            showKeyHandaling()
        }
        
        if isFromYourTrip {
            viewPay.isHidden = true
            constrainbtnPayHeight.constant = 0
            self.tableView.tableFooterView = viewSendReceipt
            btnSendReceipt.layer.cornerRadius = btnSendReceipt.frame.height / 2
        } else {
            viewPay.isHidden = false
            constrainbtnPayHeight.constant = 90
        }

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
    }
    
    //MARK: Prepare DataSource
    func prepareDataSource(){
        
        arrCellData.removeAll()
        
        arrCellData.append(CellModel.getModel(type: .FareSummaryCarInfo, cellObj:rideModel))
        
        if rideModel.startLocation != nil{
            arrCellData.append(CellModel.getModel(type: .FareSummaryTripInfo, cellObj:rideModel))
        }
        
        let currency = rideModel.partnerSetting?.currency
        
        //Rental time
        
//        let priceConfig = "(\(rideModel.pricingConfigData?.hours ?? 0) \((rideModel.pricingConfigData?.hours ?? 0 > 1) ? "KLblHoursCapital".localized : "KLblHourCapital".localized) \(Utilities.convertToCurrency(number: rideModel.fareSummary?.selectedPackageRate ?? 0, currencyCode: currency)))"
        
        
        let dailyPackage = rideModel.pricingConfigData?.hours == 24 ? rideModel.pricingConfigData : nil
        let hourlyPackage = rideModel.pricingConfigData?.hours == 1 ? rideModel.pricingConfigData : nil

        var description = ""
        
        if dailyPackage != nil {
            description = "1 day \(Utilities.convertToCurrency(number: dailyPackage?.rate ?? 0.0, currencyCode: currency))"
        }
        
            var rate = 0.0
            if hourlyPackage?.rate != 0.0 && hourlyPackage?.rate != nil {
                rate = hourlyPackage?.rate ?? 0.0
            } else if rideModel.pricingConfigData?.perHourCharge != 0.0 && rideModel.pricingConfigData?.perHourCharge != nil {
                rate = rideModel.pricingConfigData?.perHourCharge ?? 0.0
            }
            if description != "" {
                description = description + ", 1 hour \(Utilities.convertToCurrency(number: (rate), currencyCode: currency))"
            } else {
                description = "1 hour \(Utilities.convertToCurrency(number: (rate), currencyCode: currency))"
            }

        var rentalTime = ""
        var amount = ""
        if rideModel.rideType == AppConstants.RideType.Schedule {
            rentalTime = "\("KLblRentalTime".localized)"
            amount = Utilities.convertToCurrency(number: rideModel.fareSummary?.totalPackageRate ?? 0, currencyCode: currency)
        } else {
            rentalTime = "\("KLblRentalTime".localized) X 1 Hour"
            amount = Utilities.convertToCurrency(number: rate, currencyCode: currency)
        }
        
        arrCellData.append(CellModel.getModel(placeholder: rentalTime , text: amount, userText1: "(\(description))", type: .PriceBreakUpCell))
                
        //Insurance
        if rideModel.fareSummary?.insurance ?? 0.0 != 0.0 {
            arrCellData.append(CellModel.getModel(placeholder : "KLblInsurance".localized, text : rideModel.getInsurance(), type: .PriceBreakUpCell))
        }
        
        //Already Paid
        if rideModel.fareSummary?.advancedAmount ?? 0.0 != 0.0 {
            arrCellData.append(CellModel.getModel(placeholder : "KLblAlreadyPaid".localized, text : Utilities.convertToCurrency(number:  rideModel.fareSummary?.advancedAmount ?? 0, currencyCode : currency), type: .FareSummaryAlreadyPaid))
        }
        
        //Rental time
        
        let totalRentalTime = ((rideModel.fareSummary?.totalPackageRate ?? 0.0) - rate)
        
        if rideModel.rideType == AppConstants.RideType.Instant && rideModel.totalTime > 60 {
            arrCellData.append(CellModel.getModel(placeholder: "\("KLblRentalTime".localized) \(getUnpaidRentalTime()) ", text: Utilities.convertToCurrency(number: totalRentalTime, currencyCode: currency), userText1: "(\(description))", type: .PriceBreakUpCell))
        }
        
        
        //Extra distance
        if Utilities.getDoubleValue(value: rideModel.fareSummary?.extraDistnce) > 0.0{
            
            let extraValue = "(\(rideModel.pricingConfigData?.additionalInfo?.distanceCap ?? 100) \("KLblKmIncluded".localized), \(Utilities.convertToCurrency(number: rideModel.pricingConfigData?.additionalInfo?.additionalDistanceRate ?? 100, currencyCode: currency))/\("KLblKm".localized))"
            
            arrCellData.append(CellModel.getModel(placeholder: "\(Utilities.getDoubleValue(value: rideModel.fareSummary?.extraDistnce)) \("KLblKmCap".localized) \("KLblExtra".localized)", text: Utilities.convertToCurrency(number: rideModel.fareSummary?.addtionalRate ?? 0.0, currencyCode : currency), userText1: extraValue, type: .PriceBreakUpCell))
            
        }
        
        
        //toll road
        if rideModel.fareSummary?.tax ?? 0.0 != 0.0 {
            arrCellData.append(CellModel.getModel(placeholder : "KLblTollRoad".localized, text :" \(Utilities.convertToCurrency(number: rideModel.fareSummary?.tax ?? 0, currencyCode: currency))" , type: .PriceBreakUpCell, classType: .FareSummaryVC))
        }
        
        
        //Fuel charges
        if rideModel.fareSummary?.fuelCharges ?? 0.0 != 0.0{
            arrCellData.append(CellModel.getModel(placeholder : "KLblFuelCharges".localized, text : rideModel.getFuleCharge(), type: .PriceBreakUpCell))
        }
        
        
        //Payable AmountNow
        if rideModel.fareSummary?.chargeableAmount ?? 0.0 != 0.0 {
            arrCellData.append(CellModel.getModel(placeholder : "KLblPayableAmountNow".localized, text : Utilities.convertToCurrency(number:  rideModel.fareSummary?.chargeableAmount ?? 0, currencyCode : currency), type: .FareSummaryAlreadyPaid))
        }
        
        //total fare
        if rideModel.fareSummary?.totalWithoutDiscount ?? 0.0 != 0.0{
            arrCellData.append(CellModel.getModel(placeholder : "KLblTotal".localized, text : Utilities.convertToCurrency(number:  rideModel.fareSummary?.totalWithoutDiscount ?? 0.0, currencyCode : currency), type: .PriceBreakUpCell, isSelected: true))
        }
        
        //Release Amount
        arrCellData.append(CellModel.getModel(type: .FareSummaryReleaseAmount, cellObj:rideModel ))
        
        tableView.reloadData()
        
    }
    
    func getUnpaidRentalTime() -> String {
        let extraTime = rideModel.totalTime - 60
        let hours = extraTime / 60
        return "X \(hours) \((hours > 1) ? "Hours" :"Hour")"
    }
    //MARK:- IBActions
    func showKeyHandaling() {
        let vc = KeyHandlingPopupVC(nibName: "KeyHandlingPopupVC", bundle: nil)
        
        vc.headerTitle = "KKeyHandling".localized
        vc.model = rideModel.keyHandling
        
        vc.isFromReserve = false
        
        vc.confirmClicked = {
            // self.bookNow()
        }
        
        vc.showOnMapClicked = {
            
            let vc = SetLocationVC()
            
            vc.geoLocation = self.rideModel.geoLocation
            vc.isFromKeyHandling = true
            vc.getLocation = { placeModel in
                
            }
            self.navigationController?.pushViewController(vc, animated: true)
            
            
            //            self.dismiss(animated: true, completion: nil)
        }
        
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        
        if UIViewController.current()?.presentedViewController != nil {
            UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
            })
        }
        else{
            UIViewController.current()?.present(vc, animated: true, completion: {
            })
        }
        
    }
    func btnClose_Click() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func btnPay_Click() {
        
        if rideModel.isPaid {
            
            moveToRating()
        }
        else {
            // PROCEED TO PAY CODE HERE
            callApiForPayment()
        }
    }
    
    
    @IBAction func btnSendReceipt_Click(_ sender: UIButton) {
        btnSendReceipt.alpha = 1
        btnSendReceipt.setTitleColor(ColorConstants.TextColorWhitePrimary, for: .normal)
        btnSendReceipt.backgroundColor = ColorConstants.TextColorPrimary
        
        callApiForSendReceipt()
    }
    
    @objc func holdDown(_ sender: UIButton) {
        btnSendReceipt.alpha = 0.5
        btnSendReceipt.setTitleColor(ColorConstants.TextColorPrimary, for: .normal)
        btnSendReceipt.backgroundColor = ColorConstants.UnderlineColor
    }
    
    @IBAction func btnRentalReport_Click(_ sender: UIButton) {
        let vc = DriveNowVC()
        vc.isRentalExpand = true
        vc.vehicleModel = rideModel.vehicleId!
        vc.rideType = rideModel.rideType
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnRentalLostItem_Click(_ sender: UIButton) {
        
    }
    
    func moveToRating(){
        let vc = RideRatingVC(nibName: "RideRatingVC", bundle: nil)
        vc.activeRideObject = self.rideModel
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        Utilities.dismissToolTip(views: appDelegate.window!.subviews)
        
    }
    
}

//MARK: Tableview methods
extension FareSummaryVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCellData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrCellData[indexPath.row]
        
        switch model.cellType! {
            
        case .FareSummaryCarInfo:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MapCell", for: indexPath) as! MapCell
            cell.setData(model: model)
            return cell
            
        case .FareSummaryTripInfo:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TripInfoCell", for: indexPath) as! TripInfoCell
            cell.setData(model: model)
            return cell
            
        case .FareSummaryCardSelection:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInformationCardCell", for: indexPath) as! MoreInformationCardCell
            cell.setData(model: model)
            cell.btnChange.addTarget(self, action: #selector(btnChange_Click(_:)), for: .touchUpInside)
            cell.btnChange.tag = indexPath.row
            cell.switchChanged = { value in
                self.changeDefaultCards(value, index : indexPath.row)
            }
            return cell
            
        case .PriceBreakUpCell, .FareSummaryAlreadyPaid:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "BreakdownCell", for: indexPath) as! BreakdownCell
            cell.setData(model)
            return cell
            
        case . FareSummaryReleaseAmount :
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReleaseAmountCell", for: indexPath) as! ReleaseAmountCell
            cell.setData(model, isFromYourTrip: isFromYourTrip)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    @objc func btnChange_Click(_ sender: UIButton) {
        
        let vc = PaymentVC()
        vc.isDeleteHidden = true
        //vc.shownCardType = !arrCellData[sender.tag].isSelected ? AppConstants.CardTypes.PersonalCard :  AppConstants.CardTypes.JobCard
        vc.screenType = .ChangeCard
        
        vc.isReload = { isFromAddClick in
            
            let filter = self.arrCellData.filter { $0.cellType == .FareSummaryCardSelection}
            
            if filter.count > 0{
                
                if filter.first!.isSelected == false{
                    filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard()
                }else{
                    filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard)
                }
                
            } else {
                if isFromAddClick {
                    //Card
                    if let card = SyncManager.sharedInstance.getPrimaryCard() {
                        self.arrCellData.append(CellModel.getModel(placeholder: "", text: "", type: .CMICard, cellObj: card))
                    } else if let card = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard) {
                        self.arrCellData.append(CellModel.getModel(placeholder: "", text: "", type: .CMICard, cellObj: card, isSelected : true))
                    } else {
                        self.arrCellData.append(CellModel.getModel(placeholder: "", text: "", type: .CMIAddCard, cellObj: nil))
                    }
                }
            }
            self.tableView.reloadRows(at: [IndexPath(row: self.arrCellData.firstIndex(of: filter.first!) ?? 0, section: 0)], with: .none)
            
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func changeDefaultCards(_ value : Bool, index : Int){
        
        let filter = arrCellData.filter { $0.cellType == .FareSummaryCardSelection}
        
        if filter.count > 0{
            
            if value == false{
                filter.first!.isSelected = false
                filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard()
            }else{
                filter.first!.isSelected = true
                filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard)
            }
        }
        
        self.tableView.reloadData()
    }
}

//MARK: Call api
extension FareSummaryVC{
    
    func callApiForPayment(){
        
        var param = [String:Any]()
        param["rideId"] = rideModel.id ?? ""
        
        let filter = self.arrCellData.filter { $0.cellType == .FareSummaryCardSelection}
        if filter.count > 0{
            param["cardId"] = (filter.first!.cellObj as! CardModel).id ?? ""
        }
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.MakePayment, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let _ = response as? [String:Any]{
                
                self.moveToRating()
                
            }
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func callApiForSendReceipt() {
        
        var param = [String:Any]()
        param["rideId"] = rideModel.id ?? ""
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.SendReceipt, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            Utilities.showAlertView(message: message)
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
