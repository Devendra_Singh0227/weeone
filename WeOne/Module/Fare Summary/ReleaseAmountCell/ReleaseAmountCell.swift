//
//  ReleaseAmountCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 07/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class ReleaseAmountCell: UITableViewCell {

    @IBOutlet weak var viewToBeRelease: UIView!
    @IBOutlet weak var viewDepositeRelease: UIView!

    @IBOutlet weak var imgInfoToBeRelease: UIImageView!
    @IBOutlet weak var imgInfoDepositeRelease: UIImageView!

    @IBOutlet weak var lblCard: UILabel!
    @IBOutlet weak var btnInfoToBeRelease: UIButton!
    @IBOutlet weak var btnDepositeRelease: UIButton!

    @IBOutlet weak var lblAmount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        imgInfoToBeRelease.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Information"))
        imgInfoDepositeRelease.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Information"))

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ model : CellModel, isFromYourTrip: Bool) {
        
        
        if let obj = model.cellObj as? RideListModel {
            lblCard.text = obj.selectedCard?.getFormattedCardNumber()
            
            if isFromYourTrip {
                viewToBeRelease.isHidden = true
                viewDepositeRelease.isHidden = false
                
                print(obj.fareSummary?.deposit ?? 0.0)
                
                let price = "\(obj.fareSummary?.deposit ?? 0.0) \(Utilities.convertToCurrency(number: obj.fareSummary?.depositRelease ?? 0.0, currencyCode: obj.partnerSetting?.currency))"
                
                lblAmount.text = price
                lblAmount.changeStringColor(string: price, array: ["\(obj.fareSummary?.deposit ?? 0.0)"], colorArray: [ColorConstants.TextColorSecondary], strikeThrough: ["\(obj.fareSummary?.deposit ?? 0.0)"])

            } else {
                viewToBeRelease.isHidden = false
                viewDepositeRelease.isHidden = true

                lblAmount.text = Utilities.convertToCurrency(number: obj.fareSummary?.depositRelease ?? 0.0, currencyCode: obj.partnerSetting?.currency)

            }

        }
    }
    
    @IBAction func btnInfoToBeRelease_Click(_ sender: UIButton) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        Utilities.dismissToolTip(views: appDelegate.window!.subviews)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            Utilities.showPopOverView(str: "KMsgDeposite".localized, arrowSize: CGSize(width: 11, height: 11), maxWidth: 191, x: 10, isAttributed: true, sender: self.imgInfoToBeRelease)
        })


    }
    
    @IBAction func btnDepositeRelease_Click(_ sender: UIButton) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        Utilities.dismissToolTip(views: appDelegate.window!.subviews)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            Utilities.showPopOverView(str: "KMsgDeposite".localized, arrowSize: CGSize(width: 11, height: 11), maxWidth: 191, x: 10, isAttributed: true, sender: self.imgInfoDepositeRelease)
        })

    }

}
