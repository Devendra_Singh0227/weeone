//
//  BreakdownCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 07/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class BreakdownCell: UITableViewCell {

    @IBOutlet weak var constrainLblTop: NSLayoutConstraint!
    @IBOutlet weak var constrainLblBottom: NSLayoutConstraint!

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var viewLine: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ model : CellModel){
        lblTitle.text = model.placeholder
        
        lblPrice.text = model.userText

        if Utilities.checkStringEmptyOrNil(str: model.userText1){
            lblDesc.isHidden = true
        }else{
            lblDesc.isHidden = false
            lblDesc.text = model.userText1
        }
        
        viewLine.isHidden = true
        
        constrainLblTop.constant = 8
        constrainLblTop.constant = 8

        if model.isSelected {
            lblPrice.textColor = ColorConstants.ThemeColor
            lblTitle.textColor = ColorConstants.ThemeColor
//            lblTitle.font = FontScheme.kBoldFont(size: 14)
//            lblPrice.font = FontScheme.kBoldFont(size: 14)
            
            constrainLblTop.constant = 16
            constrainLblTop.constant = 16
        } else if model.cellType == .FareSummaryAlreadyPaid {
            lblPrice.textColor = ColorConstants.TextColorPrimary
            lblTitle.textColor = ColorConstants.TextColorPrimary
//            lblTitle.font = FontScheme.kBoldFont(size: 14)
//            lblPrice.font = FontScheme.kBoldFont(size: 14)
            
            viewLine.isHidden = false

        } else {
            lblPrice.textColor = ColorConstants.TextColorPrimary
            lblTitle.textColor = ColorConstants.TextColorPrimary
//            lblTitle.font = FontScheme.kMediumFont(size: 14)
//            lblPrice.font = FontScheme.kMediumFont(size: 14)

        }
    }
}

