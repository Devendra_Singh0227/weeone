//
//  FareSummaryInfoCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 14/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class FareSummaryInfoCell: UITableViewCell {
    
    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    

    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        
        if let obj = model.cellObj as? RideListModel {
            
            lblName.text = obj.vehicleId?.name
            
            let (duration, durationUnit) = obj.getTotalDuration(isShortUnit: false)
            lblTime.text = "\(duration) \(durationUnit)"
            
            let (distance, distanceUnit) = obj.getTotalDistance(isShortUnit: false)
            lblDistance.text = "\(distance) \(distanceUnit)"
            
            lblPrice.text = obj.getTotalFare()
            
            imgView.setImageForURL(url: obj.vehicleId?.getVehicleImage, placeHolder: UIImage(named: "carPlaceholder"))
        }
    }
    
}
