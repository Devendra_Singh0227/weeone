//
//  MapCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 07/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class MapCell: UITableViewCell {

    @IBOutlet weak var viewCarInfo: UIView!
    @IBOutlet weak var imgMap: UIImageView!
    @IBOutlet weak var lblCarInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        
        viewCarInfo.backgroundColor = ColorConstants.UnderlineColor.withAlphaComponent(0.90)
        
        if let obj = model.cellObj as? RideListModel {
            
            if let statLoc = obj.startLocation, let endLoc = obj.endLocation {
                
                let size = CGSize(width: AppConstants.ScreenSize.SCREEN_WIDTH, height: 187)
                let strUrl = Utilities.getGoogleMapImageUrl(size: size, startLocation: statLoc, endLocation: endLoc)
                print(strUrl)
                imgMap.setImageForURL(url: URL(string: strUrl), placeHolder: UIImage(named: "noImage"))
                imgMap.isHidden = false

            }
            else {
                imgMap.isHidden = true
            }
            
            lblCarInfo.text = "\(obj.vehicleId?.model ?? "") | \(obj.vehicleId?.numberPlate ?? "") | \(obj.vehicleId?.getVehicleCategory ?? "") Car"
        }
    }
}
