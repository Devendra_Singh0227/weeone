//
//  TripInfoCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 14/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class TripInfoCell: UITableViewCell {

    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var lblFromTime: UILabel!
    @IBOutlet weak var lblFromLocation: UILabel!
    @IBOutlet weak var lblTotalKm: UILabel!
    @IBOutlet weak var lblTotalHr: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
    @IBOutlet weak var lblEndLocation: UILabel!
    @IBOutlet weak var viewTopLine: UIView!
    @IBOutlet weak var viewDashedBorder: UIView!
    @IBOutlet weak var viewStack: UIStackView!
    
    @IBOutlet weak var constraintHeightViewMain: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightStack: NSLayoutConstraint!
    @IBOutlet weak var constraintTopStack: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomLine: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        viewDashedBorder.addDashedBorderVertical(color: ColorConstants.ThemeColor,isVertical: false)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(model:CellModel){
        
        if let obj = model.cellObj as? RideListModel {
            
            if model.cellType == CellType.CancellationTripInfo {
                
                if let startDate = obj.reservedDateTime, startDate.count > 0 {
                    lblFromDate.text = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: startDate), format: DateUtilities.DateFormates.kRegularDate)
                    
                    lblFromTime.text = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: startDate), format: DateUtilities.DateFormates.k24Time)
                    
                }
                
                if let endDate = obj.reservedEndDateTime, endDate.count > 0 {
                    lblEndDate.text = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: endDate), format: DateUtilities.DateFormates.kRegularDate)
                    
                    lblEndTime.text = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: endDate), format: DateUtilities.DateFormates.k24Time)
                }

                if let endDate = obj.reservedEndDateTime, endDate.count > 0, let startDate = obj.reservedDateTime, startDate.count > 0 {
                    

                    let start = DateUtilities.convertDateFromStringWithFromat(dateStr: startDate, format: DateUtilities.DateFormates.kMainSourceFormat)
                    let end = DateUtilities.convertDateFromStringWithFromat(dateStr: endDate, format: DateUtilities.DateFormates.kMainSourceFormat)

                    let day = Utilities.getDayAndHours(startDate: start, endDate: end, isDay: true)
              
                    let hour = Utilities.getDayAndHours(startDate: start, endDate: end, isDay: false, isFromCancellation: true)

                    if day != "" && hour != "" {
                        lblTotalHr.text = "\(day ?? "") , \(hour ?? "")"
                    } else if day != "" {
                        lblTotalHr.text = "\(day ?? "")"
                    } else if hour != "" {
                        lblTotalHr.text = "\(hour ?? "")"
                    }

                }
                


                lblTotalKm.isHidden = true
                lblTotalHr.textColor = ColorConstants.TextColorSecondary
               // lblTotalHr.font = FontScheme.kSemiBoldFont(size: 14)
//                lblTotalHr.text = "\(obj.vehicleId?.packages.first?.actualDay ?? 0) \("KLblDays".localized) \(obj.vehicleId?.packages.first?.actualHours ?? 0) \("KLblHr".localized)"

                viewStack.isHidden = true
                
                constraintHeightViewMain.constant = 79
                constraintHeightStack.constant = 0
                constraintTopStack.constant = 10

            } else {
                
                if let startDate = obj.startDateTime, startDate.count > 0 {
                    lblFromDate.text = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: startDate), format: DateUtilities.DateFormates.kRegularDate)
                    
                    lblFromTime.text = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: startDate), format: DateUtilities.DateFormates.k24Time)
                    
                }
                
                if let endDate = obj.endDateTime, endDate.count > 0 {
                    lblEndDate.text = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: endDate), format: DateUtilities.DateFormates.kRegularDate)
                    
                    lblEndTime.text = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: endDate), format: DateUtilities.DateFormates.k24Time)
                }

                lblTotalHr.textColor = ColorConstants.TextColorSecondary
                                
                lblTotalKm.text = "\(obj.totalKm) \("KLblKm".localized)"
                lblTotalHr.text = Utilities.getFormattedTime(min: obj.totalTime)

                lblFromLocation.text = obj.startLocation?.street ?? "-"
                lblEndLocation.text = obj.endLocation?.street ?? "-"

                if model.cellType == .IssueWithTripInfo {
                    constraintBottomLine.constant = 0
                }
                
            }
        }
    }

}
