//
//  RideSummary_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 15/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class RideSummary_ViewController: UIViewController {

    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var support_View: ViewDesign!
    @IBOutlet weak var lblVehicleName: UILabel!
    @IBOutlet weak var lblVehicleNumber: UILabel!
    @IBOutlet weak var lblPricePerDay: UILabel!
    @IBOutlet weak var totalAmount_Lbl: UILabel!
    var detailModel : VehicleDetailModel = VehicleDetailModel()
    @IBOutlet weak var detailView: ViewDesign!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        initialConfig()
    }

    func initialConfig() {
//           if AppConstants.hasSafeArea {
//               headerView_HeightConstraint.constant = 90
//           }
//           else {
//               headerView_HeightConstraint.constant = 74
//           }
        lblVehicleName.text = detailModel.vehicle?.name ?? ""
        lblVehicleNumber.text = detailModel.vehicle?.numberPlate
        lblPricePerDay.text = "\(detailModel.vehicle?.packages.first?.rate ?? 0) Nok/Hour"
        totalAmount_Lbl.text = "\(detailModel.vehicle?.deposit ?? 0) Nok"
       }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func i_Button(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            detailView.isHidden = false
        } else {
            sender.tag = 0
             detailView.isHidden = true
        }
    }
    
    @IBAction func support_Btn(_ sender: UIButton) {
        support_View.isHidden = false
    }
    
    @IBAction func support_View_Support_Btn(_ sender: UIButton) {
        support_View.isHidden = true
    }
    
    @IBAction func pay_Btn(_ sender: ButtonDesign) {
        //callApiForSendReceipt()
        moveTo_FeedbackVC()
    }
    
    func moveTo_FeedbackVC() {
        let VC = FeedBack_ViewController()
        navigationController?.pushViewController(VC, animated: true)
    }
    
    func callApiForSendReceipt() {
        
        var param = [String:Any]()
        param["rideId"] = ""//rideModel.id ?? ""
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.SendReceipt, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            self.moveTo_FeedbackVC()
//            Utilities.showAlertView(message: message)
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
