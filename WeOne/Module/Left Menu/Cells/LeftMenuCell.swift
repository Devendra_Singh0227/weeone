//
//  LeftMenuCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 30/09/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class LeftMenuCell: UITableViewCell {

    //MARK: - Variables
    
    //MARK: - Outlets    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgview: UIImageView!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        lblTitle.text = model.placeholder
        imgview.image = UIImage(named: model.imageName ?? "")
//        if model.cellType == .LMFreeRides {
//            imgview.image = UIImage(named: ThemeManager.sharedInstance.getImage(string: "heart-2"))
//        } else {
//            imgview.image = UIImage(named: model.imageName ?? "")
//        }
    }
    
}
