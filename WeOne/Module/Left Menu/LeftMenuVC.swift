//
//  LeftMenuVC.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved

import UIKit

class LeftMenuVC: UIViewController, SideMenuNavigationControllerDelegate {
    
    //MARK: - Variables
    var arrData = [CellModel]()
    //MARK: - Outlets
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var vwLeftMenu: UIView!
    @IBOutlet var viewHeader: UIView!
    @IBOutlet var viewGuest: UIView!
    @IBOutlet var viewUser: UIView!
    @IBOutlet var viewShareYourCar: UIView!
    @IBOutlet weak var btnDismiss: UIButton!
    @IBOutlet weak var btnShareYourCar: UIButton!
    
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        intialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         setUserData()
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "", isStausColor:false)
    }
    
    func intialConfig() {
        SideMenuManager.default.leftMenuNavigationController?.sideMenuDelegate = self
        table_view.register(UINib(nibName: "LeftMenuCell", bundle: nil), forCellReuseIdentifier: "LeftMenuCell")
        
       
        prepareDataSource()
        
//        btnShareYourCar.layer.cornerRadius = btnShareYourCar.frame.height / 2
//        btnShareYourCar.addTarget(self, action: #selector(btnShareYourCar_Click), for: .touchUpInside)
//        btnShareYourCar.addTarget(self, action: #selector(holdDown(_:)), for: .touchDown)
        
        table_view.tableHeaderView = viewHeader
        table_view.sectionHeaderHeight = 139
        
//        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationReceived(notification:)), name: Notification.Name(AppConstants.Notification.resetMenu), object: nil)
    }
    
    //Set User Data
    func setUserData() {
        
        lblUserName.text = ApplicationData.user?.fullName ?? ""//"\(Defaults[.FirstName] ?? "") \(Defaults[.LastName] ?? "")"
        if let url = URL(string: AppConstants.serverURL + "/\(appDelegate.userDetails?.profilePic ?? "")")  {
            imgProfile.setImageForURL(url: url, placeHolder: UIImage(named: "user_placeholder"))
            imgProfile.contentMode = .scaleAspectFill
        }
        
        //        if ApplicationData.isUserLoggedIn {
        //
        //            let user = ApplicationData.user
        //
        //            viewUser.isHidden = false
        //            viewShareYourCar.isHidden = false
        //
        //            viewGuest.isHidden = true
        //
        //            lblUserName.text = user?.firstName
        //            if let url = URL(string: AppConstants.serverURL + "/\(user?.image ?? "")")  {
        //                imgProfile.setImageForURL(url: url, placeHolder: UIImage(named: "user_placeholder"))
        //                imgProfile.contentMode = .scaleAspectFill
        //            } else {
        //                imgProfile.image = UIImage(named: "user_placeholder")
        //            }
        //        }
        //        else{
        ////            viewUser.isHidden = true
        //           // viewShareYourCar.isHidden = true
        //
        //           // viewGuest.isHidden = false
        //            imgProfile.image = UIImage(named: "user_placeholder")
        //        }
        
    }
    
    @objc func notificationReceived(notification: Notification) {
        
        dismiss(animated: true, completion: nil)
        //KD self.sideMenuViewController.hideViewController()
        prepareDataSource()
    }
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appearing! (animated: \(animated))")
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appeared! (animated: \(animated))")
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappearing! (animated: \(animated))")
    }
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappeared! (animated: \(animated))")
    }
    
    func prepareDataSource() {
        
        //        setUserData()
        
        UIView.animate(withDuration: 0.2) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
            self.view.layoutIfNeeded()
        }
        arrData.append(CellModel.getModel(placeholder: "KLblLMFreeRides".localized, type: .LMFreeRides, imageName: "heart-2"))
        arrData.append(CellModel.getModel(placeholder: "KLblLMPayment".localized, type: .LMPayment, imageName:"Payment"))
        arrData.append(CellModel.getModel(placeholder: "KLblNotification".localized, type: .LMNotification, imageName:"notification"))
        arrData.append(CellModel.getModel(placeholder: "KLblLMYourTrips".localized, type: .LMYourTrips, imageName:"YourTrips"))
        arrData.append(CellModel.getModel(placeholder: "KLblLMPromos".localized, type: .LMPromos, imageName:"Promo"))
        arrData.append(CellModel.getModel(placeholder: "KLblLMSupport".localized, type: .LMSupport, imageName:"Support-1"))
        arrData.append(CellModel.getModel(placeholder: "KLblLMAbout".localized, type: .LMAbout, imageName:"Group 4524"))
        
        table_view.reloadData()
    }
    
    @IBAction func btnLogout_Click(_ sender: UIButton) {
        
        if ApplicationData.isUserLoggedIn {
            
            UIViewController.current().view.showConfirmationPopupWithMultiButton(title: "", message: "KLblLogoutMsg".localized, cancelButtonTitle: StringConstants.ButtonTitles.kNo, confirmButtonTitle: StringConstants.ButtonTitles.kYes, onConfirmClick: {
                
                ApplicationData.sharedInstance.logoutUser()
                
            }) {
                
            }
        }
        else{
            
            let vc = LoginVC()
            ThemeManager.sharedInstance.RideType = ThemeManager.DefaultTheme
            vc.isFromMenu = true
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
            dismiss(animated: true, completion: nil)
            
            //KD self.sideMenuViewController.hideViewController()
        }
    }
    
    @IBAction func btnSignUp_Click(_ sender: UIButton) {
        ThemeManager.sharedInstance.RideType = AppConstants.RideType.Schedule

        AppNavigation.shared.currentStateList = AppNavigation.shared.config.accessConfig.requiredToUseApp
        let vc = SignUpVC()
        vc.isFromMain = true
        UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnProfile_Click(_ sender: UIButton) {
        
//        if !ApplicationData.isUserLoggedIn{
//            AppNavigation.shared.currentStateList = AppNavigation.shared.config.accessConfig.requiredToUseApp
//            let vc = LoginVC()
//            ThemeManager.sharedInstance.RideType = ThemeManager.DefaultTheme
//            vc.isFromMenu = true
//            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
//            dismiss(animated: true, completion: nil)
            
            //KD self.sideMenuViewController.hideViewController()
//        }else{
            let vc = ProfileVC(nibName: "ProfileVC", bundle: nil)
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
            dismiss(animated: true, completion: nil)
            
//            let vc = EditProfileVC(nibName: "EditProfileVC", bundle: nil)
//            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
//            dismiss(animated: true, completion: nil)
                        
//        }
    }
    
    
    //MARK:- IBActions
    @IBAction func btnDismiss_Click(_ sender: UIButton) {
        self.view.backgroundColor = UIColor.clear
        dismiss(animated: true, completion: nil)
    }
    
    @objc func btnShareYourCar_Click(_ sender: UIButton) {
        //btnShareYourCar.backgroundColor = ColorConstants.UnderlineColor.withAlphaComponent(1)
        
        self.showToast(text: "KLblLMComingSoon".localized)

    }
    
    @objc func holdDown(_ sender: UIButton) {
//        btnShareYourCar.backgroundColor = ColorConstants.UnderlineColor.withAlphaComponent(0.50)
    }
    
}

//MARK: TableView Delegates & DataSource
extension LeftMenuVC: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = arrData[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell") as? LeftMenuCell
        cell?.setData(model: model)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = arrData[indexPath.row]
        
        switch model.cellType! {
            
        case .LMFreeRides:
            
            dismiss(animated: true, completion: nil)
            let vc = FreeRidesVC()
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
            
            break
            
            
        case .LMPayment:
            
            dismiss(animated: true, completion: nil)
            let vc = AddPayment_ViewController()
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
                
            break
            
        case .LMNotification :
            
            dismiss(animated: true, completion: nil)
            let vc = NotificationVC()
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
            
            break
            
        case .LMYourTrips:
            
            dismiss(animated: true, completion: nil)
            let vc = TripHistoryContainerVC()
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
            
            break
            
        case .LMSupport:
            
            dismiss(animated: true, completion: nil)
            let vc = SupportVC()
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
            
            break
            
        case .LMPromos:
            
            dismiss(animated: true, completion: nil)
            let vc = PromoCodeVC()
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
            
            break
            
        case .LMAbout:
            
            dismiss(animated: true, completion: nil)
            let vc = AboutWeoneViewController()
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
            
            break
            
//        case .LMFaq:
//
//            let vc = FaqVC()
//            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
//            break
            //
            //
            //        case .LMChat :
            //
            //            let vc = ContactUsVC()
            //            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
            //            break
            //
            //        case .LMContactUs :
            //
            //            let vc = ContactUsVC()
            //            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
            //            break
            //
            //        case .LMHelp:
            //
            //            let vc = SupportVC()
            //            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
            //            break
            
        default:
            print("")
        }
        
        dismiss(animated: true, completion: nil)
        
    }
}
