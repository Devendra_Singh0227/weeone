//
//  CompanyProfileVC.swift
//  WeOne
//
//  Created by iMac on 11/06/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class CompanyDetailsVC: UIViewController {
    
    //MARK: - Variables
    var arrData = [CellModel]()
        
    //MARK: - Outlets
    @IBOutlet weak var viewNavigation: UIViewCommon!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnSave: UIButtonCommon!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
        
    }
    
    //MARK: Private Methods
    func initialConfig() {
                
        viewNavigation.text = "KLblCompanyDetails".localized as NSString
        
        tblView.register(UINib(nibName: "SignupTextFieldCell", bundle: nil), forCellReuseIdentifier: "SignupTextFieldCell")
        
        preapareDataSource()
        
        viewNavigation.click = {
            self.btnClose_Click()
        }
        
        btnSave.click = {
            self.btnSave_Clicked()
        }
        
    }
    
    func preapareDataSource() {
        arrData.removeAll()
        
        arrData.append(CellModel.getModel(placeholder: "KLblCompanyName".localized, text: "", type: .CompanyName, imageName: "userName",  keyBoardType: UIKeyboardType.default, textFieldType:0, errorMessage : "KLblEnterCompanyName".localized))
        
        arrData.append(CellModel.getModel(placeholder: "KLblRegNumber".localized, text: "", type: .CompanyRegNo, imageName: "userName",  keyBoardType: UIKeyboardType.default, textFieldType:0, errorMessage : "KLblEnterRegNo".localized))
        
        arrData.append(CellModel.getModel(placeholder: "KLblAddress".localized, text: "", type: .CompanyAddress, imageName: "email",  keyBoardType: UIKeyboardType.emailAddress, textFieldType:1, errorMessage : "KLblEnterAddress".localized))
        
        tblView.reloadData()
    }
    //MARK:- IBActions
    func btnClose_Click() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func btnSave_Clicked() {
        let (validate, req) = getrequestAndValidate()
        if validate {
            
        }
    }
    
    func getrequestAndValidate() -> (Bool, [String:Any]) {
        var req = [String:Any]()
        
        for model in arrData {
            
            guard !Utilities.checkStringEmptyOrNil(str: model.userText) else {
                self.showToast(text: model.errorMessage ?? "")

//                Utilities.showAlertView(message: model.errorMessage)
                return (false,[:])
            }
            
             if model.cellType == .CompanyName {
                
                
//                req["countryCode"] = model.userText1
            }
            else if model.cellType == .CompanyRegNo {
                //                req["countryCode"] = model.userText1

                
            } else if model.cellType == .CompanyAddress {
                //                req["countryCode"] = model.userText1

                
            }
        }
        
        return (true, req)
    }
    
}
//MARK: - UITableView's DataSource & Delegate Methods
extension CompanyDetailsVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 82
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SignupTextFieldCell") as? SignupTextFieldCell
        cell?.setData(model: arrData[indexPath.row], isSignUp: false)
        
        return cell!
    }
    
}
