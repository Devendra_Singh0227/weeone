//
//  AddCar_Pics_CollectionViewCell.swift
//  WeOne
//
//  Created by Dev's Mac on 20/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class AddCar_Pics_CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var plus_Lbl: UILabel!
    @IBOutlet weak var cars_ImageView: UIImageView!
    @IBOutlet weak var border_View: ViewDesign!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
