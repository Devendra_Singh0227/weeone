//
//  Report_Crash_DetailViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 21/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Report_Crash_DetailViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var support_View: ViewDesign!
    @IBOutlet weak var Cars_CollectioView: UICollectionView!
    @IBOutlet weak var documents_CollectionView: UICollectionView!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var yesBtn: ButtonDesign!
    @IBOutlet weak var noBtn: ButtonDesign!
    var imageArray = [UIImage]()
    var documentsArray = [UIImage]()
    var type = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
        Cars_CollectioView.register(UINib(nibName: "AddCar_Pics_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AddCar_Pics_CollectionViewCell")
        documents_CollectionView.register(UINib(nibName: "AddCar_Pics_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AddCar_Pics_CollectionViewCell")
        // Do any additional setup after loading the view.
    }
    
    func initialConfig() {
        
//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func support_Btn(_ sender: UIButton) {
        support_View.isHidden = false
    }
    
    @IBAction func yes_Btn(_ sender: ButtonDesign) {
        sender.borderwidth = 2
        noBtn.borderwidth = 0
        sender.setTitleColor(ColorConstants.TextColorPrimary, for: .normal)
        noBtn.setTitleColor(ColorConstants.textcolor, for: .normal)
    }
    
    @IBAction func no_Btn(_ sender: ButtonDesign) {
        sender.borderwidth = 2
        yesBtn.borderwidth = 0
        sender.setTitleColor(ColorConstants.TextColorPrimary, for: .normal)
        yesBtn.setTitleColor(ColorConstants.textcolor, for: .normal)
    }
    
    @IBAction func supportView_SupportBtn(_ sender: UIButton) {
        support_View.isHidden = true
    }
    
    func showPickedImage(_ info: [UIImagePickerController.InfoKey : Any]) {
           addImages(info)
       }
       
       func addImages(_ info: [UIImagePickerController.InfoKey : Any]) {
           if let image = info[.originalImage] as? UIImage{
            if type == "cars" {
                imageArray.append(image)
                Cars_CollectioView.reloadData()
            } else {
                documentsArray.append(image)
                documents_CollectionView.reloadData()
            }
           }
       }
       
    func openCustomCamera() {
        if ( UIImagePickerController.isSourceTypeAvailable(.camera)) {
            let vc = CustomeCameraVC.init(nibName: "CustomeCameraVC", bundle: nil)
            vc.isFromProfile = true
            vc.isImagePicked = { dictInfo in
                self.showPickedImage(dictInfo)
            }
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let actionController: UIAlertController = UIAlertController(title: "KMsgInvalidCamera".localized, message: "", preferredStyle: .alert)
            let cancelAction: UIAlertAction = UIAlertAction(title: "KLblOk".localized, style: .cancel) { action -> Void     in
                //Just dismiss the action sheet
            }
            
            actionController.addAction(cancelAction)
            actionController.popoverPresentationController?.sourceView = UIViewController.current().view
            UIViewController.current().present(actionController, animated: true, completion: nil)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == Cars_CollectioView {
            return imageArray.count + 1
        } else {
            return documentsArray.count + 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == Cars_CollectioView {
            let cell = Cars_CollectioView.dequeueReusableCell(withReuseIdentifier: "AddCar_Pics_CollectionViewCell", for: indexPath) as! AddCar_Pics_CollectionViewCell
            cell.border_View.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 252/255, alpha: 1.0)
            //cell.cars_ImageView.image = imageArray[indexPath.row]
            if indexPath.row > imageArray.count - 1 {
                cell.plus_Lbl.text = "+"
                cell.plus_Lbl.font = UIFont(name: "System", size: 30)
            }
            return cell
        } else {
            let cell = documents_CollectionView.dequeueReusableCell(withReuseIdentifier: "AddCar_Pics_CollectionViewCell", for: indexPath) as! AddCar_Pics_CollectionViewCell
             cell.border_View.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 252/255, alpha: 1.0)
            //cell.cars_ImageView.image = documentsArray[indexPath.row]
            if indexPath.row > documentsArray.count - 1 {
                cell.plus_Lbl.text = "+"
                cell.plus_Lbl.font = UIFont(name: "System", size: 30)
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == Cars_CollectioView {
            type = "cars"
        } else {
            type = "docs"
        }
        openCustomCamera()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70, height: 70)
    }
    
    
}
