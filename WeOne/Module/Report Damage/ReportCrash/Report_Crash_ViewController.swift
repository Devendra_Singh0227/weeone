//
//  Report_Crash_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 21/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Report_Crash_ViewController: UIViewController {

    @IBOutlet weak var support_View: ViewDesign!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       initialConfig()
    }

    func initialConfig() {
        
//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func support_Btn(_ sender: UIButton) {
        support_View.isHidden = false
    }
    
    @IBAction func support_View_Support_Btn(_ sender: UIButton) {
        support_View.isHidden = true
    }
    
    @IBAction func get_Started_Btn(_ sender: ButtonDesign) {
        let vc = Report_Crash_DetailViewController()
        navigationController?.pushViewController(vc, animated: true)
    }

//    func callAPIForSubmitDamage() {
//
//        var req = [String:Any]()
//        req["vehicleId"] = rideModel.vehicleId?.id ?? ""
//        req["partnerId"] = rideModel.vehicleId?.partnerId ?? ""
//
//        if rideModel.id != nil {
//            req["rideId"] = rideModel.id ?? ""
//        }
//
//
//        if true {
//            let filter = arrCell.filter {$0.cellType == .ReportDamageAddPictureCell}
//
//            if filter.count > 0 {
//                if filter.first!.dataArr.count < 2 {
//                    //   Utilities.showAlertView(message: "KLblPleaseUploadAllImageMsg".localized)
//                    self.showToast(text: "KLblPleaseUploadAllImageMsg".localized)
//
//                    return
//                }
//
//
//                var dictAttachment : [[String:Any]] = [[:]]
//                dictAttachment.removeAll()
//
//                for cellModel  in filter.first!.dataArr {
//                    if let model = cellModel as? CellModel {
//                        let dict = ["path": model.userText ?? "", "type": model.imageType] as [String : Any]
//                        dictAttachment.append(dict)
//                    }
//                }
//
//                req["images"] = dictAttachment
//
//            }
//        }
//
//        var arrDamages = [Int]()
//
//        if true {
//
//            let filter = arrCell.filter { $0.cellType == .ReportDamageIssueCell1 ||  $0.cellType == .ReportDamageIssueCell2}
//
//            if filter.count > 0 {
//
//                for item in filter {
//                    if item.isSelected {
//                        arrDamages.append(Int(item.userText ?? "0") ?? 0)
//                    }
//
//                    if item.isSelected2 {
//                        arrDamages.append(Int(item.userText1 ?? "0") ?? 0)
//                    }
//                }
//            }
//        }
//
//        if arrDamages.count == 0 {
//            self.showToast(text: "KLblSelectSingleDamage".localized)
//            //            Utilities.showAlertView(message: "KLblSelectSingleDamage".localized)
//            return
//        }
//
//        req["damageCategory"] = arrDamages
//
//        if true {
//
//            let filter = arrCell.filter { $0.cellType == .ReportDamageTextViewCell }
//
//            if Utilities.checkStringEmptyOrNil(str: filter.first?.userText) == false {
//                req["description"] = filter.first?.userText
//            }
//        }
//
//        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
//        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.AddDamageReoport, method: .post, parameters: req, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
//
//            Utilities.showAlertWithButtonAction(title: "", message: message, buttonTitle: StringConstants.ButtonTitles.KOk, onOKClick: {
//
//                self.navigationController?.popViewController(animated: true)
//            })
//        }) { (failureMessage, failureCode) in
//            Utilities.showAlertView(message: failureMessage)
//        }
//    }
}
