//
//  AddRentalReport_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 15/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class AddRentalReport_ViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var support_View: ViewDesign!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scroll_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var comment_TxtView: IQTextView!
    @IBOutlet weak var textView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var count_Lbl: UILabel!
    @IBOutlet weak var lblVehicleName: UILabel!
    @IBOutlet weak var lblVehicleNumber: UILabel!
    var detailModel : VehicleDetailModel = VehicleDetailModel()
    @IBOutlet weak var first_Image: UIImageView!
    @IBOutlet weak var second_Image: UIImageView!
    @IBOutlet weak var thied_Image: UIImageView!
    @IBOutlet weak var fourth_Image: UIImageView!
    var side = ""
    @IBOutlet weak var odometerView: UIView!
    @IBOutlet weak var odometer_Height: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        comment_TxtView.delegate = self
        checkSreenSize()
    }
    
    func checkSreenSize() {
        
//        lblVehicleName.text = self.detailModel.vehicle?.name ?? ""
//        lblVehicleNumber.text = self.detailModel.vehicle?.numberPlate
//        if self.detailModel.vehicle?.iotProvider == AppConstants.IOTProvider.WithTelemetric {
//            odometerView.isHidden = true
//            odometer_Height.constant = 4
//        } else {
//
//        }
//        if AppConstants.DeviceType.IS_IPHONE_6 {
//            scroll_HeightConstraint.constant = 610
//        } else if AppConstants.DeviceType.IS_IPHONE_6P {
//            scroll_HeightConstraint.constant = 632
//        } else if AppConstants.DeviceType.IS_IPHONE_X { //11pro
//            scroll_HeightConstraint.constant = 710
//        } else if AppConstants.DeviceType.IS_IPHONE_XR { //11
//            scroll_HeightConstraint.constant = 800
//        } else if AppConstants.DeviceType.IS_IPHONE_XS_MAX {
//            scroll_HeightConstraint.constant = 810
//        }
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func support_Btn(_ sender: UIButton) {
        support_View.isHidden = false
    }
    
    @IBAction func support_View_Support_Btn(_ sender: UIButton) {
        support_View.isHidden = true
    }
    
    @IBAction func submit_Btn(_ sender: ButtonDesign) {
//        if self.detailModel.vehicle?.iotProvider == AppConstants.IOTProvider.WithTelemetric {
            let VC = RideSummary_ViewController()
            VC.detailModel = self.detailModel
            navigationController?.pushViewController(VC, animated: true)
//        }  else {
//            UserDefaults.standard.set("AddRentalVC", forKey: "CheckScreen")
//            UserDefaults.standard.synchronize()
//            navigationController?.pop_To_ViewController(ofClass: TelematicsKeyPickUpVC.self)
//        }
    }
    
    @IBAction func damage_Report_Btn(_ sender: UIButton) {
        let VC = ReportDamageVC()
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func report_History_Btn(_ sender: UIButton) {
        let VC = Report_History_ViewController()
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func frontSide_Btn(_ sender: UIButton) {
        side = "front"
        openCustomCamera()
    }
    
    @IBAction func leftSide_Btn(_ sender: UIButton) {
         side = "left"
        openCustomCamera()
    }
    
    @IBAction func rightSide_Btn(_ sender: UIButton) {
         side = "right"
        openCustomCamera()
    }
    
    @IBAction func backSide_Btn(_ sender: UIButton) {
         side = "back"
        openCustomCamera()
    }
    
    @IBAction func odometer_Btn(_ sender: UIButton) {
        openCustomCamera()
    }
    
    func textViewDidChange(_ textView: UITextView) {
       let height = ceil(textView.contentSize.height)

        if height != textView_HeightConstraint.constant {
            textView_HeightConstraint.constant = height
            textView.setContentOffset(CGPoint.zero, animated: false)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        if numberOfChars > 255 {
            
        } else {
            self.count_Lbl.text =  "\(textView.text.count)/255"
        }
        return numberOfChars < 256
    }
    
    func showPickedImage(_ info: [UIImagePickerController.InfoKey : Any]) {
        addImages(info)
    }
    
    func addImages(_ info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage{
            if side == "front" {
                first_Image.image = image
            } else if side == "left" {
                second_Image.image = image
            } else if side == "right" {
                thied_Image.image = image
            } else {
                fourth_Image.image = image
            }
        }
    }
    
    func openCustomCamera() {
        if ( UIImagePickerController.isSourceTypeAvailable(.camera)) {
            let vc = CustomeCameraVC.init(nibName: "CustomeCameraVC", bundle: nil)
            //                vc.isFromProfile = true
            vc.isImagePicked = { dictInfo in
                    self.showPickedImage(dictInfo)
            }
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let actionController: UIAlertController = UIAlertController(title: "KMsgInvalidCamera".localized, message: "", preferredStyle: .alert)
            let cancelAction: UIAlertAction = UIAlertAction(title: "KLblOk".localized, style: .cancel) { action -> Void     in
                //Just dismiss the action sheet
            }
            
            actionController.addAction(cancelAction)
            actionController.popoverPresentationController?.sourceView = UIViewController.current().view
            UIViewController.current().present(actionController, animated: true, completion: nil)
            
        }
    }
}
