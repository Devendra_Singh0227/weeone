//
//  ReportDamageListCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 11/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class ReportDamageListCell: UITableViewCell {

    var arrImages : [ImagesModel] = [ImagesModel]()
    var arrDamageCategory : [Int] = [Int]()
    
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var collectViewReason: UICollectionView!

    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var collViewReasonHeight: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        collView.delegate = self
        collView.dataSource = self
                
        collView.register(UINib(nibName: "CollectionImagesCell", bundle: nil), forCellWithReuseIdentifier: "CollectionImagesCell")
        collectViewReason.register(UINib(nibName: "DamageReportReasonCell", bundle: nil), forCellWithReuseIdentifier: "DamageReportReasonCell")

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- setData
    func setData(model: DamageReportModel) {
        arrImages = model.images
        arrDamageCategory = model.damageCategory
        lblComment.text = model.desc
        
        if arrDamageCategory.count > 3 {
            
            collViewReasonHeight.constant = 34 * 2
        }
        else {
            collViewReasonHeight.constant = 34
        }
                
        collView.reloadData()
        collectViewReason.reloadData()
    }
    
}

//MARK:- UICollectionView's Data Source & Delegate Methods
extension ReportDamageListCell : UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collView {
            return arrImages.count
        } else {
            return arrDamageCategory.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collView {
            let model =  arrImages[indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionImagesCell", for: indexPath) as? CollectionImagesCell
            cell?.setData(images: model)
            return cell!
        } else {
            let model = arrDamageCategory[indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DamageReportReasonCell", for: indexPath) as? DamageReportReasonCell
            cell?.setData(categoryId:model)
            return cell!
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collView {
            return CGSize(width: 82, height: collectionView.bounds.height)
        } else {
            return CGSize(width: collectionView.bounds.width / 2, height: 34)
        }
    }
}

