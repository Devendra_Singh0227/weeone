//
//  ReportDamageListVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 27/01/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class ReportDamageListVC: UIViewController {

    var vehicleModel = VehicleModel()
    var arrDamageReport = [DamageReportModel]()
    var cellModel = CellModel()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewNoData: UIView!
//    @IBOutlet weak var viewNoDataHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var lblCarInfo: UILabel!
    
    //MARK: view life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
    }
    
    //MARK: initial config
    func initialConfig(){
        
        cellModel.dataArr = vehicleModel.images 
        cellModel.cellType = .ReportDamageListVC
        cellModel.placeholder = vehicleModel.fuelLevelStr ?? ""
        cellModel.placeholder2 = "\(Int(vehicleModel.totalKm )) \("KLblKm".localized)"


        tableView.register(UINib(nibName: "ReportDamageListCell", bundle: nil), forCellReuseIdentifier: "ReportDamageListCell")
        tableView.register(UINib(nibName: "SliderCell", bundle: nil), forCellReuseIdentifier: "SliderCell")

        callAPIForDamageReport()
    }
    
    //MARK: Prepare data source
    func preparaDataSource() {
                
//        let brandName = SyncManager.sharedInstance.fetchNameFromBrand((vehicleModel.brand ?? ""))
//
//        if brandName != nil{
//            lblCarInfo.text = "\(brandName ?? "") - \(vehicleModel.model ?? "") | \(vehicleModel.numberPlate ?? "")"
//        } else {
        lblCarInfo.text = "\(vehicleModel.getCarName) | \(vehicleModel.numberPlate ?? "")"
//        }
        
//        lblCarInfo.changeStringColor(string: lblCarInfo.text ?? "", array: [(vehicleModel.numberPlate ?? "")], colorArray: [ColorConstants.TextColorWhitePrimary],changeFontOfString: [(vehicleModel.numberPlate ?? "")], font : [FontScheme.kMediumFont(size: 20)])


        if arrDamageReport.count == 0 {
//            tableView.tableFooterView = viewNoData
            self.adjustFooterViewHeightToFillTableView()
            /*tableView.loadNoDataFoundView(message: "KMsgNoDamageReportFound".localized) {
                // refresh
            }*/
        }
        else {
            tableView.removeLoadAPIFailedView()
            tableView.reloadData()

        }
        
    }
    
    func adjustFooterViewHeightToFillTableView() {
        // Invoke from UITableViewController.viewDidLayoutSubviews()

        guard let tableFooterView = self.viewNoData else { return }

        let minHeight = tableFooterView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        let currentFooterHeight = tableFooterView.frame.height
        let fitHeight = tableView.frame.height - tableView.adjustedContentInset.top - tableView.contentSize.height + currentFooterHeight
        let nextHeight = (fitHeight > minHeight) ? fitHeight : minHeight

        // No height change needed ?
        guard round(nextHeight) != round(currentFooterHeight) else { return }

        var frame = tableFooterView.frame
        frame.size.height = nextHeight
        tableFooterView.frame = frame
        tableView.tableFooterView = tableFooterView
    }

    
    //MARK: button click methods
    @IBAction func btnBack_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - UITableView's DataSource & Delegate Methods
extension ReportDamageListVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SliderCell") as! SliderCell
        cell.setData(model: cellModel)
        cell.lblTitle.text = "KLblDamageCategory".localized
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 224
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDamageReport.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrDamageReport[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportDamageListCell") as! ReportDamageListCell
        cell.setData(model: model)
        return cell
    }
}

//MARK: - API Calling
extension ReportDamageListVC {
    
    func callAPIForDamageReport() {
        
        var req = [String:Any]()
        req["vehicleId"] = vehicleModel.id ?? ""
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.DamageReportList, method: .post, parameters: req, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            //save Data
            if let list = response as? [[String:Any]] {
                
                self.arrDamageReport = Mapper<DamageReportModel>().mapArray(JSONArray: list)
            }
            self.preparaDataSource()
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(title: AppConstants.AppName, message: failureMessage)
        }
    }
}
