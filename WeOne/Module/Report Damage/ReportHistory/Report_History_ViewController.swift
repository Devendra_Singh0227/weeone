//
//  Report_History_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 20/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Report_History_ViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, TagListViewDelegate {
    
    @IBOutlet weak var tags_View: TagListView!
    @IBOutlet weak var Previous_Rental_CollectionView: UICollectionView!
    @IBOutlet weak var prevoius_Dental_CollectionView: UICollectionView!
    var Feddback_Array = ["Dirty Interior", "Refill Windscreen"]
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    var vehicleModel = VehicleModel()
    var arrDamageReport = [DamageReportModel]()
    
    @IBOutlet weak var support_View: ViewDesign!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
        Previous_Rental_CollectionView.register(UINib(nibName: "Previous_Rental_Reports_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Previous_Rental_Reports_CollectionViewCell")
        prevoius_Dental_CollectionView.register(UINib(nibName: "Previous_Rental_Reports_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Previous_Rental_Reports_CollectionViewCell")
        tags_View.addTags(Feddback_Array)
        tags_View.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func initialConfig() {
           
//           if AppConstants.hasSafeArea {
//               headerView_HeightConstraint.constant = 90
//           }
//           else {
//               headerView_HeightConstraint.constant = 74
//           }
        callAPIForDamageReport()
       }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func support_Btn(_ sender: UIButton) {
        support_View.isHidden = false
    }
    
    @IBAction func supportView_Support_Btn(_ sender: UIButton) {
        support_View.isHidden = true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4//arrCollData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == Previous_Rental_CollectionView {
            let cell = Previous_Rental_CollectionView.dequeueReusableCell(withReuseIdentifier: "Previous_Rental_Reports_CollectionViewCell", for: indexPath) as! Previous_Rental_Reports_CollectionViewCell
            cell.showCar_Btn.addTarget(self, action: #selector(viewDetail), for: .touchUpInside)
            return cell
        } else {
            let cell = prevoius_Dental_CollectionView.dequeueReusableCell(withReuseIdentifier: "Previous_Rental_Reports_CollectionViewCell", for: indexPath) as! Previous_Rental_Reports_CollectionViewCell
            cell.showCar_Btn.addTarget(self, action: #selector(viewDetail), for: .touchUpInside)
            return cell
        }
        
        //let model = arrCollData[indexPath.row]
    }
    
    @objc func viewDetail() {
        let vc = Report_History_CarsViewController()
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        
        if UIViewController.current()?.presentedViewController != nil {
            UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
            })
        }
        else{
            UIViewController.current()?.present(vc, animated: true, completion: {
            })
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 0
        layout.invalidateLayout()
        return CGSize(width: self.prevoius_Dental_CollectionView.frame.width , height: 150)
    }
    
    func callAPIForDamageReport() {
        
        var req = [String:Any]()
        req["vehicleId"] = Defaults[.vehicleId] ?? ""
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.DamageReportList, method: .post, parameters: req, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            print(response)
            //save Data
            if let list = response as? [[String:Any]] {
                
                self.arrDamageReport = Mapper<DamageReportModel>().mapArray(JSONArray: list)
            }
//            self.preparaDataSource()
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(title: AppConstants.AppName, message: failureMessage)
        }
    }
}
