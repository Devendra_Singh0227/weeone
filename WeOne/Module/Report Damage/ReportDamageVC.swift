//
//  ReportDamageVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 23/01/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class ReportDamageVC: UIViewController, TagListViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout, UITextViewDelegate {
    
    //MARK: variables
    var arrCell : [CellModel] = [CellModel]()
    var rideModel : RideListModel = RideListModel()
    
    //MARK: outlets
    @IBOutlet weak var viewNavigation: UIViewCommon!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnSubmit: UIButtonCommon!
    @IBOutlet var viewFooter: UIView!
    @IBOutlet weak var viewInfo: UIView!
    @IBOutlet weak var lblCarInfo: UILabel!
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet var viewHeader: UIView!
    @IBOutlet weak var btnCancel: UIButtonCommon!
    @IBOutlet weak var tag_View: TagListView!
    @IBOutlet weak var collection_View: UICollectionView!
    @IBOutlet weak var scroll_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var comment_TxtView: IQTextView!
    @IBOutlet weak var textView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var count_Lbl: UILabel!
    @IBOutlet weak var support_View: ViewDesign!
    var tags_array = ["Dirty Interior", "Refill windscreen washer fluid", "Dirty exterior", "Damage"]
    var imageArray = [UIImage]()
    
    //MARK: view life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkSreenSize()
        comment_TxtView.delegate = self
        collection_View.register(UINib(nibName: "AddCar_Pics_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "AddCar_Pics_CollectionViewCell")
        tag_View.addTags(tags_array)
        // initialConfig()
    }
    
    //MARK: initial congif
    func initialConfig(){
        
        //        viewNavigation.button.setImage(#imageLiteral(resourceName: "close"), for: .normal)
        //
        //        viewNavigation.text = "KLblDamageCategory".localized as NSString
        //
        //        viewNavigation.click = {
        //            self.navigationController?.popViewController(animated: true)
        //        }
        //
        //        viewInfo.backgroundColor = ColorConstants.UnderlineColor.withAlphaComponent(0.90)
        
        //        tableView.register(UINib(nibName: "AddPictureCell", bundle: nil), forCellReuseIdentifier: "AddPictureCell")
        //        tableView.register(UINib(nibName: "ReportDamageIssueCell", bundle: nil), forCellReuseIdentifier: "ReportDamageIssueCell")
        //        tableView.register(UINib(nibName: "UnlockCarCell", bundle: nil), forCellReuseIdentifier: "UnlockCarCell")
        
        //        tableView.tableFooterView = viewFooter
        //        tableView.tableHeaderView = viewHeader
        //        btnSubmit_Click()
        //        prepareDataSource()
        
        //        btnCancel.click = {
        //            self.navigationController?.popViewController(animated: true)
        //        }
        
        //        let brandName = SyncManager.sharedInstance.fetchNameFromBrand((rideModel.vehicleId?.brand ?? ""))
        //
        //        if brandName != nil{
        //            lblCarInfo.text = "\(brandName ?? "") - \(rideModel.vehicleId?.model ?? "") | \(rideModel.vehicleId?.numberPlate ?? "")"
        //        } else {
        //            lblCarInfo.text = "\(rideModel.vehicleId?.getCarName ?? "") | \(rideModel.vehicleId?.numberPlate ?? "")"
        //        }
        
        //        imgCar.setImageForURL(url: rideModel.vehicleId?.getVehicleImage, placeHolder: UIImage(named : "carPlaceholder"))
        //
        //        btnCancel.setBackGroundColor(color: ColorConstants.TextColorSecondary)
        //        btnCancel.isCancel = true
        
    }
    
    func checkSreenSize() {
            
//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
        
//        if AppConstants.DeviceType.IS_IPHONE_6 {
//            scroll_HeightConstraint.constant = 580
//        } else if AppConstants.DeviceType.IS_IPHONE_6P {
//            scroll_HeightConstraint.constant = 644
//        } else if AppConstants.DeviceType.IS_IPHONE_X { //11pro
//            scroll_HeightConstraint.constant = 710
//        } else if AppConstants.DeviceType.IS_IPHONE_XR { //11
//            scroll_HeightConstraint.constant = 800
//        } else if AppConstants.DeviceType.IS_IPHONE_XS_MAX {
//            scroll_HeightConstraint.constant = 810
//        }
    }
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title)")
        tagView.isSelected = !tagView.isSelected
    }
    
    func showPickedImage(_ info: [UIImagePickerController.InfoKey : Any]) {
        addImages(info)
    }
    
    func addImages(_ info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage{
            imageArray.append(image)
            collection_View.reloadData()
        }
    }
    
    func openCustomCamera() {
        if ( UIImagePickerController.isSourceTypeAvailable(.camera)) {
            let vc = CustomeCameraVC.init(nibName: "CustomeCameraVC", bundle: nil)
            vc.isFromProfile = true
            vc.isImagePicked = { dictInfo in
                self.showPickedImage(dictInfo)
            }
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let actionController: UIAlertController = UIAlertController(title: "KMsgInvalidCamera".localized, message: "", preferredStyle: .alert)
            let cancelAction: UIAlertAction = UIAlertAction(title: "KLblOk".localized, style: .cancel) { action -> Void     in
                //Just dismiss the action sheet
            }
            
            actionController.addAction(cancelAction)
            actionController.popoverPresentationController?.sourceView = UIViewController.current().view
            UIViewController.current().present(actionController, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func support_Btn(_ sender: UIButton) {
        support_View.isHidden = false
    }
    
    @IBAction func supportView_Suport_Btn(_ sender: UIButton) {
        support_View.isHidden = true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collection_View.dequeueReusableCell(withReuseIdentifier: "AddCar_Pics_CollectionViewCell", for: indexPath) as! AddCar_Pics_CollectionViewCell
        cell.border_View.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 252/255, alpha: 1.0)
       // cell.cars_ImageView.image = imageArray[indexPath.row]
        if indexPath.row > imageArray.count - 1 {
            cell.plus_Lbl.text = "+"
            cell.plus_Lbl.font = UIFont(name: "System", size: 22)
        } else {
            cell.cars_ImageView.image = imageArray[indexPath.row]
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        openCustomCamera()
    }
    
    //MARK: prepare datasource
    func prepareDataSource(){
        
        arrCell.removeAll()
        
        arrCell.append(CellModel.getModel(placeholder : "KLblAddPictures".localized, type: .ReportDamageAddPictureCell, dataArr: [CellModel]()))
        arrCell.append(CellModel.getModel(placeholder : "KLblAddComments".localized, type : .ReportDamageTextViewCell))
        arrCell.append(CellModel.getModel(placeholder : "KLblDirtyInterior".localized, placeholder2 : "KLblDamaged".localized, text : String(AppConstants.ReportDamage.DIRTY_INTERIOR), userText1: String(AppConstants.ReportDamage.DAMAGE), type: .ReportDamageIssueCell1))
        arrCell.append(CellModel.getModel(placeholder : "KLblDirtyExterior".localized, placeholder2 : "KLblRefillWindScreen".localized, text : String(AppConstants.ReportDamage.DIRTY_EXTERIOR), userText1: String(AppConstants.ReportDamage.REFILL_WINDSCREEN_WASHER_FLUID), type: .ReportDamageIssueCell2))
        
        tableView.reloadData()
    }
    
    //MARK: Button action methods
    
    func btnSubmit_Click(){
        btnSubmit.click = {
            self.callAPIForSubmitDamage()
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
       let height = ceil(textView.contentSize.height)

        if height != textView_HeightConstraint.constant {
            textView_HeightConstraint.constant = height
            textView.setContentOffset(CGPoint.zero, animated: false)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        if numberOfChars > 255 {
            
        } else {
            self.count_Lbl.text =  "\(textView.text.count)/255"
        }
        return numberOfChars < 256
    }
    
    @IBAction func btnRentalReport(_ sender: UIButton) {
        let vc = DriveNowVC()
        vc.isRentalExpand = true
        if let vehicleModel = rideModel.vehicleId {
            vc.vehicleModel = vehicleModel
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCrashReport(_ sender: UIButton) {
        let vc = Report_Crash_ViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func submit_Btn(_ sender: ButtonDesign) {
        callAPIForSubmitDamage()
    }
    
    @IBAction func view_History_Btn(_ sender: UIButton) {
        let VC = Report_History_ViewController()
        navigationController?.pushViewController(VC, animated: true)
    }
}

//MARK: Tableview methods
extension ReportDamageVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCell.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrCell[indexPath.row]
        
        switch model.cellType!{
            
        case .ReportDamageAddPictureCell:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddPictureCell", for: indexPath) as! AddPictureCell
            
            cell.setCellData(model, maxCount: 1)
            cell.carModel = [CarModel.getModel(photoSide: "Full Car", imageType: AppConstants.DamageImageType.FULL_CAR),               CarModel.getModel(photoSide: "Close", imageType: AppConstants.DamageImageType.CLOSE_UP)]
            
            cell.reloadTable = { arr in
                model.dataArr = arr
                //                self.tableView.reloadRows(at: [indexPath], with: .automatic)
            }
            return cell
            
        case .ReportDamageIssueCell1, .ReportDamageIssueCell2:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReportDamageIssueCell", for: indexPath) as! ReportDamageIssueCell
            cell.setCellData(model)
            cell.btnCheck1.addTarget(self, action: #selector(btnCheck1_Cilck), for: .touchUpInside)
            cell.btnCheck1.tag = indexPath.row
            cell.btnCheck2.addTarget(self, action: #selector(btnCheck2_Cilck), for: .touchUpInside)
            cell.btnCheck2.tag = indexPath.row
            return cell
            
        case .ReportDamageTextViewCell :
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "UnlockCarCell", for: indexPath) as! UnlockCarCell
            cell.setCellData(model)
            return cell
            
        default : return UITableViewCell()
        }
        
    }
    
    @IBAction func btnCheck1_Cilck(_ sender : UIButton){
        let model = arrCell[sender.tag]
        model.isSelected = !model.isSelected
        tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .automatic)
    }
    
    @IBAction func btnCheck2_Cilck(_ sender : UIButton){
        let model = arrCell[sender.tag]
        model.isSelected2 = !model.isSelected2
        tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .automatic)
    }
}

//MARK: Api call
extension ReportDamageVC{
    
    func callAPIForSubmitDamage() {
        
        var req = [String:Any]()
        req["vehicleId"] = rideModel.vehicleId?.id ?? ""
        req["partnerId"] = rideModel.vehicleId?.partnerId ?? ""
        
        if rideModel.id != nil {
            req["rideId"] = rideModel.id ?? ""
        }
        
        if true {
            let filter = arrCell.filter {$0.cellType == .ReportDamageAddPictureCell}
            
            if filter.count > 0 {
                if filter.first!.dataArr.count < 2 {
                    //   Utilities.showAlertView(message: "KLblPleaseUploadAllImageMsg".localized)
                    self.showToast(text: "KLblPleaseUploadAllImageMsg".localized)
                    
                    return
                }
                
                var dictAttachment : [[String:Any]] = [[:]]
                dictAttachment.removeAll()
                
                for cellModel  in filter.first!.dataArr {
                    if let model = cellModel as? CellModel {
                        let dict = ["path": model.userText ?? "", "type": model.imageType] as [String : Any]
                        dictAttachment.append(dict)
                    }
                }
                
                req["images"] = dictAttachment
                
            }
        }
        
        var arrDamages = [Int]()
        
        if true {
            
            let filter = arrCell.filter { $0.cellType == .ReportDamageIssueCell1 ||  $0.cellType == .ReportDamageIssueCell2}
            
            if filter.count > 0 {
                
                for item in filter {
                    if item.isSelected {
                        arrDamages.append(Int(item.userText ?? "0") ?? 0)
                    }
                    
                    if item.isSelected2 {
                        arrDamages.append(Int(item.userText1 ?? "0") ?? 0)
                    }
                }
            }
        }
        
        if arrDamages.count == 0 {
            self.showToast(text: "KLblSelectSingleDamage".localized)
            //            Utilities.showAlertView(message: "KLblSelectSingleDamage".localized)
            return 
        }
        
        req["damageCategory"] = arrDamages
        
        if true {
            
            let filter = arrCell.filter { $0.cellType == .ReportDamageTextViewCell }
            
            if Utilities.checkStringEmptyOrNil(str: filter.first?.userText) == false {
                req["description"] = filter.first?.userText
            }
        }
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.AddDamageReoport, method: .post, parameters: req, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            Utilities.showAlertWithButtonAction(title: "", message: message, buttonTitle: StringConstants.ButtonTitles.KOk, onOKClick: {
                
                self.navigationController?.popViewController(animated: true)
            })
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
