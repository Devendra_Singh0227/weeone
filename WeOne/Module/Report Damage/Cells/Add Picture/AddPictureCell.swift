//
//  AddPictureCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 23/01/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class AddPictureCell: UITableViewCell {
    
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var constraintHeightCollView : NSLayoutConstraint!
    
    @IBOutlet weak var viewBorder: UIView!

    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    var cellCount : Double = 4.0
    var arrData : [CellModel] = [CellModel]()
  //  var dictCarSide: [String: Int] = ["Front": AppConstants.RentalImageType.FRONT, "Right": AppConstants.RentalImageType.RIGHT, "Back": AppConstants.RentalImageType.BACK, "Left": AppConstants.RentalImageType.LEFT]
    
    var carModel: [CarModel] = [CarModel.getModel(photoSide: "Front", imageType: AppConstants.RentalImageType.FRONT),               CarModel.getModel(photoSide: "Right", imageType: AppConstants.RentalImageType.RIGHT),
    CarModel.getModel(photoSide: "Back", imageType: AppConstants.RentalImageType.BACK),
    CarModel.getModel(photoSide: "Left", imageType: AppConstants.RentalImageType.LEFT)]
    
    var reloadTable : ((_ arr : [CellModel]) -> ())?
    var collViewLine : Double = 1
    var maxCount : Int = 0
    var tappedIndex : Int = 0
    
    var cellModel = CellModel()
    
    var classType: ClassType?
    var selectedIndex: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        collView.delegate = self
        collView.dataSource = self
        collView.register(UINib(nibName: "AddPictureCollCell", bundle: nil), forCellWithReuseIdentifier: "AddPictureCollCell")
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setCellData(_ model : CellModel, maxCount : Int = 0){
        
        self.cellModel = model
        
        self.maxCount = maxCount
        self.classType = model.classType
        
        lblTitle.text = model.placeholder ?? "-"
        
        if Utilities.checkStringEmptyOrNil(str: model.placeholder2 ){
            lblDesc.isHidden = true
        }else{
            lblDesc.isHidden = false
            lblDesc.text = model.placeholder2
        }
        
        if let arr = model.dataArr as? [CellModel]{
            arrData = arr
        }
        
        if maxCount > 0 {
            lblCount.text = "\(arrData.count)/\(maxCount)"
            lblCount.isHidden = false
        } else {
            lblCount.isHidden = true
        }
        
        viewBorder.isHidden = false

        //Damage report cell configuration
        if model.cellType == .ReportDamageAddPictureCell {
            lblTitle.isHidden = true
            lblDesc.isHidden = true
            lblCount.isHidden = true
        } else {
           // lblTitle.font = FontScheme.kBoldFont(size: 14)
            lblTitle.textAlignment = .left
            if maxCount > 0 {
                lblCount.isHidden = false
            } else {
                lblCount.isHidden = true
            }
        }
        
        collView.reloadData()
    }
    
    func addImages(_ info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage{
            
            let model = CellModel()
            
            print(tappedIndex)
            print(carModel)

//            let keys = Array(dictCarSide.keys)
//
////            dictCarSide = dictCarSide.sorted {$0.value as? Int < $1.value as? Int}
//
//
//            let side: String = keys[tappedIndex]
//
            
            
            if arrData.count > 0 && arrData.contains(where: {$0.message == carModel[tappedIndex].photoSide}){
                self.arrData[tappedIndex].cellObj = image
                self.arrData[tappedIndex].imageType = carModel[tappedIndex].imageType
                self.arrData[tappedIndex].isUploading = true
                self.arrData[tappedIndex].uploadProgress = 0.0
                
            } else {
                
                model.cellObj = image
                model.imageType = carModel[tappedIndex].imageType
                model.message = carModel[tappedIndex].photoSide
                model.isUploading = true
                
                self.arrData.append(model)
            }
            
        }
        
        self.collView.reloadData()
        
        //        }
    }
    
    func showPickedImage(_ info: [UIImagePickerController.InfoKey : Any]) {
        addImages(info)
    }
    
    func openCustomCamera() {
        if ( UIImagePickerController.isSourceTypeAvailable(.camera)) {
            let vc = CustomeCameraVC.init(nibName: "CustomeCameraVC", bundle: nil)
            vc.isImagePicked = { dictInfo in
                self.showPickedImage(dictInfo)
            }
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let actionController: UIAlertController = UIAlertController(title: "KMsgInvalidCamera".localized, message: "", preferredStyle: .alert)
            let cancelAction: UIAlertAction = UIAlertAction(title: "KLblOk".localized, style: .cancel) { action -> Void     in
                //Just dismiss the action sheet
            }
            
            actionController.addAction(cancelAction)
            actionController.popoverPresentationController?.sourceView = UIViewController.current().view
            UIViewController.current().present(actionController, animated: true, completion: nil)
            
        }
        
    }
    
    func choosePicture() {
        if NetworkReachabilityManager()?.isReachable ?? false {
            
            UIViewController.current().view.endEditing(true)
            
            #if DEBUG
//            openCustomCamera()
            
            Utilities.open_galley_or_camera(delegate: self)
            
            //
            #else
            openCustomCamera()
            
            #endif
        } else {
            Utilities.showAlertView(message: "KMsgNoInternetConnection".localized)
        }
        
    }
    
    @IBAction func btnPlus_Click(_ sender : UIButton){
        
        if self.cellModel.cellType == .ReportDamageAddPictureCell && self.arrData.count >= 1 {
            carModel.append(CarModel.getModel(photoSide: "Other \(self.arrData.count)", imageType: AppConstants.DamageImageType.OTHER))
        }
                
        if maxCount > 0 {
            
            if sender.tag == 0 && maxCount != arrData.count {
                tappedIndex = arrData.count
            } else {
                if maxCount != arrData.count{
                    tappedIndex = sender.tag - 1
                }else{
                    tappedIndex = sender.tag
                }
            }
            
        }else{
            
            if sender.tag == 0{
                tappedIndex = sender.tag
            }else{
                tappedIndex = sender.tag - 1
            }
        }
        
        choosePicture()
    }
}

//MARK: Collection view delegate methods
extension AddPictureCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.cellModel.cellType == .ReportDamageAddPictureCell {
            maxCount = maxCount + 1
            return arrData.count + 1
        } else {
            if (maxCount > 0 && maxCount == arrData.count) {
                return arrData.count
            } else if self.cellModel.cellType == .UnlockCarOdometerCell {
                return 1
            } else {
                return arrData.count + 1
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddPictureCollCell", for: indexPath) as! AddPictureCollCell
        
        
        if maxCount > 0 {
            
            if (indexPath.row == 0 && maxCount != arrData.count) || (indexPath.row == 0 && self.cellModel.cellType == .ReportDamageAddPictureCell) {
                cell.setCellForPickImage(cellType: cellModel.cellType, carSide: carModel[arrData.count].photoSide ?? "", index: carModel.count)
                cell.btnPlus.addTarget(self, action: #selector(btnPlus_Click), for: .touchUpInside)
                cell.btnPlus.tag = indexPath.row
            } else {
                if maxCount != arrData.count || self.cellModel.cellType == .ReportDamageAddPictureCell{
                    cell.setCellData(arrData[indexPath.row - 1])
                }else{
                    cell.setCellData(arrData[indexPath.row])
                }
            }
            
        }else{
            
            if indexPath.row == 0 {
                
                cell.setCellForPickImage(cellType: cellModel.cellType, carSide: carModel[indexPath.row].photoSide ?? "", index: carModel.count)
                cell.btnPlus.addTarget(self, action: #selector(btnPlus_Click), for: .touchUpInside)
                cell.btnPlus.tag = indexPath.row
            }else{
                cell.setCellData(arrData[indexPath.row - 1])
            }
        }
        
        cell.isImageUploaded = { (value, path, cellModel) in
            self.lblCount.text = "\(self.arrData.count)/\(self.maxCount)"
            self.reloadTable?(self.arrData)
            
        }
        return cell
    }
    
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        
//        if self.cellModel.cellType == .UnlockCarOdometerCell || self.cellModel.cellType == .ReportDamageAddPictureCell{
//            let cellWidth: CGFloat = 82
//            let cellSpacing: CGFloat = 6
//            let cellCount = CGFloat(collectionView.numberOfItems(inSection: section))
//            let collectionWidth = collectionView.frame.size.width
//            
//            let totalCellWidth = cellWidth * cellCount
//            let totalSpacingWidth = cellSpacing * (cellCount - 1)
//
//            let leftInset = (collectionWidth - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
//            let rightInset = leftInset
//
//            return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
//        } else {
//            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//        }
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 92, height: 100)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
      //  if self.classType != .ReportDamageVC {
            selectedIndex = indexPath.row
            if maxCount > 0 {
                
                if indexPath.row == 0 && maxCount != arrData.count {
                    tappedIndex = indexPath.row
                } else {
                    if maxCount != arrData.count {
                        tappedIndex = indexPath.row - 1
                    }else{
                        tappedIndex = indexPath.row
                    }
                }
                
            }else{
                
                if indexPath.row == 0{
                    tappedIndex = indexPath.row
                }else{
                    tappedIndex = indexPath.row - 1
                }
            }

            choosePicture()
      //  }
    }
}


//MARK: Imagepicker delegate methods
extension AddPictureCell : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        addImages(info)
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

