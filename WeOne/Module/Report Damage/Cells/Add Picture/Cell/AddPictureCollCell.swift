//
//  AddPictureCollCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 23/01/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class AddPictureCollCell: UICollectionViewCell {
    
    @IBOutlet weak var viewMain: CustomDashedView!

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgAdd: UIImageView!

    @IBOutlet weak var imgOdometer: UIImageView!
    @IBOutlet weak var imgRetry: UIImageView!
    @IBOutlet weak var btnRetry: UIButton!
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var viewProgress: UIView!
    @IBOutlet weak var viewOdometer: UIView!
    @IBOutlet weak var lblCarSide: UILabel!

    @IBOutlet weak var lblProgress: UILabel!
    @IBOutlet weak var circularProgressBar: CircularProgressBar!
    
    var cellModel : CellModel?
    var isImageUploaded :((Bool, String, CellModel?) -> ())?
    
    var indexPath: IndexPath?
    
    var isFromReportDamage = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setCircularProgress()
        viewProgress.isHidden = true
        viewMain.dashColor = ColorConstants.TextColorSecondary
        self.circularProgressBar.isHidden = true
    }
    
    func setCellData(_ model : CellModel){
        
        cellModel = model
                
        if model.cellObj != nil {
            
            lblCarSide.isHidden = true 
            viewMain.betweenDashesSpace = 0
            viewOdometer.isHidden = true
            imgView.image = model.cellObj as? UIImage
            imgView.isHidden = false
            btnPlus.isUserInteractionEnabled = false
            imgAdd.isHidden = true
        } else {
            
            viewMain.betweenDashesSpace = 4

            if cellModel?.cellType == .UnlockCarOdometerCell {
                viewOdometer.isHidden = false
            } else {
                viewOdometer.isHidden = true
            }
            imgView.isHidden = true
            btnPlus.isUserInteractionEnabled = true
        }

        if model.isUploading && model.uploadProgress == 0.0 {
            self.uploadImage()
        }
        
        

        if model.classType == .ReportDamageVC {
            viewProgress.isHidden = true
            
            btnRetry.isHidden = true
            imgRetry.isHidden = true

        } else {
            viewMain.backgroundColor = ColorScheme.kTextColorWhitePrimary()
        }
        viewMain.layoutSubviews()
    }
    
    func setCellForPickImage(cellType: CellType?, carSide: String, index: Int){
        
        imgAdd.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Add"))

        lblCarSide.isHidden = true
        imgAdd.isHidden = true
        imgView.isHidden = true
        viewMain.betweenDashesSpace = 4
        
        if cellType == .UnlockCarOdometerCell {
            viewOdometer.isHidden = false
        } else {
            if cellType == .ReportDamageAddPictureCell && index > 2 {
                lblCarSide.isHidden = true
                viewOdometer.isHidden = true
                imgAdd.isHidden = false
            } else {
                
                viewOdometer.isHidden = true
                lblCarSide.text = carSide
                lblCarSide.isHidden = false
            }
        }

        btnPlus.isHidden = false
        btnPlus.isUserInteractionEnabled = true
        
        viewMain.layoutSubviews()

    }
    
    
    func setCircularProgress(){
        circularProgressBar.lineWidth = 3.0
        circularProgressBar.safePercent = 100
        circularProgressBar.labelSize = 0
    }
    
    func uploadImage() {
        callAPIForUploadImage(image: imgView.image!)
        
    }
    
    @IBAction func btnRetry_Click(_ sender: UIButton) {
        if cellModel?.isUploading == true && cellModel?.isUploaded == false {
            callAPIForUploadImage(image: imgView.image!)
        }
    }
    
}
//MARK: Api calls
extension AddPictureCollCell{
    
    func callAPIForUploadImage(image:UIImage) {
        
        UploadManager.sharedInstance.uploadFile(AppConstants.serverURL, command: AppConstants.URL.UploadFile, image: image, uploadParamKey: "file", params: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, uploadProgress: { (progress, uploadIndex) in
            
            self.cellModel?.uploadProgress = Double((progress) * 100)
            self.cellModel?.isUploading = true
            self.lblProgress.text = Utilities.toDoubleString(value: Double((progress) * 100))
            //self.circularProgressBar.setProgress(to: Double(progress), withAnimation: false)
            
        }, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any] {
                
                print("Response \(dictResponse)")
                
                if let dict = dictResponse["data"] as? [String:Any]{
                    
                    if let arrFiles = dict["files"] as? [[String:Any]] {
                        if let path = arrFiles.first?["absolutePath"] as? String {
                            
                            self.cellModel?.userText = path
                            self.cellModel?.isUploaded = true
                            self.cellModel?.isUploading = false
                            
                            self.viewProgress.isHidden = true
                            self.isImageUploaded?(true, self.cellModel?.userText ?? "", self.cellModel)
 
                        }
                    }
                }
            }
            
        }) { (failureMessage) in
            self.cellModel?.isUploaded = false
            self.cellModel?.isFailed = true
            self.btnRetry.isHidden = false
            self.imgRetry.isHidden = false
            
            self.viewProgress.isHidden = true
            
            self.btnPlus.isHidden = true
            
            
            self.isImageUploaded?(false, "", self.cellModel)
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
class CustomDashedView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var dashWidth: CGFloat = 0
    @IBInspectable var dashColor: UIColor = .clear
    @IBInspectable var dashLength: CGFloat = 0
    @IBInspectable var betweenDashesSpace: CGFloat = 0

    var dashBorder: CAShapeLayer?

    override func layoutSubviews() {
        super.layoutSubviews()
        dashBorder?.removeFromSuperlayer()
        let dashBorder = CAShapeLayer()
        dashBorder.lineWidth = dashWidth
        dashBorder.strokeColor = dashColor.cgColor
        dashBorder.lineDashPattern = [dashLength, betweenDashesSpace] as [NSNumber]
        dashBorder.frame = bounds
        dashBorder.fillColor = nil
        if cornerRadius > 0 {
            dashBorder.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        } else {
            dashBorder.path = UIBezierPath(rect: bounds).cgPath
        }
        layer.addSublayer(dashBorder)
        self.dashBorder = dashBorder
    }
}
