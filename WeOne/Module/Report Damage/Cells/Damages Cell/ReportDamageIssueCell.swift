//
//  ReportDamageIssueCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 23/01/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class ReportDamageIssueCell: UITableViewCell {

    @IBOutlet weak var btnCheck1: UIButton!
    @IBOutlet weak var btnCheck2: UIButton!
    @IBOutlet weak var btnCheckSel1: UIButton!
    @IBOutlet weak var btnCheckSel2: UIButton!
    @IBOutlet weak var lblDesc1: UILabel!
    @IBOutlet weak var lblDesc2: UILabel!
    @IBOutlet weak var viewOption1: UIView!
    @IBOutlet weak var viewOption2: UIView!
    @IBOutlet weak var constraintHeightView1: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(_ model: CellModel){
        
        lblDesc1.text = model.placeholder ?? "-"
        btnCheckSel1.isSelected = model.isSelected
        
        lblDesc2.text = model.placeholder2 ?? "-"
        btnCheckSel2.isSelected = model.isSelected2
        
        if Utilities.checkStringEmptyOrNil(str: model.userText1) {
            lblDesc2.isHidden = true
            btnCheckSel2.isHidden = true
        }else{
            lblDesc2.isHidden = false
            btnCheckSel2.isHidden = false
        }
        
        if model.cellType == .ReportDamageIssueCell1{
            constraintHeightView1.constant = 24
        }else{
            constraintHeightView1.constant = 44
        }
        
        if model.isSelected {
            btnCheckSel1.setImage(UIImage(named: ThemeManager.sharedInstance.getImage(string: "checked")), for: .normal)
        }else {
            btnCheckSel1.setImage(UIImage(named: "unchecked_checkbox_black"), for: .normal)

        }

        if model.isSelected2 {
            btnCheckSel2.setImage(UIImage(named: ThemeManager.sharedInstance.getImage(string: "checked")), for: .normal)
        }else {
            btnCheckSel2.setImage(UIImage(named: "unchecked_checkbox_black"), for: .normal)
        }

    }
}
