//
//  TextViewCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 23/01/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class TextViewCell: UITableViewCell {

    @IBOutlet weak var textView: UIPlaceHolderTextView!
    
    var model : CellModel = CellModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        textView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(_ model : CellModel){
        self.model = model
        textView.placeholder = model.placeholder
    }
}

extension TextViewCell : UITextViewDelegate{
    
    func textViewDidEndEditing(_ textView: UITextView) {
        model.userText = textView.text
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        
        
        if !text.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        
        if range.location == 0{
            
            if text == " " {
                return false
            }
        }
        
        return true
    }
}
