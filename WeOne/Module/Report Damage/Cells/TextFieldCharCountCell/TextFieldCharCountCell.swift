//
//  TextFieldCharCountCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 10/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class TextFieldCharCountCell: UITableViewCell {
    
    var model : CellModel = CellModel()
    var charLimit = 250
    
    @IBOutlet weak var txtField: SkyFloatingLabelTextField!
    @IBOutlet weak var lblCharCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        txtField.delegate = self
        
        txtField.placeholderColor = ColorConstants.TextColorPrimary
//        txtField.placeholderFont = FontScheme.kLightFont(size: 16)
        txtField.titleColor = ColorConstants.TextColorPrimary
//        txtField.titleFont = FontScheme.kLightFont(size: 13)
        txtField.selectedTitleColor = ColorConstants.TextColorTheme
        txtField.selectedLineColor = UIColor.clear
        txtField.lineColor = UIColor.clear
        txtField.tintColor = ColorConstants.TextColorTheme
        
        lblCharCount.text = "0/250"
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setCellData(_ model : CellModel){
        self.model = model
        txtField.text = model.userText
        txtField.placeholder = model.placeholder
    }
}


extension TextFieldCharCountCell : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.model.userText = textField.text
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let result = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        
        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        
        if range.location == 0{
            
            if string == " " {
                return false
            }
        }
        
        
        guard let stringRange = Range(range, in: textField.text ?? "") else { return false }
        
        let updatedText = (textField.text ?? "").replacingCharacters(in: stringRange, with: string)
        
        lblCharCount.text = "\(updatedText.count)/250"
        return updatedText.count < charLimit
    }
}
