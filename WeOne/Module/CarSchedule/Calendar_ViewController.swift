//
//  Calendar_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 25/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit
import FSCalendar

class Calendar_ViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance {

    @IBOutlet weak var date_Lbl: UILabel!
    @IBOutlet weak var calendar_View: FSCalendar!
    var checkScreen = ""
    var pickup = ""
    var drop = ""
    var callback_Date : ((String) -> ())?
    
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //print(pickup, drop)
        
        setUp_Calendar()
        initialConfig()
    }
    
    func initialConfig() {
        
//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
    }

    func setUp_Calendar() {
        calendar_View.scrollDirection = .vertical
    
        if pickup == "" && drop == "" {
            date_Lbl.text = "Start Date - End Date"
        } else if pickup == "" && drop != "" {
            date_Lbl.text = "Start Date - \(drop)"
        } else if drop == "" && pickup != "" {
            date_Lbl.text = "\(pickup) End - Date"
        } else if drop != "" && pickup != "" {
            date_Lbl.text = "\(pickup) - \(drop)"
        }
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // print("did deselect date \(self.formatter.string(from: date))")
       
        if checkScreen == "PickUp" {
           if pickup == "" && drop == "" {
                date_Lbl.text = "\(self.formatter.string(from: date)) - End Date"
            } else if pickup == "" && drop != "" {
                date_Lbl.text = "\((self.formatter.string(from: date))) - \(drop)"
            } else if drop == "" && pickup != "" {
                date_Lbl.text = "\((self.formatter.string(from: date))) End - Date"
           } else if drop != "" && pickup != "" {
                date_Lbl.text = "\((self.formatter.string(from: date))) - \(drop)"
           }
            
        } else {
            if pickup == "" && drop == "" {
                date_Lbl.text = "Start Date - \(self.formatter.string(from: date))"
            } else if pickup == "" && drop != "" {
                date_Lbl.text = "\(pickup) - \((self.formatter.string(from: date)))"
            } else if drop == "" && pickup != "" {
                date_Lbl.text = "\(pickup) - \(self.formatter.string(from: date))"
            } else if drop != "" && pickup != "" {
                 date_Lbl.text = " \(pickup) - \((self.formatter.string(from: date)))"
            }
        }
        callback_Date?(self.formatter.string(from: date))
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {        
        navigationController?.popViewController(animated: true)
    }

    @IBAction func confirm_Btn(_ sender: ButtonDesign) {
        navigationController?.popViewController(animated: true)
    }
}
