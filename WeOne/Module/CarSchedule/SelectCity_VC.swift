//
//  SelectCity_VC.swift
//  WeOne
//
//  Created by Dev's Mac on 25/09/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class SelectCity_VC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var country_TblView: UITableView!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    var arrCities = [String]()
    var arrCityId = [String]()
    var arrCity = [CityModel]()
    var selectedCity : ((_ name: String, _ id: String) -> ())?
    var selected_Index = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialConfig()
        // Do any additional setup after loading the view.
    }
    
    func initialConfig() {
//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
        callApiForGetCities()
        
         country_TblView.register(UINib(nibName: "CountryCell", bundle: nil), forCellReuseIdentifier: "CountryCell")
    }

    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = country_TblView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath) as? CountryCell
        let cities = arrCities[indexPath.row]
        cell?.imgTrailing.constant = -30
        cell?.lblTitle.text = cities
        if indexPath.row == selected_Index {
            cell?.lblTitle.font = FontScheme.kSemiBoldFont(size: 18.0)
            cell?.lblTitle.textColor = ColorScheme.kTextColorGreen()
        } else {
            cell?.lblTitle.textColor = ColorScheme.KcommontextColor()
            cell?.lblTitle.font = FontScheme.kRegularFont(size: 16.0)
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cities = arrCities[indexPath.row]
        let id = arrCityId[indexPath.row]
        print(cities)
        selected_Index = indexPath.row
        country_TblView.reloadData()
        selectedCity?(cities, id)
    }
  
    func callApiForGetCities(){
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.GetCities, method: .post, parameters: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            self.arrCities.removeAll()
            print(response)
            //save Data
            if let dict = response as? [String:Any]{
                if let list = dict["list"] as? [[String:Any]]{
                    self.arrCity = Mapper<CityModel>().mapArray(JSONArray: list)
                }
            }
            
            for id in self.arrCity {
                self.arrCityId.append(id.id ?? "")
            }
            for city in self.arrCity {
                self.arrCities.append(city.baseName ?? "")
            }
            self.country_TblView.reloadData()
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
