//
//  CarScheduleVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 26/11/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class CarScheduleVC: UIViewController {
    
    //MARK: - Variables
    var selectedTag = -1
    let dateStartPicker = GMDatePicker()
    let dateEndPicker = GMDatePicker()
    var cityId: String = ""
    var startDate:Date = Date()
    var endDate:Date = Date()
    @IBOutlet weak var city_TxtFld: UITextField!
    var StartDate = ""
    var EndDate = ""
    var getScheduleDetail : ((_ startDate : Date, _ endDate: Date,  _ cityId: String) -> ())?
    var arrCities = [String]()
    var arrCity = [CityModel]()
    var check_image = ""
    var drop = ""
    var Pickup = ""
    
//    var selectedCountry: Int? {
//        didSet {
//            if let index = selectedCountry {
//                //KD lblCountryCode.text = self.arrCountryCodes[index]
//                btnCityPicker.setTitle(self.arrCities[index], for: .normal)
//                cityId = arrCity[index].id ?? ""
//                btnCityPicker.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 5.0, bottom: 0.0, right: 0.0)
//
//
//            }
//        }
//    }

    //MARK: - Outlets

    @IBOutlet weak var self_picup_View: ViewDesign!
    @IBOutlet weak var home_delivey_View: ViewDesign!
    @IBOutlet weak var airport_delivery_View: ViewDesign!
    @IBOutlet weak var self_pick_Btn: UIButton!
    @IBOutlet weak var home_delivery_Btn: UIButton!
    @IBOutlet weak var airport_Btn: UIButton!
    @IBOutlet weak var pickUp_Date_Lbl: UITextField!
    @IBOutlet weak var drop_Date_Lbl: UITextField!
    @IBOutlet weak var pickUp_Time_Lbl: UITextField!
    @IBOutlet weak var drop_Time_Lbl: UITextField!
    @IBOutlet weak var cardelivery_priceView: UIView!
    @IBOutlet weak var find_Cars_Btn_TopConstraint: NSLayoutConstraint!
    @IBOutlet weak var drop_image: UIImageView!
    @IBOutlet weak var same_pickup_dropimage: UIImageView!
    @IBOutlet weak var pick_Location_TxtFld: UITextField!
    @IBOutlet weak var drop_Location_TxtFld: UITextField!
    @IBOutlet weak var info_View: ViewDesign!
    @IBOutlet weak var cardDelivery_ViewHeight: NSLayoutConstraint!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        check_image = "drop"
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }


    //MARK: Private Methods
    func initialConfig() {
        
//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
        LocationManager.SharedManager.getLocation()
        LocationManager.SharedManager.getCurrentLocation = { location in
            Utilities.getAddressFromLatLon(pdblLatitude: location.coordinate.latitude, withLongitude: location.coordinate.longitude) { (address, pm) in
                self.city_TxtFld.text = pm?.locality
            }
        }
                  
//        callApiForGetCities()
                
//        DispatchQueue.main.async {
//            LocationManager.SharedManager.getLocation()
//            LocationManager.SharedManager.getCurrentLocation = { location in
//                Utilities.getAddressFromLatLon(pdblLatitude: location.coordinate.latitude, withLongitude: location.coordinate.longitude) { (address, pm) in
//                    self.btnCityPicker.setTitle(pm?.locality ?? "", for: .normal)
//                }
//            }
//        }

//        txtStartTime.tintColor = ColorConstants.ThemeColor
//        txtEndTime.tintColor = ColorConstants.ThemeColor
//
//        dateStartPicker.datePicker.minimumDate = Date()
//        dateEndPicker.datePicker.minimumDate = Date()
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            self.selectedTag = 0
//            self.zoomInAnimation()
//            UIViewController.openDateTimerPicker(datePicker: self.dateStartPicker, delegate: self, selectedDate: self.startDate)
//        }
//        viewNavigation.text = "KLblScheduleSmall".localized as NSString
//
//        viewNavigation.click = {
//            self.navigationController?.popViewController(animated: true)
//        }
//
//        btnShowCar.click = {
//            self.btnShowVehicle_Click()
//        }

    }
    
    func configureTextField(txtFld:SkyFloatingLabelTextField) {
        txtFld.lineColor = UIColor.white.withAlphaComponent(0.8)
        txtFld.selectedLineColor = UIColor.white
        txtFld.titleColor = UIColor.white.withAlphaComponent(0.8)
        txtFld.selectedTitleColor = UIColor.white.withAlphaComponent(0.8)
        txtFld.placeholderColor = UIColor.white
        txtFld.textColor = UIColor.white
//        txtFld.font = FontScheme.kRegularFont(size: 14)
//        txtFld.titleFont = FontScheme.kRegularFont(size: 12)
//        txtFld.placeholderFont = FontScheme.kRegularFont(size: 18)
        txtFld.tintColor = ColorConstants.ThemeColor
    }
    
    //MARK:- IBActions
//    func btnShowVehicle_Click() {
//        self.view.endEditing(true)
//
//        if Utilities.checkStringEmptyOrNil(str: txtStartTime.text) {
//            Utilities.showAlertView(message: "KLblPleaseSelectStartTime".localized)
//            return
//        }
//
//        if Utilities.checkStringEmptyOrNil(str: txtEndTime.text) {
//            Utilities.showAlertView(message: "KLblPleaseSelectEndTime".localized)
//            return
//        }
//
//        if startDate.ignoreSeconds() >= endDate.ignoreSeconds() {
//            Utilities.showAlertView(message: "KLblEnddateBiggerStartdate".localized)
//            return
//        }
//
//        if DateUtilities.findDateDiff(date1: startDate, date2: endDate) <= AppConstants.ScheduleMinuteInterval {
//
//            Utilities.showAlertView(message: String(format : "KLblScheduleDateDifferece".localized,"\(AppConstants.ScheduleMinuteInterval)"))
//            return
//        }
//
//        if cityId != "" {
////            callApiForAvailableVehicleInCity()
//        } else {
//            self.getScheduleDetail?(self.startDate, self.endDate, "")
//            self.navigationController?.popViewController(animated: true)
//        }
//    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func pick_up_time_btn(_ sender: UIButton) {
        
    }
    
    @IBAction func find_Cars_Btn(_ sender: UIButton) {
        if drop_Time_Lbl.text == "" || drop_Date_Lbl.text == "" {
            
        } else {
            let End_Date = drop_Date_Lbl.text! + drop_Time_Lbl.text!
             endDate = DateUtilities.convertDateFromStringWithFromat(dateStr: End_Date, format: DateUtilities.DateFormates.kMainSourceFormat)
        }
        if pickUp_Date_Lbl.text == "" || pickUp_Time_Lbl.text == "" {
             
        } else {
            let Start_Date = pickUp_Date_Lbl.text! + pickUp_Time_Lbl.text!
             startDate = DateUtilities.convertDateFromStringWithFromat(dateStr: Start_Date, format: DateUtilities.DateFormates.kMainSourceFormat)
        }

        print(StartDate, EndDate, cityId)
        getScheduleDetail?(endDate, startDate, cityId)
    }
    
    @IBAction func citySelect_DropDownBtn(_ sender: UIButton) {
        let VC = SelectCity_VC()
        VC.selectedCity = { (city, id) in
//            print(city,id)
            self.cityId = id
            self.city_TxtFld.text = city
        }
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func self_pickup_Btn(_ sender: UIButton) {
        self_picup_View.borderwidth = 2
        home_delivey_View.borderwidth = 0
        airport_delivery_View.borderwidth = 0
        sender.setTitleColor(UIColor(red: 54/255, green: 216/255, blue: 226/255, alpha: 1.0), for: .normal)
        home_delivery_Btn.setTitleColor(UIColor.darkGray, for: .normal)
        airport_Btn.setTitleColor(UIColor.darkGray, for: .normal)
        cardelivery_priceView.isHidden = true
//        find_Cars_Btn_TopConstraint.constant = 40
        cardDelivery_ViewHeight.constant = 0
        drop_image.image = UIImage(named: "back_down")
        same_pickup_dropimage.image = UIImage(named: "back_down")
        check_image = "drop"
    }
    
    @IBAction func home_delivery_Btn(_ sender: UIButton) {
        self_picup_View.borderwidth = 0
        home_delivey_View.borderwidth = 2
        airport_delivery_View.borderwidth = 0
        sender.setTitleColor(UIColor(red: 54/255, green: 216/255, blue: 226/255, alpha: 1.0), for: .normal)
        self_pick_Btn.setTitleColor(UIColor.darkGray, for: .normal)
        airport_Btn.setTitleColor(UIColor.darkGray, for: .normal)
        cardelivery_priceView.isHidden = false
        cardDelivery_ViewHeight.constant = 90
//        find_Cars_Btn_TopConstraint.constant = 100
        drop_image.image = UIImage(named: "location_on-24px (1)")
        same_pickup_dropimage.image = UIImage(named: "location_on-24px (1)")
         check_image = "location"
    }
    
    @IBAction func airport_delivery_Btn(_ sender: UIButton) {

        self_picup_View.borderwidth = 0
        home_delivey_View.borderwidth = 0
        airport_delivery_View.borderwidth = 2
        sender.setTitleColor(UIColor(red: 54/255, green: 216/255, blue: 226/255, alpha: 1.0), for: .normal)
        self_pick_Btn.setTitleColor(UIColor.darkGray, for: .normal)
        home_delivery_Btn.setTitleColor(UIColor.darkGray, for: .normal)
        cardelivery_priceView.isHidden = false
        cardDelivery_ViewHeight.constant = 90
//        find_Cars_Btn_TopConstraint.constant = 100
        drop_image.image = UIImage(named: "back_down")
        same_pickup_dropimage.image = UIImage(named: "back_down")
        check_image = "drop"
    }
    
    @IBAction func pickUp_Date_Btn(_ sender: UIButton) {
        let VC = Calendar_ViewController()
        VC.checkScreen = "PickUp"
        VC.callback_Date = { (date) in
            self.pickUp_Date_Lbl.text = date
            self.pickUp_Date_Lbl.font = FontScheme.kSemiBoldFont(size: 16)
            self.Pickup = date
        }
        VC.pickup = Pickup
        VC.drop = drop
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func pickUp_Time_Btn(_ sender: UIButton) {
        let VC = Time_ViewController()
        VC.pickDate = pickUp_Date_Lbl.text!
        VC.dropDate = drop_Date_Lbl.text!
        VC.pickTime = { (time) in
            self.pickUp_Time_Lbl.text = time
            self.pickUp_Time_Lbl.font = FontScheme.kSemiBoldFont(size: 16)
        }
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func drop_Date_Btn(_ sender: UIButton) {
        let VC = Calendar_ViewController()
        VC.checkScreen = "Drop"
        VC.callback_Date = { (date) in
            self.drop_Date_Lbl.text = date
            self.drop = date
            self.drop_Date_Lbl.font = FontScheme.kSemiBoldFont(size: 16)
        }
        VC.drop = drop
        VC.pickup = Pickup
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func drop_Time_Btn(_ sender: UIButton) {
        let VC = Time_ViewController()
        VC.dropDate = drop_Date_Lbl.text!
        VC.pickDate = pickUp_Date_Lbl.text!
        VC.dropTime = { (time) in
            self.drop_Time_Lbl.text = time
           self.drop_Time_Lbl.font = FontScheme.kSemiBoldFont(size: 16)
        }
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func btnOpenDatePicker_Click(_ sender: UIButton) {
        
    }
    
    @IBAction func info_Btn_Click(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            info_View.isHidden = false
        } else {
            sender.tag = 0
            info_View.isHidden = true
        }
    }

    @IBAction func location_Btn(_ sender: UIButton) {
        if check_image == "location" {
            let VC = SetLocationVC()
            VC.pick_locationField = { (location) in
                self.pick_Location_TxtFld.text = location
                self.pick_Location_TxtFld.font = FontScheme.kSemiBoldFont(size: 16)
            }
            navigationController?.pushViewController(VC, animated: true)
        } else {
            
        }        
    }
    
    @IBAction func drop_Location_Btn(_ sender: UIButton) {
        if check_image == "location" {
            let VC = SetLocationVC()
            VC.drop_locationField = { (location) in
                self.drop_Location_TxtFld.text = location
                self.drop_Location_TxtFld.font = FontScheme.kSemiBoldFont(size: 16)
            }
            navigationController?.pushViewController(VC, animated: true)
        } else {
            
        }
    }
    
    
    //MARK: Private methods
//    func zoomInAnimation() {
//        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
//            if self.selectedTag == 0 {
//                self.viewStartTime.transform = CGAffineTransform.init(scaleX: 1.2, y: 1.2)
//                self.viewStartTimeLine.backgroundColor = ColorConstants.ThemeColor
//            }
//            else if self.selectedTag == 1  {
//                self.viewEndTime.transform = CGAffineTransform.init(scaleX: 1.2, y: 1.2)
//                self.viewEndTimeLine.backgroundColor = ColorConstants.ThemeColor
//
//            }
//        }) { (completed) in
//            //
//        }
//    }
    
//    func zoomOutAnimation() {
//        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
//            if self.selectedTag == 0 {
//                self.viewStartTime.transform = .identity
//                self.viewStartTimeLine.backgroundColor = ColorConstants.UnderlineColor
//            }
//            else if self.selectedTag == 1  {
//                self.viewEndTime.transform = .identity
//                self.viewEndTimeLine.backgroundColor = ColorConstants.UnderlineColor
//            }
//        }) { (completed) in
//            //
//        }
//    }
    
    func showPopUp() {
        
        let vc = ComingSoonPopupVC(nibName: "ComingSoonPopupVC", bundle: nil)
        vc.headerTitle = "KLblComingSoon".localized
        vc.message = "KLblCarMsg".localized
        vc.confirmButtonTitle = "KLblOKCap".localized
        vc.notifyButtonTitle = "KLblNotifyMe".localized
        vc.cityId = cityId
        
        vc.cancelBtnClicked = {
//                        onCancelClick()
        }
        
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        
        if UIViewController.current()?.presentedViewController != nil {
            UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
            })
        } else {
            UIViewController.current()?.present(vc, animated: true, completion: {
            })
        }
    }
    
    
//    func openEndDatePicker(){
//        selectedTag = 1
//        zoomInAnimation()
//        UIViewController.openDateTimerPicker(datePicker: dateEndPicker, delegate: self, selectedDate: endDate)
//    }
}

//MARK:- DatePicker Delegate's Methods
//extension CarScheduleVC: GMDatePickerDelegate {

//    func gmDatePicker(_ gmDatePicker: GMDatePicker, didSelect date: Date) {
//
//        let strDate = DateUtilities.convertStringFromDate(date: date, format: DateUtilities.DateFormates.kRegularDate)
//
//        let strTime = DateUtilities.convertStringFromDate(date: date, format: DateUtilities.DateFormates.k24Time)

//        if selectedTag == 0 {
//
//            zoomOutAnimation()
//            startDate = date
//            txtStartTime.text = "\(strDate) at \(strTime)"
//            dateEndPicker.datePicker.minimumDate = date
//
//            openEndDatePicker()
//        }
//        else if selectedTag == 1 {
//
//            zoomOutAnimation()
//            endDate = date
//            txtEndTime.text = "\(strDate) at \(strTime)"
//        }
//    }

//    func gmDatePickerDidCancelSelection(_ gmDatePicker: GMDatePicker) {
//        //cancel
//        print(gmDatePicker.datePicker)
//        if selectedTag == 0{
//            zoomOutAnimation()
//        }else{
//            zoomOutAnimation()
//        }
//    }
//}
//MARK: Api calls
extension CarScheduleVC {
    func callApiForGetCities(_ completion : (() -> ())? = nil){

        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.GetCities, method: .post, parameters: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in

            self.arrCities.removeAll()

            //save Data
            if let dict = response as? [String:Any]{
                if let list = dict["list"] as? [[String:Any]]{
                    self.arrCity = Mapper<CityModel>().mapArray(JSONArray: list)
                }
            }

            for city in self.arrCity {
                self.arrCities.append(city.baseName ?? "")
            }

            completion?()

        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }

    func callApiForAvailableVehicleInCity(_ completion : (() -> ())? = nil) {

        var param : [String:Any] = [:]

        param["reservedDateTime"] = DateUtilities.convertToISOFormat(dateStr: startDate)
        param["reservedEndDateTime"] = DateUtilities.convertToISOFormat(dateStr: endDate)

        if cityId != "" {
            param["cityId"] = self.cityId
        }

        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.AvailableVehicleInCity, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in

            //save Data
            if let dict = response as? [String:Any]{
                if let count = dict["count"] as? Int {
                    if count > 0 {
                        self.getScheduleDetail?(self.startDate, self.endDate, self.cityId)
                        self.navigationController?.popViewController(animated: true)

                    } else {
                        self.showPopUp()
                        return
                    }
                }
            }

            completion?()

        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
