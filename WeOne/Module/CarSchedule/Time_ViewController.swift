//
//  Time_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 26/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Time_ViewController: UIViewController {

    
    @IBOutlet weak var pickUpView: UIView!
    @IBOutlet weak var dropView: UIView!
    @IBOutlet weak var pickup_Picker: UIDatePicker!
    @IBOutlet weak var drop_Picker: UIDatePicker!
    @IBOutlet weak var first_Image: UIImageView!
    @IBOutlet weak var second_Check_Image: UIImageView!
    @IBOutlet weak var pickup_Time_Lbl: UILabel!
    @IBOutlet weak var drop_Time_Lbl: UILabel!
    @IBOutlet weak var pickupDate_Lbl: UILabel!
    @IBOutlet weak var drop_Date_Lbl: UILabel!
    var pickDate = ""
    var dropDate = ""
    var pickTime : ((String) -> ())?
    var dropTime : ((String) -> ())?
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialConfig()
        
         pickup_Picker.datePickerMode =  UIDatePicker.Mode.time
         drop_Picker.datePickerMode =  UIDatePicker.Mode.time
//        drop_Picker.setValue(UIColor.green, forKeyPath: "textColor")
//        drop_Picker.setValue(UIColor.green, forKeyPath: "textColor")
        pickupDate_Lbl.text = pickDate
        drop_Date_Lbl.text = dropDate
        // Do any additional setup after loading the view.
    }
    
    func initialConfig() {
           
//           if AppConstants.hasSafeArea {
//               headerView_HeightConstraint.constant = 90
//           }
//           else {
//               headerView_HeightConstraint.constant = 74
//           }
       }
    
    @IBAction func pickUp_Picker(_ sender: UIDatePicker) {
        sender.datePickerMode =  UIDatePicker.Mode.time
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateFormat = "HH:mm"
        let time = formatter.string(from: sender.date)
        print(time)//Group 2604c
        first_Image.image = UIImage(named: "Group 2604")
        pickup_Time_Lbl.text = time
        pickTime?(time)
    }
    
    @IBAction func drop_Picker(_ sender: UIDatePicker) {
        sender.datePickerMode =  UIDatePicker.Mode.time
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateFormat = "HH:mm"
        let time = formatter.string(from: sender.date)
        print(time) //Group 4575u
        second_Check_Image.image = UIImage(named: "Group 2604")
        drop_Time_Lbl.text = time
        dropTime?(time)
     }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func confirm_Btn(_ sender: ButtonDesign) {
        navigationController?.popViewController(animated: true)
    }
}
