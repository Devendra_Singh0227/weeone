//
//  Inspect_Car_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 13/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit
import TagListView

class Inspect_Car_ViewController: UIViewController, TagListViewDelegate, UITextViewDelegate {

    @IBOutlet weak var photos_CollectionView: UICollectionView!
    var callBack_Method :(() -> ())?
    lazy var Feddback_Array = ["Dirty Interior", "Wrong Input"]
    @IBOutlet weak var tags_View: TagListView!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var comment_TxtView: IQTextView!
    @IBOutlet weak var textView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var count_Lbl: UILabel!
    @IBOutlet weak var km_TxtField: UITextField!
    @IBOutlet weak var fuel_TxtField: UITextField!
    @IBOutlet weak var carName_Lbl: UILabel!
    @IBOutlet weak var carNumber_Lbl: UILabel!
    @IBOutlet weak var km_Lbl: UIView!
    var imageArray = [UIImage]()
    @IBOutlet weak var km_LblHeight: NSLayoutConstraint!
    var detailModel : VehicleDetailModel = VehicleDetailModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        comment_TxtView.delegate = self
        initialConfig()
        tags_View.addTags(Feddback_Array)
        tags_View.delegate = self
        
        photos_CollectionView.register(UINib(nibName: "Location_Photos_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Location_Photos_CollectionViewCell")
        
    }

    func initialConfig() {

        if detailModel.vehicle?.iotProvider == AppConstants.IOTProvider.WithTelemetric {
            km_Lbl.isHidden = true
            km_LblHeight.constant = 6
        }
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func textViewDidChange(_ textView: UITextView) {
          let height = ceil(textView.contentSize.height)

           if height != textView_HeightConstraint.constant {
               textView_HeightConstraint.constant = height
               textView.setContentOffset(CGPoint.zero, animated: false)
           }
       }
       
       func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
           let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
           let numberOfChars = newText.count
           if numberOfChars > 255 {
               
           } else {
               self.count_Lbl.text =  "\(textView.text.count)/255"
           }
           return numberOfChars < 256
       }
    
    @IBAction func update_Distance_Btn(_ sender: UIButton) {
        let vc = Update_DetailPopUp_ViewController()
        vc.headerTitle = "Update Distance"
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        vc.confirmClicked = { (value) in
            self.km_TxtField.text = value
        }
        UIViewController.current()?.present(vc, animated: true, completion: {
        })
    }
    
    @IBAction func update_Fuel_Btn(_ sender: UIButton) {
        let vc = Update_DetailPopUp_ViewController()
         vc.headerTitle = "Add Updated Fuel Value"
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        vc.confirmClicked = { (value) in
            self.fuel_TxtField.text = value + " %"
        }
            UIViewController.current()?.present(vc, animated: true, completion: {
        })
    }
    
    @IBAction func car_Picture_Btn(_ sender: UIButton) {
        openCustomCamera()
    }
    
    func showPickedImage(_ info: [UIImagePickerController.InfoKey : Any]) {
        addImages(info)
    }
    
    func addImages(_ info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage{
            imageArray.append(image)
            photos_CollectionView.reloadData()
//            self.imgProfile.image = image
//            self.isUploadImage = true
        }
    }
    
    func openCustomCamera() {
//        if ( UIImagePickerController.isSourceTypeAvailable(.camera)) {
            let vc = CustomeCameraVC.init(nibName: "CustomeCameraVC", bundle: nil)
            vc.isFromProfile = false
            vc.isImagePicked = { dictInfo in
                    self.showPickedImage(dictInfo)
            }
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
//        }
//        else {
//            let actionController: UIAlertController = UIAlertController(title: "KMsgInvalidCamera".localized, message: "", preferredStyle: .alert)
//            let cancelAction: UIAlertAction = UIAlertAction(title: "KLblOk".localized, style: .cancel) { action -> Void     in
//                //Just dismiss the action sheet
//            }
//
//            actionController.addAction(cancelAction)
//            actionController.popoverPresentationController?.sourceView = UIViewController.current().view
//            UIViewController.current().present(actionController, animated: true, completion: nil)
//        }
    }
    
    @IBAction func done_Btn(_ sender: UIButton) {
        
        let vc = FeedBackPop_ViewController()
        let first_String: [NSAttributedString.Key: Any] = [.foregroundColor: ColorScheme.kTextColorSecondary(), .font: UIFont(name: "Montserrat-Regular", size: 16)!]
        let firstOne = NSMutableAttributedString(string: "Thank you for inspecting car, 30 Weone credit added in your Weone wallet. ", attributes: first_String)
        vc.desc =  firstOne
        vc.cancelBtnClicked = {
//            self.callApiForInspectCar()
            self.callBack_Method?()
            self.navigationController?.popViewController(animated: true)
        }
        
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        
        if UIViewController.current()?.presentedViewController != nil {
            UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
            })
        }
        else{
            UIViewController.current()?.present(vc, animated: true, completion: {
            })
        }
    }
    
    func callApiForInspectCar(){
        
        let param : [String:Any] = ["frontSide" : "askfdlask.jpg",
                                    "leftSide"  : "askfdlask.jpg",
                                    "rightSide" : "askfdlask.jpg",
                                    "backSide"  : "askfdlask.jpg",
                                    "odoMeter"  : "askfdlask.jpg",
                                    "comment"   : "askfdlask.jpg",
                                    "vehicleId" : detailModel.vehicleId ?? 0,
                                    "createdBy" : ApplicationData.user?.id ?? 0,
                                    "kiloMeter" : 4,//km_TxtField.text!,
                                    "fuelLevel" : 7]//fuel_TxtField.text!]
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.InspectCar, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            print(response)
            self.callBack_Method?()
            self.navigationController?.popViewController(animated: true)
            if let dictResponse = response as? [String:Any] {
                self.detailModel = Mapper<VehicleDetailModel>().map(JSON: dictResponse)!
            }
            
        }) { (failureMessage, failureCode) in
            
        }
    }
}

extension Inspect_Car_ViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count//arrCollData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //let model = arrCollData[indexPath.row]
        let cell = photos_CollectionView.dequeueReusableCell(withReuseIdentifier: "Location_Photos_CollectionViewCell", for: indexPath) as! Location_Photos_CollectionViewCell
        cell.location_Image.image = imageArray[indexPath.row]
//        cell.setData(model, rideType: rideType, startDate: reservationStartDate, endDate: reservationEndDate)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: 70, height: 68)
    }
}
