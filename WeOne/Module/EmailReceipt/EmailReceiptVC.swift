//
//  CompanyProfileVC.swift
//  WeOne
//
//  Created by iMac on 11/06/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class EmailReceiptVC: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var viewNavigation: UIViewCommon!
    
    @IBOutlet weak var lblLine: UILabel!
    
    @IBOutlet weak var btnSave: UIButtonCommon!
    @IBOutlet weak var btnMonthly: UIButton!
    @IBOutlet weak var btnWeekly: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
        
    }
    
    //MARK: Private Methods
    func initialConfig() {
        
        viewNavigation.text = "KLblEmailForReceipt".localized as NSString
                
        let user = ApplicationData.user
        
//        txtEmail.text = user?.primaryEmail?.email
        txtEmail.tintColor = ColorConstants.ThemeColor

        viewNavigation.click = {
            self.btnClose_Click()
        }
        
        btnSave.click = {
            self.btnSave_Clicked()
        }
        
    }

    //MARK:- IBActions
    func btnClose_Click() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnWeekly_Click(_ sender: UIButton) {
        btnWeekly.isSelected = !btnWeekly.isSelected
    }
    
    @IBAction func btnMonthly_Click(_ sender: UIButton) {
        btnMonthly.isSelected = !btnMonthly.isSelected
    }
    
    func btnSave_Clicked() {

    }
    
}

//MARK: textField delegate methods
extension EmailReceiptVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

//        if !string.canBeConverted(to: String.Encoding.ascii){
//            return false
//        }
//
//        if self.textFiledType == .email || self.textFiledType == .password || self.textFiledType == .mobile {
//            if string == " " { return false }
//        }
//
//        if range.location == 0{
//
//            if string == " " {
//                return false
//            }
//        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
//        self.delegate?.shouldReturn(textField: textField)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        lblLine.backgroundColor = ColorConstants.UnderlineColor
//        self.setText(text: textField.text)
//
//        self.delegate?.didEndEditing(textField: textField)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        lblLine.backgroundColor = ColorConstants.ThemeColor
//
//        if self.textFiledType == .clickable{
//            self.delegate?.didBeginEditing(textField: textField)
//
//        }
    }
}
