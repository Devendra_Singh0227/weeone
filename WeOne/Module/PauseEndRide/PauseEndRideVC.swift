//
//  PauseEndRideVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 16/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit
import Reachability

class PauseEndRideVC: UIViewController {
    
    //MARK: - Variables
    var rideModel:RideListModel = RideListModel()
    
    var getNavigation: ((_ model : [[String:Any]]) -> ())?

    
    //MARK: - Outlets
    @IBOutlet weak var vwEndLocation: UIView!
    @IBOutlet weak var viewLock: UIView!
    @IBOutlet weak var viewInfoDetail: ShadowCard!
    @IBOutlet weak var viewInfo: ShadowCard!
    
    @IBOutlet weak var viewMain: ShadowCard!
    @IBOutlet weak var viewReport: ShadowCard!
    @IBOutlet weak var viewRiderCall: ShadowCard!
    @IBOutlet weak var viewNetwork: UIView!
    @IBOutlet weak var viewInstant: UIView!
    @IBOutlet weak var viewSchedule: UIView!

    @IBOutlet weak var lblTime: UILabel!
//    @IBOutlet weak var lblKm: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblCarInfo: UILabel!

//    @IBOutlet weak var imgCardType: UIImageView!
//    @IBOutlet weak var lblCardNo: UILabel!
    @IBOutlet weak var lblNetwork: UILabel!
    @IBOutlet weak var lblReturnTime: UILabel!
    @IBOutlet weak var lblGreetings: UILabel!
    
    @IBOutlet weak var lblScheduleGreetings: UILabel!
    @IBOutlet weak var lblScheduleReturnTime: UILabel!

    @IBOutlet weak var lblFuelPercentage: UILabel!

//    @IBOutlet weak var lblvw2Location: UILabel!
    @IBOutlet weak var btnEndRide: UIButtonCommon!
    @IBOutlet weak var btnLock: UIButton!
    @IBOutlet weak var btnUnLock: UIButton!

    @IBOutlet weak var imgNetwork: UIImageView!
    @IBOutlet weak var imgInfo: UIImageView!
    @IBOutlet weak var imgFuelType: UIImageView!

    @IBOutlet weak var constraintHeightViewMain: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomSearch: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightViewStack: NSLayoutConstraint!


    var totalViewHeight : CGFloat = 468
    
    var collaspeHeight: CGFloat = 325
    var mainViewHeight: CGFloat = 345
        
    var fullView: CGFloat = 100
    
    var isCollaspe = false
    
    let reachability = try! Reachability()

    var collaspeView: CGFloat {
        return UIScreen.main.bounds.height - collaspeHeight
    }
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        commonInit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareBackgroundView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: reachability)
    }
    
    //MARK: Private Methods
    func commonInit() {
        
        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(PauseEndRideVC.panGesture))
        gesture.delegate = self
        self.view.addGestureRecognizer(gesture)
        
        if self.rideModel.rideType  == AppConstants.RideType.Instant {
            self.mainViewHeight = ((self.constraintHeightViewMain.constant) - (143))
        } else {
            self.mainViewHeight = 341 - (143) - 38
            totalViewHeight = 468 - 60
        }
        
        self.fullView = AppConstants.ScreenSize.SCREEN_HEIGHT - totalViewHeight
                
        self.vwEndLocation.alpha = 1
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8, execute: {
            UIView.animate(withDuration: 0.6, animations: { [weak self] in
                let frame = self?.view.frame
                let _ = self?.collaspeView
                
                if self?.rideModel.rideType == AppConstants.RideType.Instant {

                        self?.view.frame = CGRect(x: 0, y: AppConstants.ScreenSize.SCREEN_HEIGHT - (frame?.height ?? 0), width: AppConstants.ScreenSize.SCREEN_WIDTH, height: frame?.height ?? 0)

                } else {

                    let frameHeight: CGFloat = 448 - 38.0
                        self?.view.frame = CGRect(x: 0, y: AppConstants.ScreenSize.SCREEN_HEIGHT - frameHeight, width: AppConstants.ScreenSize.SCREEN_WIDTH, height: frameHeight)

                }
                

            })
        })
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
                
        btnEndRide.click = {
            self.btnEndRide_Click(UIButton())
        }

        imgInfo.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Information"))
        btnLock.addTarget(self, action: #selector(holdDown(_:)), for: .touchDown)
        btnUnLock.addTarget(self, action: #selector(holdDown(_:)), for: .touchDown)

    }
    
    @objc func reachabilityChanged(note: Notification) {

      let reachability = note.object as! Reachability

        viewNetwork.isHidden = false

      switch reachability.connection {
      case .wifi, .cellular:
        imgNetwork.image = #imageLiteral(resourceName: "tick1")
        viewNetwork.backgroundColor = ColorConstants.TextColorGreen.withAlphaComponent(0.80)
        lblNetwork.text = "KLblConnected".localized

        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            self.viewNetwork.isHidden = true
        })

      case .unavailable:
        imgNetwork.image = #imageLiteral(resourceName: "info")
        viewNetwork.backgroundColor = ColorConstants.TextColorPrimary.withAlphaComponent(0.60)
        lblNetwork.text = "KLblNoNetworkConnection".localized

        print("Network not reachable")
      case .none:
        break
        }
    }

    
    func prepareBackgroundView(){
        
        viewMain.layer.cornerRadius = 20
        viewMain.backgroundColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        
        viewMain.layer.shadowColor = UIColor.black.withAlphaComponent(1.0).cgColor
        viewMain.layer.shadowOpacity = 0.3
        viewMain.layer.shadowOffset = CGSize(width: 0, height: -5)
        viewMain.layer.shadowRadius = 10.0
        viewMain.layer.masksToBounds = false
        viewMain.layer.shadowPath = CGPath(ellipseIn: CGRect(x: -20,y: -20 * 0.5,width: viewMain.layer.bounds.width + 20 * 2,height: 5),transform: nil)
    }
    
    func setData(_ model : RideListModel) {
        
        rideModel = model

        if model.estimateReturnLocation != nil{
       //     lblvw2Location.text = model.estimateReturnLocation?.street
        }

        
        lblCarInfo.text = "\(rideModel.vehicleId?.getCarName ?? "") | \(rideModel.vehicleId?.numberPlate ?? "")"
        
                let (distance, distanceUnit) = rideModel.getTotalDistance(isShortUnit: true)

        let (duration, _) = rideModel.getTotalDuration(isShortUnit: true)
//        lblTime.text = "Time \(TimeInterval(duration)?.getFormattedTime ?? "") | \(distance) \(distanceUnit)"
//
//        lblTime.changeStringColor(string: lblTime.text ?? "", array: ["| \(distance) \(distanceUnit)"], colorArray: [ColorConstants.TextColorSecondary])

        if rideModel.rideType == AppConstants.RideType.Instant {
     
        constraintHeightViewStack.constant = 60
        viewInstant.isHidden = false
        viewSchedule.isHidden = true

        
            lblTime.text = "\("KLblTime".localized) \(Utilities.getFormattedTime(min: rideModel.totalTime) ?? "")" + " | \(rideModel.totalKm) \("KLblKm".localized)"
                lblTime.changeStringColor(string: lblTime.text ?? "", array: [" | \(rideModel.totalKm) \("KLblKm".localized)"], colorArray: [ColorConstants.TextColorSecondary])
        } else {
//            constraintBottomSearch.constant = 24
            constraintHeightViewStack.constant = 0
            viewInstant.isHidden = true
            viewSchedule.isHidden = true

        }
        
//        lblKm.text = "Distance \(distance) \(distanceUnit)"
        
//        lblCardNo.text = rideModel.selectedCard?.getFormattedCardNumber()
        lblPrice.text = "Total \(rideModel.getTotalFare())"
        
        lblReturnTime.text = DateUtilities.convertStringDateintoStringWithFormat(dateStr: rideModel.estimatedReturnTime ?? "", destinationFormat: DateUtilities.DateFormates.kRideDateFormat)
        
        lblScheduleReturnTime.text = lblReturnTime.text
        if model.vehicleId?.iotProvider == AppConstants.IOTProvider.WithTelemetric {
            viewLock.isHidden = false
        } else {
            viewLock.isHidden = true

        }
        let user = ApplicationData.user

        lblGreetings.text = "\(DateUtilities.getTimeOfTheDay()), \(user?.firstName ?? "") "
        
        lblScheduleGreetings.text = lblGreetings.text
        
        if rideModel.vehicleId?.fuelType == AppConstants.FuelType.Combustion{
            imgFuelType.image = UIImage(named : "fuel")
        }else{
            imgFuelType.image = UIImage(named : "electric-station")
        }
        
        lblFuelPercentage.text = "\(rideModel.vehicleId?.fuelLevelStr ?? "")"


    }
    
    
    
    func btnEndRide_Click(_ sender: UIButton) {
        
            self.view.showConfirmationPopupWithMultiButton(title: "KLblEndRide".localized, message: "KLblEndRideConfirmationMsg".localized, cancelButtonTitle: "KLblCancelCap".localized, confirmButtonTitle: "KLblEndRideCap".localized, onConfirmClick: {
                // Confirm Click
                
                let vc = UnlockCarVC(nibName: "UnlockCarVC", bundle: nil)
                vc.attachmentReason = .EndRide
                vc.rideModel = self.rideModel
                UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
                
            }) {
                // cancel click
            }

        
    }
    
    @objc func holdDown(_ sender: UIButton) {

        if sender.tag == 0 {
            btnLock.backgroundColor = #colorLiteral(red: 0.9647058824, green: 0.3803921569, blue: 0.8745098039, alpha: 1)
        } else {
            btnUnLock.backgroundColor = #colorLiteral(red: 0.4901960784, green: 0.9490196078, blue: 0.7843137255, alpha: 1)
        }
        
    }
    
    @IBAction func btnExpandCollapse_Click(_ sender: UIButton) {
        if isCollaspe {
            
            self.vwEndLocation.alpha = 1
            
//            self.constraintHeightViewMain.constant = self.mainViewHeight + self.vwEndLocation.frame.height
            
            self.constraintHeightViewMain.constant = 341//323

            if self.rideModel.rideType == AppConstants.RideType.Instant {
                self.view.frame = CGRect(x: 0, y: AppConstants.ScreenSize.SCREEN_HEIGHT - 468, width: self.view.frame.width, height: 468)

            } else {
                let frameHeight: CGFloat = 410.0
                
                self.constraintHeightViewStack.constant = 0
                self.viewInstant.isHidden = true
                self.viewSchedule.isHidden = true

                    self.view.frame = CGRect(x: 0, y: AppConstants.ScreenSize.SCREEN_HEIGHT - frameHeight, width: AppConstants.ScreenSize.SCREEN_WIDTH, height: frameHeight)

            }

            
        } else {
            
            self.vwEndLocation.alpha = 0
            
            if self.rideModel.rideType == AppConstants.RideType.Instant {
                
                self.constraintHeightViewMain.constant = 323 - 143

                self.constraintHeightViewStack.constant = 60
                self.viewInstant.isHidden = false
                self.viewSchedule.isHidden = true

                self.view.frame = CGRect(x: 0, y: AppConstants.ScreenSize.SCREEN_HEIGHT - (self.view.frame.height - (self.vwEndLocation.frame.height)), width: AppConstants.ScreenSize.SCREEN_WIDTH, height: self.view.frame.height - (self.vwEndLocation.frame.height ))

            } else {
                
                self.constraintHeightViewMain.constant = 323

                self.constraintHeightViewStack.constant = 80
                self.viewInstant.isHidden = true
                self.viewSchedule.isHidden = false
                   
                self.view.frame = CGRect(x: 0, y: AppConstants.ScreenSize.SCREEN_HEIGHT - (448 - 90), width: AppConstants.ScreenSize.SCREEN_WIDTH, height: 448 - 90)

                
            }
        }
        
        isCollaspe = !isCollaspe
    }

    @IBAction func btnLock_Click(_ sender: UIButton) {
        btnLock.backgroundColor = ColorConstants.ThemeColor

        callApiForLockUnLock(rideModel, isLock: true)
        
    }
    
    @IBAction func btnUnLock_Click(_ sender: UIButton) {
        btnUnLock.backgroundColor = ColorConstants.TextColorGreen

        callApiForLockUnLock(rideModel, isLock: false)
    }

    @IBAction func btnInfo_Click(_ sender: UIButton) {
        viewInfoDetail.isHidden = !viewInfoDetail.isHidden
        viewInfo.isHidden = !viewInfo.isHidden
    }
    
    @IBAction func btnChat_Click(_ sender: UIButton) {
        
    }

    @IBAction func btnSupport_Click(_ sender: UIButton) {
        let vc = SupportVC()
        UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func btnClose_Click(_ sender: UIButton) {
        viewInfoDetail.isHidden = !viewInfoDetail.isHidden
        viewInfo.isHidden = !viewInfo.isHidden
    }

    
    @IBAction func btnReport_Click(_ sender: UIButton) {
        
        let vc = ReportDamageVC(nibName: "ReportDamageVC", bundle: nil)
        vc.rideModel = rideModel
        UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func btnNavigate_Click(_ sender: UIButton) {
        
        if rideModel.estimateEndLocation != nil{
            Utilities.openMap(latitude: rideModel.estimateEndLocation?.latitude ?? 0.0, longitude: rideModel.estimateEndLocation?.longitude ?? 0.0)
        } else
            
            if rideModel.endLocation != nil{
                Utilities.openMap(latitude: rideModel.endLocation?.latitude ?? 0.0, longitude: rideModel.endLocation?.longitude ?? 0.0)
        }else
            
            if rideModel.estimateReturnLocation != nil{
                Utilities.openMap(latitude: rideModel.estimateReturnLocation?.latitude ?? 0.0, longitude: rideModel.estimateReturnLocation?.longitude ?? 0.0)
        }
    }
        
    @IBAction func btnDestination_Click(_ sender: UIButton) {
        
        let vc = PlacePickerVC(nibName: "PlacePickerVC", bundle: nil)
        
        vc.rideModel = rideModel
        
        vc.getDestination = { arrNavigateLocations in
            self.callApiForUpdateLocation(arrNavigateLocations)
        }
        
        UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
        
    }
}

//MARK: Api call
extension PauseEndRideVC{
    
    func callApiForUpdateLocation(_ model : [[String:Any]]){
        
        var param : [String:Any] = [:]
        param["rideId"] = rideModel.id ?? ""
        param["navigateLocations"] = model
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.UpdateNavigateLocation, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any]{
                
              //  self.viewNavigate.isHidden = false
                let rideModel = Mapper<RideListModel>().map(JSON: dictResponse)!
                
                SyncManager.sharedInstance.activeRideObject = rideModel
                self.setData(rideModel)
                self.getNavigation?(model)
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func callApiForLockUnLock(_ model : RideListModel, isLock: Bool){
        
        var param : [String:Any] = [:]
        param["id"] = rideModel.id
        param["qnr"] = rideModel.vehicleId?.qnr ?? ""
        param["command"] = AppConstants.Command.CENTRAL_LOCK
        
        if isLock {
            param["operation"] = AppConstants.Operation.LOCK
        } else {
            param["operation"] = AppConstants.Operation.UNLOCK
        }


        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.InverseLockUnlock, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any]{
                self.viewNetwork.isHidden = false
                
                self.imgNetwork.image = #imageLiteral(resourceName: "tick1")
                self.viewNetwork.backgroundColor = ColorConstants.ThemeColor.withAlphaComponent(0.80)

                if isLock {
                    self.lblNetwork.text = "KLblCloseDoor".localized
                    self.btnLock.isUserInteractionEnabled = false
                    self.btnUnLock.isUserInteractionEnabled = true

                } else {
                    self.lblNetwork.text = "KLblOpenDoor".localized
                    self.btnUnLock.isUserInteractionEnabled = false
                    self.btnLock.isUserInteractionEnabled = true
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                    self.viewNetwork.isHidden = true
                })

            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }

}

//MARK: - UIGesture Recognizer Delegate's Methods
extension PauseEndRideVC : UIGestureRecognizerDelegate {
    
    @objc func panGesture(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self.view)
        let velocity = recognizer.velocity(in: self.view)
        
        
        let y = self.view.frame.minY
        
        
        if (y + translation.y >= fullView) && (y + translation.y <= collaspeView) {
            self.view.frame = CGRect(x: 0, y: y + translation.y, width: self.view.frame.width, height: totalViewHeight)
            recognizer.setTranslation(CGPoint.zero, in: self.view)
            
        }
        
        if recognizer.state == .changed {
            
            if recognizer.direction == Direction.up {
                
                if constraintHeightViewMain.constant >= totalViewHeight {
                    return
                }
                
                let alpha = (-translation.y - 334)/totalViewHeight
//                self.vwEndLocation.alpha = alpha
                
            } else {
                
                if constraintHeightViewMain.constant <= collaspeHeight ||  constraintHeightViewMain.constant >= totalViewHeight {
                    return
                }
                
                let alpha = (-translation.y + 334)/334
//                self.vwEndLocation.alpha = alpha - 0.37
                
            }
        }
        
        if recognizer.state == .ended || recognizer.state == .cancelled || recognizer.state == .failed{
            
            var duration =  velocity.y < 0 ? Double((y - fullView) / -velocity.y) : Double((collaspeView - y) / velocity.y )
            
            duration = duration > 1.3 ? 1 : duration
            
            UIView.animate(withDuration: duration, delay: 0.0, options: [.allowUserInteraction], animations: {
                if  velocity.y >= 0 && (y + translation.y) > self.fullView {
                    
                    self.isCollaspe = true
                    
                    self.vwEndLocation.alpha = 0
                    
                    if self.rideModel.rideType == AppConstants.RideType.Instant {
                        self.constraintHeightViewMain.constant = 323 - 143

                        self.constraintHeightViewStack.constant = 60
                        self.viewInstant.isHidden = false
                        self.viewSchedule.isHidden = true

                        self.view.frame = CGRect(x: 0, y: AppConstants.ScreenSize.SCREEN_HEIGHT - (self.view.frame.height - (self.vwEndLocation.frame.height)), width: AppConstants.ScreenSize.SCREEN_WIDTH, height: self.view.frame.height - (self.vwEndLocation.frame.height ))
                        
                    } else {
                        
                        self.constraintHeightViewMain.constant = 323

                        self.constraintHeightViewStack.constant = 80
                        self.viewInstant.isHidden = true
                        self.viewSchedule.isHidden = false
                           
                        self.view.frame = CGRect(x: 0, y: AppConstants.ScreenSize.SCREEN_HEIGHT - (448 - 90), width: AppConstants.ScreenSize.SCREEN_WIDTH, height: 448 - 90)
                        
                    }
                    
                    
                } else {
                    
                    self.isCollaspe = false
                    
                    self.vwEndLocation.alpha = 1
                    
                    self.constraintHeightViewMain.constant = 341//323
                    
                    if self.rideModel.rideType == AppConstants.RideType.Instant {
                        
                        self.constraintHeightViewStack.constant = 60
                        self.view.frame = CGRect(x: 0, y: AppConstants.ScreenSize.SCREEN_HEIGHT - 468, width: AppConstants.ScreenSize.SCREEN_WIDTH, height: 468)

                    } else {
                        let frameHeight: CGFloat = 410.0
                        
                        self.constraintHeightViewStack.constant = 0
                        self.viewInstant.isHidden = true
                        self.viewSchedule.isHidden = true

                            self.view.frame = CGRect(x: 0, y: AppConstants.ScreenSize.SCREEN_HEIGHT - frameHeight, width: AppConstants.ScreenSize.SCREEN_WIDTH, height: frameHeight)

                    }
                    
                }
                
            }, completion: { _ in
                if ( velocity.y < 0 ) {
                    // self?.collView.isScrollEnabled = true
                }
            })
        }
    }
}
