//
//  ReserveRideVC.swift
//  WeOne
//  Created by Coruscate Mac on 13/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class ReserveRideVC: UIViewController {
    
    //MARK: variables
    var arrRides : [RideListModel] = [RideListModel]()

    var timer : Timer = Timer()
    var seconds : Int = 0
    
    var currentViewModel : RideListModel?
    var currentCarousolIndex : Int = 0
    
    //Return blocks
    var swipeDetected : ((Int) -> ())?
    var refreshAllRides : (() -> ())?
    var reserveRideCompleted : (() -> ())?
    
    var currentNavigationButton : (() -> ())?
    var selectNavigation : (() -> ())?
    var onConfirm_Click:(()->())?
    var onStartRide_Click:(()->())?
    var currentLocation: CLLocation = CLLocation()

    //MARK: outlets
//    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var collExpandView: UICollectionView!
    
//    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewInfoDetail: UIView!
    
    @IBOutlet weak var imgInfo: UIImageView!

    @IBOutlet weak var viewInfo: ShadowCard!
    
    
//    @IBOutlet weak var constraintHeightViewMain: NSLayoutConstraint!
//    @IBOutlet weak var constraintViewContainerHeight: NSLayoutConstraint!
//
//    @IBOutlet weak var constraintCollViewHeight: NSLayoutConstraint!
//    @IBOutlet weak var constraintCollExpandViewHeight: NSLayoutConstraint!
    
    var totalViewHeight : CGFloat = 500
    var collaspeHeight: CGFloat = 466
    var fullView: CGFloat = 100
    
    var instantRideHight: CGFloat = 410
    var scheduleRideHight: CGFloat = 500
    
    var collaspeView: CGFloat {
        return UIScreen.main.bounds.height - collaspeHeight
    }
    
    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        commonInit()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareBackgroundView()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can beReserveRideViewCell recreated.
    }
    
    //MARK: Private Methods
    func commonInit() {
        
        imgInfo.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Information"))
        
        collExpandView.register(UINib(nibName: "ReserveRideViewCell", bundle: nil), forCellWithReuseIdentifier: "ReserveRideViewCell")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8, execute: {
            UIView.animate(withDuration: 0.6, animations: { [weak self] in
                let yComponent = AppConstants.ScreenSize.SCREEN_HEIGHT - (self?.view.frame.height ?? 466)
                self?.view.frame = CGRect(x: 0, y: yComponent, width: AppConstants.ScreenSize.SCREEN_WIDTH, height: self?.view.frame.height ?? 466)
            })
        })
    }
    
    func prepareBackgroundView(){
        
        viewMain.layer.cornerRadius = 20
        
        viewMain.layer.shadowColor = UIColor.black.withAlphaComponent(1.0).cgColor
        viewMain.layer.shadowOpacity = 0.3
        viewMain.layer.shadowOffset = CGSize(width: 0, height: -5)
        viewMain.layer.shadowRadius = 10.0
        viewMain.layer.masksToBounds = false
        viewMain.layer.shadowPath = CGPath(ellipseIn: CGRect(x: -20,
                                                             y: -20 * 0.5,
                                                             width: viewMain.layer.bounds.width + 20 * 2,
                                                             height: 5),
                                           transform: nil)
        
    }
    
    //MARK: Button action methods
    @IBAction func btnStartRide_Click(index: Int){
        
        UIViewController.current().view.showConfirmationPopupWithMultiButton(title: "KLblStartRide".localized, message: "KLblStartRideMsg".localized, cancelButtonTitle: "KLblCancelCap".localized, confirmButtonTitle: "KLblConfirmCap".localized, onConfirmClick: {
            
            var param : [String:Any] = [:]
            
            if self.arrRides.count > 0{
                param["rideId"] = self.arrRides[index].id
            }else{
                param["rideId"] = self.currentViewModel?.id
            }
                
            SyncManager.sharedInstance.startRide(reqDict: param, success: { (response) in
                // success
                self.invalidateTimer()
                self.onStartRide_Click?()
                
            }) {
                
            }

        }) {
            
        }

    }
    
    @IBAction func btnInfo_Click(_ sender: UIButton) {
        viewInfoDetail.isHidden = !viewInfoDetail.isHidden
        viewInfo.isHidden = !viewInfo.isHidden

    }
    
    @IBAction func btnChat_Click(_ sender: UIButton) {
        
    }

    @IBAction func btnSupport_Click(_ sender: UIButton) {
        
    }

    @IBAction func btnClose_Click(_ sender: UIButton) {
        viewInfoDetail.isHidden = !viewInfoDetail.isHidden
        viewInfo.isHidden = !viewInfo.isHidden
    }

    
    @IBAction func btnReport_Click(_ sender: UIButton) {
        
        if let arrRides = SyncManager.sharedInstance.arrReserveRides, arrRides.count > 0{
            
            let rideModel = RideListModel()
            rideModel.vehicleId = Mapper<VehicleModel>().map(JSON: arrRides[currentCarousolIndex].vehicleId?.getDict() ?? [:])!
            
            let vc = ReportDamageVC(nibName: "ReportDamageVC", bundle: nil)
            vc.rideModel = arrRides[0]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func btnCurrentLocationReserve_Click(_ sender: UIButton) {
        currentNavigationButton?()
    }
    
    @IBAction func btnChangeCar_Click(_ sender: UIButton) {
    }
    
    
    @IBAction func btnCancelReservation_Click(index: Int) {
        
        let model = self.arrRides[index]
        
        if model.rideType == AppConstants.RideType.Instant {
            UIViewController.current().view.showConfirmationPopupWithMultiButton(title: "KLblCancelReservation".localized, message: "KLblCancelRideMsg".localized, cancelButtonTitle: "KLblCancelCap".localized, confirmButtonTitle: "KLblConfirmCap".localized, onConfirmClick: {
                
                var param : [String:Any] = [String:Any]()
                param["rideId"] = model.id ?? ""
                
                self.callApiForCancelReservation(param)
                
            }) {
                
            }
        } else {
            let vc = CancellationDetailVC(nibName: "CancellationDetailVC", bundle: nil)
            vc.rideModel = model
            vc.onCancel = {
                self.onConfirm_Click?()
            }
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func btnExtend_Click(_ sender: UIButton) {
        
        self.callApiForExtendReservationTime(index: sender.tag)
            
    }
    
    @objc func btnKeyHandaling_Click(_ sender: UIButton) {
        
        let model = arrRides[sender.tag]

        let vc = KeyHandlingPopupVC(nibName: "KeyHandlingPopupVC", bundle: nil)
        
        vc.model = model.keyHandling
        
        vc.headerTitle = "KKeyHandling".localized
        vc.isFromReserve = true
        
        vc.confirmClicked = {
           // self.bookNow()
        }
        
        vc.showOnMapClicked = {
            
            let vc = SetLocationVC()
            
            vc.geoLocation = self.arrRides.first?.geoLocation
            vc.isFromKeyHandling = true
            vc.getLocation = { placeModel in

            }
            self.navigationController?.pushViewController(vc, animated: true)

            
            //            self.dismiss(animated: true, completion: nil)
        }
        
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        
        if UIViewController.current()?.presentedViewController != nil {
            UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
            })
        }
        else{
            UIViewController.current()?.present(vc, animated: true, completion: {
            })
        }

        
    }
    
    @objc func btnNavigate_Click(_ sender: UIButton) {
        selectNavigation?()
    }
    
    @objc func btnDestination_Click(_ sender: UIButton) {
      /*  let vc = PlacePickerVC(nibName: "PlacePickerVC", bundle: nil)
        vc.rideModel = currentViewModel!
        vc.getDestination = { model in
            self.callApiForUpdateLocation(model, isSourceLocation: isSource)
        }
        UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)*/
        
    }
}

//MARK: SetData
extension ReserveRideVC{
    
    func setData(_ model : RideListModel){
        
        setDataForExpandedView(model)
        
        collExpandView.reloadItems(at: [IndexPath(item: currentCarousolIndex, section: 0)])
    }
    
    func setAllReserveRides(_ arr : [RideListModel],model: RideListModel = RideListModel(), isFromExtend: Bool = false, index: Int = 0){
        
        self.arrRides = arr
        
        if isFromExtend {
            
            setDataForExpandedView(model, isFromExtend: true)
            
            collExpandView.reloadItems(at: [IndexPath(item: index, section: 0)])
        } else {
            
            setDataForExpandedView(arr.first, isFromExtend: true)
            collExpandView.reloadData()
        }
    }
    
    func setDataForExpandedView(_ model : RideListModel?, isFromExtend: Bool = false ){
        
        currentViewModel = model
        
        if model?.rideType == AppConstants.RideType.Instant {
            totalViewHeight = instantRideHight
        } else {
            totalViewHeight = scheduleRideHight
        }
        
        
//        totalViewHeight = totalViewHeight + Utilities.getLabelHeight(constraintedWidth: AppConstants.ScreenSize.SCREEN_WIDTH -  32, font: FontScheme.kSemiBoldFont(size: 14), text: model?.vehicleId?.geoLocation?.name ?? "")
        
        self.fullView = AppConstants.ScreenSize.SCREEN_HEIGHT //- totalViewHeight
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        if appDelegate.isShowKeyHandaling {
            appDelegate.isShowKeyHandaling = false 
            if model?.vehicleId?.iotProvider != AppConstants.IOTProvider.WithTelemetric {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.8, execute: {
                    self.btnKeyHandaling_Click(UIButton())
                })
            }
        }
    }
    
    func invalidateTimer(){
        self.timer.invalidate()
        
        if let cell = self.collExpandView.cellForItem(at: IndexPath(row: 0, section: 0)) as? ReserveRideViewCell{
            if cell.reservationTimer.isValid {
                cell.reservationTimer.invalidate()
            }
            if cell.waitingTimer.isValid {
                cell.waitingTimer.invalidate()
            }
        }
        
    }
}


//MARK: collectionview delegate methods
extension ReserveRideVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrRides.count > 0{
            return arrRides.count
        }
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReserveRideViewCell", for: indexPath) as! ReserveRideViewCell
        cell.setCellData(arrRides[indexPath.row], isExpand: true)
        
        
        cell.btnStartRide.click = {
            self.btnStartRide_Click(index: indexPath.row)
        }
        
        cell.btnCancel.click = {
            self.btnCancelReservation_Click(index: indexPath.row)
        }
        
        
        cell.btnCancel.tag = indexPath.row
        cell.btnStartRide.tag = indexPath.row
        
        
        cell.btnSelectDestination.addTarget(self, action: #selector(btnDestination_Click(_:)), for: .touchUpInside)
        cell.btnNevigate.addTarget(self, action: #selector(btnNavigate_Click(_:)), for: .touchUpInside)
        cell.btnExtendTime.addTarget(self, action: #selector(btnExtend_Click(_:)), for: .touchUpInside)
        cell.btnKeyHandling.addTarget(self, action: #selector(btnKeyHandaling_Click(_:)), for: .touchUpInside)
        
        cell.btnSelectDestination.tag = indexPath.row
        cell.btnNevigate.tag = indexPath.row
        cell.btnExtendTime.tag = indexPath.row
        cell.btnKeyHandling.tag = indexPath.row
        
        cell.timerCompleted = {
            self.reserveRideCompleted?()
            self.invalidateTimer()
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            var visibleRect = CGRect()
            
            visibleRect.origin = collExpandView.contentOffset
            visibleRect.size = collExpandView.bounds.size
            
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            
            guard let indexPath = collExpandView.indexPathForItem(at: visiblePoint) else { return }
            
            let model = arrRides[indexPath.row]
            currentViewModel = model
            
            if currentViewModel?.rideType == AppConstants.RideType.Instant {
                totalViewHeight = instantRideHight
            } else {
                totalViewHeight = scheduleRideHight
            }

            UIView.animate(withDuration: 0.5, delay: 0.0, options: [.allowUserInteraction], animations: {
                self.view.frame = CGRect(x: 0, y: AppConstants.ScreenSize.SCREEN_HEIGHT - self.totalViewHeight, width: self.view.frame.width, height: self.totalViewHeight)
                
            })
            
            self.swipeDetected?(indexPath.row)
            
            currentCarousolIndex = indexPath.row
            
        }
}

//MARK: Api calls
extension ReserveRideVC{
    
    func callApiForExtendReservationTime(index: Int){
        
        var param : [String:Any] = [:]
        param["rideId"] = self.currentViewModel?.id
        param["timeLimit"] = self.currentViewModel?.partnerSetting?.extendTimeLimit ?? 0
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.ExtendReservation, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any]{
                
                let model = (Mapper<RideListModel>().map(JSON: dictResponse)!)
                SyncManager.sharedInstance.syncMaster({
                    if self.arrRides.count > 0{
                        if SyncManager.sharedInstance.arrReserveRides?.count ?? 0 > 0 {
                            self.setAllReserveRides(SyncManager.sharedInstance.arrReserveRides!, model: model, isFromExtend: true, index: index)
                        }
                    }
                }) {
                    
                }
                
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func callApiForCancelReservation(_ param : [String:Any]){
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.CancelRide, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any]{
                
                if SyncManager.sharedInstance.activeRideObject != nil{
                    SyncManager.sharedInstance.activeRideObject = Mapper<RideListModel>().map(JSON: dictResponse)!
                    self.invalidateTimer()
                    self.onConfirm_Click?()
                } else {
                    
                    SyncManager.sharedInstance.syncMaster ({
                        
                        self.refreshAllRides?()
                        self.invalidateTimer()
                        self.onConfirm_Click?()
                        
                    })
                }
            }
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func callApiforFindCar(){
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.FindCar, method: .post, parameters: ["rideId" : currentViewModel?.id ?? ""], headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any]{
                
                if let dictLocation = dictResponse["geoLocation"] as? [String:Any]{
                    
                    let model = Mapper<LocationModel>().map(JSON: dictLocation)!
                    Utilities.openMap(latitude: model.latitude, longitude: model.longitude)
                }
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func callApiForUpdateLocation(_ model : PlacesModel, isSourceLocation: Bool){
        
        var param : [String:Any] = [:]
        param["rideId"] = currentViewModel?.id ?? ""
        
        
        var location : [String:Any] = [:]
        location["type"] = "point"
        location["name"] = model.addressFullText
        location["coordinates"] = [model.longitude ?? 0, model.latitude ?? 0]
        
        var command = AppConstants.URL.UpdateEndLocation
        
        if isSourceLocation {
            command = AppConstants.URL.UpdateStartLocation
            param["startLocation"] = location
        } else {
            param["estimateEndLocation"] = location
        }
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: command, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any]{
                
                let rideModel = Mapper<RideListModel>().map(JSON: dictResponse)!
                
                let index = SyncManager.sharedInstance.arrReserveRides?.firstIndex(where: { (model) -> Bool in
                    return model.id == self.currentViewModel?.id
                })
                
                if index != nil {
                    var rides = SyncManager.sharedInstance.arrReserveRides!
                    rides[index!] = rideModel
                    
                    self.arrRides = rides
                    
                    self.totalViewHeight = self.scheduleRideHight
                    
                   // self.totalViewHeight = self.totalViewHeight + Utilities.getLabelHeight(constraintedWidth: //AppConstants.ScreenSize.SCREEN_WIDTH -  32, font: FontScheme.kSemiBoldFont(size: 14), text: rideModel.vehicleId?.geoLocation?.name ?? "")
                    
                    
                    self.collExpandView.reloadItems(at: [IndexPath(item: index ?? 0, section: 0)])
                    self.setDataForExpandedView(rideModel)
                    
                }
                
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
}
