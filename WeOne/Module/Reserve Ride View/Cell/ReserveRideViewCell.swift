//
//  ReserveRideViewCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 13/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class ReserveRideViewCell: UICollectionViewCell {
    
   
    @IBOutlet weak var viewNavigate: UIView!
    @IBOutlet weak var viewSchedule: UIView!

    @IBOutlet weak var viewTime: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewKeyHandling: UIView!

    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var lblCarInfo: UILabel!
    @IBOutlet weak var lblFuelPercentage: UILabel!
   
  //  @IBOutlet weak var lblDestination: UILabel!
    @IBOutlet weak var lblPricingPackage: UILabel!
    @IBOutlet weak var lblTimerMessage: UILabel!

    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var imgFuelType: UIImageView!
    @IBOutlet weak var imgNevigate: UIImageView!

    @IBOutlet weak var btnSelectDestination: UIButton!
    @IBOutlet weak var btnNevigate: UIButton!
    @IBOutlet weak var btnExtendTime: UIButton!
    @IBOutlet weak var btnKeyHandling: UIButton!

    @IBOutlet weak var btnStartRide: UIButtonCommon!
    @IBOutlet weak var btnCancel: UIButtonCommon!

    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var lblFromTime: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
    @IBOutlet weak var viewDashedBorder: UIView!

    
    @IBOutlet weak var constraintKeyHandlingWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintScheduleHeigh: NSLayoutConstraint!

    var reservationTimer : Timer = Timer()
    var waitingTimer : Timer = Timer()

    var seconds : Int = 0
    var activeRideModel : RideListModel?
    var timerCompleted : (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setCellData(_ model : RideListModel, isExpand: Bool){
        
        imgNevigate.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "gps"))

        btnCancel.setBackGroundColor(color: ColorConstants.TextColorSecondary)
        btnCancel.isCancel = true 
        activeRideModel = model
        
        
        viewMain.roundCorners([.topLeft, .topRight], radius: 20, width: AppConstants.ScreenSize.SCREEN_WIDTH)
        
        if model.rideType == AppConstants.RideType.Instant{
           
            viewSchedule.isHidden = true
            viewTime.isHidden = false
            
            if let priceConfig = model.pricingConfigData{
                lblPricingPackage.text = String(format : "\(Utilities.convertToCurrency(number: priceConfig.rate, currencyCode: priceConfig.currency)) \(priceConfig.getFormattedUnit)")

            }
            
        } else {
            
            lblFromDate.text = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: model.startDateTime ?? ""), format: DateUtilities.DateFormates.kRegularDate)
            
            lblFromTime.text = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: model.startDateTime ?? ""), format: DateUtilities.DateFormates.kScheduleTime)

            
            lblEndDate.text = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: model.endDateTime ?? ""), format: DateUtilities.DateFormates.kRegularDate)
            
            lblEndTime.text = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: model.endDateTime ?? ""), format: DateUtilities.DateFormates.kScheduleTime)

            viewDashedBorder.addDashedBorderVertical(color: ColorConstants.ThemeColor,isVertical: false)

            imgCar.alpha = 1
            viewTime.isHidden = true
            
            viewSchedule.isHidden = false
            constraintScheduleHeigh.constant = 80
            
            if let priceConfig = model.pricingConfigData{
                lblPricingPackage.text = priceConfig.desc ?? ""
            }
        }
        
        if model.rideType == AppConstants.RideType.Instant {
            startTimer(model)
        }
        
//        lblCarInfo.fadeTransition(0.4)
//        imgCar.fadeTransition(0.4+)
//        lblPricingPackage.fadeTransition(0.4)

        imgCar.setImageForURL(url: model.vehicleId?.getVehicleImage, placeHolder: UIImage(named : "carPlaceholder"))
        
        if model.estimateReturnLocation != nil{
            viewNavigate.isHidden = false
            btnSelectDestination.setTitle(model.geoLocation?.street ?? "", for: .normal)
        }
            lblCarInfo.text = "\(model.vehicleId?.getCarName ?? "") | \(model.vehicleId?.numberPlate ?? "")"
        
        
        if model.vehicleId?.fuelType == AppConstants.FuelType.Combustion{
            imgFuelType.image = UIImage(named : "fuel")
        }else{
            imgFuelType.image = UIImage(named : "electric-station")
        }
        
        lblFuelPercentage.text = "\(model.vehicleId?.fuelLevelStr ?? "")"
        
        if model.vehicleId?.iotProvider == AppConstants.IOTProvider.WithTelemetric {
            viewKeyHandling.isHidden = true
            constraintKeyHandlingWidth.constant = 0
        }
        
        
    }
    
    func startTimer(_ model : RideListModel){
        
        seconds =  Int(DateUtilities.convertDateFromServerString(dateStr: model.reservedEndDateTime ?? "", sourceFormate: DateUtilities.DateFormates.kMainSourceFormat).timeIntervalSince(Date()))
        
        btnExtendTime.isHidden = true
        lblTimerMessage.text = "KLblGoToCar".localized
        reservationTimer.invalidate()
        reservationTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerFired), userInfo: nil, repeats: true)

        lblTimer.text = TimeInterval(seconds).formattedTime
    }
    
    @objc func timerFired(_ timer : Timer) {
        seconds -= 1
        
        if seconds > 0{
            lblTimer.text = TimeInterval(seconds).formattedTime
        } else {
            timer.invalidate()
            startWaitingTimer()
        }
    }
   
    
    func startWaitingTimer() {
        
        seconds = 60
         
        lblTimer.text = TimeInterval(seconds).formattedTime

        reservationTimer.invalidate()
        lblTimerMessage.text = "KLblWaitingTime".localized
        lblTimer.isHidden = true
        btnExtendTime.isHidden = false
        waitingTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(waitingTimerFired), userInfo: nil, repeats: true)
    }
    
    func startReservationTimer() {
        lblTimer.text = TimeInterval(seconds).formattedTime

        reservationTimer.invalidate()
        lblTimerMessage.text = "KLblWaitingTime".localized
        lblTimer.isHidden = true
        btnExtendTime.isHidden = false
        waitingTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(waitingTimerFired), userInfo: nil, repeats: true)
    }

    @objc func waitingTimerFired(_ timer : Timer) {
        seconds -= 1
        
        if seconds == 0{
            timer.invalidate()
            waitingTimer.invalidate()
            timerCompleted?()
        }
    }
    
}
