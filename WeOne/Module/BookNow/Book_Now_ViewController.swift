//
//  Book_Now_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 06/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Book_Now_ViewController: UIViewController {

    @IBOutlet weak var car_pricing_View: ViewDesign!
    @IBOutlet weak var fuel_Info_View: ViewDesign!
    @IBOutlet weak var info_View: ViewDesign!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var lblVehicleName: UILabel!
    @IBOutlet weak var lblPricePerDay: UILabel!
    @IBOutlet weak var lblVehicleNumber: UILabel!
    @IBOutlet weak var lblVehicleAvailable: UILabel!
    @IBOutlet weak var vehicleDistance_Lbl: UILabel!
    @IBOutlet weak var totalPrice_Lbl: UILabel!
    @IBOutlet weak var pay_Btn: ButtonDesign!
    @IBOutlet weak var personal_card_Lbl: UILabel!
    @IBOutlet weak var business_Card_Lbl: UILabel!
    var detailModel : VehicleDetailModel = VehicleDetailModel()
    var rideType = AppConstants.RideType.Schedule
    var strVehicleId : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        callApiForCarDetail()
        //initialConfig()
    }

    func initialConfig() {
        
//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
        Defaults[.vehicleId] = detailModel.vehicle?.id ?? ""
        let url = URL(string: "\(AppConstants.imageURL)\(detailModel.vehicle?.images.first ?? "")")
        carImage.setImageForURL(url: url, placeHolder: UIImage(named: "carPlaceholder"))
        lblVehicleName.text = detailModel.vehicle?.name ?? ""
        lblVehicleNumber.text = detailModel.vehicle?.numberPlate
        lblPricePerDay.text = "\(detailModel.vehicle?.packages.first?.rate ?? 0) Nok/Hour"
        pay_Btn.setTitle("PAY \(Utilities.convertToCurrency(number: detailModel.vehicle?.deposit ?? 0.0, currencyCode: detailModel.vehicle?.currency))", for: .normal)
        totalPrice_Lbl.text = (Utilities.convertToCurrency(number: detailModel.vehicle?.deposit ?? 0.0, currencyCode: detailModel.vehicle?.currency))
        
    }
    
    @IBAction func viewCar_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func card_Toogle_Btn(_ sender: UIButton) {
        
    }
    
    @IBAction func insuranceSwitch(_ sender: UIButton) {
        if sender.isSelected == false {
            sender.isSelected = true
        } else {
            sender.isSelected = false
        }
    }
    
    @IBAction func info_Btn(_ sender: UIButton) {
        info_View.isHidden = false
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func chat_Btn(_ sender: UIButton) {
        info_View.isHidden = true
        let VC = Chat_ViewController()
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func supportView_Support_Btn(_ sender: UIButton) {
        info_View.isHidden = true
        let vc = SupportVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func seeInsurance_Btn(_ sender: UIButton) {
        let VC = Terms_ConditionViewController()
        VC.checkScreen = "BookVC"
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func supportView_Damage_Btn(_ sender: UIButton) {
        info_View.isHidden = true
        let vc = ReportDamageVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func car_pricinginfo_Btn(_ sender: UIButton) {
        if sender.tag == 0 {
            car_pricing_View.isHidden = false
            sender.tag = 1
        } else {
            sender.tag = 0
            car_pricing_View.isHidden = true
        }
    }
    
    @IBAction func fuel_info_Btn(_ sender: UIButton) {
        if sender.tag == 0 {
            fuel_Info_View.isHidden = false
            sender.tag = 1
        } else {
            sender.tag = 0
            fuel_Info_View.isHidden = true
        }
    }
    
    @IBAction func pay_Btn(_ sender: UIButton) {
        
        let vc = BookingconfirmationPopupVC(nibName: "BookingconfirmationPopupVC", bundle: nil)
        vc.headerTitle = "Booking Confirmation"
        vc.desc1 = "You now enter into a binding agreement with Weone about vehicle rental."
        vc.desc2 = ""
        vc.payableamout = ""
        vc.amountNok = ""
        vc.instantSetting = ""
        vc.carOpenlock = "Car opens/locks by App"
        vc.desc2 = "We Reserve"
        let attristring = Utilities.attributedString(firstString: totalPrice_Lbl.text!, secondString: "3213 NOK", firstFont: "Montserrat-Medium", secondFont: "Montserrat-Medium", firstColor: ColorScheme.kDriveNowThemeColor(), secondColor: ColorScheme.kDriveNowThemeColor(), firstSize: 18, secondSize: 18)
//        let first_String: [NSAttributedString.Key: Any] = [.foregroundColor: ColorScheme.kDriveNowThemeColor(), .font: UIFont(name: "Montserrat-Medium", size: 18)!]
//        let second_String: [NSAttributedString.Key: Any] = [.foregroundColor: ColorScheme.kTextColorSecondary(), .font: UIFont(name: "Montserrat-Regular", size: 18)!]
//        let firstOne = NSMutableAttributedString(string: totalPrice_Lbl.text!, attributes: first_String)
//        let secondOne = NSMutableAttributedString(string: "", attributes: second_String)
//        firstOne.append(secondOne)
        vc.reserveNok = attristring//"2000 Nok On Your Card"
        // vc.desc2 = "We reserve \(strDeposite) on your card."
        // vc.price = strDeposite
        
//        if detailModel.vehicle?.iotProvider == AppConstants.IOTProvider.WithTelemetric {
//                           vc.keyType = "KAutomaticKey".localized
//                           vc.imgKey = #imageLiteral(resourceName: "iphone")
//                       } else {
//                           vc.keyType = "KManualKey".localized
//                           vc.imgKey = #imageLiteral(resourceName: "car-key")
//                       }
        
        vc.confirmClicked = {
            if self.detailModel.vehicle?.iotProvider == AppConstants.IOTProvider.WithTelemetric {
                let VC = TelematicsKeyPickUpVC()
                VC.detailModel = self.detailModel
                self.navigationController?.pushViewController(VC, animated: true)
//                vc.keyType = "KAutomaticKey".localized
//                vc.imgKey = #imageLiteral(resourceName: "iphone")
            } else {
                let VC = TelematicsKeyPickUpVC()
                VC.detailModel = self.detailModel
                self.navigationController?.pushViewController(VC, animated: true)
//                vc.keyType = "KManualKey".localized
//                vc.imgKey = #imageLiteral(resourceName: "car-key")
            }
        }
        
        vc.cancelBtnClicked = {
            self.dismiss(animated: true, completion: nil)
        }
        
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        
        if UIViewController.current()?.presentedViewController != nil {
            UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
            })
        }
        else{
            UIViewController.current()?.present(vc, animated: true, completion: {
            })
        }
    }
    
    func callApiForCarDetail(){
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.GetVehicleDetail, method: .post, parameters: ["vehicleId" : strVehicleId,"rideType" : rideType], headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            print(response)
            if let dictResponse = response as? [String:Any] {
                self.detailModel = Mapper<VehicleDetailModel>().map(JSON: dictResponse)!
            }
            self.initialConfig()
        
//          self.preparecarFeatureDataSource()
        }) { (failureMessage, failureCode) in
            
        }
    }
}
