//
//  CompanyCell.swift
//  WeOne
//
//  Created by iMac on 11/06/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class CompanyCell: UITableViewCell {

    @IBOutlet weak var btnInfo: UIButton!
    
    @IBOutlet weak var viewStack: UIStackView!
    @IBOutlet weak var viewInvite: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none

        btnInfo.setImage(UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Information")), for: .normal)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model: CellModel) {
        
        if model.cellType == .InviteEmployee {
            viewStack.isHidden = true
            viewInvite.isHidden = false
        } else {
            viewStack.isHidden = false
            viewInvite.isHidden = true

            lblTitle.text = model.placeholder
            lblSubTitle.text = model.userText
            
        }
        
    }
    
    //MARK: Button Action Methods
    @IBAction func btnInfo_Click(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        Utilities.dismissToolTip(views: appDelegate.window!.subviews)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            Utilities.showPopOverView(str: "\("KLblKmPerdayIncluded".localized)", arrowSize: CGSize(width: 9, height: 9), maxWidth: 150, x: 27, sender: sender)
        })
    }

}
