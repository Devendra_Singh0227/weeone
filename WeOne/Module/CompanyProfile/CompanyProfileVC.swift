//
//  CompanyProfileVC.swift
//  WeOne
//
//  Created by iMac on 11/06/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class CompanyProfileVC: UIViewController {
    
    //MARK: - Variables
    var arrData = [CellModel]()
    
    var isFromPersonal : Bool = false
    var addClick: (()->())?

    //MARK: - Outlets
    @IBOutlet weak var viewNavigation: UIViewCommon!
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
        
    }
    
    //MARK: Private Methods
    func initialConfig() {
                
        tblView.register(UINib(nibName: "CompanyCell", bundle: nil), forCellReuseIdentifier: "CompanyCell")
        
        (isFromPersonal) ? (viewNavigation.text = "KLblPersonalProfile".localized as NSString) : (viewNavigation.text = "KLblCompanyProfile".localized as NSString)
        
        preapareDataSource()
        
        viewNavigation.click = {
            self.btnClose_Click()
        }
     

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.cancelsTouchesInView = false
        
        self.view.addGestureRecognizer(tap)

    }
    
    func preapareDataSource() {
        arrData.removeAll()
    
        let user = ApplicationData.user
    
        if isFromPersonal {
            arrData.append(CellModel.getModel(placeholder: "KLblName".localized, text: user?.fullName, type: .CompanyProfile))
            arrData.append(CellModel.getModel(placeholder: "KLblDefaultPaymentMethod".localized, text: "KLblCard".localized, type: .DefaultPaymentMethod))
//            arrData.append(CellModel.getModel(placeholder: "KLblEmailforReceipt".localized, text: user?.primaryEmail?.email, type: .EmailForReceipt))

        } else {
//            arrData.append(CellModel.getModel(placeholder: "KLblCompanyDetails".localized, text: "KLblWeoneAS".localized, type: .CompanyDetails))
            arrData.append(CellModel.getModel(placeholder: "KLblDefaultPaymentMethod".localized, text: "KLblCard".localized, type: .DefaultPaymentMethod))
//            arrData.append(CellModel.getModel(placeholder: "KLblEmailforReceipt".localized, text: "torje@weone.no", type: .EmailForReceipt))
//            arrData.append(CellModel.getModel(placeholder: "KLblExpenseProvider".localized, text: "KLblCertify".localized, type: .ExpenseProvider))
//            arrData.append(CellModel.getModel(placeholder: "KLblInviteEmployee".localized, type: .InviteEmployee))
        }
        
        
        
        
        tblView.reloadData()
    }
    //MARK:- IBActions
    func btnClose_Click() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        Utilities.dismissToolTip(views: appDelegate.window!.subviews)

    }

}
//MARK: - UITableView's DataSource & Delegate Methods
extension CompanyProfileVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 82
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CompanyCell") as? CompanyCell
            cell?.setData(model: arrData[indexPath.row])
            return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = arrData[indexPath.row]
        
        switch model.cellType! {
            
        case .CompanyProfile:
            let vc = EditProfileVC()
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
            break

        case .CompanyDetails:
            let vc = CompanyDetailsVC()
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
            break
            
        case .DefaultPaymentMethod:
//            if isFromPersonal {
                let vc = DefaultPaymentVC()
                
                if isFromPersonal {
                    vc.profileType = .Personal
                } else {
                    vc.profileType = .Business
                }

                vc.addClick = { isFromAddClick in
                    self.addClick?()
                }
                UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
//            }

            break
            
        case .EmailForReceipt:
            let vc = EmailReceiptVC()
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)

            break
        
        case .ExpenseProvider:
           
            self.showToast(text: "KLblLMComingSoon".localized)

            break
       
        case .InviteEmployee:
            let vc = CompanyDetailsVC()
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)

            break
            
        default:
            break
        }
    }
    
}
