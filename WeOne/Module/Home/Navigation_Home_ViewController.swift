//
//  Navigation_Home_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 06/09/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Navigation_Home_ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var location_TableView: UITableView!
    @IBOutlet weak var expandView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var plus_BtnTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var expand_last_View: UIView!
    @IBOutlet weak var AView: ViewDesign!
    @IBOutlet weak var BView: ViewDesign!
    var send_Event_Method :((String) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        location_TableView.register(UINib(nibName: "SavedPlaceCell", bundle: nil), forCellReuseIdentifier: "SavedPlaceCell")
               
        // Do any additional setup after loading the view.
    }

    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func plus_Btn(_ sender: ButtonDesign) {
        // b34 b100 190 120
        if sender.tag == 0 {
            sender.tag = 1
            expandView_HeightConstraint.constant = 190
            expand_last_View.isHidden = false
            AView.isHidden = false
            BView.isHidden = false
            sender.setImage(UIImage(named:"Group 4263"), for: .normal)
            plus_BtnTopConstraint.constant = 100
        } else {
            sender.tag = 0
            expandView_HeightConstraint.constant = 120
            expand_last_View.isHidden = true
            AView.isHidden = true
            BView.isHidden = true
            sender.setImage(UIImage(named:"Group 4248"), for: .normal)
            plus_BtnTopConstraint.constant = 34
        }
    }
    
    @IBAction func add_Places_Btn(_ sender: UIButton) {
        let VC = SavedPlaceListVC()
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func navigate_Btn(_ sender: ButtonDesign) {
        UserDefaults.standard.set("shownavigation", forKey: "shownavigation")
        UserDefaults.standard.synchronize()
        let VC = HomeVC()
        send_Event_Method?("home")
        navigationController?.popAllAndSwitch(to: VC)
    }
    
    @IBAction func showOnMap_Btn(_ sender: ButtonDesign) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = location_TableView.dequeueReusableCell(withIdentifier: "SavedPlaceCell") as? SavedPlaceCell
        return cell!
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
