//
//  PlacePickerCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 17/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class SavedPlaceCell: UITableViewCell {

    @IBOutlet weak var imgFav: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var btnRemove: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(model : FavouritePlaceModel) {
        
        imgFav.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Fav"))

        if !Utilities.checkStringEmptyOrNil(str: model.name){
            lblTitle.isHidden = false
        }else{
            lblTitle.isHidden = true
        }
        lblTitle.text = model.name
        lblAddress.text = model.geoLocation?.name
    }
}
