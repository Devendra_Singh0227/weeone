//
//  HomeVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 09/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit
import SideMenu

class HomeVC: UIViewController {
    
    //MARK: Variables
    var currentLocation: CLLocation = CLLocation()
    var currentLocationMarker: GMSMarker = GMSMarker()
    var arrMarkers: [GMSMarker] = []
    var camera = GMSCameraPosition()
    var isShowFuelStation : Bool = false
    var isShowChargeStation : Bool = false
    var checkScreen = ""
    
    //MARK: Outlets
    
    @IBOutlet weak var corporateView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var notification_View: UIStackView!
    @IBOutlet weak var location_View: ShadowCard!
    @IBOutlet weak var bottom_View: ViewDesign!
    @IBOutlet weak var return_Location_View: ViewDesign!
    @IBOutlet weak var ride_Navigate_View: UIStackView!
    @IBOutlet weak var stack_Last_View: UIView!
    @IBOutlet weak var stack_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var plus_Btn_TopConstraint: NSLayoutConstraint!
    @IBOutlet weak var walkThrough_Image: UIView!
    @IBOutlet weak var right_Profile_Btn: UIImageView!
    @IBOutlet weak var Aview: ViewDesign!
    @IBOutlet weak var Bview: ViewDesign!
    @IBOutlet weak var driveView: ViewDesign!
    @IBOutlet weak var scheduleView: ViewDesign!
    @IBOutlet weak var drive_Lbl: UILabel!
    @IBOutlet weak var schedule_Lbl: UILabel!
    @IBOutlet weak var locationBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var navigateView: ViewDesign!
    @IBOutlet weak var notificationHeight: NSLayoutConstraint!
    
    //MARK: view life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
        checkFromScreen()
        notification_View.isHidden = true
//        notificationHeight.constant = 108
       LocationManager.SharedManager.getLocation()
       LocationManager.SharedManager.getCurrentLocation = { location in
           self.currentLocation = location
           self.location_View.isHidden = true
//           self.showCurrentLocation()
           self.callApiForGetNearbyCityVehicle()
       }
    }
    
    func checkFromScreen() {
        if UserDefaults.standard.value(forKey: "shownavigation") != nil {
            notification_View.isHidden = true
            location_View.isHidden = true
            bottom_View.isHidden = true
            return_Location_View.isHidden = false
            ride_Navigate_View.isHidden = false
            locationBottomConstraint.constant = -90
            navigateView.isHidden = false
            UserDefaults.standard.removeObject(forKey: "shownavigation")
        } else {
            
        }
    }
    
    //MARK: initial config
    
    func initialConfig(){
        Defaults[.isUserLoggedIn] = true
        if Defaults[.location_access] == true {
            location_View.isHidden = true
        } else {
            location_View.isHidden = false
        }
        checkUserlogged()
//      setDataAccordingToRideStatus()
//        loadMapJsonFile()
        mapView.mapType = .normal
        mapView.setMinZoom(2.0, maxZoom: 14.0)
        mapView.isBuildingsEnabled = true
    }
    
    func checkUserlogged() {
        if checkScreen == "" {
            walkThrough_Image.isHidden = false
        } else {
            walkThrough_Image.isHidden = true
        }
    }
    
    func loadMapJsonFile(){
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
                print("style",styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    @IBAction func drive_now_Btn(_ sender: UIButton) {
        driveView.borderwidth = 2
        drive_Lbl.textColor = ColorConstants.DriveNowThemeColor
        schedule_Lbl.textColor = ColorConstants.secondTextColor
        scheduleView.borderwidth = 0
        let vc = CarMapVC()
        vc.isFromDriveNow = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func schedule_Btn(_ sender: UIButton) {
        driveView.borderwidth = 0
        scheduleView.borderwidth = 2
        schedule_Lbl.textColor = ColorConstants.TextColorGreen
        drive_Lbl.textColor = ColorConstants.secondTextColor
        let vc = CarScheduleVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func plus_Btn(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            sender.setImage(UIImage(named: "Group 4263"), for: .normal)
             stack_Last_View.isHidden = false
            Aview.isHidden = false
            Bview.isHidden = false
            stack_HeightConstraint.constant = 200
            plus_Btn_TopConstraint.constant = 100
        } else {
            sender.tag = 0
            sender.setImage(UIImage(named: "Group 4248"), for: .normal)
            Aview.isHidden = true
            Bview.isHidden = true
             plus_Btn_TopConstraint.constant = 32
            stack_Last_View.isHidden = true
            stack_HeightConstraint.constant = 120
        }
    }
    
    @IBAction func endRide_Btn(_ sender: UIButton) {
        let vc = EndRidePopup_ViewController()
        
        vc.confirmClicked = {
            let VC = AddRentalReport_ViewController()
            self.navigationController?.pushViewController(VC, animated: true)
        }
        
        vc.cancelBtnClicked = {
            self.dismiss(animated: true, completion: nil)
        }
        
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        
        if UIViewController.current()?.presentedViewController != nil {
            UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
            })
        }
        else{
            UIViewController.current()?.present(vc, animated: true, completion: {
            })
        }
    }
    
    @IBAction func btnMenu_Click(_ sender: UIButton) {
        let leftMenu = LeftMenuVC(nibName: "LeftMenuVC", bundle: nil)
        let rootLeft = SideMenuNavigationController(rootViewController: leftMenu)
        rootLeft.presentationStyle = .menuSlideIn
        rootLeft.presentationStyle.backgroundColor = UIColor.clear
        rootLeft.menuWidth = UIScreen.main.bounds.width
        rootLeft.leftSide = true
        rootLeft.enableSwipeToDismissGesture = false
        present(rootLeft, animated: true, completion: nil)
    }
    
    @IBAction func close_WalkThrough_Btn(_ sender: UIButton) {
        walkThrough_Image.isHidden = true
    }
    
    @IBAction func business_Btn(_ sender: UIButton) {
        corporateView.isHidden = true
    }
    
    @IBAction func Profile_Btn(_ sender: UIButton) {
        corporateView.isHidden = false
    }
    
    @IBAction func location_Service_Btn(_ sender: UIButton) {
        if Defaults[.location_access] == true {
             showCurrentLocation()
        } else {
            if let url = URL(string: UIApplication.openSettingsURLString) {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:], completionHandler:  { (Bool) in
                        //print(Bool)
                        self.location_View.isHidden = true
                        self.showCurrentLocation()
                    })
                }
            }
        }
    }
    
    //MARK: Show current Location
    
    func showCurrentLocation() {
        camera = GMSCameraPosition.camera(withLatitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude, zoom: 16.0)
        
        mapView.camera = camera
       
        let position = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude)
        currentLocationMarker.position = position
        currentLocationMarker.icon = UIImage(named : "currentLocationMarker")
        currentLocationMarker.map = mapView
        
        loadMapJsonFile()
    }
    
    //MARK: set data
    func setDataAccordingToRideStatus(){
        showActiveRideView()
    }
}

//MARK: Reserve Ride Methods
extension HomeVC {
    
    func showActiveRideView(){
        
        if let model = SyncManager.sharedInstance.activeRideObject{
            let pauseEndRideVC = PauseEndRideVC()

            pauseEndRideVC.modalPresentationStyle = .overCurrentContext
            self.addChild(pauseEndRideVC)
            self.view.addSubview(pauseEndRideVC.view)
            pauseEndRideVC.didMove(toParent: self)
            pauseEndRideVC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: self.view.frame.width, height: pauseEndRideVC.view.frame.height)
            
            pauseEndRideVC.setData(model)
            
            getStation()
        }
    }
}

//Mark : Active ride Map pin
extension HomeVC {
    
    //Get Station with Partner
    func getStation() {
        
        var request = Parameters()
        request["partnerId"] = SyncManager.sharedInstance.activeRideObject?.partnerId
        
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.StationOfPartner, method: .post, parameters: request, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
           
            if let dictResponse = response as? [[String:Any]]{
                
                let arr = Mapper<VehicleModel>().mapArray(JSONArray: dictResponse)
                var bounds = GMSCoordinateBounds()
                
                for item in arr {
                    
                    if item.geoLocation != nil{
                        
                        let location = CLLocation(latitude: item.geoLocation?.latitude ?? 0.0, longitude: item.geoLocation?.longitude ?? 0.0)
                        self.currentLocation = location
                        self.dropMarker(location, vehicleModel : item)
                        bounds = bounds.includingCoordinate(CLLocationCoordinate2D(latitude: item.geoLocation?.latitude ?? 0.0, longitude: item.geoLocation?.longitude ?? 0.0))
                    }
                    self.showCurrentLocation()
                }
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func callApiForGetNearbyCityVehicle(_ completion : (() -> ())? = nil){
        
        var param : [String:Any] = [:]
        param["zoomLevel"] = 10//mapView.camera.zoom
        param["long"] =  "77.55"//currentLocation.coordinate.longitude
        param["lat"] = "30.85"//currentLocation.coordinate.latitude
        param["rideType"] = 1
//        rideTypeif isShowFuelChargeStation {
//            let coordinate = mapView.projection.coordinate(for: mapView.center)
//            param["currentLocation"] = [coordinate.longitude , coordinate.latitude]
//        } else {
//            param["currentLocation"] = [currentLocation.coordinate.longitude , currentLocation.coordinate.latitude]
//        }
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.GetNearbyCityVehicle, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            self.arrMarkers.removeAll()
            self.mapView.clear()
            Defaults[.isUserLoggedIn] = true
            if let dict = response as? [String:Any]{
                
                if let list = dict["vehicles"] as? [[String:Any]]{
                    
                    let arr = Mapper<VehicleModel>().mapArray(JSONArray: list)
//                    var bounds = GMSCoordinateBounds()
                    for data in arr {
                        let lat = Double(data.latitude ?? "0.0")
                        let long = Double(data.longitude ?? "0.0")
                        let location = CLLocation(latitude: lat ?? 0.0, longitude: long ?? 0.0)
                        self.currentLocation = location
                        self.dropMarker(location,  vehicleModel: data)
                    }
                    self.showCurrentLocation()
//                  self.arrCities = Mapper<VehicleModel>().mapArray(JSONArray: list).filter { $0.geoLocation != nil }
                    
//                  self.arrCities = SyncManager.sharedInstance.sortigBasedOnLocation(arrMarker: self.arrCities, currentLocation: self.currentLocation)
                }
            }
            completion?()
                        
        }) { (failureMessage, failureCode) in
            Utilities.showAlertWithButtonAction(title: "", message: failureMessage, buttonTitle: StringConstants.ButtonTitles.KOk, onOKClick: {
                if failureMessage == "Token is not provided" || failureMessage == "You are not authorize to access this resource." || failureMessage == "Your token is expired or invalid." {
                    let vc = SignUpVC()
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    
                }
            })
            //Utilities.showAlertView(message: failureMessage)
        }
    }
}

//MARK: MapView methods
extension HomeVC : GMSMapViewDelegate{
    
    func dropMarker(_ location : CLLocation, _ isSelected: Bool = false, vehicleModel : VehicleModel) {
        
        let marker = GMSMarker()
        camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 16.0)
        mapView.camera = camera
        marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        marker.icon = UIImage(named : "carmarker")
        marker.map = mapView
        marker.userData = vehicleModel
        arrMarkers.append(marker)
        //print(location.coordinate.latitude, location.coordinate.longitude)
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
    
        if marker.userData == nil {
            return true
        }
        
        let model = marker.userData as? VehicleModel
        Utilities.openMap(latitude: model?.geoLocation?.longitude ?? 0.0, longitude: model?.geoLocation?.latitude ?? 0.0)
        return true
    }
}
