//
//  Loading_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 26/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit
import GTProgressBar

class Loading_ViewController: UIViewController {

    @IBOutlet weak var progress_Bar: GTProgressBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        progress_Bar.progress = 0
       
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let newProgress: CGFloat = progress_Bar.progress == 0.75 ? 0.45 : 1
        progress_Bar.animateTo(progress: newProgress)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            let VC = HomeVC()
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }



}
