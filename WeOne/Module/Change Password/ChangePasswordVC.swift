//
//  ChangePasswordVC.swift
//  E-Scooter
//
//  Created by Coruscate Mac on 07/06/19.
//  Copyright © 2019 CORUSCATEMAC. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    //MARK: variables
    
    var isFromForgotPassword = false
    var otp = ""
    var mobile = ""

    
    //MARK: outlets
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewOldPassword: UIView!
    @IBOutlet weak var viewNewPassword: UIView!
    
    @IBOutlet weak var lblOldPasswordLine: UILabel!
    @IBOutlet weak var lblNewPasswordLine: UILabel!

    @IBOutlet weak var viewTost: UIViewCommonTost!

    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    
    @IBOutlet weak var btnOldPassword: UIButton!
    @IBOutlet weak var btnNewPassword: UIButton!

//    @IBOutlet weak var txtConfirmPassword: UITextFiledCommon!
    
    @IBOutlet weak var btnChangePassword: UIButtonCommon!
    
    //MARK: view life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
        
//        if isFromForgotPassword {
//            lblChangePwd.text = "KLblSetPassword".localized
//        } else {
//            lblChangePwd.text = "KLblChangePassword".localized
//        }
    }

    //MARK: initial config
    func initialConfig(){
        
//        viewBottom.roundCorners([.topRight,.topLeft], radius: 30, width: AppConstants.ScreenSize.SCREEN_WIDTH)
//        viewBottom.dropShadow()
        
        txtOldPassword.tintColor = ColorConstants.ThemeColor
        txtNewPassword.tintColor = ColorConstants.ThemeColor
        
        if isFromForgotPassword {
            viewOldPassword.isHidden = true
            btnChangePassword.text = NSString(format: "%@", "KLblSetPasswordCapital".localized)
        }
        else {
            viewOldPassword.isHidden = false
            btnChangePassword.text = NSString(format: "%@", "KLblChangePasswordCapital".localized)
            
        }
        
        btnChangePassword.click = {
             self.btnChangePassword_Click()
        }
    }
    
    //MARK: Button action methods
    @IBAction func btnClose_Click(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnOldPassword_Click(_ sender: UIButton) {
        if txtOldPassword.isSecureTextEntry == true{
            sender.isSelected = true
            txtOldPassword.isSecureTextEntry = false
        } else {
            sender.isSelected = false
            txtOldPassword.isSecureTextEntry = true
        }
    }

    @IBAction func btnNewPassword_Click(_ sender: UIButton) {
        if txtNewPassword.isSecureTextEntry == true{
            sender.isSelected = true
            txtNewPassword.isSecureTextEntry = false
        } else {
            sender.isSelected = false
            txtNewPassword.isSecureTextEntry = true
        }        
    }

    func btnChangePassword_Click() {
        self.view.endEditing(true)
        
        let (isValidate, request) = getRequestAndValidate()
        if isValidate {
            if isFromForgotPassword {
                let param = ["mobile" : self.mobile, "newPassword" : txtNewPassword.text ?? ""] as [String : Any]
                resetPassword(param as! [String : String])
            } else {
                callApiForChangePassword(param: request)
            }
        }
    }
    
    func getRequestAndValidate() -> (Bool,[String:Any]) {
        var req = [String:Any]()
        
        // If this screen is from forgot password then don't need to verify the old plassword
        if !isFromForgotPassword {
            guard !Utilities.checkStringEmptyOrNil(str: txtOldPassword.text) else {
                
                viewTost.text = StringConstants.ChangePassword.kOldPassword as NSString
                viewTost.imageIcon = "info" as NSString

                
                viewTost.isHidden = false
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                    self.viewTost.isHidden = true
                })

//                Utilities.showAlertView(message: StringConstants.ChangePassword.kOldPassword)
                return(false,[:])
            }
        }
        
        guard !Utilities.checkStringEmptyOrNil(str: txtNewPassword.text) else {
           
            viewTost.text = StringConstants.ChangePassword.kNewPassword.localized as NSString
            viewTost.imageIcon = "info" as NSString

            
            viewTost.isHidden = false
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                self.viewTost.isHidden = true
            })

            // Utilities.showAlertView(message: StringConstants.ChangePassword.kNewPassword)
            return(false,[:])
        }
        
        
        guard txtOldPassword.text != txtNewPassword.text else {
            Utilities.showAlertView(message: StringConstants.ChangePassword.kOldPasswordMismatch)
            return(false,[:])
        }
        
        req["currentPassword"] = txtOldPassword.text
        req["newPassword"] = txtNewPassword.text
        
        return(true, req)
    }
}

//MARK:- API Calling
extension ChangePasswordVC {
    
    func resetPassword(_ param: [String : String]) {
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.ResetPassword, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            self.navigationController?.popViewController(animated: true)
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func callApiForChangePassword(param : [String:Any]){
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.ChangePassword, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            self.navigationController?.popViewController(animated: true)
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
extension ChangePasswordVC: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtOldPassword {
            lblOldPasswordLine.backgroundColor = ColorConstants.ThemeColor
            lblNewPasswordLine.backgroundColor = ColorConstants.UnderlineColor
        } else {
            lblNewPasswordLine.backgroundColor = ColorConstants.ThemeColor
            lblOldPasswordLine.backgroundColor = ColorConstants.UnderlineColor
        }
        return true
    }
}
