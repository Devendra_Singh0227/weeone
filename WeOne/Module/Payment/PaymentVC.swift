//
//  PaymentVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 01/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class PaymentVC: UIViewController {
    
    //MARK: - Variables
    var arrData = [CellModel]()
    var arrCardPersonal = [CellModel]()
    var arrCardBussiness = [CellModel]()
    var screenType:PaymentVCType = .Payment
    var isDeleteHidden : Bool = false
    var isReload:((_ isFromAddClick: Bool)->())?
    var isPresented : Bool = false
    var shownCardType : Int = AppConstants.CardTypes.PersonalCard
    var isFromPersonalCard = false
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!

    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        Utilities.setNavigationBar(controller: self, isHidden: true)
    }
    
    //MARK: Private Methods
    func initialConfig() {
        
        tableView.register(UINib(nibName: "PaymentSectionCell", bundle: nil), forCellReuseIdentifier: "PaymentSectionCell")
        tableView.register(UINib(nibName: "CardsCell", bundle: nil), forCellReuseIdentifier: "CardsCell")
        tableView.register(UINib(nibName: "CardsTableCell", bundle: nil), forCellReuseIdentifier: "CardsTableCell")
        tableView.register(UINib(nibName: "PaymentVoucherCell", bundle: nil), forCellReuseIdentifier: "PaymentVoucherCell")
                
        prepareDataSource() 
    }
    
    func prepareDataSource() {
        
        arrData.removeAll()
        
        //cards
        arrData.append(CellModel.getModel(placeholder: "KLblCards".localized, text: "", type: .PaymentSection))
        
        if isFromPersonalCard {
            let cards = SyncManager.sharedInstance.fetchCard(AppConstants.CardTypes.PersonalCard)
                        
            for model in cards {
                arrData.append(CellModel.getModel(placeholder: model.getFormattedCardNumber(), text: "", type: .PaymentCard, imageName: Utilities.getCreditCardImage(cardType: model.brand ?? ""), cellObj: model, isSelected: model.isPrimary))
            }
            
        } else {
            let cards = SyncManager.sharedInstance.fetchCard(AppConstants.CardTypes.JobCard)
                                
            for model in cards {
                arrData.append(CellModel.getModel(placeholder: model.getFormattedCardNumber(), text: "", type: .PaymentCard, imageName: Utilities.getCreditCardImage(cardType: model.brand ?? ""), cellObj: model, isSelected: model.isPrimary))
            }
        }
        
        arrData.append(CellModel.getModel(placeholder: "KLblAddNewCard".localized, text: "", type: .PaymentAddNewCard))
        tableView.reloadData()
    }
}

//MARK: - UITableView's DataSource & Delegate Methods
//extension PaymentVC: UITableViewDataSource, UITableViewDelegate, SwipeTableViewCellDelegate {
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return arrData.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let model = arrData[indexPath.row]
//        switch model.cellType! {
//
//        case .PaymentCard, .PaymentPromoCode, .PaymentAddNewCard:
//
//            let cell = tableView.dequeueReusableCell(withIdentifier: "CardsTableCell") as? CardsTableCell
//
//            cell?.setData(model: model)
//            cell?.delegate = self
//            cell?.onClick = { (obj) in
//                self.onClick(model: obj)
//            }
//
//            cell?.onPrimaryClick = { model in
//                // code
//                if !model.isPrimary{
//                    self.openPopupForPrimarySelection(model)
//                }
//            }
//
//            cell?.onDeleteClick = { model in
//                self.openPopupForDeleteCard(model)
//            }
//
//            return cell!
//
//        case .PaymentSection:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentSectionCell") as? PaymentSectionCell
//            cell?.setData(model: model)
//            return cell!
//
//        case .PaymentVouchers:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentVoucherCell") as? PaymentVoucherCell
//            cell?.setData(model: model)
//            return cell!
//
//        default:
//            return UITableViewCell()
//        }
//
//    }
//
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
//
//        let model = self.arrData[indexPath.row]
//
//
//        if orientation == .right {
//
//            if model.isSelected {
//                return nil
//            }
//
//            let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
//                if let obj = self.arrData[indexPath.row].cellObj as? CardModel {
//                    self.openPopupForDeleteCard(obj)
//                }
//            }
//
//            deleteAction.image = UIImage(named: "bin")
//
//            return [deleteAction]
//
//
//        } else {
//            return nil
//        }
//    }
//
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return true
//    }
//
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        onClick(model: arrData[indexPath.row])
//    }
//
//    func onClick(model: CellModel) {
//
//        if model.cellType == .PaymentAddNewCard {
//
//            let vc = AddCardViewController()
//
//            if isFromPersonalCard {
//                vc.cardType = AppConstants.CardTypes.PersonalCard
//            } else {
//                vc.cardType = AppConstants.CardTypes.JobCard
//            }
//
//            vc.addClick = {
//                self.prepareDataSource()
//                self.isReload?(true)
//            }
//
//            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
//
////            let navigationController = UIApplication.shared.keyWindow?.rootViewController as! UINavigationController
////
////            navigationController.pushViewController(vc, animated: true)
//
//        } else {
//            if model.cellType == .PaymentVouchers {
//                let vc = RedeemVoucherVC()
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//        }
//    }
//
//    func openPopupForPrimarySelection(_ obj : CardModel) {
//        UIViewController.current()?.view.showConfirmationPopupWithMultiButton(viewController : self, title: "", message: StringConstants.MakePayment.KMsgPrimaryCard, cancelButtonTitle: StringConstants.ButtonTitles.kNo, confirmButtonTitle: StringConstants.ButtonTitles.kYes, onConfirmClick: {
//            // confirm
//            self.callAPIForSetPrimaryCard(cardModel: obj)
//
//        }, onCancelClick: {
//            // cancel
//        })
//    }
//
//    func openPopupForDeleteCard(_ obj : CardModel){
//
//        UIViewController.current()?.view.showConfirmationPopupWithMultiButton(viewController : self, title: "", message: StringConstants.MakePayment.KLblDeleteCardConfirmation, cancelButtonTitle: StringConstants.ButtonTitles.kNo, confirmButtonTitle: StringConstants.ButtonTitles.kYes, onConfirmClick: {
//            // confirm
//            self.callApiForDeleteCard(cardModel: obj)
//
//        }, onCancelClick: {
//            // cancel
//        })
//    }
//}


//MARK:- API Calling
extension PaymentVC {
    
    func callAPIForSetPrimaryCard(cardModel:CardModel) {
        
        var param = [String:Any]()
        param["cardId"] = cardModel.id ?? ""
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.SetDefaultCard, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            SyncManager.sharedInstance.setPrimaryCard(cardModel: cardModel)
            if self.screenType == .ChangeCard {
                self.isReload?(false)
            }
            self.prepareDataSource()
            
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func callApiForDeleteCard(cardModel:CardModel) {
        
        var param = [String:Any]()
        param["cardId"] = cardModel.id ?? ""
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.DeleteCard, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            
            SyncManager.sharedInstance.deleteCard(obj: cardModel)
            self.prepareDataSource()
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}

//MARK:- Extra Methods
extension PaymentVC {
    
    convenience init() {
        self.init(nibName: "PaymentVC", bundle: nil)
    }
}
