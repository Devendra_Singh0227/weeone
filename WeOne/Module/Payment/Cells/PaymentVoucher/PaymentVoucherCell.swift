//
//  PaymentVoucherCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 02/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class PaymentVoucherCell: UITableViewCell {

    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var imgVoucher: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        lblTitle.text = model.placeholder
        lblCount.text = model.placeholder2
        imgVoucher.image = UIImage(named: model.imageName ?? "")
    }
}
