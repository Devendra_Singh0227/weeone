//
//  CardsTableCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 02/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class CardsTableCell: UITableViewCell {

    //MARK: - Variables
    var onClick:((_ model : CellModel)->())?
    var onPrimaryClick:((_ model : CardModel)->())?
    var onDeleteClick :((_ model : CardModel)->())?
    var model = CellModel()
   
    //MARK: - Outlets
    @IBOutlet weak var vwCard: UIView!
    @IBOutlet weak var vwAddNewCard: UIView!
    @IBOutlet weak var imgCardType: UIImageView!
    @IBOutlet weak var lblCardNumber: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnPrimary: UIButton!
    @IBOutlet weak var lblCardType: UILabel!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK: Set Data
    func setData(model:CellModel) {
        
        self.model = model
        
        if model.cellType == .PaymentCard {
            
            vwCard.isHidden = false
            vwAddNewCard.isHidden = true
            imgCardType.image = UIImage(named: model.imageName ?? "")
            lblCardNumber.text = model.placeholder
        }
        else {
            
            vwAddNewCard.isHidden = false
            vwCard.isHidden = true
            lblTitle.text = model.placeholder
            
            if model.cellType == .PaymentCardTitle{
                lblTitle.textColor = ColorConstants.TextTitleBlack
//                lblTitle.font = FontScheme.kMediumFont(size: 16.0)
            }else{
                lblTitle.textColor = ColorConstants.ThemeColor
//                lblTitle.font = FontScheme.kMediumFont(size: 16.0)
            }
        }
        
        btnPrimary.isSelected = model.isSelected
//        if model.isSelected {
//            btnDelete.isHidden = true
//        }
//        else {
//            btnDelete.isHidden = false
//        }
    }
    
    @IBAction func btnPrimary_Click(_ sender: UIButton) {

        if let obj = model.cellObj as? CardModel {
            onPrimaryClick?(obj)
        }
    }
    
    @IBAction func btnDelete_Click(_ sender: UIButton) {
        
        if let obj = model.cellObj as? CardModel {
            
            onDeleteClick?(obj)
        }
    }
}
