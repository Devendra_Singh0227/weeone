//
//  CardsCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 02/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class CardsCell: UITableViewCell {

    //MARK: - Variables
    var arrData = [CellModel]()
    var onClick:((_ model : CellModel)->())?
    var onPrimaryClick:((_ model : CardModel)->())?
    var onDeleteClick :((_ model : CardModel)->())?
    var isDeleteHidden : Bool = false
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var vwBottom: NSLayoutConstraint!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        
        tableView.register(UINib(nibName: "CardsTableCell", bundle: nil), forCellReuseIdentifier: "CardsTableCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK: Set Data
    func setData(model:CellModel, isDeleteHidden : Bool = false) {
        
        self.isDeleteHidden = isDeleteHidden
        
        if let arr = model.dataArr as? [CellModel] {
            arrData = arr
        }
        
        tableViewHeight.constant = CGFloat(arrData.count * 60)
        
        if model.cellType == .PaymentCardTitle {
            vwBottom.constant = 16
        }else
        if model.cellType == .PaymentPromoCode {
            vwBottom.constant = 26
        }
        else{
            vwBottom.constant = 16
        }
        
        tableView.reloadData()
    }
}

//MARK: - UITableView's DataSource & Delegate Methods
extension CardsCell: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = arrData[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardsTableCell") as? CardsTableCell
        cell?.setData(model: model)
        cell?.btnDelete.tag = indexPath.row
        cell?.btnPrimary.tag = indexPath.row
        
        if isDeleteHidden {
            cell?.btnDelete.isHidden = true
        }
        
        cell?.btnDelete.addTarget(self, action: #selector(btnDelete_Click(_:)), for: .touchUpInside)
        cell?.btnPrimary.addTarget(self, action: #selector(btnPrimary_Click(_:)), for: .touchUpInside)
        
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.onClick?(arrData[indexPath.row])
    }
    
    @objc func btnPrimary_Click(_ sender: UIButton) {

        if let obj = arrData[sender.tag].cellObj as? CardModel {
            onPrimaryClick?(obj)
        }
    }
    
    @objc func btnDelete_Click(_ sender: UIButton) {
        
        if let obj = arrData[sender.tag].cellObj as? CardModel {
            
            onDeleteClick?(obj)
        }
    }
}
