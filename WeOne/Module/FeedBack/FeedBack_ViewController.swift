//
//  FeedBack_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 05/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit
import HCSStarRatingView

class FeedBack_ViewController: UIViewController, TagListViewDelegate, UITextViewDelegate {

    @IBOutlet weak var tags_View: TagListView!
    lazy var Feddback_Array = ["Driving", "Cleanliness", "Price", "Pickup", "Technical Issue", "Need repair", "Professionalism", "GPS Route", "Car Quality", "Weone App", "Other"]
    var rating = 0
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var comment_TxtView: IQTextView!
    @IBOutlet weak var count_Lbl: UILabel!
    @IBOutlet weak var textView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var good_Lbl: UILabel!
    var activeRideObject : RideListModel?
    var arrRatting = [RatingModel]()
    var arrData = [ReviewModel]()
    var arrSelectedID = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        comment_TxtView.delegate = self
        initialConfig()
        tags_View.addTags(Feddback_Array)
        tags_View.delegate = self
        
    }
    
    func initialConfig() {
//           if AppConstants.hasSafeArea {
//               headerView_HeightConstraint.constant = 90
//           }
//           else {
//               headerView_HeightConstraint.constant = 74
//           }
       }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func rating_View(_ sender: HCSStarRatingView) {
        print(sender.value)
        rating = Int(sender.value)
        if rating == 1 {
            good_Lbl.text = "Terrible"
        } else if rating == 2 {
            good_Lbl.text = "Bad"
        } else if rating == 3 {
            good_Lbl.text = "Okay"
        } else if rating == 4 {
            good_Lbl.text = "Good"
        } else if rating == 5 {
            good_Lbl.text = "Awesome"
        } else {
            good_Lbl.text = ""
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
         let height = ceil(textView.contentSize.height) // ceil to avoid decimal

          if height != textView_HeightConstraint.constant { // set when height changed
              textView_HeightConstraint.constant = height
              textView.setContentOffset(CGPoint.zero, animated: false) // scroll to top to avoid "wrong contentOffset" artefact when line count changes
          }
      }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
           let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
           let numberOfChars = newText.count
           if numberOfChars > 255 {
               
           } else {
               self.count_Lbl.text =  "\(textView.text.count)/255"
           }
           return numberOfChars < 256
       }
    
    @IBAction func done_Btn(_ sender: UIButton) {
        if rating < 5 {
            let vc = FeedBackPop_ViewController()
            vc.desc = NSAttributedString(string: "Thank You For Your Feedback")
            //            vc.confirmClicked = {
            //                let VC = KeyPickup_InPerson_ViewController()
            //                self.navigationController?.pushViewController(VC, animated: true)
            //            }
            //
            //            vc.cancelBtnClicked = {
            //                self.dismiss(animated: true, completion: nil)
            //            }
            
            vc.modalPresentationStyle = .custom
            vc.modalTransitionStyle = .crossDissolve
            
            if UIViewController.current()?.presentedViewController != nil {
                UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
                })
            }
            else{
                UIViewController.current()?.present(vc, animated: true, completion: {
                })
            }
        } else {
            let vc = FiveStar_feedBack_ViewController()
            
//            vc.confirmClicked = {
//                let VC = KeyPickup_InPerson_ViewController()
//                self.navigationController?.pushViewController(VC, animated: true)
//            }
//
//            vc.cancelBtnClicked = {
//                self.dismiss(animated: true, completion: nil)
//            }
            
            vc.modalPresentationStyle = .custom
            vc.modalTransitionStyle = .crossDissolve
            
            if UIViewController.current()?.presentedViewController != nil {
                UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
                })
            }
            else{
                UIViewController.current()?.present(vc, animated: true, completion: {
                })
            }
        }
    }
    
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        print("Tag pressed: \(title)")
        tagView.isSelected = !tagView.isSelected
    }
    
    func callApiforGiveRating(){
        
        var param : [String:Any] = [:]
        param["to"] = activeRideObject?.vehicleId?.id
        param["rideId"] = activeRideObject?.id
        
        var request : [String:Any] = [:]
        request["rating"] = rating
        let parent = SyncManager.sharedInstance.fetchMasterWithCode(AppConstants.MasterCode.RatingType).first
        
        request["ratingType"] = SyncManager.sharedInstance.fetchMasterWithId(parent?.id ?? "").first?.id
        request["ratingCategory"] = arrSelectedID
        
        param["ratings"] = [request]
        
        if comment_TxtView.text.count > 0{
            param["review"] = comment_TxtView.text
        }
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.GiveRating, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            SyncManager.sharedInstance.syncMaster ({
                AppNavigation.shared.moveToHome(isPopAndSwitch: true)
            })
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
}
