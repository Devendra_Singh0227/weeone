//
//  ContactUsVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 25/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class ContactUsVC: UIViewController {
    
    //MARK: varibles
    
    //MARK: outlets
    @IBOutlet weak var txtEmail: UITextFiledCommon!
    @IBOutlet weak var txtView: UIPlaceHolderTextView!
    @IBOutlet weak var btnSubmit: UIButtonCommon!
    
    //MARK: View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtView.delegate = self
        btnSubmit.click = {
            self.callApiForContactUs()
        }
    }
    
    
    //MARK: Button action methods
    @IBAction func btnClose_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: Api calls
extension ContactUsVC{
    
    func callApiForContactUs(){
        
        if txtEmail?.getText()?.count ?? 0 == 0{
            self.showToast(text: "KLblEnterEmail".localized)

//            Utilities.showAlertView(message: "KLblEnterEmail".localized)
            return
        }else if txtEmail?.getText()!.isValidEmail() == false{
            self.showToast(text: "KLblEnterValidEmail".localized)

//            Utilities.showAlertView(message: "KLblEnterValidEmail".localized)
            return
        }else if txtView.text.count == 0{
            self.showToast(text: "KLblMessage".localized)

//            Utilities.showAlertView(message: "KLblMessage".localized)
            return
        }
        
        var param : [String:Any] = [:]
        param["subject"] = txtEmail.getText()
        param["message"] = txtView.text
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.ContactUs, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            Utilities.showAlertWithButtonAction(title: "", message: message, buttonTitle: StringConstants.ButtonTitles.KOk) {
                
                self.txtEmail.setText(text: "")
                self.txtView.text = ""
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}

//Textview
extension ContactUsVC : UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        
        
        if !text.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        
        if text == "\n"{
            textView.resignFirstResponder()
        }
        
        if range.location == 0{
            
            if text == " " {
                return false
            }
        }
        
        return true
    }
}
