//
//  RatingCell.swift
//  WeOne
//
//  Created by iMac on 31/03/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class RatingCell: UICollectionViewCell {

    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK: Set Data
    func setData(_ model: ReviewModel) {
        
        if model.isSelected! {
            lblName.layer.borderColor = ColorConstants.ThemeColor.cgColor
            lblName.textColor = ColorConstants.ThemeColor
        } else {
            lblName.layer.borderColor = ColorConstants.TextColorSecondary.cgColor
            lblName.textColor = ColorConstants.TextColorSecondary
        }
        lblName.text = model.name
    }

}
