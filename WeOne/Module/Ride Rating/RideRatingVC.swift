//
//  RideRatingVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 10/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit
import HCSStarRatingView

class RideRatingVC: UIViewController {
    
    //MARK: - Variables
    var rating = 0
    var activeRideObject : RideListModel?
    var arrRatting = [RatingModel]()
    var arrData = [ReviewModel]()
    
    //var arrSelectedID = [String]()
    
    var arrSelectedID = [0]
    
    //MARK: - Outlets
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var viewNavigation: UIViewCommon!
    @IBOutlet weak var viewComment: UIView!

    @IBOutlet weak var lblRating: UILabel!
    
    @IBOutlet weak var btnRate1: UIButton!
    @IBOutlet weak var btnRate2: UIButton!
    @IBOutlet weak var btnRate3: UIButton!
    @IBOutlet weak var btnRate4: UIButton!
    @IBOutlet weak var btnRate5: UIButton!
    @IBOutlet weak var txtView: UIPlaceHolderTextView!
    @IBOutlet weak var btnSubmitReview: UIButtonCommon!
    @IBOutlet weak var starRating: HCSStarRatingView!
    @IBOutlet weak var collectView: UICollectionView!
    
    @IBOutlet weak var lblRatingTitle: UILabel!
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }
    
    
    //MARK: Private Methods
    func initialConfig() {
        
        viewNavigation.viewGradient.backgroundColor = ColorConstants.ThemeColor
        imgLogo.image = UIImage(named : ThemeManager.sharedInstance.getImage(string: "appLogo"))
        
        //arrselectedIndex.removeAll()
        
        collectView.register(UINib(nibName: "RatingCell", bundle: nil), forCellWithReuseIdentifier: "RatingCell")
        
        arrRatting = SyncManager.sharedInstance.fetchRating()
        
        btnSubmitReview.button.isEnabled = true
        //        btnSubmitReview.button.backgroundColor = ColorConstants.ButtonBGColor
        
        //        btnSubmitReview.button.isEnabled = false
        //        btnSubmitReview.button.backgroundColor = ColorConstants.ButtonBGColor.withAlphaComponent(0.5)
        
        if activeRideObject?.brandName != nil {
            viewNavigation.text = activeRideObject?.brandName as! NSString
        }
        
        viewNavigation.button.isHidden = true
        
        btnSubmitReview.click = {
            self.btnSubmitReview_Click()
        }
        
        txtView.placeholder = "KLblAddComments".localized
        txtView.placeholderColor = ColorConstants.TextColorPrimary
        
        txtView.delegate = self
        
        if arrRatting.count > 0 {
            for categoryID in arrRatting[0].allowCategory {
                
                let model = ReviewModel()
                model.id = categoryID
                model.name = SyncManager.sharedInstance.fetchRatingMasterWithId(categoryID)
                
                arrData.append(model)
            }
            lblRatingTitle.text = arrRatting[0].ratingTitle
        }
        
        collectView.isHidden = true
        viewComment.isHidden = true
        lblRatingTitle.isHidden = true
        
        
        let user = ApplicationData.user
        lblRating.text = "\(user?.firstName ?? "Martins"), \("KLblHowsRide".localized)"
        
       // lblRating.changeStringColor(string: lblRating.text ?? "", array: [("KLblWeone".localized)], colorArray: [ColorConstants.TextColorPrimary],changeFontOfString :[("KLblWeone".localized)], font : [FontScheme.kSemiBoldFont(size: 20)])
        
    }
    
    //MARK:- IBActions
    @IBAction func btnRating_Click(_ sender: UIButton) {
        self.view.endEditing(true)
        setRatting(tag: sender.tag)
    }
    
    @objc func btnSubmitReview_Click() {
        self.view.endEditing(true)
        
        //        guard !Utilities.checkStringEmptyOrNil(str: txtView.text) else {
        //            Utilities.showAlertView(message: "KLblPleaseEntercomment".localized)
        //            return
        //        }
        
        if !Utilities.checkStringEmptyOrNil(str: txtView.text) && starRating.value != 0 {
            callApiforGiveRating()
        }else{
            SyncManager.sharedInstance.syncMaster ({
                AppNavigation.shared.moveToHome(isPopAndSwitch: true)
            })
        }
    }
    
    
    @IBAction func btnBack_Click(_ sender: UIButton) {
        SyncManager.sharedInstance.syncMaster ({
            AppNavigation.shared.moveToHome(isPopAndSwitch: true)
        })
    }
    
    @IBAction func btnSkip_Click(_ sender: UIButton) {
        SyncManager.sharedInstance.syncMaster ({
            AppNavigation.shared.moveToHome(isPopAndSwitch: true)
        })
    }
    
    
    func setRatting(tag:Int) {
        
        if tag == 1 {
            btnRate1.isSelected = true
            btnRate2.isSelected = false
            btnRate3.isSelected = false
            btnRate4.isSelected = false
            btnRate5.isSelected = false
            
        }
        else if tag == 2 {
            btnRate1.isSelected = false
            btnRate2.isSelected = true
            btnRate3.isSelected = false
            btnRate4.isSelected = false
            btnRate5.isSelected = false
            
        }
        else if tag == 3 {
            btnRate1.isSelected = false
            btnRate2.isSelected = false
            btnRate3.isSelected = true
            btnRate4.isSelected = false
            btnRate5.isSelected = false
            
        }
        else if tag == 4 {
            btnRate1.isSelected = false
            btnRate2.isSelected = false
            btnRate3.isSelected = false
            btnRate4.isSelected = true
            btnRate5.isSelected = false
            
        }
        else if tag == 5 {
            btnRate1.isSelected = false
            btnRate2.isSelected = false
            btnRate3.isSelected = false
            btnRate4.isSelected = false
            btnRate5.isSelected = true
            
        }
        
        rating = tag
        if starRating.value > 0 {
            btnSubmitReview.button.isEnabled = true
            btnSubmitReview.button.backgroundColor = ColorConstants.ButtonBGColor
        }
        
    }
    
    //Star rating value changed
    @IBAction func valueChanged(_ sender: HCSStarRatingView) {
        
        arrData.removeAll()
        collectView.isHidden = false
        viewComment.isHidden = false
        lblRatingTitle.isHidden = false
        
        starRating.filledStarImage = #imageLiteral(resourceName: "starFilled")
        txtView.placeHolderLabel.text = "KLblAddComments".localized
        
        if sender.value == 1 {
            for categoryID in arrRatting[0].allowCategory {
                let model = ReviewModel()
                model.id = categoryID
                model.name = SyncManager.sharedInstance.fetchRatingMasterWithId(categoryID)
                
                arrData.append(model)
            }
            lblRatingTitle.text = arrRatting[0].ratingTitle
            
        }
        else if sender.value == 2 {
            for categoryID in arrRatting[1].allowCategory {
                let model = ReviewModel()
                model.id = categoryID
                model.name = SyncManager.sharedInstance.fetchRatingMasterWithId(categoryID)
                
                arrData.append(model)
            }
            lblRatingTitle.text = arrRatting[1].ratingTitle
            
        }
        else if sender.value == 3 {
            for categoryID in arrRatting[2].allowCategory {
                let model = ReviewModel()
                model.id = categoryID
                model.name = SyncManager.sharedInstance.fetchRatingMasterWithId(categoryID)
                
                arrData.append(model)
            }
            lblRatingTitle.text = arrRatting[2].ratingTitle
            
        }
        else if sender.value == 4 {
            for categoryID in arrRatting[3].allowCategory {
                let model = ReviewModel()
                model.id = categoryID
                model.name = SyncManager.sharedInstance.fetchRatingMasterWithId(categoryID)
                
                arrData.append(model)
            }
            lblRatingTitle.text = arrRatting[3].ratingTitle
            
        }
        else if sender.value == 5 {
            
            starRating.filledStarImage = #imageLiteral(resourceName: "starGolden")
            txtView.placeHolderLabel.text = "KLblThankyouNote".localized
            
            self.collectView.isHidden = true
            self.lblRatingTitle.isHidden = true
            
            //            for categoryID in arrRatting[4].allowCategory {
            //                let model = ReviewModel()
            //                model.id = categoryID
            //                model.name = SyncManager.sharedInstance.fetchRatingMasterWithId(categoryID)
            //
            //                arrData.append(model)
            //            }
            //            lblRatingTitle.text = arrRatting[4].ratingTitle
        }
        
        collectView.reloadData()
        
        
        //        if sender.value > 0 {
        //            btnSubmitReview.button.isEnabled = true
        //            btnSubmitReview.button.backgroundColor = ColorConstants.ButtonBGColor
        //        }else{
        //            btnSubmitReview.button.isEnabled = false
        //            btnSubmitReview.button.backgroundColor = ColorConstants.ButtonBGColor.withAlphaComponent(0.5)
        //        }
    }
}

//MARK: Call api
extension RideRatingVC{
    
    func callApiforGiveRating(){
        
        
        
        /*  "ratings": [{
         "rating": 3,
         "ratingCategory": ["5e70772bc2c440283a69ac19", "5e70773fc2c440283a69ac1b"],
         "ratingType": "5d8e14ddd3164d7dc8c9e9a7"
         }], */
        
        var param : [String:Any] = [:]
        param["to"] = activeRideObject?.vehicleId?.id
        param["rideId"] = activeRideObject?.id
        
        
        var request : [String:Any] = [:]
        request["rating"] = starRating.value
        let parent = SyncManager.sharedInstance.fetchMasterWithCode(AppConstants.MasterCode.RatingType).first
        
        request["ratingType"] = SyncManager.sharedInstance.fetchMasterWithId(parent?.id ?? "").first?.id
        request["ratingCategory"] = arrSelectedID
        
        param["ratings"] = [request]
        
        if txtView.text.count > 0{
            param["review"] = txtView.text
        }
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.GiveRating, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            SyncManager.sharedInstance.syncMaster ({
                AppNavigation.shared.moveToHome(isPopAndSwitch: true)
            })
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}

//Textview
extension RideRatingVC : UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        
        
        if !text.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        
        if text == "\n"{
            textView.resignFirstResponder()
        }
        
        if range.location == 0{
            
            if text == " " {
                return false
            }
        }
        
        return true
    }
}
extension RideRatingVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
//        let model = arrData[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RatingCell", for: indexPath) as! RatingCell
//
//        if arrselectedIndex.contains(indexPath.row) {
//            model.isSelected = true
//        } else {
//            model.isSelected = false
//        }
//
//        cell.setData(model)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        if arrselectedIndex.contains(indexPath.row) {
//            if let index = arrselectedIndex.firstIndex(of: indexPath.row) {
//                arrselectedIndex.remove(at: index)
//                arrSelectedID.remove(at: index)
//            }
//        } else {
//            arrselectedIndex.append(indexPath.row)
//            arrSelectedID.append(arrData[indexPath.row].id ?? "")
//        }
        
        collectionView.reloadData()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let model = arrData[indexPath.row]
        return CGSize(width: 40, height: 40)
        //return CGSize(width: Utilities.getLabelWidth(constraintedHeight: 31, font: FontScheme.kSemiBoldFont(size: 12), text: model.name ?? "") + 20, height: 31)
    }
    
    
}
