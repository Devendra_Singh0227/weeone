//
//  SettingsVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 24/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class NotificationSettingsVC: UIViewController {
    
    //MARK: - Variables
    var arrData = [CellModel]()
    var arrStaticData = [StaticModel]()

    var saticPageCode = ""
    
    var isFromLegal : Bool = false
    var isFromSettings: Bool = false
    
    //MARK: - Outlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNavigation: UIViewCommon!

    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }

    //MARK: Private Methods
    func initialConfig() {
        tblView.register(UINib(nibName: "SettingCell", bundle: nil), forCellReuseIdentifier: "SettingCell")
        tblView.register(UINib(nibName: "SettingDescCell", bundle: nil), forCellReuseIdentifier: "SettingDescCell")

        
        viewNavigation.click = {
            self.btnClose_Click()
        }

            viewNavigation.text = "KNotifications".localized as NSString
            preapareDataSource()
        
    }
    
    func preapareDataSource() {
        
        let userSettings = SyncManager.sharedInstance.fetchAllUserSettings().first
        
        arrData.removeAll()
        
        arrData.append(CellModel.getModel(placeholder: "KAccountAndRide".localized, placeholder2: "KForExample".localized,  type: .NotificationAccountAndRide, classType: .NotificationSettingsVC))
        arrData.append(CellModel.getModel(placeholder: "KPushNotifications".localized, type: .NotificationAccountPush, classType: .NotificationSettingsVC, isSelected: userSettings?.isAccAndRideUpdate?.isPushNotification ?? false))
            arrData.append(CellModel.getModel(placeholder: "KTextMessages".localized, type: .NotificationAccountText, classType: .NotificationSettingsVC, isSelected: userSettings?.isAccAndRideUpdate?.isSMS ?? false))

        arrData.append(CellModel.getModel(placeholder: "KDiscountsandNews".localized, placeholder2: "KFeatures".localized,  type: .NotificationDiscountAndNews, classType: .NotificationSettingsVC))
        arrData.append(CellModel.getModel(placeholder: "KPushNotifications".localized, type: .NotificationDiscountPush, classType: .NotificationSettingsVC, isSelected: userSettings?.isNewsAndDisUpdate?.isPushNotification ?? false))
        arrData.append(CellModel.getModel(placeholder: "KTextMessages".localized, type: .NotificationDiscountText, classType: .NotificationSettingsVC, isSelected: userSettings?.isNewsAndDisUpdate?.isSMS ?? false))
        arrData.append(CellModel.getModel(placeholder: "KLblEmail".localized, type: .NotificationDiscountEmail, classType: .NotificationSettingsVC, isSelected: userSettings?.isNewsAndDisUpdate?.isEmail ?? false))

        
        tblView.reloadData()
    }
        
    //MARK:- IBActions
     func btnClose_Click() {
        callAPIForUpsert()
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK: - UITableView's DataSource & Delegate Methods
extension NotificationSettingsVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = arrData[indexPath.row]
        
        switch model.cellType {
        case .NotificationAccountAndRide, .NotificationDiscountAndNews:
            return UITableView.automaticDimension
            
        default:
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = arrData[indexPath.row]
        
        switch model.cellType {
        case .NotificationAccountAndRide, .NotificationDiscountAndNews:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingDescCell") as? SettingDescCell
            cell?.setData(model: model)
            return cell!
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as? SettingCell
            cell?.setData(model: model)
            return cell!
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func getRequest() -> [String:Any] {
     
        var accountAndRide: [String:Any] = [:]
        var newsAndDisUpdate: [String:Any] = [:]

        for model in arrData {

            if model.cellType == .NotificationAccountPush {
                accountAndRide["isPushNotification"] = model.isSelected
            } else if model.cellType == .NotificationAccountText {
                accountAndRide["isSms"] = model.isSelected
            } else if model.cellType == .NotificationDiscountPush {
                newsAndDisUpdate["isPushNotification"] = model.isSelected
            } else if model.cellType == .NotificationDiscountText {
                newsAndDisUpdate["isSms"] = model.isSelected
            } else if model.cellType == .NotificationDiscountEmail {
                newsAndDisUpdate["isEmail"] = model.isSelected
            }
            
        }
        
        var param : [String:Any] = [:]
        param["isAccAndRideUpdate"] = accountAndRide
        param["isNewsAndDisUpdate"] = newsAndDisUpdate

        return param
        
    }
    
}

//MARK: Call api
extension NotificationSettingsVC{
    
    func callAPIForUpsert() {
        
        let param = getRequest()
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.Upsert, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any] {
                SyncManager.sharedInstance.insertUserSettings(list: [dictResponse])
            }
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
}
