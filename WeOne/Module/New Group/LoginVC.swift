//
//  LoginVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 23/09/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    
    //MARK: - Variables
    var previousTheme = ThemeManager.DefaultTheme
    var isFromMenu = false
    //MARK: - Outlets
    
    @IBOutlet weak var imgLogo: UIImageView!
    
    @IBOutlet weak var txtEmail: UITextFiledCommon!
    @IBOutlet weak var txtPassword: UITextFiledCommon!
    @IBOutlet weak var btnLogin: UIButtonCommon!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnCloseHeight: NSLayoutConstraint!
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }

    //MARK: Private Methods
    func initialConfig() {
        
        //        txtEmail.setText(text: "mayuramipara1@gmail.com")
        //        txtPassword.setText(text: "Mayur@123")
        
        imgLogo.image = UIImage(named : ThemeManager.sharedInstance.getImage(string: "appLogo"))
        
        if ThemeManager.sharedInstance.RideType != ThemeManager.DefaultTheme {
            previousTheme = ThemeManager.sharedInstance.RideType
        }
        btnClose.isHidden = AppNavigation.shared.isRequiredLogin(AppConfigurationModel.shared.accessConfig.requiredToUseApp)
        btnSignup.layer.cornerRadius = btnSignup.frame.height / 2
        btnSignup.addTarget(self, action: #selector(holdDown(_:)), for: .touchDown)
        
        btnLogin.click = {
            self.btnSubmit_Click()
        }
    }
    
    //MARK:- IBActions
    func btnSubmit_Click() {
        self.view.endEditing(true)
        
        let (validate, req) = getrequestAndValidate()
        if validate {
            callApiForLogin(req: req)
        }
    }
    
    @IBAction func btnForgotPassword_Click(_ sender: UIButton) {
        self.view.endEditing(true)
        let vc = ForgotPasswordVC(nibName: "ForgotPasswordVC", bundle: nil)
        vc.strEmail = txtEmail.getText()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSignup_Click(_ sender: UIButton) {
        btnSignup.setTitleColor(ColorConstants.TextColorPrimary, for: .normal)
        
        if previousTheme == ThemeManager.DefaultTheme  {
            ThemeManager.sharedInstance.RideType = AppConstants.RideType.Schedule
        }
        
        self.view.endEditing(true)
        let vc = SignUpVC(nibName: "SignUpVC", bundle: nil)
        vc.isSuccess = {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                if self.navigationController != nil{
                    for viewController in (self.navigationController?.viewControllers)!{
                        if viewController.isKind(of: LoginVC.self) {
                            self.navigationController?.viewControllers.remove(at: (self.navigationController?.viewControllers.firstIndex(of: viewController))!)
                        }
                    }
                }
            })
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func holdDown(_ sender: UIButton) {
        btnSignup.setTitleColor(ColorConstants.TextColorSecondary, for: .normal)
    }
    
    @IBAction func btnClose_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}


//MARK:- API Calling
extension LoginVC {
    
    func callApiForLogin(req:[String:Any]) {
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.Login, method: .post, parameters: req, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any]{
                ApplicationData.sharedInstance.saveLoginData(data: dictResponse)
                SyncManager.sharedInstance.upsertPlayerId()
            }
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateData"), object: nil, userInfo: nil)

            if AppNavigation.shared.checkUserHasAccess(vc: self) {
                if self.isFromMenu {
                    AppNavigation.shared.moveToHome()
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}

//MARK:- Extra Methods
extension LoginVC {
    
    func getrequestAndValidate() -> (Bool, [String:Any]) {
        var req = [String:Any]()
        
        guard !Utilities.checkStringEmptyOrNil(str: txtEmail.getText()) else {
            self.showToast(text: "KLblEnterEmail".localized)

//            Utilities.showAlertView(message: "KLblEnterEmail".localized)
            return (false,[:])
        }
        
        guard txtEmail.getText()?.isValidEmail() == true else {
            self.showToast(text: "KLblEnterValidEmail".localized)

//            Utilities.showAlertView(message: "KLblEnterValidEmail".localized)
            return (false,[:])
        }
        
        guard !Utilities.checkStringEmptyOrNil(str: txtPassword.getText()) else {
            self.showToast(text: "KLblEnterPassword".localized)

//            Utilities.showAlertView(message: "KLblEnterPassword".localized)
            return (false,[:])
        }
        
        req["username"] = txtEmail.getText()
        req["password"] = txtPassword.getText()
        
        return (true, req)
    }
}
