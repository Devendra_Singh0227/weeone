//
//  DefaultPaymentVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 12/06/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class DefaultPaymentVC: UIViewController {
    
    //MARK: Variables
    var arrData = [CellModel]()
    var arrCard = [CardModel]()
    
    var profileType = ProfileType.Personal
    var isLongPressBegan = false
    var isFromReservation = false

    var addClick: ((_ isFromAddClick: Bool)->())?
    
    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewHeightSelectAll: NSLayoutConstraint!
    @IBOutlet weak var viewNavigation: UIViewCommon!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    //MARK: View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
    }
    
    //MARK: Initial config
    func initialConfig(){
        
        tableView.register(UINib(nibName: "PaymentSectionCell", bundle: nil), forCellReuseIdentifier: "PaymentSectionCell")
        tableView.register(UINib(nibName: "PaymentCardCell", bundle: nil), forCellReuseIdentifier: "PaymentCardCell")
        tableView.register(UINib(nibName: "PaymentAddCell", bundle: nil), forCellReuseIdentifier: "PaymentAddCell")
        
        prepareDataSource()
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPress(_:)))
        self.view.addGestureRecognizer(longPressRecognizer)
        
        if isFromReservation {
            viewNavigation.text = "KLblLMPayment".localized as NSString
        }
        
        viewNavigation.click = {
            self.btnBack_Click()
        }
        
        btnDelete.isHidden = true
    }
    
    //MARK: Prepare datasource
    func prepareDataSource(){
        
        self.viewHeightSelectAll.constant = 0
        btnDelete.isHidden = true
        
        arrData.removeAll()
        
        //cards
        arrData.append(CellModel.getModel(placeholder: "KLblCards".localized, text: "", type: .PaymentSection))
        
        arrCard = SyncManager.sharedInstance.fetchAllCards()
        
        for model in arrCard {
            arrData.append(CellModel.getModel(placeholder: model.getFormattedCardNumber(), text: "", type: .PaymentCard, imageName: Utilities.getCreditCardImage(cardType: model.brand ?? ""), cellObj: model, isSelected2: model.isPrimary))
            
        }
        
        arrData.append(CellModel.getModel(placeholder: "KLblAddNewCard".localized, text: "", type: .PaymentAddNewCard))
        
        tableView.reloadData()
    }
    
    //Called, when long press occurred
    @objc func longPress(_ sender: UILongPressGestureRecognizer) {
        
        if sender.state == UIGestureRecognizer.State.began {
            let touchPoint = sender.location(in: self.tableView)
            if let indexPath = tableView.indexPathForRow(at: touchPoint) {
                
                let model = arrData[indexPath.row]
                if model.cellType == .PaymentCard {
                    print("Long press")
                    btnDelete.isHidden = false
                    isLongPressBegan = true
                    showViewSelectAll(true)
                    self.tableView.reloadData()
                }
            }
        }
        
    }
    
    //MARK: button action methodss
    @IBAction func btnDelete_Click(_ sender: UIButton) {
        
        let arr = arrData.map { $0.isSelected == true && $0.cellType == .PaymentCard}
        
        var title = ""
        
        if arr.count > 0 {
            title = "KLblDeleteCardConfirmation".localized
        } else {
            title = "KLbRemoveCardConfirmation".localized
        }
        
        UIViewController.current()?.view.showConfirmationPopupWithMultiButton(viewController : self, title: "", message: title, cancelButtonTitle: StringConstants.ButtonTitles.kNo, confirmButtonTitle: StringConstants.ButtonTitles.kYes, onConfirmClick: {
            self.callApiForDeleteCard()
        }, onCancelClick: {
            // cancel
        })
    }
    
    @IBAction func btnSelectAll_Click(_ sender: UIButton) {
        if btnSelect.isSelected{
            
            btnSelect.isSelected = false
            btnDelete.isHidden = !btnSelect.isSelected
            arrData.forEach { $0.isSelected = false}
            tableView.reloadData()
        }else{
            
            btnSelect.isSelected = true
            btnDelete.isHidden = !btnSelect.isSelected
            arrData.forEach { ($0.cellType == .PaymentCard) ? ($0.isSelected = true) : ($0.isSelected = false)}
            tableView.reloadData()
        }
    }
    
    func btnBack_Click(){
        if isLongPressBegan {
            showViewSelectAll(false)
            isLongPressBegan = false
            self.tableView.reloadData()
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func showViewSelectAll(_ value : Bool){
        if value {
            self.viewHeightSelectAll.constant = 60
        }else{
            self.viewHeightSelectAll.constant = 0
        }
    }
}

//MARK: Tableview methods
extension DefaultPaymentVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrData[indexPath.row]
        
        switch model.cellType! {
        case .PaymentSection:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentSectionCell") as? PaymentSectionCell
            cell?.setData(model : model, isLongPress: isLongPressBegan)
            return cell!
            
        case .PaymentCard:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCardCell") as? PaymentCardCell
//            cell?.setDataForDefaultPayment(model, isLongPress: isLongPressBegan)
//            cell?.btnSelect.addTarget(self, action: #selector(btnSelectCard_Click), for: .touchUpInside)
//            cell?.btnSelect.tag = indexPath.row
            return cell!
            
        case .PaymentAddNewCard:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentAddCell") as? PaymentAddCell
            cell?.setData(model, isLongPress: isLongPressBegan)
            return cell!
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let model = arrData[indexPath.row]
        
        switch model.cellType! {
        case .PaymentAddNewCard:
            let vc = AddCardViewController()
            if isFromReservation {
                AppNavigation.shared.currentStateList = AppNavigation.shared.config.accessConfig.requiredForReservation
            } 

            if profileType == .Personal {
                vc.cardType = AppConstants.CardTypes.PersonalCard
            } else {
                vc.cardType = AppConstants.CardTypes.JobCard
            }
            
            vc.addClick = {
                self.addClick?(true)
                self.prepareDataSource()
            }
            
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
            break
        case .PaymentCard:
            if !isLongPressBegan {
                callAPIForSetPrimaryCard(cardModel: arrCard[indexPath.row - 1])
            } else {
                let model = arrData[indexPath.row]
                model.isSelected = !model.isSelected
                self.tableView.reloadData()
                
                let filter = arrData.filter { $0.isSelected == true}
                
                if filter.count > 0{
                    btnDelete.isHidden = false
                } else {
                    btnDelete.isHidden = true
                }
                
                if filter.count == arrData.filter({$0.cellType == .PaymentCard}).count{
                    btnSelect.isSelected = true
                } else {
                    btnSelect.isSelected = false
                }
            }
            
            break
        default:
            break
        }
    }
    
    @objc func btnSelectCard_Click(_ sender : UIButton){
        let model = arrData[sender.tag]
        model.isSelected = !model.isSelected
        self.tableView.reloadData()
        
        let filter = arrData.filter { $0.isSelected == true}
        
        if filter.count > 0{
            btnDelete.isHidden = false
        } else {
            btnDelete.isHidden = true
        }
        
        if filter.count == arrData.filter({$0.cellType == .PaymentCard}).count{
            btnSelect.isSelected = true
        } else {
            btnSelect.isSelected = false
        }
    }
}

//MARK: Api calls
extension DefaultPaymentVC {
    
    func callAPIForSetPrimaryCard(cardModel:CardModel) {
        
        var param = [String:Any]()
        param["cardId"] = cardModel.id ?? ""
        if profileType == .Personal {
            param["cardType"] = AppConstants.CardTypes.PersonalCard
        } else {
            param["cardType"] = AppConstants.CardTypes.JobCard
        }
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.SetDefaultCard, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let responseDict = response as? [String:Any] {
                    if let arrCard = responseDict["cards"] as? [[String:Any]] {
                        SyncManager.sharedInstance.insertCards(list: arrCard)
                    }
            }
            
           // NotificationCenter.default.post(name: Notification.Name(rawValue: "updateData"), object: nil, userInfo: nil)

//            SyncManager.sharedInstance.setPrimaryCard(cardModel: cardModel)
            self.addClick?(false)
            self.prepareDataSource()
            
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func callApiForDeleteCard() {
        
        let filter = arrData.filter { $0.isSelected == true && $0.cellType == .PaymentCard}
        var param = [String:Any]()
        param["cardIds"] = filter.map { ($0.cellObj as? CardModel)?.id ?? ""}
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.DeleteCard, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
    
            SyncManager.sharedInstance.deleteCards(filter.map { ($0.cellObj as? CardModel)?.id ?? ""})
            self.showViewSelectAll(false)
            self.isLongPressBegan = false
            self.prepareDataSource()
    
            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateData"), object: nil, userInfo: nil)

            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
