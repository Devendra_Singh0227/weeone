//
//  InviteEmployees_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 19/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class InviteEmployees_ViewController: UIViewController,  UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var table_View: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
        loadNib()
    }
    
    func initialConfig(){
        
        if AppConstants.hasSafeArea {
            headerView_HeightConstraint.constant = 90
        }
        else {
            headerView_HeightConstraint.constant = 74
        }
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func loadNib() {
        table_View.register(UINib(nibName: "Profile_TableViewCell", bundle: nil), forCellReuseIdentifier: "Profile_TableViewCell")
        table_View.register(UINib(nibName: "Invite_TableViewCell", bundle: nil), forCellReuseIdentifier: "Invite_TableViewCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6//arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Profile_TableViewCell") as? Profile_TableViewCell
//            cell?.default_PaymentLbl.text = "How frequently do you want to receive your expense report?"
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Invite_TableViewCell") as? Invite_TableViewCell
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 310
        } else {
            return 88
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        if indexPath.row == 0 {
            
        } else {
            let vc = ConfirmPopupVC()
            
            //        vc.confirmClicked = {
            //            let VC = KeyPickup_InPerson_ViewController()
            //            self.navigationController?.pushViewController(VC, animated: true)
            //        }
            //
            //        vc.cancelBtnClicked = {
            //            self.dismiss(animated: true, completion: nil)
            //        }
            vc.headerTitle = "Remove Employee"
            vc.message = "Are you sure you want to remove"
            vc.name = "Priyanshi Jaiswal?"
            vc.modalPresentationStyle = .custom
            vc.modalTransitionStyle = .crossDissolve
            
            if UIViewController.current()?.presentedViewController != nil {
                UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
                })
            }
            else{
                UIViewController.current()?.present(vc, animated: true, completion: {
                })
            }
        }
        
    }
}
