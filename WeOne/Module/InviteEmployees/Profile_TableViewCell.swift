//
//  Profile_TableViewCell.swift
//  WeOne
//
//  Created by Dev's Mac on 19/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Profile_TableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var employe_name_View: ViewDesign!
    @IBOutlet weak var employe_email_View: ViewDesign!
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        nameTxtField.delegate = self
        emailTxtField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == nameTxtField {
            employe_name_View.borderwidth = 2
            employe_name_View.backgroundColor = UIColor.white
        } else {
            employe_email_View.borderwidth = 2
            employe_email_View.backgroundColor = UIColor.white
        }
    }
    
}
