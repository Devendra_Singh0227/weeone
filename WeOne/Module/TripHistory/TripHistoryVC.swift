//
//  TripHistoryVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 11/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class TripHistoryVC: UIViewController {
    
    //MARK: - Variables
    var arrUpComingRide = [RideListModel]()
    var arrPastRide = [RideListModel]()
    var rideType : RideType = .Upcoming
    var arrFilter : [CellModel] = [CellModel]()
    var isFilterShown : Bool = false
    var isFromSupport : Bool = false

    //MARK: - Outlets
    @IBOutlet weak var tableview: UITableView!

    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
    }
    
    //MARK: Initial config
    func initialConfig(){
        
        if isFromSupport {
            rideType = .Past
        }
        
        tableview.register(UINib(nibName: "TripHistoryCell", bundle: nil), forCellReuseIdentifier: "TripHistoryCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(callAPINotification(_:)), name: NSNotification.Name(rawValue: "ApiCall"), object: nil)
        

        //callAPIForRideList(isShowLoader: true)
        
        tableview.es.addPullToRefresh {
            self.callAPIForRideList(isShowLoader: true)
        }
    
    }
        
    
    
    //MARK: Button click methods
    func btnClose_Click() {
        self.navigationController?.popViewController(animated: true)
    }
    

    
    //MARK: Private methods
    func reloadData(_ message : String, cardType: Int) {
            tableview.es.stopPullToRefresh()
            if arrUpComingRide.count == 0 {
                if cardType == AppConstants.CardTypes.All {
                    tableview.loadNoDataFoundView(message: StringConstants.Nodata.KNoRide, image: "Ride") {
                        
                    }
                } else if cardType == AppConstants.CardTypes.PersonalCard {
                    tableview.loadNoDataFoundView(message: StringConstants.Nodata.KNoPersonalRide, image: "user") {
                        
                    }
                } else {
                    tableview.loadNoDataFoundView(message: StringConstants.Nodata.KNoBussinessRide, image: "work1") {
                        
                    }
                }
            } else {

                tableview.removeLoadAPIFailedView()
            }
            tableview.reloadData()
    }
    
}

//MARK: Tableview methods
extension TripHistoryVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                
        if tableView == tableview {
            return arrUpComingRide.count
        }
        return 0
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
            let model = arrUpComingRide[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "TripHistoryCell", for: indexPath) as! TripHistoryCell
            cell.setData(model: model, rideType: rideType)
            return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isFromSupport {
            let vc = IssueWithLastTripVC()
            vc.rideModel = arrUpComingRide[indexPath.row]
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)

        } else {
            
            if rideType != .Upcoming && arrUpComingRide[indexPath.row].status != AppConstants.RideStatus.CANCELLED {
                
                if arrUpComingRide[indexPath.row].rideType == AppConstants.RideType.Schedule {
                    ThemeManager.sharedInstance.RideType = AppConstants.RideType.Schedule
                } else {
                    ThemeManager.sharedInstance.RideType = AppConstants.RideType.Instant
                }
                
                let vc = FareSummaryVC(nibName: "FareSummaryVC", bundle: nil)
                vc.rideModel = arrUpComingRide[indexPath.row]
                vc.isFromYourTrip = true
                UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
        
    }
}

//MARK:- API Calling
extension TripHistoryVC {
    
    @objc func callAPINotification(_ notification: NSNotification) {
        if let type = notification.object as? RideType {
            rideType = type
            callAPIForRideList()
        }

        
    }
    func callAPIForRideList(isShowLoader:Bool = true, rideType: RideType = .Upcoming, cardType: Int = 0) {
                
        var req = [String:Any]()
        req["filterType"] = self.rideType.rawValue
        
        if cardType != AppConstants.CardTypes.All {
            req["cardType"] = cardType
        }
        
        print("req\(req)")

        if isShowLoader {
            NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        }
        
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.RideList, method: .post, parameters: req, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any] {
                
                if let list = dictResponse["list"] as? [[String:Any]] {
                        self.arrUpComingRide = Mapper<RideListModel>().mapArray(JSONArray: list)
                    self.reloadData(message, cardType: cardType)
                }
            }
        }) { (failureMessage, failureCode) in
            self.reloadData(failureMessage, cardType: cardType)
        }
    }
}
