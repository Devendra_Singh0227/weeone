//
//  PriceBreakUpCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 13/01/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class PriceBreakUpCell: UITableViewCell {

    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var lblText: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(_ model : CellModel){
        lblPlaceholder.text = model.placeholder
        lblText.text = model.userText
        
        if model.isSelected {
            lblPlaceholder.textColor = ColorConstants.TextColorTheme
            lblText.textColor = ColorConstants.TextColorTheme
        }else{
            lblPlaceholder.textColor = ColorConstants.TextColorSecondary
            lblText.textColor = ColorConstants.TextColorTheme
        }
    }
}
