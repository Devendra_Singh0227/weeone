//
//  PriceBreakUpVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 11/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class PriceBreakUpVC: UIViewController {
    
    //MARK: - Variables
    var rideListModel:RideListModel = RideListModel()
    var arrCellData : [CellModel] = [CellModel]()
    var rideType : RideType = .Past
    
    //MARK: - Outlets
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblDistanceUnit: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblDurationUnit: UILabel!
    
    @IBOutlet weak var lblFinal: UILabel!
    @IBOutlet weak var lblCardNo: UILabel!
    @IBOutlet weak var imgCardImage: UIImageView!
    
    @IBOutlet weak var lblRideSummary: UILabel!
    @IBOutlet weak var lblInsurance: UILabel!
    @IBOutlet weak var lblDeposite: UILabel!
    @IBOutlet weak var lblAdvancePaidAmt: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblTotalAmt: UILabel!
    
    @IBOutlet weak var vwRideSummary: UIView!
    @IBOutlet weak var vwInsurance: UIView!
    @IBOutlet weak var vwDeposite: UIView!
    @IBOutlet weak var vwAdvancePaidAmt: UIView!
    @IBOutlet weak var vwDiscount: UIView!
    @IBOutlet weak var vwTotalAmt: UIView!
    @IBOutlet weak var lblExtraDistance: UILabel!
    @IBOutlet weak var lblAdditionatlRate: UILabel!
    @IBOutlet weak var lblFuelCharges: UILabel!
    @IBOutlet weak var lblParkingRate: UILabel!
    @IBOutlet weak var lblTax: UILabel!
    @IBOutlet weak var viewExtraDis: UIView!
    @IBOutlet weak var viewAdditionalRate: UIView!
    @IBOutlet weak var viewFuelCharge: UIView!
    @IBOutlet weak var viewParkingRate: UIView!
    @IBOutlet weak var viewTax: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintHeightTableView: NSLayoutConstraint!
    
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
        
        tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        tableView.register(UINib(nibName: "PriceBreakUpCell", bundle: nil), forCellReuseIdentifier: "PriceBreakUpCell")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        tableView.removeObserver(self, forKeyPath: "contentSize")
        super.viewWillDisappear(true)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if(keyPath == "contentSize"){
            if let newvalue = change?[.newKey]
            {
                let newsize  = newvalue as! CGSize
               constraintHeightTableView.constant = newsize.height
            }
        }
    }
    
    //MARK: Private Methods
    func initialConfig() {
        
        let (distance,distanceUnit) = rideListModel.getTotalDistance()
        lblDistance.text = distance
        lblDistanceUnit.text = distanceUnit
        
        let (time,timeUnit) = rideListModel.getTotalDuration()
        lblDuration.text = time
        lblDurationUnit.text = timeUnit
        
        lblTotalAmt.text = rideListModel.getTotalFare()
        
        
        lblFinal.text = rideListModel.getTotalFare()
        
        if let cardDetails = rideListModel.selectedCard {
            lblCardNo.isHidden = false
            imgCardImage.isHidden = false
            lblCardNo.text = cardDetails.getFormattedCardNumber()
            imgCardImage.image = UIImage(named: Utilities.getCreditCardImage(cardType: cardDetails.brand ?? ""))
        }
        else {
            lblCardNo.isHidden = true
            imgCardImage.isHidden = true
        }
        
        if rideType == .Past{
            if rideListModel.status == AppConstants.RideStatus.COMPLETED{
                
                getStartDate(true)
                getEndDate(true)
            }else{
                getStartDate(false)
                getEndDate(false)
                
                if rideListModel.startDateTime == nil && rideListModel.endDateTime == nil{
                    
                    getReservedStartDate()
                    getReservedEndDate()
                }
            }
        }else{
            
            getReservedStartDate()
            getReservedEndDate()
        }
        
        if rideListModel.status == AppConstants.RideStatus.CANCELLED{
            getCancelledDate()
        }
        
        if rideListModel.fareSummary?.refundAmount ?? 0.0 != 0.0{
            arrCellData.append(CellModel.getModel(placeholder : "KLblRefundAmount".localized, text : rideListModel.getRefundAmount(), type: CellType.PriceBreakUpCell))
        }
        
        //if rideListModel.rideType == AppConstants.RideType.Instant{
            
            if rideListModel.fareSummary?.selectedPackageRate ?? 0.0 != 0.0{
                
                arrCellData.append(CellModel.getModel(placeholder : "KLblPackage".localized, text : Utilities.convertToCurrency(number:  rideListModel.fareSummary?.selectedPackageRate ?? 0, currencyCode: rideListModel.pricingConfigData?.currency), type: CellType.PriceBreakUpCell))
                
            }
        //}else
            
        //Schedule ride pricing package condition
       // if rideListModel.rideType == AppConstants.RideType.Schedule{
            
            if rideListModel.fareSummary?.calculatedPackageRate ?? 0.0 != 0.0{
                
                arrCellData.append(CellModel.getModel(placeholder : "KLblTotalPackageRate".localized, text : Utilities.convertToCurrency(number:  rideListModel.fareSummary?.calculatedPackageRate ?? 0, currencyCode: rideListModel.pricingConfigData?.currency), type: CellType.PriceBreakUpCell))
                
            }
       // }
        
        if rideListModel.fareSummary?.deposit ?? 0.0 != 0.0{
            arrCellData.append(CellModel.getModel(placeholder : "KLblDeposit".localized, text : rideListModel.getDeposit(), type: CellType.PriceBreakUpCell))
        }
        
        if rideListModel.fareSummary?.insurance ?? 0.0 != 0.0{
            arrCellData.append(CellModel.getModel(placeholder : "KLblInsurance".localized, text : rideListModel.getInsurance(), type: CellType.PriceBreakUpCell))
        }
        
        if rideType == .Past{
            if rideListModel.status == AppConstants.RideStatus.CANCELLED{
                
                if rideListModel.fareSummary?.advancedAmount ?? 0.0 != 0.0{
                    arrCellData.append(CellModel.getModel(placeholder : "KLblAdvancePaidAmount".localized, text : rideListModel.getAdvanceAmount(), type: CellType.PriceBreakUpCell))
                }
            }
           
            
            if rideListModel.fareSummary?.extraDistnce ?? 0.0 != 0.0{
                arrCellData.append(CellModel.getModel(placeholder : "KLblExtraDistance".localized, text : rideListModel.getExtraDistance(), type: CellType.PriceBreakUpCell))
            }
            
            if rideListModel.fareSummary?.addtionalRate ?? 0.0 != 0.0{
                       arrCellData.append(CellModel.getModel(placeholder : "KLblExtraDistanceFare".localized, text : rideListModel.getAdditionalRate(), type: CellType.PriceBreakUpCell))
                   }
            
            
            if rideListModel.fareSummary?.fuelCharges ?? 0.0 != 0.0{
                       arrCellData.append(CellModel.getModel(placeholder : "KLblFuelCharges".localized, text : rideListModel.getFuleCharge(), type: CellType.PriceBreakUpCell))
                   }
            
            
            if rideListModel.fareSummary?.parkingRate ?? 0.0 != 0.0{
                arrCellData.append(CellModel.getModel(placeholder : "KLblParkingRate".localized, text : rideListModel.getParkingRate(), type: CellType.PriceBreakUpCell))
            }
            
            
            if rideListModel.fareSummary?.tax ?? 0.0 != 0.0{
                arrCellData.append(CellModel.getModel(placeholder : "KLblTax".localized, text : rideListModel.getTax(), type: CellType.PriceBreakUpCell))
            }
            
            if rideListModel.fareSummary?.discount ?? 0.0 != 0.0{
                arrCellData.append(CellModel.getModel(placeholder : "KLblDiscount".localized, text : rideListModel.getDiscount(), type: CellType.PriceBreakUpCell))
            }
            
        }else{
            if rideListModel.fareSummary?.advancedAmount ?? 0.0 != 0.0{
                arrCellData.append(CellModel.getModel(placeholder : "KLblAdvancePaidAmount".localized, text : rideListModel.getAdvanceAmount(), type: CellType.PriceBreakUpCell))
            }
        }
        
        if rideListModel.totalFare != 0.0{
            arrCellData.append(CellModel.getModel(placeholder : "KLblTotalAmount".localized, text : rideListModel.getTotalFare(), type: .PriceBreakUpCell, isSelected : true))
        }
    }
    
    func getStartDate(_ isForReserveTime : Bool = false){
        
        if rideListModel.startDateTime != nil{
            
            let startDate = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: rideListModel.startDateTime ?? ""), format: DateUtilities.DateFormates.kCarSchedule)
               
            let startTime = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: rideListModel.startDateTime ?? ""), format: DateUtilities.DateFormates.kCarScheduleTime)
            
            arrCellData.append(CellModel.getModel(placeholder : isForReserveTime ? "KLblReservationStartTime".localized : "KStartTime".localized, text : "\(startDate) at \(startTime)", type: .PriceBreakUpCell))
        }
    }
    
    func getEndDate(_ isForReserveTime : Bool = false){
        
        if rideListModel.endDateTime != nil{
            
            let startDate = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: rideListModel.endDateTime ?? ""), format: DateUtilities.DateFormates.kCarSchedule)
               
            let startTime = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: rideListModel.endDateTime ?? ""), format: DateUtilities.DateFormates.kCarScheduleTime)
            
            arrCellData.append(CellModel.getModel(placeholder : isForReserveTime ? "KLblReservationEndTime".localized : "KEndTime".localized, text : "\(startDate) at \(startTime)", type: .PriceBreakUpCell))
        }
    }
    
    func getReservedStartDate(_ isForReserveTime : Bool = false){
        
        if rideListModel.reservedDateTime != nil{
            
            let startDate = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: rideListModel.reservedDateTime ?? ""), format: DateUtilities.DateFormates.kCarSchedule)
               
            let startTime = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: rideListModel.reservedDateTime ?? ""), format: DateUtilities.DateFormates.kCarScheduleTime)
            
            arrCellData.append(CellModel.getModel(placeholder : "KLblReservationStartTime".localized, text : "\(startDate) at \(startTime)", type: .PriceBreakUpCell))
        }
    }
    
    func getReservedEndDate(){
        
        if rideListModel.reservedEndDateTime != nil{
            
            let startDate = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: rideListModel.reservedEndDateTime ?? ""), format: DateUtilities.DateFormates.kCarSchedule)
               
            let startTime = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: rideListModel.reservedEndDateTime ?? ""), format: DateUtilities.DateFormates.kCarScheduleTime)
            
            arrCellData.append(CellModel.getModel(placeholder : "KLblReservationEndTime".localized , text : "\(startDate) at \(startTime)", type: .PriceBreakUpCell))
        }
    }
    
    func getCancelledDate(){
        
        if rideListModel.cancelDateTime != nil{
            
            let startDate = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: rideListModel.cancelDateTime ?? ""), format: DateUtilities.DateFormates.kCarSchedule)
               
            let startTime = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: rideListModel.cancelDateTime ?? ""), format: DateUtilities.DateFormates.kCarScheduleTime)
            
            arrCellData.append(CellModel.getModel(placeholder : "KLblCancelledTime".localized , text : "\(startDate) at \(startTime)", type: .PriceBreakUpCell))
        }
    }
    
    //MARK:- IBActions
    @IBAction func btnDismiss_Click(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: Tableview methods
extension PriceBreakUpVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCellData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrCellData[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PriceBreakUpCell", for: indexPath) as! PriceBreakUpCell
        cell.setCellData(model)
        return cell
    }
}
