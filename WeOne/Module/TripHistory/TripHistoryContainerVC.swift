//
//  TripHistoryContainerVC.swift
//  WeOne
//
//  Created by iMac on 13/03/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class TripHistoryContainerVC: UIViewController {
    
    @IBOutlet weak var viewPlaceHolder: UIView!
    @IBOutlet weak var viewFilter: UIView!
    @IBOutlet weak var viewUpcoming: UIView!

    @IBOutlet weak var lblRideType: UILabel!
    
    @IBOutlet weak var viewNavigation: UIViewCommon!
   
    @IBOutlet weak var constraintViewFilter: NSLayoutConstraint!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var second_View: UIView!
    @IBOutlet weak var third_View: UIView!
    @IBOutlet weak var first_View_Lbl: UILabel!
    @IBOutlet weak var thirdView_Lbl: UILabel!
    @IBOutlet weak var second_View_Lbl: UILabel!
    let arrTabs = ["All", "Personal", "Business"]
    var currentIndex = 0
    var arrTabViews = [UIView]()
    
    @IBOutlet weak var info_View: ViewDesign!
    @IBOutlet weak var header_Lbl: UILabel!
    @IBOutlet weak var table_View: UITableView!
    @IBOutlet weak var trips_DropDown_View: ViewDesign!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    
    var arrFilter : [CellModel] = [CellModel]()
    var arrUpComingRide = [RideListModel]()
    var isPresented : Bool = false
    var isFromSupport : Bool = false

    var rideType : RideType = .Upcoming
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       // self.showFilter(isFilterShow: false)
    }

    
    func initialConfig() {

//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
//        callAPIForRideList()
//        self.delegate = self
//        self.dataSource = self
//        self.addGestures(true)
//        self.tabHeight = 46
//
//        if AppConstants.hasSafeArea {
//            self.tabY = viewNavigation.frame.size.height + 44
//        } else {
//            self.tabY = viewNavigation.frame.size.height + 20
//        }
//
//        self.tabWidth = AppConstants.ScreenSize.SCREEN_WIDTH / CGFloat(self.arrTabs.count)
//        self.indicatorHeight = 2
//        self.indicatorColor = .white
//
//        self.tabsViewBackgroundColor = .white
        
        table_View.register(UINib(nibName: "Issue_PastTrip_TableViewCell", bundle: nil), forCellReuseIdentifier: "Issue_PastTrip_TableViewCell")
        
//        viewNavigation.click = {
//            self.btnClose_Click()
//        }
//
//        if isFromSupport {
//            viewNavigation.text = "KLblIssueWithThePastTrip".localized as NSString
//            viewUpcoming.isHidden = true
//        } else {
//            viewNavigation.text = "KLblTripHistory".localized as NSString
//        }
        
        //prepareDataSourceForFilter()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
//        tap.cancelsTouchesInView = false
        
        //viewPlaceHolder.addGestureRecognizer(tap)
        
    }
    
    //MARK: Prepare datasource for Filter
    func prepareDataSourceForFilter(){
        
        arrFilter.removeAll()
        arrFilter.append(CellModel.getModel(placeholder : "KLblUpcoming".localized, type: .FilterTripAllCell,classType: .TripHistoryContainerVC, isSelected: true))
        arrFilter.append(CellModel.getModel(placeholder : "KLblPast".localized, type: .FilterTripPersonalCell, classType: .TripHistoryContainerVC))
        
       // tableView.reloadData()
    }
    
    func showFilter(isFilterShow: Bool){
        
        if isFilterShow {
            //                UIView.animate(withDuration: 0.5) {
            self.viewPlaceHolder.isHidden = false
           // self.viewFilter.isHidden = false
            self.constraintViewFilter.constant = CGFloat(self.arrFilter.count * 54) - 5
            //                    self.view.layoutIfNeeded()
            //                }
        }else{
            //                UIView.animate(withDuration: 0.5) {
            self.constraintViewFilter.constant = 0
            self.viewPlaceHolder.isHidden = true
           // self.viewFilter.isHidden = true
            self.view.layoutIfNeeded()
            //                }
        }
    }
    
//    func apiCall() {
//        if let vc = self.viewController(at: UInt(self.currentIndex)) as? TripHistoryVC {
//            if isFromSupport {
//                rideType = .Past
//            }
//
//            vc.rideType = self.rideType
//
//            var cardType = 0
//            if self.currentIndex == 0 {
//                cardType = AppConstants.CardTypes.All
//            } else if self.currentIndex == 1 {
//                cardType = AppConstants.CardTypes.PersonalCard
//            } else {
//                cardType = AppConstants.CardTypes.JobCard
//            }
//
//            vc.callAPIForRideList(isShowLoader: true, rideType: rideType, cardType: cardType)
//        }
//    }
    
    func callAPIForRideList(isShowLoader:Bool = true, rideType: RideType = .Upcoming, cardType: Int = 0) {
        
        var req = [String:Any]()
        req["filterType"] = self.rideType.rawValue
        
        if cardType != AppConstants.CardTypes.All {
            req["cardType"] = cardType
        }
        
        print("req\(req)")
        
        if isShowLoader {
            NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        }
        
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.RideList, method: .post, parameters: req, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any] {
                
                if let list = dictResponse["list"] as? [[String:Any]] {
                    self.arrUpComingRide = Mapper<RideListModel>().mapArray(JSONArray: list)
                    
                    self.reloadData()
                }
            }
        }) { (failureMessage, failureCode) in
//            Utilities.showAlertView(message: failureMessage)
            self.reloadData()
        }
    }
    
    func reloadData() {
        if arrUpComingRide.count <= 0 {
            self.table_View.loadNoDataFoundView(message: StringConstants.Nodata.KNoRide, image: "norides") {
            }
        } else {
             
        }
         self.table_View.reloadData()
    }

    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func info_Btn(_ sender: UIButton) {
        info_View.isHidden = false
    }
    
    @IBAction func issue_With_LastTrip(_ sender: UIButton) {
        info_View.isHidden = true
        let VC = IssueWithLastTripVC()
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func issue_With_PastTrip(_ sender: UIButton) {
         info_View.isHidden = true
        header_Lbl.text = "Issue With The Past Trip"
    }
    
    @IBAction func need_Help_Btn(_ sender: UIButton) {
         info_View.isHidden = true
    }
    
    @IBAction func support_Btn(_ sender: UIButton) {
         info_View.isHidden = true
    }
    
    @IBAction func dropdown_Btn(_ sender: UIButton) {
        trips_DropDown_View.isHidden = false
    }
    
    @IBAction func all_Trips_Btn(_ sender: UIButton) {
        trips_DropDown_View.isHidden = true
    }
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        showFilter(isFilterShow: false)
    }
    
    func btnClose_Click() {
        if isPresented{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func firstView_Btn(_ sender: UIButton) {
        firstView.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 253/255, alpha: 1.0)
        second_View.backgroundColor = UIColor.white
         third_View.backgroundColor = UIColor.white
        first_View_Lbl.isHidden = false
        second_View_Lbl.isHidden = true
        thirdView_Lbl.isHidden = true
    }
    
    @IBAction func secondView_Btn(_ sender: UIButton) {
        second_View.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 253/255, alpha: 1.0)
        firstView.backgroundColor = UIColor.white
         third_View.backgroundColor = UIColor.white
        second_View_Lbl.isHidden = false
        thirdView_Lbl.isHidden = true
        first_View_Lbl.isHidden = true
    }
    
    @IBAction func thirdView_Btn(_ sender: UIButton) {
        third_View.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 253/255, alpha: 1.0)
        firstView.backgroundColor = UIColor.white
         second_View.backgroundColor = UIColor.white
        second_View_Lbl.isHidden = true
        thirdView_Lbl.isHidden = false
        first_View_Lbl.isHidden = true
    }
    
    @IBAction func btnFilter(_ sender: UIButton) {
        showFilter(isFilterShow: true)
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension TripHistoryContainerVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4//arrUpComingRide.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let model = arrFilter[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Issue_PastTrip_TableViewCell", for: indexPath) as! Issue_PastTrip_TableViewCell
        //cell.setData(model: model)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let VC = RideSummary_ViewController()
        navigationController?.pushViewController(VC, animated: true)
//        if indexPath.row == 0 {
//            rideType = .Upcoming
//            lblRideType.text = "Upcoming"
//        } else {
//            rideType = .Past
//            lblRideType.text = "Past"
//        }
//
//        self.arrFilter.forEach { $0.isSelected = false}
//        arrFilter[indexPath.row].isSelected = true
//        tableView.reloadData()
//        showFilter(isFilterShow: false)
        
       // apiCall()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 234
    }
    
}

// MARK: - Pager DataSource, Pager Delegate
//extension TripHistoryContainerVC : ViewPagerDataSource,ViewPagerDelegate {
//
//    func numberOfTabs(forViewPager viewPager: ViewPagerController!) -> UInt {
//        return UInt(self.arrTabs.count)
//    }
//
//    func viewPager(_ viewPager: ViewPagerController!, viewForTabAt index: UInt) -> UIView! {
//
//        if self.arrTabViews.count > index {
//            return self.arrTabViews[Int(index)]
//        } else {
//            let tabView = UIView(frame: CGRect(x: 0, y: 0, width: self.tabWidth , height: self.tabHeight))
//            tabView.backgroundColor = UIColor.clear
//            let labelT = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: tabView.frame.width, height: tabView.frame.height))
//            labelT.font = FontScheme.kMediumFont(size: 14)
//            labelT.text = self.arrTabs[Int(index)]
//            labelT.numberOfLines = 1
//            labelT.textAlignment = .center
//            labelT.textColor = ColorScheme.kTextColorPrimary()
//            labelT.tag = 444
//            tabView.addSubview(labelT)
//
//            let viewLine = UIView.init(frame: CGRect(x: 0, y: tabView.frame.height - 1, width: tabView.frame.width, height: 1))
//            viewLine.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
//            viewLine.tag = 445
//            tabView.addSubview(viewLine)
//
//            self.arrTabViews.append(tabView)
//            return tabView
//        }
//    }
//
//    func viewPager(_ viewPager: ViewPagerController!, contentViewControllerForTabAt index: UInt) -> UIViewController! {
//
//        let vc = TripHistoryVC(nibName: "TripHistoryVC", bundle: nil)
//        vc.isFromSupport = isFromSupport
//        vc.rideType = rideType
//
//        return vc
//    }
//
//    func viewPager(_ viewPager: ViewPagerController!, didChangeTabTo index: UInt, from previousIndex: UInt, didSwipe: Bool) {
//
//        self.currentIndex = Int(index)
//
//       // apiCall()
//
//        var i = 0
//        for view in self.arrTabViews{
//
//            if let lbl = view.viewWithTag(444) as? UILabel {
//                if i == currentIndex {
//                 //KD   view.backgroundColor = ColorConstants.TextColorSecondary.withAlphaComponent(0.1)
//                }
//                else{
//                    view.backgroundColor = ColorConstants.TextColorWhitePrimary
//                }
//            }
//
//            for item in view.subviews {
//
//                if item.tag == 445 {
//                    if i == currentIndex {item.backgroundColor = ColorConstants.ThemeColor}
//                    else {item.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)}
//                }
//            }
//
//            i = i + 1
//        }
//        self.view.layoutSubviews()
//    }
//}
