//
//  TripHistoryCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 11/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class TripHistoryCell: UITableViewCell {
    
    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var imgMap: UIImageView!
    @IBOutlet weak var imgMapHeight: NSLayoutConstraint!
    @IBOutlet weak var viewCancelHeight: NSLayoutConstraint!

    @IBOutlet weak var viewContent: UIView!

    @IBOutlet weak var viewDot1: UIView!
    @IBOutlet weak var viewDot2: UIView!
    
    @IBOutlet weak var viewDashedLine: UIView!
    @IBOutlet weak var viewCancel: UIView!

    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblStartTime: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblCarType: UILabel!
    @IBOutlet weak var lblCar: UILabel!
    @IBOutlet weak var lblCost: UILabel!
    @IBOutlet weak var lblCostCaption: UILabel!
    @IBOutlet weak var lblKm: UILabel!
    @IBOutlet weak var lblCancel: UILabel!
        
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        viewDot1.layer.cornerRadius = viewDot1.frame.height / 2
        viewDot2.layer.cornerRadius = viewDot2.frame.height / 2
        
        viewContent.backgroundColor = ColorConstants.UnderlineColor.withAlphaComponent(0.6)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:RideListModel, rideType:RideType) {
                
        if model.rideType == AppConstants.RideType.Schedule {
            ThemeManager.sharedInstance.RideType = AppConstants.RideType.Schedule
        } else {
            ThemeManager.sharedInstance.RideType = AppConstants.RideType.Instant
        }
        
        viewDashedLine.addDashedBorderVertical(color: ColorConstants.ThemeColor, isVertical: false)
        viewDot1.backgroundColor = ColorConstants.ThemeColor
        viewDot2.backgroundColor = ColorConstants.ThemeColor
        
        if rideType == .Upcoming {
            lblCostCaption.text = "KLblEstimatedCost".localized
            lblStartDate.text = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: model.reservedDateTime ?? ""), format: DateUtilities.DateFormates.kRegularDate)
            lblEndDate.text = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: model.reservedEndDateTime ?? ""), format: DateUtilities.DateFormates.kRegularDate)
            
            
            let startTime = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: model.reservedDateTime ?? ""), format: DateUtilities.DateFormates.k24Time)
            
            let endTime = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: model.reservedEndDateTime ?? ""), format: DateUtilities.DateFormates.k24Time)
            
            lblStartTime.text = startTime
            lblEndTime.text = endTime
            
        }
        else if rideType == .Past {
            lblCostCaption.text = "KLblCost".localized
            lblStartDate.text = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: model.startDateTime ?? ""), format: DateUtilities.DateFormates.kRegularDate)
            lblEndDate.text = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: model.endDateTime ?? ""), format: DateUtilities.DateFormates.kRegularDate)
            
            let startTime = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: model.startDateTime ?? ""), format: DateUtilities.DateFormates.k24Time)
            
            let endTime = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: model.endDateTime ?? ""), format: DateUtilities.DateFormates.k24Time)
            
            lblStartTime.text = startTime
            lblEndTime.text = endTime
        }
        
        lblCost.text = model.getTotalFare()
        
        
        if let statLoc = model.startLocation, let endLoc = model.endLocation {
            
            let size = CGSize(width: AppConstants.ScreenSize.SCREEN_WIDTH - 40, height: 140)
            let strUrl = Utilities.getGoogleMapImageUrl(size: size, startLocation: statLoc, endLocation: endLoc)
            imgMap.setImageForURL(url: URL(string: strUrl), placeHolder: UIImage(named: "noImage"))
            imgMap.isHidden = false
            imgMapHeight.constant = 140

        }
        else {
            imgMap.isHidden = true
            imgMapHeight.constant = 0

        }
        
        let (distance,distanceUnit) = model.getTotalDistance()
        lblDistance.text = distance
        lblKm.text = distanceUnit
        
        
        lblCar.text = "\(model.vehicleId?.model ?? "") | \(model.vehicleId?.numberPlate ?? "") | \(model.vehicleId?.getVehicleCategory ?? "")"
        lblCarType.text = "\(model.vehicleId?.getVehicleCategory ?? "") Car"

        if model.status == AppConstants.RideStatus.CANCELLED {
            viewCancel.isHidden = false
            lblCancel.backgroundColor = ColorConstants.ThemeColor
            viewCancelHeight.constant = 47
        }
        else{
            viewCancel.isHidden = true
            viewCancelHeight.constant = 0
        }
    }
}
