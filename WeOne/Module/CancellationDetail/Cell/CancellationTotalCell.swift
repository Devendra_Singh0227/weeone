//
//  CancellationTotalCell.swift
//  WeOne
//
//  Created by iMac on 07/05/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class CancellationTotalCell: UITableViewCell {

    @IBOutlet weak var btnInfo: UIButton!
    
    @IBOutlet weak var lblPrice: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ model : CellModel){
        
        lblPrice.text = model.placeholder
        btnInfo.setImage(UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Information")), for: .normal)
    }
    
}
