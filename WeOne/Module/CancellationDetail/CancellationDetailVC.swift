//
//  FareSummaryVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 14/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class CancellationDetailVC: UIViewController {
    
    //MARK: Variables
    var arrCellData : [CellModel] = [CellModel]()
    var rideModel:RideListModel = RideListModel()
    
    var isExpand = false
    var onCancel:(()->())?

    //MARK: Outlets
    @IBOutlet weak var viewNavigation: UIViewCommon!
    @IBOutlet var viewHeader: UIView!

    @IBOutlet weak var imgCar: UIImageView!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnCancelBooking: UIButtonCommon!
    
    @IBOutlet weak var imgInfo: UIImageView!
    @IBOutlet weak var lblCard: UILabel!
    @IBOutlet weak var btnInfo: UIButton!

    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    
    //MARK: view life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initial_Config()
        initialConfig()
    }
    
    //MARK: Initial config
    
    func initial_Config() {
//           if AppConstants.hasSafeArea {
//               headerView_HeightConstraint.constant = 90
//           }
//           else {
//               headerView_HeightConstraint.constant = 74
//           }
       }
    
    func initialConfig() {
        callApiForCancelRideDetail()
        
        viewNavigation.text = "\(rideModel.vehicleId?.getCarName ?? "") | \(rideModel.vehicleId?.numberPlate ?? "")" as NSString
        
        prepareDataSource()
        
        btnCancelBooking.setText(text: "KLblCancelBooking".localized)
        
        btnCancelBooking.click = {
            self.callApiForCancelReservation()
        }
        
        viewNavigation.click = {
            self.btnClose_Click()
        }
        
        imgCar.setImageForURL(url: rideModel.vehicleId?.getVehicleImage, placeHolder: UIImage(named : "carPlaceholder"))

        tableView.register(UINib(nibName: "TripInfoCell", bundle: nil), forCellReuseIdentifier: "TripInfoCell")
        tableView.register(UINib(nibName: "PaymentBreakdownCell", bundle: nil), forCellReuseIdentifier: "PaymentBreakdownCell")
        tableView.register(UINib(nibName: "SectionCell", bundle: nil), forCellReuseIdentifier: "SectionCell")

        tableView.register(UINib(nibName: "MoreInformationCardCell", bundle: nil), forCellReuseIdentifier: "MoreInformationCardCell")
        tableView.register(UINib(nibName: "BreakdownCell", bundle: nil), forCellReuseIdentifier: "BreakdownCell")
        tableView.register(UINib(nibName: "CancellationTotalCell", bundle: nil), forCellReuseIdentifier: "CancellationTotalCell")

        tableView.tableHeaderView = viewHeader

        imgInfo.image = UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Information"))

    }
    
    //MARK: Prepare DataSource
    func prepareDataSource(){
        
        arrCellData.removeAll()
        
        
        if rideModel.reservedDateTime != nil{
            arrCellData.append(CellModel.getModel(type: .CancellationTripInfo, cellObj:rideModel))
        }
        
        let currency = rideModel.partnerSetting?.currency
        
        let dailyPackage = rideModel.pricingConfigData?.hours == 24 ? rideModel.pricingConfigData : nil
        let hourlyPackage = rideModel.pricingConfigData?.hours == 1 ? rideModel.pricingConfigData : nil

        var description = ""
        
        if dailyPackage != nil {
            description = "1 day \(Utilities.convertToCurrency(number: dailyPackage?.rate ?? 0.0, currencyCode: currency))"
        }
        
            var rate = 0.0
            if hourlyPackage?.rate != 0.0 && hourlyPackage?.rate != nil {
                rate = hourlyPackage?.rate ?? 0.0
            } else if rideModel.pricingConfigData?.perHourCharge != 0.0 && rideModel.pricingConfigData?.perHourCharge != nil {
                rate = rideModel.pricingConfigData?.perHourCharge ?? 0.0
            }
            if description != "" {
                description = description + ", 1 hour \(Utilities.convertToCurrency(number: (rate), currencyCode: currency))"
            } else {
                description = "1 hour \(Utilities.convertToCurrency(number: (rate), currencyCode: currency))"
            }
            
        arrCellData.append(CellModel.getModel(placeholder: "Cancellation (\(rideModel.fareSummary?.percentageReturn ?? 0)% Credit)", text: Utilities.convertToCurrency(number: rideModel.fareSummary?.refundAmount ?? 0, currencyCode: currency), userText1: "(\(description))", type: .PriceBreakUpCell))
        
        //Insurance
        if rideModel.fareSummary?.insurance ?? 0.0 != 0.0{
            arrCellData.append(CellModel.getModel(placeholder : "KLblInsurance".localized, text : rideModel.getInsurance(), type: .PriceBreakUpCell))
        }

        arrCellData.append(CellModel.getModel(placeholder: Utilities.convertToCurrency(number: rideModel.totalFare ?? 0, currencyCode: currency), text: "", type: .CancellationTotal, cellObj: rideModel))
        
        
        tableView.reloadData()
        
        lblCard.text = rideModel.selectedCard?.getFormattedCardNumber()
        lblAmount.text = Utilities.convertToCurrency(number: rideModel.totalFare, currencyCode: rideModel.partnerSetting?.currency)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)

        
    }
    
    //MARK:- IBActions
    func btnClose_Click() {
        self.navigationController?.popViewController(animated: true)
    }

    
    @objc func btnInfo_Click(_ sender: UIButton) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        Utilities.dismissToolTip(views: appDelegate.window!.subviews)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            Utilities.showPopOverView(str: "Weone credit can be used in future bookings",
                                      arrowSize: CGSize(width: 9, height: 9), isTop: true, maxWidth: 293, x: 10, sender: sender)
        })
        
    }
    
    @IBAction func btnInfoCard_Click(_ sender: UIButton) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        Utilities.dismissToolTip(views: appDelegate.window!.subviews)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            Utilities.showPopOverView(str: "KMsgDeposite".localized, arrowSize: CGSize(width: 11, height: 11), maxWidth: 191, x: 10, isAttributed: true, sender: self.imgInfo)
        })

    }


    func moveToRating(){
        let vc = RideRatingVC(nibName: "RideRatingVC", bundle: nil)
        vc.activeRideObject = self.rideModel
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        Utilities.dismissToolTip(views: appDelegate.window!.subviews)
    }

}

//MARK: Tableview methods
extension CancellationDetailVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCellData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrCellData[indexPath.row]

        switch model.cellType! {
                        
        case .CancellationTripInfo:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TripInfoCell", for: indexPath) as! TripInfoCell
            cell.setData(model: model)
            return cell

        case .FareSummaryCardSelection:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInformationCardCell", for: indexPath) as! MoreInformationCardCell
            cell.setData(model: model)
            cell.btnChange.addTarget(self, action: #selector(btnChange_Click(_:)), for: .touchUpInside)
            cell.btnChange.tag = indexPath.row
            cell.switchChanged = { value in
                self.changeDefaultCards(value, index : indexPath.row)
            }
            return cell
            
        case .PriceBreakUpCell:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "BreakdownCell", for: indexPath) as! BreakdownCell
            cell.setData(model)
            return cell
            
        case .CancellationTotal:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CancellationTotalCell", for: indexPath) as! CancellationTotalCell
            cell.btnInfo.addTarget(self, action: #selector(btnInfo_Click), for: .touchUpInside)

            cell.setData(model)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    

    @objc func btnChange_Click(_ sender: UIButton) {
        
        let vc = PaymentVC()
        vc.isDeleteHidden = true
        //vc.shownCardType = !arrCellData[sender.tag].isSelected ? AppConstants.CardTypes.PersonalCard :  AppConstants.CardTypes.JobCard
        vc.screenType = .ChangeCard
    
        vc.isReload = { isFromAddClick in

            let filter = self.arrCellData.filter { $0.cellType == .FareSummaryCardSelection}
            
            if filter.count > 0{
                
                if filter.first!.isSelected == false{
                    filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard()
                }else{
                    filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard)
                }
                
            } else {
                if isFromAddClick {
                    //Card
                    if let card = SyncManager.sharedInstance.getPrimaryCard() {
                        self.arrCellData.append(CellModel.getModel(placeholder: "", text: "", type: .CMICard, cellObj: card))
                    } else if let card = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard) {
                        self.arrCellData.append(CellModel.getModel(placeholder: "", text: "", type: .CMICard, cellObj: card, isSelected : true))
                    } else {
                        self.arrCellData.append(CellModel.getModel(placeholder: "", text: "", type: .CMIAddCard, cellObj: nil))
                    }
                }
            }
            self.tableView.reloadRows(at: [IndexPath(row: self.arrCellData.firstIndex(of: filter.first!) ?? 0, section: 0)], with: .none)
            
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func changeDefaultCards(_ value : Bool, index : Int){
        
        let filter = arrCellData.filter { $0.cellType == .FareSummaryCardSelection}
        
        if filter.count > 0{
            
            if value == false{
                filter.first!.isSelected = false
                filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard()
            }else{
                filter.first!.isSelected = true
                filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard)
            }
        }
        
        self.tableView.reloadData()
    }
}

//MARK: Call api
extension CancellationDetailVC{
    
    func callApiForCancelReservation(){
        
        var param : [String:Any] = [String:Any]()
        param["rideId"] = rideModel.id ?? ""

        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.CancelRide, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any]{
                
                if SyncManager.sharedInstance.activeRideObject != nil{
                    SyncManager.sharedInstance.activeRideObject = Mapper<RideListModel>().map(JSON: dictResponse)!
//                    self.invalidateTimer()
//                    self.onConfirm_Click?()

                } else {
                    
                    SyncManager.sharedInstance.syncMaster ({

//                        self.refreshAllRides?()
//                        self.invalidateTimer()
//                        self.onConfirm_Click?()
                        
                    })
                }
                
                AppNavigation.shared.moveToHome(isPopAndSwitch: true)
            }
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func callApiForCancelRideDetail(){
        
        var param = [String:Any]()
        param["rideId"] = rideModel.id ?? ""
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.CancelRideDetail, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any] {
                print(dictResponse)
                self.rideModel = Mapper<RideListModel>().map(JSON: dictResponse)!
              self.prepareDataSource()
                //  self.tableView.reloadData()
            }
            

        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
