//
//  RedeemVoucherVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 02/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class RedeemVoucherVC: UIViewController {

    //MARK: - Variables
    
    //MARK: - Outlets
    
    @IBOutlet weak var txtRedeemCode: UITextField!
    @IBOutlet weak var btnRedeemVoucher: UIButtonCommon!
    @IBOutlet weak var lblDesc: UILabel!
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        initialConfig()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }
    
    //MARK: Private Methods
    func initialConfig() {
        
        txtRedeemCode.delegate = self
        btnRedeemVoucher.button.isEnabled = false
        btnRedeemVoucher.button.backgroundColor = ColorConstants.ButtonBGColor.withAlphaComponent(0.5)
        lblDesc.setLineSpacing(lineSpacing: 1.0, lineHeightMultiple: 1.5)
    }
    
    //MARK:- IBActions
    
    @IBAction func btnBack_Clicked(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }    
}

//MARK:- UITextfield Delegate Methods
extension RedeemVoucherVC : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if (textField.text?.count ?? 0) > 0 {
            btnRedeemVoucher.button.isEnabled = true
            btnRedeemVoucher.button.backgroundColor = ColorConstants.ButtonBGColor
        }
        else {
            btnRedeemVoucher.button.isEnabled = false
            btnRedeemVoucher.button.backgroundColor = ColorConstants.ButtonBGColor.withAlphaComponent(0.5)
        }
    }
}

//MARK:- Extra Methods
extension RedeemVoucherVC {
    
    convenience init() {
        self.init(nibName: "RedeemVoucherVC", bundle: nil)
    }
}
