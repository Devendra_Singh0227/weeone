//
//  FaqCell.swift
//  E-Scooter
//
//  Created by Coruscate Mac on 07/06/19.
//  Copyright © 2019 CORUSCATEMAC. All rights reserved.
//

import UIKit

class FaqCell: UITableViewCell {

    @IBOutlet weak var imgCaret: UIImageView!
    @IBOutlet weak var lblTitle: MMLabel!
    @IBOutlet weak var lblAnswer: MMLabel!
    @IBOutlet weak var viewDesc: UIView!
    @IBOutlet weak var viewLine: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        viewDesc.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(_ model : CellModel){
        
        lblTitle.text = model.placeholder
        lblAnswer.text = model.placeholder2
        
        if model.isLast {
            viewLine.isHidden = false
        } else {
            viewLine.isHidden = true
        }
        
        
        if model.isSelected == true{
            viewDesc.isHidden = false
            imgCaret.image = UIImage(named : "back_down")
        }else{
            imgCaret.image = UIImage(named : "backGrey")
            viewDesc.isHidden = true
        }
    }
}
