//
//  FaqVC.swift
//  E-Scooter
//
//  Created by Coruscate Mac on 07/06/19.
//  Copyright © 2019 CORUSCATEMAC. All rights reserved.
//

import UIKit

class FaqVC: UIViewController {

    //MARK: Variables
    var arrCellData : [CellModel] = []
    
    //MARK: outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: view life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
        
        getFAQData()
    }
    
    //MARK: intial config
    func initialConfig(){
        tableView.register(UINib(nibName: "FaqCell", bundle: nil), forCellReuseIdentifier: "FaqCell")
    }
    
    //MARK: prepare Data source
    func prepareFAQ(from data: [[String:Any]]) {
        let models = data.map {
            CellModel.getModel(placeholder: $0["question"] as? String ?? "", placeholder2: $0["answer"] as? String ?? "", type: .CarReserveDirectionCell)
        }
        self.arrCellData = models
        tableView.reloadData()
    }
    
    func getFAQData() {
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.FAQ, method: .post, parameters: [:], headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any]{
                
                if let arr = dictResponse["list"] as? [[String:Any]]{
                    self.prepareFAQ(from: arr)
                }
            }
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    @IBAction func btnClose_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: Tableview methods
extension FaqVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCellData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrCellData[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "FaqCell", for: indexPath) as! FaqCell
        cell.setCellData(model)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = arrCellData[indexPath.row]
        model.isSelected = !model.isSelected
        tableView.reloadRows(at: [indexPath], with: .fade)
    }
}
