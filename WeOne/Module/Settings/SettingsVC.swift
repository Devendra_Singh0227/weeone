////
////  SettingsVC.swift
////  WeOne
////
////  Created by CORUSCATEMAC on 24/12/19.
////  Copyright © 2019 Coruscate Mac. All rights reserved.
////
//
//import UIKit
//import PhoneNumberKit
//
//class SettingsVC: UIViewController {
//    
//    //MARK: - Variables
//    var arrData = [CellModel]()
//    var arrStaticData = [StaticModel]()
//
//    var saticPageCode = ""
//    
//    var isFromLegal : Bool = false
//    var isFromSettings: Bool = false
//    
//    //MARK: - Outlets
//    @IBOutlet weak var tblView: UITableView!
//    @IBOutlet var viewHeader: UIView!
//    @IBOutlet weak var imgProfile: UIImageView!
//    @IBOutlet weak var lblName: MMLabel!
//    @IBOutlet weak var lblEmail: MMLabel!
//    @IBOutlet weak var lblMobile: MMLabel!
//    @IBOutlet weak var lblTitle: UILabel!
//    @IBOutlet weak var viewSettings: UIView!
//    @IBOutlet weak var viewNavigation: UIViewCommon!
//
//    //MARK:- Controller's Life Cycle
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        initialConfig()
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
//    }
//
//    //MARK: Private Methods
//    func initialConfig() {
//        tblView.register(UINib(nibName: "EditNameCell", bundle: nil), forCellReuseIdentifier: "EditNameCell")
//        tblView.register(UINib(nibName: "MoreCell", bundle: nil), forCellReuseIdentifier: "MoreCell")
//        tblView.register(UINib(nibName: "SettingCell", bundle: nil), forCellReuseIdentifier: "SettingCell")
//        tblView.register(UINib(nibName: "SettingDescCell", bundle: nil), forCellReuseIdentifier: "SettingDescCell")
//
//        
//        viewNavigation.click = {
//            self.btnClose_Click()
//        }
//
//        if isFromSettings {
//            viewNavigation.text = "KLblLMSettings".localized as NSString
////            preapareDataSource()
//        } else {
//            if saticPageCode == AppConstants.StaticPageCode.Legal {
//                viewNavigation.text = "KLblLMLegal".localized as NSString
//            } else if saticPageCode == AppConstants.StaticPageCode.Account {
//                viewNavigation.text = "KAccount".localized as NSString
//            } else if saticPageCode == AppConstants.StaticPageCode.Payment_Pricing {
//                viewNavigation.text = "KPaymentPricing".localized as NSString
//            } else if saticPageCode == AppConstants.StaticPageCode.Using_Weone {
//                viewNavigation.text = "KUsingWeone".localized as NSString
//            }
//            
//            getStaticData()
//        }
//        
//    }
//    
////    func preapareDataSource() {
////
////        arrData.removeAll()
////
////        if isFromSettings{
////            let user = ApplicationData.user
////
////            arrData.append(CellModel.getModel(text: user?.firstName, type:  .SettingName , classType: .SettingsVC, imageName: "User1@x", keyBoardType: .default))
////
////            arrData.append(CellModel.getModel(text: user?.primaryEmail?.email, type: .SettingEmail , classType: .SettingsVC , imageName: "mail", keyBoardType: .emailAddress))
////
////
////           let phoneNumber = PartialFormatter().formatPartial("+\((user?.primaryMobile?.countryCode ?? "") + (user?.primaryMobile?.mobile ?? ""))")
////
////            arrData.append(CellModel.getModel(text: phoneNumber, type: .SettingPhone , classType: .SettingsVC,  imageName: "iphone"))
////
////            arrData.append(CellModel.getModel(text: "KBusinessProfile".localized, type: .SettingBusinessProfile, classType: .SettingsVC, imageName: "Briefcase"))
////
////            arrData.append(CellModel.getModel(text: "KLblSavedPlace".localized, type: .SettingSavedPlace, classType: .SettingsVC, imageName: ThemeManager.sharedInstance.getImage(string: "Fav")))
////
////            arrData.append(CellModel.getModel( type: .SettingSeprator))
////
////            arrData.append(CellModel.getModel(placeholder: "KNotifications".localized, type: .SettingNotification, classType: .SettingsVC))
////            arrData.append(CellModel.getModel(placeholder: "KPrivacy".localized, type: .SettingPrivacy, classType: .SettingsVC))
////            arrData.append(CellModel.getModel(placeholder: "KLblLMLegal".localized, type: .SettingLegal, classType: .SettingsVC))
////            arrData.append(CellModel.getModel(placeholder: "KLblLMLOGOUT".localized, type: .SettingLogout, classType: .SettingsVC, isLast: true))
////
////
////        } else {
////                for data in arrStaticData {
////                    arrData.append(CellModel.getModel(placeholder: data.title, type: .SettingStatics, classType: .SettingsVC))
////                }
////        }
////
////
////        tblView.reloadData()
////    }
//    
//    //MARK: SetData
//    func setData(){
//        let user = ApplicationData.user
//        
//        imgProfile.clipsToBounds = true
//        imgProfile.layer.borderWidth = 1
//        imgProfile.layer.borderColor = #colorLiteral(red: 0.1215686275, green: 0.1215686275, blue: 0.1254901961, alpha: 1)
//        
//        imgProfile.layer.cornerRadius = imgProfile.frame.height / 2
//
//        
//        if let url = URL(string: AppConstants.serverURL + "/\(user?.image ?? "")")  {
//            imgProfile.setImageForURL(url: url, placeHolder: UIImage(named: "user_placeholder"))
//        }
//        else {
//            imgProfile.image = UIImage(named: "user_placeholder")
//        }
//        
//        
//        lblName.text = user?.fullName
//        lblEmail.text = user?.primaryEmail?.email
//        lblMobile.text = "\(user?.primaryMobile?.countryCode ?? "") \(user?.primaryMobile?.mobile ?? "")"
//        
//        
//    }
//    
//    //MARK:- IBActions
//
//     func btnClose_Click() {
//        self.navigationController?.popViewController(animated: true)
//    }
//    
//    @IBAction func btnProfile_Click(_ sender: UIButton) {
//        let vc = EditProfileVC.init(nibName: "EditProfileVC", bundle: nil)
//        UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
//    }
//}
//
////MARK: - UITableView's DataSource & Delegate Methods
//extension SettingsVC: UITableViewDataSource, UITableViewDelegate {
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return arrData.count
//    }
//    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if !isFromSettings {
//            return 60
//        } else {
//            let model = arrData[indexPath.row]
//            switch model.cellType! {
//            case .SettingSeprator:
//                var height = 0
//                if AppConstants.hasSafeArea {
//                    height = (80 + ((arrData.count) * 60))
//                } else {
//                    height = (20 + (arrData.count) * 60)
//                }
//                return AppConstants.ScreenSize.SCREEN_HEIGHT - CGFloat(height)
//            default:
//                return 60
//            }
//        }
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let model = arrData[indexPath.row]
//        
//        if !isFromSettings {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreCell") as? MoreCell
//            cell?.setData(model: model)
//            return cell!
//        } else {
//            switch model.cellType! {
//            case .SettingNotification, .SettingPrivacy, .SettingLegal, .SettingLogout, .SettingSeprator:
//                
//                let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as? SettingCell
//                cell?.setData(model: model)
//                return cell!
//                
//            default:
//                let cell = tableView.dequeueReusableCell(withIdentifier: "EditNameCell") as? EditNameCell
//                cell?.setData(model: model)
//                return cell!
//            }
//        }
//    }
//    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//        let model = arrData[indexPath.row]
//        switch model.cellType! {
//            
//        case .SettingName:
//            let vc = EditProfileVC()
//            self.navigationController?.pushViewController(vc, animated: true)
//            
//            break
//            
//        case .SettingEmail:
//            let vc = VerifyMobileVC()
//            self.navigationController?.pushViewController(vc, animated: true)
//            
//            break
//            
//        case .SettingPhone:
//            let vc = VerifyMobileVC()
//            vc.isFromMobile = true
//            self.navigationController?.pushViewController(vc, animated: true)
//            
//            break
//            
//        case .SettingBusinessProfile:
//            let vc = CompanyProfileVC()
//            self.navigationController?.pushViewController(vc, animated: true)
//            
//            break
//            
//        case .SettingSavedPlace:
//            let vc = AddSavePlaceVC()
//            vc.isFromSetting = true 
//            self.navigationController?.pushViewController(vc, animated: true)
//            
//            break
//            
//        case .SettingNotification:
//            
//            let vc = NotificationSettingsVC()
//            self.navigationController?.pushViewController(vc, animated: true)
//            break
//            
//        case .SettingPrivacy:
//            
//            let vc = GdprPopupVC(nibName: "GdprPopupVC", bundle: nil)
//            vc.dict = ApplicationData.user?.gdprCompliance?.getDict() ?? [:]
//            vc.isForUpdate = true
//            vc.isFromSubmit = true 
//            
//            vc.submitGDPR = { dict in
//                self.callAPIForUpdateProfile(dict)
//            }
//            
//            self.navigationController?.pushViewController(vc, animated: true)
//            
//            break
//        case .SettingChangePassword:
//            
//            let vc = ChangePasswordVC()
//            self.navigationController?.pushViewController(vc, animated: true)
//            break
//            
//        case .SettingAboutUs:
//            
//            Utilities.openStaticPage(title: model.placeholder ?? "", url: AppConstants.StaticPage.AboutUs)
//            break
//            
//        case .SettingRateUs:
//            
//            Utilities.rateUs()
//            break
//            
//        case .SettingContactUs:
//            
//            let vc = ContactUsVC(nibName: "ContactUsVC", bundle: nil)
//            self.navigationController?.pushViewController(vc, animated: true)
//            break
//            
//        case .SettingTermsConditions:
//            
//            Utilities.openStaticPage(title: AppConstants.StaticCode.TermsCondition, url: AppConstants.URL.StaticPage)
//            
//            break
//            
//        case .SettingPrivacyPolicy:
//            
//            Utilities.openStaticPage(title: AppConstants.StaticCode.PrivacyPolicy, url: AppConstants.URL.StaticPage)
//            break
//            
//        case .SettingLegal:
//            
//            let vc = SettingsVC()
//            vc.saticPageCode = AppConstants.StaticPageCode.Legal
//            self.navigationController?.pushViewController(vc, animated: true)
//            
//            break
//            
//        case .SettingStatics:
//            Utilities.openStaticPage(title: arrStaticData[indexPath.row].code ?? "", url: AppConstants.URL.StaticPage)
//            break
//            
//        case .SettingSafety:
//            Utilities.openStaticPage(title: AppConstants.StaticCode.PrivacyPolicy, url: AppConstants.URL.StaticPage)
//            break
//            
//        case .SettingSecurity:
//            Utilities.openStaticPage(title: AppConstants.StaticCode.PrivacyPolicy, url: AppConstants.URL.StaticPage)
//            break
//            
//        case .SettingCopyright:
//            Utilities.openStaticPage(title: AppConstants.StaticCode.PrivacyPolicy, url: AppConstants.URL.StaticPage)
//            break
//            
//        case .SettingDataProviders:
//            Utilities.openStaticPage(title: AppConstants.StaticCode.PrivacyPolicy, url: AppConstants.URL.StaticPage)
//            break
//            
//        case .SettingSoftwareLicences:
//            Utilities.openStaticPage(title: AppConstants.StaticCode.PrivacyPolicy, url: AppConstants.URL.StaticPage)
//            break
//            
//        case .SettingLocationInformation:
//            Utilities.openStaticPage(title: AppConstants.StaticCode.PrivacyPolicy, url: AppConstants.URL.StaticPage)
//            break
//            
//        case .SettingLogout :
//            
//            if ApplicationData.isUserLoggedIn {
//                
//                UIViewController.current().view.showConfirmationPopupWithMultiButton(title: "", message: "KLblLogoutMsg".localized, cancelButtonTitle: StringConstants.ButtonTitles.kNo, confirmButtonTitle: StringConstants.ButtonTitles.kYes, onConfirmClick: {
//                    AppNavigation.shared.currentStateList = AppNavigation.shared.config.accessConfig.requiredToUseApp
//                    ApplicationData.sharedInstance.logoutUser()
//                    
//                }) {
//                    
//                }
//            }
//            else{
//                
//                let vc = LoginVC(nibName: "LoginVC", bundle: nil)
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//            
//            break
//        default:
//            print("")
//            break
//        }
//    }
//    
//    
//    func getRequest() -> [String : Any] {
//        
//        var finalParam : [String : Any] = [:]
//        
//        let user = ApplicationData.user
//        
//        if let emailModel = user?.primaryEmail {
//            finalParam["emails"] = [["email": emailModel.email ?? "",
//                                    "isPrimary": emailModel.isPrimary ?? false,
//                                    "isVerified": emailModel.isVerified ?? false]]
//        }
//        
//        if let mobileModel = user?.primaryMobile {
//            finalParam["mobiles"] = [["mobile": mobileModel.mobile ?? "",
//                                     "isPrimary": mobileModel.isPrimary ?? false,
//                "countryCode": mobileModel.countryCode ?? "-",
//                "isVerified": mobileModel.isVerified ?? false]]
//        }
//        
//        if let image = user?.image {
//            finalParam["image"] = image
//        }
//        
//        finalParam["firstName"] = user?.firstName
//        finalParam["lastName"] = user?.lastName
//        return finalParam
//    }
//}
//
////MARK: Call api
//extension SettingsVC{
//    
//    func callAPIForUpdateProfile(_ dictGdpr : [String:Any]) {
//        
//        var param : [String:Any] = getRequest()
//        if (dictGdpr["gdprCompliance"] as? [String:Any] ?? [:]).count > 0{
//            param["gdprCompliance"] = dictGdpr["gdprCompliance"]
//        }
//        
//        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
//        
//        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.UpdateProfile, method: .put, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
//            
//            if let dictResponse = response as? [String:Any] {
//                ApplicationData.sharedInstance.saveUserData(dictResponse)
////                Utilities.showAlertView(message: message)
//            }
//        }) { (failureMessage, failureCode) in
//            Utilities.showAlertView(message: failureMessage)
//        }
//    }
//    
//    func getStaticData() {
//        
//        var param : [String:Any] = [:]
//        param["code"] = [saticPageCode]
//        
//        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
//        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.SupportPage, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
//            
//            if let dictResponse = response as? [String:Any], !dictResponse.isEmpty {
//                if let list = dictResponse["list"] as? [[String:Any]] {
//                    self.arrStaticData = Mapper<StaticModel>().mapArray(JSONArray: list)
//                    if self.arrStaticData.count > 0 {
////                        self.preapareDataSource()
//                    } else {
//                        self.tblView.loadNoDataFoundView(message: StringConstants.Nodata.NoDataFound, image: "NoData") {
//                        }
//                    }
//                    
//                }
//            } else {
//                self.tblView.loadNoDataFoundView(message: StringConstants.Nodata.NoDataFound, image: "NoData") {
//                }
//            }
//        }) { (failureMessage, failureCode) in
//            if failureMessage == "No Internet" {
//                self.tblView.loadNoDataFoundView(message: StringConstants.Nodata.kNoInternet, image: "NoInternet") {
//                }
//            } else {
//                Utilities.showAlertView(message: failureMessage)
//            }
//        }
//                
//    }
//}
