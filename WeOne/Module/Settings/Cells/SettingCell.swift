//
//  SettingCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 24/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {

    //MARK: - Variables
    var cellModel:CellModel?
    
    //MARK: - Outlets
    @IBOutlet weak var viewContainer: UIView!

    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var viewLineTop: UIView!

    @IBOutlet weak var imgSelected: UIImageView!

    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var btnSwitch: UIButton!
    @IBOutlet weak var btnSwitchWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintViewLineTopLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintViewLineTopTrailing: NSLayoutConstraint!
    @IBOutlet weak var constraintViewLineTopHeight: NSLayoutConstraint!

    @IBOutlet weak var constraintViewLineLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintViewLineTrailing: NSLayoutConstraint!
    @IBOutlet weak var constraintViewLineHeight: NSLayoutConstraint!

    @IBOutlet weak var constraintLblNameLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintbtnSwitchTrailing: NSLayoutConstraint!

    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        
        if model.cellType == .SettingSeprator {
            viewContainer.isHidden = true
        }

        if model.classType == .TripHistoryContainerVC {
            
//            lblName.font = FontScheme.kMediumFont(size: 16)

            imgSelected.image = UIImage(named : ThemeManager.sharedInstance.getImage(string: "CircleChecked"))
            
            viewLine.isHidden = true
            if model.isSelected {
                imgSelected.isHidden = false
            } else {
                imgSelected.isHidden = true
            }
            
        } else if model.classType == .SettingsVC || model.classType == .NotificationSettingsVC {
//            lblName.font = FontScheme.kMediumFont(size: 16)
            viewContainer.backgroundColor = ColorConstants.TextColorWhitePrimary
            viewLineTop.isHidden = false
            viewLine.isHidden = true
            if model.isLast {
                
               // lblName.font = FontScheme.kSemiBoldFont(size: 16)

                constraintViewLineTopLeading.constant = 0
                constraintViewLineTopTrailing.constant = 0
                constraintViewLineTopHeight.constant = 3

            } else if model.classType == .NotificationSettingsVC {
                
                viewLineTop.isHidden = true
                viewLine.isHidden = false

                constraintLblNameLeading.constant = 30
                constraintbtnSwitchTrailing.constant = 30
                
                if model.cellType == .NotificationAccountText {
                    constraintViewLineHeight.constant = 3
                    constraintViewLineLeading.constant = 0
                    constraintViewLineTrailing.constant = 0

                } else {
                    constraintViewLineHeight.constant = 1
                    constraintViewLineLeading.constant = 30
                    constraintViewLineTrailing.constant = 30

                }

            } else {
                constraintViewLineTopLeading.constant = 20
                constraintViewLineTopTrailing.constant = 20
                constraintViewLineTopHeight.constant = 1
            }
        }
        
        
        cellModel = model
        lblName.text = model.placeholder
        btnSwitch.isSelected = model.isSelected
//        btnSwitch.tintColor = ColorConstants.ThemeColor
        btnSwitchWidth.constant = (model.classType == .NotificationSettingsVC) ? 55 : 0
        btnSwitch.isHidden = (model.classType == .NotificationSettingsVC) ? false : true
        
        
    }
    
    //MARK:- IBActions
    @IBAction func btnSwitch_Click(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        cellModel?.isSelected = sender.isSelected
    }
    
}
