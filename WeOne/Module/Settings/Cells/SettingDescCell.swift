//
//  SettingCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 24/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class SettingDescCell: UITableViewCell {

    //MARK: - Variables
    var cellModel:CellModel?
    
    //MARK: - Outlets
    @IBOutlet weak var viewLine: UIView!
    
    @IBOutlet weak var constraintViewLineHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintViewLineLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintViewLineTrailing: NSLayoutConstraint!

    @IBOutlet weak var constraintLblNameLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintLblNameTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        
        if model.cellType == .SettingLogout {
            viewLine.isHidden = true
        }
        
        if model.classType == .NotificationSettingsVC {
            constraintViewLineLeading.constant = 30
            constraintViewLineTrailing.constant = 30
            
            constraintLblNameLeading.constant = 30
            constraintLblNameTrailing.constant = 30

           // lblName.font = FontScheme.kSemiBoldFont(size: 18)
//            lblDesc.font = FontScheme.kLightFont(size: 16)
        }
        
        cellModel = model
        lblName.text = model.placeholder
        lblDesc.text = model.placeholder2
    }
    
    
}
