//
//  OnGoingRide_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 14/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class OnGoingRide_ViewController: UIViewController {

    @IBOutlet weak var header_Label: UILabel!
    @IBOutlet weak var support_View: ViewDesign!
    @IBOutlet weak var header_navigation_View: UIView!
    var send_Event_Method :((String) -> ())?
    @IBOutlet weak var detail_View: UIView!
    var checkScreen = ""
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    var detailModel : VehicleDetailModel = VehicleDetailModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialConfig()
        if checkScreen == "KeyPickup" {
            detail_View.isHidden = true
             addBottomSheetView()
        } else {
            
        }
    }
    
    func initialConfig() {
//           if AppConstants.hasSafeArea {
//               headerView_HeightConstraint.constant = 90
//           }
//           else {
//               headerView_HeightConstraint.constant = 74
//           }
       }

    func addBottomSheetView(scrollable: Bool? = true) {
        let bottomSheetVC = ScrollableBottomSheetViewController()
        bottomSheetVC.detailModel = self.detailModel
        bottomSheetVC.send_Event_Method = { (value) in
            print(value)
            if value == "up" {
                self.header_navigation_View.isHidden = true
                //self.header_Label.text = "Ride Started. Have a safe journey."
            } else {
                self.header_navigation_View.isHidden = false
                self.header_Label.text = "Navigate to car return location. "
            }
        }
        bottomSheetVC.lockUnlock = { (value) in
            if value == "unlock" {
                self.header_Label.text = "Car is open now"
            } else {
                self.header_Label.text = "Car is locked now"
            }
        }
        self.addChild(bottomSheetVC)
        self.view.addSubview(bottomSheetVC.view)
        bottomSheetVC.didMove(toParent: self)
        
        let height = view.frame.height
        let width  = view.frame.width
        bottomSheetVC.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func support_Btn(_ sender: UIButton) {
        support_View.isHidden = false
    }
    
    @IBAction func support_View_Btn(_ sender: UIButton) {
        support_View.isHidden = true
    }
    
    @IBAction func road_Assitance_Btn(_ sender: UIButton) {
        let VC = RoadAssitance_VC()
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func car_Return_Btn(_ sender: UIButton) {
//        UserDefaults.standard.set("shownavigation", forKey: "shownavigation")
//        UserDefaults.standard.synchronize()
        let VC = Navigation_Home_ViewController()
        navigationController?.pushViewController(VC, animated: true)
//        let VC = HomeVC()
//        send_Event_Method?("home")
//        navigationController?.popAllAndSwitch(to: VC)
    }
}


