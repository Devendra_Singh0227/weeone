//
//  ScrollableBottomSheetViewController.swift
//  BottomSheet
//
//  Created by Ahmed Elassuty on 10/15/16.
//  Copyright © 2016 Ahmed Elassuty. All rights reserved.
//

import UIKit

class ScrollableBottomSheetViewController: UIViewController {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var endRide_Btn: UIButton!
    @IBOutlet weak var telematicsEnd_Btn: ButtonDesign!
    @IBOutlet weak var telematicsEndBtn: UIStackView!
    @IBOutlet weak var endRide_TopConsraint: NSLayoutConstraint!
    @IBOutlet weak var lblVehicleName: UILabel!
    @IBOutlet weak var lblVehicleNumber: UILabel!
    @IBOutlet weak var unlock_Image: UIImageView!
    @IBOutlet weak var lock_Image: UIImageView!
        
    var detailModel : VehicleDetailModel = VehicleDetailModel()
    var send_Event_Method :((String) -> ())?
    var lockUnlock : ((String) -> ())?
    let fullView: CGFloat = 200
    var partialView: CGFloat {
        return UIScreen.main.bounds.height - 210
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gesture = UIPanGestureRecognizer.init(target: self, action: #selector(ScrollableBottomSheetViewController.panGesture))
        gesture.delegate = self
        view.addGestureRecognizer(gesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        lblVehicleName.text = self.detailModel.vehicle?.name ?? ""
//        lblVehicleNumber.text = self.detailModel.vehicle?.numberPlate
//        if self.detailModel.vehicle?.iotProvider == 2 {
//
//        } else {
//             telematicsEndBtn.isHidden = true
//            telematicsEnd_Btn.isHidden = true
//            endRide_Btn.isHidden = false
//        }
        prepareBackgroundView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.6, animations: { [weak self] in
            let frame = self?.view.frame
            let yComponent = self?.partialView
            self?.view.frame = CGRect(x: 0, y: yComponent!, width: frame!.width, height: frame!.height - 100)
            })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func return_Car_Location_Btn(_ sender: UIButton) {
        let VC = HomeVC()
        navigationController?.popAllAndSwitch(to: VC)
    }
    
    @IBAction func endRideBtn(_ sender: ButtonDesign) {
        endRidePopUp()
    }
    
    func endRidePopUp() {
        let vc = EndRidePopup_ViewController()
        
        vc.confirmClicked = {
            let VC = AddRentalReport_ViewController()
            VC.detailModel = self.detailModel
            self.navigationController?.pushViewController(VC, animated: true)
        }
        
        vc.cancelBtnClicked = {
            self.dismiss(animated: true, completion: nil)
        }
        
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        
        if UIViewController.current()?.presentedViewController != nil {
            UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
            })
        }
        else{
            UIViewController.current()?.present(vc, animated: true, completion: {
            })
        }
    }
    
    @IBAction func unlock_Btn(_ sender: UIButton) {
        unlock_Image.image = UIImage(named: "unlockactive")
        lock_Image.image = UIImage(named: "lockinactive")
        lockUnlock?("unlock")
    }
    
    @IBAction func lock_Btn(_ sender: UIButton) {
        lock_Image.image = UIImage(named: "lockactive")
        unlock_Image.image = UIImage(named: "unlockInactive")
        lockUnlock?("lock")
    }
    
    @IBAction func endRide_Btn(_ sender: UIButton) {
        endRidePopUp()
//        let vc = CancellationPopUp()
//        vc.modalPresentationStyle = .custom
//        vc.modalTransitionStyle = .crossDissolve
//        vc.confirmClicked = {
//
//        }
//
//        vc.viewCancellation_Btn = {
//            let Vc = CancellationPolicy_ViewController()
//            self.navigationController?.pushViewController(Vc, animated: true)
//        }
//            UIViewController.current()?.present(vc, animated: true, completion: {
//        })
    }
    
    @objc func panGesture(_ recognizer: UIPanGestureRecognizer) {
        
        let translation = recognizer.translation(in: self.view)
        let velocity = recognizer.velocity(in: self.view)
//        self.endRide_TopConsraint.constant = -260
        let y = self.view.frame.minY
        if (y + translation.y >= fullView) && (y + translation.y <= partialView) {
            self.view.frame = CGRect(x: 0, y: y + translation.y, width: view.frame.width, height: view.frame.height)
            recognizer.setTranslation(CGPoint.zero, in: self.view)
        }
        
        if recognizer.state == .ended {
            var duration =  velocity.y < 0 ? Double((y - fullView) / -velocity.y) : Double((partialView - y) / velocity.y )
            
            duration = duration > 1.3 ? 1 : duration
            
            UIView.animate(withDuration: duration, delay: 0.0, options: [.allowUserInteraction], animations: {
                if  velocity.y >= 0 {
                    self.send_Event_Method?("down")
                    self.endRide_TopConsraint.constant = -224
                    self.view.frame = CGRect(x: 0, y: self.partialView, width: self.view.frame.width, height: self.view.frame.height)
                } else {
                    self.view.frame = CGRect(x: 0, y: self.fullView, width: self.view.frame.width, height: self.view.frame.height)
                }
                
                }, completion: { [weak self] _ in
                    if ( velocity.y < 0 ) {
                        self?.endRide_TopConsraint.constant = 30
                        self?.send_Event_Method?("up")
                       // print("kkkkkk")
//                        self?.tableView.isScrollEnabled = true
                    }
                   
                     //print("jjjjjj")
            })
        }
    }
    
    
    func prepareBackgroundView(){
        let blurEffect = UIBlurEffect.init(style: .light)
        let visualEffect = UIVisualEffectView.init(effect: blurEffect)
        let bluredView = UIVisualEffectView.init(effect: blurEffect)
        bluredView.contentView.addSubview(visualEffect)
        visualEffect.frame = UIScreen.main.bounds
        bluredView.frame = UIScreen.main.bounds
        view.insertSubview(bluredView, at: 0)
    }

}

extension ScrollableBottomSheetViewController: UIGestureRecognizerDelegate {

    // Solution
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        let gesture = (gestureRecognizer as! UIPanGestureRecognizer)
        let direction = gesture.velocity(in: view).y

        let y = view.frame.minY
        if (y == fullView && direction > 0) || (y == partialView) {
//            tableView.isScrollEnabled = false
        } else {
//            tableView.isScrollEnabled = true
        }
        
        return false
    }
    
}
