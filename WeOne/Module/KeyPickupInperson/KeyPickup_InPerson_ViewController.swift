//
//  KeyPickup_InPerson_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 12/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit
import UICircularProgressRing
import HCSStarRatingView

class KeyPickup_InPerson_ViewController: UIViewController {

    @IBOutlet weak var photos_CollectionView: UICollectionView!
    @IBOutlet weak var photos_View: UIView!
    @IBOutlet weak var locationView_LineTop_Constraint: NSLayoutConstraint!
    @IBOutlet weak var drop_Image: UIImageView!
    @IBOutlet weak var support_View: ViewDesign!
    @IBOutlet weak var timer_View: ViewDesign!
    @IBOutlet weak var ring: UICircularTimerRing!
    @IBOutlet weak var header_Title: UILabel!
    @IBOutlet weak var navigate_Btn: UIButton!
    @IBOutlet weak var call_Btn: UIButton!
    @IBOutlet weak var move_Arrow_View: UIView!
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    var detailModel : VehicleDetailModel = VehicleDetailModel()
    
    @IBOutlet weak var lblVehicleName: UILabel!
    @IBOutlet weak var lblVehicleNumber: UILabel!
    @IBOutlet weak var lblPricePerDay: UILabel!
    @IBOutlet weak var user_Image: UIImageView!
    @IBOutlet weak var rating_View: HCSStarRatingView!
    @IBOutlet weak var name_Lbl: UILabel!
    @IBOutlet weak var pin_Lbl: UILabel!
    @IBOutlet weak var location_Lbl: UILabel!
    var phoneNumber = "+919997423577"
    var timer = Timer()
    var Count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
         photos_CollectionView.register(UINib(nibName: "Location_Photos_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Location_Photos_CollectionViewCell")
 
    }
    
    func initialConfig() {
//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
        pin_Lbl.text = detailModel.vehicle?.vinNumber ?? ""
        lblVehicleName.text = detailModel.vehicle?.name ?? ""
        lblVehicleNumber.text = detailModel.vehicle?.numberPlate
        lblPricePerDay.text = "\(detailModel.vehicle?.packages.first?.rate ?? 0) Nok/Hour"
        location_Lbl.text = detailModel.vehicle?.geoLocation?.name ?? ""
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timeRemaining), userInfo: nil, repeats: true)
        if self.detailModel.vehicle?.iotProvider == AppConstants.IOTProvider.WithTelemetric {
            self.timer_View.isHidden = false
            let time: Double = 600//Double(600 - self.Count)
            self.set_Progress(Time: time)
            self.startTimer(Time: time)
        } else {
            
        }
    }
    
    @objc func timeRemaining() {
        Count += 1
    }
    
    func set_Progress(Time: Double) {
        ring.style = .ontop
        ring.innerRingWidth = 6
        ring.innerRingColor = #colorLiteral(red: 0.5764763355, green: 0.2338527441, blue: 0.8235091567, alpha: 1)
        ring.outerRingWidth = 4
        ring.outerRingColor = #colorLiteral(red: 0.7546172738, green: 0.7448188663, blue: 0.7663549185, alpha: 1)
        ring.startAngle = 270
        ring.isClockwise = false
        ring.valueFormatter = TimerRingFormatter(timerTo: Int(Time))
        ring.font = UIFont.systemFont(ofSize: 14)
        ring.translatesAutoresizingMaskIntoConstraints = false
    }

    private func startTimer(Time: Double) {
           self.ring.startTimer(to: Time) { (state) in
               switch state {
               case .finished:
                   print("finished")
               default: break
               }
           }
       }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
   
    @IBAction func drop_down_Btn(_ sender: UIButton) {
        if sender.tag == 0 {
            drop_Image.image = UIImage(named: "back_down")
            sender.tag = 1
            photos_View.isHidden = false
            locationView_LineTop_Constraint.constant = 258
        } else {
            sender.tag = 0
             drop_Image.image = UIImage(named: "back_right")
             photos_View.isHidden = true
            locationView_LineTop_Constraint.constant = 36
        }
    }
    
    @IBAction func support_Btn(_ sender: UIButton) {
        support_View.isHidden = false
    }
    
    @IBAction func support_View_Btn(_ sender: UIButton) {
        support_View.isHidden = true
    }
    
    @IBAction func navigate_BtnAction(_ sender: UIButton) {
         Utilities.openMap(latitude: detailModel.vehicle?.geoLocation?.longitude ?? 0.0, longitude: detailModel.vehicle?.geoLocation?.latitude ?? 0.0)
    }
    
    @IBAction func inspect_ride_Btn(_ sender: UIButton) {
        let VC = Inspect_Car_ViewController()
        VC.callBack_Method = {
            self.call_Btn.backgroundColor = ColorConstants.TextColorPrimary
             self.call_Btn.isUserInteractionEnabled = true
            self.call_Btn.setTitle("START RIDE", for: .normal)
            self.call_Btn.setTitleColor(ColorConstants.commonWhite, for: .normal)
//            self.header_Title.text = "Thank you for inspection. You can start your ride now."
             DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                self.move_Arrow_View.isHidden = true
                self.timer_View.isHidden = false
                let time: Double = 600//Double(600 - self.Count)
                self.set_Progress(Time: time)
                self.startTimer(Time: time)
               
             })
        }
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func chat_Btn(_ sender: UIButton) {
        let VC = Chat_ViewController()
        navigationController?.pushViewController(VC, animated: true)
    }
    
    @IBAction func cancel_Ride_Btn(_ sender: UIButton) {
        let vc = CancellationPopUp()
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        vc.confirmClicked = {
           
        }
        
        vc.viewCancellation_Btn = {
            let Vc = CancellationPolicy_ViewController()
            self.navigationController?.pushViewController(Vc, animated: true)
        }
            UIViewController.current()?.present(vc, animated: true, completion: {
        })
    }
    
    @IBAction func call_Btn(_ sender: UIButton) {
        if sender.currentTitle == "CALL" {
            let phone = self.phoneNumber.replacingOccurrences( of:"[^0-9]", with: "", options: .regularExpression)
            
            guard let number = URL(string: "tel://" + phone) else { return }
            UIApplication.shared.open(number)
        } else {
            let vc = ConfirmPopupVC()
             vc.headerTitle = "Start Ride"
            vc.message = "Are you sure you want to start the ride?"
            vc.modalPresentationStyle = .custom
            vc.modalTransitionStyle = .crossDissolve
            vc.confirmClicked = {
                let Vc = OnGoingRide_ViewController()
                Vc.checkScreen = "KeyPickup"
                Vc.detailModel = self.detailModel
                self.navigationController?.pushViewController(Vc, animated: true)
            }
                UIViewController.current()?.present(vc, animated: true, completion: {
            })
        }
    }
}

extension KeyPickup_InPerson_ViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4//arrCollData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //let model = arrCollData[indexPath.row]
        let cell = photos_CollectionView.dequeueReusableCell(withReuseIdentifier: "Location_Photos_CollectionViewCell", for: indexPath) as! Location_Photos_CollectionViewCell
        
//        cell.setData(model, rideType: rideType, startDate: reservationStartDate, endDate: reservationEndDate)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: 100, height: 80)
    }
}

public struct TimerRingFormatter: UICircularRingValueFormatter {
       
       public var timerTo: Int
       
       public init(timerTo: Int) {
           self.timerTo = timerTo
       }
       
       public func string(for value: Any) -> String? {
           guard let value = value as? CGFloat else { return nil }
           let duration: Int = self.timerTo - Int(value)
           let s: Int = Int(duration) % 60
           let m: Int = Int(duration) / 60
           
           let formattedDuration = String(format: "%02d:%02d", m, s)
           return formattedDuration
       }
   }
