//
//  NotificationVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 24/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {
    
    //MARK: Variables
    var page = 1
    var totalCount = 0
    var arrNotification : [NotificationModel] = []
    
    var arrSelectedID = [Int]()
    var arrIds = [String]()
    var isCheckVisible = false
    var deleteall = ""
    
    //MARK: outlets
    @IBOutlet weak var viewNavigation: UIViewCommon!
    @IBOutlet weak var viewClearAll: UIView!
    // @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var clear_All_Btn: UIButton!
    
    @IBOutlet weak var viewClearAllHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imgChecked: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnMarkAsRead: UIButton!
   
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    
    //MARK: View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
        
        callApiForNotificationList()
    }
    
    //MARK: initial config
    func initialConfig(){

        tableView.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        setUpRefreshControl()
        
        //        viewNavigation.click = {
        //            self.btnClose_Click()
        //        }
        //
        //        viewNavigation.text = "KLblNotification".localized as NSString
        //        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPress(_:)))
        //        self.view.addGestureRecognizer(longPressRecognizer)
        
    }
    
    func setUpRefreshControl(){
        
        tableView.es.addPullToRefresh {
            self.resetData()
        }
        
        tableView.es.addInfiniteScrolling {
            if self.totalCount == self.arrNotification.count{
                self.tableView.es.noticeNoMoreData()
                self.tableView.es.resetNoMoreData()
            }else{
                self.page = self.page + 1
                self.callApiForNotificationList()
            }
        }
    }
    
    func resetData() {
        viewClearAll.isHidden = true
        viewClearAllHeight.constant = 0
        self.page = 1
        self.totalCount = 0
        self.arrNotification.removeAll()
//        self.callApiForNotificationList()
    }
    
    //    func hideViewMarkRead() {
    //
    //        self.isCheckVisible = false
    //        for model in arrNotification {
    //            model.isSelected = false
    //        }
    //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
    //
    //            self.viewMarkRead.alpha = 0
    //
    //            UIView.animate(withDuration: 0.6, animations: { [weak self] in
    //                self?.viewMarkReadHeight.constant = 0
    //                self?.imgChecked.image = #imageLiteral(resourceName: "UncheckRound")
    //                self?.viewMarkRead.isHidden = true
    //            })
    //        })
    //
    //    }
    
    //    func showViewMarkRead() {
    //        viewMarkRead.alpha = 0
    //        viewMarkRead.isHidden = false
    //        self.isCheckVisible = true
    //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
    //            UIView.animate(withDuration: 0.6, animations: { [weak self] in
    //                self?.viewMarkReadHeight.constant = 61
    //                self?.viewMarkRead.alpha = 1
    //
    //            })
    //        })
    //
    //        tableView.reloadData()
    //
    //    }
    //Called, when long press occurred
    //    @objc func longPress(_ sender: UILongPressGestureRecognizer) {
    //        if viewMarkRead.isHidden {
    //            showViewMarkRead()
    //        }
    //    }
    
    //MARK: Button action methods
    //    func btnClose_Click() {
    //
    //        if !viewMarkRead.isHidden {
    //            hideViewMarkRead()
    //            tableView.reloadData()
    //        } else{
    //            self.navigationController?.popViewController(animated: true)
    //        }
    //    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
//    @IBAction func btnViewMarkRead_Click(_ sender: UIButton) {
//
//        sender.isSelected = !sender.isSelected
//        if sender.isSelected {
//            imgChecked.image = #imageLiteral(resourceName: "checkMarkGreen")
//        }
//
//        for model in arrNotification {
//            model.isSelected = true
//        }
//
//        reloadData()
//    }
    
//    @IBAction func btnDelete_Click(_ sender: UIButton) {
//
//        let arr = arrNotification.filter {$0.isSelected == true}.map { (obj) -> String in
//            return obj.id ?? ""
//        }
//
//        if arr.count > 0 {
//            callAPIForDelete(isUpdateAll: true, ids: arr)
//        } else {
//            Utilities.showAlertView(message: "KLblNoUnreadNotification".localized)
//        }
//
//        //        hideViewMarkRead()
//
//    }
    
    @IBAction func select_All_Btn(_ sender: UIButton) {
//        for i in 0..<4 {
//            arrSelectedID.append(i)
//        }
//        tableView.reloadData()
    }
    
    @IBAction func delete_Btn(_ sender: UIButton) {
        clearNotification()
    }
    
    func clearNotification()  {
        let vc = ConfirmPopupVC()
        vc.headerTitle = "Clear All Notification"
        vc.message = "Are you sure you want to Clear all notifications?"
        vc.confirmClicked = {
            self.callAPIForDelete(isUpdateAll: true, ids: self.arrSelectedID)
        }
        
        vc.cancelBtnClicked = {
            self.dismiss(animated: true, completion: nil)
        }
        
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        
        if UIViewController.current()?.presentedViewController != nil {
            UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
            })
        }
        else{
            UIViewController.current()?.present(vc, animated: true, completion: {
            })
        }
    }
    
    @IBAction func btnDeleteAll_Click(_ sender: UIButton) {
        clearNotification()
//        viewClearAll.isHidden = false
//        viewClearAllHeight.constant = 50
//        deleteall = "show"
//        tableView.reloadData()
        //        let arr = arrNotification.map { $0.id }
        //
        //
        //        if arr.count > 0 {
        //            callAPIForDelete(isUpdateAll: true, ids: arr as! [String])
        //        } else {
        //            Utilities.showAlertView(message: "KLblNoUnreadNotification".localized)
        //        }
        //
        //        hideViewMarkRead()
    }
}

//MARK: Tableview methods
extension NotificationVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrNotification[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        cell.lblTitle.text = model.title
        cell.lblMsg.text = model.message
        if let createdDT = model.createdAt {

            let startDate = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: createdDT), format: DateUtilities.DateFormates.kLongDate)

            cell.lblTime.text = startDate.capitalized
        }

        if deleteall == "" {
            cell.CheckMark_width_Constraint.constant = 0
        } else {
            
            if arrSelectedID.contains(model.id!) {
//            if arrSelectedID.contains(indexPath.row) {
                viewClearAll.isHidden = false
                viewClearAllHeight.constant = 50
                cell.CheckMark_width_Constraint.constant = 30
                cell.viewMain.backgroundColor = UIColor(red: 247/255, green: 247/255, blue: 252/255, alpha: 1.0)
                cell.imgChecked.image =  #imageLiteral(resourceName: "checkMarkGreen")
                cell.imgChecked.isHidden = false
            } else {
                cell.CheckMark_width_Constraint.constant = 30
                cell.viewMain.backgroundColor = UIColor.white
                cell.imgChecked.image =  #imageLiteral(resourceName: "UncheckRound")
                cell.imgChecked.isHidden = false
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        deleteall = "show"
        // let model = arrNotification[indexPath.row]
        //        if arrSelectedID.contains(indexPath.row) {
        //            arrSelectedID = arrSelectedID.filter { $0 != indexPath.row }
        //        } else {
        //            arrSelectedID.append(indexPath.row)
        //            //arrIds.append(model.id!)
        //        }
        
        let model = arrNotification[indexPath.row]
        
        model.isSelected = !model.isSelected
        if arrSelectedID.contains(model.id! ) {
            arrSelectedID = arrSelectedID.filter { $0 != model.id }
        } else {
            arrSelectedID.append(model.id! )
        }
        //
        tableView.reloadData()
        //
        //        UIView.setAnimationsEnabled(false)
        //        self.tableView.beginUpdates()
        //        self.tableView.endUpdates()
        //        UIView.setAnimationsEnabled(true)
        
    }
    
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
//
//        let model = self.arrNotification[indexPath.row]
//
//        //        if !viewMarkRead.isHidden {
//        //            return nil
//        //        }
//
//        if orientation == .left {
//            let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
//
//                self.callAPIForDelete(isUpdateAll: true, ids: [(model.id ?? "")])
//            }
//
//            deleteAction.image = UIImage(named: "bin")
//
//            return [deleteAction]
//
//        } else {
//
//            if model.status == AppConstants.NotificationStatus.READ {
//                return nil
//            }
//
//            let readAction = SwipeAction(style: .default, title: "Read") { action, indexPath in
//                if model.status == AppConstants.NotificationStatus.SEND {
//                    self.callAPIForUpdateStatus(model:model)
//                }
//            }
//
//            readAction.image = UIImage(named: "Read")
//            readAction.backgroundColor = ColorConstants.ThemeColor
//
//            return [readAction]
//
//        }
//    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK: Api calls
extension NotificationVC{
    
    func callApiForNotificationList(){
        
        if page == 1{
            NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        }
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.NotificationList, method: .get, parameters: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, messsage) in
            print(response)
            if let dictResponse = response as? [[String:Any]]{
                
                for item in dictResponse{
                    self.arrNotification.append(Mapper<NotificationModel>().map(JSON: item)!)
                }
            }
            self.reloadData()
        }) { (failureMessage, failureCode) in
            self.reloadData()
        }
    }
    
    func reloadData() {
        
        if arrNotification.count == 0 || arrNotification.count <= 0 {
            clear_All_Btn.isHidden = true
            viewClearAll.isHidden = true
            viewClearAllHeight.constant = 0
            self.tableView.loadNoDataFoundView(message: StringConstants.Nodata.KMsgNoNotification, image: "Group 4472") {
            }
        } else {
            tableView.removeLoadAPIFailedView()
        }
//        tableView.es.stopLoadingMore()
//        tableView.es.stopPullToRefresh()
        tableView.reloadData()
    }
    
    func callAPIForUpdateStatus(isUpdateAll:Bool = false, ids:[String] = [String](), model:NotificationModel? = nil) {
        var param : [String:Any] = [:]
        
        if ids.count > 0 {
            param["ids"] = ids
        }
        else if let obj = model {
            param["ids"] = [obj.id]
        }
        
        param["status"] = AppConstants.NotificationStatus.READ
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.NotificationStatusUpdate, method: .patch, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, messsage) in
            
            if isUpdateAll {
                self.resetData()
            }
            else {
                model?.status = AppConstants.NotificationStatus.READ
                
                UIView.setAnimationsEnabled(false)
                self.tableView.beginUpdates()
                self.tableView.reloadData()
                self.tableView.endUpdates()
                UIView.setAnimationsEnabled(true)
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
//    func callAPIForDelete(isUpdateAll:Bool = false, ids:[String] = [String](), model:NotificationModel? = nil) {
//        var param : [String:Any] = [:]
//
//        if ids.count > 0 {
//            param["notificationIds"] = ids
//        }
//        else if let obj = model {
//            param["notificationIds"] = [obj.id]
//        }
//
//        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
//        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.NotificationDelete, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, messsage) in
//            self.resetData()
//            self.callApiForNotificationList()
//        }) { (failureMessage, failureCode) in
//            Utilities.showAlertView(message: failureMessage)
//        }
//    }
    
    func callAPIForDelete(isUpdateAll:Bool = true, ids:[Int]) {
        
        var param : [String:Any] = [:]
        param["ids"] = ids
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.NotificationDeleteSelected, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, messsage) in
            self.arrNotification.removeAll()
            self.callApiForNotificationList()
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
