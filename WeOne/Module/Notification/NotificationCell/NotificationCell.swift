//
//  NotificationCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 24/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var viewMain: UIView!
    
    @IBOutlet weak var imgChecked: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var CheckMark_width_Constraint: NSLayoutConstraint!
    
    @IBOutlet weak var msgLblTrailing_Consraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(_ model : NotificationModel, isCheckVisible: Bool){
        
        lblTitle.text = model.title
        lblMsg.text = model.message
        
      //  lblTime.textColor = ColorConstants.MoreInformtion.MICardTextColor
        
        if isCheckVisible {
           // constraintImgCheckedWidth.constant = 21
            //constraintImgCheckedTrailing.constant = 30
            imgChecked.isHidden = false 
        } else {
           // constraintImgCheckedWidth.constant = 0
            //constraintImgCheckedTrailing.constant = 0
            imgChecked.isHidden = true
        }
        
        if let createdDT = model.createdAt {
            
            let startDate = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: createdDT), format: DateUtilities.DateFormates.kLongDate)

            lblTime.text = startDate.capitalized
        }
        
        if model.isSelected {
            imgChecked.image = #imageLiteral(resourceName: "checkMarkGreen")
        } else {
            imgChecked.image = #imageLiteral(resourceName: "UncheckRound")
        }
        
//        if model.status == AppConstants.NotificationStatus.SEND  {
//            viewMain.backgroundColor = UIColor.white
//        } else if model.status == AppConstants.NotificationStatus.SELECTED {
//            viewMain.backgroundColor = ColorConstants.ThemeGradientEndColor.withAlphaComponent(0.1)
//        } else {
//            viewMain.backgroundColor = ColorConstants.NotificationBGColor
//        }
    }
}
