//
//  NotificationListTableViewCell.swift
//  WeOne
//
//  Created by Dev's Mac on 09/09/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class NotificationListTableViewCell: UITableViewCell {

    @IBOutlet weak var msg_Lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
