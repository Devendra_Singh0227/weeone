//
//  SetLocationVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 04/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class SetLocationVC: UIViewController {
    
    //MARK: variables
    var placesModel = PlacesModel()
    var getLocation : ((_ model : PlacesModel) -> ())?
    
    var currentLocation: CLLocation = CLLocation()
    var geoLocation: LocationModel?

    var isFromKeyHandling = false
    
    //MARK: Outlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var imgMarker: UIImageView!

    @IBOutlet weak var lblTitle: UILabel!
//    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnSetLocation: UIButtonCommon!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewTitle: ShadowCard!
    @IBOutlet weak var location_TxtFld: UITextField!
    var pick_locationField : ((String) -> ())?
    var drop_locationField : ((String) -> ())?
    @IBOutlet weak var headerView_HeightConstraint: NSLayoutConstraint!
    
    //MARK: View life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
        mapView.delegate = self

        if isFromKeyHandling {
            if geoLocation != nil {
                btnSetLocation.setText(text: "KLblNavigateCap".localized)
            }
            
            imgMarker.isHidden = true
            
            lblTitle.text = geoLocation?.street
        }
        
//        btnSetLocation.click = {
//            self.btnSetLocation_Click()
//        }

        
        DispatchQueue.main.async {
            LocationManager.SharedManager.getLocation()
            LocationManager.SharedManager.getCurrentLocation = { location in
                self.showCurrentLocation(location)
                self.currentLocation = location
                if self.isFromKeyHandling {
                    self.drawPathWithouStopLocation()
                }
            }
        }
        
        loadMapJsonFile()
        prepareBackgroundView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        mapView.removeFromSuperview()
    }
    
    func initialConfig() {
        
//        if AppConstants.hasSafeArea {
//            headerView_HeightConstraint.constant = 90
//        }
//        else {
//            headerView_HeightConstraint.constant = 74
//        }
    }
    
    func prepareBackgroundView(){
        
//        viewBottom.layer.cornerRadius = 20
//
//        viewBottom.layer.shadowColor = UIColor.black.withAlphaComponent(1.0).cgColor
//        viewBottom.layer.shadowOpacity = 0.3
//        viewBottom.layer.shadowOffset = CGSize(width: 0, height: -5)
//        viewBottom.layer.shadowRadius = 10.0
//        viewBottom.layer.masksToBounds = false
//        viewBottom.layer.shadowPath = CGPath(ellipseIn: CGRect(x: -20,
//                                                             y: -20 * 0.5,
//                                                             width: viewBottom.layer.bounds.width + 20 * 2,
//                                                             height: 5),
//                                           transform: nil)
        
    }
    
    //MARK: Button action methods
    
    @IBAction func btnBack_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func done_Btn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func btnSetLocation_Click(){
        if isFromKeyHandling {
            
        } else {
//            btnSetLocation.click = {
                
                if self.placesModel.name != nil{
                    self.getLocation?(self.placesModel)
                    self.navigationController?.popViewController(animated: true)
                }
//            }
        }
    }
    
    //Map json file
    func loadMapJsonFile(){
        do {
            // Set the map style by passing the URL of the local file.
            if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
    }
    
    func drawPathWithouStopLocation() {

        Utilities.getRoutesBetweenCoordinates(CLLocation(latitude: currentLocation.coordinate.latitude , longitude: currentLocation.coordinate.longitude ), CLLocation(latitude: geoLocation?.latitude ?? 0.0, longitude: geoLocation?.longitude ?? 0.0)) { (dict) in
            
            self.mapView.clear()
            
//            if self.geoLocation != self.currentLocation{
//            }
            self.dropPathMarker(CLLocationCoordinate2D(latitude: self.geoLocation?.latitude ?? 0.0, longitude: self.geoLocation?.longitude ?? 0.0), imageName: ThemeManager.sharedInstance.getImage(string: "markerCarKey"), scaledToSize: CGSize(width: 42, height: 49))

            if let arr = dict?["routes"] as? [[String:Any]], arr.count > 0 {
                self.drawRoute(arr: arr)
            }
        }
        
    }
    
    func drawRoute(arr: [[String:Any]]) {
        
        if let path = GMSPath.init(fromEncodedPath: (arr.first?["overview_polyline"] as? [String:Any] ?? [:])?["points"] as? String ?? "") {
            
            let polyline = GMSPolyline(path: path)
            polyline.strokeWidth = 3.0
            polyline.strokeColor = ColorConstants.ThemeColor
            polyline.map = self.mapView
            
            let bounds = GMSCoordinateBounds(path: path)
            
            
            self.mapView.animate(with: GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top:  100, left: 50, bottom: 260, right: 50)))
            
            
        } else {
            print("error some error in fetching data")
        }
    }
    func dropPathMarker(_ location : CLLocationCoordinate2D, imageName: String, scaledToSize: CGSize? = CGSize()) {
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(location.latitude, location.longitude)
        if scaledToSize == nil {
            marker.icon = UIImage(named: imageName)
        } else {
            marker.icon = self.imageWithImage(image: UIImage(named: imageName)!, scaledToSize: CGSize(width: 42, height: 49))
        }
        
        
        marker.map = self.mapView
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }


    //MARK: Map view methods
    func showCurrentLocation(_ location : CLLocation) {
        let camera : GMSCameraPosition = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 15)
        mapView.camera = camera
        //mapView.isMyLocationEnabled = true
//        if !isFromKeyHandling {
//            getAddressFromLatLon( pdblLatitude: location.coordinate.latitude, withLongitude: location.coordinate.longitude){ title, address in
//                //self.lblTitle.text = address
//             //   self.lblDesc.text = address
//            }
//        }

    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double, completion : ((_ name : String , _ address : String) -> ())?) {
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude
        let lon: Double = pdblLongitude
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                
                NetworkClient.sharedInstance.stopIndicator()
                
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                if placemarks != nil {
                    
                    let pm = placemarks! as [CLPlacemark]
                    
                    if pm.count > 0 {
                        
                        let pm = placemarks![0]
                        
                        var arrLoc = [String]()
                        arrLoc.append(pm.subLocality ?? "")
                        arrLoc.append(pm.locality ?? "")
                        arrLoc.append(pm.administrativeArea ?? "")
                        arrLoc.append(pm.country ?? "")
                        
                        print(arrLoc.joined(separator: ", "))
                        self.pick_locationField?(arrLoc.joined(separator: ", "))
                        self.drop_locationField?(arrLoc.joined(separator: ", "))
                        completion?(pm.name ?? "-", arrLoc.joined(separator: ", "))
                        
                        self.placesModel.addressFullText = arrLoc.joined(separator: ", ")
                        self.placesModel.addressSecondaryText = pm.compactAddress
                        self.placesModel.latitude = pdblLatitude
                        self.placesModel.latitude = pdblLongitude
                        self.placesModel.name = pm.name
                        
                    }
                }
        })
        
    }
}

//MARK: Mapview delegate methods
extension SetLocationVC : GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if !isFromKeyHandling {
            let zoom = mapView.camera.zoom
            print("map idle position zoom is ",String(zoom))
            
            let loc = self.mapView.projection.coordinate(for: mapView.center)
            self.getAddressFromLatLon(pdblLatitude: loc.latitude, withLongitude: loc.longitude){ title, address in
                
               // self.lblTitle.text = address
              //  self.lblDesc.text = address
                
            }
        }
    }
}
