//
//  MoreCell.swift
//  WeOne
//
//  Created by iMac on 10/06/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class MoreCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(model:CellModel) {
        lblTitle.text = model.placeholder
    }
}
