//
//  FareSummaryVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 14/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class IssueWithLastTripVC: UIViewController {
    
    //MARK: Variables
    var arrCellData : [CellModel] = [CellModel]()
    var rideModel:RideListModel = RideListModel()
    
    var isExpand = false
    
    //MARK: Outlets
    @IBOutlet weak var viewNavigation: UIViewCommon!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnPay: UIButtonCommon!
    
    //MARK: view life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
       // initialConfig()
    }
    
    //MARK: Initial config
    func initialConfig(){
        
        viewNavigation.backgroundColor = ColorConstants.ThemeColor
        viewNavigation.text = "KLblIssueWithLastTrip".localized as NSString
        
        prepareDataSource()
        
        viewNavigation.click = {
            self.btnClose_Click()
        }
        tableView.register(UINib(nibName: "MapCell", bundle: nil), forCellReuseIdentifier: "MapCell")
        tableView.register(UINib(nibName: "TripInfoCell", bundle: nil), forCellReuseIdentifier: "TripInfoCell")
        tableView.register(UINib(nibName: "MoreInformationPrice", bundle: nil), forCellReuseIdentifier: "MoreInformationPrice")
        tableView.register(UINib(nibName: "MoreCell", bundle: nil), forCellReuseIdentifier: "MoreCell")
        tableView.register(UINib(nibName: "NeedHelpCell", bundle: nil), forCellReuseIdentifier: "NeedHelpCell")
        
    }
    
    //MARK: Prepare DataSource
    func prepareDataSource(){
        
        arrCellData.removeAll()
        
        arrCellData.append(CellModel.getModel(type: .FareSummaryCarInfo, cellObj:rideModel))
        
        if rideModel.startLocation != nil{
            arrCellData.append(CellModel.getModel(type: .IssueWithTripInfo, cellObj:rideModel))
        }
        
        
        arrCellData.append(CellModel.getModel(placeholder: "KLblTotal".localized, text: "\(Utilities.convertToCurrency(number: rideModel.fareSummary?.total ?? 0.0, currencyCode: rideModel.pricingConfigData?.currency))", type: .IssueWithLastTripTotal, cellObj: nil))
        arrCellData.append(CellModel.getModel(type: .IssueNeedHelp, cellObj: nil))
        
        arrCellData.append(CellModel.getModel(placeholder: "KLblLostItem".localized,type: .IssueWithMore, cellObj: nil))
        arrCellData.append(CellModel.getModel(placeholder: "KLblReportAccident".localized,type: .IssueWithMore, cellObj: nil))
        arrCellData.append(CellModel.getModel(placeholder: "KLblRideCancellation".localized,type: .IssueWithMore, cellObj: nil))
        arrCellData.append(CellModel.getModel(placeholder: "KLblOtherQuestion".localized,type: .IssueWithMore, cellObj: nil))
        
        
        tableView.reloadData()
        
        
    }
    
    //MARK:- IBActions
    
    func btnClose_Click() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func back_Btn(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
    
    func btnPay_Click() {
        
        if rideModel.isPaid {
            
            moveToRating()
        }
        else {
            // PROCEED TO PAY CODE HERE
            callApiForPayment()
        }
    }
    
    func moveToRating(){
        let vc = RideRatingVC(nibName: "RideRatingVC", bundle: nil)
        vc.activeRideObject = self.rideModel
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: Tableview methods
extension IssueWithLastTripVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCellData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrCellData[indexPath.row]
        
        switch model.cellType! {
            
        case .FareSummaryCarInfo:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MapCell", for: indexPath) as! MapCell
            cell.setData(model: model)
            return cell
            
        case .IssueWithTripInfo:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TripInfoCell", for: indexPath) as! TripInfoCell
            cell.setData(model: model)
            return cell
            
        case .IssueWithMore:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreCell", for: indexPath) as! MoreCell
            cell.setData(model: model)
            return cell
            
        case .IssueNeedHelp:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NeedHelpCell", for: indexPath) as! NeedHelpCell
            return cell
            
        case .IssueWithLastTripTotal:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInformationPrice") as! MoreInformationPrice
            
            cell.setData(model: model, rideType: rideModel.rideType)
            /* cell.packageSelect = {
             self.getFareSummary()
             } */
            return cell
            
            
        default:
            return UITableViewCell()
        }
    }
    
    @objc func btnChange_Click(_ sender: UIButton) {
        
        let vc = PaymentVC()
        vc.isDeleteHidden = true
        //vc.shownCardType = !arrCellData[sender.tag].isSelected ? AppConstants.CardTypes.PersonalCard :  AppConstants.CardTypes.JobCard
        vc.screenType = .ChangeCard
        
        vc.isReload = { isFromAddClick in
            
            let filter = self.arrCellData.filter { $0.cellType == .FareSummaryCardSelection}
            
            if filter.count > 0{
                
                if filter.first!.isSelected == false{
                    filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard()
                }else{
                    filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard)
                }
                
            } else {
                if isFromAddClick {
                    //Card
                    if let card = SyncManager.sharedInstance.getPrimaryCard() {
                        self.arrCellData.append(CellModel.getModel(placeholder: "", text: "", type: .CMICard, cellObj: card))
                    } else if let card = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard) {
                        self.arrCellData.append(CellModel.getModel(placeholder: "", text: "", type: .CMICard, cellObj: card, isSelected : true))
                    } else {
                        self.arrCellData.append(CellModel.getModel(placeholder: "", text: "", type: .CMIAddCard, cellObj: nil))
                    }
                }
            }
            self.tableView.reloadRows(at: [IndexPath(row: self.arrCellData.firstIndex(of: filter.first!) ?? 0, section: 0)], with: .none)
            
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func changeDefaultCards(_ value : Bool, index : Int){
        
        let filter = arrCellData.filter { $0.cellType == .FareSummaryCardSelection}
        
        if filter.count > 0{
            
            if value == false{
                filter.first!.isSelected = false
                filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard()
            }else{
                filter.first!.isSelected = true
                filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard)
            }
        }
        
        self.tableView.reloadData()
    }
}

//MARK: Call api
extension IssueWithLastTripVC {
    
    func callApiForPayment(){
        
        var param = [String:Any]()
        param["rideId"] = rideModel.id ?? ""
        
        let filter = self.arrCellData.filter { $0.cellType == .FareSummaryCardSelection}
        if filter.count > 0{
            param["cardId"] = (filter.first!.cellObj as! CardModel).id ?? ""
        }
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.MakePayment, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let _ = response as? [String:Any]{
                
                self.moveToRating()
                
            }
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
