//
//  AttachmentCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 13/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class AttachmentCell: UICollectionViewCell {
    
    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var imgPlus: UIImageView!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK: Set Data
    func setData(model:CellModel) {
        
        imgPlus.isHidden = model.isSelected
        imgView.isHidden = !model.isSelected
        lblName.text = model.placeholder
        
        if let img = model.cellObj as? UIImage {
            imgView.image = img
        }
        
    }
}
