//
//  AttachmentForStartRideVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 13/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class AttachmentForStartRideVC: UIViewController {
    
    //MARK: - Variables
    var rideId:String = ""
    var arrData = [CellModel]()
    var currentIndex : Int = -1
    var isRideStarted : (() -> ())?
    var attachmentReason:AttachmentReson = .StartRide
    
    //MARK: - Outlets
    @IBOutlet weak var btnSwipe: TGFlingActionButton!
    @IBOutlet weak var collectView: UICollectionView!
    @IBOutlet weak var btnStop: UIButton!
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }
    
    //MARK: Private Methods
    func initialConfig() {
        
        collectView.register(UINib(nibName: "AttachmentCell", bundle: nil), forCellWithReuseIdentifier: "AttachmentCell")
        
        if attachmentReason == .StartRide {
            btnSwipe.isHidden = false
            btnStop.isHidden = true
        }
        else if attachmentReason == .EndRide {
            btnStop.isHidden = false
            btnSwipe.isHidden = true
        }
        
        preparedataSource()
    }
    
    func preparedataSource() {
        arrData.removeAll()
        
        arrData.append(CellModel.getModel(placeholder: "KLblFront".localized, text: "", type: .UAFront, imageName: "", cellObj: nil))
        arrData.append(CellModel.getModel(placeholder: "KLblRear".localized, text: "", type: .UARear, imageName: "", cellObj: nil))
        arrData.append(CellModel.getModel(placeholder: "KLblLeft".localized, text: "", type: .UALeft, imageName: "", cellObj: nil))
        arrData.append(CellModel.getModel(placeholder: "KLblRight".localized, text: "", type: .UARight, imageName: "", cellObj: nil))
        
        collectView.reloadData()
    }
    
    //MARK:- IBActions
    @IBAction func btnSwipe_Callback(_ sender: TGFlingActionButton) {
        
        print(sender.swipe_direction)
        if sender.swipe_direction == .right {            
            //TO DO: Add the code for actions to be performed once the user swipe the button to right.
            btnSwipe.setTitle("KLblUnlock".localized, for: .normal)
            DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
                self.checkValidationAndCallAPI()
            }
        }
        if sender.swipe_direction == .left {
            //TO DO: Add the code for actions to be performed once the user swipe the button to left.
            btnSwipe.setTitle("KLblSlideToUnlock".localized, for: .normal)
        }
    }

    
    @IBAction func btnStop_Click(_ sender: UIButton) {
        self.checkValidationAndCallAPI()
    }
    
    
    @IBAction func btnClose_Click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func checkValidationAndCallAPI() {
     
        let arr = arrData.filter { Utilities.checkStringEmptyOrNil(str: $0.userText ?? "") == false }
//        if arr.count == arrData.count {
            
            let arrImages = arr.map { (obj) -> String in
                return obj.userText ?? ""
            }
            
            var req = [String:Any]()
            if arrImages.count > 0{
                req["attachments"] = arrImages
            }
            req["rideId"] = rideId
            
            if attachmentReason == .StartRide {
                callAPIForStartRide(req: req)
            }
            else if attachmentReason == .EndRide {
                callAPIForEndRide(req: req)
            }
//        }
//        else {
//            Utilities.showAlertView(message: "KLblPleaseUploadAllImageMsg".localized)
//            resetSwipeButton()
//        }
    }
    
    func resetSwipeButton() {
        btnSwipe.reset()
        btnSwipe.setTitle("KLblSlideToUnlock".localized, for: .normal)
    }
    
}

//MARK: - UICollectionView's DataSource & Delegate Methods
extension AttachmentForStartRideVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let model = arrData[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentCell", for: indexPath) as? AttachmentCell
        cell?.setData(model: model)
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let width = (collectView.bounds.width / 2) - 4
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = arrData[indexPath.row]
        if !model.isSelected {
            currentIndex = indexPath.row
            
            #if DEBUG
                Utilities.open_galley_or_camera(delegate: self)
            #else
                Utilities.openCamera(self)
            #endif
            
        }
    }
}


//MARK: -  UIImagePicker Delegate Methods
extension AttachmentForStartRideVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        if let pickedImage = info[.originalImage] as? UIImage {
            
            callAPIForUploadImage(image: pickedImage)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

extension AttachmentForStartRideVC {
    
    func callAPIForUploadImage(image:UIImage){
        
       UploadManager.sharedInstance.uploadFile(AppConstants.serverURL, command: AppConstants.URL.UploadFile, image: image, folderKeyvalue: nil, uploadParamKey: "file", params: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, isPregress:true, success: { (response, message ) in
            
            if let dictResponse = response as? [String:Any] {
                if let dict = dictResponse["data"] as? [String:Any]{
                    
                    if let arrFiles = dict["files"] as? [[String:Any]] {
                        if let path = arrFiles.first?["absolutePath"] as? String {
                            let model = self.arrData[self.currentIndex]
                            model.cellObj = image
                            model.userText = path
                            model.isSelected = true
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                                self.collectView.reloadData()
                            })
                        }
                    }
                }
            }
            
        }) { (failureMessage) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func callAPIForStartRide(req:[String:Any]) {
        
        SyncManager.sharedInstance.startRide(reqDict: req, success: { (response) in
            // success
            self.isRideStarted?()
            
            let vc = HomeVC()
            self.navigationController?.popAllAndSwitch(to: vc)
            
        }) {
            // failed
            self.resetSwipeButton()
        }
        
    }
    
    func callAPIForEndRide(req:[String:Any]) {
        
        SyncManager.sharedInstance.stopRide(reqDict: req, { (response) in
            // success code
            let model = Mapper<RideListModel>().map(JSON: response)!
            SyncManager.sharedInstance.activeRideObject = model
            
            let vc = FareSummaryVC(nibName: "FareSummaryVC", bundle: nil)
            vc.rideModel = model
            self.navigationController?.popAllAndSwitch(to: vc)
        }) {
            // failure copde
        }
        
    }
}
