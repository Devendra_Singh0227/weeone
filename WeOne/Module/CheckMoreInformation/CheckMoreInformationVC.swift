//
//  CheckMoreInformationVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 10/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class CheckMoreInformationVC: UIViewController {
    
    //MARK: - Variables
    var arrData = [CellModel]()
    var vehicleModel : VehicleModel?
    var vehicleDetail:VehicleDetailModel = VehicleDetailModel()
    var rideModel:RideListModel = RideListModel()
    
    // start and end date used for ride type differenbtiation and for reservation purpose
    var startDate:Date?
    var endDate:Date?
    var rideType = AppConstants.RideType.Instant
    var isFromCarMap : Bool = false
    var strVehicleId : Int = 0
    var isExpand : Bool = false
    var isInsurance : Bool = false
    var strRefuellingRules = ""
    var profileType = ProfileType.Personal

    //MARK: - Outlets
    @IBOutlet weak var vwbg: UIView!
    @IBOutlet var vwFooter: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableBottom: NSLayoutConstraint!
    @IBOutlet weak var btnBookNow: UIButtonCommon!
    
    var arrcarFeatures = [CellModel]()
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initialConfig()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "updateData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateData(_:)), name: Notification.Name(rawValue: "updateData"), object: nil)
    }
    
    //MARK: Private Methods
    func initialConfig() {
        
//        tableView.tableFooterView = vwFooter
        
        tableView.register(UINib(nibName: "MoreInformationFeatureCollCell", bundle: nil), forCellReuseIdentifier: "MoreInformationFeatureCollCell")
        tableView.register(UINib(nibName: "MoreInfoCell", bundle: nil), forCellReuseIdentifier: "MoreInfoCell")
        tableView.register(UINib(nibName: "MoreInformationTitleDescCell", bundle: nil), forCellReuseIdentifier: "MoreInformationTitleDescCell")
        tableView.register(UINib(nibName: "MoreInformationDeliverTimeCell", bundle: nil), forCellReuseIdentifier: "MoreInformationDeliverTimeCell")
        tableView.register(UINib(nibName: "MoreInformationPrice", bundle: nil), forCellReuseIdentifier: "MoreInformationPrice")
        
        tableView.register(UINib(nibName: "MoreInformationEmptySpaceCell", bundle: nil), forCellReuseIdentifier: "MoreInformationEmptySpaceCell")
        
        tableView.register(UINib(nibName: "MoreInformationInsuranceCell", bundle: nil), forCellReuseIdentifier: "MoreInformationInsuranceCell")
        
        tableView.register(UINib(nibName: "MoreInformationCancellationCell", bundle: nil), forCellReuseIdentifier: "MoreInformationCancellationCell")
        
        tableView.register(UINib(nibName: "MoreInformationCardCell", bundle: nil), forCellReuseIdentifier: "MoreInformationCardCell")
        tableView.register(UINib(nibName: "MoreInformationAddCardCell", bundle: nil), forCellReuseIdentifier: "MoreInformationAddCardCell")
        tableView.register(UINib(nibName: "DetailKeyValueTextCell", bundle: nil), forCellReuseIdentifier: "DetailKeyValueTextCell")
        tableView.register(UINib(nibName: "CancellationCell", bundle: nil), forCellReuseIdentifier: "CancellationCell")
        tableView.register(UINib(nibName: "MoreInformationNotIncludeCell", bundle: nil), forCellReuseIdentifier: "MoreInformationNotIncludeCell")
        tableView.register(UINib(nibName: "MoreInformationReduceDeductibleCell", bundle: nil), forCellReuseIdentifier: "MoreInformationReduceDeductibleCell")
        
        
        if rideType == AppConstants.RideType.Schedule {
            btnBookNow.setText(text: "KLblBook".localized)
        } else {
            btnBookNow.setText(text: "kLblRESERVE".localized)
        }

        btnBookNow.click = {
            self.btnBookNow_Clicked()
        }
        
        if isFromCarMap{
            
            callApiForCarDetail({
                self.prepareDataSource()
            })
        }else{
            prepareDataSource()
        }
        
        prepareTitle()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)

        
    }
    
    func prepareTitle() {
              
        lblTitle.text = "\(vehicleModel?.getCarName ?? "") | \(vehicleModel?.numberPlate ?? "")"

//        lblTitle.changeStringColor(string: lblTitle.text ?? "", array: [(vehicleModel?.numberPlate ?? "")], colorArray: [ColorConstants.TextColorWhitePrimary],changeFontOfString: [(vehicleModel?.numberPlate ?? "")], font : [FontScheme.kMediumFont(size: 20)])
        
    }
   
    func prepareDataSource() {
        
        arrData.removeAll()
        
        if let arr = vehicleModel?.getCarProperties(isLoadAll: true) {
            arrcarFeatures = arr
        }
        
        if arrcarFeatures.count > 0 {
            arrData.append(CellModel.getModel(type: .CMIFeature))
        }
        
        
        arrData.append(CellModel.getModel(placeholder: "More Information", type: .CMIMoreInfo))
        
        // Other Description
        if let arrOtherDesc = vehicleDetail.vehicle?.otherDescriptions, arrOtherDesc.count > 0 {
            for obj in arrOtherDesc {
                
                let master = SyncManager.sharedInstance.fetchMasterWithId(obj.id ?? "")
                
                if master.count > 0 {
                    
                    var isLast = false
                    
                    if obj.id == arrOtherDesc.last?.id {
                        isLast = true
                    }
                                        
                    arrData.append(CellModel.getModel(placeholder: master.first!.name, text: obj.desc,  type: .CMITitleDesc, imageName: master.first!.image ?? "", cellObj: obj, isLast: isLast))
                    
                    if master.first!.code?.lowercased() == AppConstants.MasterCode.Fuel{
                        
                        let strDiscount = String(format : "KLblFuelDiscount".localized, Utilities.convertToCurrency(number: vehicleDetail.vehicle?.fuelDiscount ?? 0,  currencyCode: vehicleDetail.vehicle?.currency))
                        let discount = "<p><span style=\"font-size: 16px;\">\(strDiscount)</span></p>"
                        let strExtraCharge = String(format : "KLblFuelExtraCharge".localized, Utilities.convertToCurrency(number: vehicleDetail.vehicle?.fuelDiscount ?? 0, currencyCode: vehicleDetail.vehicle?.currency))
                        let extraCharge = "<p><span style=\"font-size: 16px;\">\(strExtraCharge)</span></p>"
                        
                        strRefuellingRules = "\(discount) \n \(extraCharge)"
                        arrData.append(CellModel.getModel(placeholder: "KLblRefuellingRules".localized, text: "\(discount) \n \(extraCharge)",  type: .CMITitleDesc, cellObj: obj, isSelected: true))
                    }
                }
            }
        }
        
        
        //Order deductible insurance
        arrData.append(CellModel.getModel(placeholder: "", text: "", type: .CMIInsurance, cellObj: vehicleDetail, isSelected : vehicleModel?.isInsurance ?? false))
        
        // Latest Deliver DateTime
        if let nextRideStartDate = vehicleDetail.nextBookingDetail?.reservedDateTime,nextRideStartDate.count > 0, rideType == AppConstants.RideType.Instant {
            
            let nextDate = DateUtilities.convertDateFromStringWithFromat(dateStr: nextRideStartDate, format: DateUtilities.DateFormates.kMainSourceFormat)
            
            let strDt = DateUtilities.convertStringFromDate(date: nextDate, format: DateUtilities.DateFormates.kCarMoreInfoDate)
            let strTime = DateUtilities.convertStringFromDate(date: nextDate, format: DateUtilities.DateFormates.kCarMoreInfoTime)
            arrData.append(CellModel.getModel(placeholder: strDt, text: strTime, type: .CMILatestDeliver, cellObj: nil))
        }
        else if rideType == AppConstants.RideType.Schedule, let startDate = startDate, let endDate = endDate {
            
            let strDt = DateUtilities.convertStringFromDate(date: startDate, format: DateUtilities.DateFormates.kRegularDate)
            let endDt = DateUtilities.convertStringFromDate(date: endDate, format: DateUtilities.DateFormates.kRegularDate)

            let strTime = DateUtilities.convertStringFromDate(date: startDate, format: DateUtilities.DateFormates.kScheduleTime)
            let endTime = DateUtilities.convertStringFromDate(date: endDate, format: DateUtilities.DateFormates.kScheduleTime)

            arrData.append(CellModel.getModel(placeholder: strDt, placeholder2: endDt, text: strTime, userText1: endTime, type: .CMILatestDeliver, cellObj: nil))
        } else {
            let date = Date().addHours(AppConstants.NextBookingTotalHours)
            let strDt = DateUtilities.convertStringFromDate(date: date, format: DateUtilities.DateFormates.kCarMoreInfoDate)
            let strTime = DateUtilities.convertStringFromDate(date: date, format: DateUtilities.DateFormates.kCarMoreInfoTime)
            
            arrData.append(CellModel.getModel(placeholder: strDt, text: strTime, type: .CMILatestDeliver, cellObj: nil))
        }
        
        //Packages
        if let package = vehicleDetail.vehicle?.packages, package.count > 0 {
                        
            arrData.append(CellModel.getModel(placeholder: "KLblPrice", text: "", type: .CMIPackage, dataArr : package))
            
            preparePriceData()

        }
        
        //Prepare Price Data
        
        arrData.append(CellModel.getModel(placeholder: "", text: "", type: .CMIReducedDeductible, cellObj: vehicleDetail, isSelected : vehicleModel?.isInsurance ?? false))
        
        if rideType == AppConstants.RideType.Schedule {
            
            //CMIPackageTotal
            let packageRate = CellModel.getModel(placeholder: "KLblTotal".localized, text: "\(Utilities.convertToCurrency(number: vehicleDetail.pricing?.totalRate ?? 0.0, currencyCode: rideModel.pricingConfigData?.currency))", type: .CMIPackageTotal, cellObj: nil)

            if let index = self.getCellIndex(.CMIPackageTotal){
                arrData.remove(at: index)
                arrData.insert(packageRate, at: index)
            } else {
                arrData.append(packageRate)
            }
        }
        // This is not included
        arrData.append(CellModel.getModel(type: .CMINotIncluded, cellObj: vehicleModel))
        
        //Card
        if let card = SyncManager.sharedInstance.getPrimaryCard() {
            self.arrData.append(CellModel.getModel(placeholder: "", text: "", type: .CMICard, cellObj: card))
        } else if let card = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard) {
            self.arrData.append(CellModel.getModel(placeholder: "", text: "", type: .CMICard, cellObj: card, isSelected : true))
        }
        
        arrData.append(CellModel.getModel(type: .CMICancellationInfo))
        
        tableView.reloadData()

    }
    
    
    
    //PreparePrice Data
    func preparePriceData() {

        //Schedule
        if rideType == AppConstants.RideType.Schedule {
            
            let hourlyPackage  = vehicleDetail.vehicle?.packages.filter{ $0.hours == 1}
            let dailyPackage  = vehicleDetail.vehicle?.packages.filter{ $0.hours == 24}
            
            if dailyPackage != nil && dailyPackage?.count ?? 0 > 0 {
                
                let day = Utilities.getDayAndHours(startDate: startDate, endDate: endDate, isDay: true)
                
                let startDateTime = CellModel.getModel(placeholder: "\("KLblDayPrice".localized) (\(day ?? "") - \("KLblinclude".localized) \(dailyPackage?.first?.additionalInfo?.distanceCap ?? 0) Km/Day)", text: "\(Utilities.convertToCurrency(number: dailyPackage?.first?.calculatedRate ?? 0.0 , currencyCode: dailyPackage?.first?.currency))", type: .CMIStartTime, cellObj: nil)
                if let index = self.getCellIndex(.CMIStartTime){
                    
                    arrData.remove(at: index)
                    arrData.insert(startDateTime, at: index)
                } else{
                    arrData.append(startDateTime)
                }
            }
            
            if hourlyPackage != nil && hourlyPackage?.count ?? 0 > 0 {
                
                let hour = Utilities.getDayAndHours(startDate: startDate, endDate: endDate, isDay: false)
                
                let endDateTime = CellModel.getModel(placeholder: "\("KLblHourPrice".localized) (\(hour ?? ""))", text: "\(Utilities.convertToCurrency(number: hourlyPackage?.first?.calculatedRate ?? 0 , currencyCode: rideModel.pricingConfigData?.currency))", type: .CMIEndTime, cellObj: nil)
                
                if let index = self.getCellIndex(.CMIEndTime){
                    
                    arrData.remove(at: index)
                    arrData.insert(endDateTime, at: index)
                } else{
                    arrData.append(endDateTime)
                }
            }
        }
        
        tableView.reloadData()
    }
    
    @objc func updateData(_ notification: Notification) {
        self.prepareDataSource()
        
//        if self.navigationController != nil {
//            for viewController in (self.navigationController?.viewControllers)!{
//                if !viewController.isKind(of: BookingconfirmationPopupVC.self) {
//                    BookingConfirmationPopup()
//                }
//            }
//        }
    }

    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        Utilities.dismissToolTip(views: appDelegate.window!.subviews)

    }

    //MARK:- IBActions

    @IBAction func btnDismiss_Click(_ sender: UIButton) {

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        Utilities.dismissToolTip(views: appDelegate.window!.subviews)

        self.dismiss(animated: true, completion: nil)
    }
        
    func btnBookNow_Clicked() {
        AppNavigation.shared.currentStateList = AppNavigation.shared.config.accessConfig.requiredForReservation
        if AppNavigation.shared.checkUserHasAccess(vc: self) {
            BookingConfirmationPopup()
        }
    }
    
    func BookingConfirmationPopup() {
        
        var strDeposite = String(format : "\(Utilities.convertToCurrency(number: vehicleModel?.deposit ?? 0.0, currencyCode: vehicleModel?.currency))")
        
        strDeposite = "\(Utilities.convertToCurrency(number: vehicleModel?.deposit ?? 0.0, currencyCode: vehicleModel?.currency))"
        
        let vc = BookingconfirmationPopupVC(nibName: "BookingconfirmationPopupVC", bundle: nil)
        
        vc.headerTitle = "KBookingConfirmation".localized
        vc.desc1 = "You now enter into a binding agreement with Weone about vehicle rental."
        vc.desc2 = "We reserve \(strDeposite) on your card."
        vc.price = strDeposite
        
        if rideModel.vehicleId?.iotProvider == AppConstants.IOTProvider.WithTelemetric {
            vc.keyType = "KAutomaticKey".localized
            vc.imgKey = #imageLiteral(resourceName: "iphone")
        } else {
            vc.keyType = "KManualKey".localized
            vc.imgKey = #imageLiteral(resourceName: "car-key")            
        }
        
        vc.confirmClicked = {
            self.bookNow()
        }
        
        vc.cancelBtnClicked = {
            //            self.dismiss(animated: true, completion: nil)
        }
        
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        
        if UIViewController.current()?.presentedViewController != nil {
            UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
            })
        }
        else{
            UIViewController.current()?.present(vc, animated: true, completion: {
            })
        }
    }
    
    func bookNow() {
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0)
        SyncManager.sharedInstance.syncMaster ({
            
            if AppNavigation.shared.isValidToReservation(){
                
                var selected = self.vehicleDetail.vehicle?.packages.filter{$0.isSelected}

                if self.rideType == AppConstants.RideType.Instant {
                    selected = self.vehicleDetail.vehicle?.packages
                }
                
                if selected?.count ?? 0 > 0 {
                    //                    self.openImplementBookingPopup(priceConfigModel: selected.first!)
                    self.callApiForReserverRide()
                } else{
                    Utilities.showAlertView(message: "KLblPleaseSelectPackageMsg".localized)
                }
            }
            else{
                self.dismiss(animated: true, completion: nil)
            }
        })
    }
}

//MARK: - UITableView's DataSource & Delegate Methods
extension CheckMoreInformationVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let model = arrData[indexPath.row]
        if model.cellType == .CMITitleDesc && !isExpand{
            return 0
        } else if model.cellType == . CMIReducedDeductible && !isInsurance{
            return 0
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = arrData[indexPath.row]
        
        switch model.cellType! {
            
        case .CMIFeature:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInformationFeatureCollCell") as? MoreInformationFeatureCollCell
            cell?.setCellData(arrcarFeatures)
            return cell!
            
        case .CMIMoreInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInfoCell") as? MoreInfoCell
            cell?.setData(model: model, isExpand: isExpand)
            return cell!
            
        case .CMITitleDesc:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInformationTitleDescCell") as? MoreInformationTitleDescCell
            cell?.btnUpArrow.addTarget(self, action: #selector(btnUpArrow_Click), for: .touchUpInside)

            cell?.setData(model: model, isExpand: isExpand)
            /*cell?.reloadTable = {
             self.tableView.reloadData()
             } */
            return cell!
            
        case .CMILatestDeliver:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInformationDeliverTimeCell") as? MoreInformationDeliverTimeCell
            cell?.setData(model: model, rideType: self.rideType)
            return cell!
            
        case .CMIPackage, .CMIPackageTotal:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInformationPrice") as! MoreInformationPrice
            
            if model.cellType == .CMIPackageTotal {
                var total = vehicleDetail.pricing?.totalRate
                if isInsurance {
                    total = (vehicleDetail.pricing?.totalRate ?? 0.0) + (vehicleDetail.pricing?.insuranceAmount ?? 0.0)
                }
                
                model.userText = "\(Utilities.convertToCurrency(number: total ?? 0.0, currencyCode: rideModel.pricingConfigData?.currency))"
            }

            cell.setData(model: model, rideType: rideType)
            /* cell.packageSelect = {
             self.getFareSummary()
             } */
            return cell
            
        case .CMIEmptySpace:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInformationEmptySpaceCell") as? MoreInformationEmptySpaceCell
            cell?.setData(model: model)
            return cell!
            
        case .CMIInsurance:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInformationInsuranceCell") as? MoreInformationInsuranceCell
            // btnShowInsurancePolicy
            cell?.btnShowInsurancePolicy.addTarget(self, action: #selector(btnInsurancePolicy), for: .touchUpInside)
            
            cell?.setData(model: model)
            cell?.reloadTable = {
                self.isInsurance = model.isSelected2
                self.tableView.reloadData()
            }
          
            return cell!
            
        case .CMICard:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInformationCardCell") as? MoreInformationCardCell
            cell?.setData(model: model)
            cell?.btnChange.addTarget(self, action: #selector(btnChange_Click(_:)), for: .touchUpInside)
            cell?.btnChange.tag = indexPath.row
            cell?.switchChanged = { value in
                //profileType = value
                if value {
                    self.profileType = .Business
                } else {
                    self.profileType = .Personal
                }
                self.changeDefaultCards(value, index : indexPath.row)
            }
            return cell!
            
        case .CMIReducedDeductible:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInformationReduceDeductibleCell") as! MoreInformationReduceDeductibleCell
            cell.setData(model: model, isInsurance: isInsurance, rideType: self.rideType)
            return cell
            
        case .CMINotIncluded:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInformationNotIncludeCell") as! MoreInformationNotIncludeCell
            cell.setData(model: model)
            cell.btnInfo.addTarget(self, action: #selector(btnNotIncludedInfo_Click), for: .touchUpInside)
            return cell
            
            
        case .CMIAddCard:
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInformationAddCardCell") as? MoreInformationAddCardCell
            cell?.setData(model: model)
            cell?.onClickAddNewCard = {
                self.openAddCardScreen()
            }
            return cell!
            
        case .CMIDepositeAmount, .CMIInsuranceAmount , .CMIPackageAmount, .CMITotalAmount, .CMIStartTime, .CMIPackageRate, .CMIEndTime:

            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailKeyValueTextCell") as? DetailKeyValueTextCell
            cell?.setData(model: model)
            cell?.leadingLblKey.constant = 20
            cell?.trailingValue.constant = 20
            return cell!
            
        case .CMICancellationInfo:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreInformationCancellationCell") as! MoreInformationCancellationCell
            cell.btnInfo.addTarget(self, action: #selector(btnCancellationInfo_Click), for: .touchUpInside)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = arrData[indexPath.row]
        
        DispatchQueue.main.async {
            
            if model.cellType == .CMIMoreInfo {
                self.isExpand = !self.isExpand
                
                if self.isExpand {
                    if true{
                        let filter = self.arrData.filter { $0.cellType == .CMITitleDesc && $0.isSelected }
                        
                        if filter.count > 0{
                            filter.first!.isSelected2 = false
                        }
                    }
                }
                
                tableView.reloadData()
                
              //  tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
    }
    
    @objc func btnChange_Click(_ sender: UIButton) {
        
        if ApplicationData.isUserLoggedIn {
            
            let vc = DefaultPaymentVC.init(nibName: "DefaultPaymentVC", bundle: nil)
            vc.isFromReservation = true 
            vc.profileType = self.profileType
            vc.addClick = { isFromAddClick in
                let filter = self.arrData.filter { $0.cellType == .CMICard}
                
                if filter.count > 0 {
                    
                    if SyncManager.sharedInstance.getPrimaryCard() != nil && SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard) != nil{
                        
                        if filter.first!.isSelected == false{
                            filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard()
                        }else{
                            filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard)
                        }
                        
                    } else if SyncManager.sharedInstance.getPrimaryCard() != nil {
                        filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard()
                    } else {
                        filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard)
                    }
                } else {
                    if isFromAddClick {
                        //Card
                        if let card = SyncManager.sharedInstance.getPrimaryCard() {
                            self.arrData.append(CellModel.getModel(placeholder: "", text: "", type: .CMICard, cellObj: card))
                        } else if let card = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard) {
                            self.arrData.append(CellModel.getModel(placeholder: "", text: "", type: .CMICard, cellObj: card, isSelected : true))
                        } else {
                            self.arrData.append(CellModel.getModel(placeholder: "", text: "", type: .CMIAddCard, cellObj: nil))
                        }
                    }
                }
                
                self.tableView.reloadData()

            }
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
                        
            self.tableView.reloadData()
            
            
        } else {
            
        }
    }
    
    func openAddCardScreen(){
        if ApplicationData.isUserLoggedIn {
            
//            let vc = PaymentContainerVC.init(nibName: "PaymentContainerVC", bundle: nil)
//            vc.addClick = { isFromAddClick in
//                self.prepareDataSource()
//            }
//            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
//        }
//        else{
//            self.dismiss(animated: true) {
//                _ = AppNavigation.shared.isValidToReservation()
//            }
        }
    }
    
    @objc func btnCancellationInfo_Click(_ sender: UIButton) {
        
        let vc = CancellationInformationVC()
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .fullScreen

        self.present(nav, animated: true, completion: nil)
    }
    
    @objc func btnNotIncludedInfo_Click(_ sender: UIButton) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        Utilities.dismissToolTip(views: appDelegate.window!.subviews)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            Utilities.showPopOverView(str: self.strRefuellingRules, arrowSize: CGSize(width: 11, height: 11), maxWidth: 293, x: 10, isAttributed: true, sender: sender)
        })
        
    }
    
    @objc func btnUpArrow_Click(_ sender: UIButton) {
        self.isExpand = false
        tableView.reloadData()
        
    }

    @objc func btnInsurancePolicy(_ sender: UIButton) {
        Utilities.openStaticPage(title: AppConstants.StaticCode.InsuranceTerms, url: AppConstants.URL.StaticPage)
    }
    
    func changeDefaultCards(_ value : Bool, index : Int){
        
        let filter = arrData.filter { $0.cellType == .CMICard}
        
        if filter.count > 0{
            
            if value == false{
                filter.first!.isSelected = false
                filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard()
            }else{
                filter.first!.isSelected = true
                filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard)
            }
        }
        self.tableView.reloadData()
        //        self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
    }
}


//MARK: Call api for reserve ride
extension CheckMoreInformationVC{
    
    func callApiForReserverRide(){
        
        var param : [String : Any] = [:]
        param["isAutoDeduct"] = AppConfigurationModel.shared.configs.isAutoDeduct
        param["vehicleId"] = vehicleDetail.vehicle?.id ?? ""
        param["rideType"] = self.rideType
        
        let filter = arrData.filter { $0.cellType == .CMICard}
        if filter.count > 0{
            param["cardId"] = (filter.first!.cellObj as! CardModel ).id ?? ""
        }
        
        if startDate != nil || endDate != nil {
            
            if let startDT = startDate {
                if Date() > startDT{
                    param["startDateTime"] = DateUtilities.convertToISOFormat(dateStr: Date())
                }else{
                    param["startDateTime"] = DateUtilities.convertToISOFormat(dateStr: startDT)
                }
            }
            
            if let endDT = endDate {
                param["endDateTime"] = DateUtilities.convertToISOFormat(dateStr: endDT)
            }
        }
        else {
            param["startDateTime"] = DateUtilities.convertToISOFormat(dateStr: Date())
        }
        

        param["pricingConfigData"] =  vehicleDetail.vehicle?.packages.filter{$0.isSelected}.first?.getDict()

        if self.rideType == AppConstants.RideType.Instant {
            param["pricingConfigData"] = vehicleDetail.vehicle?.packages.first?.getDict()
        }
        
        if (arrData.filter { $0.cellType == .CMIInsurance }.first?.isSelected == true){
            param["isInsurance"] = isInsurance
        }
        
        if vehicleModel?.nextBookingDetail != nil{
            param["estimatedReturnTime"] = vehicleModel?.nextBookingDetail?.reservedDateTime
        }
        
        SyncManager.sharedInstance.callApiForReserverRide(param) {
            self.dismiss(animated: true) {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.isShowKeyHandaling = true
                AppNavigation.shared.moveToHome()
            }
        }
    }
    
    func callApiForCarDetail(_ completed : (() -> ())?){
        
        var param : [String : Any] = [:]
        param["vehicleId"] = strVehicleId
        param["rideType"] = self.rideType
        
        if startDate != nil || endDate != nil {
            
            if let startDT = startDate {
                if Date() > startDT{
                    param["reservedDateTime"] = DateUtilities.convertToISOFormat(dateStr: Date())
                }else{
                    param["reservedDateTime"] = DateUtilities.convertToISOFormat(dateStr: startDT)
                }
            }
            
            if let endDT = endDate {
                param["reservedEndDateTime"] = DateUtilities.convertToISOFormat(dateStr: endDT)
            }
        }
        else {
            param["reservedDateTime"] = DateUtilities.convertToISOFormat(dateStr: Date())
        }
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.GetVehicleDetail, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any] {
                self.vehicleDetail = Mapper<VehicleDetailModel>().map(JSON: dictResponse)!
            }
            
            completed?()
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    //FindCell Index
    func getCellIndex(_ type : CellType) -> Int? {
        
        let index = arrData.firstIndex{$0.cellType == type}
        return index
    }
    

}
