//
//  MoreInformationNotIncludeCell.swift
//  WeOne
//
//  Created by iMac on 24/03/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class MoreInformationReduceDeductibleCell: UITableViewCell {

    @IBOutlet weak var lblTopLine: UILabel!

    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPerDay: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(model:CellModel, isInsurance: Bool, rideType: Int) {
        
        lblTopLine.isHidden = true
        
        
        if isInsurance {
            lblPrice.isHidden = false

            if let detailModel = model.cellObj as? VehicleDetailModel {
                if rideType == AppConstants.RideType.Schedule {
                    lblTopLine.isHidden = false
                    lblPerDay.text =  "\(Utilities.convertToCurrency(number: detailModel.pricing?.insuranceAmount ?? 0, currencyCode: detailModel.vehicle?.currency)) \("KLblPerDay".localized)"
                    
                }
                lblPrice.text = String(format : Utilities.convertToCurrency(number: detailModel.vehicle?.insurance?.insuranceAmount ?? 0, currencyCode: detailModel.vehicle?.currency))
            }
        } else {
            lblPrice.isHidden = true
        }
    }
    
    
}
