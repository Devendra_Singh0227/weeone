//
//  MoreInformationNotIncludeCell.swift
//  WeOne
//
//  Created by iMac on 24/03/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class MoreInformationNotIncludeCell: UITableViewCell {

    @IBOutlet weak var btnInfo: UIButton!
    
    @IBOutlet weak var lblFuel: UILabel!
    @IBOutlet weak var lblTollPasss: UILabel!

    @IBOutlet weak var lblFuelDot: UILabel!
    @IBOutlet weak var lblTollPasssDot: UILabel!
    @IBOutlet weak var lblOverRun: UILabel!
    @IBOutlet weak var lblDeposit: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnInfo.setImage(UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Information")), for: .normal)

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(model:CellModel) {
        if let vehicleModel = model.cellObj as? VehicleModel{
         
            var package  = vehicleModel.packages.filter{ $0.isSelected == true }.first

            if package == nil {
                package = vehicleModel.packages.first
            }
            
            if vehicleModel.packages.count > 0 {
                
                lblOverRun.text = String(format : "KLblOverRun".localized, "\(Utilities.convertToCurrency(number: package?.additionalInfo?.additionalDistanceRate ?? 0 , currencyCode: package?.currency))")
            }
            
            lblDeposit.text = String(format : "KLblDepositInfo".localized, "\(Utilities.convertToCurrency(number: vehicleModel.deposit, currencyCode: vehicleModel.currency))")

            if vehicleModel.fuelType != AppConstants.FuelType.Combustion  {
                lblTollPasss.isHidden = true
                lblTollPasssDot.isHidden = true
                btnInfo.isHidden = true
                
                lblFuel.text = "Toll passes"
                
            }
        }
    }

    
}
