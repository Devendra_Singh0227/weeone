//
//  CarPropertyCell.swift
//  WeOne
//
//  Created by iMac on 23/03/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class MoreInformationFeatureCell: UICollectionViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProperty: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setCellData(_ model : CellModel) {
                
        lblName.text = model.placeholder
        
        if (model.imageName?.contains("images/master"))!{
            imgProperty.setImageForURL(url: URL(string: "\(AppConstants.imageURL)\(model.imageName ?? "")"), placeHolder: nil)
        } else {
            imgProperty.image = UIImage(named : model.imageName ?? "")
        }
    
    }
}
