//
//  MoreInformationDeliverTimeCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 11/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class MoreInformationDeliverTimeCell: UITableViewCell {
    
    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var viewSchedule: UIView!
    @IBOutlet weak var viewInstant: UIView!

    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var lblFromTime: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
    @IBOutlet weak var viewDashedBorder: UIView!

    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel, rideType: Int) {
        
        if rideType == AppConstants.RideType.Schedule {

            viewInstant.isHidden = true
            viewSchedule.isHidden = false
            
            viewDashedBorder.addDashedBorderVertical(color: ColorConstants.ThemeColor,isVertical: false)
            
            lblFromDate.text = model.placeholder
            lblFromTime.text = model.userText
            
            
            lblEndDate.text = model.placeholder2
            lblEndTime.text = model.userText1
            
        } else {
            viewInstant.isHidden = false
            viewSchedule.isHidden = true 

            let time = String(format : "KLblDeliverTime".localized,"\(model.userText ?? "")")
            lblDate.text = String(format: "%@ %@",model.placeholder ?? "",time)
        }
        

    }
    
}
