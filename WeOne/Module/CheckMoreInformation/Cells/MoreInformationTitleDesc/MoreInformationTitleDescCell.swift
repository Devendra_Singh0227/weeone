//
//  MoreInformationTitleDescCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 10/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class MoreInformationTitleDescCell: UITableViewCell {
    
    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var viewDesc: UIView!
    @IBOutlet weak var viewbtnUpArrow: UIView!

//    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var imgImage: UIImageView!
    @IBOutlet weak var btnUpArrow: UIButton!

    var cellData : CellModel?
    var reloadTable:(()->())?
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
        
    //MARK: Set Data
    func setData(model:CellModel, isExpand: Bool) {
        
        
        cellData = model
        lblTitle.text = model.placeholder
//        btnDropDown.isSelected = model.isSelected
        
        btnUpArrow.setImage(UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "UpArrow")), for: .normal)

        if let url = URL(string : AppConstants.serverURL + (model.imageName ?? "")){
            imgImage.setImageForURL(url: url, placeHolder: UIImage(named : "noImage"))
        }else{
            imgImage.image = UIImage(named : "noImage")
        }
        
        if isExpand {
            
            viewDesc.isHidden = false
            lblTitle.isHidden = false
            imgImage?.isHidden = false
                        
            if model.isLast {
                viewbtnUpArrow.isHidden = false
            } else {
                viewbtnUpArrow.isHidden = true
            }
            
//            let str = model.userText?.replacingOccurrences(of: "\n", with: "", options: [.regularExpression])

            
//            let t =  model.userText?.replacingOccurrences(of: "</p>\n", with: "</p>", options: [.regularExpression])
//            let w = t?.replacingOccurrences(of: "</p>", with: "", options: [.regularExpression])
//            let e = w?.replacingOccurrences(of: "<p>", with: "\n", options: [.regularExpression])

//            print(t)
            
//            let data = (t ?? "").data(using: .utf8)!
//            let att = try! NSAttributedString.init(
//                data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue],
//                documentAttributes: nil)
//
//            let matt = NSMutableAttributedString(attributedString:att)
//
//            matt.enumerateAttribute(
//                NSAttributedString.Key.font,
//                in:NSMakeRange(0,matt.length),
//                options:.longestEffectiveRangeNotRequired) { value, range, stop in
//                    let f1 = value as! UIFont
//                    let f2 = FontScheme.kRegularFont(size: 12)
//                    if let f3 = applyTraitsFromFont(f1, to:f2) {
//                        matt.addAttribute(
//                            NSAttributedString.Key.font, value:f3, range:range)
//
//                        if model.isSelected2{
//                            matt.addAttribute(
//                            NSAttributedString.Key.foregroundColor, value:ColorConstants.ThemeColor, range:range)
//                            lblTitle.textColor = ColorConstants.ThemeColor
//                        }else{
//                            matt.addAttribute(
//                            NSAttributedString.Key.foregroundColor, value:ColorConstants.MoreInformtion.DescTextColor, range:range)
//                            lblTitle.textColor = ColorConstants.TextColorPrimary
//                        }
//                    }
//            }
            
            lblDesc.text = model.userText?.htmlToString
            
            if model.isSelected{
                imgImage?.isHidden = true
            }else{
                imgImage?.isHidden = false
            }
        } else{
            lblDesc.text = ""
            viewDesc.isHidden = true
            lblTitle.isHidden = true
            imgImage?.isHidden = true
            viewbtnUpArrow.isHidden = true 
        }
        
        
    }
    
    @IBAction func btnDropDown_Click(_ sender: UIButton) {
        
        cellData!.isSelected = !cellData!.isSelected
        reloadTable?()
    }
    
    func applyTraitsFromFont(_ f1: UIFont, to f2: UIFont) -> UIFont? {
        let t = f1.fontDescriptor.symbolicTraits
        if let fd = f2.fontDescriptor.withSymbolicTraits(t) {
            return UIFont.init(descriptor: fd, size: 0)
        }
        return nil
    }
}


