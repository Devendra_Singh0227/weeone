//
//  MoreInformationEmptySpaceCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 11/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class MoreInformationPrice: UITableViewCell {
    
    //MARK: - Variables
    var packageModel = PackagesModel()
    
    //MARK: - Outlets
    @IBOutlet weak var viewSchedule: UIView!
    @IBOutlet weak var lblDailyAmount: UILabel!
    @IBOutlet weak var lblHourlyAmount: UILabel!
    @IBOutlet weak var lblSeprator: UILabel!
    
    @IBOutlet weak var lblTopLine: UILabel!
    @IBOutlet weak var heightBottomLine: NSLayoutConstraint!
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnInfo: UIButton!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        
        btnInfo.setImage(UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Information")), for: .normal)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model: CellModel, rideType: Int) {
        
        lblTopLine.isHidden = true
        
        if model.cellType == CellType.CMIPackageTotal || model.cellType == CellType.IssueWithLastTripTotal {
            btnInfo.isHidden = true
            lblTitle.text = model.placeholder
            lblPrice.text = model.userText
//            lblPrice.font = FontScheme.kSemiBoldFont(size: 14)
            if model.cellType == CellType.IssueWithLastTripTotal {
                heightBottomLine.constant = 3
//                lblTitle.font = FontScheme.kSemiBoldFont(size: 14)
            } else {
                lblTopLine.isHidden = false
            }
            
        } else {
            let arrPackages = model.dataArr as! [PackagesModel]
            
            
            
            if rideType == AppConstants.RideType.Schedule {
                
                let hourlyPackage  = arrPackages.filter{ $0.hours == 1 && $0.isHide == false}.first
                let dailyPackage  = arrPackages.filter{ $0.hours == 24 && $0.isHide == false}.first
                
                btnInfo.isHidden = true
                viewSchedule.isHidden = false
                if hourlyPackage != nil && dailyPackage != nil{
                    
                    lblHourlyAmount.text = "\(Utilities.convertToCurrency(number: hourlyPackage?.rate ?? 0.0, currencyCode: hourlyPackage?.currency)) /hour"
                    lblSeprator.isHidden = false
                    lblDailyAmount.text = "\(Utilities.convertToCurrency(number: dailyPackage?.rate ?? 0, currencyCode: dailyPackage?.currency))\(dailyPackage?.getFormattedUnit ?? "") "
                    
                } else if dailyPackage != nil {
                    lblHourlyAmount.isHidden = true
                    lblSeprator.isHidden = true
                    lblDailyAmount.text = "\(Utilities.convertToCurrency(number: dailyPackage?.rate ?? 0, currencyCode: dailyPackage?.currency))\(dailyPackage?.getFormattedUnit ?? "") "

                } else if hourlyPackage != nil {
                    lblDailyAmount.isHidden = true
                    lblSeprator.isHidden = true
                    lblHourlyAmount.text = "\(Utilities.convertToCurrency(number: hourlyPackage?.rate ?? 0, currencyCode: hourlyPackage?.currency)) /hour"
                }
                
            } else {
                viewSchedule.isHidden = true

                if let package = arrPackages.first {
                    btnInfo.isHidden = false
                    
                    lblPrice.text = "\(Utilities.convertToCurrency(number: package.rate, currencyCode: package.currency))\(package.getFormattedUnit)"
                    
                    packageModel = package
                    
                }
                
            }
        }
        
    }
    
    @IBAction func btnInfo_Click(_ sender: UIButton) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        Utilities.dismissToolTip(views: appDelegate.window!.subviews)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            Utilities.showPopOverView(str: "\(self.packageModel.additionalInfo?.distanceCap ?? 0) \("KLblKmPerdayIncluded".localized)", arrowSize: CGSize(width: 9, height: 9), maxWidth: 150, x: 27, sender: sender)
        })
    }
}

