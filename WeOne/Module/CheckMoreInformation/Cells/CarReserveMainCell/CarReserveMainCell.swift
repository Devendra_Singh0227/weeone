//
//  CarReserveMainCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 03/10/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class CarReserveMainCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionLayout: UICollectionViewFlowLayout! {
        didSet {
            collectionLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        }
    }
    
    var arrPackages = [PackagesModel]()
    var packageSelect:(()->())?
    var rideType: Int?

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        collectionView.register(UINib(nibName: "MorePackageCollectionCell", bundle: nil), forCellWithReuseIdentifier: "MorePackageCollectionCell")
        collectionView.dataSource = self
        collectionView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ model: CellModel, rideType: Int){
        
        self.rideType = rideType

        arrPackages = model.dataArr as! [PackagesModel]
        collectionView.reloadData()
    }
    
}

//MARK: - UICollectionView's DataSource & Delegate Methods

extension CarReserveMainCell: UICollectionViewDelegate,UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrPackages.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MorePackageCollectionCell", for: indexPath) as! MorePackageCollectionCell
        cell.setCellData(model: arrPackages[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if rideType != AppConstants.RideType.Schedule {
            for item in arrPackages {
                item.isSelected = false
            }

            arrPackages[indexPath.row].isSelected = true
            self.collectionView.reloadData()
            packageSelect?()
        }
    }
}
