//
//  MorePackageCollectionCell.swift
//  WeOne
//
//  Created by Mayur on 23/01/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class MorePackageCollectionCell: UICollectionViewCell {
    
        @IBOutlet weak var viewMain: ShadowCard!
        @IBOutlet weak var lbltitle: UILabel!
//        @IBOutlet weak var lblDesc: UILabel!
        @IBOutlet weak var lblFare: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: leftAnchor),
            contentView.rightAnchor.constraint(equalTo: rightAnchor),
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }

    func setCellData(model : PackagesModel) {
        
        lbltitle.text = model.name
//        lblDesc.text = model.desc
        lblFare.text = "\(Utilities.convertToCurrency(number: model.rate, currencyCode: model.currency))/\(model.getFormattedUnit)"
            
        if model.isSelected{

            viewMain.mainBGColor = ColorConstants.TextColorTheme
            lbltitle.textColor = UIColor.white
//            lblDesc.textColor = UIColor.white
            lblFare.textColor = UIColor.white
        }else{

            viewMain.mainBGColor = UIColor.white
            lbltitle.textColor = ColorConstants.TextColorTheme
//            lblDesc.textColor = ColorConstants.TextColor
            lblFare.textColor = ColorConstants.TextColorTheme
        }

        viewMain.layoutSubviews()
    }
}
