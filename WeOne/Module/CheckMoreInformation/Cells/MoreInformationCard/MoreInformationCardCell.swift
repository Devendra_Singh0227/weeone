//
//  MoreInformationCardCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 11/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class MoreInformationCardCell: UITableViewCell {
    
    //MARK: - Variables
    var switchChanged : ((_ switch : Bool) -> ())?
    
    //MARK: - Outlets
    @IBOutlet weak var lblCardNo: UILabel!
    @IBOutlet weak var lblCardType: UILabel!
    
    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var viewSwitch: UIView!
    
    @IBOutlet weak var constraintviewSwitchWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintviewSwitchTrailing: NSLayoutConstraint!

    //   @IBOutlet weak var switchView: Switch!
    
    @IBOutlet weak var iconsSegmentedControl: CustomSegmentedControl! {
        didSet {
            
            //Set this booleans to adapt control
            iconsSegmentedControl.itemsWithText = false
            iconsSegmentedControl.fillEqually = false
            iconsSegmentedControl.roundedControl = true
            
            iconsSegmentedControl.setSegmentedWith(items: [#imageLiteral(resourceName: "User1@x"),#imageLiteral(resourceName: "work")])
            iconsSegmentedControl.padding = 2
            iconsSegmentedControl.thumbViewColor = ColorConstants.ThemeColor
            iconsSegmentedControl.buttonColorForNormal = ColorConstants.ThemeColor
            iconsSegmentedControl.buttonColorForSelected = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            iconsSegmentedControl.backgroundColor = .clear //ColorConstants.TextColorSecondary.withAlphaComponent(0.1)

            
        }
    }
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        //        switchView.thumbImage = #imageLiteral(resourceName: "user-30").cgImage
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        
        iconsSegmentedControl.isSelected = model.isSelected
        
        if model.isSelected {
            iconsSegmentedControl.moveThumbViewFillEquallyFalse(at: 1)
            lblCardType.text = "Job"
            
        } else {
            iconsSegmentedControl.moveThumbViewFillEquallyFalse(at: 0)
            lblCardType.text = "Personal"
        }

            constraintviewSwitchWidth.constant = 75
            constraintviewSwitchTrailing.constant = 9
            iconsSegmentedControl.isHidden = false

        
        if let obj = model.cellObj as? CardModel {
            lblCardNo.text = obj.getFormattedCardNumber()
            imgCard.image = UIImage(named: Utilities.getCreditCardImage(cardType: obj.brand ?? ""))
        }
    }
    
    
    
    @IBAction func btnSegment_Changed(_ sender: CustomSegmentedControl) {
        
        if iconsSegmentedControl.selectedSegmentIndex == 0 {
            if SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.PersonalCard) != nil {
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.switchChanged?(false)
                }
                lblCardType.text = "Job"
            } else {

                sender.resetButton(selectedIndex: 1)
                let vc = DefaultPaymentVC()
                vc.profileType = .Personal
                
                UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)

            }
        } else if iconsSegmentedControl.selectedSegmentIndex == 1 {
            if SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard) != nil {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    self.switchChanged?(true)
                }
                lblCardType.text = "Personal"
                
            } else {
                sender.resetButton(selectedIndex: 0)

                let vc = DefaultPaymentVC()
                vc.profileType = .Business
                UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
