//
//  MoreInformationAddCardCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 12/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class MoreInfoCell: UITableViewCell {
    
    //MARK: - Variables
    var onClickAddNewCard:(()->())?
    
    //MARK: - Outlets
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var btnMoreInformation: UIButton!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var lblBottomLine: UILabel!

    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model: CellModel, isExpand: Bool) {
        lblText.text = model.placeholder
        btnDropDown.isSelected = isExpand
        
        if model.cellType == .DNDamageReport {
            lblBottomLine.isHidden = false
        } else {
            lblBottomLine.isHidden = true
        }
        
    }

}
