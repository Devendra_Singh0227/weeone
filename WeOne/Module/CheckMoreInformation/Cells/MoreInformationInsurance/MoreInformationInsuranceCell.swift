//
//  MoreInformationInsuranceCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 11/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class MoreInformationInsuranceCell: UITableViewCell {
    
    //MARK: - Variables
    var cellModel:CellModel?
    
    //MARK: - Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var viewDesc: UIView!
    @IBOutlet weak var btnShowInsurancePolicy: UIButton!
    @IBOutlet weak var btnSwitch: UIButton!
    
     var reloadTable:(()->())?
    var insuranceSelect:(()->())?
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        
//        let attributedTitle = NSAttributedString(string: "KLblSeeInsurancePolicy".localized, attributes: [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.font : FontScheme.kSemiBoldFont(size: 12.0), NSAttributedString.Key.foregroundColor: ColorConstants.TextColorTheme])
//        let attributedTitle = NSAttributedString(string: "KLblSeeInsurancePolicy".localized, attributes: [ NSAttributedString.Key.font : FontScheme.kSemiBoldFont(size: 12.0), NSAttributedString.Key.foregroundColor: ColorConstants.TextColorTheme])
//
//        btnShowInsurancePolicy.setAttributedTitle(attributedTitle, for: .normal)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        
        cellModel = model
        btnSwitch.isSelected = model.isSelected2
        
        if model.isSelected2 {
            
            if let detailModel = model.cellObj as? VehicleDetailModel{
                lblDesc.text = String(format : "KLblOrderInsuranceDesc".localized, Utilities.convertToCurrency(number: detailModel.vehicle?.insurance?.insuranceTo ?? 0, currencyCode: detailModel.vehicle?.currency),Utilities.convertToCurrency(number: detailModel.vehicle?.insurance?.insuranceFrom ?? 0, currencyCode: detailModel.vehicle?.currency),Utilities.convertToCurrency(number: detailModel.vehicle?.insurance?.insuranceAmount ?? 0, currencyCode: detailModel.vehicle?.currency))
            }
            else{
                lblDesc.text = ""
            }
            
            viewDesc.isHidden = false
        }
        else{
            viewDesc.isHidden = true
        }
    }
    
    @IBAction func btnSwitch_Click(_ sender: UIButton) {
        cellModel!.isSelected2 = !sender.isSelected
        reloadTable?()
    }
}
