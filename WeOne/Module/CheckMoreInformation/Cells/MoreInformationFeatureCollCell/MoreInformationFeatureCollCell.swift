//
//  MoreInformationFeatureCollCell.swift
//  WeOne
//
//  Created by iMac on 26/03/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class MoreInformationFeatureCollCell: UITableViewCell {

    @IBOutlet weak var collection_view: UICollectionView!
    @IBOutlet weak var constraintHeightCollView: NSLayoutConstraint!
    
    var arrcarFeatures = [CellModel]()

    override func awakeFromNib() {
        super.awakeFromNib()
        collection_view.register(UINib(nibName: "MoreInformationFeatureCell", bundle: nil), forCellWithReuseIdentifier: "MoreInformationFeatureCell")

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellData(_ arr : [CellModel]){
        arrcarFeatures = arr
        
        if (arrcarFeatures.count % 3) == 0 {
            constraintHeightCollView.constant = CGFloat(((arrcarFeatures.count / 3)*45))
        }else{
            constraintHeightCollView.constant = CGFloat((((arrcarFeatures.count / 3) + 1)*45))
        }
        collection_view.reloadData()
    }
}
//MARK: - UICollectionView's DataSource & Delegate Methods
extension MoreInformationFeatureCollCell: UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrcarFeatures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: (AppConstants.ScreenSize.SCREEN_WIDTH - 40) / 3, height: 45)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoreInformationFeatureCell", for: indexPath) as! MoreInformationFeatureCell
        cell.setCellData(arrcarFeatures[indexPath.row])
        return cell
    }
    
}
