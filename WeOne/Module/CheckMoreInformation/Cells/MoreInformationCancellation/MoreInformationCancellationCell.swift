//
//  MoreInformationCancellationCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 11/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class MoreInformationCancellationCell: UITableViewCell {
    
    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var btnInfo: UIButton!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        btnInfo.setImage(UIImage.init(named: ThemeManager.sharedInstance.getImage(string: "Information")), for: .normal)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
