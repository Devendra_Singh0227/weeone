//
//  MoreInformationAddCardCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 12/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class MoreInformationAddCardCell: UITableViewCell {
    
    //MARK: - Variables
    var onClickAddNewCard:(()->())?
    
    //MARK: - Outlets
    @IBOutlet weak var btnAddNewCard: UIButtonCommon!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        btnAddNewCard.click = {
            self.onClickAddNewCard?()
        }
        
    }
}
