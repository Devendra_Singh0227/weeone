//
//  CancellationCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 27/01/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class CancellationCell: UITableViewCell {

    @IBOutlet weak var lblCancellationPolicy: UILabel!
    @IBOutlet weak var btnCancellation: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
//        lblCancellationPolicy.changeStringColor(string: "KLblCancellationPolicy".localized, array: ["KLblCancellationPolicy".localized], colorArray: [ColorConstants.TextColorTheme], font : [FontScheme.kMediumFont(size: 16)])
//         
//        lblCancellationPolicy.changeStringUnderLine(attrStr: lblCancellationPolicy.attributedText?.mutableCopy() as! NSMutableAttributedString, array: ["KLblCancellationPolicy".localized], font: FontScheme.kLightFont(size: 16))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
