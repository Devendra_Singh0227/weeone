//
//  UITextFiledCommon.swift
//  SnapAcleaner
//
//  Created by Coruscate on 18/09/18.
//  Copyright © 2018 Coruscate. All rights reserved.
//

import UIKit

enum TextFieldType
{
    case defaultType //0
    case email //1
    case password // 2
    case mobile //3
    case clickable //4
}

@objc protocol TextFieldDelegate : AnyObject {
    @objc optional func textFieldValueChanged(textField:UITextField)
    func didBeginEditing(textField:UITextField)
    func didEndEditing(textField:UITextField)
    func shouldReturn(textField:UITextField)
}

class FloatingLabelTextField: SkyFloatingLabelTextField {

    @objc dynamic open var leftPadding = CGFloat(0)
    @objc dynamic open var rightPadding = CGFloat(0)

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {

        let rect = CGRect(
            x: leftPadding,
            y: titleHeight(),
            width: bounds.size.width - rightPadding,
            height: bounds.size.height - titleHeight() - selectedLineHeight
        )

        return rect

    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {

        let rect = CGRect(
            x: leftPadding,
            y: titleHeight(),
            width: bounds.size.width - rightPadding,
            height: bounds.size.height - titleHeight() - selectedLineHeight
        )

        return rect

    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {

        let rect = CGRect(
            x: leftPadding,
            y: titleHeight(),
            width: bounds.size.width - rightPadding,
            height: bounds.size.height - titleHeight() - selectedLineHeight
        )

        return rect

    }

    override func titleLabelRectForBounds(_ bounds: CGRect, editing: Bool) -> CGRect {

        if editing {
            return CGRect(x: 0, y: 0, width: bounds.size.width, height: titleHeight())
        }

        return CGRect(x: 0, y: 0, width: bounds.size.width, height: titleHeight())
    }

}

class UITextFiledCommon: UIView {
    
    @IBOutlet weak var contentView : UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var btnEye: UIButton!
    @IBOutlet weak var viewMain: UIView!
    
    @IBOutlet weak var widthIcon: NSLayoutConstraint!
    @IBOutlet weak var heightIcon: NSLayoutConstraint!
    @IBOutlet weak var leadingIcon: NSLayoutConstraint!
    @IBOutlet weak var leadingLine: NSLayoutConstraint!
    @IBOutlet weak var widthBtnEye: NSLayoutConstraint!
    @IBOutlet weak var tralingEyeBtn: NSLayoutConstraint!
    @IBOutlet weak var heightViewLine: NSLayoutConstraint!
    @IBOutlet weak var viewTop: NSLayoutConstraint!

    var textFiledType : TextFieldType = .defaultType
    
    var delegate : TextFieldDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    func commonInit()  {
    
        Bundle.main.loadNibNamed("UITextFiledCommon", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        self.textField.delegate = self
        self.textField.keyboardType = .default
//        self.textField.placeholderColor = ColorConstants.TextColorPrimary
//        self.textField.placeholderFont = FontScheme.kLightFont(size: 16)
//        self.textField.titleColor = ColorConstants.TextColorPrimary
//        self.textField.titleFont = FontScheme.kLightFont(size: 13)
//        self.textField.selectedTitleColor = ColorConstants.TextColorPrimary
        self.textField.tintColor = ColorConstants.ThemeColor
        self.viewLine.backgroundColor = ColorConstants.UnderlineColor
        self.isIcon = false
        
    }

    //Set TextFiled Type
    func setTextFiledProperty()  {
        
        if self.textFiledType == .email {
            self.textField.keyboardType = .emailAddress
        }
        else if self.textFiledType == .password {
            self.textField.isSecureTextEntry = true
        }
        else if self.textFiledType == .mobile {
            self.textField.keyboardType = .numberPad
        }else{
            self.textField.keyboardType = .default
            self.textField.isSecureTextEntry = false
        }
        
        self.manageEyeButton()
    }
    
    //Manage Password Show Button
    func manageEyeButton() {
        
        if self.textFiledType != .password {
            widthBtnEye.constant = 0
            tralingEyeBtn.constant = 0
        }else{
            widthBtnEye.constant = 30
            tralingEyeBtn.constant = 16
        }
    }
    
    //get Text
    func getText() -> String? {
    
        
        return textField.text
    }
    
    //Set Text
    func setText(text : String?)  {
        
        textField.text = text
    }
    
    //MARK: IBInspectable
    @IBInspectable
    var text: NSString {
        
        get {
            return  ""
        }
        set {
           textField.text = newValue as String
        }
    }
    
    @IBInspectable
    var placeholder: NSString {
        
        get {
            return  ""
        }
        set {
//            let attributedTitle = NSAttributedString(string: (newValue as String).localized, attributes: [NSAttributedString.Key.kern: 0.64, NSAttributedString.Key.foregroundColor : ColorConstants.TextColorPrimary, NSAttributedString.Key.font : FontScheme.kLightFont(size: 16.0)])
//            textField.attributedPlaceholder = attributedTitle
        }
    }
    
    @IBInspectable
    var placecolor: NSString {
        
        get {
            return  ""
        }
        set {
            
//            let attributedTitle = NSAttributedString(string: textField.placeholder != nil ? textField.placeholder! : "", attributes: [NSAttributedString.Key.kern: 1.2, NSAttributedString.Key.foregroundColor: ColorScheme.colorFromConstant(textColorConstant: newValue as String), NSAttributedString.Key.font : FontScheme.kLightFont(size: 16.0)])
//            textField.attributedPlaceholder = attributedTitle
            
        }
    }
    
    @IBInspectable
    var textcolor: NSString {
        
        get {
            return  ""
        }

        set {
            textField.textColor = ColorScheme.colorFromConstant(textColorConstant: newValue as String)
        }
    }
    
    @IBInspectable
    var textFieldBackGroundColor: UIColor = ColorConstants.bgColorSecondary{
        
        didSet{
            self.viewMain.backgroundColor = textFieldBackGroundColor
            self.viewLine.backgroundColor = ColorConstants.BorderColor
            
        }
        
    }
    
    @IBInspectable
    var icon: NSString {
        
        get {
            return  ""
        }
        
        set {
            imgView.image = UIImage(named: newValue as String)
        }
    }
    
    @IBInspectable
    var isSecureText: Bool = false {
        
        didSet{
            textField.isSecureTextEntry = isSecureText as Bool
        }
    }
    
    @IBInspectable
    var isIcon: Bool = true {
        didSet{
            
            if isIcon == false {
                widthIcon.constant = 0
                heightIcon.constant = 0
                leadingIcon.constant = 0
                leadingLine.constant = 0
//                viewLine.isHidden = true
            }
        }
    }
    
    @IBInspectable
    var showEyeButton: Bool = true {
        didSet{
            
            if showEyeButton == false {
                widthBtnEye.constant = 0
                tralingEyeBtn.constant = 0
            }
        }
    }
    
    @IBInspectable
    var type: Int = 0 {
        didSet{
            
            if type == 1 {
                self.textFiledType = .email
            }
            else if type == 2 {
                self.textFiledType = .password
            }
            else if type == 3 {
                self.textFiledType = .mobile
            }
            else if type == 4 {
                self.textFiledType = .clickable
            }
            else {
                self.textFiledType = .defaultType
            }
            self.setTextFiledProperty()
        }
    }

    //MARK: Action
    @IBAction func btnPasswordShow_Click(_ sender: UIButton) {
        
        if textField.isSecureTextEntry == true{
            sender.isSelected = true
            textField.isSecureTextEntry = false
        }else{
            sender.isSelected = false
            textField.isSecureTextEntry = true
        }
    }
    
    @IBAction func textFieldValueChanged(_ sender: UITextField) {
        self.delegate?.textFieldValueChanged?(textField: sender)
    }
}

//MARK: textField delegate methods
extension UITextFiledCommon : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }
        
        if self.textFiledType == .email || self.textFiledType == .password || self.textFiledType == .mobile {
            if string == " " { return false }
        }
        
        if range.location == 0{
            
            if string == " " {
                return false
            }
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.delegate?.shouldReturn(textField: textField)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        viewLine.backgroundColor = ColorConstants.UnderlineColor
        self.setText(text: textField.text)
        
        self.delegate?.didEndEditing(textField: textField)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        viewLine.backgroundColor = ColorConstants.ThemeColor

        if self.textFiledType == .clickable{
            self.delegate?.didBeginEditing(textField: textField)
            
        }
    }
}


