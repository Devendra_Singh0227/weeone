//
//  UIButtonCommon.swift
//  SnapAcleaner
//
//  Created by Coruscate on 19/09/18.
//  Copyright © 2018 Coruscate. All rights reserved.
//

import UIKit

class UIViewCommon: UIView {
    
    @IBOutlet weak var contentView : UIView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewGradient: GradientView!

    var click:(()->())?
    var isHideBackground = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    func commonInit()  {
        
        Bundle.main.loadNibNamed("UIViewCommon", owner: self, options: nil)
        if isHideBackground {
            viewGradient.backgroundColor = ColorConstants.TextColorWhitePrimary
        } else {
            viewGradient.backgroundColor = ColorConstants.ThemeColor
        }
        self.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
  
    }
    
    private func applyShadow() {

        let shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        contentView.layer.shadowColor = shadowColor.cgColor
        contentView.layer.shadowOpacity = 0.2
        contentView.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        contentView.layer.shadowRadius = 5
        
    }

    func setBackGroundColor(color : UIColor) {
        
        button.backgroundColor = color
    }
    
    @IBAction func button_Click(_ sender: UIButton) {
        if let cc = click {
            cc()
        }
    }
    
    
    //MARK: IBInspectable
    @IBInspectable
    var text: NSString {
        
        get {
            return  ""
        }
        set {
           // let attributedTitle = NSAttributedString(string: (newValue as String).localized, attributes: [NSAttributedString.Key.kern: 1.2, NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : FontScheme.kSemiBoldFont(size: 20.0)])
            //lblTitle.attributedText = attributedTitle
        }
    }
    
    //MARK: IBInspectable
    @IBInspectable
    var imageRight: NSString {
        
        get {
            return  ""
        }
        set {
            
            if newValue.length > 0 {
                button.setImage(UIImage(named: newValue as String), for: .normal)
                button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
            }
        }
    }
    
    
    @IBInspectable
    var isGradientbackground : Bool = false {
        didSet {
            let gradient = CAGradientLayer()
            gradient.frame = contentView.bounds
            gradient.colors = [ColorConstants.ThemeGradientStartColor, ColorConstants.ThemeGradientEndColor]
            
            if isGradientbackground == false{
                gradient.removeFromSuperlayer()
            }else{
                button.backgroundColor = UIColor.clear
                contentView.layer.insertSublayer(gradient, at: 0)
            }
        }
    }
    
    func setText(text : String) {
        
        //let attributedTitle = NSAttributedString(string: text, attributes: [NSAttributedString.Key.kern: 1.2, NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : FontScheme.kSemiBoldFont(size: 16.0)])
       // button.setAttributedTitle(attributedTitle, for: .normal)
    }
}
 
