//
//  UIViewCommonTost.swift
//  SnapAcleaner
//
//  Created by Coruscate on 19/09/18.
//  Copyright © 2018 Coruscate. All rights reserved.
//

import UIKit

class UIViewCommonTost: UIView {
    
    @IBOutlet weak var contentView : UIView!
    @IBOutlet weak var viewMain : UIView!

    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewGradient: GradientView!

    var click:(()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    func commonInit()  {
        
        Bundle.main.loadNibNamed("UIViewCommonTost", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
  
    }
    
    private func applyShadow() {

        let shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        contentView.layer.shadowColor = shadowColor.cgColor
        contentView.layer.shadowOpacity = 0.2
        contentView.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        contentView.layer.shadowRadius = 5
        
    }

    func setBackGroundColor(color : UIColor) {
        
        viewMain.backgroundColor = color
    }
    
    
    
    //MARK: IBInspectable
    @IBInspectable
    var text: NSString {
        
        get {
            return  ""
        }
        set {
//            let attributedTitle = NSAttributedString(string: (newValue as String).localized, attributes: [NSAttributedString.Key.kern: 1.2, NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : FontScheme.kMediumFont(size: 12.0)])
//            lblTitle.attributedText = attributedTitle
        }
    }
    
    //MARK: IBInspectable
    @IBInspectable
    var imageIcon: NSString {
        
        get {
            return  ""
        }
        set {
            
            if newValue.length > 0 {
                imgIcon.image = UIImage(named: newValue as String)

            }
        }
    }
    
    
    @IBInspectable
    var isGradientbackground : Bool = false {
        didSet {
            let gradient = CAGradientLayer()
            gradient.frame = contentView.bounds
            gradient.colors = [ColorConstants.ThemeGradientStartColor, ColorConstants.ThemeGradientEndColor]
            
            if isGradientbackground == false{
                gradient.removeFromSuperlayer()
            }else{
                contentView.layer.insertSublayer(gradient, at: 0)
            }
        }
    }
    
    func setText(text : String) {
        
        lblTitle.text = text
    }
}
 
