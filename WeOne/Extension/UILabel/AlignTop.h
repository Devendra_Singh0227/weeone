//
//  AlignTop.h
//  Bank2Grow
//
//  Created by Vish on 13/12/17.
//  Copyright © 2017 Coruscate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlignTop : UILabel
- (void)drawTextInRect:(CGRect)rect;
@end
