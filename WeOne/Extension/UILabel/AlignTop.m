//
//  AlignTop.m
//  Bank2Grow
//
//  Created by Vish on 13/12/17.
//  Copyright © 2017 Coruscate. All rights reserved.
//

#import "AlignTop.h"

@implementation AlignTop

- (void)drawTextInRect:(CGRect)rect {
    
    if (self.text) {
        CGSize labelStringSize = [self.text boundingRectWithSize:CGSizeMake(CGRectGetWidth(rect), CGFLOAT_MAX)
                                                         options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                                      attributes:@{NSFontAttributeName:self.font}
                                                         context:nil].size;
        
        CGFloat height = MIN(ceilf(labelStringSize.height), CGRectGetHeight(rect));
        
        [super drawTextInRect:CGRectMake(0, 0, rect.size.width, height)];
        
    } else {
        [super drawTextInRect:rect];
    }
}

@end
