//
//  UIViewControllerExtension.swift
//  DivineSolitaires
//
//  Created by CORUSCATEMAC on 18/06/19.
//  Copyright © 2019 CORUSCATEMAC. All rights reserved.
//
import UIKit
import Foundation

extension UIViewController {
    
    func ShowPopupForForceUpdate(isHardUpdate : Bool){
        
        if isHardUpdate == true{
            self.view.showConfirmationPopupWithSingleButton(title: "KLblApplicationUpdate".localized, message: "KLblNewVerionMsg".localized, confirmButtonTitle: "KLblUpdate".localized) {
                
            }
        }else{
            self.view.showConfirmationPopupWithMultiButton(title: "KLblApplicationUpdate".localized, message: "KLblNewVerionMsg".localized, cancelButtonTitle: StringConstants.ButtonTitles.KCancel, confirmButtonTitle: "KLblUpdate".localized, onConfirmClick: {
                
                Utilities.openURL(AppConstants.iTunesUrl)
            }) {
                
            }
        }
    }
    
    //MARK:- Version Popup
    func checkVersioningUpdate(dict : [String:Any]) {
        
        print(dict)
        if let dictAppVersion = dict["app_version"] as? [String:Any]{
            
            if let dictVersionNumber = dictAppVersion["iphone"] as? [String:Any]{
                
                let currentVersion  :Double =  Utilities.getDoubleValue(value: Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String)
                let pastVersion :Double = Utilities.getDoubleValue(value: dictVersionNumber["version"])
                
                if currentVersion < pastVersion{
                    
                    let isHardUpdate = dictVersionNumber["is_hard_update"] as? Bool ?? false
                    
                    if isHardUpdate == true{
                        Defaults[.SkipUpdate] = false
                        
                        self.ShowPopupForForceUpdate(isHardUpdate: isHardUpdate)
                    }else {
                        
                        if Defaults[.SkipUpdate] == false {
                            self.ShowPopupForForceUpdate(isHardUpdate: isHardUpdate)
                        }
                    }
                }
            }
        }
    }
    
    //MARK: Open Date Picker
    class func openDatePicker(datePicker:GMDatePicker, delegate:GMDatePickerDelegate,selectedDate : Date)
    {
        UIViewController.current().view.endEditing(true)
        datePicker.delegate = delegate
        datePicker.config.startDate = selectedDate
        datePicker.config.animationDuration = 0.5
        datePicker.config.cancelButtonTitle = "KLblCancel".localized
        datePicker.config.confirmButtonTitle = "KLblDone".localized
        datePicker.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        datePicker.config.headerBackgroundColor = ColorConstants.TextColorTheme
        datePicker.config.confirmButtonColor = .white
        datePicker.config.cancelButtonColor = .white
        //datePicker.config.confirmButtonFont = FontScheme.kSemiBoldFont(size: 18)
        //datePicker.config.cancelButtonFont = FontScheme.kSemiBoldFont(size: 18)
        datePicker.show(inVC: UIViewController.current())
        
    }
    
    //MARK: Open Date Picker
    class func openDateTimerPicker(datePicker:GMDatePicker, delegate:GMDatePickerDelegate,selectedDate : Date)
    {
        UIViewController.current().view.endEditing(true)
        datePicker.delegate = delegate
        datePicker.config.startDate = selectedDate
        datePicker.config.datePickerMode = .dateAndTime
        datePicker.config.animationDuration = 0.5
        datePicker.config.cancelButtonTitle = "KLblCancel".localized
        datePicker.config.confirmButtonTitle = "KLblDone".localized
        datePicker.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        datePicker.config.headerBackgroundColor = ColorConstants.TextColorTheme
        datePicker.config.confirmButtonColor = .white
        datePicker.config.cancelButtonColor = .white
        //datePicker.config.confirmButtonFont = FontScheme.kSemiBoldFont(size: 18)
        //datePicker.config.cancelButtonFont = FontScheme.kSemiBoldFont(size: 18)
        datePicker.show(inVC: UIViewController.current())
        
    }
    
    func showToast(text : String, strImg : String? = "info") {
        
        let view = UIViewCommonTost(frame: CGRect(x: 5, y: AppConstants.ScreenSize.SCREEN_HEIGHT - 130    , width: AppConstants.ScreenSize.SCREEN_WIDTH - 10, height: 40))
        view.text = text as NSString
        view.imageIcon = strImg! as NSString
        
        self.view.addSubview(view)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            view.removeFromSuperview()
        })
        
    }
    
    func show_Toast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-100, width: self.view.frame.size.width - 100, height: 60))
        toastLabel.center = self.view.center
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.numberOfLines = 3
        //toastLabel.font = constantsNaming.fontType.kOpenSans_RegularMedium
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 6.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.isHidden = true
        })
    }
    
//    func setTextFieldValidation(range: Int) -> Bool {
//
//        func textField(_ textField: UITextField,
//                       shouldChangeCharactersIn range: NSRange,
//                       replacementString string: String) -> Bool {
//            if let text = textField.text,
//                let textRange = Range(range, in: text) {
//                let updatedText = text.replacingCharacters(in: textRange,
//                                                           with: string)
//                if updatedText.count > range {
//
//                } else {
//
//                }
//            }
//            return true
//        }
//    }
}
