//
//  UIImageViewExtension.swift
//  KiranJewellery
//
//  Created by Rushi Sangani on 27/08/17.
//  Copyright © 2017 Rushi Sangani. All rights reserved.
//

import Foundation
import SDWebImage
import SDWebImageSVGKitPlugin

extension UIImageView {
    
    func setImageForURL(url: URL?, placeHolder: UIImage?) {

        if let tUrl = url {
            self.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.sd_setImage(with: tUrl, placeholderImage: placeHolder)
        }
        else{
            self.image = placeHolder
        }
    }

    @IBInspectable
    var changeColor: UIColor? {
        get {
            let color = UIColor(cgColor: layer.borderColor!);
            return color
        }
        set {
            let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
            self.image = templateImage
            self.tintColor = newValue
        }
    }
}
