//
//  UIViewExtension.swift
//  E-Scooter
//
//  Created by MacOs14 on 13/05/19.
//  Copyright © 2019 CORUSCATEMAC. All rights reserved.
//

//MARK: UIView IBInspactable
extension UIView {
    
    //-------- Language Support
    @IBInspectable
    var NSLanguageKey: NSString   {
        
        get {
            return  "nil"
        }
        
        set {
            let localizedString = (newValue as String).localized
            
            //UILabel
            if let lbl = self as? UILabel{
                lbl.text = localizedString
            }
            
            //UIButton
            if let btn = self as? UIButton{
                btn.setTitle(localizedString, for: .normal)
            }
            
            //UITextField
            if let txt = self as? UITextField{
                txt.text = localizedString
            }
        }
    }
    
    @IBInspectable
    var TintColorKey: NSString   {
        
        get {
            return  "nil"
        }
        
        set {
            tintColor = ColorScheme.colorFromConstant(textColorConstant: newValue as String)
        }
    }
    
    @IBInspectable
    var BackgroundColorKey: NSString   {
        
        get {
            return  "nil"
        }
        
        set {
            backgroundColor = ColorScheme.colorFromConstant(textColorConstant: newValue as String)
        }
    }
        
    @IBInspectable
    var BorderColorKey: NSString   {
        
        get {
            return  "nil"
        }
        
        set {
            layer.borderColor = ColorScheme.colorFromConstant(textColorConstant: newValue as String).cgColor
        }
    }
    
    @IBInspectable
    var corRadius: CGFloat {
        get {
            return  0.0
        }
        
        set {
            layer.cornerRadius = newValue as CGFloat
            layer.masksToBounds = true
        }
    }
    
    @IBInspectable
    var BorderWidth: CGFloat   {
        
        get {
            return  0.0
        }
        
        set {
            layer.borderWidth = newValue as CGFloat
        }
    }
    
    
    func dropShadow() {
        
        layer.shadowColor = ColorConstants.ShadowColor.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 2.0
    }
    
    // set Corner Radius
    func setRadius(radius:CGFloat)
    {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
        self.layer.masksToBounds = true
    }
    
    // set Corner Radius
    func setBorderWidth(width : CGFloat)
    {
        self.layer.borderWidth = width
        self.layer.masksToBounds = true
    }
    
    // set Corner Radius
    func setBorderColor(color:UIColor)
    {
        self.layer.borderColor = color.cgColor
        self.layer.masksToBounds = true
    }
    
    func setCornerWithShadow(radius: CGFloat) {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = radius
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 1.0
    }
    
    func customRoundCornerWithShadow(shadowColor: UIColor) {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = self.frame.height/2
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        self.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 1.0
    }
        
    //MARK: giving corner from particular side
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat ,width : CGFloat,height : CGFloat = AppConstants.ScreenSize.SCREEN_HEIGHT) {
        let frame = CGRect(x: 0, y: 0, width: width, height: height)
        let path = UIBezierPath(roundedRect: frame, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func addDashedBorderVertical(color : UIColor, isVertical : Bool = true) {
        //Create a CAShapeLayer
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = 2
        // passing an array with the values [2,3] sets a dash pattern that alternates between a 2-user-space-unit-long painted segment and a 3-user-space-unit-long unpainted segment
        shapeLayer.lineDashPattern = [4,4]
        
        let path = CGMutablePath()
        if isVertical == true{
            path.addLines(between: [CGPoint(x: 0, y: 0),
                                    CGPoint(x: 0, y: self.frame.height)])
        }else{
            path.addLines(between: [CGPoint(x: 0, y: 0),
                                    CGPoint(x: self.frame.width, y: 0)])
        }
        
        shapeLayer.path = path
        layer.addSublayer(shapeLayer)
    }
    
    func addDashedBorder(color: UIColor) {
        
      let shapeLayer:CAShapeLayer = CAShapeLayer()
      let frameSize = self.frame.size
      let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

      shapeLayer.bounds = shapeRect
      shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
      shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color.cgColor
      shapeLayer.lineWidth = 2
      shapeLayer.lineJoin = CAShapeLayerLineJoin.round
      shapeLayer.lineDashPattern = [6,3]
      shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath

      self.layer.addSublayer(shapeLayer)
        
      }

    
    
    // MARK: Show Confirmation Popup with Multiple Button
    func showConfirmationPopupWithMultiButton(viewController:UIViewController? = nil, title: String?,message:String,cancelButtonTitle:String, isAttributedChangeColor: Bool = false, strAttribute: String = "", confirmButtonTitle:String,onConfirmClick: @escaping () -> (), onCancelClick: @escaping () -> ()){
        
        self.window?.rootViewController?.view.endEditing(true)
        
        let vc = ConfirmPopupVC(nibName: "ConfirmPopupVC", bundle: nil)
        vc.headerTitle = title
        vc.message = message
        vc.cancelButtonTitle = cancelButtonTitle
        vc.confirmButtonTitle = confirmButtonTitle
        vc.isAttributtedChangeColor = isAttributedChangeColor
        vc.strAttribute = strAttribute
        vc.confirmClicked = {
            onConfirmClick()
        }
        
        vc.cancelBtnClicked = {
            onCancelClick()
        }
        
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        
        if let testVC2 = viewController {
            testVC2.present(vc, animated: true, completion: {
            })
        }
        else{
            if UIViewController.current()?.presentedViewController != nil {
                UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
                })
            }
            else{
                UIViewController.current()?.present(vc, animated: true, completion: {
                })
            }
        }
    }
    
    
    // MARK: Show Confirmation Popup with Single Button
    func showConfirmationPopupWithSingleButton(title: String?, message:String,  confirmButtonTitle:String, onConfirmClick: @escaping () -> ()){
        
        self.window?.rootViewController?.view.endEditing(true)
        
        let vc = ConfirmPopupVC(nibName: "ConfirmPopupVC", bundle: nil)
        vc.headerTitle = title
        vc.message = message
        vc.isShowSingleButton = true
        vc.confirmButtonTitle = confirmButtonTitle
        vc.confirmClicked = {
            onConfirmClick()
        }
        
        vc.modalPresentationStyle = .custom
        vc.modalTransitionStyle = .crossDissolve
        
        if UIViewController.current()?.presentedViewController != nil {
            UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
            })
        }
        else{
            UIViewController.current()?.present(vc, animated: true, completion: {
            })
        }
    }
    
    
    func returnConfirmationPopup(title: String?, message:String,  confirmButtonTitle:String, onConfirmClick: @escaping () -> ()) -> UIViewController{
        
        let vc = ConfirmPopupVC(nibName: "ConfirmPopupVC", bundle: nil)
        vc.headerTitle = title
        vc.message = message
        vc.isShowSingleButton = true
        vc.confirmButtonTitle = confirmButtonTitle
        vc.confirmClicked = {
            onConfirmClick()
        }
        
        return vc
    }
    
        func showSelectionPopup(topHeading: String?,defaultSelectedIndex:Int?, isFromCity : Bool = false, isFromSignup : Bool = false, isShowSearchBar:Bool,isShowSelectedImage:Bool, arrData: [String], onSubmitClick: @escaping (Int) -> ()) {
            let popup = CountryListPopup.init(nibName: "CountryListPopup", bundle: nil)
            popup.arr_tblData = arrData
            popup.isFromCity = isFromCity
            popup.isShowSearchBar = isShowSearchBar
            popup.selectedIndex = defaultSelectedIndex
            popup.isShowSelectedImage = isShowSelectedImage
            popup.isFromSignup = isFromSignup
            popup.selectedCountry = { countryIndex in
                 onSubmitClick(countryIndex)
            }
            popup.modalPresentationStyle = .fullScreen
    //        popup.didSelectRow = {(selectedText,selectedIndex) in
    //            onSubmitClick(selectedText,selectedIndex)
    //
    //        }
         //   popup.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency

//            popup.modalTransitionStyle = .crossDissolve
            UIViewController.current()?.present(popup, animated: true, completion: {
            })
        }

    func removeLoadAPIFailedView() {
        
        for vw in self.subviews{
            if let myvw = vw as? LoadAPIFailedView {
                myvw.removeFromSuperview()
            }
        }
        
    }
    
    func loadNoDataFoundView(message:String,image : String? = nil,isShowButton : Bool = false, textColor :UIColor = UIColor.white, onRefreshClick: @escaping () -> ()) {
        
        self.window?.rootViewController?.view.endEditing(true)
        
        let APIFailedView = Bundle.main.loadNibNamed("LoadAPIFailedView", owner: self, options: nil)?[0] as! LoadAPIFailedView
        APIFailedView.isShowRefreshButton = isShowButton
        APIFailedView.message = message
        APIFailedView.image = image
        APIFailedView.textColor = ColorConstants.TextTitleBlack
        
        APIFailedView.frame = CGRect(x: 0, y: (self.bounds.size.height / 2) - 106, width: self.bounds.size.width, height: 212)
        //        APIFailedView.center = self.center
        APIFailedView.layoutIfNeeded()
        var isFound = false
        for vw in self.subviews{
            if vw is LoadAPIFailedView{
                isFound = true
            }
        }
        
        if !isFound{
            self.addSubview(APIFailedView)
        }
        
        APIFailedView.btnRefreshClick = {()
            onRefreshClick()
        }
    }
    
    func addshadow(top: Bool,
                   left: Bool,
                   bottom: Bool,
                   right: Bool,
                   shadowRadius: CGFloat = 2.0, width : CGFloat = 0, height : CGFloat = 0) {
        
        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.6
        
        let path = UIBezierPath()
        var x: CGFloat = 0
        var y: CGFloat = -5
        
        var viewWidth = width == 0 ? self.frame.width : width
        var viewHeight = height == 0 ? self.frame.height+2 : height
        
        // here x, y, viewWidth, and viewHeight can be changed in
        // order to play around with the shadow paths.
        if (!top) {
            y+=(shadowRadius+1)
        }
        if (!bottom) {
            viewHeight-=(shadowRadius+1)
        }
        if (!left) {
            x+=(shadowRadius+1)
        }
        if (!right) {
            viewWidth-=(shadowRadius+1)
        }
        // selecting top most point
        path.move(to: CGPoint(x: x, y: y))
        // Move to the Bottom Left Corner, this will cover left edges
        /*
         |☐
         */
        path.addLine(to: CGPoint(x: x, y: viewHeight))
        // Move to the Bottom Right Corner, this will cover bottom edge
        /*
         ☐
         -
         */
        path.addLine(to: CGPoint(x: viewWidth, y: viewHeight))
        // Move to the Top Right Corner, this will cover right edge
        /*
         ☐|
         */
        path.addLine(to: CGPoint(x: viewWidth, y: y))
        // Move back to the initial point, this will cover the top edge
        /*
         _
         ☐
         */
        path.close()
        self.layer.shadowPath = path.cgPath
    }
    
    func addShadow(offset: CGSize, color: UIColor = .black, opacity: Float = 0.5, radius: CGFloat = 5.0) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = radius
    }
    
        func fadeTransition(_ duration:CFTimeInterval) {
            let animation = CATransition()
            animation.timingFunction = CAMediaTimingFunction(name:
                CAMediaTimingFunctionName.easeInEaseOut)
            animation.type = CATransitionType.fade
            animation.duration = duration
            layer.add(animation, forKey: CATransitionType.fade.rawValue)
        }
}

//MARK: Pan gesture Direction
internal enum Direction {
    case up
    case down
    case left
    case right
}

//MARK: - UIPanGestureRecognizer
internal extension UIPanGestureRecognizer {
    var direction: Direction? {
        let vel = velocity(in: view)
        let isVertical = abs(vel.y) > abs(vel.x)
        
        switch (isVertical, vel.x, vel.y) {
        case (true, _, let y) where y < 0: return .up
        case (true, _, let y) where y > 0: return .down
        case (false, let x, _) where x > 0: return .right
        case (false, let x, _) where x < 0: return .left
        default: return nil
        }
    }
}

