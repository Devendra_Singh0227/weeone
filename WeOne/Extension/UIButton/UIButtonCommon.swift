//
//  UIButtonCommon.swift
//  SnapAcleaner
//
//  Created by Coruscate on 19/09/18.
//  Copyright © 2018 Coruscate. All rights reserved.
//

import UIKit

class UIButtonCommon: UIView {
    
    @IBOutlet weak var contentView : UIView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var viewGradient: UIView!

    var isCancel = false 
    var click:(()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    func commonInit()  {
        
        Bundle.main.loadNibNamed("UIButtonCommon", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true

        //   self.applyShadow()
        viewGradient.setRadius(radius: viewGradient.frame.height / 2)
        
        button.addTarget(self, action: #selector(button_Click), for: .touchUpInside)
        button.addTarget(self, action: #selector(holdDown(_:)), for: .touchDown)

  
    }
    
    private func applyShadow() {

        let shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        contentView.layer.shadowColor = shadowColor.cgColor
        contentView.layer.shadowOpacity = 0.2
        contentView.layer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        contentView.layer.shadowRadius = 5
        
    }

    func setBackGroundColor(color : UIColor) {
        button.backgroundColor = color
    }
    
    @IBAction func button_Click(_ sender: UIButton) {
        if isCancel {
            button.backgroundColor = ColorConstants.TextColorSecondary
        } else {
           // let attributedTitle = NSAttributedString(string: button.titleLabel?.text ?? "", attributes: [NSAttributedString.Key.kern: 1.2, NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : FontScheme.kSemiBoldFont(size: 16.0)])
           // button.setAttributedTitle(attributedTitle, for: .normal)
            button.backgroundColor = ColorConstants.TextColorPrimary
        }
        if let cc = click {
            cc()
        }
    }
    
    @objc func holdDown(_ sender: UIButton) {
        if isCancel {
            button.backgroundColor = ColorConstants.UnderlineColor
        } else {
            //let attributedTitle = NSAttributedString(string: button.titleLabel?.text ?? "", attributes: [NSAttributedString.Key.kern: 1.2, NSAttributedString.Key.foregroundColor : ColorConstants.TextColorPrimary, NSAttributedString.Key.font : FontScheme.kSemiBoldFont(size: 16.0)])
           // button.setAttributedTitle(attributedTitle, for: .normal)

            button.backgroundColor = ColorConstants.UnderlineColor
        }
    }
    
    //MARK: IBInspectable
    @IBInspectable
    var text: NSString {
        
        get {
            return  ""
        }
        set {
            //button.setTitle(newValue as String, for: .normal)
            //let attributedTitle = NSAttributedString(string: (newValue as String).localized, attributes: [NSAttributedString.Key.kern: 1.2, NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : FontScheme.kSemiBoldFont(size: 16.0)])
            //button.setAttributedTitle(attributedTitle, for: .normal)
        }
    }
    
    //MARK: IBInspectable
    @IBInspectable
    var imageRight: NSString {
        
        get {
            return  ""
        }
        set {
            
            if newValue.length > 0 {
                button.setImage(UIImage(named: newValue as String), for: .normal)
                button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
            }
        }
    }
    
    
    @IBInspectable
    var isGradientbackground : Bool = false {
        didSet {
            let gradient = CAGradientLayer()
            gradient.frame = contentView.bounds
            gradient.colors = [ColorConstants.ThemeGradientStartColor, ColorConstants.ThemeGradientEndColor]
            
            if isGradientbackground == false{
                gradient.removeFromSuperlayer()
            }else{
                button.backgroundColor = UIColor.clear
                contentView.layer.insertSublayer(gradient, at: 0)
            }
        }
    }
    
    func setText(text : String) {
        
        //let attributedTitle = NSAttributedString(string: text, attributes: [NSAttributedString.Key.kern: 1.2, NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : FontScheme.kSemiBoldFont(size: 16.0)])
       // button.setAttributedTitle(attributedTitle, for: .normal)
    }
}
 
