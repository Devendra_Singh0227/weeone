//
//  FareMasterModel.swift
//  E-Scooter
//
//  Created by Coruscate Mac on 11/06/19.
//  Copyright © 2019 CORUSCATEMAC. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
import ObjectMapper_Realm
import ObjectMapper

class MasterModel: Object, Mappable {
    
    @objc dynamic var createdAt : String?
    @objc dynamic var updatedAt : String?
    @objc dynamic var id : String?
    @objc dynamic var name : String?
    @objc dynamic var normalizeName : String?
    @objc dynamic var slug : String?
    @objc dynamic var code : String?
    @objc dynamic var group : String?
    @objc dynamic var desc : String?
    @objc dynamic var isActive : Bool = false
    @objc dynamic var isDefault : Bool = false
    @objc dynamic var sortingSequence : Int = 0
    @objc dynamic var image : String?
    @objc dynamic var icon : String?
    @objc dynamic var likeKeyWords : String?
    @objc dynamic var isDeleted : Bool = false
    @objc dynamic var addedBy : String?
    @objc dynamic var updatedBy : String?
    @objc dynamic var parentId : String?
    
    var isSelected : Bool = false
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        
        createdAt                       <- map["createdAt"]
        updatedAt                       <- map["updatedAt"]
        id                              <- map["id"]
        name                            <- map["name"]
        normalizeName                   <- map["normalizeName"]
        slug                            <- map["slug"]
        code                            <- map["code"]
        group                           <- map["group"]
        desc                            <- map["desc"]
        isActive                        <- map["isActive"]
        isDefault                       <- map["isDefault"]
        sortingSequence                 <- map["sortingSequence"]
        image                           <- map["image"]
        icon                            <- map["icon"]
        likeKeyWords                    <- map["likeKeyWords"]
        isDeleted                       <- map["isDeleted"]
        addedBy                         <- map["addedBy"]
        updatedBy                       <- map["updatedBy"]
        parentId                        <- map["parentId"]
    }
    
}

            // MARK: - RatingModel
class RatingModel:  Object, Mappable {
    
    @objc dynamic var createdAt : String?
    @objc dynamic var updatedAt : String?
    @objc dynamic var id : String?
    @objc dynamic var star : Int = 0
    @objc dynamic var ratingTitle : String?
    @objc dynamic var addedBy : String?
    @objc dynamic var updatedBy : String?
    
    var allowCategory = List<String>()

    required convenience init?(map: Map) {
        self.init()
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    
    func mapping(map: Map) {
        
        createdAt                       <- map["createdAt"]
        updatedAt                       <- map["updatedAt"]
        id                              <- map["id"]
        star                            <- map["star"]
        ratingTitle                     <- map["ratingTitle"]
        addedBy                         <- map["addedBy"]
        updatedBy                       <- map["updatedBy"]
        
        allowCategory                   <- (map["allowCategory"] , arrayToList())
        
    }
}

// Transform Array to Realm.List -> Primitives

func arrayToList<T>() -> TransformOf<List<T>, [T]> {
    return TransformOf(
        fromJSON: { (value: [T]?) -> List<T> in
            let result = List<T>()
            if let value = value {
                result.append(objectsIn: value)
            }
            return result
    },
        toJSON: { (value: List<T>?) -> [T] in
            var results = [T]()
            if let value = value {
                results.append(contentsOf: Array(value))
            }
            return results
    })
}

