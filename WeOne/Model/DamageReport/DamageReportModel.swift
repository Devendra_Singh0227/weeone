//
//  DamageReportModel.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 18/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit
import ObjectMapper

class DamageReportModel: NSObject, Mappable {

    var createdAt : String?
    var updatedAt : String?
    var id : String?
    var damageParts : String?
    var desc : String?
    var addedBy : String?
    var updatedBy : String?
    var vehicleId : String?
    var partnerId : String?
    var rideId : String?
    
    var damageCategory : [Int] = [Int]()
    var status : Int = 0
    
    var images: [ImagesModel] = [ImagesModel]()

    var isSelected:Bool = false
   
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        createdAt               <- map["createdAt"]
        updatedAt               <- map["updatedAt"]
        id                      <- map["id"]
        damageParts             <- map["damageParts"]
        desc                    <- map["description"]
        addedBy                 <- map["addedBy"]
        updatedBy               <- map["updatedBy"]
        vehicleId               <- map["vehicleId"]
        partnerId                <- map["partnerId"]
        rideId                  <- map["rideId"]
        
        damageCategory          <- map["damageCategory"]
        status                  <- map["status"]
        
        images                  <- map["images"]
    }
    
    func getImage() -> String? {
    
        if images.count > 0 {
            return images[0].path
        }
        return nil
    }
}

