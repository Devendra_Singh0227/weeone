//
//  CellModel.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved

import UIKit
import ObjectMapper
import RealmSwift
import Realm
import ObjectMapper_Realm

class StaticModel: NSObject, Mappable {
    
    var createdAt : String?
    var updatedAt : String?
    var id : String?
    var code : String?
    var listDescription : String?
    var title : String?
    var type : Int = 0
    var addedBy : String?
    var updatedBy : String?
    var parentID : String?
    var multiLanguageData: MultiLanguageData?

    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        createdAt                        <- map["createdAt"]
        updatedAt                      <- map["updatedAt"]
        id                       <- map["id"]
        code              <- map["code"]
        listDescription              <- map["description"]
        title              <- map["title"]
        type              <- map["type"]
        addedBy              <- map["addedBy"]
        updatedBy              <- map["updatedBy"]
        parentID              <- map["parentId"]
        multiLanguageData              <- map["multiLanguageData"]

//        packages.first?.isSelected     = true
    }
}

// MARK: - MultiLanguageData
class MultiLanguageData: Codable {
    let enUS: EnUS

    enum CodingKeys: String, CodingKey {
        case enUS = "en-US"
    }

    init(enUS: EnUS) {
        self.enUS = enUS
    }
}

// MARK: - EnUS
class EnUS: Codable {
    let enUSDescription: String

    enum CodingKeys: String, CodingKey {
        case enUSDescription = "description"
    }

    init(enUSDescription: String) {
        self.enUSDescription = enUSDescription
    }
}


