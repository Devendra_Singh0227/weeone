//
//  RideListModel.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 12/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit
import ObjectMapper

//MARK:- RideListModel`
class RideListModel: NSObject, Mappable {
    
    var createdAt : String?
    var updatedAt : String?
    var id : String?
    var rideNumber : String?
    var reservedDateTime : String?
    var reservedEndDateTime : String?
    var estimatedReturnTime : String?
    var estimateReturnLocation : LocationModel?
    var pauseEndDateTime : String?
    var startDateTime : String?
    var endDateTime : String?
    var cancelDateTime : String?
    var navigateLocations : [NavigateLocation] = [NavigateLocation]()

    var startLocation : LocationModel?
    var endLocation : LocationModel?
    var geoLocation : LocationModel?
    var stopOverTrack : String?
    var estimateEndLocation : LocationModel?

    
    var selectedCard : CardModel?
    var beforeAttachment : String?
    var afterAttachment : String?
    var requestEndDateTime : String?
    var addedBy : String?
    var updatedBy : String?
    var userId : String?
    var pickupParkedStationId : String?
    var dropOffParkedStationId : String?
    var partnerId : String?
    var rating : String?
    var qnr : String?

    var iotRideId : Int = 0
    var status : Int = 0
    var startKm : Int = 0
    var endKm : Int = 0
    var startFuelLevel : Int = 0
    var endFuelLevel : Int = 0
    var otp : Int = 0
    var totalKm : Double = 0
    var totalTime : Int = 0
    var totalFare : Double = 0.0
    var perHourCharge: Double? = 0.0
    var advancedPaidAmount : Double = 0.0
    var pauseTime : Int = 0
    var currentRequestTry : Int = 0
    var maxRequestTry : Int = 0
    var rideType : Int = 0
    
    var isOTPVerify : Bool = false
    var isChargeBefore : Bool = false
    var isPaid : Bool = false
    var isPaused : Bool = false
    var isRequested : Bool = false
    var isDepositRefund : Bool = false

    var fareSummary : FareSummaryModel?
    var partnerSetting : PartnerSettingModel?
    var pricingConfigData : PackagesModel?
    var vehicleId : VehicleModel?
    var nextBookingDetail : NextBookingDetailModel?
    var keyHandling : KeyHandlingModel?

    var payments : [PaymentsModel] = [PaymentsModel]()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        createdAt                   <- map["createdAt"]
        updatedAt                   <- map["updatedAt"]
        id                          <- map["id"]
        rideNumber                  <- map["rideNumber"]
        reservedDateTime            <- map["reservedDateTime"]
        reservedEndDateTime         <- map["reservedEndDateTime"]
        estimatedReturnTime         <- map["estimatedReturnTime"]
        pauseEndDateTime            <- map["pauseEndDateTime"]
        startDateTime               <- map["startDateTime"]
        endDateTime                 <- map["endDateTime"]
        cancelDateTime              <- map["cancelDateTime"]
        startLocation               <- map["startLocation"]
        navigateLocations           <- map["navigateLocations"]
        endLocation                 <- map["endLocation"]
        geoLocation                 <- map["geoLocation"]
        stopOverTrack               <- map["stopOverTrack"]
        estimateEndLocation         <- map["estimateEndLocation"]
        estimateReturnLocation         <- map["estimateReturnLocation"]
        selectedCard                <- map["selectedCard"]
        beforeAttachment            <- map["beforeAttachment"]
        afterAttachment             <- map["afterAttachment"]
        requestEndDateTime          <- map["requestEndDateTime"]
        addedBy                     <- map["addedBy"]
        updatedBy                   <- map["updatedBy"]
        userId                      <- map["userId"]
        pickupParkedStationId       <- map["pickupParkedStationId"]
        dropOffParkedStationId      <- map["dropOffParkedStationId"]
        partnerId                    <- map["partnerId"]
        rating                      <- map["rating"]
        qnr                         <- map["qnr"]
        iotRideId                   <- map["iotRideId"]
        status                      <- map["status"]
        startKm                     <- map["startKm"]
        endKm                       <- map["endKm"]
        startFuelLevel              <- map["startFuelLevel"]
        endFuelLevel                <- map["endFuelLevel"]
        otp                         <- map["otp"]
        totalKm                     <- map["totalKm"]
        totalTime                   <- map["totalTime"]
        totalFare                   <- map["totalFare"]
        perHourCharge               <- map["perHourCharge"]
        advancedPaidAmount          <- map["advancedPaidAmount"]
        pauseTime                   <- map["pauseTime"]
        currentRequestTry           <- map["currentRequestTry"]
        maxRequestTry               <- map["maxRequestTry"]
        rideType                    <- map["rideType"]
        
        isOTPVerify                 <- map["isOTPVerify"]
        isChargeBefore              <- map["isChargeBefore"]
        isPaid                      <- map["isPaid"]
        isPaused                    <- map["isPaused"]
        isRequested                 <- map["isRequested"]
        isDepositRefund             <- map["isDepositRefund"]
        
        fareSummary                 <- map["fareSummary"]
        partnerSetting              <- map["partnerSetting"]
        pricingConfigData           <- map["pricingConfigData"]
        vehicleId                   <- map["vehicleId"]
        payments                    <- map["payments"]
        nextBookingDetail           <- map["nextBookingDetail"]
        keyHandling                 <- map["keyHandling"]
        
    }
    
    
    // lazy variables for display with some formates
    public var brandName : String? {
        get {
            
            let arrBrands = SyncManager.sharedInstance.fetchMasterWithId(vehicleId?.brand ?? "" )

            if arrBrands.first?.name != nil{
                return "\(arrBrands.first?.name ?? "") - \(vehicleId?.name ?? "")"
            }
            return vehicleId?.name ?? nil
        }
    }
    
    func getTotalDuration(isShortUnit:Bool = true) -> (String, String){
        return ("\(totalTime)", isShortUnit ? StringConstants.MeasurementUnit.KLblMin : StringConstants.MeasurementUnit.KLblMinutes)
    }
    
    func getTotalDistance(isShortUnit:Bool = true) -> (String, String){
        return ("\(Utilities.toDoubleString(value: totalKm))", isShortUnit ? StringConstants.MeasurementUnit.KLblKm : StringConstants.MeasurementUnit.KLblKilometers)
    }
    
    func getTotalFare() -> String {
        
        return Utilities.convertToCurrency(number: totalFare, currencyCode: partnerSetting?.currency)
    }
    
    func getInsurance() -> String {
        return Utilities.convertToCurrency(number: fareSummary?.insurance ?? 0.0, currencyCode: partnerSetting?.currency)
    }
    
    func getDeposit() -> String {
        if let dep = vehicleId?.deposit {
            return Utilities.convertToCurrency(number: dep, currencyCode: partnerSetting?.currency)
        }
        return Utilities.convertToCurrency(number: fareSummary?.deposit ?? 0.0, currencyCode: partnerSetting?.currency)
    }
    
    
    func getRefundAmount() -> String {
        return Utilities.convertToCurrency(number: fareSummary?.refundAmount ?? 0.0, currencyCode: partnerSetting?.currency)
    }
    
    func getAdvanceAmount() -> String {
        return Utilities.convertToCurrency(number: fareSummary?.advancedAmount ?? 0.0, currencyCode: partnerSetting?.currency)
    }
    
    func getBaseFare() -> String {
        return Utilities.convertToCurrency(number: pricingConfigData?.rate ?? 0.0, currencyCode: partnerSetting?.currency)
    }
    
    func getExtraDistance() -> String {
        return "\(Utilities.toDoubleString(value: fareSummary?.extraDistnce ?? 0.0)) \("KLblKm".localized)"
    }
    
    func getAdditionalRate() -> String {
//        return "+ \(Utilities.convertToCurrency(number: fareSummary?.addtionalRate ?? 0.0, currencyCode: pricingConfigData?.currency))"
        return " \(Utilities.convertToCurrency(number: fareSummary?.addtionalRate ?? 0.0, currencyCode: partnerSetting?.currency))"

    }
    
    
    func getCalculatedPackageRate() -> String {
    //        return "+ \(Utilities.convertToCurrency(number: fareSummary?.addtionalRate ?? 0.0, currencyCode: pricingConfigData?.currency))"
            return " \(Utilities.convertToCurrency(number: fareSummary?.calculatedPackageRate ?? 0.0, currencyCode: partnerSetting?.currency))"

        }
    func getTax() -> String {
//        return "+ \(Utilities.convertToCurrency(number: fareSummary?.tax ?? 0.0, currencyCode: pricingConfigData?.currency))"
        return " \(Utilities.convertToCurrency(number: fareSummary?.tax ?? 0.0, currencyCode: partnerSetting?.currency))"

    }
    
    func getParkingRate() -> String {
//        return "+ \(Utilities.convertToCurrency(number: fareSummary?.parkingRate ?? 0.0, currencyCode: pricingConfigData?.currency))"
        return " \(Utilities.convertToCurrency(number: fareSummary?.parkingRate ?? 0.0, currencyCode: partnerSetting?.currency))"

    }
    
    func getFuleCharge() -> String {
//        return "+ \(Utilities.convertToCurrency(number: fareSummary?.fuelCharges ?? 0.0, currencyCode: pricingConfigData?.currency))"
        return " \(Utilities.convertToCurrency(number: fareSummary?.fuelCharges ?? 0, currencyCode: partnerSetting?.currency))"

    }
    
    func getDiscount() -> String {
//        return "- \(Utilities.convertToCurrency(number: fareSummary?.discount ?? 0.0, currencyCode: pricingConfigData?.currency))"

        return " \(Utilities.convertToCurrency(number: fareSummary?.discount ?? 0.0, currencyCode: partnerSetting?.currency))"
    }
    
    func getCardDetails() -> CardModel? {
        if payments.count > 0 {
            return payments.first?.card
        }
        return nil
    }
    
    
    public var nextBookingAvailabelTime : String{
        
        get{
            
            if nextBookingDetail != nil{
                
                let date = DateUtilities.convertToISOFormat(dateStr: Date())
                let startDate = DateUtilities.convertDateFromStringWithFromat(dateStr: date, format: DateUtilities.DateFormates.kMainSourceFormat)
                let endDate = DateUtilities.convertDateFromStringWithFromat(dateStr: nextBookingDetail?.reservedDateTime ?? "", format: DateUtilities.DateFormates.kMainSourceFormat)
                let distanceBetweenDates: TimeInterval? = endDate.timeIntervalSince(startDate)
                let secondsInAnHour: Double = 3600

                let hoursBetweenDates = Int((distanceBetweenDates! / secondsInAnHour))

                if hoursBetweenDates < AppConstants.NextBookingTotalHours && hoursBetweenDates > 0 {
                    return String(format : "\("KLblCarAvailableTime".localized)", hoursBetweenDates)
                }else{
                    return String(format : "\("KLblCarAvailableTime".localized)", AppConstants.NextBookingTotalHours)
                }
            }else{
                return String(format : "\("KLblCarAvailableTime".localized)", AppConstants.NextBookingTotalHours)
            }
        }
    }
}

//MARK:- Key Handaling Model
class KeyHandlingModel: NSObject, Mappable {
    
    var id : String = ""
    var name : String = ""
    var contactNumber : String = ""
    var countryCode : String = ""
    var personName : String = ""

    var keyCode  : String = ""
    var stationType : Int = 0

    var geoLocation : LocationModel?

    var locationImages: [LocationImageModel]?

    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id               <- map["id"]
        name                     <- map["name"]
        contactNumber                <- map["contactNumber"]
        countryCode            <- map["countryCode"]
        personName   <- map["personName"]
        keyCode        <- map["keyCode"]
        stationType                   <- map["stationType"]
        geoLocation          <- map["geoLocation"]
        locationImages          <- map["locationImages"]

    }
}

//MARK:- Deductible Insurance Model
class LocationImageModel: NSObject, Mappable {
    
    var path : String = ""
    var name : String = ""
    var locationDesc : String = ""
    var err : String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        path                        <- map["path"]
        name                        <- map["name"]
        locationDesc    <- map["description"]
        err                         <- map["err"]
        
    }
}

//MARK:- Fare Summary Model
class FareSummaryModel: NSObject, Mappable {
    
    var insurance : Double = 0.0
    var deposit : Double = 0.0
    var selectedPackageRate : Double = 0.0
    var advancedAmount : Double = 0.0
    var totalFareEstimate : Double = 0.0
    var extraDistnce : Double = 0.0
    var addtionalRate : Double = 0.0
    var fuelCharges : Double = 0.0
    var parkingRate : Double = 0.0
    var tax : Double = 0.0
    var discount : Double = 0.0
    var refundAmount : Double = 0.0
    var calculatedPackageRate : Double = 0.0
    var totalPackageRate : Double = 0.0
    var total : Double = 0.0
    var depositRelease : Double = 0.0
    var percentageReturn : Int = 0
    var totalWithoutDiscount : Double = 0.0
    var chargeableAmount : Double = 0.0

    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        insurance               <- map["insurance"]
        deposit                 <- map["deposit"]
        selectedPackageRate     <- map["SelectedPackageRate"]
        advancedAmount          <- map["advancedAmount"]
        totalFareEstimate       <- map["totalFareEstimate"]
        extraDistnce            <- map["extraDistance"]
        addtionalRate           <- map["additionalRate"]
        fuelCharges             <- map["fuelCharges"]
        parkingRate             <- map["parkingRate"]
        tax                     <- map["tax"]
        discount                <- map["discount"]
        refundAmount            <- map["rideCancelRefundableAmount"]
        calculatedPackageRate   <- map["calculatedPackageRate"]
        totalPackageRate        <- map["totalPackageRate"]
        total                   <- map["total"]
        depositRelease          <- map["depositRelease"]
        percentageReturn          <- map["percentageReturn"]
        totalWithoutDiscount          <- map["totalWithoutDiscount"]
        chargeableAmount          <- map["chargeableAmount"]

    }
}

//MARK:- Partner Setting Model
class PartnerSettingModel: NSObject, Mappable {
    
    var createdAt : String?
    var updatedAt : String?
    var id : String?
    var addedBy : String?
    var updatedBy : String?
    var partnerId : String?
    
    var reserveTimeLimit : Int = 0
    var extendTimeLimit : Int = 0
    var cancellationTime : Int = 0
    var lockDisplayDistance : Int = 0
    var fuelCharge : Int = 0
    var currency : Int = 0
    
    var fuelDiscount : Double = 0.0
    var insuranceAmount : Double = 0.0
    
    var deductibleInsurance : DeductibleInsuranceModel?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        createdAt               <- map["createdAt"]
        updatedAt               <- map["updatedAt"]
        id                      <- map["id"]
        addedBy                 <- map["addedBy"]
        updatedBy               <- map["updatedBy"]
        partnerId                <- map["partnerId"]
        
        reserveTimeLimit        <- map["reserveTimeLimit"]
        extendTimeLimit         <- map["extendTimeLimit"]
        cancellationTime        <- map["cancellationTime"]
        lockDisplayDistance     <- map["lockDisplayDistance"]
        fuelCharge              <- map["fuelCharge"]
        currency                <- map["currency"]
        
        fuelDiscount            <- map["fuelDiscount"]
        insuranceAmount         <- map["insuranceAmount"]
        
        deductibleInsurance     <- map["deductibleInsurance"]
    }
}

//MARK:- Deductible Insurance Model
class DeductibleInsuranceModel: NSObject, Mappable {
    
    var from : Int = 0
    var to : Int = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        from                    <- map["from"]
        to                      <- map["to"]
    }
}


//MARK:- Payments Model
class PaymentsModel: NSObject, Mappable {
    
    var createdAt : String?
    var updatedAt : String?
    var id : String?
    var remark : String?
    var stripeTransactionId : String?
    var bankDetail : String?
    var fees : String?
    var addedBy : String?
    var updatedBy : String?
    var transactionBy : String?
    var transactionTo : String?
    var rideId : String?
    var invoiceId : String?
    var partnerId : String?
    
    var status : Int = 0
    var type : Int = 0
    var chargeType : Int = 0
    var currency : Int = 0
    
    var amount : Double = 0.0
    
    
    var refunded : Bool = false
    
    var card : CardModel?
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        createdAt               <- map["createdAt"]
        updatedAt               <- map["updatedAt"]
        id                      <- map["id"]
        remark                  <- map["remark"]
        stripeTransactionId     <- map["stripeTransactionId"]
        bankDetail              <- map["bankDetail"]
        fees                    <- map["fees"]
        addedBy                 <- map["addedBy"]
        updatedBy               <- map["updatedBy"]
        transactionBy           <- map["transactionBy"]
        transactionTo           <- map["transactionTo"]
        rideId                  <- map["rideId"]
        invoiceId               <- map["invoiceId"]
        partnerId                <- map["partnerId"]
        
        status                  <- map["status"]
        type                    <- map["type"]
        chargeType              <- map["chargeType"]
        currency                <- map["currency"]
        
        amount                  <- map["amount"]
        
        refunded                <- map["refunded"]
        
        card                    <- map["card"]
    }
}

//MARK: Partner Model
class PartnerModel : Mappable{
    
    public var id : String?
    public var firstName : String?
    public var lastName : String?
    public var mobiles: [MobileModel] = [MobileModel]()
    public var emails: [MobileModel] = [MobileModel]()
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id                      <- map["id"]
        firstName               <- map["firstName"]
        lastName                <- map["lastName"]
        
        var mob :Any?
        mob     <- map["mobiles"]
                
        if let arrMobile = mob as? [[String:Any]]{
            mobiles = Mapper<MobileModel>().mapArray(JSONArray: arrMobile)
        }
        
        var eml :Any?
        eml     <- map["mobiles"]
                
        if let arrMobile = eml as? [[String:Any]]{
            emails = Mapper<MobileModel>().mapArray(JSONArray: arrMobile)
        }

    }
    
    //get Full Name
    public var fullName : String? {
        return "\(firstName ?? "") \(lastName ?? "")"
    }
    
    //Primary Email
    public var primaryEmail : MobileModel? {
        
        let filter = emails.filter{($0.isPrimary ?? false)}
        if filter.count > 0 {
            return filter.first
        }
        return emails.first
    }
    
    //Primary Mobile
    public var primaryMobile : MobileModel? {
        
        let filter = mobiles.filter{($0.isPrimary ?? false)}
        if filter.count > 0 {
            return filter.first
        }
        return mobiles.first
    }
}
