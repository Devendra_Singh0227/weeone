//
//  CellModel.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved

import UIKit

class CellModel: NSObject {
    
    
    var placeholder : String?
    var placeholder2 : String?
    var userText : String?
    var userText1 : String?
    var userText2 : String?
    var cellType : CellType?
    var classType : ClassType?
    var cellObj : Any?
    var imageName: String?
    var imageType: Int = 0
    var keyboardType: UIKeyboardType?
    var isSelected : Bool = false
    var isSelected2 : Bool = false
    var selectedDate : Date?
    var dataArr = [Any]()
    var data  = [Any]()
    var textFieldReturnType: UIReturnKeyType?
    var message : String?
    var apiKey:String?
    var apiKey2:String?
    var errorMessage:String?
    var errorMessage2:String?
    var isRequired : Bool = false
    var textFieldType:Int = 0
    var key:Int = 0
    var emptySpace:CGFloat = 0
    var isUploaded : Bool = false
    var isUploading : Bool = false
    var isFailed : Bool = false
    var isExpand : Bool = false
    var isLast : Bool = false
    var index: Int = 0
    var uploadProgress: Double = 0.0
    var startKm : Int = 0
    var inSequence : Int = 0

    override init() {
    }
    
    //Get Model
    
    class func getModel(placeholder : String? = nil,placeholder2:String? = nil, text : String? = nil, userText1: String? = nil,type : CellType, classType : ClassType? = nil, imageName:String? = nil,imageType: Int = 0, cellObj:Any? = nil,dataArr : [Any] = [Any](), data : [Any] = [Any](), keyBoardType : UIKeyboardType? = .default,textFieldReturnType: UIReturnKeyType? = .default,  isRequired : Bool = false, isSelected:Bool = false, isSelected2:Bool = false, textFieldType:Int = 0, key:Int = 0 , selectedDate:Date? = nil, apiKey : String? = nil, apiKey2 : String? = nil, errorMessage : String? = nil, errorMessage2 : String? = nil, emptySpace:CGFloat = 0.0, startKm: Int = 0, inSequence: Int = 0, isExpand:Bool = false, isLast:Bool = false) -> CellModel {
        
        let model = CellModel()
        model.placeholder = placeholder
        model.placeholder2 = placeholder2
        model.userText = text
        model.cellType = type
        model.classType = classType
        model.cellObj = cellObj
        model.imageName = imageName
        model.imageType = imageType
        model.keyboardType = keyBoardType
        model.data = data
        model.textFieldReturnType = textFieldReturnType
        model.key = key
        model.isSelected = isSelected
        model.isSelected2 = isSelected2
        model.userText1 = userText1
        model.selectedDate = selectedDate
        model.textFieldType = textFieldType
        model.apiKey = apiKey
        model.apiKey2 = apiKey2
        model.isRequired = isRequired
        model.errorMessage = errorMessage
        model.errorMessage2 = errorMessage2
        model.emptySpace = emptySpace
        model.startKm = startKm
        model.inSequence = inSequence
        model.isExpand = isExpand
        model.isLast = isLast

        if dataArr.count > 0
        {
            model.dataArr = dataArr
        }
        
        return model
    }
  
    class func getLeftMenuModel(text : String,type : CellType,imageName:String) -> CellModel {
        
        let model = CellModel()
        model.userText = text
        model.cellType = type
        model.imageName = imageName
        return model
    }
}

class CarModel: NSObject {
    
    
    var photoSide: String?
    var imageType: Int = 0

    override init() {
    }
    
    //Get Model
    
    class func getModel(photoSide : String? = nil,imageType: Int = 0) -> CarModel {
        
        let model = CarModel()
        model.photoSide = photoSide
        model.imageType = imageType

        return model
    }
}
