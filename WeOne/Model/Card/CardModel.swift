//
//  CardModel.swift
//  E-Scooter
//
//  Created by CORUSCATEMAC on 11/06/19.
//  Copyright © 2019 CORUSCATEMAC. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift
import Realm
import ObjectMapper_Realm

class CardModel: Object, Mappable {

    @objc dynamic var id : String?
    @objc dynamic var brand : String?
    @objc dynamic var last4 : String?
    @objc dynamic var cardType : Int = 0
    
    @objc dynamic var expMonth : Int = 0
    @objc dynamic var expYear : Int = 0
    
    @objc dynamic var isPrimary : Bool = false
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        
        id                          <- map["id"]
        brand                       <- map["brand"]
        last4                       <- map["last4"]
        cardType                    <- map["cardType"]
        expMonth                    <- map["expMonth"]
        expYear                     <- map["expYear"]
        isPrimary                   <- map["isPrimary"]
        
    }
    
    func getFormattedCardNumber() -> String {
        return "xxxx \(last4 ?? "")"
    }
}

class FavouritePlaceModel: Object, Mappable {
    
    @objc dynamic var id : String?
    @objc dynamic var addedBy : String?
    @objc dynamic var name : String?
    @objc dynamic var action : Int = 0
    @objc dynamic var isPrimary : Bool = false
    @objc dynamic var geoLocation : LocationModel?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        
        id                          <- map["id"]
        addedBy                     <- map["addedBy"]
        name                        <- map["name"]
        action                      <- map["action"]
        isPrimary                   <- map["isPrimary"]
        geoLocation                 <- map["geoLocation"]
    }

}

class UserSettingModel: Object, Mappable {
    
    @objc dynamic var id : String?
    @objc dynamic var userID : String?
    @objc dynamic var createdAt : String?
    @objc dynamic var updatedAt : String?

    @objc dynamic var addedBy : String?
    @objc dynamic var updatedBy : String?

    @objc dynamic var isAccAndRideUpdate : UpdateModel?
    @objc dynamic var isNewsAndDisUpdate : UpdateModel?

    required convenience init?(map: Map) {
        self.init()
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {

        id                            <- map["id"]
        addedBy                       <- map["addedBy"]
        userID                        <- map["userId"]
        updatedBy                     <- map["updatedBy"]
        isNewsAndDisUpdate            <- map["isNewsAndDisUpdate"]
        isAccAndRideUpdate            <- map["isAccAndRideUpdate"]
        updatedAt                     <- map["updatedAt"]
        createdAt                     <- map["createdAt"]

    }
}

class UpdateModel: Object, Mappable {
    
    @objc dynamic var isSMS : Bool = false
    @objc dynamic var isPushNotification : Bool = false
    @objc dynamic var isEmail : Bool = false

    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        isSMS                          <- map["isSms"]
        isPushNotification             <- map["isPushNotification"]
        isEmail                        <- map["isEmail"]
    }
}
