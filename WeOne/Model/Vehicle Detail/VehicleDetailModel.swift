//
//  VehicleDetailModel.swift
//  WeOne
//
//  Created by Coruscate Mac on 29/11/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift
import Realm
import ObjectMapper_Realm

class VehicleDetailModel: NSObject, Mappable {
    
    var vehicleId : Int?
    var winterTyre : Int?
    var features : Features?
    var car_images : [String]?
    var availability : Int?
    var availableRange : Int?
    var fuelLevel : Int?
    var estimatedKmPerFuel : Int?
    var more_info : [MoreInfo]?
    var cost : Cost?
    
    var vehicle : VehicleModel?
    var otherInfo:[OtherInfoModel] = [OtherInfoModel]()
    var packages:[PackagesModel] = [PackagesModel]()
    var nextBookingDetail : NextBookingDetailModel?
    var pricing : PricingModel?
    var previousDamageReports : [PreviousDamageReports]?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        features                       <- map["features"]
        car_images                     <- map["images"]
        more_info                      <- map["templates"]
        availability                   <- map["availability"]
        vehicle                        <- map["vehicle"]
        availableRange                 <- map["availableRange"]
        fuelLevel                      <- map["fuelLevel"]
        estimatedKmPerFuel             <- map["estimatedKmPerFuel"]
        
        
        otherInfo                      <- map["otherInfo"]
        packages                       <- map["packages"]
        nextBookingDetail              <- map["nextBookingDetail"]
        pricing                        <- map["pricing"]
        previousDamageReports          <- map["previousDamageReports"]
    }
}

class Features: NSObject, Mappable {
    
    var CarCategory : String?
    var childSeat : String?
    var fuelType : String?
    var gearType : String?
    var suitcases : Int?
    var winterTyre : Bool?
    var noOfSeat : Int?
    var model : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        CarCategory              <- map["CarCategory"]
        childSeat                <- map["childSeat"]
        fuelType                 <- map["fuelType"]
        gearType                 <- map["gearType"]
        suitcases                <- map["suitcases"]
        winterTyre               <- map["winterTyre"]
        noOfSeat                 <- map["noOfSeat"]
        model                    <- map["model"]
    }
}

class MoreInfo: NSObject, Mappable {
    
    var name : String?
    var text : String?
    var code : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        name              <- map["name"]
        code              <- map["code"]
        text              <- map["text"]
              
    }
}

class Cost: NSObject, Mappable {
    
    var perDayCost : Double?
    var perExtraKmCost : Double?
    var perHourCost : Double?
    var perMonthCost : Double?
    var perweekCost : Double?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        perDayCost              <- map["perDayCost"]
        perExtraKmCost          <- map["perExtraKmCost"]
        perHourCost             <- map["perHourCost"]
        perMonthCost            <- map["perMonthCost"]
        perweekCost             <- map["perweekCost"]
              
    }
}

class PreviousDamageReports: NSObject, Mappable {
      
      var name: String?
      var images: [DamageImages]?
      var damageParts : String?
      var vehicleId : String?
    
      required convenience init?(map: Map) {
          self.init()
      }
      
      func mapping(map: Map) {
          name                        <- map["name"]
          images                      <- map["images"]
          damageParts                 <- map["damageParts"]
          vehicleId                   <- map["vehicleId"]
      }
  }

class DamageImages: NSObject, Mappable {
    
    var name: String?
    var path: String?
    var type : Int?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        name                        <- map["name"]
        path                        <- map["path"]
        type                        <- map["type"]
        
    }
}

class VehiclePropertyList: NSObject, Mappable {
    
    var name: String?
    var image: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        name                      <- map["name"]
        image                       <- map["image"]
    }
}

class PricingModel: NSObject, Mappable {
    
    var insuranceAmount: Double = 0
    var totalRate: Double = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        insuranceAmount                 <- map["insuranceAmount"]
        totalRate                       <- map["totalRate"]
    }
}

class VehicleModel: NSObject, Mappable {
    
    var createdAt : String?
    var updatedAt : String?
    var id : String?
    var vehicleid : Int?
    var city : String?
    var country : String?
    var day : String?
    var endTime : String?
    var model : String?
    var latitude : String?
    var longitude : String?
    var distance : Int = 0
    var perDayCost : Int?
    var perExtraKmCost: Int?
    var perHourCost : Double?
    var perMonthCost : Int?
    var perWeekCost : Int?
    var registrationNumber : String?
    var startTime : String?
    var state : String?
    var station_name : String?
    
    
    var type : Int = 0
    var registerId : String?
    var image : String?
    var numberPlate : String?
    var name : String?
    var color : String?
    var vinNumber : String?
    var qnr : String?
   
    var fuelType : Int = 0
    var telematicBrand : Int = 0
    var gearBox : Int = 0
    var iotProvider : Int = 0
    var isActive : Bool = false
    var fuelDiscount : Double = 0
    var extraFuelCharge : Double = 0
    var maintenanceStatus : Int = 0
    var isDeleted : Bool = false
    var fuelLevel : Double = 0
    var totalKm : Double = 0
    var estimatedKmPerFuel : Double = 0
    var extras : String?
    var geoLocation : LocationModel?
    var isAvailable : Bool = false
    var isRideCompleted : Bool = false
    var status : Int = 0
    var travelBags : Int = 0
    var suitcases : Int = 0
    var seats : Int = 0
    var tyres : String?
    var yearOfProduction : Int = 0
    var deposit : Double = 0
    var markerType : Int = 0
    var addedBy : String?
    var updatedBy : String?
    var currentParkedStationId : String?
    var vehicleGroupId : String?
    var vehicleKey : Int = 0
    var userGroupId : String?
    var partnerId : String?
    var brand : String?
    var category : String?
    var lastUsed : String?
    var iotVehicleId : String?
    var priceConfigId : String?
   
    var multiLanguageData : MultiLanguageDataModel?
    var currency : Int = 0
    var packages = [PackagesModel]()
    var childSeats = [String]()
    
    var otherDescriptions:[OtherInfoModel] = [OtherInfoModel]()
    var images : [String] = [String]()
    var insurance : InsuranceModel?
    var carAttributes : CarAttributesModel?

    var nextBookingDetail : NextBookingDetailModel?
    
    var isInsurance : Bool = true
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
                
        vehicleid                       <- map["VehicleId"]
        city                            <- map["city"]
        country                         <- map["country"]
        day                             <- map["day"]
        distance                        <- map["distance"]
        endTime                         <- map["endTime"]
        extraFuelCharge                 <- map["extraFuelCharge"]
        perDayCost                      <- map["perDayCost"]
        perExtraKmCost                  <- map["perExtraKmCost"]
        perHourCost                     <- map["perHourCost"]
        perMonthCost                    <- map["perMonthCost"]
        perWeekCost                     <- map["perWeekCost"]
        latitude                        <- map["latitude"]
        longitude                       <- map["longitude"]
        registrationNumber              <- map["registrationNumber"]
        startTime                       <- map["startTime"]
        state                           <- map["state"]
        station_name                    <- map["station_name"]
        
        
        createdAt                       <- map["createdAt"]
        updatedAt                       <- map["updatedAt"]
        isActive                        <- map["isActive"]
        type                            <- map["type"]
        geoLocation                     <- map["geoLocation"]
        markerType                      <- map["markerType"]
        registerId                      <- map["registerId"]
        image                           <- map["image"]
        model                           <- map["model"]
        name                            <- map["name"]
        color                           <- map["color"]
        vinNumber                       <- map["vinNumber"]
        qnr                             <- map["qnr"]
       
        fuelType                        <- map["fuelType"]
        telematicBrand                  <- map["telematicBrand"]
        gearBox                         <- map["gearBox"]
        iotProvider                     <- map["iotProvider"]
        maintenanceStatus               <- map["maintenanceStatus"]
        isDeleted                       <- map["isDeleted"]
        fuelLevel                       <- map["fuelLevel"]
        totalKm                         <- map["totalKm"]
        estimatedKmPerFuel              <- map["estimatedKmPerFuel"]
        extras                          <- map["extras"]
        isAvailable                     <- map["isAvailable"]
        isRideCompleted                 <- map["isRideCompleted"]
        status                          <- map["status"]
        travelBags                      <- map["travelBags"]
        suitcases                      <- map["suitcases"]
        seats                           <- map["seats"]
        tyres                           <- map["tyres"]
        yearOfProduction                <- map["yearOfProduction"]
        deposit                         <- map["deposit"]
        markerType                      <- map["markerType"]
        addedBy                         <- map["addedBy"]
        updatedBy                       <- map["updatedBy"]
        currentParkedStationId          <- map["currentParkedStationId"]
        vehicleGroupId                  <- map["vehicleGroupId"]
        userGroupId                     <- map["userGroupId"]
        partnerId                       <- map["partnerId"]
        brand                           <- map["brand"]
        category                        <- map["category"]
        lastUsed                        <- map["lastUsed"]
        iotVehicleId                    <- map["iotVehicleId"]
        priceConfigId                   <- map["priceConfigId"]
        vehicleKey                      <- map["vehicleKey"]
        fuelDiscount                    <- map["fuelDiscount"]
        
        
        multiLanguageData               <- map["multiLanguageData"]
        currency                        <- map["currency"]
        otherDescriptions               <- map["otherDescriptions"]
        images                          <- map["images"]
        insurance                       <- map["insurance"]
        carAttributes                   <- map["carAttributes"]
        packages                        <- map["packages"]
        childSeats                      <- map["childSeats"]
        nextBookingDetail               <- map["nextBookingDetail"]
        
//        packages.first?.isSelected = true
    }

    
   /* object VehicleType {
        const val CAR = 1
        const val VAN = 2
        const val TRAILER = 3
        fun getNameFromType(type: Int): String {
            return when (type) {
                CAR -> MyApp.getInstance().getString(R.string.lbl_car)
                VAN -> MyApp.getInstance().getString(R.string.lbl_van)
                TRAILER -> ""
                else -> ""
            }
        }
     } */
    
    func getNameFromType() -> String {
        switch type {
        case 1: //Car
            return "Car"
        case 2: //Car
            return "Van"
        case 3: //Car
            return ""
        default:
            return ""
        }
    }

    
    func getCarProperties(isLoadAll: Bool = false) -> [CellModel] {

        var arrcarFeatures = [CellModel]()
        
        //Add Car Category
        let master = SyncManager.sharedInstance.fetchMasterWithId(category ?? "")
        
        if master.count > 0{
            arrcarFeatures.append(CellModel.getModel(placeholder: "\(master.first!.name ?? "") \(getNameFromType())", type: .CFFamilyCar, imageName: categoryImage, cellObj: nil, inSequence: 0))
        }
        
        //Add Seats
        if seats != 0 {
            arrcarFeatures.append(CellModel.getModel(placeholder: "\(seats) \("KSeats".localized)", type: .CFFiveSeat, imageName: "seat", cellObj: nil, inSequence: isLoadAll ? 1 : 2))
        }

        // Add Winter Tyres
        if carAttributes?.winterTyre == true {
            arrcarFeatures.append(CellModel.getModel(placeholder: "\("KLblWinterTyre".localized)", type: .CFWinterTires, imageName: "tyre-1", cellObj: nil,inSequence: isLoadAll ? 2 : 3))
        }

        // Add Transmission Type
        arrcarFeatures.append(CellModel.getModel(placeholder: gearBox == AppConstants.GearBoxType.AUTOMATIC ? "\("KAutoGear".localized)" : "\("KManualGear".localized)", type: .CFAutomaticTransmission, imageName: gearBox == AppConstants.GearBoxType.AUTOMATIC ? "AutoGear" : "ManualGear", cellObj: nil, inSequence: isLoadAll ? 4 : 1))

        //Load all car attributes in this arraylist (Used For details and previous rental screens)
        //or if there are not enough items to show on pager then add these items

        if isLoadAll || arrcarFeatures.count < 4 {
        
            //Add Suitecases
            if suitcases != 0 {
                arrcarFeatures.append(CellModel.getModel(placeholder: "\(suitcases) \("KSuitcases".localized)", type: .CFThreeSuitcase, imageName: "Briefcase", cellObj: nil, inSequence:  5))
            }

            //Add Car key
            arrcarFeatures.append(CellModel.getModel(placeholder: iotProvider == AppConstants.IOTProvider.WithTelemetric ? "KLblApp".localized : "KManual".localized, type: .CFCarKey, imageName: iotProvider == AppConstants.IOTProvider.WithTelemetric ? "App" : "car-key", cellObj: nil, inSequence:  6))

            //Add Loading Facility
            if carAttributes?.throughLoadingFacility == true {
                arrcarFeatures.append(CellModel.getModel(placeholder: "KLblThroughLoading".localized, type: .CFLoadingFacility, imageName: "ThroughLoading", cellObj: nil, inSequence:  7))
            }
            
            //Add Wheel Facility
            if carAttributes?.wheelDrive == true {
                arrcarFeatures.append(CellModel.getModel(placeholder: "KLblAllWheel".localized, type: .CFAllWheel, imageName: "AllWheel", cellObj: nil, inSequence:  8))
            }

            //Add Cargo Facility
            if carAttributes?.cargo == true {
                arrcarFeatures.append(CellModel.getModel(placeholder: "KLblCargo".localized, type: .CFCargo, imageName: "Cargo", cellObj: nil, inSequence:  9))
            }

            //Add Roof Rack
           // if carAttributes?.roofRack == true {
                arrcarFeatures.append(CellModel.getModel(placeholder: "KLblRoofRack".localized, type: .CFRoofRack, imageName: "RoofRack", cellObj: nil, inSequence:  10))
           // }
            
            //Add Tow Hitch
            if carAttributes?.towHitch == true {
                arrcarFeatures.append(CellModel.getModel(placeholder: "KLblTowHitch".localized, type: .CFTowHitch, imageName: "TowHitch", cellObj: nil, inSequence:  11))
            }

//            let extra = arrcarFeatures.count - ((arrcarFeatures.count / 3) * 3)
//            if (extra > 0) {
//                var requiredToFill = 3 - extra
//                for i in 0..<requiredToFill {
////              if carAttributes?.towHitch == true {
//                  arrcarFeatures.append(CellModel.getModel(placeholder: "KLblTowHitch".localized, type: .CFTowHitch, imageName: "TowHitch", cellObj: nil, inSequence:  13))
////              }
//
//            }
//            }


        }

        return arrcarFeatures.sorted(by: { $0.inSequence < $1.inSequence })

    }
    


    public var getCarName: String {
        get {
            if name != "" {
                return "\(name ?? "") \(yearOfProduction)".capitalized
            }
            return ""
        }
    }
    
    public var getVehicleImage : URL?{
        get {
            if images.count > 0{
                let uri = (AppConstants.imageURL + (images.first ?? "")).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                
                return URL(string: uri ?? "")
            }
            
            let uri = (AppConstants.imageURL + (image ?? "")).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            
            return URL(string: uri ?? "")
        }
    }
    
    public var getVehicleCategory : String{
        get {
            let master = SyncManager.sharedInstance.fetchMasterWithId(category ?? "")
            
            if master.count > 0{
                return master.first!.name ?? ""
            }
            return ""
        }
    }
    
    public var fuelLevelStr : String? {
        get {
            return String(format: "%.f%%",fuelLevel)
        }
    }
    
    // lazy variables for display with some formates
    public var brandName : String? {
        get {
            
            let arrBrands = SyncManager.sharedInstance.fetchMasterWithId(brand ?? "" )

            if arrBrands.first?.name != nil{
                return "\(arrBrands.first?.name ?? "") - \(model ?? "")"
            }
            return name ?? nil
        }
    }
    
    public var categoryName : String? {
        get {
            
            let arrCategory = SyncManager.sharedInstance.fetchMasterWithId(category ?? "" )
            return arrCategory.first?.name
        }
    }
    
    public var categoryImage : String? {
        get {
            
            let arrCategory = SyncManager.sharedInstance.fetchMasterWithId(category ?? "" )
            return arrCategory.first?.image
        }
    }

    
    public var getPricingPackage : (String?, String?) {
        get {
            
            let filter = packages.filter { (model) -> Bool in
                if model.hours == AppConstants.GetPricePackageConfigHour{
                    return true
                }
                
                return false
            }
            
            if filter.count  > 0 {
                return (Utilities.convertToCurrency(number: filter.first?.rate ?? 0, currencyCode: filter.first?.currency) ,"\(String( filter.first?.getFormattedUnit ?? ""))")
            }else{
                
                return (Utilities.convertToCurrency(number: packages.first?.rate ?? 0, currencyCode: packages.first?.currency ) ,"\(String( packages.first?.getFormattedUnit ?? ""))")
                
            }
        }
    }
    
    public var getChildSeatGroups : String{
        
        get{
        
            var arrSeats : [String] = [String]()
            
            for str in childSeats{
                arrSeats.append(SyncManager.sharedInstance.fetchMasterWithId(str).first?.name ?? "")
            }
            
            return arrSeats.joined(separator: ", ")
        }
    }
    
    public var nextBookingAvailabelTime : String{
        
        get{
            
            if nextBookingDetail != nil{
                
                let date = DateUtilities.convertToISOFormat(dateStr: Date())
                let startDate = DateUtilities.convertDateFromStringWithFromat(dateStr: date, format: DateUtilities.DateFormates.kMainSourceFormat)
                let endDate = DateUtilities.convertDateFromStringWithFromat(dateStr: nextBookingDetail?.reservedDateTime ?? "", format: DateUtilities.DateFormates.kMainSourceFormat)
                let distanceBetweenDates: TimeInterval? = endDate.timeIntervalSince(startDate)
                let secondsInAnHour: Double = 3600

                let hoursBetweenDates = Int((distanceBetweenDates! / secondsInAnHour))

                if hoursBetweenDates < AppConstants.NextBookingTotalHours && hoursBetweenDates > 0 {
                    return String(format : "\("KLblCarAvailableTime".localized)", hoursBetweenDates)
                }else{
                    return String(format : "\("KLblCarAvailableTime".localized)", AppConstants.NextBookingTotalHours)
                }
            }else{
                return String(format : "\("KLblCarAvailableTime".localized)", AppConstants.NextBookingTotalHours)
            }
        }
    }
    
    func getDict() -> [String:Any]{
        
        var req = [String:Any]()
        req["id"] = id
        req["name"] = name
        req["category"] = category
        req["seats"] = seats
        req["fuelLevel"] = fuelLevel
        req["gearBox"] = gearBox
        req["travelBags"] = travelBags
        req["suitcases"] = suitcases
        req["fuelType"] = fuelType
        req["numberPlate"] = numberPlate
        req["image"] = image
        req["images"] = images
        req["deposit"] = deposit
        req["partnerId"] = partnerId
        req["brand"] = brand
        
        return req
    }
}


class OtherInfoModel: NSObject, Mappable {
    
    var id : String?
    var desc : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id                          <- map["id"]
        desc                        <- map["description"]
    }
}


class HourlyModel :  NSObject, Mappable {
    
    var id : String?
    var name : String?
    var desc : String?
    var hours : Int = 0
    var rate : Double = 0.0
    var extraHourCharge: Double = 0
    var extraHours: Double = 0
        
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        
        id                  <- map["id"]
        name                <- map["name"]
        desc                <- map["description"]
        hours               <- map["hours"]
        rate                <- map["rate"]
        extraHourCharge     <- map["extraHourCharge"]
        extraHours          <- map["extraHours"]

    }
}
class PackagesModel :  NSObject, Mappable {
    
    var id : String?
    var name : String?
    var packageDescription : String?
    var reservationTimeString: String?
    var desc : String?
    var reservationTime : String?

    var hours : Int = 0
    var type : Int = 0
    var currency : Int = 0
    var extraHourCharge : Int = 0
    var extraHours : Int = 0
    var extraMinutes: Int?
    
    var isSelected : Bool = false
    var isHide : Bool = false
    
    var mValueselected : Bool = false
    var actualHours: Double = 0
    var actualDay: Double = 0
    var calculatedRate: Double = 0
    var perHourCharge: Double = 0
    var rate : Double = 0.0

    var additionalInfo : AdditionalInfoModel?
    var hourly : HourlyModel?

    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        
        id                      <- map["id"]
        name                    <- map["name"]
        packageDescription      <- map["packageDescription"]
        reservationTimeString   <- map["reservationTimeString"]
        desc                    <- map["description"]
        reservationTime         <- map["reservationTimeString"]
        hours                   <- map["hours"]
        rate                    <- map["rate"]
        type                    <- map["type"]
        isSelected              <- map["isSelected"]
        isHide                  <- map["isHide"]
        additionalInfo          <- map["additionalInfo"]
        actualHours             <- map["actualHours"]
        actualDay               <- map["actualDay"]
        calculatedRate          <- map["calculatedRate"]
        perHourCharge           <- map["perHourCharge"]
        hourly                  <- map["hourly"]
        currency                <- map["currency"]
        extraHourCharge         <- map["extraHourCharge"]
        extraHours              <- map["extraHours"]
        extraMinutes            <- map["extraMinutes"]
        
    }
    
    public var getFormattedUnit : String {
        get{
            
            let days = hours / AppConstants.GetPricePackageConfigHour
            
            if hours == 1{
                return "/\("KLblhour".localized)"
            }else if hours > 1 && hours < AppConstants.GetPricePackageConfigHour{
                return "/\(hours) \("KLblHours".localized)"
            }else if hours == AppConstants.GetPricePackageConfigHour {
                return "/\("KLblday".localized)"
            }else if days == AppConstants.GetPricePackageConfigWeek {
                return "/\("KLblWeek".localized)"
            }else if days == AppConstants.GetPricePackageConfigMonths{
                return "/\("KLblmonth".localized)"
            }else{
                return "/\(days) \("KLblDays".localized)"
            }
        }
    }
    
    func getDict() -> [String:Any]{
        
        var req : [String:Any] = [:]
        
        req["name"] = self.name
        req["description"] = self.desc
        req["reservationTimeString"] = self.reservationTime
        req["hours"] = self.hours
        req["rate"] = self.rate
        req["type"] = self.type
        req["isSelected"] = self.isSelected
        req["actualHours"] = self.actualHours
        req["actualDay"] = self.actualDay
        req["calculatedRate"] = self.calculatedRate
        
        var additionalInfo : [String:Any] = [:]
        additionalInfo["parkingRate"] = self.additionalInfo?.parkingRate
        additionalInfo["distanceUnit"] = self.additionalInfo?.distanceUnit
        additionalInfo["distanceCap"] = self.additionalInfo?.distanceCap
        additionalInfo["additionalDistanceRate"] = self.additionalInfo?.additionalDistanceRate
        additionalInfo["shortTripDistanceRate"] = self.additionalInfo?.shortTripDistanceRate
        additionalInfo["currency"] = self.additionalInfo?.currency
        
        req["additionalInfo"] = additionalInfo
        
        return req

    }
}

class AdditionalInfoModel : NSObject, Mappable {
    
    var parkingRate : Double = 0.0
    var distanceUnit : Int = 0
    var distanceCap : Int = 0
    var additionalDistanceRate : Double = 0.0
    var shortTripDistanceRate : Double = 0.0
    var currency : Int = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        parkingRate                 <- map["parkingRate"]
        distanceUnit                <- map["distanceUnit"]
        distanceCap                 <- map["distanceCap"]
        additionalDistanceRate      <- map["additionalDistanceRate"]
        shortTripDistanceRate       <- map["shortTripDistanceRate"]
        currency                    <- map["currency"]
    }
}

class MultiLanguageDataModel: NSObject, Mappable {
    
    var en_US : EnUSModel?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        en_US <- map["en-US"]
    }
}


class EnUSModel: NSObject, Mappable {
    
    var name : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        name <- map["name"]
    }
}

class NavigateLocation: NSObject, Mappable {
    
    var coordinates: [Double]?
    var name: String?
    var type: String?

    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        name <- map["name"]
        type <- map["type"]
        coordinates <- map["coordinates"]

    }
}


class NextBookingDetailModel : NSObject, Mappable {
    
    var id : String?
    var reservedDateTime : String?
    var reservedEndDateTime : String?
    var vehicleId : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id                      <- map["id"]
        reservedDateTime        <- map["reservedDateTime"]
        reservedEndDateTime     <- map["reservedEndDateTime"]
        vehicleId               <- map["vehicleId"]
    }
}

class InsuranceModel : NSObject, Mappable {
    
    var insuranceAmount : Double = 0
    var insuranceFrom : Double = 0
    var insuranceTo : Double = 0
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        insuranceAmount     <- map["insuranceAmount"]
        insuranceFrom       <- map["insuranceFrom"]
        insuranceTo         <- map["insuranceTo"]
    }
}

class CarAttributesModel : NSObject, Mappable {
    
    var cargo : Bool = false
    var diesel : Bool = false
    var gasoline : Bool = false
    var throughLoadingFacility : Bool = false
    var towHitch : Bool = false
    var wheelDrive : Bool = false
    var winterTyre : Bool = false
    var roofRack : Bool = false

    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        cargo                           <- map["cargo"]
        diesel                          <- map["diesel"]
        gasoline                        <- map["gasoline"]
        throughLoadingFacility          <- map["throughLoadingFacility"]
        towHitch                        <- map["towHitch"]
        wheelDrive                      <- map["wheelDrive"]
        winterTyre                      <- map["winterTyre"]
        roofRack                        <- map["roofRack"]

    }
}


//Geo Model
class LocationModel: Object, Mappable {
    
    @objc dynamic var name : String?
    @objc dynamic var street : String?
    @objc dynamic var geoType : String?
    @objc dynamic var latitude : Double = 0
    @objc dynamic var longitude : Double = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override class func primaryKey() -> String? {
        return nil
    }

    func mapping(map: Map) {
        
        name                            <- map["name"]
        street                          <- map["street"]
        geoType                         <- map["type"]
        
        var arrCoo = [Double]()
        
        arrCoo <- map["coordinates"]
        
        longitude = arrCoo.first ?? 0.0
        latitude = arrCoo.last ?? 0.0
        
    }
}



