//
//  SettingModel.swift
//  E-Scooter


import UIKit
import Realm
import RealmSwift
import ObjectMapper_Realm
import ObjectMapper

class SettingModel: Object, Mappable {
    
    @objc dynamic var id : String?
    @objc dynamic var createdAt : String?
    @objc dynamic var updatedAt : String?
    @objc dynamic var addedBy : String?
    @objc dynamic var updatedBy : String?
    
    @objc dynamic var type : Double = 0
    @objc dynamic var basicRadius : Double = 0
    @objc dynamic var zoomLevel : Double = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        
        id                              <- map["id"]
        createdAt                       <- map["createdAt"]
        updatedAt                       <- map["updatedAt"]
        addedBy                         <- map["addedBy"]
        updatedBy                       <- map["updatedBy"]
 
        type                            <- map["type"]
        basicRadius                     <- map["basicRadius"]
        zoomLevel                       <- map["zoomLevel"]
    }
}
