//
//  PreviousRentalsModel.swift
//  WeOne
//
//  Created by Brijesh on 10/04/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit
import ObjectMapper

class DriveNowModel: NSObject, Mappable {
    
    var vehicleDetails : VehicleModel?
    var previousRentals:[PreviousRentalsModel] = [PreviousRentalsModel]()
    var lastDamageReport:[DamageReportModel] = [DamageReportModel]()
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        vehicleDetails                       <- map["vehicleDetails"]
        previousRentals                      <- map["previousRentals"]
        lastDamageReport                     <- map["lastDamageReport"]
    }
}

class PreviousRentalsModel: NSObject, Mappable {
    
    var id : String?
    var afterOdometer : String?
    var comment : String?
    
    var startKm : Int = 0
    var endKm : Int = 0
    var endFuelLevel : Double = 0.0
    var totalKm : Int = 0
    var afterAttachment: [ImagesModel] = [ImagesModel]()
    
    var isSelected : Bool = false
    var isExpand : Bool = false
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id                              <- map["id"]
        afterOdometer                   <- map["afterOdometer"]
        comment                         <- map["comment"]
        
        startKm                         <- map["startKm"]
        endKm                           <- map["endKm"]
        endFuelLevel                    <- map["endFuelLevel"]
        totalKm                         <- map["totalKm"]
        
        afterAttachment                 <- map["afterAttachment"]
        
    }
    
    public var fuelLevelStr : String? {
           get {
               return String(format: "%.f%%",endFuelLevel)
           }
       }
}

class ImagesModel: NSObject, Mappable {
    
    var path: String?
    var type: Int = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        path                       <- map["path"]
        type                      <- map["type"]

    }
}

class CityModel: NSObject, Mappable {
    
    var id : String?
    var parentID : String?
    var addedBy : String?

    var createdAt : String?
    var updatedAt : String?
    var updatedBy : String?

    var name : String?
    var baseName : String?
    
    var parentLocalID : String?
    var localID : String?

    var type: Int = 0
    var localIDSequence: Int = 0
    var sequence: Int = 0
    var markerType: Int = 0
    var geoLocation : LocationModel?

    
    var isActive : Bool = false
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id                              <- map["id"]
        parentID                        <- map["parentID"]
        addedBy                         <- map["addedBy"]
        createdAt                       <- map["createdAt"]
        updatedAt                       <- map["updatedAt"]
        updatedBy                       <- map["updatedBy"]
        name                            <- map["name"]
        baseName                            <- map["baseName"]
        parentLocalID                   <- map["parentLocalID"]
        localID                         <- map["localID"]
        type                            <- map["type"]
        localIDSequence                 <- map["localIDSequence"]
        sequence                        <- map["sequence"]
        markerType                      <- map["markerType"]
        isActive                        <- map["isActive"]
        geoLocation                     <- map["geoLocation"]

        
    }
}
