//
//  UserInfoModel.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved

import UIKit
import ObjectMapper
import RealmSwift
import Realm
import ObjectMapper_Realm

class UserInfoModel: NSObject, Codable, Mappable {
    
    public var id : Int?
    public var firstName : String?
    public var lastName : String?
    public var name : String?
    public var email : String?
    public var code : String?
    public var mobile : String?
    public var profilePic : String?
    public var isVerified : Bool?
    public var recieveNews : Bool?
    public var businessName : String?
    public var businessRegistrationNo : String?
    public var businessAddress : String?
    
    public var createdAt : String?
    public var type : Int?
    public var walletAmount : Double?

    public var dob : String?
    public var isDocumentVerify : Bool?
    public var isOnfidoRequest : Bool?
    public var isWalletPaymentEnable : Bool?

    public var image : String?
//    public var rideSummary : RideSummary?
//    public var gdprCompliance : GdprCompliance?
    public var selectedCard : String?
//    public var mobiles: [MobileModel] = [MobileModel]()
//    public var emails: [MobileModel] = [MobileModel]()
        
    //get Full Name
    public var fullName : String? {
        return "\(firstName ?? "") \(lastName ?? "")"
    }
    
    //Primary Email
//    public var primaryEmail : MobileModel? {
//
//        let filter = emails.filter{($0.isPrimary ?? false)}
//        if filter.count > 0 {
//            return filter.first
//        }
//        return emails.first
//    }
    
    //Primary Mobile
//    public var primaryMobile : MobileModel? {
//
//        let filter = mobiles.filter{($0.isPrimary ?? false)}
//        if filter.count > 0 {
//            return filter.first
//        }
//        return mobiles.first
//    }
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        createdAt                        <- map["createdAt"]
        firstName                        <- map["firstName"]
        id                               <- map["id"]
        lastName                         <- map["lastName"]
        name                             <- map["name"]
        dob                              <- map["dob"]
        type                             <- map["type"]
        walletAmount                     <- map["walletAmount"]
        isDocumentVerify                 <- map["isDocumentVerify"]
        isOnfidoRequest                  <- map["isOnfidoRequest"]
        isWalletPaymentEnable            <- map["isWalletPaymentEnable"]
        image                            <- map["image"]
//        rideSummary                      <- map["rideSummary"]
//        gdprCompliance                   <- map["gdprCompliance"]
        selectedCard                     <- map["selectedCard"]
//        mobiles                          <- map["mobiles"]
//        emails                           <- map["emails"]
        
        email                            <- map["email"]
        code                             <- map["countryCode"]
        mobile                           <- map["mobile"]
        profilePic                       <- map["profilePic"]
        isVerified                       <- map["isVerified"]
        recieveNews                      <- map["recieveNews"]
        businessName                     <- map["businessName"]
        businessRegistrationNo           <- map["businessRegistrationNo"]
        businessAddress                  <- map["businessAddress"]
        
        //        packages.first?.isSelected     = true
    }
    
}


class ReviewModel:  Codable, Mappable {
    
    public var id : String?
    public var name : String?
    public var isSelected : Bool?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id                         <- map["id"]
        name                       <- map["name"]
        isSelected                 <- map["isSelected"]
    }
}

class MobileModel : Codable, Mappable{
    
    public var id : String?
    public var mobile : String?
    public var countryCode : String?
    public var isPrimary : Bool?
    public var isVerified : Bool?
    public var email : String?
    public var lastVerifiedAt : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id                      <- map["id"]
        mobile                  <- map["mobile"]
        countryCode             <- map["countryCode"]
        isPrimary               <- map["isPrimary"]
        isVerified              <- map["isVerified"]
        email                   <- map["email"]
        lastVerifiedAt          <- map["lastVerifiedAt"]
        
    }
}

class RideSummary : Codable{
    public var completed : Int = 0
    public var distance : Double = 0
}

class GdprCompliance : Codable{
    public var isShareEmail : Bool?
    public var isUseForAnalysis : Bool?
    public var isNotContactViaEmail : Bool?
    public var isNoAddInNewsLetter : Bool?
    public var isShareLocation : Bool?

    func getDict() -> [String:Any] {
        
        var req = [String:Any]()
        req["isShareEmail"] = isShareEmail
        req["isUseForAnalysis"] = isUseForAnalysis
        req["isNotContactViaEmail"] = isNotContactViaEmail
        req["isNoAddInNewsLetter"] = isNoAddInNewsLetter
        req["isShareLocation"] = isShareLocation

        var dict = [String:Any]()
        dict["gdprCompliance"] = req
        
        return dict
    }
}
