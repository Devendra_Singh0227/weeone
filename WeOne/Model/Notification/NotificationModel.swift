//
//  NotificationModel.swift
//  SnapAcleaner
//
//  Created by Coruscate on 24/10/18.
//  Copyright © 2018 Coruscate. All rights reserved.
//

import UIKit
import ObjectMapper

class NotificationModel:NSObject, Mappable{
    
    var id : Int?
    var updatedAt : String?
    var title : String?
    var status : Int = 0
    var isSelected : Bool = false
    var createdAt : String?
    var userId : String?
    var action : Int = 0
    var addedBy : String?
    var updatedBy : String?
    var message:String?
    
    override init() {
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        id                       <- map["id"]
        updatedAt                <- map["updatedAt"]
        title                    <- map["title"]
        status                   <- map["status"]
        createdAt                <- map["createdAt"]
        userId                   <- map["userId"]
        updatedBy                <- map["updatedBy"]
        addedBy                  <- map["addedBy"]
        action                   <- map["action"]
        message                  <- map["description"]
        isSelected               <- map["isSelected"]
        
    }
}

class CardsListModel: NSObject, Mappable {
    var id: Int?
    var cardNo: String?
    var nameonCard : String?
    var exp: String?
    var isDefault : Bool?
    var cardType : String?
    var isPersonal : Bool?
    var isBusiness : Bool?
    
    override init() {
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        
        id                    <- map["id"]
        cardNo                <- map["cardNo"]
        nameonCard            <- map["nameonCard"]
        exp                   <- map["exp"]
        isDefault             <- map["isDefault"]
        cardType              <- map["cardType"]
        isPersonal            <- map["isPersonal"]
        isBusiness            <- map["isBusiness"]
    }
}
