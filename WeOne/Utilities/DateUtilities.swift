//
//  DateUtilities.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved

import UIKit

class DateUtilities: NSObject {
    
    //MARK: Constant
    
    static var formatter: DateFormatter { return DateFormatter() }
    
    struct DateFormates {
        static let kLongDate = "dd MMM yyyy h:mm a"
        static let kLong24Date = "dd MMM yyyy H:mm"
        static let kMainSourceFormat  = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        static let kActualDateFormat  = "yyyy-MM-dd HH:mm:ss"
        static let kCarSchedule  = "EEEE, MMM dd"
        static let kCarScheduleTime  = "h:mm a"
        static let kScheduleTime  = "h:mm"
        static let k24Time  = "HH:mm"

        static let kCarMoreInfoDate  = "EEEE, dd MMM"
        static let kCarMoreInfoTime  = "hh:mm a"
        static let kCarMoreInforDateTime = "EEE dd, MMMM hh:mm a"
        static let kTripHistoryDate  = "EEEE dd, MMMM"
        static let kFareSummaryDate  = "dd MMM, hh:mm a"
        static let kDetailPopup  = "EEE, MMM dd"
        static let kRegularDate  = "dd MMM yyyy"
        static let kRequestSourceFormat  = "yyyy-MM-dd'T'HH:mm:00.000'Z'"
        static let kScheduleFormat = "EEEE, MMM dd 'at' h:mm a"
        static let kRideDateFormat  = "dd MMM yyyy | H:mm"
        static let kVerifiedDateFormat  = "MM/dd/yy."

    }
    
    static func getTimeOfTheDay() -> String {
        let hour = Calendar.current.component(.hour, from: Date())

        switch hour {
        case 6..<12 :
            return("KLblMorning".localized)
        case 12..<17 :
            return("KLblAfternoon".localized)
        case 17..<22 :
            return("KLblEvening".localized)
        default:
            return("KLblNight".localized)
            
        }
    }
    
    static func dateFromTimeStamp(timestamp:Double) -> Date?{
        
        if timestamp > 0.0{
            
            return Date(timeIntervalSince1970: timestamp / 1000)
        }
        return nil
    }
    
    static func convertToDate(dateStr: String) -> Date{
        let dateFormatter = DateFormatter()
        let date = dateFormatter.date(from: dateStr)
        return date ?? Date()
    }
    
    static func convertStringDateintoStringWithFormat(dateStr : String,sourceFormat : String? = DateFormates.kMainSourceFormat, destinationFormat : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = sourceFormat
        let myDate = dateFormatter.date(from: dateStr)
        
        dateFormatter.dateFormat = destinationFormat
        
        if myDate == nil {
            return ""
        }
        
        let somedateString = dateFormatter.string(from: myDate!)
        return somedateString
    }
    
    static func convertDateFromServerString(dateStr: String,sourceFormate : String? = DateFormates.kMainSourceFormat) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = sourceFormate//this your string date format
        let date = dateFormatter.date(from: dateStr)
        return date ?? Date()
    }
    
    static func convertDateFromStringWithFromat(dateStr: String, format : String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format //this your string date format
        let date = dateFormatter.date(from: dateStr)
        return date ?? Date()
    }
    
    static func convertStringFromDate(date: Date, format:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.amSymbol = "am"
        dateFormatter.pmSymbol = "pm"
        return dateFormatter.string(from: date)
    }
    
    static func findDateDiff(date1: Date, date2: Date) -> Int {
        
        //You can directly use from here if you have two dates
        let interval = date2.ignoreSeconds().timeIntervalSince(date1.ignoreSeconds())
        // let hour = interval / 3600; // hours
        return Int((interval / 3600) * 60); // Total Minutes
    }
    
    static func minusHoursFromDate(date : Date,hours : Int) -> Date {

        return Calendar.current.date(byAdding: .hour, value: hours, to: date)!
    }
    
    static func convertToISOFormat(dateStr: Date) -> String {
        let dateFormatter = DateFormatter()
        let calendar = Calendar.init(identifier: .gregorian)
        dateFormatter.calendar = calendar
        let locale: NSLocale? = NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.locale = locale! as Locale
        let timeZone = NSTimeZone(forSecondsFromGMT: 0)
        dateFormatter.timeZone = timeZone as TimeZone
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'00'.'000'Z'"
        let myDateString: String = dateFormatter.string(from: dateStr)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.Z"
        return myDateString
    }
    
    //MARK: Calculate time
    static func timeAgoSinceDate(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }


    static func getHrTime(_ date:Date,currentDate:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }
   
}
extension Date {
    
    // date with add number of days
    func dateBeforeOrAfterFromToday(numberOfDays :Int) -> Date {
        
        let resultDate = Calendar.current.date(byAdding: .day, value: numberOfDays, to: Date())!
        return resultDate
    }
    
    func addHours(_ hours : Int) -> Date{
        let calendar = Calendar.current
        let date = calendar.date(byAdding: .hour, value: hours, to: self)
        return date!
    }
    
    func addMinutes(_ min : Int) -> Date{
        let calendar = Calendar.current
        let date = calendar.date(byAdding: .minute, value: min, to: self)
        return date!
    }
    
    func ignoreSeconds() -> Date {
        let components = Calendar.current.dateComponents([.year, .month, .day, .hour ,.minute], from: self)
        let date = Calendar.current.date(from: components)
        return date!
    }
}
