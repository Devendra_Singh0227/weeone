//
//  Utilities.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved

import UIKit
import MobileCoreServices
import AVKit
import MapKit
import StoreKit

struct PickerMediaType {
    
    static let image = kUTTypeImage as String
    static let video = kUTTypeMovie as String
}

class Utilities: NSObject {
    
    // convert Currency
    class func convertToCurrency(number : Double, currencyCode : Int? = AppConstants.Currency.USD) -> String {
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.currency
        
        if currencyCode == AppConstants.Currency.USD{
            currencyFormatter.currencyCode = AppConstants.USD
            
            currencyFormatter.maximumFractionDigits = 2
            currencyFormatter.minimumFractionDigits = 0
            currencyFormatter.locale = Locale(identifier: "en_US")
            
            return currencyFormatter.string(from: NSNumber(value: number))!
        }else{
            
            currencyFormatter.currencyCode = AppConstants.NOK
            
            return "\(NSNumber(value: number)) NOK"
        }
        
        
    }
    
    func isValidEmail(testStr:String) -> Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //Convert To html
    class func convertToHtml(str : String) -> NSAttributedString{
        
        var attributedString: NSMutableAttributedString? = nil
        if let anEncoding = str.data(using: .unicode) {
            
            attributedString = try? NSMutableAttributedString(data: anEncoding, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            
        }
        
        return attributedString ?? NSAttributedString()
    }
    //MARK: get doouble value
    class func toDoubleString(value : Double, numberOfDigits : Int = 2) -> String{
        
        //let disValue = dis.toDouble()
        let disValue = String(format: "%0.\(numberOfDigits)f",value)
        if disValue != "0.0" && disValue != "0.00"{
            
            return "\(disValue)"
        }
        
        return "0.0"
    }
    
    static func getGoogleMapImageUrl(size:CGSize, startLocation:LocationModel, endLocation:LocationModel) -> String {
        
        //http://maps.google.com/maps/api/staticmap?path=color:0x212121|weight:4|59.8937803,10.6446907|60.1975501,11.0982212&size=970x193&sensor=true&markers=icon:https://api.weone.io/images/master/Group3132-e7601975-215d-4cdc-a47c-65db18df6422.png|59.8937803,10.6446907|60.1975501,11.0982212&key=AIzaSyDMvmnlbLnYfKASTQFWnCbeI-xITmqCkuU
        
        
        
        let strUrl1 = "http://maps.google.com/maps/api/staticmap?path=color:\(self.hexStringFromColor(color: ColorConstants.ThemeColor).replace(target: "#", withString: "0x"))FF|weight:4|\(startLocation.latitude),\(startLocation.longitude)|\(endLocation.latitude),\(endLocation.longitude)&size=\(Int(size.width))x\(Int(size.height))&sensor=true&markers=icon:https://api.weone.io/images/master/Group3132-e7601975-215d-4cdc-a47c-65db18df6422.png".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        
        
        let strUrl3 = "|\(startLocation.latitude),\(startLocation.longitude)|\(endLocation.latitude),\(endLocation.longitude)&key=\(AppConstants.googleApiKeyForImage)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        
        return "\(strUrl1)\(strUrl3)"
        
        //        return "http://maps.google.com/maps/api/staticmap?path=color:0x3C97D3FF|weight:4|21.171021,72.854210|21.267553,72.960861&size=\(Int(size.width))x\(Int(size.height))&sensor=true&markers=size:mid%7Ccolor:0xFF0000FF|21.171021,72.854210|21.267553,72.960861&key=\(AppConstants.googleApiKeyForImage)"
        //            .addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        
    }
    
    
    
    
    //MARK: - Rating
    class func rateUs() {
        
        SKStoreReviewController.requestReview()
    }
    
    //MARK: NavigationBar
    
    static func setNavigationBar(controller : UIViewController,isHidden : Bool, isHideBackButton : Bool = false, title : String? = nil,isStausColor:Bool? = true) {
        
        controller.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
//        let attrs = [
//            NSAttributedString.Key.foregroundColor: UIColor.white,
//            NSAttributedString.Key.font: FontScheme.kRegularFont(size: 18)
//        ]
        
        //        if isStausColor == true{
        
        //            if #available(iOS 13.0, *) {
        //
        //                if let frame = UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame {
        //                    let statusBar1 =  UIView()
        //                    statusBar1.frame = frame
        //                    statusBar1.backgroundColor = UIColor.clear
        //                    UIApplication.shared.keyWindow?.addSubview(statusBar1)
        //                }
        //
        //            } else {
        
        
        //               let statusBar: UIView = UIApplication.shared.statusBarView!
        //               if statusBar.responds(to: #selector(setter: UIView.backgroundColor)){
        //                   statusBar.backgroundColor = UIColor.clear
        //               }
        //            }
        //        }
        //        else{
        //
        //            if #available(iOS 13.0, *) {
        //
        //                if let frame = UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame {
        //                    let statusBar1 =  UIView()
        //                    statusBar1.frame = frame
        //                    statusBar1.backgroundColor = ColorConstants.BgColor
        //                    UIApplication.shared.keyWindow?.addSubview(statusBar1)
        //                }
        //
        //            } else {
        
        //               let statusBar: UIView = UIApplication.shared.statusBarView!
        //               if statusBar.responds(to: #selector(setter: UIView.backgroundColor)){
        //                   statusBar.backgroundColor = ColorConstants.BgColor
        //               }
        //            }
        //        }
        
//        controller.navigationController?.navigationBar.titleTextAttributes = attrs
        controller.navigationController?.isNavigationBarHidden = isHidden
        controller.navigationController?.navigationBar.isHidden = isHidden
        
        
        controller.navigationController?.navigationBar.barTintColor = ColorConstants.ThemeColor
        controller.navigationController?.navigationBar.tintColor = .white
        controller.navigationController?.navigationBar.isTranslucent = false
        
        controller.title = title
    }
    
    class func showValueWithPer(dis : Double) -> String{
        
        let disValue = String(format: "%0.2f",dis)
        if disValue != "0.0" && disValue != "0.00"{
            
            return "\(disValue) %"
        }
        return "-"
    }
    
    class func getDocumentsDirectory() -> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    //MARK: Strings
    static func checkStringEmptyOrNil(str : String?) -> Bool{
        let trimmedStr = str?.replacingOccurrences(of: "\\s+$", with: "", options: .regularExpression)
        if trimmedStr?.isEmpty == nil || trimmedStr?.count == 0{
            return true
        }
        return false
    }
    
    class func getFormattedTime(min: Int) -> String? {
        
        let hours = min / 60
        let minutes = min % 60
        let minuteString = "KLblMinCap".localized

        if (hours > 0) {
            let hourString =  (hours > 1) ? "KLblHrs".localized : "KLblHr".localized
            if (minutes > 0) {
                return "\(hours) \(hourString) \(minutes) \(minuteString)"
            } else {
                return "\(hours) \(hourString)"
            }
        } else {
            return "\(minutes) \(minuteString)"
        }
    }

    //MARK: Show Alert View
    
    class func showAlertView(title: String? = AppConstants.AppName, message: String?) {
        
        UIViewController.current()?.view.showConfirmationPopupWithSingleButton(title: title, message: message ?? "", confirmButtonTitle: StringConstants.ButtonTitles.KOk, onConfirmClick: {
            
        })
    }
    
    class func showAlertWithButtonAction(title: String,message:String,buttonTitle:String,onOKClick: @escaping () -> ()){
        
        DispatchQueue.main.async {
            
            UIViewController.current()?.view.showConfirmationPopupWithSingleButton(title: "", message: message, confirmButtonTitle: buttonTitle, onConfirmClick: {
                
                onOKClick()
            })
        }
    }
    
    class func getIntFromDouble(str: String) -> String{
        let value = str.components(separatedBy: ".")
        let newValue = value[0]
        return newValue
    }
    
    //MARK: Device Unique ID
    class func getDeviceUniqueID() -> String{
        
        return UIDevice.current.identifierForVendor?.uuidString ?? ""
    }
    
    //MARK: Imagepicker controller
    class func open_galley_or_camera(delegate : UIImagePickerControllerDelegate,mediaType:[String] = [kUTTypeImage as String], sender:UIView = UIViewController.current().view){
        
        let actionSheetController: UIAlertController = UIAlertController(title: "KLblChooseOption".localized, message: nil, preferredStyle: .actionSheet)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "KLblCancel".localized, style: .cancel) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        //Create and add first option action
        let takePictureAction: UIAlertAction = UIAlertAction(title: "KMsgCamera".localized, style: .default) { action -> Void in
            
            if(  UIImagePickerController.isSourceTypeAvailable(.camera))
            {
                let myPickerController = UIImagePickerController()
                myPickerController.navigationBar.isTranslucent = false
                myPickerController.navigationBar.barTintColor = ColorConstants.ThemeColor // Background color
                myPickerController.navigationBar.tintColor = UIColor.white // Cancel button ~ any UITabBarButton items
                myPickerController.navigationBar.titleTextAttributes = [
                    NSAttributedString.Key.foregroundColor : UIColor.white
                ]
                
                let BarButtonItemAppearance = UIBarButtonItem.appearance()
                
                BarButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
                BarButtonItemAppearance.setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -200, vertical: 0), for:UIBarMetrics.default)
                myPickerController.delegate = (delegate as! UIImagePickerControllerDelegate & UINavigationControllerDelegate)
                myPickerController.sourceType = .camera
                myPickerController.mediaTypes = mediaType
                UIViewController.current().present(myPickerController, animated: true, completion: nil)
                
            }
            else
            {
                let actionController: UIAlertController = UIAlertController(title: "KMsgInvalidCamera".localized, message: "", preferredStyle: .alert)
                let cancelAction: UIAlertAction = UIAlertAction(title: "KLblOk".localized, style: .cancel) { action -> Void     in
                    //Just dismiss the action sheet
                }
                
                actionController.addAction(cancelAction)
                actionController.popoverPresentationController?.sourceView = sender
                UIViewController.current().present(actionController, animated: true, completion: nil)
                
            }
        }
        actionSheetController.addAction(takePictureAction)
        
        //Create and add a second option action
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "KMsgChooseGallery".localized, style: .default) { action -> Void in
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = (delegate as! UIImagePickerControllerDelegate & UINavigationControllerDelegate);
            myPickerController.sourceType = .photoLibrary
            myPickerController.mediaTypes = mediaType
            
            myPickerController.navigationBar.barTintColor = ColorConstants.ThemeColor // Background color
            myPickerController.navigationBar.tintColor = UIColor.white // Cancel button ~ any UITabBarButton items
            myPickerController.navigationBar.titleTextAttributes = [
                NSAttributedString.Key.foregroundColor : UIColor.white
            ]
            
            let BarButtonItemAppearance = UIBarButtonItem.appearance()
            
            BarButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
            BarButtonItemAppearance.setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -200, vertical: 0), for:UIBarMetrics.default)
            
            UIViewController.current().present(myPickerController, animated: true, completion: nil)
        }
        actionSheetController.addAction(choosePictureAction)
        actionSheetController.popoverPresentationController?.sourceView = sender
        
        //Present the AlertController
        UIViewController.current().present(actionSheetController, animated: true, completion: nil)
        
    }
    
    //MARK: Open gallary
    class func openGallery(_ delegate : UIImagePickerControllerDelegate, mediaType : [String] = [kUTTypeImage as String]) {
        let myPickerController = UIImagePickerController()
        myPickerController.delegate = (delegate as! UIImagePickerControllerDelegate & UINavigationControllerDelegate);
        myPickerController.sourceType = .photoLibrary
        myPickerController.mediaTypes = mediaType
        
        myPickerController.navigationBar.barTintColor = ColorConstants.TextColorPrimary // Background color
        myPickerController.navigationBar.tintColor = UIColor.white // Cancel button ~ any UITabBarButton items
        myPickerController.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
        
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        
        BarButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
        BarButtonItemAppearance.setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -200, vertical: 0), for:UIBarMetrics.default)
        
        UIViewController.current()?.present(myPickerController, animated: true, completion: nil)
    }
    
    //MARK:- open camera
    class func openCamera(_ delegate : UIImagePickerControllerDelegate, mediaType : [String] = [kUTTypeImage as String]) {
        
        if ( UIImagePickerController.isSourceTypeAvailable(.camera)) {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = (delegate as! UIImagePickerControllerDelegate & UINavigationControllerDelegate)
            myPickerController.sourceType = .camera
            myPickerController.mediaTypes = mediaType
            myPickerController.navigationBar.barTintColor = ColorConstants.TextColorPrimary // Background color
            myPickerController.navigationBar.tintColor = UIColor.white // Cancel button ~ any UITabBarButton items
            myPickerController.navigationBar.titleTextAttributes = [
                NSAttributedString.Key.foregroundColor : UIColor.white
            ]
            
            let BarButtonItemAppearance = UIBarButtonItem.appearance()
            
            BarButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
            BarButtonItemAppearance.setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -200, vertical: 0), for:UIBarMetrics.default)
            UIViewController.current().present(myPickerController, animated: true, completion: nil)
        }
        else {
            let actionController: UIAlertController = UIAlertController(title: "KMsgInvalidCamera".localized, message: "", preferredStyle: .alert)
            let cancelAction: UIAlertAction = UIAlertAction(title: "KLblOk".localized, style: .cancel) { action -> Void     in
                //Just dismiss the action sheet
            }
            
            actionController.addAction(cancelAction)
            actionController.popoverPresentationController?.sourceView = UIViewController.current().view
            UIViewController.current().present(actionController, animated: true, completion: nil)
            
        }
    }
    
    //MARK: Change Color
    class func changeStringColor(string : String, array : [String], colorArray : [UIColor],arrFont:[UIFont]) ->  NSAttributedString {
        
        let attrStr = NSMutableAttributedString(string: string)
        let inputLength = attrStr.string.count
        let searchString = array
        
        for i in 0...searchString.count-1
        {
            
            let string  = searchString[i]
            let searchLength = string.count
            var range = NSRange(location: 0, length: attrStr.length)
            
            while (range.location != NSNotFound) {
                range = (attrStr.string as NSString).range(of: string, options: [], range: range)
                if (range.location != NSNotFound) {
                    
                    if colorArray.count >= i{
                        attrStr.addAttribute(NSAttributedString.Key.foregroundColor, value: colorArray[i], range: NSRange(location: range.location, length: searchLength))
                    }
                    
                    if arrFont.count >= i{
                        attrStr.addAttribute(NSAttributedString.Key.font, value: arrFont[i], range: NSRange(location: range.location, length: searchLength))
                    }
                    
                    range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))
                    //                    return attrStr
                }
            }
        }
        
        return attrStr
    }
    
    //MARK: get double value
    class func getDoubleValue<T>(value : T) -> Double{
        if let val = value as? NSNumber{
            return val.doubleValue
        }
        
        if let val = value as? Float{
            return Double(val)
        }
        
        if let val = value as? Double{
            return val
        }
        
        if let val = value as? Int{
            return Double(val)
        }
        
        if let val = value as? String{
            return Double(val) ?? 0
        }
        
        return 0
    }
    
    class func configureExpandableLabel(label: ExpandableLabel) {
//        label.expandedAttributedLink = StringConstants.ExpandableLabelConstant.attributedLessText
//        label.collapsedAttributedLink = StringConstants.ExpandableLabelConstant.attributedMoreText
//        label.font = FontScheme.kRegularFont(size: 14)
        label.shouldCollapse = true
        label.textReplacementType = .word
        label.numberOfLines = 2
    }
    
    //MARK:- Calling number
    class func openURL(_ strUrl : String) {
        if let url = URL(string: strUrl), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    class func toDouble(value : Double) -> String{
        
        //let disValue = dis.toDouble()
        let disValue = String(format: "%0.2f",value)
        if disValue != "0.0" && disValue != "0.00"{
            
            return "\(disValue)"
        }
        
        return "-"
    }
    
    
    class func getCreditCardImage(cardType:String) -> String{
        
        if cardType == CardType.Visa.rawValue {
            return "Visa"
        }
        else if cardType == CardType.AmericanExpress.rawValue {
            return "American Express"
        }
        else if cardType == CardType.JCB.rawValue {
            return "JCB"
        }
        else if cardType == CardType.DinersClub.rawValue {
            return "Diners Club"
        }
        else if cardType == CardType.Discover.rawValue {
            return "Discover"
        }
        else if cardType == CardType.Maestro.rawValue {
            return "Maestro"
        }
        else if cardType == CardType.MasterCard.rawValue {
            return "MasterCard"
        }
        else if cardType == CardType.UnionPay.rawValue {
            return "UnionPay"
        }
        else {
            return "defaultCard"
        }
    }
    
    class func getTitleForDamageReport(type:Int) -> String {
        switch type {
            
        case AppConstants.DamageCategory.SCRATCH:
            return "KLblScratch".localized
            
        case AppConstants.DamageCategory.DENT:
            return "KLblDent".localized
            
        case AppConstants.DamageCategory.CHIP:
            return "KLblChip".localized
            
        case AppConstants.DamageCategory.MISSING_PART:
            return "KLblMissingPart".localized
            
        case AppConstants.DamageCategory.BROKEN_PART:
            return "KLblBrokenPart".localized
            
        case AppConstants.DamageCategory.INTERIOR:
            return "KLblInterior".localized
            
        default:
            return ""
        }
        
    }
    class func getGearTypeName(gearBoxType:Int) -> String {
        if gearBoxType == AppConstants.GearBoxType.AUTOMATIC {
            return  "KAutomatic".localized
        }
        else if gearBoxType == AppConstants.GearBoxType.MANUAL {
            return "KManual".localized
        }
        return ""
    }
    
    
    
    class func getCaptureDevice() -> AVCaptureDevice? {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video)
            else {return nil }
        return device
    }
    
    class func toggleTorch(_ value: String) {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video)
            else {return}
        
        if device.hasTorch {
            do {
                try device.lockForConfiguration()
                
                if value == "On" {
                    device.torchMode = .on
                } else if value == "Off" {
                    device.torchMode = .off
                } else {
                    device.torchMode = .auto
                }
                
                device.unlockForConfiguration()
            } catch {
                print("Torch could not be used")
            }
        } else {
            print("Torch is not available")
        }
    }
    
    class func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    //Open apple map
    class func openMap(latitude:Double, longitude:Double) {
        
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            UIApplication.shared.open(URL(string:"comgooglemaps://?saddr=&daddr=\(longitude),\(latitude)&directionsmode=driving")!, options: [:], completionHandler: nil)
        }
        else {
            let str = "https://www.google.co.in/maps/dir/?saddr=&daddr=\(longitude),\(latitude)&directionsmode=driving"
            Utilities.openUrl(strUrl: str)
        }
    }
    
    class func openUrl(strUrl:String){
        let url = URL(string: strUrl)
        if UIApplication.shared.canOpenURL(url!)
        {
            UIApplication.shared.open(url!, options:[:], completionHandler: nil)
        }
    }
    
    class func openStaticPage(title:String, url:String) {
        
        if !Utilities.checkStringEmptyOrNil(str: url) {
            let vc = StaticPageVC()
            vc.strTitle = title
            vc.strUrl = url
            UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            Utilities.showAlertView(message: "KNoUrlFound".localized)
        }
    }
    
    class func getRoutesBetweenCoordinates(_ origin: CLLocation, _ destination : CLLocation, _ completion: @escaping (_ repsonse: [String:Any]?) -> ()){
        
        let directionURL = "https://maps.googleapis.com/maps/api/directions/json?" +
            "origin=\(origin.coordinate.latitude),\(origin.coordinate.longitude)&destination=\(destination.coordinate.latitude),\(destination.coordinate.longitude)&mode=driving&" +
        "key=\(AppConstants.googleApiKey)"
        
        Alamofire.request(directionURL).responseJSON { response in
            print("Polyline response (response)")
            if let dict = response.result.value as? [String:Any] {
                completion(dict)
            } else {
                completion(nil)
            }
        }
    }
    
    class func drawPathBetweenCoordinates(_ origin: CLLocation, _ stopLocation : CLLocation, _ destination : CLLocation, _ completion: @escaping (_ repsonse: [String:Any]?) -> ()){
        
        let directionURL = "https://maps.googleapis.com/maps/api/directions/json?" +
            "origin=\(origin.coordinate.latitude),\(origin.coordinate.longitude)&destination=\(destination.coordinate.latitude),\(destination.coordinate.longitude)&waypoints=\(stopLocation.coordinate.latitude),\(stopLocation.coordinate.longitude)&mode=driving&" +
        "key=\(AppConstants.googleApiKey)"
        
        Alamofire.request(directionURL).responseJSON { response in
            print("Polyline response (response)")
            if let dict = response.result.value as? [String:Any] {
                completion(dict)
            } else {
                completion(nil)
            }
        }
    }
    
    class func readJsonFile(fileName : String) -> [[String:Any]]{
        
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let object = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                if let arr = object as? [[String: Any]] {
                    return arr
                }
            } catch {
                print("Error!! Unable to parse  \(fileName).json")
            }
        }
        return [[:]]
    }
    
    class func call(phoneNumber: String) {
        let trimPhone = phoneNumber.replacingOccurrences(of: " ", with: "")
        if let url = URL(string: "tel://\(trimPhone)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else {
            Utilities.showAlertView(title: AppConstants.AppName, message: "KMsgCallNotSupport".localized)
        }
    }
    
    //MARK: Open mail
    class func openEmail(email: String)
    {
        if let url = URL(string: "mailto:\(email)"), UIApplication.shared.canOpenURL(url)   {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        else {
            Utilities.showAlertView(message: "KMsgMailError".localized)
        }
    }
    
    class func returnPartName(_ str : String) -> String{
        switch str{
            
        case "SFR" :
            return "Front screen RIGHT"
            
        case "SFL" :
            return "SFLFront screen LEFT"
            
        case "SRR":
            return "Rear screen RIGHT"
            
        case "SRL":
            return "Rear screen LEFT"
            
        case "TFL":
            return "Tires / rims LEFT.FRONT"
            
        case "TFR":
            return "Tires / rims RIGHT. FRONT"
            
        case "TBR":
            return "Tires / rims RIGHT. BACK"
            
        case "TBL":
            return "Tires / rims LEFT. BACK"
            
        case "HL":
            return "Headlight LEFT"
            
        case "HR":
            return "Headlight RIGHT"
            
        case "BR":
            return "Backlight RIGHT"
            
        case "BL":
            return "Backlight LEFT"
            
        case "DRL":
            return "Door rear LEFT"
            
        case "DFL":
            return "Door front LEFT"
            
        case "DFR":
            return "Door Front RIGHT"
            
        case "DRR":
            return "Door rear RIGHT"
            
        case "CR":
            return "Channel RIGHT"
            
        case "CL":
            return "Channel LEFT"
            
        case "BMPF":
            return "Front bumper"
            
        case "BMPR":
            return "Rear bumper"
            
        case "PLB":
            return "Back plate"
            
        case "PLF":
            return "Front plate"
            
        case "RPR":
            return "Roof Plates RIGHT"
            
        case "ROOF":
            return "Roof"
            
        case "RPL":
            return "Roof Plates LEFT"
            
        case "FHOOD":
            return "Front Hood"
            
        case "HBACK":
            return "Hatchback – Back Hood"
            
        case "GRILL":
            return "Front Grill"
            
        case "RVML":
            return "Rear view mirror left"
            
        case "RVMR":
            return "Rear view mirror right"
            
        case "BWSHIELD":
            return "Back glass windshield"
            
        case "FWSHIELD":
            return "Front glass windshield"
            
        default:
            return ""
        }
        
    }
    
    class func returnDamageCaterory(_ arr : [Int]) -> String{
        
        var arrValues : [String] = [String]()
        
        if arr.contains(AppConstants.ReportDamage.DIRTY_INTERIOR){
            arrValues.append("KLblDirtyInterior".localized)
        }
        
        if arr.contains(AppConstants.ReportDamage.DAMAGE){
            arrValues.append("KLblDamaged".localized)
        }
        
        if arr.contains(AppConstants.ReportDamage.DIRTY_EXTERIOR){
            arrValues.append("KLblDirtyExterior".localized)
        }
        
        if arr.contains(AppConstants.ReportDamage.REFILL_WINDSCREEN_WASHER_FLUID){
            arrValues.append("KLblRefillWindScreen".localized)
        }
        
        return arrValues.joined(separator: ", ")
    }
    

    
    class func dismissToolTip(views: [UIView]) {
        for view in views {
            if let tipView = view as? EasyTipView {
                tipView.dismiss()
            }
            
            dismissToolTip(views: view.subviews)
        }
    }
    
    class func showPopOverView(str : String, arrowSize: CGSize, isTop: Bool = false,  maxWidth: CGFloat, x: CGFloat, isAttributed: Bool = false, sender : Any?) {
        
        var preferences = EasyTipView.globalPreferences
        if isTop {
            preferences.drawing.arrowPosition = .top
        } else {
            preferences.drawing.arrowPosition = .right
        }
        preferences.drawing.font = FontScheme.kMediumFont(size: 12.0)
        preferences.drawing.textAlignment = .left
        preferences.drawing.backgroundColor = ColorConstants.tooltipcolor
        preferences.drawing.foregroundColor = ColorConstants.secondTextColor
        
        preferences.positioning.maxWidth = maxWidth
//        preferences.positioning.bubbleHInset = x

        //        preferences.animating.dismissTransform = CGAffineTransform(translationX: 100, y: 0)
        //        preferences.animating.showInitialTransform = CGAffineTransform(translationX: 100, y: 0)
//        preferences.animating.showInitialAlpha = 0
//        preferences.animating.showDuration = 0
//        preferences.animating.dismissDuration = 0
//        preferences.drawing.borderColor = ColorConstants.TextColorSecondary
//        preferences.drawing.borderWidth = 1
        preferences.drawing.arrowHeight = arrowSize.height
        preferences.drawing.arrowWidth = arrowSize.width
        
        var text = str
        if isAttributed {
            text = str.htmlToString
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        dismissToolTip(views: appDelegate.window!.subviews)
        
        let tip = EasyTipView(text: text, preferences: preferences)
        tip.show(forView: sender as! UIView)
                
        if !tip.isHidden {
//            TapToDismissEasyTip().set(easyTipView: tip, superView: sender as? UIView)
//            TapContainerToDismissEasyTip().set(easyTipView: tip, superView: sender as? UIView)
        }
        
    }
    
    
    class TapToDismissEasyTip: UITapGestureRecognizer {
        var easyTipView: EasyTipView? = nil
        var superView: UIView? = nil
        
        func set(easyTipView: EasyTipView?, superView: UIView?) {
            self.easyTipView = easyTipView
            self.superView = superView
            self.numberOfTouchesRequired = 1
            superView?.addGestureRecognizer(self)
            self.addTarget(self, action: #selector(self.dismiss))
        }
        
        @objc func dismiss()  {
            easyTipView?.dismiss(withCompletion: {
                self.superView?.removeGestureRecognizer(self)
            })
        }
    }
    
    class TapContainerToDismissEasyTip: UITapGestureRecognizer {
        var easyTipView: EasyTipView? = nil
        var superView: UIView? = nil
        
        func set(easyTipView: EasyTipView?, superView: UIView?) {
            self.easyTipView = easyTipView
            self.superView = superView
            superView?.parentContainerViewController?.view.addGestureRecognizer(self)
            self.numberOfTouchesRequired = 1
            self.addTarget(self, action: #selector(self.dismiss))
        }
        
        @objc func dismiss()  {
            easyTipView?.dismiss(withCompletion: {
//                self.superView?.removeGestureRecognizer(self)
            })
        }
    }
    
    func applyTraitsFromFont(_ f1: UIFont, to f2: UIFont) -> UIFont? {
        let t = f1.fontDescriptor.symbolicTraits
        if let fd = f2.fontDescriptor.withSymbolicTraits(t) {
            return UIFont.init(descriptor: fd, size: 0)
        }
        return nil
    }
    
    //MARK: Text Height
    class func getLabelHeight(constraintedWidth width: CGFloat, font: UIFont,text:String) -> CGFloat {
        
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = text
        label.font = font
        label.sizeToFit()
        
        return label.frame.height
    }
    
    class func getLabelWidth(constraintedHeight height: CGFloat, font: UIFont,text:String) -> CGFloat {
        
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: .greatestFiniteMagnitude, height: height))
        label.numberOfLines = 0
        label.text = text
        label.font = font
        label.sizeToFit()
        
        return label.frame.width
    }
    
    class func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double, completion : ((_ address : String, _ placemarks: CLPlacemark?) -> ())?) {
        
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude
        let lon: Double = pdblLongitude
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                
                NetworkClient.sharedInstance.stopIndicator()
                
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    
                    let pm = placemarks![0]
                    
                    
                    var arrLoc = [String]()
                    arrLoc.append(pm.subLocality ?? "")
                    arrLoc.append(pm.locality ?? "")
                    arrLoc.append(pm.administrativeArea ?? "")
                    arrLoc.append(pm.country ?? "")
                    
                    completion?(arrLoc.joined(separator: ","), pm)
                }
        })
        
    }
    
    class func hexStringFromColor(color: UIColor) -> String {
        let components = color.cgColor.components
        let r: CGFloat = components?[0] ?? 0.0
        let g: CGFloat = components?[1] ?? 0.0
        let b: CGFloat = components?[2] ?? 0.0
        
        let hexString = String.init(format: "#%02lX%02lX%02lX", lroundf(Float(r * 255)), lroundf(Float(g * 255)), lroundf(Float(b * 255)))
        print(hexString)
        return hexString
    }
    
    class func attributedString(firstString: String, secondString: String, firstFont: String, secondFont: String, firstColor: UIColor, secondColor: UIColor, firstSize: CGFloat, secondSize: CGFloat) -> NSMutableAttributedString {
        let first_String: [NSAttributedString.Key: Any] = [.foregroundColor: firstColor, .font: UIFont(name: firstFont, size: firstSize)!]
        let second_String: [NSAttributedString.Key: Any] = [.foregroundColor: secondColor, .font: UIFont(name: secondFont, size: secondSize)!]
        let firstOne = NSMutableAttributedString(string: firstString, attributes: first_String)
        let secondOne = NSMutableAttributedString(string: secondString, attributes: second_String)
        firstOne.append(secondOne)
        return firstOne
    }
    
    class func getDayAndHours(startDate: Date?,endDate: Date?, isDay: Bool, isFromCancellation: Bool = false) -> String? {
        
        let dateRangeStart = DateUtilities.convertStringFromDate(date: startDate!, format: DateUtilities.DateFormates.kLongDate)
         let dateRangeEnd = DateUtilities.convertStringFromDate(date: endDate!, format: DateUtilities.DateFormates.kLongDate)
         
         let start = DateUtilities.convertDateFromStringWithFromat(dateStr: dateRangeStart, format: DateUtilities.DateFormates.kLongDate)
         let end = DateUtilities.convertDateFromStringWithFromat(dateStr: dateRangeEnd, format: DateUtilities.DateFormates.kLongDate)
        
        var components: DateComponents?
        var days: Int
        var hour: Int
        var minutes: Int
        var durationString: String?
        
        components = Calendar.current.dateComponents([.day, .hour, .minute], from: start, to: end)
        
        days = components?.day ?? 0
        hour = components?.hour ?? 0
        minutes = components?.minute ?? 0
        
        if isDay {
            if days > 0 {
                
                if days > 1 {
                    durationString = "\(days) days"
                } else {
                    durationString = "\(days) day"
                }
                return durationString
            }
        } else {
            if hour > 0 {
                
                if hour > 1 {
                    if isFromCancellation {
                        durationString = "\(hour) Hrs"
                    } else {
                        durationString = "\(hour) hours"
                    }
                } else {
                    if isFromCancellation {
                        durationString = "\(hour) Hr"
                    } else {
                        durationString = "\(hour) hour"
                    }
                }
                return durationString
            }
            
            if minutes > 0 {
                
                if minutes > 1 {
                    durationString = "\(minutes) minutes"
                } else {
                    durationString = "\(minutes) minute"
                }
                return durationString
            }
        }
        return ""
    }
}
