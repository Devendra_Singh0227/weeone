//
//  ColorScheme.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved.
//

import UIKit

class ColorScheme: NSObject {
    
    static func colorFromConstant(textColorConstant: String) -> UIColor {
        
        var result = UIColor()
        
        switch textColorConstant {
            
        case "kThemeColor":
            result = self.kThemeColor()
            break
            
        case "kDriveNowThemeColor":
            result = self.kDriveNowThemeColor()
            break
            
        case "kScheduleThemeColor":
            result = self.kScheduleThemeColor()
            break
            
        case "kUnderlineColor":
            result = self.kUnderlineColor()
            break
            
        case "kStartGradiant":
            result = self.kStartGradiant()
            break
            
        case "kEndGradiant":
            result = self.kEndGradiant()
            break
            
        case "kTextColorGreen":
            result = self.kTextColorGreen()
            break
            
        case "kNotificationBGColor":
            result = self.kNotificationBGColor()
            break
            
        case "kTextTitleBlack":
            result = self.kTextTitleBlack()
            break
            
        case "kBGCancelColor":
            result = self.kBGCancelColor()
            break
            
        case "kShadowColor":
            result = self.kShadowColor()
            break
            
        case "kTextColorSecondary":
            result = self.kTextColorSecondary()
            break
            
        case "kTextColorSecondaryAlpha":
            result = self.kTextColorSecondaryAlpha()
            break
            
        case "kTextColorPrimaryAlpha":
            result = self.kTextColorPrimaryAlpha()
            break
            
        case "kTextColorPrimary":
            result = self.kTextColorPrimary()
            break
            
        case "kTextColorWhitePrimary":
            result = self.kTextColorWhitePrimary()
            break
            
        case "kThemeColorSecondary":
            result = self.kThemeColorSecondary()
            break
            
        case "KBgColor":
            result = self.KBgColor()
            break
            
        case "kBorderColor":
            result = self.kBorderColor()
            break
            
        case "kButtonBorderColor":
            result = self.kButtonBorderColor()
            break
            
        case "kDarkBorderColor":
            result = self.kDarkBorderColor()
            break
            
        case "kTextGrayColor":
            result = self.kTextGrayColor()
            break
            
        case "kTextLeftMenuColor":
            result = self.kTextLeftMenuColor()
            break
            
        case "kButtonBGColor":
            result = self.kButtonBGColor()
            break
            
        case "kButtonBGPurpleColor":
            result = self.kButtonBGPurpleColor()
            break
            
        case "kPlaceHolderColor":
            result = self.kPlaceHolderColor()
            break
            
        case "kPaymentTextBlack":
            result = self.kPaymentTextBlack()
            break
            
        case "kPaymentTextPurple":
            result = self.kPaymentTextPurple()
            break
            
        case "kPaymentTextSection":
            result = self.kPaymentTextSection()
            break
            
        case "kPaymentBorderColor":
            result = self.kPaymentBorderColor()
            break
            
        case "kPaymentBGColor":
            result = self.kPaymentBGColor()
            break
            
        case "kRedeemVoucherBGColor":
            result = self.kRedeemVoucherBGColor()
            break
            
        case "kRedeemPlaceHolderColor":
            result = self.kRedeemPlaceHolderColor()
            break
            
        case "kFreeTripsBorderColor":
            result = self.kFreeTripsBorderColor()
            break
            
        case "kFilterBorderBGColor":
            result = self.kFilterBorderBGColor()
            break
            
        case "kFilterSepratorBGColor":
            result = self.kFilterSepratorBGColor()
            break
            
       case "kOtpBGColor":
            result = self.kOtpBGColor()
            break
            
        case "kTitleTextBoldColor":
            result = self.kTitleTextBoldColor()
            break
            
        case "kDescTextColor":
            result = self.kDescTextColor()
            break
            
        case "kMIBorderColor":
            result = self.kMIBorderColor()
            break
            
        case "kMICardTextColor":
            result = self.kMICardTextColor()
            break
          
            //Detail Popup
        case "kDPTextColor":
            result = self.kDPTextColor()
            break
            
        default:
            result = self.kThemeColor()
        }
        return result
    }
    
    //MARK: Private Methods
    
    static func KcommontextColor() -> UIColor{
        return ColorConstants.textcolor
    }
    
    static func KcommonWhite() -> UIColor{
        return ColorConstants.commonWhite
    }
    
    static func kTextColorSecondary() -> UIColor{
        return ColorConstants.secondTextColor
    }
    
    static func kDriveNowThemeColor() -> UIColor{
        return ColorConstants.DriveNowThemeColor
    }

    static func kScheduleThemeColor() -> UIColor{
        return ColorConstants.ScheduleThemeColor
    }

    static func kUnderlineColor() -> UIColor{
        return ColorConstants.UnderlineColor
    }

    static func kThemeColor() -> UIColor{
        return ColorConstants.TextColorTheme
    }
    
    static func kStartGradiant() -> UIColor{
        return ColorConstants.ThemeGradientStartColor
    }

    static func kEndGradiant() -> UIColor{
        return ColorConstants.ThemeGradientEndColor
    }
    
    static func kNotificationBGColor() -> UIColor{
        return ColorConstants.NotificationBGColor
    }
    
    static func kTextColorGreen() -> UIColor{
        return ColorConstants.TextColorGreen
    }
    
    

    static func kTextColorSecondaryAlpha() -> UIColor{
        return ColorConstants.TextColorSecondaryAlpha
    }

    static func kTextColorPrimary() -> UIColor{
        return ColorConstants.TextColorPrimary
    }
    
    static func kTextColorPrimaryAlpha() -> UIColor{
        return ColorConstants.TextColorPrimaryAlpha
    }

    static func kTextColorWhitePrimary() -> UIColor{
        return ColorConstants.TextColorWhitePrimary
    }

    static func kThemeColorSecondary() -> UIColor{
        return ColorConstants.ThemeColorSecondary
    }

    static func kBGCancelColor() -> UIColor{
        return ColorConstants.BGCancelColor
    }
    
    static func kShadowColor() -> UIColor{
        return ColorConstants.ShadowColor
    }

    static func KBgColor() -> UIColor{
        return ColorConstants.BgColor
    }
    
    static func kBorderColor() -> UIColor{
        return ColorConstants.BorderColor
    }
    
    static func kButtonBorderColor() -> UIColor{
        return ColorConstants.ButtonBorderColor
    }
    
    static func kDarkBorderColor() -> UIColor{
        return ColorConstants.DarkBorderColor
    }
    
    static func kTextTitleBlack() -> UIColor{
        return ColorConstants.TextTitleBlack
    }
    
    static func kTextGrayColor() -> UIColor{
        return ColorConstants.TextGrayColor
    }
    static func kTextLeftMenuColor() -> UIColor{
        return ColorConstants.TextLeftMenuColor
    }    
    static func kButtonBGColor() -> UIColor{
        return ColorConstants.ButtonBGColor
    }
    static func kButtonBGPurpleColor() -> UIColor{
        return ColorConstants.ButtonBGPurpleColor
    }
    
    static func kPlaceHolderColor() -> UIColor{
        return ColorConstants.PlaceHolderColor
    }
    
    // Payment
    static func kPaymentTextBlack() -> UIColor{
        return ColorConstants.Payment.PaymentTextBlack
    }
    static func kPaymentTextPurple() -> UIColor{
        return ColorConstants.Payment.PaymentTextPurple
    }
    static func kPaymentTextSection() -> UIColor{
        return ColorConstants.Payment.PaymentTextSection
    }
    static func kPaymentBorderColor() -> UIColor{
        return ColorConstants.Payment.PaymentBorderColor
    }
    static func kPaymentBGColor() -> UIColor{
        return ColorConstants.Payment.PaymentBGColor
    }
    
    //RedeemVoucher
    static func kRedeemVoucherBGColor() -> UIColor{
        return ColorConstants.RedeemVoucher.RedeemVoucherBGColor
    }
    static func kRedeemPlaceHolderColor() -> UIColor{
        return ColorConstants.RedeemVoucher.RedeemPlaceHolderColor
    }
    
    //Free Trips
    static func kFreeTripsBorderColor() -> UIColor{
        return ColorConstants.FreeTrips.FreeTripsBorderColor
    }
    
    static func kFilterBorderBGColor() -> UIColor{
        return ColorConstants.Filter.FilterBorderBGColor
    }
    
    static func kFilterSepratorBGColor() -> UIColor{
        return ColorConstants.Filter.FilterSepratorBGColor
    }
    
    static func kOtpBGColor() -> UIColor{
        return ColorConstants.Otp.BgColor
    }
    
    
    // More Information
    
    static func kTitleTextBoldColor() -> UIColor{
        return ColorConstants.MoreInformtion.TitleTextBoldColor
    }
    
    static func kDescTextColor() -> UIColor{
        return ColorConstants.MoreInformtion.DescTextColor
    }
    
    static func kMIBorderColor() -> UIColor{
        return ColorConstants.MoreInformtion.MIBorderColor
    }
    
    static func kMICardTextColor() -> UIColor{
        return ColorConstants.MoreInformtion.MICardTextColor
    }
    
    
    //Detail Popup
    static func kDPTextColor() -> UIColor{
        return ColorConstants.DetailPopup.DPTextColor
    }
}
