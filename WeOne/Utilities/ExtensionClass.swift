//
//  ExtensionClass.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved
import Foundation
import UIKit
import MapKit

extension Data {
    
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}

extension UIButton{
    
    @IBInspectable
    var TextColorKey: NSString   {
        
        get {
            return  "nil"
        }
        
        set {
            
            setTitleColor(ColorScheme.colorFromConstant(textColorConstant: newValue as String), for: .normal)
        }
    }
}

extension UITextField{
    
    @IBInspectable
    var PlaceHolderColorKey: NSString   {
        
        get {
            return  "nil"
        }
        
        set {
            
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: ColorScheme.colorFromConstant(textColorConstant: newValue as String)])
        }
        
    }
    
    @IBInspectable
    var TextColorKey: NSString   {
        
        get {
            return  "nil"
        }
        
        set {
            
            textColor = ColorScheme.colorFromConstant(textColorConstant: newValue as String)
        }
    }
    
    @IBInspectable
    var placeholderText: NSString {
        
        get {
            return (placeholder ?? "") as NSString
        }
        set {
            placeholder = (newValue as String).localized
        }
    }
    
    func textFieldLeftPadding(padding : CGFloat) {
        // Create a padding view
        self.leftView = UIView(frame: CGRect(x: 0, y: 0, width: padding, height: self.frame.height))
        self.leftViewMode = .always//For left side padding
    }
    
    func textFieldRightPadding(padding : CGFloat) {
        // Create a padding view
        self.leftView = UIView(frame: CGRect(x: 0, y: 0, width: padding, height: self.frame.height))
        self.rightViewMode = .always//For left side padding
    }
    
    var isValidEmail: Bool {
        NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}").evaluate(with: self)
    }
        
}

// Statusbar background color
extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}

extension UINavigationController{
    
    func popAllAndSwitch(to : UIViewController){
        
        self.setViewControllers([to], animated: false)
    }
    
    func pop_To_ViewController(ofClass: AnyClass, animated: Bool = true) {
      if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
        popToViewController(vc, animated: animated)
      }
    }
}

extension UITableView {
    
    func addScrollToTopButton(_ tralingSpace : CGFloat = 0){
        
        //let btn = UIButton(frame: CGRect(x: self.frame.size.width - 60, y: self.frame.size.height - 50, width: 70, height: 50))
        
        
        let btn = UIButton(frame: CGRect(x: AppConstants.ScreenSize.SCREEN_WIDTH - 50 - tralingSpace, y: AppConstants.ScreenSize.SCREEN_HEIGHT - 250, width: 50, height: 30))
        btn.tag = 1002
        btn.isHidden = true
        btn.addTarget(self, action: #selector(self.btnScroll(_:)), for: .touchUpInside)
        btn.setImage(UIImage(named : "UpScroll"), for: .normal)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            UIViewController.current().view.addSubview(btn)
        }
    }
    
    @objc func btnScroll(_ sender: UIButton){
        
        self.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    
    func handleScrollPosition(scrollView: UIScrollView){
        
        if scrollView.contentOffset.y > (self.frame.size.height - 300) {
            
            let btn = UIViewController.current().view.viewWithTag(1002)
            btn?.isHidden = false
        }
        else{
            
            let btn = UIViewController.current().view.viewWithTag(1002)
            btn?.isHidden = true
        }
    }
}

extension Double {
    var formattedTime: String {
        var temp = self
        let hours = Int(temp / 3600.0)
        temp -= (TimeInterval(hours) * 3600)
        
        let minutes = Int(temp / 60.0)
        temp -= (TimeInterval(minutes) * 60)
        
        let seconds = Int(temp)
        temp -= TimeInterval(seconds)
        
        let strMinutes = String(format: "%02d", hours)
        let strSeconds = String(format: "%02d", minutes)
        let strFraction = String(format: "%02d", seconds)
        if hours > 0 {
            return String("\(strMinutes):\(strSeconds):\(strFraction)")
        } else {
            return String("\(strSeconds):\(strFraction)")
        }
    }
    
    var getFormattedTime: String {
        var temp = self
        let hours = Int(temp / 3600.0)
        temp -= (TimeInterval(hours) * 3600)
        
        let minutes = Int(temp / 60.0)
        temp -= (TimeInterval(minutes) * 60)
        
        let seconds = Int(temp)
        temp -= TimeInterval(seconds)
        
        let strMinutes = String(format: "%02d", hours)
        let strSeconds = String(format: "%02d", minutes)
        let strFraction = String(format: "%02d", seconds)
        if hours > 0 {
            return String("\(strMinutes)hr \(strSeconds)min \(strFraction)sec")
        } else {
            return String("\(strSeconds)hr \(strFraction) min")
        }
    }
}

extension UILabel{
    
    func changeStringUnderLine(attrStr : NSMutableAttributedString, array : [String], font: UIFont) {
        
        let inputLength = attrStr.string.count
        let searchString = array
        for i in 0...searchString.count-1
        {
            
            let string  = searchString[i]
            let searchLength = string.count
            var range = NSRange(location: 0, length: attrStr.length)
            
            while (range.location != NSNotFound) {
                range = (attrStr.string as NSString).range(of: string, options: [], range: range)
                if (range.location != NSNotFound) {
                    attrStr.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: range.location, length: searchLength))
                    attrStr.addAttribute(NSAttributedString.Key.font, value: font, range: NSRange(location: range.location, length: searchLength))
                    range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))
                    self.attributedText = attrStr
                }
            }
        }
    }
    
    func changeStringColor(string : String, array : [String], colorArray : [UIColor], changeFontOfString :[String] = [],  font : [UIFont] = [],   strikeThrough :[String] = []) {
        let attrStr = NSMutableAttributedString(string: string)
        let inputLength = attrStr.string.count
        let searchString = array
        for i in 0...searchString.count-1
        {
            
            let string  = searchString[i]
            let searchLength = string.count
            var range = NSRange(location: 0, length: attrStr.length)
            
            while (range.location != NSNotFound) {
                range = (attrStr.string as NSString).range(of: string, options: [], range: range)
                if (range.location != NSNotFound) {
                    attrStr.addAttribute(NSAttributedString.Key.foregroundColor, value: colorArray[i], range: NSRange(location: range.location, length: searchLength))
                    range = NSRange(location: range.location + range.length, length: inputLength - (range.location + range.length))

                    self.attributedText = attrStr
                }
            }
        }
        
        for i in 0..<strikeThrough.count{
            let string  = strikeThrough[i]
            let searchLength = string.count
            var range = NSRange(location: 0, length: attrStr.length)
            
            while (range.location != NSNotFound) {
                range = (attrStr.string as NSString).range(of: string, options: [], range: range)
                if (range.location != NSNotFound) {
                    attrStr.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSRange(location: range.location, length: searchLength))
                    attrStr.addAttribute(NSAttributedString.Key.baselineOffset, value: 0, range: NSRange(location: range.location, length: searchLength))
                    self.attributedText = attrStr
                    break
                }
            }
        
        }
        
        for i in 0..<changeFontOfString.count{
            let string  = changeFontOfString[i]
            let searchLength = string.count
            var range = NSRange(location: 0, length: attrStr.length)
            
            while (range.location != NSNotFound) {
                range = (attrStr.string as NSString).range(of: string, options: [], range: range)
                if (range.location != NSNotFound) {
                    attrStr.addAttribute(NSAttributedString.Key.font, value: font[i], range: NSRange(location: range.location, length: searchLength))
                    self.attributedText = attrStr
                    break
                }
            }
        }
    }
}

