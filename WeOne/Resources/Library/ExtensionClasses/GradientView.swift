//
//  GradientView.swift
//  PTE
//
//  Created by CS-MacSierra on 20/09/17.
//  Copyright © 2017 CS-Mac-Mini. All rights reserved.
//

import UIKit

@IBDesignable

class GradientView: UIView {
    
    var startColor:   UIColor = ColorScheme.kStartGradiant() { didSet { updateColors() }}
    var endColor:     UIColor = ColorScheme.kEndGradiant() { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  true { didSet { updatePoints() }}
    
    
    @IBInspectable var startColorKey:   String = "" { didSet { updateColors() }}
    @IBInspectable var endColorKey:     String = "" { didSet { updateColors() }}
    
    override class var layerClass: AnyClass { return CAGradientLayer.self }
    
    var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    
    func updateColors() {
        
        if startColorKey.count > 0 && endColorKey.count > 0 {
            gradientLayer.colors    = [ColorScheme.colorFromConstant(textColorConstant: startColorKey).cgColor, ColorScheme.colorFromConstant(textColorConstant: endColorKey).cgColor]
        }
        else {
            gradientLayer.colors    = [startColor.cgColor, endColor.cgColor]
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updatePoints()
        updateLocations()
        updateColors()
    }
    
}
