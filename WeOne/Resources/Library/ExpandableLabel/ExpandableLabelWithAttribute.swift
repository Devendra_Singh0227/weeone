//
//  ExpandableLabel.swift
//  IODModels
//
//  Created by Keshav Tiwari on 29/12/17.
//  Copyright © 2017 Keshav Tiwari. All rights reserved.
//

import UIKit

class ExpandableLabelWithAttribute :UILabel {
    
    var isExpaded = false
    var button = UIButton()
    
    @IBInspectable
    var minimumLine: Int = 3
    
    
    @IBInspectable
    var hideRedMore: Bool = false
    
    @IBInspectable
    var buttonTextAlignment: NSTextAlignment = .left
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let buttonAray =  self.superview?.subviews.filter({ (subViewObj) -> Bool in
            return subViewObj.tag ==  9090
        })
        
        if buttonAray?.isEmpty == true {
            if !hideRedMore {
                self.addReadMoreButton()
            }            
        }
    }
    
    //Add readmore button in the label.
    func addReadMoreButton() {
        
        let theNumberOfLines = numberOfLinesInLabel(yourString: self.text ?? "", labelWidth: self.frame.width, labelHeight: self.frame.height, font: self.font)
        
        let height = self.frame.height
        self.numberOfLines =  self.isExpaded ? 0 : minimumLine
        
        if theNumberOfLines > minimumLine {
            
            self.numberOfLines = minimumLine
            
            button = UIButton(frame: CGRect(x: 0, y: height+15, width: self.frame.width, height: 15))
            button.tag = 9090
            button.frame = self.frame
            button.frame.origin.y =  self.frame.origin.y  +  self.frame.size.height + 25
//            button.setAttributedTitle(StringConstants.ExpandableLabelConstant.attributedMoreText, for: .normal)
            button.backgroundColor = .clear
            button.setTitleColor(UIColor.blue, for: .normal)
            button.addTarget(self, action: #selector(ExpandableLabelWithAttribute.buttonTapped(sender:)), for: .touchUpInside)
            self.superview?.addSubview(button)
            self.superview?.bringSubviewToFront(button)
//            button.setAttributedTitle(StringConstants.ExpandableLabelConstant.attributedLessText, for: .selected)
            button.isSelected = self.isExpaded
            button.translatesAutoresizingMaskIntoConstraints = false
            
            if buttonTextAlignment == .center {
                NSLayoutConstraint.activate([
                    button.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                    //button.trailingAnchor.constraint(equalTo: self.trailingAnchor),
                    button.bottomAnchor.constraint(equalTo:  self.bottomAnchor, constant: +24),
                    button.trailingAnchor.constraint(equalTo:  self.trailingAnchor)
                    ])
            }
            else if buttonTextAlignment == .right {
                NSLayoutConstraint.activate([
                    button.bottomAnchor.constraint(equalTo:  self.bottomAnchor, constant: +24),
                    button.trailingAnchor.constraint(equalTo:  self.trailingAnchor)
                    ])
            }
            else {
                NSLayoutConstraint.activate([
                    button.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                    //button.trailingAnchor.constraint(equalTo: self.trailingAnchor),
                    button.bottomAnchor.constraint(equalTo:  self.bottomAnchor, constant: +24)
                    ])
            }
            
            button.titleLabel?.textAlignment = buttonTextAlignment
        }else{
            
            self.numberOfLines = minimumLine
        }
    }
    
    //Calculating the number of lines. -> Int
    func numberOfLinesInLabel(yourString: String, labelWidth: CGFloat, labelHeight: CGFloat, font: UIFont) -> Int {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.minimumLineHeight = labelHeight
        paragraphStyle.maximumLineHeight = labelHeight
        paragraphStyle.lineBreakMode = .byWordWrapping
        
        let attributes: [NSAttributedString.Key: AnyObject] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): font, NSAttributedString.Key(rawValue: NSAttributedString.Key.paragraphStyle.rawValue): paragraphStyle]
        
        let constrain = CGSize(width: labelWidth, height: CGFloat(Float.infinity))
        
        let size = yourString.size(withAttributes: attributes)
        
        let stringWidth = size.width
        
        let numberOfLines = ceil(Double(stringWidth/constrain.width))
        
        return Int(numberOfLines)
    }
    
    //ReadMore Button Action
    @objc func buttonTapped(sender : UIButton) {
        
        self.isExpaded = !isExpaded
        sender.isSelected =   self.isExpaded
        
        self.numberOfLines =  sender.isSelected ? 0 : minimumLine
        
        self.layoutIfNeeded()
        
        var viewObj :UIView?  = self
        var cellObj :UITableViewCell?
        while viewObj?.superview != nil  {
          
           if let cell = viewObj as? UITableViewCell  {
                
              cellObj = cell
            }
            
            if let tableView = (viewObj as? UITableView)  {
               
                if let indexPath = tableView.indexPath(for: cellObj ?? UITableViewCell()){
                                        
                    tableView.beginUpdates()
                    print(indexPath)
                    tableView.endUpdates()
                    
                }
                return
            }

            viewObj = viewObj?.superview
        }
 
        
    }
    
}

