//
//  NetworkClient.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved
import Alamofire
import ObjectMapper
import NVActivityIndicatorView

public typealias ResponseData = [String: Any]
public typealias ResponseArray = [Any]
public typealias FailureMessage = String
public typealias ResponseString = String
public typealias FailureCode    = String

class NetworkClient {
  
    // MARK: - Constant
    struct Constants {
      
      struct RequestHeaderKey {
        static let contentType                  = "Content-Type"
        static let applicationJSON              = "application/json"
      }
      
      struct HeaderKey {
        
        static let Authorization                = "x-token"//"Authorization"
      }
      
      struct ResponseKey {
        
        static let code                         = "code"
        static let data                         = "data"
        static let message                      = "message"
        static let list                         = "list"
        static let serverLastSync               = "server_last_sync"
      }
      
        //MARK:  ResponseCode
        struct ResponseCode {
            
            static let EmailRequired                     = "EMAIL_IS_REQUIRED"
            static let DobRequired                       = "DOB_IS_REQUIRED"
            static let EmailDobRequired                  = "EMAIL_DOB_IS_REQUIRED"
            static let ok                                = "OK"
            static let kTokenExpiredCode                 = "E_TOKEN_EXPIRED"
            static let kUnAuthorized                     = "E_UNAUTHORIZED"
            static let kListNotFound                     = "E_NOT_FOUND"
            static let OK                                = 200
            static let error                             = 401
        }
    }
  
    // MARK: - Properties
    
    // A Singleton instance
    static let sharedInstance = NetworkClient()
    
    // A network reachability instance
    let networkReachability = NetworkReachabilityManager()
    
    // Initialize
    private init() {
        networkReachability?.startListening()
        setIndicatorViewDefaults()
    }

    // MARK: - Indicator view
    private func setIndicatorViewDefaults() {
        
        NVActivityIndicatorView.DEFAULT_TYPE = .lineSpinFadeLoader
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
        NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE = CGSize(width: 40, height: 40)
        NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE_FONT = UIFont.boldSystemFont(ofSize: 17)
    }
    
    func isNetworkAvailable() -> Bool {
        
        guard (networkReachability?.isReachable)! else {
            return false
        }
        
        return true
    }
    
    /// show indicator before network request
    func showIndicator(_ message:String?, stopAfter: Double) {
        
        let activityData = ActivityData(message: message)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
        if stopAfter > 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + stopAfter) {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }
        }
    }
    
    ///Stop Indicator Manually
    func stopIndicator() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    // MARK: - Network Request
    
    // Global function to call web service
    func request(_ url: URLConvertible, command: String, method: Alamofire.HTTPMethod = .get, parameters: Parameters? = nil, headers: HTTPHeaders? = nil, success:@escaping ((Any, String)->Void), failure:@escaping ((FailureMessage,FailureCode)->Void)) {
                
        // check network reachability
        guard (networkReachability?.isReachable)! else {

            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()

            failure("No Internet","")            
             //Utilities.showAlertView(title: AppConstants.AppName, message: "No Internet Connection.")
            return
        }
        
        // create final url
        let finalURLString: String = "\(url)\(command)"
        let finalURL = NSURL(string : finalURLString)! as URL
        
        print(finalURL)
        // Network request
        Alamofire.request(finalURL, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response: DataResponse<Any>) in

            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()

            // check result is success
            guard response.result.isSuccess else {
                failure((response.result.error?.localizedDescription)!,"")
                return
            }
           
            let responseObject = response.result.value as? [String: Any]
            print(responseObject as Any)
            var statusCode = ""
            // get status code //old
            let status = responseObject?[Constants.ResponseKey.code] as? Int ?? 0
            if status == 200 || status == 201 {
                 statusCode = "OK"
            }

            // get status message
            var message = responseObject?[Constants.ResponseKey.message] as? String ?? responseObject?["errorMessage"] as? String ?? responseObject?["data"] as? String ?? ""
            if message == "" {
                let messageC = responseObject?["data"] as? Dictionary<String,Any>
                message = messageC?["message"] as? String ?? ""
            }
//            let error_message = responseObject?["message"] as? Dictionary<String, Any>
//            let message = error_message?["message"] as? String ?? ""
//            let statusCode = error_message?["code"] as? String ?? ""
            
            //User LogOut
//            if statusCode == Constants.ResponseCode.kTokenExpiredCode {
//                ApplicationData.sharedInstance.logoutUser()
//                return
//            }
            
            //User LogOut
//            if statusCode == Constants.ResponseCode.kUnAuthorized {
//                ApplicationData.sharedInstance.logoutUser()
//                return
//            }

            //Chek Code
            if statusCode != Constants.ResponseCode.ok {
                failure(message,statusCode)
                return
            }

            // get data object
            let dataObject = responseObject?[Constants.ResponseKey.data] 
            
            // pass data as dictionary if data key contains json object
            if let data = dataObject as? ResponseData {
                success(data, message)
                return
            } else if let data = dataObject as? ResponseArray {
                success(data, message)
                return
            } else if statusCode == Constants.ResponseCode.ok {
                success(message, statusCode)
            } else {
                failure(message,statusCode)
                return
            }
        }
    }
}
