//
//  SyncManager.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved

import UIKit
import Realm
import RealmSwift

class SyncManager: NSObject {
    
    // A Singleton instance
    static let sharedInstance = SyncManager()
    
    var arrReserveRides : [RideListModel]?
    var activeRideObject : RideListModel?
    var lastRideObject : RideListModel?

    let realm = try! Realm()
    
    // Initialize
    private override init() {
        print("REALM PATH : \(Realm.Configuration.defaultConfiguration.fileURL!)")
    }
    
    func syncDataBase() {
        
        // self.syncMaster(success: nil)
    }
    
    //Sync Master
    func syncMaster(_ success:(()->Void)? = nil, failure:(()->Void)? = nil) {
        
        SyncManager.sharedInstance.arrReserveRides?.removeAll()
        SyncManager.sharedInstance.activeRideObject = nil
        
        let reqDict = ["lastSyncDate" : Defaults[.MasterSyncDate] ?? AppConstants.startingDate]
        
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.MasterSync, method: .post, parameters: reqDict, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            print(response)
            //save Data
            if let responseDict = response as? [String:Any] {
                
                //Save UserInfo
                if let dictUser = responseDict["loggedInUser"] as? [String:Any] {
                    
                    ApplicationData.sharedInstance.saveUserData(dictUser)
//                    ApplicationData.sharedInstance.saveLoginData(data: dictUser)
                    
                    if let arrCard = dictUser["cards"] as? [[String:Any]] {
                        self.insertCards(list: arrCard)
                    }
                    
                    if let arrFavouritePlace = dictUser["favouritePlaces"] as? [[String:Any]] {
                        self.insertFavouritePlaces(list: arrFavouritePlace)
                    }

                    if let userSettings = dictUser["userSetting"] as? [String:Any] {
                        self.insertUserSettings(list: [userSettings])
                    }

                    do {
                        let jsonData = try JSONSerialization.data(withJSONObject: dictUser)
                        UserDefaults.standard.set(jsonData, forKey: AppConstants.UserDefaultKey.UserInfo)
                    } catch {
                        print("Whoops, an error occured: \(error)")
                    }
                }
                
                //Master
                if let dictMaster = responseDict["masters"] as? [String:Any]{
                    
                    if let arrList = dictMaster["list"] as? [[String:Any]]{
                        self.insertMaster(list: arrList)
                    }
                    
                    if let arrDeletedIds = dictMaster["deleted"] as? [String]{
                        self.deleteMaster(arrDeletedIds)
                    }
                }
                
                
                //Ratting

                if let list = responseDict["ratingMaster"] as? [[String:Any]], list.count > 0 {
                    self.insertRating(list: list)
                }
                
                //                    if let arrList = dictMaster["list"] as? [[String:Any]]{
//                        self.insertMaster(list: arrList)
//                    }
//
//                    if let arrDeletedIds = dictMaster["deleted"] as? [String]{
//                        self.deleteMaster(arrDeletedIds)
//                    }
//                }

                //Setting
                if let dictSetting = responseDict["setting"] as? [String:Any]{
                    self.insertSettings(list: [dictSetting])
                }
                
                Defaults[.MasterSyncDate] = responseDict["lastSyncDate"] as? String
                
                //Pending Payment Ride
                if let reserveRide = responseDict["pendingPaymentRide"] as? [String:Any], reserveRide.count > 0{
                    self.activeRideObject = Mapper<RideListModel>().map(JSON: reserveRide)!
                }
                
                //Reserve Ride
                if let arrRide = responseDict["reserveRide"] as? [[String:Any]], arrRide.count > 0{
                    
                    self.arrReserveRides = Mapper<RideListModel>().mapArray(JSONArray: arrRide)
                    
//                    let filter = self.arrReserveRides?.filter { $0.rideType == AppConstants.RideType.Instant}
//                    
//                    if filter?.count ?? 0 > 0{
//                        self.activeRideObject = filter!.first
//                    }
                }
                
                //Last Ride
                if let lastRide = responseDict["lastRide"] as? [String: Any], lastRide.count > 0 {
                    self.lastRideObject = Mapper<RideListModel>().map(JSON: lastRide)!
                }
                // Active Ride
                if let reserveRide = responseDict["activeRide"] as? [String:Any], reserveRide.count > 0{
                    self.activeRideObject = Mapper<RideListModel>().map(JSON: reserveRide)!
                }
            }
            
            if let cliick = success {
                cliick()
            }
        }) { (failureMessage, failureCode) in
            
            Utilities.showAlertWithButtonAction(title: AppConstants.AppName, message: failureMessage, buttonTitle: StringConstants.ButtonTitles.KOk) {
                if let cliick = failure {
                    cliick()
                }
            }
        }
    }
    
    func callApiForReserverRide(_ req : [String:Any], completed :(() -> ())?){
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.ReserveRide, method: .post, parameters: req, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let _ = response as? [String:Any]{
                
                self.syncMaster ({
                    
                    completed?()
                })
            }
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
    
    func upsertPlayerId() {

        if !ApplicationData.isUserLoggedIn {
            return
        }
        
        if Utilities.checkStringEmptyOrNil(str: Defaults[.PlayerId]) == true{
            return
        }

        
        var header = ApplicationData.sharedInstance.authorizationHeaders
        header["devicetype"]    = "\(AppConstants.DeviceTypeIphone)"
        header["deviceId"]      = ApplicationData.deviceId
        header["playerid"]      = Defaults[.PlayerId]
        
        NetworkClient.sharedInstance
            .request(AppConstants.serverURL, command: AppConstants.URL.UpsertPlayerId, method: .post, parameters: nil, headers: header, success: { (response, message) in
                print("Player Id sended")
                print(response)
            }) { (failureMessage, failureCode) in
        }
    }
}

//MARK: = Common API Called

extension SyncManager {
    
    // Start Ride
    func startRide(reqDict: [String:Any], success:((_ response : [String:Any])->Void)?, failure:(()->Void)? = nil) {
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.StartRide, method: .post, parameters: reqDict, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            //save Data
            if let responseDict = response as? [String:Any] {
                
                SyncManager.sharedInstance.activeRideObject = Mapper<RideListModel>().map(JSON: responseDict)!
                
                success?(responseDict)
            }
            
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(title: AppConstants.AppName, message: failureMessage)
            failure?()
        }
    }
    
    // Pause Ride
    func pauseRide(rideId: String, _ success:((_ response : [String:Any])->Void)? = nil, failure:(()->Void)? = nil) {
        
        let reqDict = ["rideId" : rideId]
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.PauseRide, method: .post, parameters: reqDict, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            //save Data
            if let responseDict = response as? [String:Any] {
                
                success?(responseDict)
            }
            
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(title: AppConstants.AppName, message: failureMessage)
            failure?()
        }
    }
    
    // Resume Ride
    func resumeRide(rideId: String, _ success:((_ response : [String:Any])->Void)? = nil, failure:(()->Void)? = nil) {
        
        let reqDict = ["rideId" : rideId]
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.ResumeRide, method: .post, parameters: reqDict, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            //save Data
            if let responseDict = response as? [String:Any] {
                
                success?(responseDict)
            }
            
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(title: AppConstants.AppName, message: failureMessage)
            failure?()
        }
    }
    
    // Stop Ride
    func stopRide(reqDict: [String:Any], _ success:((_ response : [String:Any])->Void)? = nil, failure:(()->Void)? = nil) {
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.StopRide, method: .post, parameters: reqDict, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            //save Data
            if let responseDict = response as? [String:Any] {
                
                print(responseDict)
                success?(responseDict)
            }
            
        }) { (failureMessage, failureCode) in
            
            Utilities.showAlertView(title: AppConstants.AppName, message: failureMessage)
            failure?()
        }
    }
    
    // Get Fare Summary
    func getFareSummary(reqDict: [String:Any], success:((_ response : [String:Any])->Void)?, failure:(()->Void)? = nil) {
        
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.GetFareSummary, method: .post, parameters: reqDict, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            //save Data
            if let responseDict = response as? [String:Any] {
                
                success?(responseDict)
            }
            
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(title: AppConstants.AppName, message: failureMessage)
            failure?()
        }
    }
    
    //Sorting Based On Location
    func sortigBasedOnLocation(arrMarker : [VehicleModel],currentLocation : CLLocation) -> [VehicleModel]{
        
        let sorted = arrMarker.sorted { (model1, model2) -> Bool in
            
            let location1 = CLLocation(latitude: model1.geoLocation?.latitude ?? 0.0, longitude: model1.geoLocation?.longitude ?? 0.0)
            let distance1 = location1.distance(from: currentLocation)
           // model1.distance = distance1
            
            let location2 = CLLocation(latitude: model2.geoLocation?.latitude ?? 0.0, longitude: model2.geoLocation?.longitude ?? 0.0)
            let distance2 = location2.distance(from: currentLocation)
            //model2.distance = distance2
            
            return distance1 < distance2
        }
        
        return sorted
    }
}

// MARK:- Database Insert/Update/Delete Entities
extension SyncManager {
    
    func clearDataBase() {
        
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    func clearFavourites(){
        try! realm.write {
            realm.delete(realm.objects(FavouritePlaceModel.self))
        }
    }
    
    func clearUserSettings(){
        try! realm.write {
            realm.delete(realm.objects(UserSettingModel.self))
        }
    }

    
    //Insert Masster
    func insertMaster(list : [[String : Any]]) {
        
        let arr = Mapper<MasterModel>().mapArray(JSONArray: list)
        
        if self.realm.isInWriteTransaction {
            self.realm.add(arr, update: .all)
        }
        else {
            try! self.realm.write {
                self.realm.add(arr, update: .all)
            }
        }
    }
    
    
    //Insert Rating
    func insertRating(list : [[String : Any]]) {

        let arr = Mapper<RatingModel>().mapArray(JSONArray: list)

        if self.realm.isInWriteTransaction {
            self.realm.add(arr, update: .all)
        }
        else {
            try! self.realm.write {
                self.realm.add(arr, update: .all)
            }
        }
    }

    //Insert Setting
    func insertSettings(list : [[String : Any]]) {
        
        let arr = Mapper<SettingModel>().mapArray(JSONArray: list)
        
        if self.realm.isInWriteTransaction {
            self.realm.add(arr, update: .all)
        }
        else {
            try! self.realm.write {
                self.realm.add(arr, update: .all)
            }
        }
    }
    
    //Delete Master
    func deleteMaster(_ arrIds : [String]) {
        
        for id in arrIds {
            
            let predicate = NSPredicate(format: "id = %@ ",id)
            let objectsToDelete = realm.objects(MasterModel.self).filter(predicate)
            
            // and then just remove the set with
            try! realm.write {
                realm.delete(objectsToDelete, cascading: false)
            }
        }
    }
}

//MARK: Master methods
extension SyncManager {
    
    func fetchMasterWithCode(_ code : String) -> [MasterModel]{
        
        return Array(realm.objects(MasterModel.self).filter { $0.code == code})
    }
    
    func fetchMasterWithParentId(_ id : String) -> [MasterModel]{
        
        return Array(realm.objects(MasterModel.self).filter { $0.parentId == id})
    }
    
    func fetchMasterWithId(_ id : String) -> [MasterModel]{
        
        return Array(realm.objects(MasterModel.self).filter { $0.id == id})
    }
    
    func fetchRatingMasterWithId(_ id : String) -> String{
        
        let arrRatting = self.fetchMasterWithId(id)

        return arrRatting.first?.name ?? ""
    }

    func fetchNameFromBrand(_ id : String) -> String?{
 
        let arrBrands = self.fetchMasterWithId(id)

        return arrBrands.first?.name ?? nil
    }
    
    
    //Fetch Rating
    func fetchRating() -> [RatingModel] {
        return Array(realm.objects(RatingModel.self))
    }
}

//MARK: Fetch Setting
extension SyncManager {
    
    func fetchSettings() -> SettingModel?{
        return Array(realm.objects(SettingModel.self)).first
    }
}

//MARK:- Payment Methods
extension  SyncManager {
    
    func fetchAllCards() -> [CardModel] {
        return Array(realm.objects(CardModel.self))
    }
    
    //Fetch Card
    func fetchCard(_ type : Int = AppConstants.CardTypes.PersonalCard) -> [CardModel] {
        
        return Array(realm.objects(CardModel.self).filter { $0.cardType == type })
    }
    
    //Insert Card
    func insertCard(dict:[String:Any]) {
        
        let model = Mapper<CardModel>().map(JSON: dict)!
        if self.realm.isInWriteTransaction {
            self.realm.add(model, update: .all)
        }
        else {
            try! self.realm.write {
                self.realm.add(model, update: .all)
            }
        }
    }
    
    //Insert Cards
    func insertCards(list : [[String : Any]]) {
        
        let arr = Mapper<CardModel>().mapArray(JSONArray: list)
        
        if self.realm.isInWriteTransaction {
            self.realm.add(arr, update: .all)
        }
        else {
            try! self.realm.write {
                self.realm.add(arr, update: .all)
            }
        }
    }

    
    //Set Primary Card
    func setPrimaryCard(cardModel:CardModel) {
        
        let arr = Array(realm.objects(CardModel.self).filter { $0.cardType == cardModel.cardType } )
        
        // set primary false to all
        for obj in arr {
            try! self.realm.write {
                obj.isPrimary = false
            }
        }
        
        // set primary true to specific card
        try! self.realm.write {
            cardModel.isPrimary = true
        }
    }
    
    func getPrimaryCard(_ cardType : Int = AppConstants.CardTypes.PersonalCard) -> CardModel?{
        let arr =  Array(realm.objects(CardModel.self).filter { $0.cardType == cardType })
        if arr.count > 0 {
            let arrPrimary = arr.filter { $0.isPrimary == true }
            if arrPrimary.count > 0 {
                return arrPrimary.first
            }
            else {
                return arr.first
            }
        }
        
        return nil
        
    }
    
    //Fetch card info from card id
    func fetchCardFromId(_ id : String) -> CardModel?{
        let arr =  Array(realm.objects(CardModel.self).filter { $0.id == id})
        
        if arr.count > 0{
            return arr.first
        }
        
        return nil
    }
    
    //Delte Card
    func deleteCard(obj:CardModel?) {
        
        if let deleteObj = obj {
            try! realm.write {
                realm.delete(deleteObj)
            }
        }
    }
    
    func deleteCards(_ arr : [String]) {
        
        try! realm.write {
            realm.delete(realm.objects(CardModel.self).filter( { arr.contains( $0.id ?? "") }))
        }
        
    }
}

//MARK: Favourite place
extension SyncManager{
    
    
    //Fetch FavouritePlaces
    func fetchAllFavouritePlaces() -> [FavouritePlaceModel] {
        return Array(realm.objects(FavouritePlaceModel.self))
    }

    //Insert FavouritePlaces
    func insertFavouritePlaces(list : [[String : Any]]) {
        
        clearFavourites()
        
        let arr = Mapper<FavouritePlaceModel>().mapArray(JSONArray: list)
        
        if self.realm.isInWriteTransaction {
            self.realm.add(arr, update: .all)
        }
        else {
            try! self.realm.write {
                self.realm.add(arr, update: .all)
            }
        }
    }
    
    func removeFavourite(_ id : String){
        
        let arr =  Array(realm.objects(FavouritePlaceModel.self).filter { $0.id == id})
        
        if arr.count > 0{
            try! realm.write {
                realm.delete(arr)
            }
        }
    }
    
    func fetchAllUserSettings() -> [UserSettingModel] {
        return Array(realm.objects(UserSettingModel.self))
    }

    func insertUserSettings(list : [[String : Any]]) {
        
        clearUserSettings()
        
        let arr = Mapper<UserSettingModel>().mapArray(JSONArray: list)
        
        if self.realm.isInWriteTransaction {
            self.realm.add(arr, update: .all)
        }
        else {
            try! self.realm.write {
                self.realm.add(arr, update: .all)
            }
        }
    }
    
    func removeUserSettings(_ id : String){
        
        let arr =  Array(realm.objects(UserSettingModel.self).filter { $0.id == id})
        
        if arr.count > 0{
            try! realm.write {
                realm.delete(arr)
            }
        }
    }
}
