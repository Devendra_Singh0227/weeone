//
//  LocationRoute.swift
//  WeOne
//
//  Created by iMac on 07/02/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import Foundation
import CoreLocation


class LocationRoute: NSObject , CLLocationManagerDelegate {
    
    static let SharedManager = LocationRoute()
    
    lazy var geocoder = CLGeocoder()
        
    var city = ""
    
    var getLocation :(([String: Double]) -> ())?

    private override init () {
        super.init()
    }
    
    func geocode(location: CLLocation) {
        
        geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            self.processResponse(withPlacemarks: placemarks, error: error)
        }
        
    }
    
    private func processResponse(withPlacemarks placemarks: [CLPlacemark]?, error: Error?) {
        // Update View
        
        if let error = error {
            print("Unable to Reverse Geocode Location (\(error))")
            
        } else {
            if let placemarks = placemarks, let placemark = placemarks.first {
                print(placemark.compactAddress)
                
                city = placemark.locality ?? ""
                getAddress(address: city)
                
            } else {
                print("No Matching Addresses Found")
            }
        }
    }
    
    func getAddress(address:String) {
        
        let key : String = AppConstants.googleApiKeyForReverseGeoCode
        let postParameters:[String: Any] = [ "address": address,"key":key]
        let url : String = "https://maps.googleapis.com/maps/api/geocode/json?address=\(address)&key=\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        
        
        //
        Alamofire.request(url, method: .get, parameters: postParameters, encoding: URLEncoding.default, headers: nil).responseJSON {  response in
            
            if let receivedResults = response.result.value {
                
                if let data = receivedResults as? [String: Any] {
                    if let arrResult = data["results"] as? [[String: Any]] {
                        if  let dictResult = arrResult.first  {
                            if let geometry = dictResult["geometry"] as? [String: Any] {
                                if let location = geometry["location"] as? [String: Double] {
                                    self.getLocation?(location)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
extension CLPlacemark {
    
    var compactAddress: String? {
        if let name = name {
            var result = name
            
            if let street = thoroughfare {
                result += ", \(street)"
            }
            
            if let city = locality {
                result += ", \(city)"
            }
            
            if let country = country {
                result += ", \(country)"
            }
            
            return result
        }
        
        return nil
    }
    
}
