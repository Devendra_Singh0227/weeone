//
//  UserDefaultExtension.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved

import UIKit
import SwiftyUserDefaults

extension UserDefaults {

    static func removeAllData() {
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()
    }
}

// MARK: - Default keys
extension DefaultsKeys {
    //location_access
    static let location_access      = DefaultsKey<Bool?>(AppConstants.UserDefaultKey.location_access)
    
    // Userinfo
    static let FirstName            = DefaultsKey<Any?>(AppConstants.UserDefaultKey.FirstName)
    static let LastName             = DefaultsKey<Any?>(AppConstants.UserDefaultKey.LastName)
    static let Email                = DefaultsKey<Any?>(AppConstants.UserDefaultKey.Email)
    static let profilePic           = DefaultsKey<Any?>(AppConstants.UserDefaultKey.ProfilePic)
    static let Code                 = DefaultsKey<Any?>(AppConstants.UserDefaultKey.Code)
    static let Mobile               = DefaultsKey<Any?>(AppConstants.UserDefaultKey.Mobile)
    static let isMobileVerified     = DefaultsKey<Bool?>(AppConstants.UserDefaultKey.isMobileVerified)
    
    //DeviceToken
    static let deviceToken          = DefaultsKey<Any?>(AppConstants.UserDefaultKey.DeviceToken)
    
    static let userInfo             = DefaultsKey<[String:Any]?>(AppConstants.UserDefaultKey.UserInfo)
    
    static let isUserLoggedIn       = DefaultsKey<Bool>(AppConstants.UserDefaultKey.isUserLoggedIn)
    static let isUserLoggedOut      = DefaultsKey<Bool>(AppConstants.UserDefaultKey.isUserLoggedOut)
    static let isNewUser            = DefaultsKey<Bool>(AppConstants.UserDefaultKey.isNewUser)
    static let isTokenExpired       = DefaultsKey<Bool>(AppConstants.UserDefaultKey.isTokenExpired)
    
    // Token
    static let accessToken          = DefaultsKey<String?>(AppConstants.UserDefaultKey.tokenKey)
    
    //Version update
    static let SkipUpdate           = DefaultsKey<Bool>(AppConstants.UserDefaultKey.SkipUpdate)
    
    //master Sync Date
    static let MasterSyncDate       = DefaultsKey<String?>(AppConstants.SyncDate.master)
    
    static let DontShowImplementBookingPopup       = DefaultsKey<Bool>(AppConstants.UserDefaultKey.DontShowImplementBookingPopup)
    
    //master Sync Date
    static let Filter               = DefaultsKey<[String:Any]?>(AppConstants.UserDefaultKey.Filter)
    
    //master Sync Date
    static let PlayerId             = DefaultsKey<String>(AppConstants.UserDefaultKey.PlayerId)
    
    //Selected Card Type
    static let SelectedCard         = DefaultsKey<CardModel?>(AppConstants.UserDefaultKey.SelectedCard)
    
    static let vehicleId            = DefaultsKey<String?>(AppConstants.UserDefaultKey.vehicleId)
}

