//
//  UserInputValidator.swift
//  E-Scooter
//
//  Created by iMac on 06/09/19.
//  Copyright © 2019 CORUSCATEMAC. All rights reserved.
//

import Foundation

struct UserInputValidator {

    static func nameOnlyWithLength(text: String, range: NSRange, replacementString: String, maxLength: Int?) -> Bool {
        let newLength = text.count + replacementString.count - range.length
        
        let aSet = NSCharacterSet(charactersIn:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ").inverted
        let compSepByCharInSet = replacementString.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        
        return replacementString == numberFiltered && (maxLength != nil ? newLength <= maxLength! : true)
    }

}
