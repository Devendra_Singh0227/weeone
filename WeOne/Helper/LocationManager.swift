//
//  LocationManager.swift
//  OrganicHome
//
//  Created by Vish on 16/02/18.
//  Copyright © 2018 CS-Mac-Mini. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class LocationManager: NSObject , CLLocationManagerDelegate{
    
    static let SharedManager = LocationManager()
    
    //Private variables
    private var locationManager = CLLocationManager()
    private var isForLiveLocation : Bool = false
    private var didFindLocation : Bool = false
    
    //Public variables
    var isLocationEnabled : Bool =  CLLocationManager.authorizationStatus() == .denied ? false : true
    var currentLocation : CLLocation?
    var getCurrentLocation : ((CLLocation) -> ())?
    var getLiveLocation : ((CLLocation) -> ())?
    var locationStatus : ((String) -> ())?
    
    private override init () {
        super.init()
    }
    
    //MARK: Get location
    func getLocation(_ isForLiveLocation : Bool = false){
        
        if (CLLocationManager.locationServicesEnabled()){
            
            locationManager = CLLocationManager()
            
            didFindLocation = false
            
            self.isForLiveLocation = isForLiveLocation
            
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            
            if isForLiveLocation {
                locationManager.distanceFilter = 50
                locationManager.pausesLocationUpdatesAutomatically = true
                locationManager.activityType = .automotiveNavigation
            }
            
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
            
        } else {
            
        }
    }
    
    func openSettings() {
        if let url = URL(string: UIApplication.openSettingsURLString) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler:   { (Bool) in
//                    print(Bool)
                    self.locationStatus?("Authorize")
                })
            }
        }
    }
    
    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if self.isForLiveLocation{
            
            getLiveLocation?(locations.first!)
            getCurrentLocation?(locations.first!)
        }else{
            
            if didFindLocation == false{
                didFindLocation = true
                getCurrentLocation?(locations.first!)
                locationManager.stopUpdatingLocation()
            }
        }
    }
    
    func stopLiveLocationUpdate(){
        locationManager.stopUpdatingLocation()
    }
}
