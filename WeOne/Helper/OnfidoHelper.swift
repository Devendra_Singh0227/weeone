////
////  OnfidoHelper.swift
////  WeOne
////
////  Created by Mayur on 11/12/19.
////  Copyright © 2019 Coruscate Mac. All rights reserved.
////
//
//import UIKit
//import Onfido
//
//class OnfidoHelper: NSObject {
//
//    // A Singleton instance
//    static let shared = OnfidoHelper()
//
//    private var applicantId : String = ""
//    private var token : String = ""
//
//    var vc = UIViewController()
//
//    // Initialize
//    private override init() {
//
//    }
//
//    //Verify Document
//    func verifyDocument(vc: UIViewController) {
//        self.vc = vc
//        self.moveToOnfido()
//        generateApplicantId()
//    }
//
//    //Generate ApplicantId
//    private func generateApplicantId() {
//
//        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
//        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.CreateApplicant, method: .post, parameters: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
//
//           if let dictResponse = response as? [String:Any]{
//             self.applicantId = dictResponse["applicantId"] as? String ?? ""
//           }
//           self.generateToken()
//
//        }) { (failureMessage, failureCode) in
//            Utilities.showAlertView(message: failureMessage)
//        }
//    }
//
//    //Generate Token
//    private func generateToken(){
//
//        getToken {
//            //self.moveToOnfido()
//        }
//    }
//    
//    //Get Token
//    private func getToken(success:(()->Void)?){
//
//        var req = [String:Any]()
//        req["applicantId"] = applicantId
//
//        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
//        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.GenerateToken, method: .post, parameters: req, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
//
//           if let dictResponse = response as? [String:Any]{
//             self.token = dictResponse["token"] as? String ?? ""
//           }
//           success?()
//
//        }) { (failureMessage, failureCode) in
//            Utilities.showAlertView(message: failureMessage)
//        }
//    }
//}
//
////MARK : Onfido Method
//extension OnfidoHelper {
//
//    //Move To Onfido
//    func moveToOnfido() {
//
//        //Config
//        let config = try! OnfidoConfig.builder()
//                   .withSDKToken(token)
//                   .withWelcomeStep()
//                   .withDocumentStep(ofType: .drivingLicence, andCountryCode: "GBR")
//                   .withFaceStep(ofVariant: .photo(withConfiguration: nil))
//                   .build()
//
//        //Response Handler
//        let onfidoFlow = OnfidoFlow(withConfiguration: config)
//        .with(responseHandler: { results in
//            // Callback when flow ends
//             if case let OnfidoResponse.error(innerError) = results {
//                 Utilities.showAlertView(message: innerError.localizedDescription)
//             } else if case OnfidoResponse.success = results {
//                 //Verify Documemnt
//                 self.callVerifyDocumentApi()
//
//             } else if case OnfidoResponse.cancel = results {
//
//             }
//        })
//
//        let onfidoRun = try! onfidoFlow.run()
//        UIViewController.current()?.present(onfidoRun, animated: true, completion: nil)
//    }
//
//    //Call VerifyDocument
//    func callVerifyDocumentApi() {
//
//        var req = [String:Any]()
//        req["applicantId"] = applicantId
//        req["userId"] = ApplicationData.user?.id
//
//        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
//        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.CreateOnfidoCheck, method: .post, parameters: req, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
//
//           if let dictResponse = response as? [String:Any]{
//              ApplicationData.sharedInstance.saveUserData(dictResponse)
//           }
//
//           let a = AppNavigation.shared.checkUserHasAccess(vc: self.vc)
//
//        }) { (failureMessage, failureCode) in
//            Utilities.showAlertView(message: failureMessage)
//        }
//    }
//}
