//
//  ApplicationData.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved

import UIKit
import ObjectMapper_Realm

/** This singleton class is used to store application level data. */
class ApplicationData: NSObject {
    
    // A Singleton instance
    static let sharedInstance = ApplicationData()
    
    // Checks if user is logged in
    static var isUserLoggedIn: Bool {
        get {
            return Defaults.bool(forKey: AppConstants.UserDefaultKey.isUserLoggedIn)
        }
    }
    
    // returns Token
    static var accessToken: String? {
        get {
            return Defaults[.accessToken]
        }
    }
    
    // returns Current Time Zone
    static var timeZone: String {
        get {
            let timeZone = NSTimeZone.local
            return timeZone.localizedName(for: .standard, locale: .current) ?? ""
        }
    }
    
    //Return user information
    static var user : UserInfoModel?{
        
        get {
            if let data = UserDefaults.standard.value(forKey: AppConstants.UserDefaultKey.UserInfo) as? Data{
                return try? JSONDecoder().decode(UserInfoModel.self, from: data)
            }
            return  nil
        }
    }
    
    //retun device Id
    static var deviceId: String {
        get {
            return UIDevice.current.identifierForVendor!.uuidString
        }
    }
    
    //retun device Name
    static var deviceName: String {
        get {
            return UIDevice.current.localizedModel
        }
    }
    
    
    //retun device version
    static var deviceVersion: String {
        get {
            return UIDevice.current.systemVersion
        }
    }
    
    static func buildVersion() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let build = dictionary["CFBundleVersion"] as! String
        return "V \(build)"
    }
    
    //return UUID
    func getUUID() -> String {
        return UUID().uuidString
    }
    
    /// Default headers to be passed in CenterAPI requests
    var authorizationHeaders: HTTPHeaders {
        
        get{
            if ApplicationData.accessToken == "" {
                return [:]
            } else {
                var headers: HTTPHeaders = [:]
                headers = [NetworkClient.Constants.HeaderKey.Authorization:"\(ApplicationData.accessToken!)"]
                print("Headers",headers)
                return headers
            }
        }
    }
    
    //Move To Home
    func moveToHome() {
        let vc = CarMapVC()
        UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
    }
    
    // Save user details
    func saveLoginData(data: [String : Any]) {
        
        let newUser = data["newUser"] as? Int
        if newUser == 1 {
           Defaults[.isNewUser] = true
        } else {
            Defaults[.isNewUser] = false
        }
        if let Token = data["user"] as? [String:Any],Token.count > 0{
            Defaults[.accessToken] = Token["token"] as? String ?? ""
        }
        
        if let User = data["data"] as? [String:Any], User.count > 1{
            Defaults[.accessToken] = User["token"] as? String ?? ""
            Defaults[.isUserLoggedIn] = true
//            print( Defaults[.userInfo])
        }
       
        //NotificationCenter.default.post(name: Notification.Name(AppConstants.Notification.resetMenu), object: nil)
    }
    
    func saveUserData(_ dict : [String:Any]){
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict)
            UserDefaults.standard.set(jsonData, forKey: AppConstants.UserDefaultKey.UserInfo)
            
            //Insert Cards
            if let arrCard = dict["cards"] as? [[String:Any]] {
                SyncManager.sharedInstance.insertCards(list: arrCard)
            }
            
        } catch {
            print("Whoops, an error occured: \(error)")
        }
    }
    
    // Logout User
    func logoutUser() {
        ApplicationData.sharedInstance.callLogoutAPI()
    }
    
    //Clear Database And User Data
    func clearUserData() {
        
        let playerId = Defaults[.PlayerId]
        
        // user default clear
        SyncManager.sharedInstance.clearDataBase()
        //UserDefaults.removeAllData()
        NotificationCenter.default.post(name: Notification.Name(AppConstants.Notification.resetMenu), object: nil)
        
        Defaults[.PlayerId] = playerId
    }
    
    private func callLogoutAPI() {
        
        //        var request = Parameters()
        //        request["player_id"] = Defaults[.playerID]
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.Logout, method: .post, parameters: nil, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (_, _) in
            Defaults[.isUserLoggedIn] = false
            // user default clear
             Defaults[.isUserLoggedOut] = true
            self.clearUserData()
            AppNavigation.shared.gotNextFromLogout()
            
        }) { (_, _) in
        }
    }
    
    //MARK:Authentication Push
    class func authenticationPush() {
        
        var requestDic = ApplicationData.sharedInstance.authorizationHeaders
        //        requestDic["player_id"] = Defaults[.NotificationPlayerId]
        requestDic["devicetype"] = "\(AppConstants.DeviceTypeConstant.IOS)"
        
        //        if let userId = ApplicationData.user.userId, userId.count > 0 {
        //            requestDic["user_id"] = ApplicationData.user.userId ?? ""
        //        }
        
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.UpsertPlayerId, method: .post, parameters: nil, headers: requestDic, success: { (response, message) in
            
        }) { (failureMessage, failureCode) in
            
        }
    }
}
