//
//  AppNavigation.swift
//  WeOne
//
//  Created by Mayur on 28/11/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class AppNavigation: NSObject {

    static let shared = AppNavigation()
    
    //Config
    let config = AppConfigurationModel()
    
    var currentStateList = AppConfigurationModel().accessConfig.requiredToUseApp

    override init() {
        super.init()
    }
    
    //Go Next from Splash
    func checkUserHasAccess(vc: UIViewController?) -> Bool {
        

        if isRequiredLogin(currentStateList) {
            
            //Login
            moveToLogin(isPopAndSwitch: false)
            return false
        }
        else if isRequiredMobileVerificaion(currentStateList) {
            

        //    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                if vc?.navigationController != nil{
                    for viewController in (vc?.navigationController?.viewControllers)!{
                        if !viewController.isKind(of: CheckMoreInformationVC.self) {
                            vc?.navigationController?.viewControllers.remove(at: (vc?.navigationController?.viewControllers.firstIndex(of: viewController))!)
                        }
                    }
                }
       //     })
            
            moveToOtp(isPopAndSwitch: false,isSendOtp: true)
            return false
        }
        else if isRequiredDocumentVerificaion(currentStateList) {
            
          //  DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                if vc?.navigationController != nil{
                    for viewController in (vc?.navigationController?.viewControllers)!{
                        if !viewController.isKind(of: CheckMoreInformationVC.self) {
                            vc?.navigationController?.viewControllers.remove(at: (vc?.navigationController?.viewControllers.firstIndex(of: viewController))!)
                        }
                    }
                }
         //   })
                
            moveToDocument(isPopAndSwitch: false)
            return false
        }
        else if isRequiredAddCards(currentStateList) {
            
            //Cards
          //  DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                if vc?.navigationController != nil{
                    for viewController in (vc?.navigationController?.viewControllers)!{
                        if !viewController.isKind(of: CheckMoreInformationVC.self) {
                            vc?.navigationController?.viewControllers.remove(at: (vc?.navigationController?.viewControllers.firstIndex(of: viewController))!)
                        }
                    }
                }
          //  })
                
            moveToAddCards(isPopAndSwitch: false)
            return false
        }
        
        return true
//        else {
//
//            //Home
//            if !(vc?.isKind(of: CheckMoreInformationVC.self))! {
//                vc?.navigationController?.popViewController(animated: true)
//            }
//        }
    }
    
    //Go Next from Login
    func goNextFromLogin() {
        
        let useApp = config.accessConfig.requiredToUseApp
//        if useApp.count == 0 {
//            useApp = config.accessConfig.requiredForReservation
//        }
        
        if isRequiredMobileVerificaion(useApp) {
            
            //Otp
            moveToOtp(isFromAuth: true)
        }
        else if isRequiredDocumentVerificaion(useApp) {
            
            //Document
            moveToDocument()
        }
        else if isRequiredAddCards(useApp) {
            
            //Cards
            moveToAddCards()
        }
        else{
            //Home
            SyncManager.sharedInstance.syncMaster ({
                self.moveToHome()
            })
        }
    }
    
    //Go Next from Otp
    func goNextFromOtp() {
        
        var useApp = config.accessConfig.requiredToUseApp
        if useApp.count == 0 {
            useApp = config.accessConfig.requiredForReservation
        }
        if isRequiredDocumentVerificaion(useApp) {
            
            //Document
            moveToDocument()
        }
        else if isRequiredAddCards(useApp) {
            
            //Cards
            moveToAddCards()
        }
        else{
        
            //Home
            SyncManager.sharedInstance.syncMaster ({
                self.moveToHome()
            })
        }
    }
    
    //Go Next from Document
    func goNextFromDocument() {
       
        var useApp = config.accessConfig.requiredToUseApp
        if useApp.count == 0 {
            useApp = config.accessConfig.requiredForReservation
        }
       if isRequiredAddCards(useApp) {
            
            //Cards
            moveToAddCards()
        }
        else{
        
            //Home
            SyncManager.sharedInstance.syncMaster ({
                self.moveToHome()
            })
        }
    }
    
    //Go Next from Cards
    func goNextFromCards() {
        
       //Home
       SyncManager.sharedInstance.syncMaster ({
           self.moveToHome()
       })
    }
    
    //No Next From Logout
    func gotNextFromLogout() {
        
//        let useApp = config.accessConfig.requiredToUseApp
//        if useApp.contains(AppConstants.ScreenCode.Login) {
//            //Login
            moveToLogin(isPopAndSwitch: true)
//        }
//        else{
//            SyncManager.sharedInstance.syncMaster ({
//                self.moveToHome(isPopAndSwitch: true)
//            })
//        }
    }
}

//Mark :- Reservation Condition
extension AppNavigation {
    
    func isValidToReservation() -> Bool {
        
        let requiredForReservation = config.accessConfig.requiredForReservation
        if isRequiredLogin(requiredForReservation) {
            
            //Login
            moveToLogin()
            return false
        }
        else if isRequiredMobileVerificaion(requiredForReservation) {
            
            //Otp
            moveToOtp(isSendOtp: true)
            return false
        }
        else if isRequiredDocumentVerificaion(requiredForReservation) {

            //Document
            moveToDocument()
            return false
        }
        else if isRequiredAddCards(requiredForReservation) {
            
            //Cards
            moveToAddCards()
            return false
        }
        
        //Document Verificatoin
        if requiredForReservation.contains(AppConstants.ScreenCode.DocVerification) {
            
            let isDocuementVerify = ApplicationData.user?.isDocumentVerify ?? false
            if isDocuementVerify == true {
                return true
            }
            
            let isOnfidoRequest = ApplicationData.user?.isOnfidoRequest ?? false
            if isOnfidoRequest == true {
                
                //Show Message
                Utilities.showAlertView(message: "KMsgDocumentVerification".localized)
                return false
            }
            
            //Move To Document
            self.moveToDocument()
        }
        
        return true
    }
}

//Mark :- Use App Condition
extension AppNavigation {
    
    //Use App Login
    func isRequiredLogin(_ parameter : [String]) -> Bool {
        
        if !ApplicationData.isUserLoggedIn && parameter.contains(AppConstants.ScreenCode.Login) {
            return true
        }
        return false
    }
    
    //Use App Mobile Verification
    private func isRequiredMobileVerificaion(_ parameter : [String]) -> Bool {
        
//        let isVerified = ApplicationData.user?.primaryMobile?.isVerified ?? false
//        if ApplicationData.isUserLoggedIn && parameter.contains(AppConstants.ScreenCode.MobileVerification) && isVerified == false {
//            return true
//        }
        return false
    }
    
    //Use App Document Verification
    private func isRequiredDocumentVerificaion(_ parameter : [String]) -> Bool {
        
        let isDocuementVerify = ApplicationData.user?.isDocumentVerify ?? false
        let isOnfidoRequest = ApplicationData.user?.isOnfidoRequest ?? false
        if ApplicationData.isUserLoggedIn && isDocuementVerify == false && isOnfidoRequest == false && parameter.contains(AppConstants.ScreenCode.DocVerification) {
            return true
        }
        return false
    }
    
    //Use App Add Cards
    private func isRequiredAddCards(_ parameter : [String]) -> Bool {
        
        if ApplicationData.isUserLoggedIn && parameter.contains(AppConstants.ScreenCode.PaymentMethod) && SyncManager.sharedInstance.fetchAllCards().count == 0 {
            return true
        }
        return false
    }
}

//Mark :- isSkip Condition
extension AppNavigation {
    
    func isSkipCard() -> Bool {
        
        let useApp = config.accessConfig.allowSkipForLogin
        if useApp.contains(AppConstants.ScreenCode.PaymentMethod) {
            return true
        }
        return false
    }
}

//Mark :- Screen Navigation
extension AppNavigation {
    
    //Move To Login
    private func moveToLogin(isPopAndSwitch : Bool = false) {
        
        //let vc = LoginVC()
        let vc = SignUpVC()
        isPopAndSwitch ? popAllAndSwitchNavigation(vc) : pushNavigation(vc)
    }
    
    //Move To Home
    func moveToHome(isPopAndSwitch : Bool = false) {
        
        let obj = SyncManager.sharedInstance.activeRideObject
        
        ThemeManager.sharedInstance.RideType = obj?.rideType ?? 1

        if (obj?.status == AppConstants.RideStatus.CANCELLED || obj?.status == AppConstants.RideStatus.COMPLETED) && obj?.isPaid == false{
        
            let vc = FareSummaryVC(nibName: "FareSummaryVC", bundle: nil)
            vc.rideModel = obj!
            popAllAndSwitchNavigation(vc)
            
        }else if SyncManager.sharedInstance.arrReserveRides?.count ?? 0 > 0 && obj == nil{
            
            let vc = CarMapVC()
            isPopAndSwitch ? popAllAndSwitchNavigation(vc) : pushNavigation(vc)
            
        } /*else if obj != nil {
            
             let vc = HomeVC()
            isPopAndSwitch ? popAllAndSwitchNavigation(vc) : pushNavigation(vc)
            
        }*/else{
            
            let vc = CarMapVC()
            isPopAndSwitch ? popAllAndSwitchNavigation(vc) : pushNavigation(vc)
            
        }
    }
     
    //Move To Otp
    private func moveToOtp(isPopAndSwitch : Bool = false,isSendOtp : Bool = false,isFromAuth : Bool = false) {
        
        let vc = ForgotPwdOTPVC()
        vc.isFromAuth = isFromAuth
        isPopAndSwitch ? popAllAndSwitchNavigation(vc) : pushNavigation(vc)
    }
    
    //Move To Docuement Verification
    private func moveToDocument(isPopAndSwitch : Bool = false) {
        isPopAndSwitch ? popAllAndSwitchNavigation(OnfidoVC()) : pushNavigation(OnfidoVC())
    }
    
    //Move To Add Card
    private func moveToAddCards(isPopAndSwitch : Bool = false) {
        
        let vc = AddCardViewController()
        vc.isFromAuth = true
        isPopAndSwitch ? popAllAndSwitchNavigation(vc) : pushNavigation(vc)
    }
}

//MARK :- Push And Switch All
extension AppNavigation {
    
    //Push Navigation
    private func pushNavigation(_ vc : UIViewController) {
        UIViewController.current()?.navigationController?.pushViewController(vc, animated: true)
    }
    
    //Pop All And Switch Navigation
    private func popAllAndSwitchNavigation(_ vc : UIViewController) {
        UIViewController.current()?.navigationController?.popAllAndSwitch(to: vc)
    }
}
