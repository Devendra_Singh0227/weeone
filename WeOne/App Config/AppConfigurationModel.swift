//
//  AppConfigurationModel.swift
//  WeOne
//
//  Created by Mayur on 28/11/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class AppConfigurationModel: NSObject {

    static let shared = AppConfigurationModel()
    
    //Variable
    private var arrSocialAuth = [SocialAuthModel]()
    var configs = ConfigModel()
    var rateConfigs = RateConfigModel()
    var accessConfig = AccessConfigModel()
    
    override init() {
        super.init()
        self.setDatafromJSON()
    }
    
    // get Social Auth which are enable
    func getSocialAuth() -> [SocialAuthModel] {
        return arrSocialAuth.filter { $0.enabled == true }
    }
    
    // set data from jsosn file.
    func setDatafromJSON() {

        if let path = Bundle.main.path(forResource: "Configuration", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                if let dict = jsonResult as? [String:Any] {
                    
                    if let social = dict["social_auth"] as? [[String:Any]] {
                        
                        self.arrSocialAuth.removeAll()
                        for obj in social {
                            self.arrSocialAuth.append(SocialAuthModel.init(dict: obj))
                        }
                    }
                    
                    //Config
                    if let configs = dict["configs"] as? [String:Any] {
                        self.configs = ConfigModel.init(dict: configs)
                    }
                    
                    //Rate App Config
                    if let configs = dict["rateAppConfig"] as? [String:Any] {
                        self.rateConfigs = RateConfigModel.init(dict: configs)
                    }
                    
                    //Access Config
                    if let configs = dict["accessConfig"] as? [String:Any] {
                        self.accessConfig = AccessConfigModel.init(dict: configs)
                    }
                }
            } catch {
                //handle error
            }
        }
    }
}

//MARK:- Social Auth Model
class SocialAuthModel:NSObject {
    
    var type:String?
    var credentials:String?
    var local_image:String?
    var enabled:Bool = false
    
    override init() {}
    
    required init(dict:[String:Any]) {
        
        type              = dict["type"] as? String
        credentials       = dict["credentials"] as? String
        local_image       = dict["local_image"] as? String
        enabled           = dict["enabled"] as? Bool ?? false
    }
}

//MARK:- Config Model
class ConfigModel:NSObject {
    
    var navigation_type:String?
    var default_auth:String?
    var language:String?
    var document_required = false
    var isAutoDeduct : Bool = false
    
    override init() {}
    
    required init(dict: [String:Any]) {
        navigation_type     = dict["navigation_type"] as? String
        default_auth        = dict["default_auth"] as? String
        language            = dict["language"] as? String
        document_required   = dict["document_required"] as? Bool ?? false
        isAutoDeduct        = dict["isAutoDeduct"] as? Bool ?? false
    }
}

//MARK:- Rate Config Model
class RateConfigModel:NSObject {
    
    var rate_us_max_app_view_count:Int?
    var rate_us_max_action_count:Int?
    var isEnabled = false
    
    override init() {}
    
    required init(dict: [String:Any]) {
        rate_us_max_app_view_count     = dict["rate_us_max_app_view_count"] as? Int
        rate_us_max_action_count       = dict["rate_us_max_action_count"] as? Int
        isEnabled                      = dict["isEnabled"] as? Bool ?? false
    }
}

//MARK:- Access Config Model
class AccessConfigModel:NSObject {
    
    var requiredToUseApp:[String] = [String]()
    var requiredForReservation:[String] = [String]()
    var allowSkipForLogin:[String] = [String]()
    
    override init() {}
    
    required init(dict: [String:Any]) {
        
        requiredToUseApp        = dict["requiredToUseApp"] as? [String] ?? []
        requiredForReservation  = dict["requiredForReservation"] as? [String] ?? []
        allowSkipForLogin       = dict["allowSkipForLogin"] as? [String] ?? []
    }
}
