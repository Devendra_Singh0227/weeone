//
//  AppConstants.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved.
//-

import Foundation
import UIKit

let appDelegate = UIApplication.shared.delegate as! AppDelegate

struct AppConstants {

    #if DEBUG // Debug Mode
    
//    static let serverURL                           = "https://apis.weone.io" //Live
//    static let imageURL                            = "https://api.weone.io/" //Live

//    static let serverURL                           = "http://60.254.95.5:7037" //Staging
//    static let imageURL                            = "http://60.254.95.5:7037" //Staging

    static let serverURL                          = "http://d942234d0766.ngrok.io"
//    static let serverURL                          = "https://apis.weone.io"

    //"http://60.254.95.5:7077" // New Staging
    static let imageURL                           = "https://api.weone.io"
    //"http://60.254.95.5:7077" //New Staging

    #else // Release Mode

    //LIVE
    static let serverURL                            = "https://apis.weone.io"
    static let imageURL                             = "https://api.weone.io/"

    #endif

    static let iTunesUrl                            = ""
    static let AppName                              = "Weone"
    static let startingDate                         = "1970-01-01T00:00:00.000Z"
    static let StripeTestKey                        = "pk_test_34z4jH0FLe5o5vpLxf5u5H1K00I0qvN8Kk"
    static let LiveAppIdOneSignal                   = "7db45759-e1e3-46ae-ae76-8a0a68b9bf32"

    static let GetPricePackageConfigHour            = 24
    static let GetPricePackageConfigWeek            = 7
    static let GetPricePackageConfigMonths          = 30

    static let DeviceTypeIphone                     = 3
    
    static let googleApiKey                         = "AIzaSyBHxTi0lrJ3Z_LCrdVYBVcsspSMsI-UVSM"
    static let googleApiKeyForImage                 = "AIzaSyA09Yz-khCxqLmUpWRZup8as20GAXkaVEA"
    static let googleApiKeyForReverseGeoCode        = "AIzaSyDcdKowxWXK3Xq3n0K8Hrvz41MOdheKtpA"


    //MARK: Currency
    static let USD                                  = "USD"
    static let NOK                                  = "NOK"
    
    //Schedule Interval
    // here interval set in minute for schedule
    static let ScheduleMinuteInterval               = 60
    
    //Booking available time
    static let NextBookingTotalHours                = 24
    
    //Contact Email
    static let ContactNo                            = "+498920180280"
    static let ContactEmail                         = "info@weone.com"
    
    //Default zoom level
    static var DefaultZoomLevel : Float             = 10.0
    
    //MARK:  API
    struct URL {
        
        static let UploadFile                       = "/api/v1/upload-file"
        static let Login                            = "/api/v1/customer/login"
        static let Signup                           = "/api/device/customer/create"
        static let checkEmail                       = "/api/device/customer/check-email-exist"
        static let sendVerifyemail                  = "/api/device/customer/send-verification-email"
        static let Logout                           = "/api/device/customer/logout"
        static let ForgorPassword                   = "/api/v1/customer/forgot-password"
        static let UpsertPlayerId                   = "/api/v1/customer/upsert-playerId"
        static let AddCard                          = "/api/device/customer/add-cards"
        static let MasterSync                       = "/api/v1/sync"
        static let getCards                         = "/api/device/customer/list-cards"
        static let DeleteCard                       = "/api/device/customer/delete-card/"
        static let SetDefaultCard                   = "/api/v1/customer/set-default-customer-card"
        static let VerifyResetOTP                   = "/api/device/customer/verify-otp"
        static let ResendOtp                        = "/api/v1/customer/mobile-verification-otp"
        static let ResetPassword                    = "/api/v1/customer/reset-password-without-login"
        static let ChangePassword                   = "/api/v1/customer/update-password-by-user"
        static let VerifyOTP                        = "/api/v1/customer/verify-mobile"
        static let SendOTP                          = "/api/device/customer/send-otp"
        static let RideList                         = "/api/v1/customer/ride-booking/ride-list"
        static let UpdateProfile                    = "/api/device/customer/update-profile"
        static let getProfile                       = "/api/device/customer/fetch-user-profile"
        static let Upsert                           = "/api/v1/customer-setting/upsert"

        //Maps api
        static let GetNearbyCityVehicle             = "/api/device/vehicle/find-nearby-city-vehicle"
        
        static let GetCities                        = "/api/v1/customer/ride-booking/get-cities"
        static let AvailableVehicleInCity           = "/api/v1/customer/ride-booking/available-vehicle-in-city"
        static let NotifyWhenAvailable              = "/api/v1/customer/ride-booking/notify-when-available"

        static let GetStationFromCity               = "/api/v1/customer/ride-booking/get-stations-of-city"
        static let GetVehicleDetail                 = "/api/device/vehicle/vehicle-detail"
        static let InspectCar                       = "/api/device/vehicle/vehicle-inspection"
        
        //Onfido Api
        static let CreateApplicant                  = "/api/v1/customer/create-applicant"
        static let GenerateToken                    = "/api/v1/customer/generate-onfido-sdk-token"
        static let CreateOnfidoCheck                = "/api/v1/customer/create-onfido-check"
        
        //Ride Api's
        static let ReserveRide                      = "/api/v1/customer/ride-booking/reserve-ride"
        static let StartRide                        = "/api/v1/customer/ride-booking/start-ride"
        static let CancelRideDetail                 = "/api/v1/customer/ride-booking/cancel-ride-detail"

        static let PauseRide                        = "/api/v1/customer/ride-booking/pause-ride"
        static let ResumeRide                       = "/api/v1/customer/ride-booking/resume-ride"
        static let StopRide                         = "/api/v1/customer/ride-booking/stop-ride"

        static let CancelRide                       = "/api/v1/customer/ride-booking/cancel-ride"
        static let FindCar                          = "/api/v1/customer/ride-booking/find-ride-by-horn"
        static let UpdateEndLocation                = "/api/v1/customer/ride-booking/update-end-location"
        static let UpdateStartLocation              = "/api/v1/customer/ride-booking/update-start-location"
        static let UpdateNavigateLocation           = "/api/v1/customer/ride-booking/update-navigate-location"

        static let AddRemoveFavPlace                = "/api/v1/customer/add-remove-favourite-place"

        static let ExtendReservation                = "/api/v1/customer/ride-booking/extend-reserve-time"
        static let MakePayment                      = "/api/v1/customer/ride-booking/make-payment"
        static let GiveRating                       = "/api/v1/customer/rating-review/upsert"
        static let ContactUs                        = "/api/v1/customer/contact-us"
        
        //Store player id api
        static let StorePlayerId                    = "/api/v1/customer/upsert-playerId"
        
        //Damage Report
        static let AddDamageReoport                 = "/api/v1/damage-report/add"
        static let DamageReportList                 = "/api/v1/damage-report/list"
        static let FAQ                              = "/api/v1/faqs/paginate"
        static let NotificationList                 = "/api/device/customer/notifications"
        static let NotificationStatusUpdate         = "/api/device/customer/update-notification/"
        static let NotificationDelete               = "/api/device/customer/delete-notification/"
        static let NotificationDeleteSelected       = "/api/device/customer/delete-selected-notifications"
        static let GetFareSummary                   = "/api/v1/customer/reserve-ride/fare-summary"
        
        //StationOfPartner
        static let StationOfPartner                 = "/api/v1/customer/ride-booking/get-stations-of-partner"
        
        static let StaticPage                       = "/api/v1/static-page/"
        static let SupportPage                      = "/api/v1/support-page/list"

        static let InverseLockUnlock                = "/iot/inverse-lock-unlock"
        
        static let GetPreviousRental                = "/api/v1/customer/ride-booking/get-previous-rentals"
        static let GetReferralCode                  = "/api/v1/customer/send-invitation-link"
        static let SendReceipt                      = "/api/v1/customer/ride-booking/send-receipt"

    }

    //Static pages
    struct StaticPage{
        static let AboutUs                          = ""
        static let PrivacyPolicy                    = "/webview/privacy"
        static let Legal                            = ""
        static let TermsCondition                   = "/webview/terms-n-condition"
    }
    
    
    struct StaticCode {
        static let AboutUs                          = "ABOUT_US"
        static let PrivacyPolicy                    = "PRIVACY_POLICY"
        static let Legal                            = "LEGAL"
        static let TermsCondition                   = "TERMS_CONDITION"
        static let InsuranceTerms                   = "INSURANCE_TERMS"
        static let Copyright                        = "Copyright"
        static let DataProvider                     = "Data Provider"
        static let SoftwareLicences                 = "Software Licences"
        static let LocationInformation              = "Location Information"
        
    }

    
    //MARK:  User Default
    struct UserDefaultKey {
        
        static let isUserLoggedIn                    = "isUserLoggedIn"
        static let isUserLoggedOut                   = "isUserLoggedOut"
        static let isNewUser                         = "isNewUser"
        static let isTokenExpired                    = "isTokenExpired"
        static let UserInfo                          = "UserInfo"
        static let tokenKey                          = "token"
        static let SkipUpdate                        = "SkipUpdate"
        static let ZoomLevel                         = "ZoomLevel"
        static let DontShowImplementBookingPopup     = "DontShowImplementBookingPopup"
        static let Filter                            = "Filter"
        static let PlayerId                          = "PlayerId"
        static let SelectedCard                      = "SelectedCard"
        static let location_access                   = "location_access"
        static let FirstName                         = "FirstName"
        static let LastName                          = "Last_Name"
        static let ProfilePic                        = "ProfilePic"
        static let Email                             = "Email"
        static let Mobile                            = "Mobile"
        static let Code                              = "CountryCode"
        static let isMobileVerified                  = "isMobileVerified"
        static let vehicleId                         = "vehicleId"
        static let DeviceToken                       = "DeviceToken"
    }

    //MARK: Marker Types
    struct MarkerType{
        static let Vehicle          = 1
        static let VehicleStation   = 2
        static let FuelStation      = 3
        static let EvChargeStations = 4
        static let City             = 5
    }
    
    //MARK: Master Constants
    struct MasterCode {
        static let Brand                            = "BRAND"
        static let VehicleCategory                  = "VEHICLE_CATEGORY"
        static let RatingType                       = "RATING_TYPE"
        static let ChildSeats                       = "CHILD_SEATS"
        static let Fuel                             = "fuel"
        static let EquipmentType                    = "EquipmentType"
        static let Transmission                     = "Transmission"
        static let Other                            = "Other"
        static let Price                            = "Price"
    }

    //MARK: Image Upload Max limit for controller
    struct ImageUploadMaxLimit {
        
        static let ReportDamage = 2
        static let EndRide = 4
        static let OdometerImage = 1
    }
    
    //MARK: Card type
    struct CardTypes{
        static let BusinessCard                     = 0
        static let PersonalCard                     = 1
        static let JobCard                          = 2
        static let All                              = 3

    }
    
    //MARK: Fule Type
    struct FuleType{
        static let COMBUSTION                       = 1
        static let ELECTRIC                         = 2
    }
    
    //MARK: Fule Type
    struct GearBoxType{
        static let AUTOMATIC                        = 1
        static let MANUAL                           = 2
    }
    
    struct Command{
        static let CENTRAL_LOCK                     = 1
        static let IMMOBILIZER                      = 2
    }

    
    struct Operation{
        static let LOCK                            = 1
        static let UNLOCK                          = 2
    }

    
    struct CarAttribute {
        static let LOADINGFACILITY = 1
        static let TOWITCH = 2
        static let ALLWHEELDRIVE = 3
        static let CARGO = 4
        static let WINTERTYRE = 7
        static let SUITECASE = 8
        static let FUEL = 9
        static let FUEL_SUBTYPE = 10
        static let TRANSMISSION = 11
    }


    struct CarKeyTypes {
        static let AUTOMATIC                       = 1
        static let MANUAL                          = 2
    }


    //MARK: Fuel Type
    struct FuelType {
        static let Combustion                       = 1
        static let Electric                         = 2
    }
    
    //MARK: Fuel Type
    struct IOTProvider {
        static let ShareBoxKeySharing                = 1
        static let ManualKeySharing                  = 3
        static let WithTelemetric                    = 2
    }
    
    //MARK: Damage report
    struct ReportDamage{
        
        static let DIRTY_INTERIOR                   = 1
        static let DAMAGE                           = 2
        static let DIRTY_EXTERIOR                   = 3
        static let REFILL_WINDSCREEN_WASHER_FLUID   = 5
        
    }
    
    //MARK: StaticPage constants

    struct StaticPageCode {
        static let Payment_Pricing = "PAYMENT_PRICING"
        static let Account = "ACCOUNT"
        static let Using_Weone = "USING_WEONE"
        static let Legal = "LEGAL"
    }

    //MARK: Notification constants
    struct Notification {
        
        static let resetMenu                        = "resetMenu"
    }
    
    //MARK: Sync Date
    struct SyncDate {
        
        static let master                           = "masterSyncDate"
    }
    
    //MARK:  User Default
    struct Language {
        
        static let kLMEnglish               = "ENGLISH"
        static let kLMFrench                = "CHINESE"
        static let kLMEnglishCode               = "EN"
        static let kLMChineseSimplifiedCode     = "CH"
    }
    
    struct PageLimit{
        static let page                             = 1
        static let limit                            = 100
    }
    
    //MARK: Currency constants
    struct Currency {
        static let NOK                              = 1
        static let USD                              = 2
    }
    
    //MARK: Ride Type Constants
    struct RideType {
        static let Instant                          = 1
        static let Schedule                         = 2
    }
    
    struct AuthType {
        static let Login                          = 1
        static let Signup                         = 2
    }

    struct RentalImageType {
        static let FRONT                    = 1
        static let BACK                     = 2
        static let LEFT                     = 3
        static let RIGHT                    = 4
    }

    struct DamageImageType {
        static let FULL_CAR                 = 1
        static let CLOSE_UP                 = 2
        static let OTHER                    = 3
        
    }

    struct DamageCategory {
        static let SCRATCH                          = 1
        static let DENT                             = 2
        static let CHIP                             = 3
        static let MISSING_PART                     = 4
        static let BROKEN_PART                      = 5
        static let INTERIOR                         = 6
    }
    
    //MARK:- Notification Status Constant
    struct NotificationStatus {
        static let SEND                             = 1
        static let READ                             = 2
        static let SELECTED                         = 3
    }
    
    
    //MARK:- Device Type Constant
    struct DeviceTypeConstant {
        static let Android                       = 2
        static let IOS                           = 3
    }
    
    struct RideStatus {
        static let RESERVED =  1
        static let START_REQUESTED = 2
        static let ON_GOING =  3
        static let COMPLETED = 4
        static let CANCELLED = 5
    }
    
    struct Action {
        static let FAVOURITE =  1
        static let REMOVE_FROM_FAVOURITE = 2
    }

    //View Controller Code
    struct ScreenCode {
        
        static let Login               = "LOGIN"
        static let MobileVerification  = "MOBILE_VERIFICATION"
        static let DocVerification     = "DOC_VERIFICATION"
        static let PaymentMethod       = "PAYMENT_METHOD"
    }
    
    //MARK: Device Constant
    enum UIUserInterfaceIdiom : Int {
        case Unspecified
        case Phone
        case Pad
    }
    
    struct ScreenSize {
        static var SCREEN_WIDTH: CGFloat {
            get {
                return UIScreen.main.bounds.size.width
            }
        }
        
        static var SCREEN_HEIGHT: CGFloat {
            get {
                return UIScreen.main.bounds.size.height
            }
        }
        
        static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
        static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    }
    
    struct DeviceType {
        static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
        static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
        static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
        static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
        static let IS_IPHONE_X          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
        static let IS_IPHONE_XS         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
        static let IS_IPHONE_XR         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
        static let IS_IPHONE_XS_MAX     = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0
        static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad
    }
    
    static var hasSafeArea: Bool {
        if #available(iOS 11.0,  *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        
        return false
    }
    
}

