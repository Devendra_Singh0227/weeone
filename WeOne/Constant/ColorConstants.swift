//
//  ColorConstants.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved.
//

import Foundation
import UIKit

/** This colors struct is used to define colors for application. */

class ThemeManager {
    
     static let sharedInstance = ThemeManager()

    static var DefaultTheme = -1

    var RideType = DefaultTheme
    
    static var instantImage = ["fuel": "fuel",
                               "appLogo": "DriveNowLogo",
                               "Information": "DriveNowInfo",
                               "markerCarKey": "DrivenowKeyPin",
                               "markerFuelStation": "DriveNowFuelMarker",
                               "markerChargeStation": "DriveNowChargeMarker",
                               "checked": "DamageDriveNowChecked",
                               "SelectedMarker": "DriveNowSelectedCarMarker",
                               "gps": "DriveNowgps",
                               "UpArrow": "DriveNowUpArrow",
                               "Fav": "ScheduleFavorite",
                               "Swap": "DrivenowSwap",
                               "Add": "DriveNowAdd",
                               "Minus": "DriveNowMinus",
                               "Pin": "DrivenowPin",
                               "Capture": "DriveNowCapture",
                               "Tick": "DriveNowTick",
                               "Dollor": "DriveNowDollar",
                               "Question": "DriveNowQuestion",
                               "CircleChecked": "DriveNowCircleChecked",
                               "Heart": "DrivewNowHeart"]
    static var scheduleImage = ["fuel": "electric-station",
                                "appLogo": "ScheduleLogo",
                                "Information": "ScheduleInfo",
                                "markerCarKey": "ScheduleKeyPin",
                                "markerFuelStation": "ScheduleFuelMarker",
                                "markerChargeStation": "ScheduleChargeMarker",
                                "checked": "DamageScheduleChecked",
                                "SelectedMarker": "ScheduleSelectedCarMarker",
                                "gps": "Schedulegps",
                                "UpArrow": "ScheduleUpArrow",
                                "Fav": "DrivenowFavourite",
                                "Swap": "ScheduleSwap",
                                "Add": "ScheduleAdd",
                                "Minus": "ScheduleMinus",
                                "Pin": "SchedulePin",
                                "Capture": "ScheduleCapture",
                                "Tick": "ScheduleTick",
                                "Dollor": "ScheduleDollar",
                                "Question": "ScheduleQuestion",
                                "CircleChecked": "ScheduleCircleChecked",
                                "Heart": "ScheduleHeart"]

    func getImage(string: String) -> String {
        if ThemeManager.sharedInstance.RideType == AppConstants.RideType.Schedule {
            return ThemeManager.scheduleImage[string] ?? ""
        } else {
            return  ThemeManager.instantImage[string] ?? ""
        }
    }
}

struct ColorConstants {
    
    static var ThemeColor: UIColor {
        
        get {
            
            if ThemeManager.sharedInstance.RideType == AppConstants.RideType.Schedule {
                return #colorLiteral(red: 0.5803921569, green: 0, blue: 0.6196078431, alpha: 1)
            } else {
                return #colorLiteral(red: 0.8941176471, green: 0.07450980392, blue: 0.7647058824, alpha: 1) //#E413C3
            }
        }
    }
    
    static var TextColorTheme: UIColor {
        
        get {
            
            if ThemeManager.sharedInstance.RideType == AppConstants.RideType.Schedule {
                return #colorLiteral(red: 0.5803921569, green: 0, blue: 0.6196078431, alpha: 1)
            } else {
                return #colorLiteral(red: 0.8941176471, green: 0.07450980392, blue: 0.7647058824, alpha: 1) //#E413C3
            }
        }
    }

    static let DriveNowThemeColor = #colorLiteral(red: 0.8941176471, green: 0.07450980392, blue: 0.7647058824, alpha: 1) //#E413C3
    static let ScheduleThemeColor = #colorLiteral(red: 0.5803921569, green: 0, blue: 0.6196078431, alpha: 1) //#94009E

    static let ThemeGradientStartColor                = #colorLiteral(red: 0.1294117647, green: 0.1294117647, blue: 0.1294117647, alpha: 1) //#212121
    static let ThemeGradientEndColor                  = #colorLiteral(red: 0.1294117647, green: 0.1294117647, blue: 0.1294117647, alpha: 1) //#212121

    static let ThemeColorSecondary = #colorLiteral(red: 0.8941176471, green: 0.07450980392, blue: 0.7647058824, alpha: 1) //#E413C3
   
    static let TextColorGreen = #colorLiteral(red: 0.2117647059, green: 0.8470588235, blue: 0.8862745098, alpha: 1) //#36d8e2
    
    static let TextColorPrimary = #colorLiteral(red: 0.6078431373, green: 0.3058823529, blue: 0.8941176471, alpha: 1) //9b4ee4
    static let TextColorSecondary = #colorLiteral(red: 0.4823529412, green: 0.4823529412, blue: 0.4823529412, alpha: 1) //7B7B7B
    static let TextColorSecondaryAlpha = #colorLiteral(red: 0.4823529412, green: 0.4823529412, blue: 0.4823529412, alpha: 0.6) //7B7B7B

    static let BorderColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1) //D9D9D9
    static let ButtonBorderColor = #colorLiteral(red: 0.4823529412, green: 0.4823529412, blue: 0.4823529412, alpha: 1) //#7B7B7B
    static let UnderlineColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1) //D9D9D9
    
    static let commonWhite = #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.9882352941, alpha: 1)
    static let inputbackground = UIColor(red: 0.969, green: 0.969, blue: 0.988, alpha: 1)//colorLiteral(red: 0.969, green: 0.969, blue: 0.988, alpha: 1)
    static let textcolor = #colorLiteral(red: 0.2484484999, green: 0.2853851547, blue: 0.3030885687, alpha: 1)
    static let secondTextColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
    
    static let TextTitleBlack = #colorLiteral(red: 0.1294117647, green: 0.1294117647, blue: 0.1294117647, alpha: 1) //212121
    static let TextColorWhitePrimary = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) //FFFFFF
    static let TextColorError = #colorLiteral(red: 1, green: 0.2431372549, blue: 0.2941176471, alpha: 1) //FF3E4B
    static let TextColorPrimaryAlpha = #colorLiteral(red: 0.1607843137, green: 0.162381798, blue: 0.1674563885, alpha: 0.3) //1F1F20
    static let NotificationBGColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1) //F2F2F2
    static let BGCancelColor = #colorLiteral(red: 0.9764705882, green: 0.4, blue: 0.4, alpha: 1) //#F96666
    static let ShadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3) //#000000 30%
    
    //CFDAD9
    static let bgColorSecondary             = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1) //D9D9D9
    static let bgColorPrimary               = #colorLiteral(red: 0.462745098, green: 0.3764705882, blue: 0.9490196078, alpha: 1) //7660F2
    static let tooltipcolor                 = #colorLiteral(red: 0.7490196078, green: 0.8, blue: 0.8509803922, alpha: 1)
    static let TextGrayColor                = #colorLiteral(red: 0.5294117647, green: 0.537254902, blue: 0.6039215686, alpha: 1)
    static let textbackgroundcolor          = #colorLiteral(red: 0.9750944972, green: 0.9756103158, blue: 0.9907335639, alpha: 1)

    static let CardActiveField              = #colorLiteral(red: 0.3254901961, green: 0.7568627451, blue: 0.5960784314, alpha: 1)
    static let CarPartsUnSelect             = #colorLiteral(red: 0.5568627451, green: 0.5568627451, blue: 0.5568627451, alpha: 1)
    static let CarPartsSelect               = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    
    //********
    static let TextLeftMenuColor            = #colorLiteral(red: 0.4117647059, green: 0.4745098039, blue: 0.5215686275, alpha: 1)
    static let ButtonBGColor                = #colorLiteral(red: 0.462745098, green: 0.3764705882, blue: 0.9490196078, alpha: 1)
    static let ButtonBGPurpleColor          = #colorLiteral(red: 0.2549019608, green: 0.3098039216, blue: 0.6235294118, alpha: 1)
    static let BgColor                      = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.2)
    static let DarkBorderColor              = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
    static let PlaceHolderColor             = #colorLiteral(red: 0.5450980392, green: 0.5450980392, blue: 0.5450980392, alpha: 0.85)

    // Screen Wise
    //Payment
    struct Payment {
        
        static let PaymentTextBlack        = #colorLiteral(red: 0.01568627451, green: 0.02745098039, blue: 0.03137254902, alpha: 1)
        static let PaymentTextPurple       = #colorLiteral(red: 0.2549019608, green: 0.3098039216, blue: 0.6235294118, alpha: 1)
        static let PaymentTextSection      = #colorLiteral(red: 0.6862745098, green: 0.7411764706, blue: 0.8039215686, alpha: 1)
        static let PaymentBorderColor      = #colorLiteral(red: 0.9098039216, green: 0.9098039216, blue: 0.9098039216, alpha: 1)
        static let PaymentBGColor          = #colorLiteral(red: 0.9607843137, green: 0.968627451, blue: 0.9803921569, alpha: 1)
    }
    
    //Redeem Voucher
    struct RedeemVoucher {
        
        static let RedeemVoucherBGColor    = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        static let RedeemPlaceHolderColor  = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.65)
    }
    
    //Free Trips
    struct FreeTrips {
        
        static let FreeTripsBorderColor    = #colorLiteral(red: 0.8235294118, green: 0.8235294118, blue: 0.8235294118, alpha: 1)
    }
    
    //Filter
    struct Filter {
        
        static let FilterSelectedTypeBGColor    = #colorLiteral(red: 0.7647058824, green: 0.8862745098, blue: 0.9647058824, alpha: 1)
        static let FilterTypeBGColor            = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
        static let FilterBorderBGColor          = #colorLiteral(red: 0.862745098, green: 0.862745098, blue: 0.862745098, alpha: 1)
        static let FilterSepratorBGColor        = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.2)
        static let FilterNormalImageColor       = #colorLiteral(red: 0, green: 0.007843137255, blue: 0.1843137255, alpha: 1)
    }
    
    //Otp
    struct Otp {
        
        static let BgColor    = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9803921569, alpha: 1)
    }
    
    //MoreInformtion
    struct MoreInformtion {
        
        static let TitleTextBoldColor           = #colorLiteral(red: 0.09803921569, green: 0.1098039216, blue: 0.1529411765, alpha: 1)
        static let DescTextColor                = #colorLiteral(red: 0.3137254902, green: 0.3098039216, blue: 0.3098039216, alpha: 1)
        static let MIBorderColor                = #colorLiteral(red: 0.8823529412, green: 0.8823529412, blue: 0.8823529412, alpha: 1)
        static let MICardTextColor              = #colorLiteral(red: 0.5058823529, green: 0.5725490196, blue: 0.6862745098, alpha: 1)
    }
    
    
    struct DetailPopup {
        static let DPTextColor               = #colorLiteral(red: 0.05882352941, green: 0.05882352941, blue: 0.05882352941, alpha: 1)
    }
    
}


extension UIColor {
    
    convenience init(_ hexString: String, alpha: Double = 1.0) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (r, g, b) = ((int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (r, g, b) = (int >> 16, int >> 8 & 0xFF, int & 0xFF)
        default:
            (r, g, b) = (1, 1, 0)
        }
        
        self.init(displayP3Red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(255 * alpha) / 255)
    }
}
