//
//  StringConstants.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved.

// DO NOT CHANGE ANY CONSTANT WITHOUT DISCUSSION OF TEAM ///////////

import Foundation

struct StringConstants {
    
    ///// DO NOT CHANGE ANY CONSTANT WITHOUT DISCUSSION OF TEAM ///////////
    static let pageLimit                                = 20
    static let CountryCode                              = "+91"
    static let DownloadFolderName                       = "Downloads"
    static let Other                                    = "other"
    
    //Common
    
    struct validationerror {
         static let validEmail = "Enter valid email"
         static let validphone = "Enter valid Phone number"
         static let validFirstname = "Enter First Name"
         static let validLastname = "Enter Last Name"
    }
    
    struct Common {
       
        static var KNoAppStoreUrl : String {
            get { return "KNoAppStoreUrl".localized }
        }

        static var KNoInternetConnection : String {
            get { return "KNoInternetConnection".localized }
        }
        
        static var KNoInternetConnectionTitle : String {
            get { return "KNoInternetConnectionTitle".localized }
        }
        
        static var KNoInternetConnectionMsg : String {
            get { return "KNoInternetConnectionMsg".localized }
        }
        
        static var KMsgOpenSettings : String {
            get { return String(format : "KMsgOpenSettings".localized, Bundle.main.infoDictionary!["CFBundleName"] as! String) }
        }
        
        static var KMsgCameraPermission : String {
            get { return "KMsgCameraPermission".localized }
        }
        
        static var KMsgFailedToGetCamera : String {
            get { return "KMsgFailedToGetCamera".localized }
        }
        
        static var KLblSomethingWentWrong : String {
            get { return "KLblSomethingWentWrong".localized }
        }

        static var KMsgAccessDenied : String {
            get { return "KMsgAccessDenied".localized }
        }
        
        static var KLblLogoutMsg : String {
            get { return "KLblLogoutMsg".localized }
        }
    }
    
    //MARK: Button Title
    struct ButtonTitles {
        static var kClear : String {
            get { return "KLblClear".localized }
        }
        
        static var KConfirm : String {
            get { return "KLblConfirm".localized }
        }
        
        static var KCancel : String {
            get { return "KLblCancel".localized }
        }
        
        static var kRemove : String {
            get { return "KLblRemove".localized }
        }
        
        static var kYes : String {
            get { return "KLblYes".localized }
        }
        
        static var kNo : String {
            get { return "KLblNo".localized }
        }
        
        static var kAll : String {
            get { return "KLblAll".localized }
        }
        
        static var kCurrent : String {
            get { return "KLblCurrent".localized }
        }
        
        static var kClose : String {
            get { return "KLblClose".localized }
        }
        
        static var kView : String {
            get { return "KLblView".localized }
        }
        
        static var kRetry : String {
            get { return "KLblRetry".localized }
        }
        
        static var kApply : String {
            get { return "KLblApply".localized }
        }
        
        static var kDone : String {
            get { return "KLblDone".localized }
        }
        
        static var kNext : String {
            get { return "KLblNext".localized }
        }
        
        static var kSubmit : String {
            get { return "KLblSubmit".localized }
        }
        
        static var KOk : String {
            get { return "KLblOk".localized }
        }
        
        static var KDelete : String {
            get { return "KLblDelete".localized }
        }
        
        static var kRefersh : String {
            get { return "KLblRefresh".localized }
        }
        
        static var kAdd : String {
            get { return "KLblAdd".localized }
        }
    }
    
    // MeasurementUnit
    struct MeasurementUnit {
        static var KLblKm : String {
            get { return "KLblKm".localized }
        }
        static var KLblMin : String {
            get { return "KLblMin".localized }
        }
        
        static var KLblKilometers : String {
            get { return "KLblKilometers".localized }
        }
        static var KLblMinutes : String {
            get { return "KLblMinutes".localized }
        }
    }
    
    //NoData
    struct Nodata {
        
        static var NoDataFound : String {
            get { return "KMsgNoDataFound".localized }
        }
        
        static var kNoInternet : String {
            get { return "KMsgNoInternetConnection".localized }
        }
        
        static var KMsgNoUpcomingRideFound : String {
            get { return "KMsgNoUpcomingRideFound".localized }
        }
        
        static var KMsgNoPastRideFound : String {
            get { return "KMsgNoPastRideFound".localized }
        }
        
        static var KMsgNoNotification : String {
            get { return "KMsgNotificationYet".localized }
        }
        
        static var KNoRide : String {
            get { return "KLblNoRide".localized }
        }
        
        static var KNoPersonalRide : String {
            get { return "KLblNoPersonalRide".localized }
        }
        
        static var KNoBussinessRide : String {
            get { return "KLblNoBussinessRide".localized }
        }
    }
    
    //Make Payment
    struct MakePayment {
        
        static var KLblEnterCardNumber : String {
            get { return "KLblEnterCardNumber".localized }
        }
        
        static var KLblEnterValidCardNumber : String {
            get { return "KLblEnterValidCardNumber".localized }
        }
        
        static var KLblEnterHolderName : String {
            get { return "KLblEnterHolderName".localized }
        }
        
        static var KLblEnterExpiryDate : String {
            get { return "KLblEnterExpiryDate".localized }
        }
        
        static var KLblEnterValidExpiryDate : String {
            get { return "KLblEnterValidExpiryDate".localized }
        }
        
        static var KLblEnterCVVCode : String {
            get { return "KLblEnterCVVCode".localized }
        }
        
        static var KLblEnterValidCVVCode : String {
            get { return "KLblEnterValidCVVCode".localized }
        }
        
        static var KLblEnterPromoCode : String {
            get { return "KLblEnterPromoCode".localized }
        }
        
        static var KLblDeleteCardConfirmation : String {
            get { return "KLblDeleteCardConfirmation".localized }
        }
        
        static var KLblDeleteAllCardConfirmation : String {
            get { return "KLblDeleteAllCardConfirmation".localized }
        }
        
        static var KLblRemoveCard : String {
            get { return "KLblRemoveCard".localized }
        }
        

        static var KMsgPrimaryCard : String {
            get { return "KMsgPrimaryCard".localized }
        }
    }
    
    //OTP
    struct Otp {
        
        static var KLblIncorrectOTP : String {
            get {  return "KLblIncorrectOTP".localized }
        }
        
        static var KLblEnterOTP : String {
            get {  return "KLblEnterOTP".localized }
        }
    }
    
    //Change Password
    struct ChangePassword {
        
        static var kOldPassword         : String {
            get { return "kEnterOldPassword".localized }
        }
        
        static var kNewPassword         : String {
            get { return "kEnterNewPassword".localized }
        }
        
        static var kConfirmPassword     : String {
            get { return "kEnterConfirmPassword".localized }
        }
        
        static var kNewPasswordMismatch : String {
            get { return "kNewPasswordMismatch".localized }
        }
        
        static var kOldPasswordMismatch : String {
            get { return "kOldPasswordMismatch".localized }
        }
        
    }
    
    //MARK: Upload license
    struct UploadLicense {
        
        static var KLblCameraNotAvailable : String {
            get { return "KLblCameraNotAvailable".localized }
        }
        
        static var KLblGallaryPermissionTitle : String {
            get { return "KLblGallaryPermissionTitle".localized }
        }
        
        static var KLblGalleryPermission : String {
            get { return "KLblGalleryPermission".localized }
        }
        
        static var KMsgCameraPermission : String {
            get { return "KMsgCameraPermission".localized }
        }
        
        static var KLblPickOrClickImage : String {
            get { return "KLblPickOrClickImage".localized }
        }
    }
    
    //MARK: Home view messagew
    struct Home {
        
        static var kLocationPermission : String {
            get { return String(format : "KMsgLocationPermission".localized, Bundle.main.infoDictionary!["CFBundleName"] as! String) }
        }
    }
    
    struct ExpandableLabelConstant {
        
//        static let attribute = [ NSAttributedString.Key.font: FontScheme.kMediumFont(size: 15), .foregroundColor:ColorConstants.ThemeColor ]
        
        static var KReadMore : String {
            get { return "KReadMore".localized }
        }
        
        static var KReadLess : String {
            get { return "KReadLess".localized }
        }
        
//        static let attributedMoreText           = NSAttributedString(string: KReadMore, attributes: attribute)
//        static let attributedLessText           = NSAttributedString(string: KReadLess, attributes: attribute)
        
    }
}
