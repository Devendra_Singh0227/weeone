//
//  EnumConstant.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved.
//

import UIKit
import AVKit


//MARK: All Cell Enum
enum CellType {
    
    //Sign up
    case SignUpFirstName
    case SignUpLastName
    case SignUpEmail
    case SignUpPhone
    case SignUpPassword
    case SignUpFooter
    case SignUpChangePassword
    
    //Left Menu
    case LMPayment
    case LMYourTrips
    case LMSettings
    case LMLegal
    case LMSupport
    case LMFreeRides
    case LMFaq
    case LMNotification
    case LMHelp
    case LMChat
    case LMContactUs
    case LMAbout
    case LMPromos
    //DamageReportVC
    case DamageReportList
    
    //Cancellation Detail
    case CancellationTripInfo
    case CancellationTotal

    
    //Car Reserve
    case CarReserveDirectionCell
    case CarReserveReduceExcessCell
    
    //Payment
    case PaymentSection
    case PaymentCard
    case PaymentPersonalCard
    case PaymentJobCard
    case PaymentCardTitle
    case PaymentAddNewCard
    case PaymentAddNewProfile
    case PaymentPromoCode
    case PaymentVouchers
    case PaymentPersonalProfile
    case PaymentBusinessProfile
    case PaymentCreditCell
    case SeparatorCell

    //Free Trips
    case FreeTripsStaticText
    case FreeTripsImage
    case FreeTripsShareCode
    
    //Filter
    case FilterType
    case FilterBrands
    case FilterChildSeats
    case FilterSpecials
    case FilterKeys

    case FilterTransmission
    case FilterFualType
    case FilterFuleCharge
    
    //Fare summary
    case FareSummaryCarInfo
    case FareSummarySection
    case FareSummaryTripInfo
    case FareSummaryCurrentlocation
    case FareSummaryDestination
    case FareSummaryPaymentBreakdown
    case FareSummaryCardSelection
    case FareSummaryChangeCard
    case FareSummaryReleaseAmount
    case FareSummaryAlreadyPaid

    
    //IssueWithLastTrip
    case IssueWithLastTripTotal
    case IssueWithTripInfo
    case IssueWithMore
    case IssueNeedHelp

    //Report issue select part
    case ReportIssueScratch
    case ReportIssueDent
    case ReportIssueChip
    case ReportIssueMissingPart
    case ReportIssueBrokenPart
    case ReportIssueInterior
    case ReportIssueCancel
    
    //Car Features
    case CFFamilyCar
    case CFWinterTires
    case CFFiveSeat
    case CFTowbar
    case CFAutomaticTransmission
    case CFThreeSuitcase
    case CFExtraDetail
    case CFFuelType
    case CFCarKey
    case CFLoadingFacility
    case CFAllWheel
    case CFCargo
    case CFRoofRack
    case CFTowHitch
    
    //Car More Information
    case CMIFeature
    case CMITitleDesc
    case CMFuelRule
    case CMILatestDeliver
    case CMIPackage
    case CMIPackageTotal

    case CMIEmptySpace
    case CMIInsurance
    case CMICancellationInfo
    case CMICard
    case CMIAddCard
    case CMIDepositeAmount
    case CMIInsuranceAmount
    case CMIPackageAmount
    case CMITotalAmount
    case CMIStartTime
    case CMIDayHour

    case CMIEndTime
    case CMIPackageRate
    case CMINotIncluded
    case CMIReducedDeductible
    case CMIMoreInfo
    
    // Cancellation information
    case CISingleText
    case CIDoubleText
    case CICreditText

    // Uplaod Attchment
    case UAFront
    case UARear
    case UALeft
    case UARight
    
    //Notification
    case NotificationAccountAndRide
    case NotificationDiscountAndNews
    case NotificationAccountPush
    case NotificationAccountText
    case NotificationDiscountPush
    case NotificationDiscountText
    case NotificationDiscountEmail


    //Settings
    case SettingNotification
    case SettingPrivacy
    case SettingGdprPolicy
    case SettingChangePassword
    case SettingAboutUs
    case SettingRateUs
    case SettingContactUs
    case SettingLegal
    case SettingPromo
    case SettingLogout
    case SettingTermsConditions
    case SettingPrivacyPolicy
    case SettingSafety
    case SettingSecurity
    case SettingCopyright
    case SettingDataProviders
    case SettingSoftwareLicences
    case SettingLocationInformation
    case SettingName
    case SettingEmail
    case SettingPhone
    case SettingBusinessProfile
    case SettingSavedPlace
    case SettingSeprator
    case SettingStatics


    //Company Profile
    case CompanyDetails
    case CompanyProfile
    case DefaultPaymentMethod
    case EmailForReceipt
    case ExpenseProvider
    case InviteEmployee
    
    //Company Detail
    case CompanyName
    case CompanyRegNo
    case CompanyAddress

    // Profile
    case ProfileDocument
    case ProfileEditProfile
    case ProfileEditName
    case ProfileEditEmail
    case ProfileEditPhone
    case ProfileChangePassword

    // Detail Popup
    case DPCarInfo
    case DPKeyValueText
    case DPPriceKeyValue
    case DPCard
    case DPCardDetail
    
    
    case PriceBreakUpCell

    //Report Damage
    case ReportDamageCarInfoCell
    case ReportDamageAddPictureCell
    case ReportDamageIssueCell1
    case ReportDamageIssueCell2
    case ReportDamageListVC
    case ReportDamageTextViewCell
    
    //Unlock Car
    case UnlockCarPictureCell
    case UnlockCarOdometerCell
    case UnlockCarKmCell
    case UnlockCarBatteryCell
    
    //Support
    case SupportCallCell
    case SupportSearchCell
    case SupportFAQCell
    
    case SupportIssueWithLastTrip
    case SupportIssueWithPastTrip
    case SupportAccount
    case SupportPayment
    case SupportUsingWeOne
    case SupportConversation


    //Filter
    case FilterTripAllCell
    case FilterTripPersonalCell
    case FilterTripJobCell
    
    //Drive Now
    case DNSlider
    case DNFuelDetailCell
    case DNCarFeature
    case DNMoreInformation
    case DNPreviousRentalReport
    case DNDamageReport
}

enum ClassType {
    case ReportDamageVC
    case UnlockCarVC
    case TripHistoryContainerVC
    case SettingsVC
    case NotificationSettingsVC
    case FareSummaryVC

}

enum CameraDirection {
    case front
    case back
}



//MARK: - Cart Brand Type
enum CardType : String {
    
    case Visa               = "Visa"
    case AmericanExpress    = "American Express"
    case JCB                = "JCB"
    case DinersClub         = "DinersClub"
    case Discover           = "Discover"
    case Maestro            = "Maestro"
    case MasterCard         = "MasterCard"
    case UnionPay           = "UnionPay"
    
}


enum PaymentVCType  {
    
    case Payment
    case ChangeCard
    
}

enum RideType : Int {
    
    case Upcoming   = 1
    case Past       = 2
    
}

enum AttachmentReson {    
    case StartRide
    case EndRide
}

enum DetailType {
    case ReservationDetail
    case CancellationDetail
    case BookingDetail
    case BookingCancellationDetail
}

enum MapScreensViewActions {
    
    case showDrawer // Drawer
    case showSupportButtons //   Chat, Report Damage
    case showCurrentLocationButton // User’s Current Location is not visible on screen
    case showHomeLayout  // Drive Now & Schedule Button, Dot Markers, ShowDrawer, ShowCurrentLocationButton
    case showFindRideLayout // Tool Bar, Car Pager, Vehicle Marker
    case showReservationLayout // Reservation Pager, Show Path to Vehicle , ShowSupportButtons, ShowDrawer
    case showOngoingRideLayout // ShowSupportButtons, OngoingRideBottomSheet, ShowNetworkConnectionLayout, ShowDestinationPath
    case showNavigationLayout // ShowOnMap button, Navigate button, ShowLocationSelectionLayout, Back Button

}
 

enum ProfileType {
    case Personal
    case Business
}
