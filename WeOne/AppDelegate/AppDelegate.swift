//
//  AppDelegate.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved.
//

import UIKit
import Instabug
import SDWebImageSVGKitPlugin
import Firebase
import FirebaseDynamicLinks
import FBSDKCoreKit
import FBSDKLoginKit
import UserNotifications
import NotificationCenter

var userDict = [String: Any]()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, OSPermissionObserver, OSSubscriptionObserver, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    var isShowKeyHandaling : Bool = false
    var userDetails:UserInfoModel?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        intialConfig()
        //configurePushNotification(launchOptions: launchOptions)
        Instabug.start(withToken: "f8da08f1a4237496a691d7861e42dc37", invocationEvents: [.none])
        Instabug.welcomeMessageMode = .disabled
        
        application.registerForRemoteNotifications()
        registerForPushNotifications()
        
        let svgCoder = SDImageSVGKCoder.shared
        SDImageCodersManager.shared.addCoder(svgCoder)
        
        let app = UIApplication.shared
        let statusBarHeight: CGFloat = app.statusBarFrame.size.height

        let statusbarView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: statusBarHeight + 8))
        statusbarView.backgroundColor = ColorConstants.commonWhite
        window?.addSubview(statusbarView)
        
        return true
    }
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            UNUserNotificationCenter.current().delegate = self
            UserDefaults.standard.set(granted, forKey: "registerNotification")
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        print("device token", token)
        Defaults[.deviceToken] = token
        UserDefaults.standard.set(token, forKey: "deviceToken")
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
         return ApplicationDelegate.shared.application(app, open: url, options: options)//true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
            if dynamicLink.matchType == .unique {
                handleDynamicLink(dynamicLink)
            }
            return true
        }
        
        let facebookDidHandle = ApplicationDelegate.shared.application(
            application,
            open: (url as URL?)!,
            sourceApplication: sourceApplication,
            annotation: annotation)
        
        return facebookDidHandle
    }
       
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: Private Methods
    func intialConfig(){
        
        STPPaymentConfiguration.shared().publishableKey = AppConstants.StripeTestKey
        
        //Back Button
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        BarButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
        BarButtonItemAppearance.setBackButtonTitlePositionAdjustment(UIOffset(horizontal: -200, vertical: 0), for:UIBarMetrics.default)
        
        //Search Bar Cancel Button
        let attributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
        
        IQKeyboardManager.shared().toolbarTintColor = UIColor.black
        IQKeyboardManager.shared().isEnabled = true
        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        
        GMSServices.provideAPIKey(AppConstants.googleApiKey)
        GMSPlacesClient.provideAPIKey(AppConstants.googleApiKey)
        
        let vc = SplashVC.init(nibName: "SplashVC", bundle: nil)
        let root = UINavigationController(rootViewController: vc)
        root.navigationBar.isHidden = true
        
        let leftMenu = LeftMenuVC(nibName: "LeftMenuVC", bundle: nil)
        
        let rootLeft = SideMenuNavigationController(rootViewController: leftMenu)
        rootLeft.navigationBar.isHidden = true
        SideMenuManager.default.leftMenuNavigationController = rootLeft
        SideMenuManager.default.menuWidth = UIScreen.main.bounds.width - 80
        SideMenuManager.default.menuPresentMode = .viewSlideOutMenuIn
        SideMenuPresentationStyle.menuSlideIn.backgroundColor = UIColor.clear
        rootLeft.presentationStyle = .menuSlideIn
        rootLeft.presentationStyle.backgroundColor = UIColor.clear
        
        rootLeft.statusBarEndAlpha = 0
        self.window?.rootViewController = root
        self.window?.makeKeyAndVisible()
        
    }
    
    //MARK: - One Signal Push Notification
    
    func configurePushNotification( launchOptions : [UIApplication.LaunchOptionsKey: Any]?){
        
        let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in
            
        }
        
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            let payload: OSNotificationPayload = result!.notification.payload
            
            if payload.additionalData != nil {
                let additionalData = payload.additionalData
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    
                })
            }
        }
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: true,
                                     kOSSettingsKeyInAppLaunchURL: false]
        
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: AppConstants.LiveAppIdOneSignal,
                                        handleNotificationReceived: notificationReceivedBlock,
                                        handleNotificationAction: notificationOpenedBlock,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification
        
        OneSignal.add(self as OSPermissionObserver)
        
        OneSignal.add(self as OSSubscriptionObserver)
        
    }
    
    func onOSPermissionChanged(_ stateChanges: OSPermissionStateChanges!) {
        
        if stateChanges.from.status == OSNotificationPermission.notDetermined {
            if stateChanges.to.status == OSNotificationPermission.authorized {
                print("Thanks for accepting notifications!")
            } else if stateChanges.to.status == OSNotificationPermission.denied {
                print("Notifications not accepted. You can turn them on later under your iOS settings.")
            }
        }
    }
    
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        if let playerId = stateChanges.to.userId {
            Defaults[.PlayerId] = playerId
            SyncManager.sharedInstance.upsertPlayerId()
        }
    }
    
    //DipLinking
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        
        let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
            
            guard error == nil else {
                print("Found an error! \(error?.localizedDescription ?? "")")
                return
            }
            
            if let dynamicLink = dynamiclink {
                self.handleDynamicLink(dynamicLink)
            }
        }
        
        return handled
    }
    
    func handleDynamicLink(_ dynamicLink: DynamicLink) {
        
        guard let url = dynamicLink.url else {
            return
        }
        
        let urlArray = url.absoluteString.components(separatedBy: "/")
        print(urlArray)
        
        if !ApplicationData.isUserLoggedIn{
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                let vc = CarMapVC()
                UIViewController.current()?.navigationController?.pushViewController(vc, animated: false)
                
                if true{
                    let vc = SignUpVC()
                    vc.strReferralCode = urlArray.last ?? ""
                    UIViewController.current()?.navigationController?.pushViewController(vc, animated: false)
                }
            })
        }
    }
}

