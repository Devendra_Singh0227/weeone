//
//  LoadAPIFailedView.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved

import UIKit

class LoadAPIFailedView: UIView {
    
    //MARK: IBOutlet
    @IBOutlet var lblNoDataFound: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var viewtemp: UIView!
    
    //MARK: Variables
    var isShowRefreshButton = true
    var message = ""
    var image : String?
    var btnRefreshClick : (() -> Void)?
    var textColor : UIColor = UIColor.white
    
    //MARK: View Life Cycle
    override func awakeFromNib() {
        
    }
    
    override func layoutSubviews() {
        
//        lblNoDataFound.textColor = textColor
        
        guard (NetworkReachabilityManager()?.isReachable)! else {
            self.lblNoDataFound.text = StringConstants.Nodata.kNoInternet
//            self.btnRefresh.setTitle(StringConstants.ButtonTitles.kRetry, for: .normal)
            self.imgView.image = UIImage(named: "no_internet")
            return
        }
        
//        if(isShowRefreshButton){
//            if message == StringConstants.Common.KLblSomethingWentWrong{
//                btnRefresh.setTitle(StringConstants.ButtonTitles.kRefersh, for: .normal)
//            }
//            self.btnRefresh.isHidden = false
//            //self.lblNoDataFound.isHidden = true
//        }else{
//            self.btnRefresh.isHidden = true
//            //self.lblNoDataFound.isHidden = false
//        }
        self.lblNoDataFound.text = message
        
        if let tempImage = image,tempImage.count > 0 {
            imgView.image = UIImage(named: image ?? "")
        }
        else{
            imgView.isHidden = true
//            heightImg.constant = 0
        }
        
    }
    
    @IBAction func btnRefresh_Click(_ sender: UIButton) {
        
        if let click = btnRefreshClick {
            click()
        }
    }
}
