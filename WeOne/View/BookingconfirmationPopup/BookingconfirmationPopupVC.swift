//
//  BookingconfirmationPopupVC.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved

import UIKit

class BookingconfirmationPopupVC: UIViewController {

    //MARK: Variables
    var headerTitle:String?
    var desc1:String?
    var desc2:String?
    var keyType:String?
    var price:String?
    var imgKey:UIImage?
    var payableamout: String?
    var amountNok :String?
    var carOpenlock: String?
    var reserveNok: NSAttributedString?
    var instantSetting: String?
    var confirmClicked:(() -> ())?
    var cancelBtnClicked: (() -> ())?
    var instantBtnClicked: (() -> ())?
    var isShowSingleButton = false
    var isAttributted : Bool = false
    
    //MARK: Outlets
    @IBOutlet var imgKeyType: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDesc1: UILabel!
    @IBOutlet var lblDesc2: UILabel!

    @IBOutlet var lblKeyType: UILabel!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnConfirm: UIButton!
    @IBOutlet weak var topTitle: NSLayoutConstraint!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewCancel: UIView!
    @IBOutlet weak var viewConfirm: UIView!
    @IBOutlet weak var carOpenLock_Btn: UIButton!
    @IBOutlet weak var payable_Amount_Lbl: UILabel!
    @IBOutlet weak var Amount_Nok: UILabel!
    @IBOutlet weak var reserveNok_Lbl: UILabel!
    @IBOutlet weak var instantSetting_Btn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    //MARK: Private Methods
    func initialConfig(){
        lblTitle.text = headerTitle
        lblDesc1.text = desc1
        lblDesc2.text = desc2
        payable_Amount_Lbl.text = payableamout
        Amount_Nok.text = amountNok
        reserveNok_Lbl.attributedText = reserveNok
        
        btnCancel.layer.cornerRadius = viewCancel.frame.height / 2
        btnConfirm.layer.cornerRadius = viewConfirm.frame.height / 2
        carOpenLock_Btn.setTitle(carOpenlock, for: .normal)
        instantSetting_Btn.setTitle(instantSetting, for: .normal)
        //let amount = price ?? ""
       // lblDesc2.changeStringColor(string: desc2 ?? "", array: [amount], colorArray: [ColorConstants.ThemeColor],changeFontOfString :[amount], font : [FontScheme.kMediumFont(size: 16)])
        //lblKeyType.text = keyType
        //imgKeyType.image = imgKey

        btnCancel.addTarget(self, action: #selector(holdDownCancel(_:)), for: .touchDown)
        btnConfirm.addTarget(self, action: #selector(holdDownConfirm(_:)), for: .touchDown)

    }
    
    //MARK: IBActions
    @objc func holdDownCancel(_ sender: UIButton) {
        btnCancel.backgroundColor = ColorConstants.UnderlineColor
    }

    @objc func holdDownConfirm(_ sender: UIButton) {
        btnConfirm.backgroundColor = ColorConstants.UnderlineColor
    }

    @IBAction func btnCancel_Clicked(_ sender: UIButton) {
        btnCancel.setTitleColor(ColorConstants.TextColorWhitePrimary, for: .normal)
        self.dismiss(animated: true) {
            self.cancelBtnClicked?()
        }
    }
    
    @IBAction func btnConfirm_Clicked(_ sender: UIButton) {
        btnConfirm.setTitleColor(ColorConstants.TextColorWhitePrimary, for: .normal)
        self.dismiss(animated: true) {
            self.confirmClicked?()
        }        
    }
    
    @IBAction func editInstant_Bn(_ sender: UIButton) {
       instantBtnClicked?()
    }
}
