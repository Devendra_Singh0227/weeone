//
//  SelectionPopupModel.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved

import UIKit

class SelectionPopupModel: NSObject {

    var id : String?
    var name : String?
    var isSelected : Bool = false
    var isAllSelected : Bool = false
    var obj:Any?
    
    override init() {
        
    }
    
    class func getSelectionPopupModel(id: String = "", name: String, isSelected:Bool = false, obj:Any? = nil) -> SelectionPopupModel {
        let model = SelectionPopupModel()
        model.id = id
        model.name = name
        model.isSelected = isSelected
        model.obj = obj
        return model
    }
    
}
