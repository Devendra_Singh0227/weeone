//
//  SelectionPopupCell.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved

import UIKit

class SelectionPopupCell: UITableViewCell {

    // MARK: - Variables
    
    // MARK: - Outlets
    @IBOutlet weak var imgSelection: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    // MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Set Data
    func setData(model: SelectionPopupModel) {
        
        lblTitle.text = model.name
        imgSelection.isHidden = !model.isSelected
//        lblTitle.font = model.isSelected ? FontScheme.kMediumFont(size: 17) : FontScheme.kLightFont(size: 17)
    }
    
    func setCellData(model : CellModel) {
        
        lblTitle.text = model.placeholder
        imgSelection.isHidden = !model.isSelected
//        lblTitle.font = model.isSelected ? FontScheme.kMediumFont(size: 17) : FontScheme.kLightFont(size: 17)
    }
    
}
