//
//  SelectionPopupVC.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved
import UIKit

class SelectionPopupVC: UIViewController {

    // MARK: - Variables
    var strHeading : String?
    var arrData = [SelectionPopupModel]()
    var didSelect : ((SelectionPopupModel) -> Void)?
    var isActionPopUp : Bool = false
    
    // MARK: - Outlet
    @IBOutlet weak var viewNavigation: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet var lblHeading: UILabel!
    
    // MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        intialConfig()
    }
    
    // MARK:- Private Methods
    func intialConfig() {
        
        setPopUpSize()
        viewMain.setRadius(radius: 10.0)
        tableView.register(UINib(nibName: "SelectionPopupCell", bundle: nil), forCellReuseIdentifier: "SelectionPopupCell")
        
        lblHeading.text = strHeading
        
        tableView.reloadData()
        
        let arr = arrData.filter { $0.isSelected == true }
        if arr.count > 0 {
            let index = arrData.firstIndex(of: arr.first ?? SelectionPopupModel())
            tableView.scrollToRow(at: IndexPath(row: index ?? 0, section: 0), at: .top, animated: true)
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        setPopUpSize()
    }
    
    func setPopUpSize() {
        
        if isActionPopUp {
            
            let height = 75 + (arrData.count * 80)
            self.preferredContentSize = CGSize(width: 330, height: height)
        }
        else{
           
            let currentOrientation = UIDevice.current.orientation
            
            if currentOrientation == .landscapeLeft || currentOrientation == .landscapeRight{
                
                self.preferredContentSize = CGSize(width: AppConstants.ScreenSize.SCREEN_WIDTH - 470, height: AppConstants.ScreenSize.SCREEN_HEIGHT - 110)
            }
            else if currentOrientation == .portrait || currentOrientation == .portraitUpsideDown || currentOrientation == .faceUp || currentOrientation == .faceDown{
                
                self.preferredContentSize = CGSize(width: AppConstants.ScreenSize.SCREEN_WIDTH - 110, height: AppConstants.ScreenSize.SCREEN_HEIGHT - 500)
            }
        }
    }
    
    //MARK:-  IBAction
    
    @IBAction func btnback_Click(_ sender: UIButton) {
        dismissPopup()
    }
    
}

extension SelectionPopupVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = arrData[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectionPopupCell", for: indexPath) as? SelectionPopupCell
        
        cell?.setData(model: model)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for model in arrData{
            model.isSelected = false
        }
        arrData[indexPath.row].isSelected = true
        tableView.reloadData()
        
        self.dismiss(animated: true) {
            self.didSelect?(self.arrData[indexPath.row])
        }
    }
    
}

extension SelectionPopupVC {
    
    func dismissPopup() {
        self.dismiss(animated: true, completion: nil)
    }
}
