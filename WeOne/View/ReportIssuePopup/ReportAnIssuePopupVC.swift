//
//  ReportAnIssuePopupVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 18/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class ReportAnIssuePopupVC: UIViewController {
    
    //MARK: - Variables
    var isUpoadPhoto:Bool = false
    var onClickSkip:(()->())?
    var onSubmitClicked:(( _ image : UIImage? , _ text : String)->())?
    
    
    //MARK: - Outlets
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var txtView: UIPlaceHolderTextView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var vwMainBottom: NSLayoutConstraint!
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

       initialConfig()
    }
    
    //MARK: Private Methods
    func initialConfig() {
        
        txtView.placeholder = "KLblDescribeDamage".localized
        
        if AppConstants.hasSafeArea {
            vwMainBottom.constant = 34
        }
        else {
            vwMainBottom.constant = 0
        }
        
        viewMain.layer.cornerRadius = 20
        
        viewMain.layer.shadowColor = UIColor.black.withAlphaComponent(1.0).cgColor
        viewMain.layer.shadowOpacity = 0.3
        viewMain.layer.shadowOffset = CGSize(width: 0, height: -5)
        viewMain.layer.shadowRadius = 10.0
        viewMain.layer.masksToBounds = false
        viewMain.layer.shadowPath = CGPath(ellipseIn: CGRect(x: -20,y: -20 * 0.5,width: viewMain.layer.bounds.width + 20 * 2,height: 5),transform: nil)
    }

    //MARK:- IBActions
    @IBAction func btnUploadImage_Click(_ sender: UIButton) {
        #if DEBUG
            Utilities.open_galley_or_camera(delegate: self)
        #else
            Utilities.openCamera(self)
        #endif
    }
    
    @IBAction func btnSubmit_Click(_ sender: UIButton) {
        
//        if !isUpoadPhoto {
//            Utilities.showAlertView(message: "KLblPleaseCapturePhoto".localized)
//            return
//        }
//        else if Utilities.checkStringEmptyOrNil(str: txtView.text) {
//            Utilities.showAlertView(message: "KLblPleaseDescribeDamage".localized)
//            return
//        }
//        else {
            self.onSubmitClicked?(imgView.image, txtView.text)
//        }
    }
    
    @IBAction func btnSkip_Click(_ sender: UIButton) {
        self.onClickSkip?()
    }
    
    @IBAction func btnTap_Click(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: - UIImage Picker Controller Delegate's Methods
extension ReportAnIssuePopupVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        if let pickedImage = info[.originalImage] as? UIImage {
            self.imgView.image = pickedImage
            self.isUpoadPhoto = true
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
