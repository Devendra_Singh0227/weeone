//
//  DamageReportDetailPopupCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 25/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class DamageReportDetailPopupCell: UICollectionViewCell {
    
    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblPArt: UILabel!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: Set Data
    func setData(damageReportModel:DamageReportModel) {
        
        if let img = damageReportModel.getImage() {
            imgView.setImageForURL(url: URL(string: "\(AppConstants.imageURL)\(img)"), placeHolder: UIImage(named: "noImage"))
        }
        
        lblPArt.text = Utilities.returnPartName(damageReportModel.damageParts ?? "")
        //lblTitle.text = Utilities.getTitleForDamageReport(type: damageReportModel.damageCategory)
        lblDesc.text = damageReportModel.desc
    }

}
