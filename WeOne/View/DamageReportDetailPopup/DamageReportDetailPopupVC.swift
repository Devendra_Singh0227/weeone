//
//  DamageReportDetailPopupVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 19/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class DamageReportDetailPopupVC: UIViewController {
    
    //MARK: - Variables
    var arrDamageReport = [DamageReportModel]()
    var damageReportModel:DamageReportModel = DamageReportModel()
    
    //MARK: - Outlets
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var vwMainBottom: NSLayoutConstraint!
    @IBOutlet weak var collectView: UICollectionView!
    @IBOutlet weak var constraintHeightCollView: NSLayoutConstraint!
    
    //MARK: - View's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }

    //MARK: Private Methods
    func initialConfig() {
        
        collectView.register(UINib(nibName: "DamageReportDetailPopupCell", bundle: nil), forCellWithReuseIdentifier: "DamageReportDetailPopupCell")
        
        if AppConstants.hasSafeArea {
            vwMainBottom.constant = 34
        }
        else {
            vwMainBottom.constant = 0
        }
        
        viewMain.layer.cornerRadius = 20
        
        viewMain.layer.shadowColor = UIColor.black.withAlphaComponent(1.0).cgColor
        viewMain.layer.shadowOpacity = 0.3
        viewMain.layer.shadowOffset = CGSize(width: 0, height: -5)
        viewMain.layer.shadowRadius = 10.0
        viewMain.layer.masksToBounds = false
        viewMain.layer.shadowPath = CGPath(ellipseIn: CGRect(x: -20,y: -20 * 0.5,width: viewMain.layer.bounds.width + 20 * 2,height: 5),transform: nil)
        
        collectView.reloadData()
    }
    
    //MARK:- IBActions
    
    @IBAction func dismissPopup(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}


//MARK: - UICollectionView's DataSource & Delegate Methods
extension DamageReportDetailPopupVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrDamageReport.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let model = arrDamageReport[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DamageReportDetailPopupCell", for: indexPath) as? DamageReportDetailPopupCell
        cell?.setData(damageReportModel: model)
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
}
