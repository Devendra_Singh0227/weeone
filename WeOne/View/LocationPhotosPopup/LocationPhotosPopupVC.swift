//
//  BookingconfirmationPopupVC.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved

import UIKit

class LocationPhotosPopupVC: UIViewController {

    //MARK: Variables
    var desc1:String?


    var confirmClicked:(() -> ())?
    var cancelBtnClicked: (() -> ())?
    var isShowSingleButton = false
    var isAttributted : Bool = false
    var currentIndex = 0
    
    var arrLocationImages: [LocationImageModel]?

    //MARK: Outlets
    @IBOutlet weak var collectionView: UICollectionView!

    @IBOutlet var lblDesc1: UILabel!

    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnConfirm: UIButton!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewShowOnMap: UIView!
    @IBOutlet weak var viewConfirm: UIView!

    @IBOutlet weak var btnLeftArrow: UIButton!
    @IBOutlet weak var btnRightArrow: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true)
        collectionView.register(UINib(nibName: "SliderCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SliderCollectionCell")

        btnCancel.addTarget(self, action: #selector(holdDownCancel(_:)), for: .touchDown)
        btnConfirm.addTarget(self, action: #selector(holdDownConfirm(_:)), for: .touchDown)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    //MARK: Private Methods
    func initialConfig(){

        btnConfirm.layer.cornerRadius = viewShowOnMap.frame.height / 2
        btnCancel.layer.cornerRadius = viewConfirm.frame.height / 2
        lblDesc1.text = arrLocationImages?.first?.locationDesc
    }
    
    //MARK: IBActions
    @objc func holdDownCancel(_ sender: UIButton) {
        btnCancel.backgroundColor = ColorConstants.UnderlineColor
    }

    @objc func holdDownConfirm(_ sender: UIButton) {
        btnConfirm.backgroundColor = ColorConstants.UnderlineColor
    }


    @IBAction func btnCancel_Clicked(_ sender: UIButton) {
        btnCancel.setTitleColor(ColorConstants.TextColorWhitePrimary, for: .normal)

        self.dismiss(animated: true) {
            self.cancelBtnClicked?()
        }
    }
    
    @IBAction func btnConfirm_Clicked(_ sender: UIButton) {
        btnConfirm.setTitleColor(ColorConstants.TextColorWhitePrimary, for: .normal)

        self.dismiss(animated: true) {
            self.confirmClicked?()
        }        
    }
    
    @IBAction func btnDismissPopup_Click(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnLeftArrow_Click(_ sender: UIButton) {
        
        let collectionBounds = self.collectionView.bounds
        let contentOffset = CGFloat(floor(self.collectionView.contentOffset.x - collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)

    }

    @IBAction func btnRight_Click(_ sender: UIButton) {
        let collectionBounds = self.collectionView.bounds
        let contentOffset = CGFloat(floor(self.collectionView.contentOffset.x + collectionBounds.size.width))
        self.moveCollectionToFrame(contentOffset: contentOffset)
        
    }
    
    func moveCollectionToFrame(contentOffset : CGFloat) {
        let frame: CGRect = CGRect(x : contentOffset ,y : self.collectionView.contentOffset.y ,width : self.collectionView.frame.width,height : self.collectionView.frame.height)
        self.collectionView.scrollRectToVisible(frame, animated: true)
        for cell in collectionView.visibleCells {
            if let indexPath = collectionView.indexPath(for: cell) {
                lblDesc1.text = arrLocationImages?[indexPath.row].locationDesc
            }
        }
    }
}

//MARK: - UICollectionView's DataSource & Delegate Methods
extension LocationPhotosPopupVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrLocationImages?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let model = arrLocationImages?[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCollectionCell", for: indexPath) as? SliderCollectionCell
        cell?.setImage(strURL: model?.path ?? "")
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//
//        let model = arrImages[indexPath.row]
//        Utilities.openYoutubeVideo(videoIdentifier: model.youtube_video_id ?? "")
//
//    }
}
