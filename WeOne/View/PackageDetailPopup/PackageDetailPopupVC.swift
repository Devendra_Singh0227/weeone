//
//  InsurancePopupVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 05/02/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class PackageDetailPopupVC: UIViewController {

    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var constrainTableViewHeight: NSLayoutConstraint!

    var vehicle : VehicleModel?

    var arrPackages = [PackagesModel]()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewMain.layer.cornerRadius = 20
        
        viewMain.layer.shadowColor = UIColor.black.withAlphaComponent(1.0).cgColor
        viewMain.layer.shadowOpacity = 0.3
        viewMain.layer.shadowOffset = CGSize(width: 0, height: -5)
        viewMain.layer.shadowRadius = 10.0
        viewMain.layer.masksToBounds = false
        viewMain.layer.shadowPath = CGPath(ellipseIn: CGRect(x: -20,y: -20 * 0.5,width: viewMain.layer.bounds.width + 20 * 2,height: 5),transform: nil)
        
        tableView.register(UINib(nibName: "PackageDetailCell", bundle: nil), forCellReuseIdentifier: "PackageDetailCell")
        tableView.tableFooterView = UIView()
        
        tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)

    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            constrainTableViewHeight.constant = min(UIScreen.main.bounds.height * 0.7 , tableView.contentSize.height)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        tableView.removeObserver(self, forKeyPath: "contentSize")
        super.viewWillDisappear(animated)
    }
    
    @IBAction func btnClose_Click(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension PackageDetailPopupVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPackages.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = arrPackages[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "PackageDetailCell", for: indexPath) as? PackageDetailCell
        
        cell?.setData(model: model)
        
        return cell!
    }
    
}


