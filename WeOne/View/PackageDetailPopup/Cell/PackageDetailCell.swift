//
//  PackageDetailCell.swift
//  WeOne
//
//  Created by iMac on 12/02/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class PackageDetailCell: UITableViewCell {

    @IBOutlet weak var viewMain: UIView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDescription: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(model: PackagesModel) {
        
        self.lblName.text = model.name
        self.lblPrice.text =  "\(Utilities.convertToCurrency(number: model.rate, currencyCode: model.currency))/\(model.getFormattedUnit)"
        
        self.lblDescription.text = "\(model.desc ?? "")"
    }

}
