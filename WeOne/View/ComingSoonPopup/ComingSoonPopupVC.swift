//
//  ConfirmPopupVC.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved

import UIKit

class ComingSoonPopupVC: UIViewController {

    //MARK: Variables
    var headerTitle:String?
    var message:String?
    var notifyButtonTitle:String?
    var confirmButtonTitle:String?
    var strAttribute:String?

    var confirmClicked:(() -> ())?
    var cancelBtnClicked: (() -> ())?
    var isShowSingleButton = false
    var isAttributted : Bool = false
    var isAttributtedChangeColor : Bool = false

    var cityId: String = ""

    //MARK: Outlets
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var btnNotifyMe: UIButton!
    @IBOutlet var btnConfirm: UIButton!
    @IBOutlet weak var topTitle: NSLayoutConstraint!
    @IBOutlet weak var viewMain: UIView!
//    @IBOutlet weak var viewCancel: UIView!
    @IBOutlet weak var viewConfirm: UIView!
    @IBOutlet weak var constraintBottomViewMain: NSLayoutConstraint!
    @IBOutlet weak var viewTost: UIViewCommonTost!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    //MARK: Private Methods
    func initialConfig(){
        
        viewTost.text = "KLblNotificationSaved".localized as NSString
        viewTost.imageIcon = "tick1" as NSString
        viewTost.setBackGroundColor(color: ColorConstants.TextColorGreen)

//        btnCancel.layer.cornerRadius = viewCancel.frame.height / 2
        btnConfirm.layer.cornerRadius = viewConfirm.frame.height / 2

        lblTitle.text = headerTitle ?? ""
//        btnCancel.titleLabel?.numberOfLines = 0
//        btnCancel.titleLabel?.adjustsFontSizeToFitWidth = true
//        btnCancel.titleLabel?.minimumScaleFactor = 0.5
        
        
        if isAttributted {
            lblDescription.attributedText = Utilities.convertToHtml(str: message ?? "")
        }
        else{
           lblDescription.text = message ?? ""
        }
    
        if isAttributtedChangeColor {
            
//            lblDescription.changeStringColor(string: lblDescription.text ?? "", array: [(strAttribute ?? "")], colorArray: [ColorConstants.ThemeColor],changeFontOfString :[strAttribute ?? ""], font : [FontScheme.kMediumFont(size: 16)])

        }

//        if cancelButtonTitle?.count ?? 0 > 0{
//            btnCancel.setTitle(cancelButtonTitle, for: .normal)
//        }else{
//            btnCancel.setTitle(StringConstants.ButtonTitles.KCancel, for: .normal)
//        }
        
        if notifyButtonTitle?.count ?? 0 > 0{
            
            if notifyButtonTitle == StringConstants.ButtonTitles.kYes{
                //btnConfirm.setTitleColor(ColorConstants.RedText, for: .normal)
            }
            btnNotifyMe.setTitle(notifyButtonTitle, for: .normal)
        }else{
            btnNotifyMe.setTitle(StringConstants.ButtonTitles.KConfirm, for: .normal)
        }

        if confirmButtonTitle?.count ?? 0 > 0{
            
            if confirmButtonTitle == StringConstants.ButtonTitles.kYes{
                //btnConfirm.setTitleColor(ColorConstants.RedText, for: .normal)
            }
            btnConfirm.setTitle(confirmButtonTitle, for: .normal)
        }else{
            btnConfirm.setTitle(StringConstants.ButtonTitles.KConfirm, for: .normal)
        }
        
        if let title = headerTitle,title.count > 0 {
            topTitle.constant = 20
        }else{
            topTitle.constant = 0
        }
        
//        btnCancel.addTarget(self, action: #selector(holdDownCancel(_:)), for: .touchDown)
        btnConfirm.addTarget(self, action: #selector(holdDownConfirm(_:)), for: .touchDown)

        
//        viewMain.roundCorners([.topLeft, .topRight], radius: 25, width: AppConstants.ScreenSize.SCREEN_WIDTH)
        
//        if isShowSingleButton == true {
//            viewCancel.isHidden = true
//        }
    }
    
    //MARK: IBActions
    
//    @objc func holdDownCancel(_ sender: UIButton) {
//        btnCancel.backgroundColor = ColorConstants.UnderlineColor
//    }

    @objc func holdDownConfirm(_ sender: UIButton) {
        btnConfirm.backgroundColor = ColorConstants.UnderlineColor
    }

//    @IBAction func btnCancel_Clicked(_ sender: UIButton) {
//        btnCancel.setTitleColor(ColorConstants.TextColorWhitePrimary, for: .normal)
//        self.dismiss(animated: true) {
//            self.cancelBtnClicked?()
//        }
//    }
    
    @IBAction func btnConfirm_Clicked(_ sender: UIButton) {
        btnConfirm.setTitleColor(ColorConstants.TextColorWhitePrimary, for: .normal)
        self.dismiss(animated: true) {
            self.confirmClicked?()
        }        
    }
    
    @IBAction func btnNotifyMe_Clicked(_ sender: UIButton) {
        btnNotifyMe.isSelected = !btnNotifyMe.isSelected
        
        callApiForNotifyWhenAvailable()

    }

    @IBAction func btnDismissPopup_Click(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: - Api Calling

extension ComingSoonPopupVC {
    func callApiForNotifyWhenAvailable(_ completion : (() -> ())? = nil){
        
        var param : [String:Any] = [:]
        
        let user = ApplicationData.user

        param["isNotify"] = btnNotifyMe.isSelected
        param["userId"] = user?.id
        if cityId != "" {
            param["cityId"] = self.cityId
        }

        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.NotifyWhenAvailable, method: .post, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            
                self.viewTost.isHidden = false
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                    self.viewTost.isHidden = true
                })
            
            completion?()
            
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }
}
