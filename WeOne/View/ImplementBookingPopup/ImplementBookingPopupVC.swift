//
//  ImplementBookingPopupVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 11/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class ImplementBookingPopupVC: UIViewController {
    
    //MARK: Variables
    var cancelButtonTitle:String?
    var confirmButtonTitle:String?
    var confirmClicked:(() -> ())?
    var cancelBtnClicked: (() -> ())?
    
    //MARK: Outlets
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnConfirm: UIButton!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var btnCheckBox: UIButton!
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Private Methods
    func initialConfig(){
        
    }
    
    //MARK: IBActions
    
    @IBAction func btnCancel_Clicked(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.cancelBtnClicked?()
        }
    }
    
    @IBAction func btnConfirm_Clicked(_ sender: UIButton) {
        
        if btnCheckBox.isSelected {
            Defaults[.DontShowImplementBookingPopup] = true
        }
        
        self.dismiss(animated: true) {
            self.confirmClicked?()
        }
    }
    
    @IBAction func btnCheckBox_Clicked(_ sender: UIButton) {
        btnCheckBox.isSelected = !btnCheckBox.isSelected
    }
    
}
