//
//  Popupswift
//  ShopByUber
//
//  Created by Administrator on 5/27/17.
//  Copyright © 2017 Administrator. All rights reserved.
//

import UIKit

class PopupCell: UITableViewCell {
    
    @IBOutlet var imgCheckBox: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    
    var isAllowMultipleSelection = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func setData(model : SelectionPopupModel)  {
        
        lblTitle.text = model.name
        
        if model.isSelected {
            imgCheckBox.image = isAllowMultipleSelection ? UIImage(named: "checkboxFilled") : UIImage(named: "radioChecked")
        } else {
            imgCheckBox.image = isAllowMultipleSelection ? UIImage(named: "checkboxEmpty") : UIImage(named: "radioUnchecked")
        }
    }
    
    func setChecked(value : Bool , model : SelectionPopupModel){
        lblTitle.text = model.name
        if value == true{
            imgCheckBox.image = UIImage(named: "radioChecked")
        }else{
            imgCheckBox.image = UIImage(named: "radioUnchecked")
        }
    }
}
