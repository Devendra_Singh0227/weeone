//
//  SelectionPopup.swift
//  DealBank
//
//  Created by CS-MacSierra on 23/07/18.
//  Copyright © 2018 coruscate. All rights reserved.
//

import UIKit

class SelectionPopup: UIViewController {
    
    //MARK: Variables
    var isAllSelect = false
    var isShowSearchBar = true
    var isFiltered:Bool = false
    var isShowSelectedImage = false
    var isAllowMultipleSelection = true
    var strHeading : String?
    var arrData = [SelectionPopupModel]()
    var filtered = [SelectionPopupModel]()
    var btnApplyTitle = String()
    var btnClearTitle = String()
    var isShowSelectAlloption = true
    
    var id = String()
    
    var didSelect : (([SelectionPopupModel]) -> Void)?
    var didClearSelect : (() -> Void)?
    
    //MARK: IBOutlets
    @IBOutlet var vwSelectAllHeight: NSLayoutConstraint!
    @IBOutlet var vwContent: UIView!
    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var tblData: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var vwSearchBarHeight: NSLayoutConstraint!
    @IBOutlet var imgCheckAll: UIImageView!
    @IBOutlet var vwSearch: UIView!
    @IBOutlet weak var viewSelectAll: UIView!
    @IBOutlet weak var vwButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var vwBottom: UIView!
    @IBOutlet weak var vwContentHeight: NSLayoutConstraint!
    @IBOutlet weak var btnApply: MMButton!
    @IBOutlet weak var btnClear: MMButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialConfig()
        //Utilities.applyShadowEffect(view: self.vwContent)
        btnApply.setTitle(btnApplyTitle, for: .normal)
        btnClear.setTitle(btnClearTitle, for: .normal)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Private Methods
    func initialConfig() {
        
        self.preferredContentSize = CGSize(width: AppConstants.ScreenSize.SCREEN_WIDTH, height: vwContentHeight.constant)
        lblHeading.text = strHeading!
        searchBar.delegate = self
        self.tblData.register(UINib(nibName: "PopupCell", bundle: nil), forCellReuseIdentifier: "PopupCell")
        self.tblData.dataSource = self
        self.tblData.delegate = self
        
        self.setLayout()
        
    }
    
    func setLayout() {
        
        vwContent.setRadius(radius: 6)
        
        var height = 0

        //searchbar
        if isShowSearchBar{
            
            height += 44
            self.vwSearchBarHeight.constant = 44
            self.vwSearch.isHidden = false
        }else{
            
            height += 0
            self.vwSearchBarHeight.constant = 0
            self.vwSearch.isHidden = true
        }
        
        //viewselect
        if isAllowMultipleSelection{
            
            if isShowSelectAlloption == true{
                height += 44
                self.vwSelectAllHeight.constant = 44
                self.viewSelectAll.isHidden = false
            }else{
                height += 0
                self.vwSelectAllHeight.constant = 0
                self.viewSelectAll.isHidden = true
            }
            
        }else{
            
            if isShowSelectAlloption == true{
                height += 44
                self.vwSelectAllHeight.constant = 44
                self.viewSelectAll.isHidden = false
            }else{
                height += 0
                self.vwSelectAllHeight.constant = 0
                self.viewSelectAll.isHidden = true
            }
           
        }
        
        //buttons
        //if isAllowMultipleSelection is true then show button else hide button view also
        if isAllowMultipleSelection{
            
            height += 40
            self.vwButtonHeight.constant = 40
            self.vwBottom.isHidden = false
        }else{
            
            height += 0
            self.vwButtonHeight.constant = 0
            self.vwBottom.isHidden = true
        }
        
        height += self.arrData.count * 45 + 40 //here 40 is header size
        
        if CGFloat(height) > AppConstants.ScreenSize.SCREEN_HEIGHT - 300{
            
            height = Int(AppConstants.ScreenSize.SCREEN_HEIGHT - 300)
            
        }
        
        self.vwContentHeight.constant = CGFloat(height)
        
        if isAllSelect
        {
            self.setSelectAll(allSelect:!isAllSelect)
        }
        
        if isAllowMultipleSelection == true {
            
            var selectedAll = true
            for model in arrData {
                
                if !model.isSelected {
                    selectedAll = false
                }
            }
            if selectedAll {
                isAllSelect = true
                imgCheckAll.image = UIImage(named: "radioBtnCheck")
            }
            
        }
        
        
        self.vwContent.layoutIfNeeded()
        self.tblData.reloadData()
    }
    
    //MARK: IBActions
    @IBAction func btnSelectAll_Clicked(_ sender: UIButton) {
        self.setSelectAll(allSelect:isAllSelect)
    }
    
    // set All sect true or false
    func setSelectAll(allSelect:Bool)
    {
        if allSelect == true {
            
            isAllSelect = false
            imgCheckAll.image = UIImage(named: "radioBtn")
            self.selectDeselectAll()
        }
        else {
            isAllSelect = true
            imgCheckAll.image = UIImage(named: "radioBtnCheck")
            self.selectDeselectAll(isDeselectAll:false)
        }
    }
    
    @IBAction func btnClear_Clicked(_ sender: UIButton) {
        didClearSelect?()
        self.selectDeselectAll(isDeselectAll: true)
        didSelect?(arrData)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    // Mark: Clear All
    // Clear All Selecion
    func selectDeselectAll(isDeselectAll:Bool = true)
    {
        // if isDeselectAll = true then all row deselect
        // if isDeselectAll  = false then all row Selected
        var selectedItems = [IndexPath]()
        
        var arrList = arrData
        
        if isFiltered && isAllowMultipleSelection {
            arrList = filtered
        }
        
        for i in 0..<(arrList.count)
        {
            let indexpath = IndexPath(row: i, section: 0)
            if isDeselectAll {
                arrList[i].isSelected = false
            } else
            {
                arrList[i].isSelected = true
            }
            
            selectedItems.append(indexpath)
        }
        // DeSelect Select All row
        if isDeselectAll {
            isAllSelect = false
            imgCheckAll.image = UIImage(named: "radioBtn")
        } else {
            //if filter is on then if we select all so select all not select
            isAllSelect = true
            imgCheckAll.image = UIImage(named: "radioBtnCheck")
        }
        if isAllowMultipleSelection {
            self.tblData.reloadRows(at: selectedItems, with: .automatic)
        }
        else {
            self.tblData.reloadData()
        }
        
        
    }
    
    func isAllSelected(arr:[SelectionPopupModel]) {
        
        let arrSelected = arr.filter { $0.isSelected }
        if arrSelected.count == arr.count {
            isAllSelect = true
            imgCheckAll.image = UIImage(named: "radioBtnCheck")
        }
        else {
            isAllSelect = false
            imgCheckAll.image = UIImage(named: "radioBtn")
        }
    }
    
    // Close Popup
    @IBAction func btnDone_Clicked(_ sender: UIButton) {
        
        let arrSelected = arrData.filter { $0.isSelected }
        
        //        var count = 0
        //        for model in arrData! {
        //            if model.isSelected {
        //                count = count + 1
        //                break
        //            }
        //        }
        
        if arrSelected.count > 0 {
            didSelect?(arrData)
            self.dismiss(animated: true, completion: nil)
        }
        else {
            self.showAlert()
        }
    }
    
    @IBAction func btnClose_Click(_ sender: UIButton) {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func showAlert(){
        
        Utilities.showAlertView(title: AppConstants.AppName, message: "Please select atleast one.")
        
    }
    
}
//MARK: UITableView's Data Source & Delegate Methods
extension SelectionPopup: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(isFiltered) {

            return filtered.count
        }else{
            return arrData.count;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PopupCell", for: indexPath) as! PopupCell
        cell.isAllowMultipleSelection = self.isAllowMultipleSelection
        if(isFiltered){
            let model = filtered[indexPath.row]
            self.isAllSelected(arr: filtered)
            cell.setData(model: model)
        }
        else{
            
            let model = arrData[indexPath.row]
            
            self.isAllSelected(arr: arrData)
            cell.setData(model: model)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(isFiltered){
            
            let model = filtered[indexPath.row]
            
            if isAllowMultipleSelection == true {
                
                model.isSelected = !model.isSelected
                
                tblData.reloadRows(at: [indexPath], with: .automatic)
            }
            else{
                
                if model.isSelected {
                    model.isSelected = false
                    tblData.reloadRows(at: [indexPath], with: .automatic)
                }
                else {
                    self.selectDeselectAll(isDeselectAll: true)
                    model.isSelected = true
                    self.tblData.reloadData()
                }
                
                //dismiss popus
                let arrSelected = arrData.filter { $0.isSelected }
                if arrSelected.count > 0 {
                    didSelect?(arrData)
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
        else {
            
            let model = arrData[indexPath.row]
            
            if isAllowMultipleSelection == true{
                model.isSelected = !model.isSelected
                tblData.reloadRows(at: [indexPath], with: .automatic)
            }else{
                if model.isSelected {
                    model.isSelected = false
                    tblData.reloadRows(at: [indexPath], with: .automatic)
                }
                else {
                    self.selectDeselectAll(isDeselectAll: true)
                    model.isSelected = true
                    self.tblData.reloadData()
                }
                
                //dismiss popus
                let arrSelected = arrData.filter { $0.isSelected }
                if arrSelected.count > 0 {
                    didSelect?(arrData)
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
        self.view.endEditing(true)
    }
    
}

//MARK: UISeachBar Delegate Methods
extension SelectionPopup: UISearchBarDelegate
{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if self.searchBar.text!.count == 0 {
            isFiltered = false
        }
        else {
            isFiltered = true
            filtered.removeAll()
            
            filtered = arrData.filter({ (model) -> Bool in
                let tmp: NSString = model.name! as NSString
                let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
        }
        
        tblData.reloadData()
        
    }
}

