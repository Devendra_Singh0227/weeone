//
//  Update_DetailPopUp_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 14/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Update_DetailPopUp_ViewController: UIViewController {

    var headerTitle:String?
    
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var header_Title: UILabel!
    
    var confirmClicked:((String) -> ())?
    var cancelBtnClicked: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        header_Title.text = headerTitle
        // Do any additional setup after loading the view.
    }

    @IBAction func btnDismissPopup_Click(_ sender: UIButton) {
           self.dismiss(animated: true, completion: nil)
       }
   
    @IBAction func done_Btn(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.confirmClicked?(self.txtField.text!)
        }
    }
}
