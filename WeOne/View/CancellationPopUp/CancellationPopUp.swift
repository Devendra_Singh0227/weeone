//
//  CancellationPopUp.swift
//  WeOne
//
//  Created by Dev's Mac on 23/12/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class CancellationPopUp: UIViewController {

    var confirmClicked:(() -> ())?
    var viewCancellation_Btn:(() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func cross_Btn(_ sender: UIButton) {
          self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel_Btn(_ sender: UIButton) {
          self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func tick_Btn(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.confirmClicked?()
        }
    }
    
    @IBAction func viewCancel_Btn(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.viewCancellation_Btn?()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
