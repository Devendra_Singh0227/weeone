//
//  EndRidePopup_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 15/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class EndRidePopup_ViewController: UIViewController {

    var confirmClicked:(() -> ())?
    var cancelBtnClicked: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func cross_Btn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
   @IBAction func btnConfirm_Clicked(_ sender: UIButton) {
          self.dismiss(animated: true) {
              self.confirmClicked?()
          }
      }

    @IBAction func close_BtnClicked(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.confirmClicked?()
        }
    }
}
