//
//  AutoFilled_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 27/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class AutoFilled_ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var phone_Table_View: UITableView!
    var cellClicked:((String) -> ())?
    var noneBtnClicked: (() -> ())?
    var itemsArray = [String]()
    var image = ""
    var phoneArray: [String]?
    var imag: String?
    @IBOutlet weak var continue_With_Lbl: UILabel!
    var headerStr: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
        // Do any additional setup after loading the view.
    }
    
    func initialConfig(){
        itemsArray = phoneArray!
        image = imag!
        continue_With_Lbl.text = headerStr
        phone_Table_View.register(UINib(nibName: "Phone_TableViewCell", bundle: nil), forCellReuseIdentifier: "Phone_TableViewCell")
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = phone_Table_View.dequeueReusableCell(withIdentifier: "Phone_TableViewCell", for: indexPath) as? Phone_TableViewCell
        cell?.phone_Image.image = UIImage(named: image)
        cell?.number_Lbl.text = itemsArray[indexPath.row]
        return cell!
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
        let phone = itemsArray[indexPath.row]
        cellClicked?(phone)
        self.dismiss(animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    
    @IBAction func none_of_Above_Btn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
