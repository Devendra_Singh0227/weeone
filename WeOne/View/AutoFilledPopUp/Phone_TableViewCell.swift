//
//  Phone_TableViewCell.swift
//  WeOne
//
//  Created by Dev's Mac on 27/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class Phone_TableViewCell: UITableViewCell {

    @IBOutlet weak var phone_Image: UIImageView!
    @IBOutlet weak var number_Lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
