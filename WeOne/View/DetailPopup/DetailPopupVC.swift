//
//  DetailPopupVC.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 25/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class DetailPopupVC: UIViewController {
    
    //MARK: - Variables
    var arrData = [CellModel]()
    var detailType:DetailType = .BookingDetail
    var rideModel:RideListModel = RideListModel()
    var startDate:Date?
    var endDate:Date?
    var onConfirm_Click:((String)->())?
    
    //MARK: - Outlets
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnReserve: UIButton!
    @IBOutlet weak var btnBooking: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var lblNote: UILabel!
    
    //MARK:- Controller's Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Utilities.setNavigationBar(controller: self, isHidden: true, title: "")
    }
    
    
    //MARK: Private Methods
    func initialConfig() {
        
        if detailType == .ReservationDetail {
            lblTitle.text = "KReservationDetail".localized
        }
        else if detailType == .CancellationDetail {
            lblTitle.text = "KCancellationDetail".localized
        }
        else if detailType == .BookingDetail {
            lblTitle.text = "KBookingDetail".localized
        }
        else if detailType == .BookingCancellationDetail {
            lblTitle.text = "KCancellationDetail".localized
        }
        
        tblView.register(UINib(nibName: "CarDetailCell", bundle: nil), forCellReuseIdentifier: "CarDetailCell")
        
        tblView.register(UINib(nibName: "DetailKeyValueTextCell", bundle: nil), forCellReuseIdentifier: "DetailKeyValueTextCell")
        tblView.register(UINib(nibName: "DetailCardCell", bundle: nil), forCellReuseIdentifier: "DetailCardCell")
        tblView.register(UINib(nibName: "DetailCardChangeCell", bundle: nil), forCellReuseIdentifier: "DetailCardChangeCell")
        
        viewMain.layer.cornerRadius = 20
        
        viewMain.layer.shadowColor = UIColor.black.withAlphaComponent(1.0).cgColor
        viewMain.layer.shadowOpacity = 0.3
        viewMain.layer.shadowOffset = CGSize(width: 0, height: -5)
        viewMain.layer.shadowRadius = 10.0
        viewMain.layer.masksToBounds = false
        viewMain.layer.shadowPath = CGPath(ellipseIn: CGRect(x: -20,y: -20 * 0.5,width: viewMain.layer.bounds.width + 20 * 2,height: 5),transform: nil)
        
        
        configureView()
        
        prepateDataSource()
    }
    
    func prepateDataSource() {
        arrData.removeAll()
        
        arrData.append(CellModel.getModel(placeholder: "", placeholder2: "", text: "", type: .DPCarInfo, cellObj: nil))
        
        arrData.append(CellModel.getModel(placeholder: "KLblDeposit".localized, text: rideModel.getDeposit(), type: .DPKeyValueText, cellObj: nil))
        
        arrData.append(CellModel.getModel(placeholder: "KLblInsurance".localized, text: rideModel.getInsurance(), type: .DPKeyValueText, cellObj: nil))
        
        arrData.append(CellModel.getModel(placeholder: "KLblPackage".localized, text: "\(Utilities.convertToCurrency(number: rideModel.pricingConfigData?.rate ?? 0.0, currencyCode: rideModel.pricingConfigData?.currency))/\(rideModel.pricingConfigData?.getFormattedUnit ?? "")", type: .DPKeyValueText, cellObj: nil))
        
        if detailType == .BookingDetail || detailType == .BookingCancellationDetail {
            
            let startDate = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: rideModel.reservedDateTime ?? ""), format: DateUtilities.DateFormates.kDetailPopup)
            
            let startTime = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: rideModel.reservedDateTime ?? ""), format: DateUtilities.DateFormates.kCarScheduleTime)
            
            
            arrData.append(CellModel.getModel(placeholder: "KStartTime".localized, text: "\(startDate) at \(startTime)", type: .DPKeyValueText, cellObj: nil))
            
            let endDate = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: rideModel.reservedEndDateTime ?? ""), format: DateUtilities.DateFormates.kDetailPopup)
            
            let endTime = DateUtilities.convertStringFromDate(date: DateUtilities.convertDateFromServerString(dateStr: rideModel.reservedEndDateTime ?? ""), format: DateUtilities.DateFormates.kCarScheduleTime)
            
            arrData.append(CellModel.getModel(placeholder: "KEndTime".localized, text: "\(endDate) at \(endTime)", type: .DPKeyValueText, cellObj: nil))
        }
        
        //        if detailType == .CancellationDetail || detailType == .BookingCancellationDetail{
        //            arrData.append(CellModel.getModel(placeholder: "KNote".localized, text: "-", type: .DPKeyValueText, cellObj: nil))
        //        }
        
        arrData.append(CellModel.getModel(placeholder: "KLblTotalAmount".localized, text: rideModel.getTotalFare(), type: .DPPriceKeyValue, cellObj: nil))
        
        //DPCard
        if detailType == .BookingCancellationDetail || detailType == .BookingCancellationDetail{
            
            arrData.append(CellModel.getModel(placeholder: "KLblTotalAmount".localized, text: rideModel.getTotalFare(), type: .DPCardDetail, cellObj: rideModel.selectedCard))
            
        }else{
            if let card = SyncManager.sharedInstance.getPrimaryCard() {
                arrData.append(CellModel.getModel(placeholder: "KLblTotalAmount".localized, text: "", type: .DPCard, cellObj: card))
            }else
            
            if let card = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard) {
                arrData.append(CellModel.getModel(placeholder: "KLblTotalAmount".localized, text: "", type: .DPCard, cellObj: card))
            }
        }
        
        calculateHeight()
        tblView.reloadData()
    }
    
    func calculateHeight() {
        
        var height = 180
        
        let arrCard = arrData.filter {$0.cellType == .DPCard || $0.cellType == .DPCardDetail}
        height += (arrCard.count * 70)
        
        let arr = arrData.filter {$0.cellType == .DPPriceKeyValue || $0.cellType == .DPKeyValueText}
        
        height += (arr.count * 32)
        
        let anotherHeight = CGFloat(162) + lblNote.bounds.height
        
        if height > Int(AppConstants.ScreenSize.SCREEN_HEIGHT - anotherHeight) {
            tblHeight.constant = AppConstants.ScreenSize.SCREEN_HEIGHT - anotherHeight
        }
        else {
            tblHeight.constant = CGFloat(height) - 16
        }
    }
    
    func configureView() {
        if detailType == .ReservationDetail {
            btnCancel.isHidden = false
            btnReserve.isHidden = false
            btnBooking.isHidden = true
            btnConfirm.isHidden = true
            lblNote.text = "KLblImplementBookingMsg".localized
        }
        else if detailType == .CancellationDetail {
            btnCancel.isHidden = false
            btnReserve.isHidden = true
            btnBooking.isHidden = true
            btnConfirm.isHidden = false
            lblNote.text = "KLblCancellationNoteMsg".localized
        }
        else if detailType == .BookingDetail {
            btnCancel.isHidden = false
            btnReserve.isHidden = true
            btnBooking.isHidden = false
            btnConfirm.isHidden = true
            lblNote.text = "KLblImplementBookingMsg".localized
        }
        else if detailType == .BookingCancellationDetail {
            btnCancel.isHidden = false
            btnReserve.isHidden = true
            btnBooking.isHidden = true
            btnConfirm.isHidden = false
            
            lblNote.text = "KLblCancellationNoteMsg".localized
        }
    }
    
    //MARK:- IBActions
    @IBAction func btnClose_Click(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnReserve_Click(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
            let filter = self.arrData.filter { $0.cellType == .DPCard}
            if filter.count > 0{
                
                if let cardModel = filter.first!.cellObj as? CardModel {
                    self.onConfirm_Click?(cardModel.id ?? "")
                }
            }
        }
    }
    
    @IBAction func btnBooking_Click(_ sender: UIButton) {
        self.dismiss(animated: true) {
            
            let filter = self.arrData.filter { $0.cellType == .DPCard}
            if filter.count > 0{
                if let cardModel = filter.first!.cellObj as? CardModel {
                    self.onConfirm_Click?(cardModel.id ?? "")
                }
            }
        }
    }
    
    @IBAction func btnConfirm_Click(_ sender: UIButton) {
        if detailType == .CancellationDetail {
            // for cancelltation detail
            self.dismiss(animated: true) {
               self.onConfirm_Click?("")
            }
        }
        else if detailType == .BookingCancellationDetail {
            // for Booking cancelltation detail
            self.dismiss(animated: true) {
                self.onConfirm_Click?("Booking")
            }
        }
    }
}

//MARK: - UITableView's DataSource & Delegate Methods
extension DetailPopupVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = arrData[indexPath.row]
        
        switch model.cellType! {
        case .DPCarInfo:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarDetailCell") as? CarDetailCell
            cell?.setData(model: model, rideModel: rideModel)
            return cell!
            
        case .DPKeyValueText, .DPPriceKeyValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailKeyValueTextCell") as? DetailKeyValueTextCell
            cell?.setData(model: model)
            return cell!
          
        case .DPCardDetail :
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCardCell") as? DetailCardCell
            cell?.setData(model: model, rideModel: rideModel)
            return cell!
            
        case .DPCard:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCardChangeCell") as? DetailCardChangeCell
            cell?.setData(model: model)
            cell?.btnChange.addTarget(self, action: #selector(btnChange_Click(_:)), for: .touchUpInside)
            cell?.btnChange.tag = indexPath.row
            cell?.switchChanged = { value in
                self.changeDefaultCards(value, index : indexPath.row)
            }
            
            if detailType == .BookingCancellationDetail || detailType == .BookingCancellationDetail{
                cell?.viewSwitch.isHidden = true
                cell?.btnChange.isHidden = true
            }
            
            return cell!
        default:
            return UITableViewCell()
        }
    }
    
    @objc func btnChange_Click(_ sender: UIButton) {
        
        let vc = PaymentVC(nibName: "PaymentVC", bundle: nil)
        vc.isPresented = false
        vc.isDeleteHidden = true
        //vc.shownCardType = !arrData[sender.tag].isSelected ? AppConstants.CardTypes.PersonalCard :  AppConstants.CardTypes.JobCard

        vc.screenType = .ChangeCard
        
        
        vc.isReload = { isFromAddClick in
            
            let filter = self.arrData.filter { $0.cellType == .DPCard}
            
            if filter.count > 0{
                
                if SyncManager.sharedInstance.getPrimaryCard() != nil && SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard) != nil{
                    
                    if filter.first!.isSelected == false{
                        filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard()
                    }else{
                        filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard)
                    }
                    
                }else if SyncManager.sharedInstance.getPrimaryCard() != nil{
                    filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard()
                }else{
                    filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard)
                }
                
            } else {
                if isFromAddClick {
                    //Card
                    if let card = SyncManager.sharedInstance.getPrimaryCard() {
                        self.arrData.append(CellModel.getModel(placeholder: "", text: "", type: .CMICard, cellObj: card))
                    } else if let card = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard) {
                        self.arrData.append(CellModel.getModel(placeholder: "", text: "", type: .CMICard, cellObj: card, isSelected : true))
                    } else {
                        self.arrData.append(CellModel.getModel(placeholder: "", text: "", type: .CMIAddCard, cellObj: nil))
                    }
                }
            }
            self.tblView.reloadRows(at: [IndexPath(row: self.arrData.firstIndex(of: filter.first!) ?? 0, section: 0)], with: .none)
            
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func changeDefaultCards(_ value : Bool, index : Int){
        
        let filter = arrData.filter { $0.cellType == .DPCard}
        
        if filter.count > 0{
            
            if value == false{
                filter.first!.isSelected = false
                filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard()
            }else{
                filter.first!.isSelected = true
                filter.first!.cellObj = SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard)
            }
        }
                
        self.tblView.reloadData()

//        self.tblView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
    }
}
