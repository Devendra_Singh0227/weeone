//
//  DetailCardCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 25/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class DetailCardCell: UITableViewCell {
    
    //MARK: - Variables
    
    //MARK: - Outlets
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var lblCardNumber: UILabel!
    @IBOutlet weak var vwCardInfo: UIView!
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    //MARK: Set Data
    func setData(model:CellModel, rideModel:RideListModel) {
        
        lblPrice.text = model.userText
        
        if let card = rideModel.selectedCard {
            vwCardInfo.isHidden = false
            lblCardNumber.text = card.getFormattedCardNumber()
            imgCard.image = UIImage(named: Utilities.getCreditCardImage(cardType: card.brand ?? ""))
        }
        else {
            vwCardInfo.isHidden = true
        }
    }
}
