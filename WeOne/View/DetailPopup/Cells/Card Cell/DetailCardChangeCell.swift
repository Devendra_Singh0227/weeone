//
//  DetailCardChangeCell.swift
//  WeOne
//
//  Created by Coruscate Mac on 07/02/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class DetailCardChangeCell: UITableViewCell {

    //MARK: - Variables
    var switchChanged : ((_ switch : Bool) -> ())?
    
    //MARK: - Outlets
    @IBOutlet weak var lblCardNo: UILabel!
    @IBOutlet weak var lblCardType: UILabel!

    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var viewSwitch: UIView!
    
    @IBOutlet weak var iconsSegmentedControl: CustomSegmentedControl! {
        didSet {
            
            //Set this booleans to adapt control
            iconsSegmentedControl.itemsWithText = false
            iconsSegmentedControl.fillEqually = false
            iconsSegmentedControl.roundedControl = true
            
            iconsSegmentedControl.setSegmentedWith(items: [#imageLiteral(resourceName: "User1@x"),#imageLiteral(resourceName: "Briefcase – 1")])
            iconsSegmentedControl.padding = 2
            iconsSegmentedControl.thumbViewColor = #colorLiteral(red: 0.2857761979, green: 0.6604915261, blue: 0.8611465693, alpha: 1)
            iconsSegmentedControl.buttonColorForNormal = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            iconsSegmentedControl.buttonColorForSelected = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }
    }

    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        
        iconsSegmentedControl.isSelected = model.isSelected
        
        if model.isSelected {
            iconsSegmentedControl.moveThumbViewFillEquallyFalse(at: 1)
            lblCardType.text = "Job Card"
        } else {
            iconsSegmentedControl.moveThumbViewFillEquallyFalse(at: 0)
            lblCardType.text = "Personal Card"
        }

        if SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.JobCard) != nil && SyncManager.sharedInstance.getPrimaryCard(AppConstants.CardTypes.PersonalCard) != nil{
            viewSwitch.isHidden = false
        }else{
            viewSwitch.isHidden = true
        }
        
        if let obj = model.cellObj as? CardModel {
            lblCardNo.text = obj.getFormattedCardNumber()
            imgCard.image = UIImage(named: Utilities.getCreditCardImage(cardType: obj.brand ?? ""))
        }
    }
    
    @IBAction func btnSegment_Changed(_ sender: CustomSegmentedControl) {
        
        if iconsSegmentedControl.selectedSegmentIndex == 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.switchChanged?(false)
            }
            
            lblCardType.text = "Job Card"
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.switchChanged?(true)
            }
            lblCardType.text = "Personal Card"
        }
    }

}
