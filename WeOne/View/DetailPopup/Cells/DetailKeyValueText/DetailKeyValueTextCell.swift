//
//  DetailKeyValueTextCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 25/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class DetailKeyValueTextCell: UITableViewCell {
    
    //MARK: - Variables

    //MARK: - Outlets
    @IBOutlet weak var lblTopLine: UILabel!
    @IBOutlet weak var lblBottomLine: UILabel!

    @IBOutlet weak var lblKey: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var leadingLblKey: NSLayoutConstraint!
    @IBOutlet weak var trailingValue: NSLayoutConstraint!
    
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel) {
        lblKey.text = model.placeholder
        lblValue.text = model.userText
        
        lblTopLine.isHidden = true
        lblBottomLine.isHidden = true
        
        
        leadingLblKey.constant = 20

        if model.cellType == CellType.DPPriceKeyValue {
            lblKey.textColor = ColorConstants.ThemeColor
            lblValue.textColor = ColorConstants.ThemeColor
            
            
//            lblKey.font = FontScheme.kMediumFont(size: 14)
//            lblValue.font = FontScheme.kSemiBoldFont(size: 14)
            
        } else if model.cellType == CellType.CMIPackageRate {

//            lblKey.font = FontScheme.KLBoldFont(size: 14)
//            lblValue.font = FontScheme.KLBoldFont(size: 14)

        }
//        else {
//            lblKey.textColor = ColorConstants.Payment.PaymentTextBlack.withAlphaComponent(0.7)
//            lblValue.textColor = ColorConstants.Payment.PaymentTextBlack
//            lblKey.font = FontScheme.kRegularFont(size: 14)
//            lblValue.font = FontScheme.kMediumFont(size: 14)
//        }
    }
    
}
