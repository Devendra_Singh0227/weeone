//
//  CarDetailCell.swift
//  WeOne
//
//  Created by CORUSCATEMAC on 25/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class CarDetailCell: UITableViewCell {
    
    //MARK: - Variables
    
    //MARK: - Outlets

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPriceRate: UILabel!
    @IBOutlet weak var lblNumberPlate: UILabel!
    @IBOutlet weak var lblGearType: UILabel!
    
    
    //MARK: - View's Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Set Data
    func setData(model:CellModel, rideModel:RideListModel) {
        
        imgView.setImageForURL(url: rideModel.vehicleId?.getVehicleImage, placeHolder: UIImage(named: "carPlaceholder"))
        
        lblName.text = rideModel.vehicleId?.name ?? "-"
        lblNumberPlate.text = rideModel.vehicleId?.numberPlate ?? "-"
        
        lblGearType.text = Utilities.getGearTypeName(gearBoxType: rideModel.vehicleId?.gearBox ?? 0) 
        lblPriceRate.text = "\(Utilities.convertToCurrency(number: rideModel.pricingConfigData?.rate ?? 0.0, currencyCode: rideModel.pricingConfigData?.currency))/\(rideModel.pricingConfigData?.getFormattedUnit ?? "")"
    }
    
}
