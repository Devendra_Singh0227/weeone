//
//  PopupModel.swift
//  OrganicHome
//
//  Created by Sierra on 02/04/18.
//  Copyright © 2018 CS-Mac-Mini. All rights reserved.
//

import UIKit

class PopupModel: NSObject {

    var id:String?
    var name:String?
    var isSelected = false
    
    override init() {
        
    }
    
    required init(strId:String,strName:String,selected:Bool = false) {
        id = strId
        name = strName
        isSelected = selected
    }
}
