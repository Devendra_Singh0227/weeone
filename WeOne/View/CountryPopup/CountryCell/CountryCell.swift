//
//  CountryCell.swift
//  OrganicHome
//
//  Created by Vish on 14/07/18.
//  Copyright © 2018 CS-Mac-Mini. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgSelected: UIImageView!
    
    @IBOutlet weak var imgWidth: NSLayoutConstraint!
    @IBOutlet weak var imgTrailing: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
