//
//  SelectionPopupVC.swift
//  PTE
//
//  Created by CS-Mac-Mini on 22/08/17.
//  Copyright © 2017 CS-Mac-Mini. All rights reserved.
//

import UIKit

class CountryListPopup: UIViewController,UITableViewDelegate,UITableViewDataSource {

    //MARK: IBOutlets
    @IBOutlet weak var viewNavigation: UIViewCommon!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet var vwContent: UIView!
    @IBOutlet var tblData: UITableView!
    
    @IBOutlet weak var txtSearch: UITextField!

    @IBOutlet weak var lblTitle: UILabel!
    //MARK: Variables
   
    var isFromCity = false
    var isFromSignup = false

    var arr_tblData = [String]()
    var arrSelectedData = [String]()
    var didSelectRow : ((String,Int) -> Void)?
    var selectedIndex:Int?
    var isFiltered:Bool = false
    var filtered: [String] = []
    var isShowSearchBar = true
    
    var arrPopUpData = [PopupModel]()
    var arrFilterPopUpData = [PopupModel]()

    var selectedCountry: ((Int) -> ())?
    
    var isShowSelectedImage = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initialConfig()
        self.viewDidLayoutSubviews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
                
    }
    
    //MARK: Private Methods
    func initialConfig(){
                
        self.tblData.register(UINib(nibName: "CountryCell", bundle: nil), forCellReuseIdentifier: "CountryCell")
        
        self.tblData.dataSource = self
        self.tblData.delegate = self
        
       // txtSearch.tintColor = ColorConstants.ThemeColor
        
        viewNavigation.click = {
            self.btnClose_Click()
        }
        if isFromSignup {
            btnBack.isHidden = false
            viewNavigation.isHidden = true
            lblTitle.text = "KLblTopCountries".localized
            txtSearch.placeholder = "KLblSearchcountry".localized
        } else {
            btnBack.isHidden = true
            viewNavigation.isHidden = false
            viewNavigation.text = "KLblSelectCity".localized as NSString
            lblTitle.text = "KLblClosestCities".localized
            txtSearch.placeholder = "KLblSearchcity".localized
        }
    }

    
    //MARK: IBAction
    func btnClose_Click() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnBack_Click(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: Search Methods
    func searchData(_ txt : String){
        if txt.count == 0 {
            isFiltered = false
        }
        else {
            isFiltered = true
            filtered.removeAll()
            
            filtered = arr_tblData.filter { $0.localizedCaseInsensitiveContains(txt) }
        }
        
        tblData.reloadData()
    }
    
    //MARK: TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(isFiltered) {
            return filtered.count
        }
        else{
            return arr_tblData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isFromSignup || isFromCity {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath) as? CountryCell
            self.setCellDataForParticularCell(cell: nil, countryCell: cell, indexPath: indexPath)
            return cell!

        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PopupCell", for: indexPath) as? PopupCell
            self.setCellDataForParticularCell(cell: cell, countryCell: nil, indexPath: indexPath)
            return cell!

        }
    }
    
    func setCellDataForParticularCell(cell : PopupCell? , countryCell : CountryCell?, indexPath : IndexPath){
        
        if(isFiltered){
            
            if isFromSignup || isFromCity{
                
                countryCell?.lblTitle.text = filtered[indexPath.row].components(separatedBy: ":")[0]
                
                if isFromSignup {
                    let url = URL(string : "http://flagpedia.net/data/flags/normal/\(filtered[indexPath.row].components(separatedBy: ":")[1].lowercased()).png")
                    countryCell?.imgView.sd_setImage(with: url, completed: { (image, error, cache, url) in
                        
                    })
                } else {
                    countryCell?.imgView.isHidden = true
                    countryCell?.imgWidth.constant = 0
                    countryCell?.imgTrailing.constant = 0
                }
            }else{
                
                cell?.lblTitle.text = filtered[indexPath.row]
            }
        } else {
            if isFromSignup || isFromCity {
                countryCell?.lblTitle.text = arr_tblData[indexPath.row].components(separatedBy: ":")[0]
                
                if isFromSignup {
                    let url = URL(string : "http://flagpedia.net/data/flags/normal/\(arr_tblData[indexPath.row].components(separatedBy: ":")[1].lowercased()).png")
                    countryCell?.imgView.sd_setImage(with: url, completed: { (image, error, cache, url) in
                    })
                } else {
                    countryCell?.imgView.isHidden = true
                    countryCell?.imgWidth.constant = 0
                    countryCell?.imgTrailing.constant = 0
                }
                
            }else{
                cell?.lblTitle.text = arr_tblData[indexPath.row]
            }
        }
        
        if(isShowSelectedImage){
            if(selectedIndex ?? 0 == indexPath.row)
            {
                if isFromSignup {
                    countryCell?.imgSelected.isHidden = false
                }else{
                    cell?.imgCheckBox.isHidden = false
                }
            }
            else
            {
                if isFromSignup {
                    countryCell?.imgSelected.isHidden = true
                }else{
                    cell?.imgCheckBox.isHidden = true
                }
            }
        }else{
            cell?.imgCheckBox.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(isFiltered) {
            let str = arr_tblData.filter { $0.contains(filtered[indexPath.row]) }
            if str.count > 0{
                self.selectedIndex = arr_tblData.firstIndex(of: str[0])
            }else{
                self.selectedIndex = indexPath.row
            }
        } else {
            self.selectedIndex = indexPath.row
        }
        selectedCountry?(selectedIndex ?? 0)
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  45.0
    }

}
//MARK: TextField delegate methods
extension CountryListPopup : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //print(textField.text!)
        searchData(textField.text ?? "")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       // textChanged?(txtSearch.text ?? "")
        return true
    }
    
    func textFieldDidChange(textField: UITextField) {
        print("Text-\(textField)")
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        searchData(newString)

        if !string.canBeConverted(to: String.Encoding.ascii){
            return false
        }

        if range.location == 0{

            if string == " " {
                return false
            }
        }
        
        return true
    }
}

