//
//  BookingconfirmationPopupVC.swift
//  WeOne
//
//  Created by Jecky Modi on 23/09/2019.
//  Copyright © 2019 Jecky Modi. All rights reserved

import UIKit
import PhoneNumberKit

class KeyHandlingPopupVC: UIViewController {

    //MARK: Variables
    var headerTitle:String?
    var keyType:String?
    var price:String?
    var imgKey:UIImage?
    var desc1: String?
    var desc2: String?
    var phoneNumber = ""

    var confirmClicked:(() -> ())?
    var showOnMapClicked: (() -> ())?
    var isShowSingleButton = false
    var isAttributted : Bool = false
    var isFromReserve : Bool = false

    var model: KeyHandlingModel?
    
    //MARK: Outlets
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDesc1: UILabel!
    @IBOutlet var lblDesc2: UILabel!

    @IBOutlet var lblKeyCode: UILabel!

    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnConfirm: UIButton!
    
    @IBOutlet var btnDismiss: UIButton!
    @IBOutlet var btnChat: UIButton!

    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewShowOnMap: UIView!
    @IBOutlet weak var viewConfirm: UIView!
    @IBOutlet weak var viewTost: UIViewCommonTost!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialConfig()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        Utilities.setNavigationBar(controller: self, isHidden: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    //MARK: Private Methods
    func initialConfig(){
        
        viewTost.text = "Provide the pin to \(model?.personName ?? "")" as NSString
        viewTost.imageIcon = "infoWhite" as NSString
        viewTost.setBackGroundColor(color: ColorConstants.TextColorGreen)

        lblTitle.text = headerTitle
        lblKeyCode.text = "\(model?.keyCode ?? " ")"
        
        phoneNumber = PartialFormatter().formatPartial("+\((model?.countryCode ?? "") + (model?.contactNumber ?? ""))")
        
        if isFromReserve {
            btnChat.isHidden = true
            btnDismiss.isHidden = false
            desc1 = "To get the key provide the pin\nto \(model?.personName ?? "") \(phoneNumber)"
            desc2 = "Key is located at \(model?.geoLocation?.street ?? "")"
            
        } else {
            btnChat.isHidden = false
            btnDismiss.isHidden = true
            desc1 = "To end the rental deliver the key and provide\nthe pin to \(model?.personName ?? "") \(phoneNumber)"
            desc2 = "Deliver the key at \(model?.geoLocation?.street ?? "")"
        }
        
        
        lblDesc1.text = desc1

//        lblDesc1.changeStringColor(string: desc1 ?? "", array: [phoneNumber], colorArray: [ColorConstants.ThemeColor],changeFontOfString :[phoneNumber], font : [FontScheme.kMediumFont(size: 16)])

        lblDesc2.text = desc2

//        lblDesc2.changeStringColor(string: desc2 ?? "", array: [(model?.geoLocation?.street ?? "")], colorArray: [ColorConstants.ThemeColor],changeFontOfString :[(model?.geoLocation?.street ?? "")], font : [FontScheme.kMediumFont(size: 16)])
            
        btnCancel.layer.cornerRadius = viewShowOnMap.frame.height / 2
        btnConfirm.layer.cornerRadius = viewConfirm.frame.height / 2

        btnCancel.addTarget(self, action: #selector(holdDownCancel(_:)), for: .touchDown)
        btnConfirm.addTarget(self, action: #selector(holdDownConfirm(_:)), for: .touchDown)

    }
    
    //MARK: IBActions
    @IBAction func btnCall_Clicked(_ sender: UIButton) {
        
        self.dismiss(animated: true) {
            self.view.showConfirmationPopupWithMultiButton(title: "KMakeACall".localized, message: "\("KDoYouCall".localized) \n\(self.phoneNumber)", cancelButtonTitle: "KLblCancelCap".localized, confirmButtonTitle: "KLblOKCap".localized, onConfirmClick: {
                        
                        let phone = self.phoneNumber.replacingOccurrences( of:"[^0-9]", with: "", options: .regularExpression)
                        
                        guard let number = URL(string: "tel://" + phone) else { return }
                        UIApplication.shared.open(number)

                        
                // Confirm Click
            }) {
                // cancel click
            }
        }
    }
    
    @objc func holdDownCancel(_ sender: UIButton) {
        btnCancel.backgroundColor = ColorConstants.UnderlineColor
    }

    @objc func holdDownConfirm(_ sender: UIButton) {
        btnConfirm.backgroundColor = ColorConstants.UnderlineColor
    }

    @IBAction func btnCancel_Clicked(_ sender: UIButton) {
        btnCancel.setTitleColor(ColorConstants.TextColorWhitePrimary, for: .normal)

        self.dismiss(animated: true) {
            self.showOnMapClicked?()
        }
    }
    
    @IBAction func btnConfirm_Clicked(_ sender: UIButton) {
        btnConfirm.setTitleColor(ColorConstants.TextColorWhitePrimary, for: .normal)

//        viewTost.isHidden = false
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
//            self.viewTost.isHidden = true
            self.dismiss(animated: true) {
                self.confirmClicked?()
            }

//        })

    }
    
    @IBAction func btnSeeLocationPhotos_Clicked(_ sender: UIButton) {
        
        self.dismiss(animated: true) {
            
            let vc = LocationPhotosPopupVC(nibName: "LocationPhotosPopupVC", bundle: nil)
            vc.confirmClicked = {
                
            }
            
            vc.cancelBtnClicked = {
                
            }
                        
            vc.arrLocationImages = self.model?.locationImages
            vc.modalPresentationStyle = .custom
            vc.modalTransitionStyle = .crossDissolve
            
            if UIViewController.current()?.presentedViewController != nil {
                UIViewController.current()?.presentedViewController?.present(vc, animated: true, completion: {
                })
            }
            else{
                UIViewController.current()?.present(vc, animated: true, completion: {
                })
            }
        }
    }

    @IBAction func btnChat_Click(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func btnDismissPopup_Click(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
