//  GMSMarkerInforWindow.swift
//  CheersWorld
//
//  Created by Mandeep Singh on 05/07/19.
//  Copyright © 2019 Mandeep Singh. All rights reserved.

import UIKit

class GMSMarkerInforWindow: UIView {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var info_Btn: UIButton!
    @IBOutlet weak var barImageView: UIImageView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib ()
    }
    
    func loadViewFromNib() {
        let view = UINib(nibName: "GMSMarkerInforWindow", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
        
    }
    
    
    
}
