//
//  GdprPopupVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 24/12/19.
//  Copyright © 2019 Coruscate Mac. All rights reserved.
//

import UIKit

class GdprPopupVC: UIViewController {

    var dict : [String:Any] = [:]
    var req : [String:Any] = [:]
    var isForUpdate : Bool = false
    var isFromSubmit : Bool = false
    var isFromSignup : Bool = false

    var submitGDPR : ((_ req : [String:Any]) -> ())?
    
    @IBOutlet weak var viewNavigation: UIViewCommon!
    @IBOutlet weak var btnSubmit: UIButtonCommon!

 //   @IBOutlet weak var btnShareEmail: UIButton!
    @IBOutlet weak var btnUseLocation: UIButton!
    @IBOutlet weak var btnContactViaEmail: UIButton!
    @IBOutlet weak var btnAddToNewsLetter: UIButton!
    
    @IBOutlet weak var btnSelectAll: UIButton!

    @IBOutlet weak var viewMain: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

    
        btnSubmit.click = {
            self.btnSubmit_Click()
        }

        if isFromSignup {
            btnSubmit.isHidden = false 
        }
        
        
        if dict.count > 0{
            
            req = dict["gdprCompliance"] as? [String:Any] ?? [:]
            
          //  btnShareEmail.isSelected = req["isShareEmail"] as? Bool ?? false
            btnUseLocation.isSelected = req["isShareLocation"] as? Bool ?? false
//            btnContactViaEmail.isSelected = req["isNotContactViaEmail"] as? Bool ?? false
//            btnAddToNewsLetter.isSelected = req["isNoAddInNewsLetter"] as? Bool ?? false
            
        }else{
            
//            req["isShareEmail"] = false
            req["isShareLocation"] = false
//            req["isNotContactViaEmail"] = false
//            req["isNoAddInNewsLetter"] = false
        }
        
        viewNavigation.click = {
            if self.isFromSubmit {
                self.btnSubmit_Click()
            } else {
                self.btnDismiss_Click()
            }
        }

    }

    @IBAction func btnShareEmail_Click(_ sender: UIButton) {
      //  btnShareEmail.isSelected = !btnShareEmail.isSelected
      //  req["isShareEmail"] = btnShareEmail.isSelected
    }
    
    @IBAction func btnShareLocation_Click(_ sender: UIButton) {
        btnUseLocation.isSelected = !btnUseLocation.isSelected
        req["isShareLocation"] = btnUseLocation.isSelected
    }
    
    @IBAction func btnContactViaEmail_Click(_ sender: UIButton) {
        btnContactViaEmail.isSelected = !btnContactViaEmail.isSelected
        req["isNotContactViaEmail"] = btnContactViaEmail.isSelected
    }
    
    @IBAction func btnAddNewsLetter_Click(_ sender: UIButton) {
        btnAddToNewsLetter.isSelected = !btnAddToNewsLetter.isSelected
        req["isNoAddInNewsLetter"] = btnAddToNewsLetter.isSelected
    }
    
    @IBAction func btnSelectAll_Click(_ sender: UIButton) {
        
        btnSelectAll.isSelected = !btnSelectAll.isSelected

        [btnUseLocation, btnContactViaEmail, btnAddToNewsLetter].forEach {
            $0?.isSelected = btnSelectAll.isSelected
        }

     //   req["isShareEmail"] = btnShareEmail.isSelected
        req["isShareLocation"] = btnUseLocation.isSelected
//        req["isNotContactViaEmail"] = btnContactViaEmail.isSelected
//        req["isNoAddInNewsLetter"] = btnAddToNewsLetter.isSelected
    }

    func btnDismiss_Click() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func btnSubmit_Click() {
        
        self.dict["gdprCompliance"] = self.req
        
        if isFromSignup {
            self.callAPIForUpdateProfile(self.dict)
        } else {
            self.submitGDPR?(self.dict)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func callAPIForUpdateProfile(_ dictGdpr : [String:Any]) {
        
        var param = [String:Any]()
        if (dictGdpr["gdprCompliance"] as? [String:Any] ?? [:]).count > 0{
            param["gdprCompliance"] = dictGdpr["gdprCompliance"]
        }
        
        NetworkClient.sharedInstance.showIndicator("", stopAfter: 0.0)
        
        NetworkClient.sharedInstance.request(AppConstants.serverURL, command: AppConstants.URL.UpdateProfile, method: .put, parameters: param, headers: ApplicationData.sharedInstance.authorizationHeaders, success: { (response, message) in
            
            if let dictResponse = response as? [String:Any] {
                ApplicationData.sharedInstance.saveUserData(dictResponse)

                if AppNavigation.shared.checkUserHasAccess(vc: self) {
                    AppNavigation.shared.moveToHome()
                }
                
            }
        }) { (failureMessage, failureCode) in
            Utilities.showAlertView(message: failureMessage)
        }
    }

}
