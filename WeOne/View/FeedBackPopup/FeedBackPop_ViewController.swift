//
//  FeedBackPop_ViewController.swift
//  WeOne
//
//  Created by Dev's Mac on 17/08/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class FeedBackPop_ViewController: UIViewController {

    var desc:NSAttributedString?
    
    @IBOutlet weak var desc_Lbl: UILabel!

    var cancelBtnClicked: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        desc_Lbl.attributedText = desc
        // Do any additional setup after loading the view.
    }

    @IBAction func cross_Btn(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.cancelBtnClicked?()
        }
       }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
