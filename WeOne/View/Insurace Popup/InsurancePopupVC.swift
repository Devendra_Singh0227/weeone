//
//  InsurancePopupVC.swift
//  WeOne
//
//  Created by Coruscate Mac on 05/02/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class InsurancePopupVC: UIViewController {

    var vehicle : VehicleModel?
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblShowInsuranceInfo: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewMain.layer.cornerRadius = 20
        
        viewMain.layer.shadowColor = UIColor.black.withAlphaComponent(1.0).cgColor
        viewMain.layer.shadowOpacity = 0.3
        viewMain.layer.shadowOffset = CGSize(width: 0, height: -5)
        viewMain.layer.shadowRadius = 10.0
        viewMain.layer.masksToBounds = false
        viewMain.layer.shadowPath = CGPath(ellipseIn: CGRect(x: -20,y: -20 * 0.5,width: viewMain.layer.bounds.width + 20 * 2,height: 5),transform: nil)
        
        lblDesc.text = String(format : "KLblOrderInsuranceDesc".localized, Utilities.convertToCurrency(number: vehicle?.insurance?.insuranceTo ?? 0, currencyCode: vehicle?.currency),Utilities.convertToCurrency(number: vehicle?.insurance?.insuranceFrom ?? 0, currencyCode: vehicle?.currency),Utilities.convertToCurrency(number: vehicle?.insurance?.insuranceAmount ?? 0, currencyCode: vehicle?.currency))
        
//        lblShowInsuranceInfo.changeStringColor(string: "KLblShowInsuranceTerms".localized, array: ["KLblShowInsuranceTerms".localized], colorArray: [ColorConstants.ThemeColor], font : [FontScheme.kMediumFont(size: 16)])
         
//        lblShowInsuranceInfo.changeStringUnderLine(attrStr: lblShowInsuranceInfo.attributedText?.mutableCopy() as! NSMutableAttributedString, array: ["KLblShowInsuranceTerms".localized], font: FontScheme.kLightFont(size: 16))
    }
    
    @IBAction func btnOk_Click(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnShowTerms_Click(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            Utilities.openStaticPage(title: AppConstants.StaticCode.InsuranceTerms, url: AppConstants.URL.StaticPage)
        })
    }
}
