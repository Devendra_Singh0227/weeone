//
//  PromocodeAppliedPopup.swift
//  WeOne
//
//  Created by Coruscate Mac on 11/06/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import UIKit

class PromocodeAppliedPopup: UIViewController {

    var isDismissed : (() -> ())?
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    @IBAction func btnDismiss_Click(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.isDismissed?()
        }
    }
}
