//
//  WeOneUITests.swift
//  WeOneUITests
//
//  Created by Coruscate Mac on 03/01/20.
//  Copyright © 2020 Coruscate Mac. All rights reserved.
//

import XCTest

class WeOneUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        continueAfterFailure = true
        
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
//        let app = XCUIApplication()
//        setupSnapshot(app)
//        app.launch()
//
//
//        XCUIDevice.shared.orientation = .portrait
//
//        addUIInterruptionMonitor(withDescription: "Location Dialog") { (alert) -> Bool in
//
//            do {
//              // Location
//              let button = alert.buttons["Only While Using the App"]
//              if button.exists {
//                button.tap()
//              }
//            }
//
//            do {
//              // Push Notification
//              let button = alert.buttons["Allow"]
//              if button.exists {
//                button.tap()
//              }
//            }
//
//            return true
//        }
//
//        app.tap()
//    }
//
//    override func tearDown() {
//        super.tearDown()
//        // Put teardown code here. This method is called after the invocation of each test method in the class.
//    }
//
//    func testExample() {
//        // UI tests must launch the application that they test.
//
//        let app = XCUIApplication()
//
//        snapshot("0-Splash")
//
//        app.buttons["btnDriveNow"].tap()
//
//        snapshot("1-Car Collection")
//
//
//        let operation = AsyncOperation { "Hello, world!" }
//        let expectation = self.expectation(description: #function)
//
//        // When
//        operation.perform { value in
//            expectation.fulfill()
//        }
//
//        // Wait for the expectation to be fullfilled, or time out
//        // after 5 seconds. This is where the test runner will pause.
//        waitForExpectations(timeout: 5)
//
//        app.collectionViews.allElementsBoundByIndex[0].buttons["btnBookNow"].tap()
//
//        snapshot("2-Car Detail")
//
//        app.buttons["btnBack"].tap()
//
//        app.buttons["btnBackToHome"].tap()
//
//        app.buttons["btnBackToHome"].tap()
//
//        snapshot("3-LeftMenu")
//
//        app.buttons["btnHeader"].tap()
//
//        snapshot("4-Login")
//
//        app.buttons["btnSignup"].tap()
//
//        snapshot("5-Signup")
//    }
//
//    func testLaunchPerformance() {
//        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
//            // This measures how long it takes to launch your application.
//            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
//                XCUIApplication().launch()
//            }
//        }
    }
}

struct AsyncOperation<Value> {
    let queue: DispatchQueue = .main
    let closure: () -> Value

    func perform(then handler: @escaping (Value) -> Void) {
        queue.async {
            let value = self.closure()
            handler(value)
        }
    }
}
